﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.FullCalender
{
    public class CalenderEntryWebModel
    {
        public int id { get; set; }

        public string title { get; set; }

        public bool allDay { get; set; }

        public DateTime start { get; set; }

        public DateTime end { get; set; }

        public string url { get; set; }

        public string className { get; set; }

        public bool editable { get; set; }

        public bool startEditable { get; set; }

        public bool durationEditable { get; set; }

        public string source { get; set; }

        public string color { get; set; }

        public string backgroundColor { get; set; }

        public string borderColor { get; set; }

        public string textColor { get; set; }

        public string description { get; set; }

        public string background { get; set; }

        public string cls { get; set; }

        public int? clientId { get; set; }

        public bool? isNoShow { get; set; }

        public bool isGeneralEntry { get; set; }

        public string comment { get; set; }

        public string rendering { get; set; }
       
    }
}
