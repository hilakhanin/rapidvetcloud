﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace RapidVet.WebModels.FullCalender
{
    public class RoomsCalenderWebModel
    {
        public List<SelectListItem> Rooms { get; set; }
    }
}