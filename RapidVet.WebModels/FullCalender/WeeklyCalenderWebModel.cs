﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RapidVet.WebModels.ClinicTasks;
using RapidVet.WebModels.Users;
using RapidVet.WebModels.Doctors;

namespace RapidVet.WebModels.FullCalender
{
    public class WeeklyCalenderWebModel
    {
        public List<CalendarModel> Calendars { get; set; }

        public List<SelectListItem> Doctors { get; set; }

        public List<ClinicTaskWebModel> Tasks { get; set; }

        public string SelectedDoctorId { get; set; }

        public int RowsNum { get; set; }

        public string GeneralText { get; set; }
    }
}
