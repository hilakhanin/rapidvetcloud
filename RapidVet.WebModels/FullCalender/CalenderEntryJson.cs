﻿using System;

namespace RapidVet.WebModels.FullCalender
{
    public class CalenderEntryJson
    {
        public int entryId { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public int duration { get; set; }
        public int? roomId { get; set; }
        public int doctorId { get; set; }
        public int byUserId { get; set; }
        public int? clientId { get; set; }
        public string clientName { get; set; }
        public int? patientId { get; set; }
        public string comments { get; set; }
        public string color { get; set; }
        public string title { get; set; }
        public bool isNewClient { get; set; }
        public bool? isNoShow { get; set; }
        public bool isGeneralEntry { get; set; }
    }
}

     