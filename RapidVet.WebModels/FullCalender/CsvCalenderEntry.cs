﻿namespace RapidVet.WebModels.FullCalender
{
    public class CsvCalenderEntry
    {        
        public string date { get; set; }
        public string day { get; set; }
        public string time { get; set; }
        public int duration { get; set; }
        public string doctorName { get; set; }
        public string comments { get; set; }
    }
}