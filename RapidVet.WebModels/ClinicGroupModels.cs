﻿
using System.ComponentModel.DataAnnotations;
using System.Web;
namespace RapidVet.WebModels.ClinicGroupModels
{
    public class ClinicGroupCreate
    {
        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup) , Name = "Name")]
        public string Name { get; set; }

         [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "Comments")]
        public string Comments { get; set; }

    }

    public class ClinicGroupEdit
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Comments { get; set; }

        public HttpPostedFileBase LogoImage { get; set; }
    }
}
