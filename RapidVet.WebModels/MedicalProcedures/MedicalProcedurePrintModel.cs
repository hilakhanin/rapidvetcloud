﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.MedicalProcedures
{
    public class MedicalProcedurePrintModel
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public string Letter { get; set; }
    }
}
