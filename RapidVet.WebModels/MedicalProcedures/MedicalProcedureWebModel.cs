﻿using System;
using System.ComponentModel.DataAnnotations;
using RapidVet.Enums;
using RapidVet.WebModels.Visits;

namespace RapidVet.WebModels.MedicalProcedures
{
    public class MedicalProcedureWebModel
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [UIHint("_ClinicDiagnoses")]
        public int DisagnosisId { get; set; }

        public DiagnosisAdminModel Diagnosis { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public DateTime ProcedureDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public DateTime Start { get; set; }

        public DateTime Finish { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public string ProcedureName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [UIHint("_MedicalProcedureType")]
        public int ProcedureTypeId { get; set; }

        public MedicalProcedureType ProcedureType
        {
            get { return (MedicalProcedureType)ProcedureTypeId; }
            set { ProcedureTypeId = (int)value; }
        }

        public string ProcedureLocation { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public string SurgeonName { get; set; }

        public string AnesthesiologistName { get; set; }

        public string NurseName { get; set; }

        public string AdditionalPersonnel { get; set; }

        public string Summery { get; set; }
    }
}
