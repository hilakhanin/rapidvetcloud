﻿namespace RapidVet.WebModels
{
  public class ClientSearchResult
    {
      public int value { get; set; }
      public string label { get; set; }
    }
}
