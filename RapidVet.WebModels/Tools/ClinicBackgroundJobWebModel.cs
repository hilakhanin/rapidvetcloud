﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Tools
{
    public enum BackgroundJobType
    {
        Email, SMS
    }

    public class ClinicBackgroundJobWebModel
    {
        [Key]
        public int Id { get; set; }
        public string JobGuid { get; set; }
        public int ClinicId { get; set; }       
        public string JobId { get; set; }
        public string JobModule { get; set; }
        public BackgroundJobType BackgroundJobType { get; set; }
        public int Processed { get; set; }
        public int Total { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int InitiatingUserId { get; set; }
        public string InitiatingUserName { get; set; }
        public bool WasCancelled { get; set; }
        public int CancellingUserId { get; set; }
        public string CancellingUserName { get; set; }
    }
}
