﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Finance;

namespace RapidVet.WebModels.VaccinesAndTreatments
{
    public class VaccineOrTreatmentJsonModel
    {
        public int id { get; set; }

        public string name { get; set; }

        public int order { get; set; }   
    }
}
