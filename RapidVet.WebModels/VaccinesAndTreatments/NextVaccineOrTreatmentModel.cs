﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Finance;

namespace RapidVet.WebModels.VaccinesAndTreatments
{
    public class NextVaccineOrTreatmentModel
    {
        public int Id { get; set; }

        public int VaccineOrTreatmentId { get; set; }

        public VaccineOrTreatmentModel VaccineOrTreatment { get; set; }

        public int ParentVaccineOrTreatmentId { get; set; }

        public int DaysToNext { get; set; }
    }
}
