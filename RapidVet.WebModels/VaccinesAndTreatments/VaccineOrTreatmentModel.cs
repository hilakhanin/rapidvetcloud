﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Finance;

namespace RapidVet.WebModels.VaccinesAndTreatments
{
    public class VaccineOrTreatmentModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Order { get; set; }

        public int AnimalKindId { get; set; }

        public int PriceListItemId { get; set; }

        public PriceListItemModel PriceListItem { get; set; }
    }
}
