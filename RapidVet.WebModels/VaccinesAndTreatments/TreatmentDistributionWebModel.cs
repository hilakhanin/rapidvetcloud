﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace RapidVet.WebModels.VaccinesAndTreatments
{
    public class TreatmentDistributionWebModel
    {
        public List<SelectListItem> Doctors { get; set; }

        public List<TreatmentDistribution> TreatmentsDistributions { get; set; }
    }
}