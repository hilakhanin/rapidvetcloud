﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Finance;

namespace RapidVet.WebModels.VaccinesAndTreatments
{
    public class NextVaccineOrTreatmentJsonModel
    {
        public int id { get; set; }

        public string name { get; set; }

        public int days { get; set; }   
    }
}
