﻿using System;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.VaccinesAndTreatments
{
    public class TreatmentDistribution
    {
        public string Name { get; set; }

        public decimal Quantity { get; set; }

        public decimal Percent { get; set; }
    }
}

