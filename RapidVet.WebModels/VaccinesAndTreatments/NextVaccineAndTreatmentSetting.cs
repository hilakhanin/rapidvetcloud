﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Patients;

namespace RapidVet.WebModels.VaccinesAndTreatments
{
    public class NextVaccineAndTreatmentSetting
    {
        public List<VaccineOrTreatmentModel> AllItems { get; set; }

        public List<NextVaccineOrTreatmentModel> SelectedItems { get; set; }

        public int AnimalKindId { get; set; }

        public AnimalKindModel AnimalKind { get; set; }

        public VaccineOrTreatmentModel VaccineOrTreatment { get; set; }

        public int VaccineOrTreatmentId { get; set; }
    }
}
