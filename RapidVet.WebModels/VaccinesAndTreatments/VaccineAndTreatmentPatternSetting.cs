﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Patients;

namespace RapidVet.WebModels.VaccinesAndTreatments
{
    public class VaccineAndTreatmentPatternSetting
    {
        public List<PriceListItemModel> AllItems { get; set; }

        public List<VaccineOrTreatmentModel> SelectedItems { get; set; }

        public int AnimalKindId { get; set; }

        public AnimalKindModel AnimalKind { get; set; }
    }
}
