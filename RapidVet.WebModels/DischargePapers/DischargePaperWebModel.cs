﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RapidVet.WebModels.DischargePapers
{
    public class DischargePaperWebModel
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public string From { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public string To { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public string Summery { get; set; }

        public string Background { get; set; }

        public string Recommendations { get; set; }

        public DateTime? CheckUp { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public DateTime DateTime { get; set; }
    }
}
