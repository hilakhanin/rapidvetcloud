﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Users
{
    public class UserRolesModel
    {
        public int UserId { get; set; }

        private ICollection<UserClinicRoles> _userClinicRoles;

        public ICollection<UserClinicRoles> ClinicRoles
        {
            get { return _userClinicRoles ?? (_userClinicRoles = new Collection<UserClinicRoles>()); }
            set { _userClinicRoles = value; }
        }
    }

    public class UserClinicRoles
    {
        public int ClinicId { get; set; }

        public string ClinicName { get; set; }

        public bool isClinicManager { get; set; }

        public bool isDoctor { get; set; }

        public bool isSecretary { get; set; }

        public bool isViewReports { get; set; }

        public bool isViewFinancialReports { get; set; }

        public bool isExcelExport { get; set; }

        public bool isNoDiary { get; set; }
    }

}
