﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RapidVet.WebModels.Users
{
    public class ChangePasswordModel
    {
        public int? Id { get; set; }

        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
      //  [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.WebModels.Account), Name = "NewPassword")]
        //[MinLength(6, ErrorMessageResourceType = typeof(Resources.WebModels.Account), ErrorMessageResourceName = "MinPasswordLength")]
        [StringLength(30, MinimumLength = 6, ErrorMessageResourceType = typeof(Resources.WebModels.Account), ErrorMessageResourceName = "MinPasswordLength")]

        public string NewPassword { get; set; }


        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.WebModels.Account), Name = "ConfirmPassword")]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(Resources.WebModels.Account), ErrorMessageResourceName = "PasswordNotMatch")]        
        public string ConfirmPassword { get; set; }
    }
}
