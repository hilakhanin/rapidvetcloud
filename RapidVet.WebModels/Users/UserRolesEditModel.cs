﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Users
{
    public class UserRolesEditModel
    {
        public int UserId { get; set; }

        public string UserFullName { get; set; }

        private ICollection<UserClinicRoles> _roles;

        public ICollection<UserClinicRoles> Roles
        {
            get { return _roles ?? (_roles = new Collection<UserClinicRoles>()); }
            set { _roles = value; }
        }

        public bool isClinicGroupManager { get; set; }
    }
}
