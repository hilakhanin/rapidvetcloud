﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RapidVet.WebModels.Users
{
    public class UserPreventiveMedicineReminderFilterWebModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string FromDateStr { get; set; }

        public string ToDateStr { get; set; }

        public string FromReminderDateStr { get; set; }

        public string ToReminderDateStr { get; set; }

        public int? AnimalKindId { get; set; }
        
        public int? RegionalCouncilId { get; set; }        

        private List<int> _priceLisItemIds;
        public List<int> PriceListItemIds
        {
            get { return _priceLisItemIds ?? (_priceLisItemIds = new List<int>()); }
            set { _priceLisItemIds = value; }
        }

        private List<int> _cityIds;
        public List<int> CityIds
        {
            get { return _cityIds ?? (_cityIds = new List<int>()); }
            set { _cityIds = value; }
        }

        public bool ShowOnlyFutureReminders { get; set; }

        public bool ShowNotActiveClients { get; set; }

        public bool ShowNotActivePatients { get; set; }

        public bool RemoveClientsWithFutureCalendarEntries { get; set; }
        
        public int? DoctorId { get; set; }
    }
}
