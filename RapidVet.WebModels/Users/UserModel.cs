﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

namespace RapidVet.WebModels.Users
{
    public class UserModel
    {

        [Key]
        public virtual int Id { get; set; }

        public int ClinicGroupId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.User), Name = "Username")]
        [Remote("IsUserNameExists", "Validation", AdditionalFields = "Id")]
        public virtual String Username { get; set; }

        [Display(ResourceType = typeof(Resources.User), Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public virtual String Email { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.User), Name = "FirstName")]
        public virtual String FirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.User), Name = "LastName")]
        public virtual String LastName { get; set; }

        [Display(ResourceType = typeof(Resources.User), Name = "MobilePhone")]
        [RegularExpression("^[0-9-]*$", ErrorMessage = "יש להזין ספרות ומקפים בלבד")]
        public virtual String MobilePhone { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.User), Name = "Comment")]
        public virtual String Comment { get; set; }

        //     [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [MinLength(6, ErrorMessage = "יש להזין סיסמה בת 6 תווים לפחות")]
        [Display(ResourceType = typeof(Resources.User), Name = "Password")]
        public virtual string Password { get; set; }

        // [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Compare("Password", ErrorMessage = "הסיסמאות אינן תואמות")]
        [Display(ResourceType = typeof(Resources.User), Name = "PasswordReType")]
        public virtual string PasswordReType { get; set; }

        [Display(ResourceType = typeof(Resources.User), Name = "DefaultClinic")]
        public int? DefaultClinicId { get; set; }

        [Display(ResourceType = typeof(Resources.User), Name = "Title")]
        [UIHint("Title")]
        public int? TitleId { get; set; }

        [Display(ResourceType = typeof(Resources.User), Name = "ShortCut")]
        [UIHint("ShortCuts")]
        public int? ShortCutAId { get; set; }

        [Display(ResourceType = typeof(Resources.User), Name = "ShortCut")]
        public int? ShortCutBId { get; set; }

        [Display(ResourceType = typeof(Resources.User), Name = "ShortCut")]
        public int? ShortCutCId { get; set; }

        [Display(ResourceType = typeof(Resources.User), Name = "LicenceNumber")]
        public String LicenceNumber { get; set; }

         [Display(ResourceType = typeof(Resources.User), Name = "DefaultDrId")]
         [UIHint("DefaultDr")]
        public int? DefaultDrId { get; set; }

         [Display(ResourceType = typeof(Resources.User), Name = "DefaultIssuerEmployerId")]
         [UIHint("Issuers")]
         public int? DefaultIssuerEmployerId { get; set; }

         public bool IsDoctor { get; set; }
    }
}
