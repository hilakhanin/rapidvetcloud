﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Users
{
    public class LogOnModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.WebModels.Account), Name = "UserName")]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.WebModels.Account), Name = "Password")]
        public string Password { get; set; }

        [Display(ResourceType = typeof(Resources.WebModels.Account), Name = "RememberMe")]
        public bool RememberMe { get; set; }
    }
}
