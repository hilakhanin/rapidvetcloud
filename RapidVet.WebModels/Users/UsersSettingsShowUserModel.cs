﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Users
{
    public class UsersSettingsShowUserModel
    {
        public int Id { get; set; }
        public bool Show { get; set; }
        public int DoctorId { get; set; }
        public string User { get; set; }
    }
}
