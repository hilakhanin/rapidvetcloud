﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RapidVet.Enums;
using RapidVet.Resources;

namespace RapidVet.WebModels.Users
{
    public class UsersSettingsWebModel
    {
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String MobilePhone { get; set; }

        [UIHint("Title")]
        public int? TitleId { get; set; }

        [UIHint("ShortCuts")]
        public int? ShortCutAId { get; set; }

        [UIHint("ShortCuts")]
        public int? ShortCutBId { get; set; }

        [UIHint("ShortCuts")]
        public int? ShortCutCId { get; set; }

      //  public bool ThermalPrinter { get; set; }

        [UIHint("Issuers")]
        public int? DefaultIssuerId { get; set; }

        [UIHint("HomePageDisplays")]
        public int HomePageDisplay { get; set; }

        public string LicenseNumber { get; set; }

        public List<UsersSettingsShowUserModel> ShowUsersList { get; set; }
    }
}
