﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.ClinicGroups
{
    public class TemplateKeyWordsWebModel
    {
        public string TemplateKeyWord { get; set; }
        public string KeyWord { get; set; }
        public string KeyWordComment { get; set; }
    }
}
