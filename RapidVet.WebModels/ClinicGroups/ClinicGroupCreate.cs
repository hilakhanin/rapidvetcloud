﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;

namespace RapidVet.WebModels.ClinicGroups
{
    public class ClinicGroupCreate
    {
        /// <summary>
        /// clinicGroup name
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// clinicGroup comments
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "Comments")]
        public string Comments { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "LogoImage")]
        public HttpPostedFileBase LogoImage { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "MaxConcurrentUsers")]
        [RegularExpression(@"([1-9]\d{0,2})", ErrorMessageResourceType = typeof(Resources.ClinicGroup), ErrorMessageResourceName = "MaxConcurrentUsersError")] //1-999
        //[RegularExpression("[0-9]+", ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "PositiveNumber")]
        public int MaxConcurrentUsers { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "MaxGoogleSyncAllowed")]
        [RegularExpression(@"([0-9]\d{0,2})", ErrorMessageResourceType = typeof(Resources.ClinicGroup), ErrorMessageResourceName = "MaxGoogleSyncAllowedError")] //0-999
        //[RegularExpression("[0-9]+", ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "PositiveNumber")]
        public int MaxGoogleSyncAllowed { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "TimeToDisconnectInactiveUsersInMinutes")]
        [RegularExpression(@"^(?:[5-9]|\d{1,2}|1\d{1,2}|2[0-3]\d|240)$", ErrorMessageResourceType = typeof(Resources.ClinicGroup), ErrorMessageResourceName = "InactiveUsersError")]        //5-240
        //[RegularExpression(@"^(?:\d|[1-3]\d|4[0-5])$")] 0-45
        public int TimeToDisconnectInactiveUsersInMinutes { get; set; }
    }
}
