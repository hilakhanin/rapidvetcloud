﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.ClinicGroups
{
    public class AdministrativeDataJsonModel
    {
        public bool IsAdministrator { get; set; }
        public int ActiveUsers { get; set; }
        public int NonActiveUsers { get; set; }
    }
}
