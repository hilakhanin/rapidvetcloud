﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;

namespace RapidVet.WebModels.ClinicGroups
{
    public class ClinicGroupEdit
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "Name")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "Comments")]
        public string Comments { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "MaxConcurrentUsers")]
        [RegularExpression(@"([1-9]\d{0,2})", ErrorMessageResourceType = typeof(Resources.ClinicGroup), ErrorMessageResourceName = "MaxConcurrentUsersError")] //1-999
        //[RegularExpression("[0-9]+", ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "PositiveNumber")]
        public int MaxConcurrentUsers { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "MaxGoogleSyncAllowed")]
        [RegularExpression(@"([0-9]\d{0,2})", ErrorMessageResourceType = typeof(Resources.ClinicGroup), ErrorMessageResourceName = "MaxGoogleSyncAllowedError")] //1-999
        //[RegularExpression("[0-9]+", ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "PositiveNumber")]
        public int MaxGoogleSyncAllowed { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "TimeToDisconnectInactiveUsersInMinutes")]
        [RegularExpression(@"^(?:[5-9]|\d{1,2}|1\d{1,2}|2[0-3]\d|240)$", ErrorMessageResourceType = typeof(Resources.ClinicGroup), ErrorMessageResourceName = "InactiveUsersError")]        //5-240
        //[RegularExpression(@"^(?:\d|[1-3]\d|4[0-5])$")] 0-45
        public int TimeToDisconnectInactiveUsersInMinutes { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "LogoImage")]
        public HttpPostedFileBase LogoImage { get; set; }

        public bool HasLogo { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "UnicellUserName")]
        public string UnicellUserName { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "UnicellPassword")]
        public string UnicellPassword { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "CalenderEntrySmsTemplate")]
        public string CalenderEntrySmsTemplate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "CalenderEntryEmailTemplate")]
        public string CalenderEntryEmailTemplate { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "CalenderEntryEmailSubject")]
        public string CalenderEntryEmailSubject { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "BirthdaySmsTemplate")]
        public string BirthdaySmsTemplate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PreventiveMedicineSmsTemplate")]
        public string PreventiveMedicineSmsTemplate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PreventiveMedicineSmsMultiAnimalsTemplate")]
        public string PreventiveMedicineSmsMultiAnimalsTemplate { get; set; }

        
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "SignatureSmsTemplate")]
        public string SignatureSmsTemplate { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "EmailAddress")]
        public string EmailAddress { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "RabiesReportCCAddress")]
        public bool RabiesReportCC { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "EmailBusinessName")]
        public string EmailBusinessName { get; set; }
       
        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PreventiveMedicineLetterTemplate")]
        public string PreventiveMedicineLetterTemplate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PreventiveMedicineLetterMultiAnimalsTemplate")]
        public string PreventiveMedicineLetterMultiAnimalsTemplate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PreventiveMedicinePostCardTemplate")]
        public string PreventiveMedicinePostCardTemplate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PreventiveMedicinePostCardMultiAnimalsTemplate")]
        public string PreventiveMedicinePostCardMultiAnimalsTemplate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PreventiveMedicineStikerTemplate")]
        public string PreventiveMedicineStikerTemplate { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PreventiveMedicineEmailSubject")]
        public string PreventiveMedicineEmailSubject { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PreventiveMedicineEmailTemplate")]
        public string PreventiveMedicineEmailTemplate { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PreventiveMedicineEmailMultiAnimalsSubject")]
        public string PreventiveMedicineEmailMultiAnimalsSubject { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PreventiveMedicineEmailMultiAnimalsTemplate")]
        public string PreventiveMedicineEmailMultiAnimalsTemplate { get; set; }


        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PostCardWidth")]
        public decimal PostCardWidth { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PostCardHight")]
        public decimal PostCardHight { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "StikerColumns")]
        public int StikerColumns { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "StikerRows")]
        public int StikerRows { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "StikerRowHight")]
        public decimal StikerRowHight { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "StikerTopMargins")]
        public decimal StikerTopMargin { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "StikerMargins")]
        public decimal StickerSideMargin { get; set; }

        //[DataType(DataType.MultilineText)]
        //[Display(ResourceType = typeof(Resources.ClinicGroup), Name = "BirthdayLetter")]
        //public string BirthdayLetterTemplate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "BirthdaySticker")]
        public string BirthdayStickerTemplate { get; set; }


        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "BirthdayEmailTemplate")]
        public string BirthdayEmailTemplate { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "BirthdayEmailSubject")]
        public String BirthdayEmailSubject { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "TreatmentSmsTemplate")]
        public string TreatmentSmsTemplate { get; set; }

        //[DataType(DataType.MultilineText)]
        //[Display(ResourceType = typeof(Resources.ClinicGroup), Name = "PersonalLetterTemplate")]
        //public string PersonalLetterTemplate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "TreatmentStikerTemplate")]
        public string TreatmentStikerTemplate { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "TreatmentEmailSubject")]
        public string TreatmentEmailSubject { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "TreatmentEmailTemplate")]
        public string TreatmentEmailTemplate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "ClientsSmsTemplate")]
        public string ClientsSmsTemplate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "ClientsStikerTemplate")]
        public string ClientsStikerTemplate { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "ClientsEmailSubject")]
        public string ClientsEmailSubject { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "ClientsEmailTemplate")]
        public string ClientsEmailTemplate { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "ActivateHSModul")]
        public bool HSModule { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "ActivateStockModul")]
        public bool StockModule { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "FollowUpsEmailSubject")]
        public string FollowUpsEmailSubject { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "FollowUpsEmailTemplate")]
        public string FollowUpsEmailTemplate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "FollowUpsSmsTemplate")]
        public string FollowUpsSmsTemplate { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "TrialPeriod")]
        public bool TrialPeriod { get; set; }

        public string ExpirationDate { get; set; }

        [Display(ResourceType = typeof(Resources.ClinicGroup), Name = "UnitePreventiveReminders")]
        public bool UnitePreventiveReminders { get; set; }
    }
}
