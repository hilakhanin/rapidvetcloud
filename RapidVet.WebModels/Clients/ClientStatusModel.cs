﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clients
{
    public class ClientStatusModel
    {
        public int Id { get; set; }

        public int ClinicGroupId { get; set; }

        public string Name { get; set; }

        public string MeetingColor { get; set; }

        public bool Active { get; set; }
    }
}
