﻿using System;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clients
{
    public enum ClientPaymentInfoType
    {
        Treatment, Invoice, Reciept, InvoiceReciept, InvoiceCredit, ManualChange, Proforma, Appointment
    }

    public class ClientPaymentInfo
    {

        public ClientPaymentInfoType ClientPaymentInfoType { get; set; }

        public DateTime Date { get; set; }

        public string Details { get; set; }

        public string InvoiceNumber { get; set; }

        public decimal Credit { get; set; }

        public decimal Debt { get; set; }

        public decimal? Redeemed { get; set; }

        public DateTime? PrintDate { get; set; }

        public int FinanceDocumentId { get; set; }

        public bool Active { get; set; }

        public bool ConvertedFromRecipt { get; set; }
    }
}
