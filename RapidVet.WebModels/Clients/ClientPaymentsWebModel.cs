﻿using System.Collections.Generic;

namespace RapidVet.WebModels.Clients
{
    public class ClientPaymentsWebModel
    {

        public List<ClientPaymentInfo> Payments { get; set; }

        public int ClientId { get; set; }

        public string ClientName { get; set; }
    }
}