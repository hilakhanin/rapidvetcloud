﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RapidVet.WebModels.Clients.Details;

namespace RapidVet.WebModels.Clients
{
    public class ClientCreateModel
    {
        public int Id { get; set; }

        public int GenderId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.Client), Name = "FirstName")]
        public string FirstName { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.Client), Name = "LastName")]
        public string LastName { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Client), Name = "ExternalVet")]
        public string ExternalVet { get; set; }

        [UIHint("Title")]
        public int? TitleId { get; set; }

        [Display(ResourceType = typeof(Resources.Client), Name = "BirthDate")]
        [DataType(DataType.Date)]
        public DateTime? BirthDate { get; set; }

        [Display(ResourceType = typeof(Resources.Client), Name = "CreatedDate")]
        [DataType(DataType.Date)]
        public DateTime? CreatedDate { get; set; }


        [Display(ResourceType = typeof(Resources.Client), Name = "WorkePlace")]
        public string WorkePlace { get; set; }


        [Display(ResourceType = typeof(Resources.Client), Name = "Practice")]
        public string Practice { get; set; }


        [Display(ResourceType = typeof(Resources.Client), Name = "ReferredBy")]
        public string ReferredBy { get; set; }


        private ICollection<AddressModel> _addresses;

        [Display(ResourceType = typeof(Resources.Client), Name = "Addresses")]
        public ICollection<AddressModel> Addresses
        {
            get { return _addresses ?? (_addresses = new Collection<AddressModel>()); }
            set { _addresses = value; }
        }

        private ICollection<PhoneModel> _phones;

        [Display(ResourceType = typeof(Resources.Client), Name = "Phones")]
        public ICollection<PhoneModel> Phones
        {
            get { return _phones ?? (_phones = new Collection<PhoneModel>()); }
            set { _phones = value; }
        }


        private ICollection<EmailModel> _emails;

        [Display(ResourceType = typeof(Resources.Client), Name = "Emails")]
        public ICollection<EmailModel> Emails
        {
            get { return _emails ?? (Emails = new Collection<EmailModel>()); }
            set { _emails = value; }
        }


        [Display(ResourceType = typeof(Resources.Client), Name = "ApproveEmailTranfer")]
        public bool ApproveEmailTranfer { get; set; }


        [Display(ResourceType = typeof(Resources.Client), Name = "ApproveDisplayPhone")]
        public bool ApproveDisplayPhone { get; set; }

        public int ClinicId { get; set; }

        public decimal Age { get; set; }

     //   [Remote("IsIdCardExist", "Validation", AdditionalFields = "Id")]
        public string IdCard { get; set; }

        [UIHint("ClinicDoctors")]
        public int VetId { get; set; }

        [UIHint("Tariffs")]
        public int? TariffId { get; set; }

        [Display(ResourceType = typeof(Resources.Client), Name = "VisibleComment")]
        public string VisibleComment { get; set; }

        [Display(ResourceType = typeof(Resources.Client), Name = "HiddenComment")]
        public string HiddenComment { get; set; }

        [UIHint("ClientStatus")]
        [Required]
        public int? StatusId { get; set; }
        
        public ClientStatusModel Status { get; set; }

        public string ProfileImage { get; set; }

        public bool? isDirectedFromCalender { get; set; }

        //[UIHint("RegionalCouncils")]
        //public int? RegionalCounilId { get; set; }

        //[Display(ResourceType = typeof(Resources.Client), Name = "Active")]
        //public bool Active { get; set; }       
    }
}
