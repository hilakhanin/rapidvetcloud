﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clients
{
    public class AdvancedSearchWebModel
    {
        public int ClientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdCard { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public int CityId { get; set; }
        public string Street { get; set; }
        public string PatientName { get; set; }
        public string AllPatientNames { get; set; }
        public int AnimalKindId { get; set; }
        public string AnimalKind { get; set; }
        public string PatientLicense { get; set; }
        public string Chip { get; set; }
        public string Sagir { get; set; }
        public string ClientUrl { get; set; }
        public string PatientUrl { get; set; }
        public bool IncludeInactivePatients { get; set; }
        public int PatientId { get; set; }
        public string Address { get; set; }
        public bool ClientUrlVisible { get; set; }
        public string ClientStatus { get; set; }
        public List<string> Phones { get; set; }
        public bool SearchByContaining { get; set; }
    }
}
