﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clients
{
    public class TitleModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
