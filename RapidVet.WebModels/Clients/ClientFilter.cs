﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clients
{
    public class ClientFilter
    {
        //Global Filters
        public bool ShowOnlyClientInfo { get; set; }
        public bool OnlyNonFilteredClients { get; set; }
        public bool OnlyClientsWithoutVisits { get; set; } //RadioButton

        //Client Filters
        public string ClientStatusId { get; set; }
        public bool OnlyWithInactivePatients { get; set; }
        public int? OnlyWithNumPatientsMoreThan { get; set; }
        public string CityId { get; set; }
        public string EmailKeyWord { get; set; }
        public string VisitedFrom { get; set; }
        public string VisitedTo { get; set; }
        public string CreatedFrom { get; set; }
        public string CreatedTo { get; set; }
        public string AgeFrom { get; set; }
        public string AgeTo { get; set; }
        public int? TreatingDoctorId { get; set; }
        public string ExternalVetKeyWord { get; set; }
        public bool? OnlyWithOrWithoutFutureAppointment { get; set; }
        public bool? NoReference { get; set; }
        public string ReferredByKeyWord { get; set; }
        public string OnlyWithNoVisitFrom { get; set; }

        //Animal Filters
        public string AnimalKindId { get; set; }
        public int? PatientActiveStatus { get; set; } 
        public int? AnimalRaceId { get; set; }
        public string AnimalColorId { get; set; }
        public bool PureBred { get; set; }
        public int? AnimalGender { get; set; } 
        public bool? Sterilized { get; set; } 
        public bool? Fertilized { get; set; } 
        public string TreatedFrom { get; set; }
        public string TreatedTo { get; set; }
        public string OnlyWithNoTreatmentFrom { get; set; }
        public bool OnlyNotVaccined { get; set; }
        public bool OnlyWithElectornicNumber { get; set; }
        public bool? OnlyGoneThroughTreatment { get; set; }
        public int? TreatmentId { get; set; }
        public string PatientAgeFrom { get; set; }
        public string PatientAgeTo { get; set; }

        //Finance Filters
        public bool? OnlyWithPositiveBalance { get; set; } 
        public int? FromBalance { get; set; }
        public int? ToBalance { get; set; }
        public int? PriceListId { get; set; }
        public int? PaymentsNumMoreThan { get; set; }
        public string PaymentFrom { get; set; }
        public string PaymentTo { get; set; }

        //FieldsTo Show
        public bool idCardCb { get; set; }
        public bool workPhoneCb { get; set; }
        public bool homePhoneCb { get; set; }
        public bool cellPhoneCb { get; set; }
        public bool addressCb { get; set; }
        public bool treatingDocCb { get; set; }
        public bool emailCb { get; set; }
        public bool localIdCb { get; set; }
        public bool referedByCb { get; set; }
        public bool clientStatusCb { get; set; }
        public bool clientTitleCb { get; set; }
        public bool clientNameCb { get; set; }
        public bool clientCreateDateCb { get; set; }
        public bool clientDetailsUpdatedDateCb { get; set; }
        public bool lastTreatmentDateCb { get; set; }
        public bool clientAgeCb { get; set; }
        public bool balanceCb { get; set; }
        public bool patientNameCb { get; set; }
        public bool animalKindCb { get; set; }
        public bool animalRaceCb { get; set; }
        public bool animalGenderCb { get; set; }
        public bool patientCommentsCb { get; set; }
        public bool externalVetCb { get; set; }
        public bool animalColorCb { get; set; }
        public bool animalAgeCb { get; set; }
        public bool pureBredCb { get; set; }
        public bool sterilizedCb { get; set; }
        public bool electronicNumCb { get; set; }
        public bool SAGIRNumCb { get; set; }
        public bool licenseNumCb { get; set; }
        public bool patientWeightCb { get; set; }
        public bool owner_farmCb { get; set; }
    }
}
