﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Clients.Details;

namespace RapidVet.WebModels.Clients
{
    public class ClientDetailsModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }


        public string LastName { get; set; }

        public TitleModel Title { get; set; }

        public string Name
        {
            get
            {
                var result = string.Format("{0} {1}", FirstName, LastName);
                if (Title != null)
                {
                    result = string.Format("{0} {1}", Title.Name, result);
                }
                return result;
            }
        }

        public int? StatusId { get; set; }

        public ClientStatusModel Status { get; set; }

        public int LocalId { get; set; }

        public int ClinicId { get; set; }

        private ICollection<AddressModel> _addresses;
        public ICollection<AddressModel> Addresses
        {
            get { return _addresses ?? (_addresses = new Collection<AddressModel>()); }
            set { _addresses = value; }
        }

        private ICollection<PhoneModel> _phones;

        public ICollection<PhoneModel> Phones
        {
            get { return _phones ?? (_phones = new Collection<PhoneModel>()); }
            set { _phones = value; }
        }

        public decimal? Balance { get; set; }
        public string NextClientAppointment { get; set; }
        public string NextClientWatch { get; set; }
        public string IdCard { get; set; }
     //   public bool Active { get; set; }
    }
}