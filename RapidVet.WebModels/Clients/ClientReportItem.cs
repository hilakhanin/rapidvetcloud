﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clients
{
    public class ClientReportItem
    {
        public int clientId { get; set; }
        public string idCard { get; set; }
        public string workPhone { get; set; }
        public string homePhone { get; set; }
        public string cellPhone { get; set; }
        public string address { get; set; }
        public string zipcode { get; set; }
        public string treatingDoc { get; set; }
        public string email { get; set; }
        public int localId { get; set; }
        public string referedBy { get; set; }
        public string clientStatus { get; set; }
        public string clientTitle { get; set; }
        public string clientName { get; set; }
        public string clientFirstName { get; set; }
        public string clientLastName { get; set; }
        public DateTime? clientCreateDate { get; set; }
        public DateTime? clientDetailsUpdatedDate { get; set; }
        public DateTime? lastTreatmentDate { get; set; }
        public string clientAge { get; set; }
        public decimal? balance { get; set; }
        public string patientName { get; set; }
        public string animalKind { get; set; }
        public string animalRace { get; set; }
        public string animalGender { get; set; }
        public string patientComments { get; set; }
        public string externalVet { get; set; }
        public string animalColor { get; set; }
        public string animalAge { get; set; }
        public string pureBred { get; set; }
        public string sterilized { get; set; }
        public string electronicNum { get; set; }
        public string SAGIRNum { get; set; }
        public string licenseNum { get; set; }
        public string patientWeight { get; set; }
        public string owner_farm { get; set; }

        public string client_city { get; set; }        
        public string client_birthdate { get; set; }
        public string client_street_and_number { get; set; }

    }
}
