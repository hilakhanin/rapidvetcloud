﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clients
{
    public class InactiveClient
    {
        public int ClientId { get; set; }

        public string FullName { get; set; }

        public string Address { get; set; }

        public string HomeNumber { get; set; }

        public string CellularNumer { get; set; }

        public DateTime? LastVisitDate { get; set; }

        public string LastVisitDateString { get { return LastVisitDate != null ? LastVisitDate.Value.ToShortDateString() : null; } }
    }
}
