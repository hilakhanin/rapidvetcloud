﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clients.Details
{
    public class PhoneModel
    {
        public int Id { get; set; }

        public int ClientId { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Details), Name = "PhoneNumber")]
        public string PhoneNumber { get; set; }

        [UIHint("PhoneType")]
        public int PhoneTypeId { get; set; }

        public PhoneTypeModel PhoneType { get; set; }

        public string Comment { get; set; }
    }
}
