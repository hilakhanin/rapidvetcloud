﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clients.Details
{
    public class PhoneTypeModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
