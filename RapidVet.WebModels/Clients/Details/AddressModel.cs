﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clients.Details
{
    public class AddressModel
    {
        public int Id { get; set; }

        public int ClientId { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Details), Name = "Street")]
        public string Street { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Details), Name = "City")]
        [UIHint("Cities")]
        public int CityId { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Details), Name = "City")]
        public string CityName { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Details), Name = "ZipCode")]
        public string ZipCode { get; set; }
    }
}
