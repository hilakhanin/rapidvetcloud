﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using RapidVet.Resources;

namespace RapidVet.WebModels.Finance
{
    public class PriceListCategoryModel
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.PriceListCategory), Name = "Name")]
        public string Name { get; set; }

        public int ClinicId { get; set; }

        private ICollection<PriceListItemModel> _items;
        public ICollection<PriceListItemModel> Items
        {
            get { return _items ?? (_items = new Collection<PriceListItemModel>()); }
            set { _items = value; }
        }

        [Display(ResourceType = typeof(Resources.PriceListCategory), Name = "Available")]
        public bool Available { get; set; }


    }
}
