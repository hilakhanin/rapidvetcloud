﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
    public enum PriceShiftAction
    {
        SelectAction, Increase, Decrease
    }

    public enum PriceShiftMethod
    {
        Select, Nis, Precentage
    }

    public class PriceShiftModel
    {

        public int ActionId { get; set; }

        [UIHint("ActionDDL")]
        public PriceShiftAction PriceShiftAction
        {
            get { return (PriceShiftAction)ActionId; }
            set { ActionId = (int)value; }
        }


        public int MethodId { get; set; }

        [UIHint("MethodDDL")]
        public PriceShiftMethod PriceShiftMethod
        {
            get { return (PriceShiftMethod)MethodId; }
            set { MethodId = (int)value; }
        }


        public int TariffId { get; set; }

        public TariffModel Tariff { get; set; }

        [RegularExpression(@"(^N/A$)|(^[-]?(\d+)(\.\d{0,3})?$)|(^[-]?(\d{1,3},(\d{3},)*\d{3}(\.\d{1,3})?|\d{1,3}(\.\d{1,3})?)$)", ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "NumbersOnly")]
        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public decimal Amount { get; set; }

    }
}
