﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
    public class ItemTariffGridModel
    {
        public int TariffId { get; set; }

        public string TariffName { get; set; }

        public int ItemId { get; set; }

        [RegularExpression(@"(^N/A$)|(^[-]?(\d+)(\.\d{0,3})?$)|(^[-]?(\d{1,3},(\d{3},)*\d{3}(\.\d{1,3})?|\d{1,3}(\.\d{1,3})?)$)",
            ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "NumbersOnly")]
        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public decimal? Price { get; set; }
    }
}
