﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Reports
{
    public class DetectionWebModel
    {
        public int Id { get; set; }
        public string CreatedDate { get; set; }
        public string ClientName { get; set; }
        public string ClientStatus { get; set; }
        public int DocumentSerialNumber { get; set; }
        public decimal TotalToPay { get; set; }
        public string PrintUrl { get; set; }
        public int FinanceDocumentTypeId { get; set; }
        public string FinanceDocumentType { get; set; }
    }
}
