﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Reports
{
    public class DailyCashFlowModel
    {
        public String Date { get; set; }
        public decimal Outgoing { get; set; }
        public decimal Incoming { get; set; }
    }
}
