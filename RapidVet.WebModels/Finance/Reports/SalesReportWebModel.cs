﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Reports
{
   public class SalesReportWebModel
    {
       public int PriceListItemId { get; set; }

       public string ItemName { get; set; }

       public string Doctor { get; set; }

       public DateTime Created { get; set; }

       public decimal Quantity { get; set; }

       public decimal UnitPrice { get; set; }

       public string CategoryName { get; set; }

       public string ItemType { get; set; }

       public decimal Price { get; set; }

       public string ClientName { get; set; }

       public String CatalogNumber { get; set; }

       public int ClientId { get; set; }

       public string AddedBy { get; set; }
       public int AddedByUserId { get; set; }
       
    }
}
