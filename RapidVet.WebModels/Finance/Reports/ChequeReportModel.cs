﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Reports
{
    public class ChequeReportModel
    {
        public string DateStr { get; set; }
        public string ClientName { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public string AccountNumber { get; set; }
        public string ChequeNumber { get; set; }
        public decimal Amount { get; set; }
        public int FinanceDocumentSerial { get; set; }
    }
}
