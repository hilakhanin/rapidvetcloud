﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Reports
{
    public class IncomingsReportCsvWebModel
    {
        public string Created { get; set; }
        public string ClientName { get; set; }
        public int SerialNumber { get; set; }
        public string FinanceDocumentType { get; set; }
        public decimal CashPaymentSum { get; set; }
        public decimal BankTransferSum { get; set; }
        public decimal TotalCreditPaymentSum { get; set; }
        public decimal TotalChequesPayment { get; set; }
        public int Payments { get; set; }  
    }
}
