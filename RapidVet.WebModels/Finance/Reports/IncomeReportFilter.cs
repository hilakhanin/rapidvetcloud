﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Reports
{
   public class IncomeReportFilter
    {
       public string From { get; set; }
       public string To { get; set; }
       public int? IssuerId { get; set; }
       public int[] NotSelectedDocIds { get; set; }
       public int[] SelectedPaymentType { get; set; }


    }
}
