﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Reports
{
    public class PeriodTaxCalculationModel
    {
        //tax report
        public decimal TuroverNoVAT { get; set; }
        public decimal AdvancedPaymentPercent { get; set; }

        //vat report
        public decimal NoVATDeals { get; set; }
        public decimal WithVATDeals { get; set; }
        public decimal VATonDeals { get; set; }

        public decimal PermanentAssetsInputTax { get; set; }
        public decimal OtherInputTax { get; set; }
    }
}
