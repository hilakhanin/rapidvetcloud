﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Reports
{
    public class InvoiceReportWebModel
    {
        public string Date { get; set; }

        public string ClientName { get; set; }

        public decimal? Cash { get; set; }

        public decimal? Transfer { get; set; }

        public string CreditCardCompany { get; set; }

        public string CreditCardBusinessType { get; set; }

        public decimal? CreditAmount { get; set; }

        public string BankName { get; set; }

        public string BankBranchName { get; set; }

        public string BankAccountNumber { get; set; }

        public string ChequeNumber { get; set; }

        public decimal? ChequeAmount { get; set; }

        public decimal? TotalAmount { get; set; }

        public decimal VAT { get; set; }         

        public string FinanceDocumentType { get; set; }

        public int FinanceDocumentSerialNumber { get; set; }

        public int FinanceDocumentTypeId { get; set; }

        public decimal SumToPayForInvoice { get; set; }

        public int FinanceDocumentId { get; set; }

        public DateTime Created { get; set; }
    }
}
