﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Reports
{
  public  class AnnualBalanceWebModel
    {
      public string MonthName { get; set; }

      public decimal? Income { get; set; }

      public decimal? Expenses { get; set; }

      public decimal? PermanentAssets { get; set; }

      public decimal? Advances { get; set; }

      public decimal? VatToPay { get; set; }
    }
}
