﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Reports
{
    public class InvoiceReportPaymentWebModel
    {
        public int PaymentTypeId { get; set; }

        public string BankBranch { get; set; }

        public string BankAccount { get; set; }

        public string CreditCardCodeName { get; set; }

        public string IssuerCreditTypeName { get; set; }

        public string BankCodeName { get; set; }

        public string ChequeNumber { get; set; }

        public int InvoiceId { get; set; }

        public int ReciptId { get; set; }

        public int RefoundId { get; set; }
    }
}
