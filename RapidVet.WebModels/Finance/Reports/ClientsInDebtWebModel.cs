﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Reports
{
    public class ClientsInDebtWebModel
    {
        public int ClientId { get; set; }

        public string ClientName { get; set; }

        public decimal DebtAmount { get; set; }

        public DateTime? LastVisitDate { get; set; }

        public string LastVisitStr { get; set; }

        public string ClientUrl { get; set; }
    }
}
