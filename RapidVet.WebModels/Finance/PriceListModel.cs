﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
    public class PriceListModel
    {
        public ICollection<PriceListItemModel> Items  { get; set; }
        public ICollection<GridTariffModel> Tariffs { get; set; }

        public PriceListModel()
        {
            Items = new List<PriceListItemModel>();
            Tariffs = new List<GridTariffModel>();
        }
    }

    public class GridCategoryModel
    {
        public int ClinicId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<GridItemModel> Items { get; set; }

        public GridCategoryModel()
        {
            Items = new List<GridItemModel>();
        }
    }

    public class GridItemModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ItemTariffGridModel> Prices { get; set; }

        public GridItemModel()
        {
            Prices = new List<ItemTariffGridModel>();
        }
    }

    public class GridTariffModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
