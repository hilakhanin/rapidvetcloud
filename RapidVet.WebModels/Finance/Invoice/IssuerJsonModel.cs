﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Invoice
{
    public class IssuerJsonModel
    {
        public bool IsEasyCard { get; set; }
        public List<IssuerCreditTypeJsonModel> CreditTypes { get; set; }
        public List<EasyCardSettingsJsonModel> Terminals { get; set; }
    }
}
