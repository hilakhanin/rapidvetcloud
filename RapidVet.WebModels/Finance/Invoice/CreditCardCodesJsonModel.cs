﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Invoice
{
    public class CreditCardCodesJsonModel
    {

        public int Id { get; set; }
        public string Name { get; set; }        
        public string ExternalAccountId { get; set; }
        public int IssuerId { get; set; }
    }
}
