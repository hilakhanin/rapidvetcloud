﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Invoice
{
    public class FinanceDocumentItemWebmodel
    {
        public int? VisitId { get; set; }
        
        public DateTime? VisitDate { get; set; }
        
        public String Description { get; set; }
        public String CatalogNumber { get; set; }

        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }

        public decimal Discount { get; set; }

        public decimal DiscountPercentage { get; set; }
        
        public decimal Total
        {
            get { 
                var factor = 1 - DiscountPercentage/100;
                return UnitPrice*Quantity*factor - Discount;
            }
        }

        public int? PriceListItemId { get; set; }

        public string AddedBy { get; set; }
        public int AddedByUserId { get; set; }

        public int ItemExaminationID { get; set; }
        public int ItemTreatmentID { get; set; }
        public int ItemMedicationID { get; set; }
        public int ItemPreventiveMedicineID { get; set; }
    }
}
