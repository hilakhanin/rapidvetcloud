﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Invoice
{
    public class EasyCardSettingsJsonModel
    {
        public int Id { get; set; }
                
        public int IssuerId { get; set; }
        
        public String Name { get; set; }
                
        public String TerminalId { get; set; }

        public String Password { get; set; }

        public bool IsDefault { get; set; }                      
    }
}
