﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Invoice
{
    public class InvoiceClientDetails
    {
        public String Name { get; set; }
        public String Address { get; set; }
        public String Phone { get; set; }
        public int DefaultTariffId { get; set; }
        public decimal Balance { get; set; }
        public List<OpenInvoiceWebmodel> OpenInvoices { get; set; }
    }
}
