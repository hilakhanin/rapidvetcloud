﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Invoice
{
    public class FinanceDocumentFromQuotation
    {
        public List<FinanceDocumentItemWebmodel> QuotationItems { get; set; }

        public decimal GlobalDiscount { get; set; }

        public string AddedBy { get; set; }
        public int AddedByUserId { get; set; }
    }
}
