﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Invoice
{
    public class OpenInvoiceWebmodel
    {
        public int Id { get; set; }
        public int SerialNumber { get; set; }
        public string Sum { get; set; }
        public string Date { get; set; }
        public string SumToPay { get; set; }
    }
}
