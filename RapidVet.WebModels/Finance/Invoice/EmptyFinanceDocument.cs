﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RapidVet.WebModels.Finance.Invoice
{
    public class EmptyFinanceDocument
    {
        public List<SelectListItem> IssuerOptions { get; set; }

        public List<SelectListItem> DoctorOptions { get; set; }

        public List<SelectListItem> ClientOptions { get; set; }
        public int? SelectedClientId { get; set; }

        public String ClientName { get; set; }
        public String ClientAddress { get; set; }
        public String ClientPhone { get; set; }

        public List<SelectListItem> PaymentOptions { get; set; }

        public List<SelectListItem> BankCodeOptions { get; set; }

        public List<SelectListItem> CreditPaymentTypeOptions { get; set; }
        public List<SelectListItem> CreditCardCodeOptions { get; set; }

        public List<SelectListItem> TariffOptions { get; set; }

        public string AddedBy { get; set; }
        public int AddedByUserId { get; set; }
        public string DefaultDrId { get; set; }

    }
}
