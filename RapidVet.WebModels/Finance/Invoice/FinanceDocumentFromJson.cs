﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Invoice
{
    public class FinanceDateConvertor : DateTimeConverterBase
    {

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            DateTime date;
            var provider = new CultureInfo("he-IL");
            if (DateTime.TryParseExact(reader.Value.ToString(), "d", provider, DateTimeStyles.None, out date))
            {
                return date;
            }
            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
    public class FinanceDocumentFromJson
    {

        public List<jsonItem> items { get; set; }


        public int DocumentTypeId { get; set; }
        public int IssuerId { get; set; }
        public int DoctorId { get; set; }

        //client
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientAddress { get; set; }
        public string ClientPhone { get; set; }    

        //cash
        public bool CashPayment { get; set; }
        public decimal CashPaymentSum { get; set; }


        //transfer
        public List<BankTranfer> BankTransfers { get; set; }
        public bool BankTransfer { get; set; }
        public string BankTransferNumber { get; set; }
        public decimal BankTransferTotal { get; set; }

        //NoMham
        public bool NoMham { get; set; }


        //Cheque
        public List<Cheque> Cheques { get; set; }
        public bool ChequePayment { get; set; }
        public decimal TotalChequesPayment { get; set; }


        //Credit
        public bool CreditPayment { get; set; }
        public bool EasyCard { get; set; }
        public decimal EasyCardPaidSum { get; set; }
        public int CreditPaymentType { get; set; }
        public decimal? FirstCreditPaymentSum { get; set; }
        public int NumOfCreditPayments { get; set; }
        public decimal? FurtherCreditPaymentsSum { get; set; }
        public int CreditCardCodeId { get; set; }
        public decimal? TotalCreditPaymentSum { get; set; }
        public string CreditOkNumber { get; set; }
        public string EasyCardDealType { get; set; } //this is only if uses Easy card.return as string
        public string CreditDealNumber { get; set; }
        public string CreditValidDate { get; set; }
        public string EasyCardDealId { get; set; }
        public string EasyCardTerminalId { get; set; }

        //total payment
        public double TotalBeforeVAT { get; set; }
        public decimal RoundingFactor { get; set; }
        public double GlobalDiscount { get; set; }
        public bool GlobalDiscountIsPercent { get; set; }
        public double TotalAfterDiscount { get; set; }
        public double VAT { get; set; }
        public bool NoVAT { get; set; }
        public double TotalToPay { get; set; }
        public decimal TotalSumPaid { get; set; }

        public List<RelatedDocument> RelatedDocuments { get; set; }

        public string CardNumberLastFour { get; set; }

        public bool? ReciptPriningOnly { get; set; }
        public string Comments { get; set; }
    }

    public class BankTranfer
    {
        [JsonConverter(typeof(FinanceDateConvertor))]
        public DateTime? TransferDate { get; set; }
        public int? TransferBankCodeId { get; set; }
        public string TransferBranch { get; set; }
        public string BankTransferNumber { get; set; }
        public string TransferAccount { get; set; }
        public decimal BankTransferSum { get; set; }
        public decimal WitholdingTax { get; set; }
    }

    public class RelatedDocument
    {
        public int Id { get; set; }
    }

    public class jsonItem
    {
        public int? VisitId { get; set; }
        public string Description { get; set; }
        public string CatalogNumber { get; set; }
        public bool AllowUpdate { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal PriceWithVAT { get; set; }
        public decimal TotalBeforeVAT { get; set; }

        public bool UpdateItemInVisit { get; set; }

        public int? PriceListItemId { get; set; }
        public string AddedBy { get; set; }
        public int AddedByUserId { get; set; }

        //items Ids
        public int itemExaminationID { get; set; }
        public int itemMedicationID { get; set; }
        public int itemPreventiveMedicineID { get; set; }
        public int itemTreatmentID { get; set; }
    }

    public class Cheque
    {
        [JsonConverter(typeof(FinanceDateConvertor))]
        public DateTime ChequeDate { get; set; }
        public int BankCodeId { get; set; }
        public string Branch { get; set; }
        public string ChequeNumber { get; set; }
        public decimal Sum { get; set; }
        public string Account { get; set; }
    }
}
