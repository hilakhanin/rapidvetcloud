﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Invoice
{
    public class PriceListJsonModel
    {
        public List<PriceListItemSingleTariffFlatModel> Items { get; set; }

        public List<PriceListCategoryModel> Categories { get; set; }        
    }
}
