﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance.Invoice
{
    public class IssuerCreditTypeJsonModel
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public bool PostponedPayment { get; set; }
        public bool IgnorePayments { get; set; } 
        public string ExternalAccountId { get; set; }
    }
}
