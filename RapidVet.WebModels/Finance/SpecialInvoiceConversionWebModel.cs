﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
   public class SpecialInvoiceConversionWebModel
    {
       public int Converted { get; set; }

       public string Url { get; set; }

       public bool AllowedToViewFinancialReports { get; set; }
    }
}
