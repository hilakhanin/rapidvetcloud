﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;


namespace RapidVet.WebModels.Finance
{
    public class CategoryJsonModel
    {
        public int Id { get; set; }
        public int ClinicId { get; set; }
        public string Name { get; set; }
        private ICollection<ItemJsonModel> _items;
        public ICollection<ItemJsonModel> Items
        {
            get { return _items ?? (_items = new Collection<ItemJsonModel>()); }
            set { _items = value; }
        }
    }
}
