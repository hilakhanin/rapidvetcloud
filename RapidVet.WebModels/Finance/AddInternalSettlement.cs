﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
   public class AddInternalSettlementModel
    {
        public int ClientId { get; set; }

        public string Description { get; set; }

        public decimal Sum { get; set; }

    }
}
