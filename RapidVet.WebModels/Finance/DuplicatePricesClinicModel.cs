﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
    public class DuplicatePricesClinicModel
    {
        public int ClinicId { get; set; }

        public string ClinicName { get; set; }
    }
}
