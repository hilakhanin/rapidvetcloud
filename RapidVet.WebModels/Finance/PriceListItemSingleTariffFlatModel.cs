﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
    public class PriceListItemSingleTariffFlatModel
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public string Catalog { get; set; }

        public string Barcode { get; set; }

        public string ItemType { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }
        
        public bool NoVAT { get; set; }


        //public string AddedBy { get; set; }

        //public int AddedByUserId { get; set; }
    }
}
