﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
   public class SimpleCategoryModel
    {
       public int Id { get; set; }

       public int ClinicId { get; set; }

       public string Name { get; set; }
    }
}
