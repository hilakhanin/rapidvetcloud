﻿using System;
using RapidVet.Enums.Finances;

namespace RapidVet.WebModels.Finance
{
    public class CheckPaymentWebModel
    {
        public int PaymentTypeId { get; set; }
        public PaymentType PaymentType { get; set; }
        public int FinanceDocumentId { get; set; }
        public DateTime? DueDate { get; set; }
        public int? BankCodeId { get; set; }
        public String BankName { get; set; }
        public String BankBranch { get; set; }
        public String BankAccount { get; set; }
        public String ChequeNumber { get; set; }
        public String ClientName { get; set; }
        public decimal Sum { get; set; }
    }
}