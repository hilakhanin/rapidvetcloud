﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using RapidVet.Resources;

namespace RapidVet.WebModels.Finance
{

    public class PriceListItemModel
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [UIHint("PriceListItemType")]
        public int ItemTypeId { get; set; }

        public int CategoryId { get; set; }

        public int ClinicId { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.PriceListItem), Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.PriceListItem), Name = "Code")]
        [Remote("IsCatalogExist", "Validation", AdditionalFields = "Id")]
        public string Catalog { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.PriceList), Name = "Barcode")]
        public string Barcode { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.PriceListItem), Name = "FixedPrice")]
        public bool FixedPrice { get; set; }


        [Display(ResourceType = typeof(RapidVet.Resources.PriceListItem), Name = "IsRabiesVaccine")]
        public bool IsRabiesVaccine { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.PriceListItem), Name = "CopyFixedPrice")]
        public bool CopyFixedPrice { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.PriceListItem), Name = "Available")]
        public bool Available { get; set; }

        public ICollection<ItemTariffGridModel> ItemsTariffs { get; set; }


        //    public int PriceListItemTypeId { get; set; }
    }
}