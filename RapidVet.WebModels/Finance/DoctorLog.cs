﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Visits;

namespace RapidVet.WebModels.Finance
{
    public class DoctorLogModel
    {
        public DateTime Day { get; set; }
        public List<VisitFinanceModel> Visits { get; set; }

        public List<CheckPaymentWebModel> Checks { get; set; }

        public decimal TotalCash { get; set; }

        public decimal TotalChecks { get; set; }

        public int TotalChecksNumberToday { get; set; }

        public decimal TotalCreditCard { get; set; }

        public decimal Total { get; set; }

        public decimal TotalToCashToday { get; set; }

        public decimal TotalTransfers { get; set; }

        public decimal TotalDocuments { get; set; }

        public int IssuerRelatedDoctors { get; set; }
    }
}
