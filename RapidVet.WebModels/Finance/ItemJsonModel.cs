﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
    public class ItemJsonModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int ClinicId { get; set; }
        public string Name { get; set; }
        public Decimal Price { get; set; }
    }
}
