﻿using System;

namespace RapidVet.WebModels.Finance
{
    public class FinanceDocumentReport
    {
        public string DoctorName { get; set; }

        public DateTime CreatedDate { get; set; }
        public string TreatmentName { get; set; }
        public string ClientName { get; set; }
        public int SerialNumber { get; set; }

        public decimal TotalCreditPaymentSum { get; set; }
        public decimal TotalChequesPayment { get; set; }
        public decimal CashPaymentSum { get; set; }
        public decimal BankTransferSum { get; set; }

        public DateTime? CashedDate { get; set; }
    }
}