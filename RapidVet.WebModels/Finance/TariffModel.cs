﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
    public class TariffModel
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.Tariff), Name = "Name")]
        public string Name { get; set; }

        public int ClinicId { get; set; }

        public ICollection<ItemTariffGridModel> TariffsItems { get; set; }

        [Display(ResourceType = typeof(Resources.Tariff), Name = "Active")]
        public bool Active { get; set; }
    }
}
