﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
    public class DuplicatePricesTariffModel
    {

        public int SourceTariffId { get; set; }

        public TariffModel SourceTariff { get; set; }

        public int DestinationTariffId { get; set; }

        public TariffModel DestinationTariff { get; set; }
    }
}
