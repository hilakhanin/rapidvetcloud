﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
    public class FinancePaymentJson
    {
        public int id { get; set; }
        public string date { get; set; }
        public string clientName { get; set; }
        public string documentTypeId { get; set; }
        public string serialNumber { get; set; }
        public decimal amountIncludingTax { get; set; }
        public string bankCodeId { get; set; }
        public string branchNumber { get; set; }
        public string accountNumber { get; set; }
        public string checkNumber { get; set; }
        public string bankTransferNumber { get; set; }
        public string creditCardCompanyId { get; set; }
        public string creditCardPaymentTypeId { get; set; }
        public string lastFourDigits { get; set; }
        public string creditCardCoupon { get; set; }
        public bool isChecked { get; set; }

        public string bankName { get; set; }
        public string documentTypeName { get; set; }
        public string creditCardCompanyName { get; set; }
        public string creditCardPaymentTypeName { get; set; }
        public string PaymentType { get; set; }
        public int documentId { get; set; }

        public bool isDeposited { get; set; }
        public int depositNumber { get; set; }
        public string depositDate { get; set; }
    }
}
