﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RapidVet.WebModels.Finance
{
    public class FinanceDocumentReportModel
    {
        public List<SelectListItem> Doctors { get; set; }
        public List<SelectListItem> Issuers { get; set; }
        public List<FinanceDocumentReport> Reports{ get; set; }
        public int SelectedDateTypeIndex { get; set; }
    }
}
