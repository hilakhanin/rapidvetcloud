﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Finance
{
   public class PriceListItemTypeModel
    {
       public int Id { get; set; }

       public string Name { get; set; }
    }
}
