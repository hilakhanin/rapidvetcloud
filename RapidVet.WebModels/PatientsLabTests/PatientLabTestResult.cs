﻿namespace RapidVet.WebModels.PatientsLabTests
{
    public class PatientLabTestResultModel
    {
        public int Id { get; set; }
        public int LabTestId { get; set; }
        public int LabTestTemplateId { get; set; }
        public string Name { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }
        public string Result { get; set; }
        public bool enableEdit { get; set; }
        public string Indicator { get; set; }
    }
}