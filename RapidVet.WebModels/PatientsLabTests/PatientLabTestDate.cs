﻿namespace RapidVet.WebModels.PatientsLabTests
{
    public class PatientLabTestDate
    {
        public int Id { get; set; }
        public int LabTestTemplateId { get; set; }
        public string Date { get; set; }
    }
}