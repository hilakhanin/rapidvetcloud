﻿using RapidVet.WebModels.LabTests;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RapidVet.WebModels.PatientsLabTests
{
    public class PatientLabTestIndexModel    
    {
        public bool enableEdit { get; set; }
        public int LabTestTypeId { get; set; }

        public string Name { get; set; }

        private ICollection<LabTestTemplatesWebModel> _labTestTemplates;
        public ICollection<LabTestTemplatesWebModel> LabTestTemplates
        {
            get { return _labTestTemplates ?? (_labTestTemplates = new Collection<LabTestTemplatesWebModel>()); }
            set { _labTestTemplates = value; }
        }
        
    }
}
