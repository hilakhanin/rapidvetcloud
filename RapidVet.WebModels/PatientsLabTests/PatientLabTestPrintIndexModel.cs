﻿using RapidVet.WebModels.Clients;
using RapidVet.WebModels.LabTests;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RapidVet.WebModels.PatientsLabTests
{
    public class PatientLabTestPrintIndexModel    
    {
        public ClientDetailsModel ClientDetailsModel { get; set; }
        public List<PatientLabTestGroupResult> PatientLabTestGroupResult { get; set; }
        public PatientLabTestData PatientLabTestData { get; set; }
        public bool isPrintingComparativeTestData { get; set; }
        public int PatientId { get; set; }
    }
}
