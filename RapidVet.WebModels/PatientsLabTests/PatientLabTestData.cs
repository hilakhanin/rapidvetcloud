﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RapidVet.WebModels.PatientsLabTests
{
    public class PatientLabTestData
    {
        public string Name { get; set; }
        public string Created { get; set; }
        private ICollection<PatientLabTestResultModel> _results;
        public ICollection<PatientLabTestResultModel> Results
        {
            get { return _results ?? (_results = new Collection<PatientLabTestResultModel>()); }
            set { _results = value; }
        }
    }
}