﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
namespace RapidVet.WebModels.PatientsLabTests
{
    public class PatientLabTestGroupResult
    {
        public int Id { get; set; }
        public int LabTestId { get; set; }
        public string LabTestName { get; set; }
        public string Name { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }

        private ICollection<GroupTestResults> _results;
        public ICollection<GroupTestResults> Results
        {
            get { return _results ?? (_results = new Collection<GroupTestResults>()); }
            set { _results = value; }
        }       
    }

    public class GroupTestResults
    {
        public string Created { get; set; }
        public int LabTestId { get; set; }
        public string Result { get; set; }
        public string Indicator { get; set; }
    }
}