﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RapidVet.WebModels.Expenses
{
    public class SupplierWebModel
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public string Name { get; set; }

        public string ExternalId { get; set; }

        public string CompanyId { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string ContactNameA { get; set; }

        [RegularExpression("^[0-9-\\*]*$", ErrorMessage = "יש להזין ספרות ומקפים בלבד")]
        public string ContactPhoneA { get; set; }

        public string ContactCommentA { get; set; }

        public string ContactNameB { get; set; }

        [RegularExpression("^[0-9-\\*]*$", ErrorMessage = "יש להזין ספרות ומקפים בלבד")]
        public string ContactPhoneB { get; set; }

        public string ContactCommentB { get; set; }

    }
}
