﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Expenses
{
    public class ExpenseIndexWebModel
    {
        public int Id { get; set; }

        public int ExpenseGroupId { get; set; }

        public string ExpenseGroup { get; set; }

        public int SupplierId { get; set; }

        public string Supplier { get; set; }

        public string InvoiceDate { get; set; }

        public string EnteredDate { get; set; }

        public string Description { get; set; }

        public string InvoiceNumber { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal Vat { get; set; }

        public string PaymentDate { get; set; }

        public decimal RecognitionPercent { get; set; }

        public string IssuerName { get; set; }
    }
}
