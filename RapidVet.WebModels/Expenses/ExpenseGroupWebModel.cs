﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Expenses
{
    public class ExpenseGroupWebModel
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public string Name { get; set; }

        public bool IsTextDeductible { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public decimal RecognitionPercent { get; set; }

        public string ExternalId { get; set; }

    }
}
