﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Expenses
{
    public class ExpenseReportModel
    {
        public string Title { get; set; }

        private ICollection<ExpenseIndexWebModel> _expenses;
        public ICollection<ExpenseIndexWebModel> Expenses
        {
            get { return _expenses ?? (_expenses = new Collection<ExpenseIndexWebModel>()); }
            set { _expenses = value; }
        }
    }
}
