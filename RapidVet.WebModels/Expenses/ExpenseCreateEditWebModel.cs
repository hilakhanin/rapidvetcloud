﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Expenses
{
    public class ExpenseCreateEditWebModel
    {
        public int Id { get; set; }

        public int ExpenseGroupId { get; set; }
        public int SupplierId { get; set; }
        public int IssuerId { get; set; }

        public string InvoiceDate { get; set; }
        public string EnteredDate { get; set; }
        public string Description { get; set; }

        public string InvoiceNumber { get; set; }
        public string ReciptNumber { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal RecognitionPercent { get; set; }
        public decimal RecognitionTotalAmountIncludingVat { get; set; }
        public decimal Vat { get; set; }

        public string PaymentDate { get; set; }
        public int? PaymentTypeId { get; set; }
        public string Comments { get; set; }
    }
}
