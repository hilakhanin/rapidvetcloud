﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Google
{
    public class GoogleSyncHistoryModel
    {
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime From { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime To { get; set; }
        public bool HasChangesOnly { get; set; }
        public bool WithErrorsOnly { get; set; }
        public List<GoogleReportModel> Reports { get; set; }
        public string DoctorName { get; set; }

        public string GoogleSettingId { get; set; }
    }
}
