﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Google
{
    public class GoogleSyncSet
    {
        public int Id { get; set; }

        public int ClinicGroupID { get; set; }

        public int GoogleTokenId { get; set; }

        public int ClinicID { get; set; }

        public int DoctorID { get; set; }

        public string GoogleCalName { get; set; }

        public bool AllowSync { get; set; }

        public bool AllowSyncCalendar { get; set; }

        public int SyncCalBackDays { get; set; }

        public int SyncCalForwardDays { get; set; }

        public bool SyncCalFromGoogle { get; set; }

        public bool SyncCalToGoogle { get; set; }

        public int SyncPeriod { get; set; }

        public bool AllowSyncContacts { get; set; }

        public string DoctorName { get; set; }

        public string ClinicName { get; set; }

        public string AccountEmail { get; set; }

        public bool HasGoogleCalendarID { get; set; }
    }
}
