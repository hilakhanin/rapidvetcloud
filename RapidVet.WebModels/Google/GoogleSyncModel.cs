﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Google
{
    public class GoogleSyncModel
    {
        public List<GoogleSyncSet> GSettings { get; set; }

        public int MaxSyncAllowedForClinigGroup { get; set; }
    }
}
