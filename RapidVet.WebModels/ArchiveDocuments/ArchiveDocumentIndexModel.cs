﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums;

namespace RapidVet.WebModels.ArchiveDocuments
{
    public class ArchiveDocumentIndexModel
    {
        public int Id { get; set; }

        public string TypeName { get; set; }

        public string Title { get; set; }

        public string Time { get; set; }

        public string Date { get; set; }

        public string Comments { get; set; }

        public int FileTypeId { get; set; }

        public ArchivesFileType FileType
        {
            get { return (ArchivesFileType)FileTypeId; }
            set { FileTypeId = (int)value; }
        }

        public string EditUrl { get; set; }

        public string ViewUrl { get; set; }

    }
}
