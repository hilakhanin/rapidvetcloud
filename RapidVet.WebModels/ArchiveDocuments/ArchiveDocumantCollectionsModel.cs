﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.ArchiveDocuments
{
    public class ArchiveDocumantCollectionsModel
    {
        private ICollection<ArchiveDocumentIndexModel> _images;
        public ICollection<ArchiveDocumentIndexModel> Images
        {
            get { return _images ?? (_images = new Collection<ArchiveDocumentIndexModel>()); }
            set { _images = value; }
        }

        private ICollection<ArchiveDocumentIndexModel> _videos;
        public ICollection<ArchiveDocumentIndexModel> Videos
        {
            get { return _videos ?? (_videos = new Collection<ArchiveDocumentIndexModel>()); }
            set { _videos = value; }
        }

        private ICollection<ArchiveDocumentIndexModel> _other;
        public ICollection<ArchiveDocumentIndexModel> Other
        {
            get { return _other ?? (_other = new Collection<ArchiveDocumentIndexModel>()); }
            set { _other = value; }
        }

    }
}
