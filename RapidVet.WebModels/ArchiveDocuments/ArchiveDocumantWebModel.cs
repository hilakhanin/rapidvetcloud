﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Resources;

namespace RapidVet.WebModels.ArchiveDocuments
{
    public class ArchiveDocumantWebModel
    {
        public int Id { get; set; }

        public int ClinicId { get; set; }

        public Clinic Clinic { get; set; }

        public int PatientId { get; set; }

        public Patient Patient { get; set; }

        [UIHint("_ArchivesFileType")]
        public int FileTypeId { get; set; }

        public ArchivesFileType FileType
        {
            get { return (ArchivesFileType)FileTypeId; }
            set { FileTypeId = (int)value; }
        }

        public string Title { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.ArchiveDocumant), Name = "KeyWord")]
        public string KeyWord1 { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.ArchiveDocumant), Name = "KeyWord")]
        public string KeyWord2 { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.ArchiveDocumant), Name = "KeyWord")]
        public string KeyWord3 { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.ArchiveDocumant), Name = "KeyWord")]
        public string KeyWord4 { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.ArchiveDocumant), Name = "Comments")]
        public string Comments { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.ArchiveDocumant), Name = "ShowInPatientHistory")]
        public bool ShowInPatientHistory { get; set; }

        public DateTime Created { get; set; }
    }
}
