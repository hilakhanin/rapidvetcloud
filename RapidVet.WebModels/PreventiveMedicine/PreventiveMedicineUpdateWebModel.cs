﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.PreventiveMedicine
{
    public class PreventiveMedicineUpdateWebModel
    {
        //general data
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int PriceListItemId { get; set; }
        public decimal Price { get; set; }
        public int? DoctorId { get; set; }
        public bool ExternallyPerformed { get; set; }
        public string ExternalDoctorName { get; set; }
        public string ExternalDoctorLicense { get; set; }

        //update preventive treatment
        public bool UpdateDate { get; set; }
        public DateTime? Date { get; set; }
        public bool UpdateScheduledPreventiveItems { get; set; }
        public bool BillAccordingToTariff { get; set; }
        public string BatchNumber { get; set; }

        //update future preventive treatment
        public bool UpdateNextDate { get; set; }
        public DateTime? NextDate { get; set; }
    }
}
