﻿using System;
namespace RapidVet.WebModels.PreventiveMedicine
{
    public class PreventiveReminderCsvModel
    {
        public string ScheduledDateStr { get; set; }

        public string ClientName { get; set; }

        public string PatientName { get; set; }

        public string PatientKind { get; set; }

        public string PatientGender { get; set; }

        public string PatientColor { get; set; }

        public decimal? PatientAge { get; /*{ return PatientAge.HasValue ? decimal.Round(PatientAge.Value, 2, MidpointRounding.AwayFromZero) : PatientAge; }*/ set; /*{ PatientAge = value; }*/ }

        public string PatientStatus { get; set; }

        public string PriceListItemName { get; set; }

        public string ClientAddress { get; set; }

        public string ClientHomePhone { get; set; }

        public string ClientCellPhone { get; set; }

        public string ClientEmail { get; set; }

        public int ReminderCount { get; set; }

        public string LastReminderDateStr { get; set; }

        public string NextAppointmentDateStr { get; set; }

        public string Comments { get; set; }
    }
}