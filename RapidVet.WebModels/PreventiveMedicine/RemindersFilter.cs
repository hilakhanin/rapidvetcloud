﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.PreventiveMedicine
{
    public class RemindersFilter
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string FromReminderDate { get; set; }
        public string ToReminderDate { get; set; }
        public int? RegionalCouncilId { get; set; }
        public int? AnimalKindId { get; set; }
        public bool InactiveClients { get; set; }
        public bool InactivePatients { get; set; }
        public bool FutureOnly { get; set; }
        public string ItemIds { get; set; }
        public string CityIds { get; set; }
        public string SelectedRemindersIds { get; set; }
        public bool RemoveClientsWithFutureCalendarEntries { get; set; }
        public int? DoctorId { get; set; }
        public string CurrentSortHeader { get; set; }
        public string CurrentSortState { get; set; }
        public int CurrentPage { get; set; }

        public override string ToString()
        {
            return
                string.Format(
                    "fromDate={0}&toDate={1}&regionalCouncilId={2}&AnimalKindId={3}&InactiveClients={4}&InactivePatients={5}&FutureOnly={6}&ItemIds={7}&FromReminderDate={8}&ToReminderDate={9}&SelectedRemindersIds={10}&RemoveClientsWithFutureCalendarEntries={11}&CityIds={12}&DoctorId={13}&CurrentSortHeader={14}&CurrentSortState={15}",
                    FromDate, ToDate, RegionalCouncilId, AnimalKindId, InactiveClients, InactivePatients, FutureOnly, ItemIds,FromReminderDate,ToReminderDate, SelectedRemindersIds,RemoveClientsWithFutureCalendarEntries, CityIds, DoctorId, CurrentSortHeader, CurrentSortHeader);
        }
    }
}
