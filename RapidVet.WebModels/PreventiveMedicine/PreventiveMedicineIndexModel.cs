﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.PreventiveMedicine
{
    public class PreventiveMedicineIndexModel
    {
        private List<PreventiveMedicineItemWebModel> _vaccines;
        public List<PreventiveMedicineItemWebModel> Vaccines
        {
            get { return _vaccines ?? (_vaccines = new List<PreventiveMedicineItemWebModel>()); }
            set { _vaccines = value; }
        }

        private List<PreventiveMedicineItemWebModel> _treatments;
        public List<PreventiveMedicineItemWebModel> Treatments
        {
            get { return _treatments ?? (_treatments = new List<PreventiveMedicineItemWebModel>()); }
            set { _treatments = value; }
        }

        public string ElectronicNumber { get; set; }

        public string LicenseNumber { get; set; }

        public bool ClientPaidOrReturnedTollVaucher { get; set; }
    }
}
