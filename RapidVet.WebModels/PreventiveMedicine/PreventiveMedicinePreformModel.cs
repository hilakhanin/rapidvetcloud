﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.PreventiveMedicine
{
    public class PreventiveMedicinePreformModel
    {
        public int PatientId { get; set; }

        public int PriceListItemId { get; set; }

        public int PriceListItemTypeId { get; set; }

        public string Comment { get; set; }

        public decimal Price { get; set; }

        public bool ExternallyPreformed { get; set; }

        public bool PrintDogOwnerLicense { get; set; }

        public bool PrintRabiesVaccineSlip { get; set; }

        public int? PreformingDoctor { get; set; }

        public string ExternalDrName { get; set; }

        public string ExternalDrLicenseNumber { get; set; }

        public string BatchNumber { get; set; }

        public DateTime PerformedOn { get; set; }

        public string ElectronicNumber { get; set; }

        public string LicenseNumber { get; set; }

        public bool ClientPaidOrReturnedTollVaucher { get; set; }

        public bool UpdateScheduledPreventiveItems { get; set; }

        public bool AddToVisit { get; set; }
    }
}
