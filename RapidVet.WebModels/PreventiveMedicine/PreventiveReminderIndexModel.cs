﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.PreventiveMedicine
{
    public class PreventiveReminderIndexModel
    {
        //client details
        public int ClientId { get; set; }

        public string ClientName { get; set; }

        public string ClientStatus { get; set; }

        public string ClientAddress { get; set; }

        public string ClientHomePhone { get; set; }

        public string ClientCellPhone { get; set; }

        public string ClientEmail { get; set; }

        //patient details
        public int PatientId { get; set; }
        public int PatientIdR { get; set; }

        public string PatientName { get; set; }

        public string PatientKind { get; set; }

        public string PatientGender { get; set; }

        public string PatientColor { get; set; }

        public decimal? PatientAge { get; set; }

        public string PatientStatus { get; set; }

        //price list item details
        public string PriceListItemName { get; set; }

        public int PriceListItemId { get; set; }

        public string Comments { get; set; }

        //preventive medicine item details
        public int PreventiveMedicineItemId { get; set; }


        //reminders details
        public int ReminderCount { get; set; }

        public DateTime? ScheduledDateStr { get; set; }

        public DateTime? LastReminderDateStr { get; set; }

        public DateTime? NextAppointmentDateStr { get; set; }

        public bool IsPast { get; set; }

        public DateTime? Scheduled { get; set; }

        public bool Selected { get; set; }

        public int PmiID { get; set; }
    }
}
