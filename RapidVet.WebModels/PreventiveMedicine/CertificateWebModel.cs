﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.PreventiveMedicine
{
    public class CertificateWebModel
    {
        public string ClientFullName { get; set; }

        public string ClientAddress { get; set; }

        public string PatientName { get; set; }

        public string PatientChipNumber { get; set; }

        public string PatientAnimalKindName { get; set; }

        public string PatientRaceName { get; set; }

        public string PatientSex { get; set; }

        public string FertilizationSterilization { get; set; }

        public string PatientBirthDate { get; set; }

        public PreventiveMedicineIndexModel Items { get; set; }


    }
}
