﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.PreventiveMedicine
{
    public class PreventiveMedicineItemWebModel
    {
        public int Id { get; set; }

        public int PriceListItemId { get; set; }

        public int PriceListItemTypeId { get; set; }

        public decimal Price { get; set; }

        public string Name { get; set; }

        public DateTime? Preformed { get; set; }

        public string PreformedStr { get; set; }

        public DateTime? Next { get; set; }

        public string NextStr { get; set; }

        public bool IsNextFuture { get; set; }

        public bool IsToday { get; set; }

        public bool IsRabiesVaccine { get; set; }

        public string ExternalDrName { get; set; }

        public string ExternalDrLicenseNumber { get; set; }

        public string BatchNumber { get; set; }

        public bool ClientPaidOrReturnedTollVaucher { get; set; }
        public decimal TollPrice { get; set; }
        public DateTime? TollPaymentDate { get; set; }
       [UIHint("TollPaymentMethod")]
        public int TollPaymentMethodId { get; set; }

       public string Comment { get; set; }
    }
}
