﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Referrals
{
    public class ReferralEditWebModel
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        [Display(ResourceType = typeof(Resources.Controllers.Referrals), Name = "RefferalDate")]
        public DateTime DateTime { get; set; }

        [Display(ResourceType = typeof(Resources.Controllers.Referrals), Name = "RefferingReason")]
        public string Reason { get; set; }

        [UIHint("ClinicDoctors")]
        public int RefferingUserId { get; set; }

        [UIHint("ClinicDoctors")]
        public int? RefferedToUserId { get; set; }

        [Display(ResourceType = typeof(Resources.Controllers.Referrals), Name = "RefferedToDoctor")]
        public string RefferedToExternal { get; set; }

        [Display(ResourceType = typeof(Resources.Controllers.Referrals), Name = "RefferedClicnicAddress")]
        public string RefferingClinicAddress { get; set; }

        //[RegularExpression("[0-9]*")]
        [Display(ResourceType = typeof(Resources.Controllers.Referrals), Name = "RefferedClinicPhone")]
        [RegularExpression(@"^[0][5][0]-\d{7}|[0][5][2]-\d{7}|[0][5][4]-\d{7}|[0][5][7]-\d{7}|[0][7][2]-\d{7}|[0][7][3]-\d{7}|[0][7][7]-\d{7}|[0][2]-\d{7}|[0][3]-\d{7}|[0][4]-\d{7}|[0][8]-\d{7}|[0][9]-\d{7}|[0][5][0]\d{7}|[0][5][2]\d{7}|[0][5][4]\d{7}|[0][5][7]\d{7}|[0][7][7]\d{7}|[0][2]\d{7}|[0][3]\d{7}|[0][4]\d{7}|[0][8]\d{7}|[0][9]\d{7}$", ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "InvalidPhoneWithTemplate")]
        public string RefferingClinicPhone { get; set; }

        [NotMapped]
        public bool IsForPrint { get; set; }
    }
}
