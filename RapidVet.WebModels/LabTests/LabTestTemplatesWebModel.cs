﻿using System.Collections.Generic;
using RapidVet.WebModels.PatientsLabTests;
using System.Collections.ObjectModel;

namespace RapidVet.WebModels.LabTests
{
    public class LabTestTemplatesWebModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int AnimalKindId { get; set; }

        public int LabTestTypeId { get; set; }        

        private ICollection<PatientLabTestDate> _dates;
        public ICollection<PatientLabTestDate> Dates
        {
            get { return _dates ?? (_dates = new Collection<PatientLabTestDate>()); }
            set { _dates = value; }
        }
    }
}
