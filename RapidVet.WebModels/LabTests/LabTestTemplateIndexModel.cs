﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RapidVet.WebModels.Patients;

namespace RapidVet.WebModels.LabTests
{
    public class LabTestTemplateIndexModel
    {
        public List<AnimalKindModel> AnimalKinds { get; set; }

        public List<SelectListItem> LabTestTypes { get; set; }

        public List<LabTestTemplateEdit> Templates { get; set; }

        public int AnimalKindId { get; set; }
    }
}
