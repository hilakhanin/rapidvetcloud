﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.LabTests
{
    public class LabTestTypeEdit
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ClinicId { get; set; }
    }
}
