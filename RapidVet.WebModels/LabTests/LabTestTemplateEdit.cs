﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Patients;

namespace RapidVet.WebModels.LabTests
{
    public class LabTestTemplateEdit
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int AnimalKindId { get; set; }

        public int LabTestTypeId { get; set; }

        public string LabTestTypeName { get; set; }

        public List<AnimalKindModel> AnimalKinds { get; set; }
    }
}
