﻿namespace RapidVet.WebModels.Quotations
{
    public class QuotationPriceListCategory
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}