﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Quotations
{
    public class QuotationPatientModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
