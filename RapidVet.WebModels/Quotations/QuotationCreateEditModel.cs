﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Quotations
{
    public class QuotationCreateEditModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ClientId { get; set; }

        private ICollection<QuotationPatientModel> _patients;
        public ICollection<QuotationPatientModel> Patients
        {
            get { return _patients ?? (_patients = new Collection<QuotationPatientModel>()); }
            set { _patients = value; }
        }

        public int PatientId { get; set; }

        public string Comments { get; set; }

        private ICollection<QuotationTreatments> _treatments;
        public ICollection<QuotationTreatments> Treatments
        {
            get { return _treatments ?? (_treatments = new Collection<QuotationTreatments>()); }
            set { _treatments = value; }
        }

        private ICollection<QuotationPriceListItems> _priceListItems;
        public ICollection<QuotationPriceListItems> PriceListItems
        {
            get { return _priceListItems ?? (_priceListItems = new Collection<QuotationPriceListItems>()); }
            set { _priceListItems = value; }
        }

        private ICollection<TreatmentPackageModel> _treatmentPackages;
        public ICollection<TreatmentPackageModel> TreatmentPackages
        {
            get { return _treatmentPackages ?? (_treatmentPackages = new Collection<TreatmentPackageModel>()); }
            set { _treatmentPackages = value; }
        }

        private ICollection<QuotationPriceListCategory> _categories;
        public ICollection<QuotationPriceListCategory> Categories
        {
            get { return _categories ?? (_categories = new Collection<QuotationPriceListCategory>()); }
            set { _categories = value; }
        }


    }
}
