﻿namespace RapidVet.WebModels.Quotations
{
    public class TreatmentPackageItemModel
    {
        public int Id { get; set; }
        public int PriceListItemId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Comments { get; set; }
    }
}