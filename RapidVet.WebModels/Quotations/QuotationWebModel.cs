﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Patients;

namespace RapidVet.WebModels.Quotations
{
    public class QuotationWebModel
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public int ClientId { get; set; }

        public List<PatientIndexListModel> Patients { get; set; }

        public int PatientId { get; set; }

        public PatientCreateModel Patient { get; set; }

        public List<PriceListItemSingleTariffFlatModel> Items { get; set; }

        public List<QuotationTreatmentModel> Treatments { get; set; }

        public List<TreatmentPackageModel> Packages { get; set; }

        public List<PriceListCategoryModel> Categories { get; set; }

        public string Comments { get; set; }

        //public decimal Discount { get; set; }

        //public decimal PercentDiscount { get; set; }

        public decimal TotalPrice { get; set; }

    }
}
