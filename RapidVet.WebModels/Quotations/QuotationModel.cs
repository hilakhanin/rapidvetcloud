﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Resources;

namespace RapidVet.WebModels.Quotations
{
    public class QuotationModel
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public string PatientName { get; set; }

        public string Name { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Updated { get; set; }
    }
}
