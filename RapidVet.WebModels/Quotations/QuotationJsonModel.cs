﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Quotations
{
    public class QuotationJsonModel
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public string PatientName { get; set; }

        public string Name { get; set; }

        public string Created { get; set; }

        public string Updated { get; set; }

        public string EditUrl { get; set; }

        public string ExecutionUrl { get; set; }

        public string FinanceUrl { get; set; }

        public string TreatmentsDescription { get; set; }
    }
}
