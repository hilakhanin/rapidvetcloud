﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Quotations
{
    public class QuotationPrefaceModel
    {
        public string QuotationPreface { get; set; }
    }
}
