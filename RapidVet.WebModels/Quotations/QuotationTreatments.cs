﻿namespace RapidVet.WebModels.Quotations
{
    public class QuotationTreatments
    {
        public int Id { get; set; }
        public int QuotationId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Comments { get; set; }
    }
}