﻿namespace RapidVet.WebModels.Quotations
{
    public class QuotationPriceListItems
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int Name { get; set; }
        public decimal Price { get; set; }
        public string Catalog { get; set; }
        public string Barcode { get; set; }
    }
}