﻿namespace RapidVet.WebModels.Quotations
{
    public class QuotationTreatmentModel
    {
        public int Id { get; set; }

        public int QuotationId { get; set; }

        public int PriceListItemId { get; set; }

        public string Catalog { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public decimal Amount { get; set; }

        public string Comments { get; set; }

        public string ItemType { get; set; }

        public int TreatmentPackageId { get; set; }

        public int TreatmentPackageItemId { get; set; }

        public decimal Discount { get; set; }

        public decimal PercentDiscount { get; set; }

        public decimal TotalAmount { get; set; }

        public bool WasTreatmentMovedToExecution { get; set; }

        public int? VisitId { get; set; }
    }
}