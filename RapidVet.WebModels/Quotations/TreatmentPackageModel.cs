﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RapidVet.WebModels.Quotations
{
    public class TreatmentPackageModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        private ICollection<TreatmentPackageItemModel> _items;

        public ICollection<TreatmentPackageItemModel> Items
        {
            get { return _items ?? (_items = new Collection<TreatmentPackageItemModel>()); }
            set { _items = value; }
        }
    }
}