﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

namespace RapidVet.WebModels.Quotations
{
    public class QuotationDiscountModel
    {
       // public int QuotationId { get; private set; }

        [Range(0, int.MaxValue, ErrorMessage = "הנחה שקלית חייבת להיות חיובית")]
        [Display(ResourceType = typeof(Resources.Quotations), Name = "SumDiscount")]
        public int SumDiscount { get; set; }

        [Range(0, 100, ErrorMessage = "אנא הזן מספר חיובי בין 0 ל100")]
        [Display(ResourceType = typeof(Resources.Quotations), Name = "PercentDiscount")]
        public int PercentDiscount { get; set; }

        //public QuotationDiscountModel(int quotationId)
        //{
        //    QuotationId = quotationId;
        //}     
    }
}
