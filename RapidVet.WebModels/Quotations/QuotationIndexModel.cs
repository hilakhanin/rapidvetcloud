﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Clients;

namespace RapidVet.WebModels.Quotations
{
    public class QuotationIndexModel
    {
        private ICollection<QuotationModel> _quotations;
        public ICollection<QuotationModel> Quotations
        {
            get { return _quotations ?? (_quotations = new Collection<QuotationModel>()); }
            set { _quotations = value; }
        }

        public ClientDetailsModel Client { get; set; }
    }
}
