﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.ClinicTasks
{
    public class ClinicTaskWebModel
    {
        public int Id { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public String StartDate { get; set; }

        public String TargetDate { get; set; }
        
        public int Status { get; set; }

        public int? DesignatedUserId { get; set; }

        public int UrgencyId { get; set; }
   
    }
}
