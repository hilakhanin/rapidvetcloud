﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Doctors
{
    public class CalendarModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Show { get; set; }
    }
}
