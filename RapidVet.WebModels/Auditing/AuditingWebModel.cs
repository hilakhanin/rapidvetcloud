﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Auditing
{
    public class AuditingWebModel
    {
        public int RecordID { get; set; }

        public int ClinicId { get; set; }

        public int UserID { get; set; }

        public string LogType { get; set; }

        public string LogDescription { get; set; }

        public string OldContent { get; set; }

        public string DateStamp { get; set; }

        public string UserName { get; set; }

        public string ClinicGroupName { get; set; }

        public string ClinicName { get; set; }

        public int LogTypeID { get; set; }
    }
}
