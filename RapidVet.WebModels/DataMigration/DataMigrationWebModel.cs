﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Resources.Enums;
using MigrationOrder = RapidVet.Enums.DataMigration.MigrationOrder;

namespace RapidVet.WebModels.DataMigration
{
    public class DataMigrationWebModel
    {
        public int StageId { get; set; }

        public MigrationOrder Stage { get; set; }

        public int StageStatusId { get; set; }

        public MigrationStageStatus MigrationStageStatus { get; set; }


    }
}
