﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.DataMigration
{
    public class ArchiveMigrationMapping
    {
        public string Name { get; set; }
        public string Destination { get; set; }
    }
}
