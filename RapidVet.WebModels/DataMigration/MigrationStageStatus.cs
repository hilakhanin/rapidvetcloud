﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.DataMigration
{
   public class MigrationStageStatus
    {
        public int MigrationStage { get; set; }

        public string Status { get; set; }
    }
}
