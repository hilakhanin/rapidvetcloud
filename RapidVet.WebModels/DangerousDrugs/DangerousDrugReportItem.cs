﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.DangerousDrugs
{
    public class DangerousDrugReportItem
    {

        public int MedicationId { get; set; }

        public String Date { get; set; }

        public String MedicationName { get; set; }

        public String OwnerName { get; set; }

        public String PatientName { get; set; }

        public int PatientId { get; set; }

        public String Procedure { get; set; }

        public String DoctorName { get; set; }

        public decimal AmountInjected { get; set; }

        public decimal AmountDestroyed { get; set; }

    }
}
