﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Medications
{
   public class MedicationJsonModel
    {
        public int Id { get; set; }       
        public int ClinicGroupId { get; set; }        
        public string Name { get; set; }
        public int MedicationAdministrationTypeId { get; set; }
        public string Dosage { get; set; }
        public int? TimesPerDay { get; set; }
        public int? NumberOfDays { get; set; }
        public string Insturctions { get; set; }
        public decimal? UnitPrice { get; set; }
        public bool DangerousDrug { get; set; }
        public string DoctorInfo { get; set; }
        public bool Active { get; set; }       
        public DateTime? Updated { get; set; }

    }
}
