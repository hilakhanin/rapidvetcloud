﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Enums;

namespace RapidVet.WebModels.Medications
{
    public class MedicationEditWebModel
    {
        //MedicationId
        public int Id { get; set; }

        /// <summary>
        /// Medication Name (שם)
        /// </summary>
       [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Medication), Name = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// Medication Administration Type ID
        /// </summary>
        [UIHint("MedicationAdministrationType")]
        [Display(ResourceType = typeof(RapidVet.Resources.Medication), Name = "MedicationAdministrationTypeId")]
        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public int MedicationAdministrationTypeId { get; set; }

        /// <summary>
        /// The Dosage of the medication (מנה)
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.Medication), Name = "Dosage")]
        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public string Dosage { get; set; }

        /// <summary>
        /// Times of usage every day (מספר פעמים ביום)
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.Medication), Name = "TimesPerDay")]
        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public int TimesPerDay { get; set; }

        /// <summary>
        /// Numbers of dayes (זמן לקיחה בימים)
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.Medication), Name = "NumberOfDays")]
        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public int NumberOfDays { get; set; }

        /// <summary>
        /// usage instructions (הוראות שימוש)
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.Medication), Name = "Insturctions")]
        public string Insturctions { get; set; }

        /// <summary>
        /// Price per Unit (מחיר יחידה)
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.Medication), Name = "UnitPrice")]
        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Is this a dangerous drug (סם מסוכן)
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.Medication), Name = "DangerousDrug")]
        public bool DangerousDrug { get; set; }

        /// <summary>
        /// Doctor Information (מידע לרופא)
        /// </summary>
        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(RapidVet.Resources.Medication), Name = "DoctorInfo")]
        public string DoctorInfo { get; set; }

    }
}
