﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.GeneralReports
{
    public class PreformedPreventiveReportFilterModel
    {
        public string From { get; set; }

        public string To { get; set; }

        public int? AnimalKindId { get; set; }

        public bool IncludeInactivePatients { get; set; }

        public bool IncludeExternalPreformances { get; set; }

        public string SelectedItems { get; set; }

    }
}
