﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.GeneralReports
{
    public class PreformedPreventiveReportModel
    {
        public string Date { get; set; }
        public string ClientName { get; set; }
        public string PatientName { get; set; }
        public string PatientKind { get; set; }
        public string PatientGender { get; set; }
        public string PatientColor { get; set; }
        public decimal PatientAge { get; set; }
        public string PriceListItemName { get; set; }
        public string Address { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string ClientIdCard { get; set; }
        public string PatientChip { get; set; }
        public string PreformingDoctor { get; set; }
    }
}
