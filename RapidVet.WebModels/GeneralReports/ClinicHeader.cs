﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.GeneralReports
{
    public class ClinicHeader
    {
        public String Name { get; set; }
        public String Address { get; set; }
        public String Phone { get; set; }
        public String Fax { get; set; }
        public String Email { get; set; }
    }
}
