﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RapidVet.WebModels.GeneralReports
{
    public class RabiesReportWebModel
    {
        public String From { get; set; }
        public String To { get; set; }

        public String MeloonaCode { get; set; }

        public int? AnimalTypeId { get; set; }
        public List<SelectListItem> AnimalTypeOptions { get; set; }

        public List<SelectListItem> SterilizationOptions { get; set; }
        public int SterilizationOption { get; set; }

        public int? RegionalCouncilId { get; set; }
        public List<SelectListItem> RegionalCouncilOptions { get; set; }

        public int? CityId { get; set; }
        public List<SelectListItem> CityOptions { get; set; }

        public List<SelectListItem> PostOptions { get; set; }

    }
}
