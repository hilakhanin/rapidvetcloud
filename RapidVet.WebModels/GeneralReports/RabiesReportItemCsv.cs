﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.GeneralReports
{
    public class RabiesReportItemCsv
    {
        public String VaccinationDate { get; set; }
        public String OwnerIdCard { get; set; }
        public String OwnerLastName { get; set; }
        public String OwnerFirstName { get; set; }
      
        public String Address { get; set; }
        public String City { get; set; }
        public String HomePhone { get; set; }
        public String CellPhone { get; set; }
        public String Email { get; set; }
        public String DisplayPhone { get; set; }

        public String AnimalName { get; set; }
        public String AnimalRace { get; set; }
        public String Gender { get; set; }
        public String Birthdate { get; set; }
        public String Color { get; set; }
        public String AnimalKind { get; set; }
        public String ElectronicNumber { get; set; }
        public String LicenceNumber { get; set; }
        public String BatchNumber { get; set; }
        public String Sterilization { get; set; }
      


    }
}
