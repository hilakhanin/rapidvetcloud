﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.GeneralReports
{
    public class RabiesReportPrintModel
    {
        public List<List<RabiesReportItem>> ReportItemsByRegionalCouncils { get; set; }
        public ClinicHeader ClinicHeader { get; set; }
        public String From { get; set; }
        public String To { get; set; }
    }
}
