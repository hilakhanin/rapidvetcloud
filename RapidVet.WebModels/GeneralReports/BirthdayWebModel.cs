﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.GeneralReports
{
    public class BirthdayWebModel
    {
        public String ClientName { get; set; }
        public String ClientFirstName { get; set; }
        public String ClientLastName { get; set; }
        public String ClientBirthdate { get; set; }
        public String Email { get; set; }
        public String ClientReferer { get; set; }
        public int ClientStatusId { get; set; }
        public String Status { get; set; }
        public String DoctorName { get; set; }
        public int ClientId { get; set; }
        public decimal ClientBalance { get; set; }
        public String ClientAge { get; set; }
        public String ClientTitle { get; set; }
        public String ClientIdCardNumber { get; set; }
        public int ClientRecordNumber { get; set; }
        
        public String City { get; set; }
        public String ClientStreetAndNumber { get; set; }
        public String Address { get; set; }
        public String Zipcode { get; set; }

        public String Phone { get; set; }
        public String WorkPhone { get; set; }
        public String CellPhone { get; set; }

        public String PatientName { get; set; }
        public String PatientAnimalKind { get; set; }
        public String PatientRace { get; set; }
        public String PatientColor { get; set; }
        public String PatientAge { get; set; }
        public String PatientSex { get; set; }
        public String BirthDate { get; set; }

    }
}
