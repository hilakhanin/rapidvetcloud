﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.GeneralReports
{
    public class RabiesReportItem
    {
        public String VaccinationDate { get; set; }
        public String OwnerIdCard { get; set; }
        public String OwnerFirstName { get; set; }
        public String OwnerLastName { get; set; }
        public String OwnerBirthDate { get; set; }
        public String Address { get; set; }
        public String RegionalCouncil { get; set; }
        public String City { get; set; }
        public int RegionalCouncilId { get; set; }
        public int CityId { get; set; }
        public String HomePhone { get; set; }
        public String CellPhone { get; set; }
        public String Email { get; set; }
        public String DisplayPhone { get; set; }

        public String AnimalName { get; set; }
        public String AnimalRace { get; set; }
        public String Gender { get; set; }
        public String Birthdate { get; set; }
        public String Color { get; set; }
        public String AnimalKind { get; set; }
        public String ElectronicNumber { get; set; }
        public String LicenceNumber { get; set; }
        public String BatchNumber { get; set; }
        public String ClientPaidOrReturnedTollVaucher { get; set; }
        public String Sterilization { get
        {
            return SterilizationBool ? "כן" : "לא";
        } }
        public String IsDangerous { get; set; }
        public bool Exemption { get; set; }
        public String ExemptionCause { get; set; }

        public String VaccinatingVetName { get; set; }
        public String SAGIRNumber { get; set; }
        public String BirthYear { get; set; }
        public int BirthMonth { get; set; }
        public String AreaCode { get; set; }
        public int AnimalKindId { get; set;}
        public int AnimalGenderTypeId { get; set; }
        public int MonthOfVaccination { get; set; }


        public bool SterilizationBool { get; set; }
        public String OwnerFullName { get { return OwnerFirstName + " " + OwnerLastName; } }

        public String Scheduled { get; set; }
        public String VetLicenseNum { get; set; }

        public int ClientId { get; set; }
    }
}
