﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums;

namespace RapidVet.WebModels.GeneralReports
{
    public class RabiesReportPostModel
    {

        public String From{ get; set; }

        public String To { get; set; }

        public int? AnimalTypeId { get; set; }
    
        public RabiesReportSterilizationOptions SterilizationOption { get; set; }

        public int? RegionalCouncilId { get; set; }

        public String MeloonaCode { get; set; }

        public int? CityId { get; set; }

    }
}
