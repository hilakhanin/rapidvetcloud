﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace RapidVet.WebModels.Clinics
{
    public class AttendanceLogsReportModel
    {
        public List<SelectListItem> Users { get; set; }

        public List<AttendanceLogWebModel> AttendanceLogs { get; set; }

        public Dictionary<string, decimal> HourSums { get; set; }
    }
}