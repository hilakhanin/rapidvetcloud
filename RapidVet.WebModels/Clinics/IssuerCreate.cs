﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Resources;

namespace RapidVet.WebModels.Clinics
{
    public class IssuerCreate
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.Issuer), Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.Issuer), Name = "CompanyId")]
        [StringLength(9, ErrorMessageResourceType = typeof(Resources.Issuer), MinimumLength = 9, ErrorMessageResourceName = "CompanyIdLengthError")]
        public string CompanyId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.Issuer), Name = "InvoiceInitialNum")]
        public int InvoiceInitialNum { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.Issuer), Name = "InvoiceReceiptInitialNum")]
        public int InvoiceReceiptInitialNum { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.Issuer), Name = "RefoundInitialNum")]
        public int RefoundInitialNum { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.Issuer), Name = "ReceiptInitialNum")]
        public int ReceiptInitialNum { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.Issuer), Name = "ProformaInitialNum")]
        public int ProformaInitialNum { get; set; }


        //[DataType(DataType.Password)]
        //[Display(ResourceType = typeof(Resources.Issuer), Name = "EasyCardPassword")]
        //public String EasyCardPassword { get; set; }


        //[Display(ResourceType = typeof(Resources.Issuer), Name = "EasyCardClientId")]
        //public String EasyCardClientId { get; set; }

        [Display(ResourceType = typeof(Resources.Issuer), Name = "TaxDedationFileNumber")]
        [StringLength(9, ErrorMessageResourceType = typeof(Resources.Issuer), MinimumLength = 9, ErrorMessageResourceName = "CompanyIdLengthError")]
        public string TaxDedationFileNumber { get; set; }

        [Display(ResourceType = typeof(Resources.Issuer), Name = "HSVatAccount")]
        public string HSVatAccount { get; set; }

        [Display(ResourceType = typeof(Resources.Issuer), Name = "HSIncomeAccount")]
        public string HSIncomeAccount { get; set; }

        [Display(ResourceType = typeof(Resources.Issuer), Name = "HSFreeIncomeAccount")]
        public string HSFreeIncomeAccount { get; set; }


        [Display(ResourceType = typeof(Resources.Issuer), Name = "HSCashAccount")]
        public string HSCashAccount { get; set; }

        [Display(ResourceType = typeof(Resources.Issuer), Name = "HSChequeAccount")]
        public string HSChequeAccount { get; set; }

        [Display(ResourceType = typeof(Resources.Issuer), Name = "HSRandoClientAccount")]
        public string HSRandoClientAccount { get; set; }

        [Display(ResourceType = typeof(Resources.Issuer), Name = "HSTranferAccount")]
        public string HSTranferAccount { get; set; }

        [Display(ResourceType = typeof(Resources.Issuer), Name = "BankDetails")]
        public string BankDetails { get; set; }

        [Display(ResourceType = typeof(Resources.Issuer), Name = "HSGeneralClientAccount")]
        public string HSGeneralClientAccount { get; set; }

        [Display(ResourceType = typeof(Resources.Issuer), Name = "HSInvoiceMovementType")]
        [StringLength(3, ErrorMessageResourceType = typeof(Resources.Issuer), ErrorMessageResourceName = "HSLengthError")]
        public string HSInvoiceMovementType { get; set; }

        [Display(ResourceType = typeof(Resources.Issuer), Name = "HSRefundMovementType")]
        [StringLength(3, ErrorMessageResourceType = typeof(Resources.Issuer), ErrorMessageResourceName = "HSLengthError")]
        public string HSRefundMovementType { get; set; }

        [Display(ResourceType = typeof(Resources.Issuer), Name = "HSClientSortKey")]
        [StringLength(4, ErrorMessageResourceType = typeof(Resources.Issuer), ErrorMessageResourceName = "HSLengthErrorFourChars")]
        public string HSClientSortKey { get; set; }

         [Display(ResourceType = typeof(Resources.Issuer), Name = "Active")]
        public bool Active { get; set; }

    }
}
