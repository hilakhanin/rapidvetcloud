﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clinics
{
    public class StorageUsage
    {
       public string ClinicGroup { get; set; }
       public string ClinicName { get; set; }
       public string Allocation { get; set; }
       public double UsagePercent { get; set; }
       public string UsageSize { get; set; }
    }
}
