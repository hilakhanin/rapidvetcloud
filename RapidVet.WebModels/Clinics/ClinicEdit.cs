﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RapidVet.WebModels.Clinics
{
    public class ClinicEdit
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "Name")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "MainVet")]
        [ForeignKey("MainVetId")]
        [UIHint("ClinicDoctors")]
        public int? MainVetId { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "EntryDuration")]
        public int? EntryDuration { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "CalendarSpread")]
        public string CalendarSpread { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "Active")]
        public bool Active { get; set; }

        //[Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "DefaultTariffId")]
        [UIHint("Tariffs")]
        public int DefualtTariffId { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "DefaultNewClientStatusId")]
        [UIHint("DefaultClientStatus")]
        public int DefaultNewClientStatusId { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "Address")]
        public string Address { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "Phone")]
        public string Phone { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "Fax")]
        public string Fax { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "Email")]
        public string Email { get; set; }


        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "DefaultCityId")]
        [UIHint("Cities")]
        public int CityId { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "DefaultAnimalId")]
        [UIHint("AnimalKind")]
        public int AnimalKindId { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "RegionalVet")]
        public string RegionalVet { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "RegionalCouncilId")]
        [UIHint("RegionalCouncils")]
        public int RegionalCouncilId { get; set; }


        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "InvoiceReciptOnly")]
        public bool InvoiceReciptOnly { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "HSInitNumber")]
        public int? HSInitNumber { get; set; }

        public List<EntryDurationList> EntryDuration2 { get; set; }

        public string CityName { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "ShowPopUpWhenClientInDebt")]
        public bool ShowPopUpWhenClientInDebt { get; set; }
        
        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "ArchiveAllocation")]
        public int ArchiveAllocation { get; set; }

    }

    public class EntryDurationList
    {
        public int value { get; set; }
        public string feild { get; set; }
    }
}
