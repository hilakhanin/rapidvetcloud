﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clinics
{
    public class DefaultCardCompaniesWebModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Localization { get; set; }
    }
}
