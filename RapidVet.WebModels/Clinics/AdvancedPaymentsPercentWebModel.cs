﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clinics
{
    public class AdvancedPaymentsPercentWebModel
    {
       public int Id { get; set; }
       public string Date { get; set; }
       public decimal Percent { get; set; }
       public DateTime DateTime { get; set; }
       public int IssuerId { get; set; }
    }
}
