﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clinics
{
    public class WaitingListItemJson
    {
        public int Id { get; set; }
        public int ClinicId { get; set; }
        public int ClientId { get; set; }
        public int? PatientId { get; set; }
        public bool IsSunday { get; set; }
        public bool IsMonday { get; set; }
        public bool IsTuesday { get; set; }
        public bool IsWednesday { get; set; }
        public bool IsThursday { get; set; }
        public bool IsFriday { get; set; }
        public bool IsSaturday { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public int AppointmentDurationInMinutes { get; set; }
        public int? DoctorId { get; set; }
        public string Comments { get; set; }
        public string DoctorName { get; set; }
        public string ClientName { get; set; }
        public string PatientName { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        public string UrgencyType { get; set; }
        public int UrgencyTypeId { get; set; }
        public string DateStamp { get; set; }
    }
}
