﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clinics
{
    public class FinanceClinicEdit
    {
        public int Id { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "DollarExchangeRate")]
        public decimal DollarExchangeRate { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "ApplyDividingIncomeModel")]
        public bool ApplyDividingIncomeModel { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "PerformDividingIncomeWithoutConciderationOfDistributedPayment")]
        public bool PerformDividingIncomeWithoutConciderationOfDistributedPayment { get; set; }

        // ArrangeIncomeByEnum
        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "ArrangeIncomeById")]
        [UIHint("ArrangeIncomeByEnums")]
        public int ArrangeIncomeById { get; set; }

        // DefaultIssuerEnum
        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "DefaultIssuerId")]
        [UIHint("DefaultIssuerEnums")]
        public int DefaultIssuerId { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "AdvancedPaymentPercent")]
        public decimal AdvancedPaymentPercent { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "TaxRate")]
        public decimal TaxRate { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "CoSignUserName")]
        public string CoSignUserName { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "CoSignPassword")]
        public string CoSignPassword { get; set; }

         [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "MoveZeroSumTreatmentsToInvoice")]
        public bool MoveZeroSumTreatmentsToInvoice { get; set; }

         [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "CancelAutomaticPrinting")]
         public bool CancelAutomaticPrinting { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "DontMoveItemsFromVisitToInvoice")]
         public bool DontMoveItemsFromVisitToInvoice { get; set; }
        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "DontAddToVisitNewItemsInInvoice")]
         public bool DontAddToVisitNewItemsInInvoice { get; set; }
        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "MoveItemsFromVisitToInvoiceFromDate")]
        public DateTime? MoveItemsFromVisitToInvoiceFromDate { get; set; }


    }
}
