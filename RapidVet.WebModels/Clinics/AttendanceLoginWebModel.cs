﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RapidVet.WebModels.Clinics
{
    public class AttendanceLoginWebModel
    {
        public List<SelectListItem> Users { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Resources.WebModels.Account), Name = "UserName")]
        public int UserId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.WebModels.Account), Name = "Password")]
        public string Password { get; set; }

        public bool LogIn { get; set; }

        [Display(ResourceType = typeof(Resources.AttendanceLog), Name = "LastLogIn")]
        public DateTime? LastLogIn { get; set; }

        [Display(ResourceType = typeof(Resources.AttendanceLog), Name = "LastLogOut")]
        public DateTime? LastLogOut { get; set; }
    }
}
