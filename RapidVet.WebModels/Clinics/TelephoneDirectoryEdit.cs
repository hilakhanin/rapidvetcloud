﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RapidVet.WebModels.Clinics
{
    public class TelephoneDirectoryEdit
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public string Name { get; set; }

        public string Address { get; set; }

        [RegularExpression("^[0-9-\\*]*$", ErrorMessage = "יש להזין ספרות ומקפים בלבד")]
        public string PrimaryPhone { get; set; }

        [RegularExpression("^[0-9-\\*]*$", ErrorMessage = "יש להזין ספרות ומקפים בלבד")]
        public string SecondaryPhone { get; set; }

        [RegularExpression("^[0-9-\\*]*$", ErrorMessage = "יש להזין ספרות ומקפים בלבד")]
        public string CellPhone { get; set; }

        [RegularExpression("^[0-9-\\*]*$", ErrorMessage = "יש להזין ספרות ומקפים בלבד")]
        public string Fax { get; set; }

        public string ContactPerson { get; set; }
        
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public int ClinicId { get; set; }

        public string PrimaryPhoneComment { get; set; }

        public string SecondaryPhoneComment { get; set; }

        public string CellPhoneComment { get; set; }

        public int? TelephoneDirectoryTypeId { get; set; }

        public List<SelectListItem> TelephoneDirectoryTypes { get; set; }
    }
}
