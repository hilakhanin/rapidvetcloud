﻿using System;

namespace RapidVet.WebModels.Clinics
{
    public class AttendanceLogWebModel
    {
        public int Id { get; set; }

        public int ClinicId { get; set; }
        
        public int UserId { get; set; }

        public string UserName { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }
    }
}