﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clinics
{
   public class TaxRateWebModel
    {
       public int Id { get; set; }
       public string Date { get; set; }
       public decimal Rate { get; set; }
       public DateTime DateTime { get; set; }
    }
}
