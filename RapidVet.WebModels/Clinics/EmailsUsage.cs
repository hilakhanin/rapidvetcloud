﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clinics
{
    public class EmailsUsage
    {
       public string ClinicGroup { get; set; }
       public string ClinicName { get; set; }
       public int EmailsCount { get; set; }
       public int SmsCount { get; set; }
    }
}
