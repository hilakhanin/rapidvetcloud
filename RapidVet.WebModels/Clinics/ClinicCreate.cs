﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Clinics
{
    public class ClinicCreate
    {
        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "Name")]
        public string Name { get; set; }

        public int ClinicGroupId { get; set; }
    }
}
