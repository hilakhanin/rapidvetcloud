﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;


namespace apidVet.WebModels.FollowUp
{
    public class FollowUpWebModel
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public string DateTime { get; set; }

        public string Comment { get; set; }

        public int UserId { get; set; }
   }
}
