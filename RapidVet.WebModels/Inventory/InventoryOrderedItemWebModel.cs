﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Inventory
{
    public class InventoryOrderedItemWebModel
    {
        public int orderItemId { get; set; }

        public int id { get; set; }

        public string category { get; set; }

        public string catalog { get; set; }

        public string name { get; set; }

        public decimal price { get; set; }

        public decimal actualUnitPrice { get; set; }

        public decimal quantity { get; set; }

        public decimal recieved { get; set; }

        public bool requireBarcode { get; set; }

        public string barcode { get; set; }

  
    }
}
