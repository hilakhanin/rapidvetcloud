﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Inventory
{
   public class InventoryValueReportModel
    {
       public int PriceListItemId { get; set; }

       public string Category { get; set; }

       public string Name { get; set; }

       public decimal Quantity { get; set; }

       public decimal Sum { get; set; }
    }
}
