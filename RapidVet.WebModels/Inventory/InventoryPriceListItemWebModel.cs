﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Inventory
{
    public class InventoryPriceListItemWebModel
    {
        public int Id { get; set; }

        public string CategoryName { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public decimal Quantity { get; set; }

        public bool RequireBarcode { get; set; }
    }
}
