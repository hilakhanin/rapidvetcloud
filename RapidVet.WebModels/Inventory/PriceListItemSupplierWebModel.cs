﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Inventory
{
    public class PriceListItemSupplierWebModel
    {
        public int SupplierId { get; set; }

        public string SupplierName { get; set; }

        public bool DefaultSupplier { get; set; }

        public bool ConnectedSupplier { get; set; }

        public decimal CostPrice { get; set; }
    }
}
