﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Inventory
{
    public class InventoryStatusReportModel
    {
        public string Date { get; set; }

        public string UserName { get; set; }

        public decimal Quantity { get; set; }

        public string Action { get; set; }

        public int ActionId { get; set; }

        public string PriceListItem { get; set; }
    }
}
