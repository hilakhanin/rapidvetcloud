﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.MinistryOfAgricultureReport
{
    public class Attack
    {
        public String Date { get; set; }
        public String Description { get; set; }
        public String CancellationDate { get; set; }
    }
}
