﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.MinistryOfAgricultureReport
{
    public class QuarantineAdmission
    {
        public String Date { get; set; }
        public String FacilityName { get; set; }
        public String AdmissionReason { get; set; }
        public String ContactName { get; set; }
        public String Phone { get; set; }
    }
}
