﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.MinistryOfAgricultureReport
{
    public class DangerousDog
    {
        public String Date { get; set; }
        public String DangerousDogReason { get; set; }
        public String InsuranceFinishDate { get; set; }
        public String InsuranceCompany { get; set; }
        public String InsuranceNumber { get; set; }
    }
}
