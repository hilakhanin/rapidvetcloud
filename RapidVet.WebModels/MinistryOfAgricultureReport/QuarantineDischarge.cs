﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.MinistryOfAgricultureReport
{
    public class QuarantineDischarge
    {
        public String Date { get; set; }
        public String DischargingFacilityName { get; set; }
        public int DischargeTypeId { get; set; }
    }
}
