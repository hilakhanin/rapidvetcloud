﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.MinistryOfAgricultureReport
{
    public class MinistryOfAgricultureReportItem
    {
        public int Id { get; set; }
        public String ClientName { get; set; }
        public String PatientName { get; set; }
        public String DateTime { get; set; }
        public String ReportType { get; set; }
        public String Comments { get; set; }

    }
}
