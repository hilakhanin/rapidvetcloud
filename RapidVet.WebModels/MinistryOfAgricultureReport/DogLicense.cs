﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.MinistryOfAgricultureReport
{
    public class DogLicense
    {
        public String Date { get; set; }
        public String DogLicenseNumber { get; set; }
        public String VerificationNumber { get; set; }
    }
}
