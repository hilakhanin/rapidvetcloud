﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.MinistryOfAgricultureReport
{
    public class ChipImplant
    {
        public String Date { get; set; }
        public String VetLicense { get; set; }
        public bool Reimplant { get; set; }
    }
}
