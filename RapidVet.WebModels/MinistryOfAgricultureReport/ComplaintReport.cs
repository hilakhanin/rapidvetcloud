﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.MinistryOfAgricultureReport
{
    public class ComplaintReport
    {
        public String Date { get; set; }
        public String ComplaintDetails { get; set; }
        public int ComplaintTypeId { get; set; }
        public bool LegalProceeding { get; set; }
        public int OffenceTypeId { get; set; }

        }
}
