﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.MinistryOfAgricultureReport
{
    public class RabiesVaccine
    {
        public string Date { get; set; }
        public string VaccinatingVetLicense { get; set; }
        public string Batch { get; set; }
    }
}
