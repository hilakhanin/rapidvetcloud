﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.MinistryOfAgricultureReport
{
    public class RevokingLicense
    {
        public string Date { get; set; }
        public string RevokingReason { get; set; }
    }
}
