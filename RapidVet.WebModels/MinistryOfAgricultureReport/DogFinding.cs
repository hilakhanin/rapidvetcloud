﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.MinistryOfAgricultureReport
{
    public class DogFinding
    {
        public String Date { get; set; }
        public String Details { get; set; }
        public bool ReportedToOwner { get; set; }
    }
}
