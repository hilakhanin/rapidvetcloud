﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Resources;

namespace RapidVet.WebModels.Visits
{
    public class VisitMedicationModel
    {
        public int Id { get; set; }

        public int MedicationId { get; set; }

        public string Name { get; set; }

        public int MedicationAdministrationTypeId { get; set; }

        public string MedicationAdministrationTypeName { get; set; }

        public string Dosage { get; set; }

        public int? TimesPerDay { get; set; }

        public int? NumberOfDays { get; set; }

        public string Insturctions { get; set; }

        public decimal UnitPrice { get; set; }
        
        public decimal? Discount { get; set; }

        public decimal? DiscountPercent { get; set; }

        public decimal Total { get; set; }

        public decimal OverallAmount { get; set; }

        public int? QuotationTreatmentId { get; set; }

        public string AddedBy { get; set; }

        public int AddedByUserId { get; set; }
    }
}
