﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Visits
{
    public class DiagnosisAdminModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
