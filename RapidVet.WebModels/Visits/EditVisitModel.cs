using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using RapidVet.Enums;


namespace RapidVet.WebModels.Visits
{
    public class VisitMetaDataModel
    {
        public List<SelectListItem> DiagnosesOptions { get; set; }

        public List<PLItem> PriceListItems { get; set; }

        public List<MedicationOption> MedicationOptions { get; set; }

        public List<SelectListItem> CategoryOpions { get; set; }

        public List<SelectListItem> DoctorOptions { get; set; }

        public List<SelectListItem> AdministrationTypeOptions
        {
            get
            {
                var res = new List<SelectListItem>();
                foreach (var type in Enum.GetValues(typeof(MedicationAdministrationType)))
                {
                    res.Add(new SelectListItem()
                    {
                        Selected = false,
                        Text = type.ToString(),
                        Value = ((int)type).ToString()
                    });
                }
                return res;
            }
        }
    }

    public class EditVisitModel
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public int ClientId { get; set; }

        [UIHint("ClinicDoctors")]
        public int UserId { get; set; }

        public decimal Weight { get; set; }

        public decimal Temp { get; set; }

        public int Pulse { get; set; }

        public int Bpm { get; set; }

        public int Bcs { get; set; }

        public string Mucose { get; set; }

        public string Color { get; set; }

        public string Date { get; set; }

        public string Time { get; set; }

        public string MainComplaint { get; set; }

        public bool PriceManualInput { get; set; }

        public decimal? InputPrice { get; set; }

        public decimal? Price
        {
            get
            {
                if (PriceManualInput && InputPrice.HasValue)
                {
                    return InputPrice.Value;
                }
                return null;
            }
        }


        
        
        public String PostedExaminations { get; set; }

        public List<CreateVisitExamination> Examinations
        {
            get
            {
                if (PostedExaminations != null)
                {
                    var js = new JavaScriptSerializer();
                    return js.Deserialize<List<CreateVisitExamination>>(PostedExaminations);
                }
                return null;
            }
        }

        public String PostedTreatments { get; set; }
        public List<CreateVisitTreatment> Treatments
        {
            get
            {
                if (PostedTreatments != null)
                {
                    var js = new JavaScriptSerializer();
                    return js.Deserialize<List<CreateVisitTreatment>>(PostedTreatments);
                }
                return null;
            }
        }

        public String PostedDiagnoses { get; set; }
        public List<CreateVisitDiagnosis> Diagnoses
        {
            get
            {
                if (PostedDiagnoses != null)
                {
                    var js = new JavaScriptSerializer();
                    return js.Deserialize<List<CreateVisitDiagnosis>>(PostedDiagnoses);
                }
                return null;
            }
        }

        public String PostedMedications { get; set; }
        public List<VisitMedicationModel> Medications
        {
            get
            {
                if (PostedMedications != null)
                {
                    var js = new JavaScriptSerializer();
                    return js.Deserialize<List<VisitMedicationModel>>(PostedMedications);
                }
                return null;
            }
        }

        private DateTime? _visitDate;

        public DateTime VisitDate
        {
            get
            {
                if (_visitDate.HasValue)
                {
                    return _visitDate.Value;
                }
                else
                {
                    var format = "dd/MM/yyyy HH:mm";
                    var unparsed = Date + " " + Time;
                    var res = DateTime.MinValue;
                    DateTime.TryParseExact(unparsed, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out res);
                    if (res == DateTime.MinValue)
                    {
                        res = DateTime.Now;
                    }
                    return res;
                }
            }
            set
            {
                _visitDate = value;
                Time = value.ToShortTimeString();
                Date = value.ToShortDateString();
            }


        }

    }
}

public class MedicationOption
{
    public int MedicationId { get; set; }

    public string Name { get; set; }

    public int MedicationAdministrationTypeId { get; set; }

    public string Dosage { get; set; }

    public int? TimesPerDay { get; set; }

    public int? NumberOfDays { get; set; }

    public string Insturctions { get; set; }

    public decimal? UnitPrice { get; set; }

    public bool DangerousDrug { get; set; }

    public string DoctorInfo { get; set; }

    public bool Active { get; set; }
}

public class CreateVisitTreatment
{
    public int PricePerUnit { get; set; }

    public decimal Quantity { get; set; }

    public int Discount { get; set; }

    public int TotalPrice { get; set; }

    public int? PriceListCategoryId { get; set; }

    private int? _treatmentId;
    public int? TreatmentId
    {
        get
        {
            if (_treatmentId.HasValue && _treatmentId == 0)
            {
                _treatmentId = null;
            }
            return _treatmentId;
        }
        set { _treatmentId = value; }
    }

    public String TreatmentText { get; set; }
}

public class CreateVisitExamination
{
    private int? _examinationId;
    public int? ExaminationId
    {
        get
        {
            if (_examinationId.HasValue && _examinationId == 0)
            {
                _examinationId = null;
            }
            return _examinationId;
        }
        set { _examinationId = value; }
    }

    public int PriceListCategoryId { get; set; }

    public string ExaminationText { get; set; }

    public int Price { get; set; }
}

public class CreateVisitDiagnosis
{
    private int? _diagnosisId;
    public int? DiagnosisId
    {
        get
        {
            if (_diagnosisId.HasValue && _diagnosisId == 0)
            {
                _diagnosisId = null;
            }
            return _diagnosisId;
        }
        set { _diagnosisId = value; }
    }
    public String Content { get; set; }
}

public class PLItem
{
    public String Name { get; set; }
    public decimal Price { get; set; }
    public String CategoryId { get; set; }
    public String Catalog { get; set; }
    public String Barcode { get; set; }
    public int Id { get; set; }
    public int Type { get; set; }
}
