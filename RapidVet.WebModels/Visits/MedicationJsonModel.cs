﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Visits
{
    public class MedicationJsonModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int MedicationTypeId { get; set; }

        public string Dosage { get; set; }

        public string Duration { get; set; }

        public decimal Total { get; set; }

        public string Insturctions { get; set; }

        public decimal UnitPrice { get; set; }

        public string DoctorNoates { get; set; }

        public bool Dangerous { get; set; }
    }
}
