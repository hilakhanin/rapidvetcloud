﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Finance;

namespace RapidVet.WebModels.Visits
{
    public class VisitSectionsJsonModel
    {
        private ICollection<CategoryJsonModel> _examinations;
        public ICollection<CategoryJsonModel> Examinations
        {
            get { return _examinations ?? (_examinations = new Collection<CategoryJsonModel>()); }
            set { _examinations = value; }
        }

        private ICollection<CategoryJsonModel> _treatments;
        public ICollection<CategoryJsonModel> Treatments
        {
            get { return _treatments ?? (_treatments = new Collection<CategoryJsonModel>()); }
            set { _treatments = value; }
        }

        private ICollection<MedicationJsonModel> _medications;
        public ICollection<MedicationJsonModel> Medications
        {
            get { return _medications ?? (_medications = new Collection<MedicationJsonModel>()); }
            set { _medications = value; }
        }

        private ICollection<DiagnosisAdminModel> _diagnoses;
        public ICollection<DiagnosisAdminModel> Diagnoses
        {
            get { return _diagnoses ?? (_diagnoses = new Collection<DiagnosisAdminModel>()); }
            set { _diagnoses = value; }
        }
    }
}
