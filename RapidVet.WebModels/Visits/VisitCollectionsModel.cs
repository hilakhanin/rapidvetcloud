﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Visits
{
    public class VisitCollectionsModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int EntryId { get; set; }
        public string Text { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
    }
}
