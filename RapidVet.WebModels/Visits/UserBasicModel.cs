namespace RapidVet.WebModels.Visits
{
    public class UserBasicModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}