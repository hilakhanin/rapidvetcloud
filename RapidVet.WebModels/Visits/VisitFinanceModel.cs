﻿using RapidVet.WebModels.Finance.Reports;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Visits
{
    public class VisitFinanceModel : IEquatable<VisitFinanceModel>
    {
        public int ClientId { get; set; }

        public int? FinanceDocumentId { get; set; }
        public int? VisitId { get; set; }
        public string VisitDate { get; set; }

        public string DoctorName { get; set; }

        public string ClientName { get; set; }

        public decimal? Price { get; set; }

        public string MainComplaint { get; set; }

        public int SerialNumber { get; set; }

        public bool IsAddedByCalender { get; set; }

        public bool Equals(VisitFinanceModel other)
        {
            if (other == null)
                return false;

            return this.ClientId == other.ClientId && this.SerialNumber == other.SerialNumber && this.Price == other.Price;
        }

        public override int GetHashCode()
        {
            int hashClientId = ClientId == null ? 0 : ClientId.GetHashCode();
            int hashSerialNumber = SerialNumber == null ? 0 : SerialNumber.GetHashCode();
            int hashPrice = Price == null ? 0 : Price.GetHashCode();

            return hashClientId ^ hashSerialNumber ^ hashPrice;
        }

        private ICollection<IncomingsReportWebModel> _recipts;
        public ICollection<IncomingsReportWebModel> Recipts
        {
            get { return _recipts ?? (_recipts = new Collection<IncomingsReportWebModel>()); }
            set { _recipts = value; }
        }
    }
}
