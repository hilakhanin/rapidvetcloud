﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Visits
{
    public class VisitChildItem
    {
        public int Id { get; set; }
        public int? PriceListItemId { get; set; } 
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal? Discount { get; set; }
        public decimal? DiscountPercent { get; set; }
        public decimal Price { get; set; }
        public int? DiagnosisId { get; set; }
        public int? QuotationTreatmentId { get; set; }
        public string AddedBy { get; set; }
        public int AddedByUserId { get; set; }
    }
}
