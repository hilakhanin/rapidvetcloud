﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Visits
{
    public class DangerousDrugWebModel
    {
        public int Id { get; set; }

        public int MedicationId { get; set; }

        public string MedicationName { get; set; }

        public decimal AmountInjected { get; set; }

        public decimal AmountDestroyed { get; set; }
    }
}
