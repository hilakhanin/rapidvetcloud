using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Medications;
using RapidVet.WebModels.PreventiveMedicine;
using RapidVet.WebModels.Quotations;

namespace RapidVet.WebModels.Visits
{
    public class VisitModel
    {
        public int Id { get; set; }

        public int ManualId { get; set; }

        public int? PatientId { get; set; }

        public string MainComplaint { get; set; }

        public decimal Weight { get; set; }

        public int Pulse { get; set; }

        public decimal Temp { get; set; }

        public int BPM { get; set; }

        public int BCS { get; set; }

        public int HR { get; set; }

        public string Mucosa { get; set; }

        public string Color { get; set; }

        public string Time { get; set; }

        public string Date { get; set; }

        public bool Close { get; set; }

        public int SelectedDoctorId { get; set; }

        public decimal TotalPrice { get; set; }

        public string VisitNote { get; set; }

        private ICollection<VisitChildItem> _visitExaminations;
        public ICollection<VisitChildItem> VisitExaminations
        {
            get { return _visitExaminations ?? (_visitExaminations = new Collection<VisitChildItem>()); }
            set { _visitExaminations = value; }
        }

        private ICollection<VisitChildItem> _visitDiagnoses;
        public ICollection<VisitChildItem> VisitDiagnoses
        {
            get { return _visitDiagnoses ?? (_visitDiagnoses = new Collection<VisitChildItem>()); }
            set { _visitDiagnoses = value; }
        }

        private ICollection<VisitChildItem> _visitTreatments;
        public ICollection<VisitChildItem> VisitTreatments
        {
            get { return _visitTreatments ?? (_visitTreatments = new Collection<VisitChildItem>()); }
            set { _visitTreatments = value; }
        }


        private ICollection<VisitMedicationModel> _visitMedications;
        public ICollection<VisitMedicationModel> VisitMedications
        {
            get { return _visitMedications ?? (_visitMedications = new Collection<VisitMedicationModel>()); }
            set { _visitMedications = value; }
        }

        private ICollection<DangerousDrugWebModel> _dangerousDrugs;
        public ICollection<DangerousDrugWebModel> DangerousDrugs
        {
            get { return _dangerousDrugs ?? (_dangerousDrugs = new Collection<DangerousDrugWebModel>()); }
            set { _dangerousDrugs = value; }
        }

        private ICollection<UserBasicModel> _doctors;
        public ICollection<UserBasicModel> Doctors
        {
            get { return _doctors ?? (_doctors = new Collection<UserBasicModel>()); }
            set { _doctors = value; }
        }

        private ICollection<PriceListCategoryModel> _categories;
        public ICollection<PriceListCategoryModel> Categories
        {
            get { return _categories ?? (_categories = new Collection<PriceListCategoryModel>()); }
            set { _categories = value; }
        }

        public DateTime VisitDate { get; set; }

        public bool Active { get; set; }

        public int VitalSignID { get; set; }

        public string ActiveUserName { get; set; }

        public int ActiveUserId { get; set; }
    }
}