﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RapidVet.WebModels.Visits
{
    public class VisitFilterWebModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ClinicId { get; set; }

        //Client
        public int? ClientId { get; set; }

        //Animal Kind
        public int? AnimalKindId { get; set; }

        //DateRange
        public string FromDate { get; set; }
        public string ToDate { get; set; }

        //Animal Age Range
        public int? FromYear { get; set; }
        public int? FromMonth { get; set; }
        public int? ToYear { get; set; }
        public int? ToMonth { get; set; }

        //Visit collections Ids
        public string CategoryId { get; set; }
        public string TreatmentId { get; set; }
        public int? DiagnosisId { get; set; }
     //   public int? ExaminationId { get; set; }
        public int? MedicationId { get; set; }
        //Doctor
        public string DoctorId { get; set; }

        public List<string> SelectedDoctorIds { get; set; }


        //Sum Range of Visit
        public decimal? FromSum { get; set; }
        public decimal? ToSum { get; set; }

        //Visit Descriptions and Complaint Keymatch
        public string VisitDescription { get; set; } //refers to same as main complaint
        public string MainComplaint { get; set; }

        //TableCheckBoxes
        public bool nameVisitCb { get; set; }
        public bool datePerformedCb { get; set; }
        public bool clientNameCb { get; set; }
        public bool patientNameCb { get; set; }
        public bool serialCb { get; set; }
        public bool addressCb { get; set; }
        public bool homePhoneCb { get; set; }
        public bool cellPhoneCb { get; set; }
        public bool animalKindCb { get; set; }
        public bool animalRaceCb { get; set; }
        public bool animalGenderCb { get; set; }
        public bool animalColorCb { get; set; }
        public bool animalAgeCb { get; set; }
        public bool sterilizationCb { get; set; }
        public bool electronicNumCb { get; set; }
        public bool birthDateCb { get; set; }
        public bool licenseNumCb { get; set; }
        public bool chargeFeeCb { get; set; }
        public bool performingDocCb { get; set; }
        public bool numOfUnitsCb { get; set; }
      //  public bool plateNumCb { get; set; }
        public bool visitDescCb { get; set; }
        public bool treatingDoctorCb { get; set; }
        public bool receiptCb { get; set; }

        public string printFontSize { get; set; }
    }
}
