﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Visits
{
    public class PrescriptionModel
    {
        public string ClinicName { get; set; }
        public string ClinicAddress { get; set; }
        public string ClinicPhone { get; set; }
        public string ClinicFax { get; set; }
        public string ClinicEmail { get; set; }
        public string DoctorName { get; set; }
        public string DoctorLicenseNumber { get; set; }
        public string ClientName { get; set; }
        public string ClientIdCard { get; set; }
        public string ClientAddress { get; set; }
        public string PatientName { get; set; }
        private ICollection<VisitMedicationModel> _visitMedications;
        public ICollection<VisitMedicationModel> VisitMedications
        {
            get { return _visitMedications ?? (_visitMedications = new Collection<VisitMedicationModel>()); }
            set { _visitMedications = value; }
        }

        public bool PrintComments { get; set; }
    }
}
