﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.Visits;

namespace RapidVet.WebModels.VitalSigns
{
    public class VitalSignsIndexModel
    {
        public ClientDetailsModel ClientDetailsModel { get; set; }

        private ICollection<VisitModel> _visits;
        public ICollection<VisitModel> Visits
        {
            get { return _visits ?? (_visits = new Collection<VisitModel>()); }
            set { _visits = value; }
        }
    }
}
