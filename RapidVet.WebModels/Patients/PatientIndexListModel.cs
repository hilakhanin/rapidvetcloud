﻿using System.IO;
using System.Web;

namespace RapidVet.WebModels.Patients
{
    public class PatientIndexListModel
    {
        public int Id { get; set; }

        public int LocalId { get; set; }

        public int ClinicId { get; set; }

        public string Name { get; set; }

        public string AnimalIconFileName { get; set; }

        public string ElectronicNumber { get; set; }

        public string IconPath
        {
            get { return string.Format("/Content/img/RapidVet/animale-icons/{0}", AnimalIconFileName); }
        }

    }
}