﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Resources;

namespace RapidVet.WebModels.Patients
{
    public class AnimalColorModel
    {
        [Display(ResourceType = typeof(RapidVet.Resources.Global), Name = "Id")]
        public int Id { get; set; }

        public int ClinicGroupId { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Controllers.AnimalColorController), Name = "Value")]
        public string Value { get; set; }

 


    }
}
