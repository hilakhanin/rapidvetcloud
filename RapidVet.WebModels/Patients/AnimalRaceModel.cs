﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Patients
{
    public class AnimalRaceModel
    {
        [Display(ResourceType = typeof(RapidVet.Resources.Global), Name = "Id")]
        public int Id { get; set; }

       
        public int AnimalKindId { get; set; }
  
        //public AnimalKindModel AnimalKind { get; set; }

        
        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Controllers.AnimalRaceController), Name = "Value")]
        public string Value { get; set; }
    }
}
