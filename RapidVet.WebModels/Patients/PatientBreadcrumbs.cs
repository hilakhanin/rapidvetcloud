﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Patients
{
    public class PatientBreadcrumbs
    {
        public int PatientId { get; set; }
        public String PatientName { get; set; }

        public int ClientId { get; set; }
        public String ClientName { get; set; }
    }
}
