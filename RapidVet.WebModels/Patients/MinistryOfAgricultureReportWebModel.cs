﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RapidVet.Enums;
using RapidVet.WebModels.Clients;

namespace RapidVet.WebModels.Patients
{
    public class MinistryOfAgricultureReportWebModel
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public ClientDetailsModel Client { get; set; }

        [UIHint("_MinistryOfAgricultureReportTypes")]
        public int ReportTypeId { get; set; }

        public MinistryOfAgricultureReportType ReportType
        {
            get { return (MinistryOfAgricultureReportType)ReportTypeId; }
            set { ReportTypeId = (int)value; }
        }

        public List<SelectListItem> ReportTypeOptions { get; set; }
        public List<SelectListItem> VetLicenseOptions { get; set; }

        public List<SelectListItem> ComplaintTypeOptions { get; set; }
        public List<SelectListItem> OffenceTypeOptions { get; set; }

        public List<SelectListItem> DischargeTypeOptions { get; set; }

        public DateTime DateTime { get; set; }

        public string Comments { get; set; }
    }
}
