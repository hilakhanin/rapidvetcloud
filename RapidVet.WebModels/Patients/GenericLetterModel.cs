﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Clients;

namespace RapidVet.WebModels.Patients
{
    public class GenericLetterModel
    {

        public ClientDetailsModel Client { get; set; }

        [UIHint("_ClinicDoctorsForLetters")]
        public string DoctorName { get; set; }

        public string ExaminationDate { get; set; }
    }
}
