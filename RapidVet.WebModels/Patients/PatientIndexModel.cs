﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Clients;
using System.ComponentModel.DataAnnotations.Schema;

namespace RapidVet.WebModels.Patients
{
    public class PatientIndexModel
    {

        public ICollection<PatientIndexListModel> Patients { get; set; }
        public ICollection<PatientIndexListModel> InactivePatients { get; set; }
        public ClientDetailsModel Client { get; set; }

        public bool HasSmsAccount { get; set; }
    }
}
