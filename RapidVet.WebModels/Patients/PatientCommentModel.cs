﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Patients
{
    public class PatientCommentModel
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public PatientCreateModel Patient { get; set; }

        public string CommentBody { get; set; }

        [DataType(DataType.Date)]
        public DateTime ValidThrough { get; set; }

        public bool Popup { get; set; }

        public bool Active { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? CreatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedById { get; set; }
    }
}
