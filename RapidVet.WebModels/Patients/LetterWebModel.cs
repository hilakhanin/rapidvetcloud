﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.WebModels.Clients;

namespace RapidVet.WebModels.Patients
{
    public class LetterWebModel
    {
        public ClientDetailsModel Client { get; set; }
        public int Id { get; set; }

        public int PatientId { get; set; }

        public int LetterTemplateTypeId { get; set; }
        public LetterTemplateType LetterTemplateType
        {
            get { return (LetterTemplateType)LetterTemplateTypeId; }
            set { LetterTemplateTypeId = (int)value; }
        }

        public string Content { get; set; }

        [Display(Name = "תעד מסמך בתיק חיה")]
        public bool ShowInPatientHistory { get; set; }

        public bool Print { get; set; }
    }
}
