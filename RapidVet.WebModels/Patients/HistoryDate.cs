﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Patients
{
    public class HistoryDate
    {
        public DateTime Date { get; set; }

        public string DateString
        {
            get { return Date.ToShortDateString(); }
        }

        private List<HistoryItem> _items; 
        public List<HistoryItem> Items
        {
            get { return _items ?? (_items = new List<HistoryItem>()); }
            set { _items = value; }
        }
    }
}
