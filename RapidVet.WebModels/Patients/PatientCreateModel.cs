﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Resources;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.Finance;

namespace RapidVet.WebModels.Patients
{
    public class PatientCreateModel
    {
        public int Id { get; set; }

        public int ClientId { get; set; }

        public ClientDetailsModel Client { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "Name")]
        public string Name { get; set; }

        public AnimalKindModel AnimalKind { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "AnimalRace")]
        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [UIHint("AnimalKind")]
        public int AnimalKindId { get; set; }

        [UIHint("AnimalRace")]
        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public int AnimalRaceId { get; set; }

        public AnimalRaceModel AnimalRace { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "AnimalColor")]
        [UIHint("AnimalColor")]
        public int? AnimalColorId { get; set; }


        public AnimalColorModel AnimalColor { get; set; }


        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "BirthDate")]
        //[DataType(DataType.Date)]
        //   [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public DateTime? BirthDate { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Global), Name = "Gender")]
        [UIHint("Gender")]
        public int GenderId { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "PureBred")]
        public bool PureBred { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "Fertilization")]
        public bool Fertilization { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "Sterilization")]
        public bool Sterilization { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "SterilizationDate")]
        [DataType(DataType.Date)]
        public DateTime? SterilizationDate { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "ElectronicNumber")]
        public string ElectronicNumber { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "LicenseNumber")]
        public string LicenseNumber { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "Owner_Farm")]
        public string Owner_Farm { get; set; }


        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "SAGIRNumber")]
        public string SAGIRNumber { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "HumanDangerous")]
        public bool HumanDangerous { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "AnimalDangerous")]
        public bool AnimalDangerous { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "BloodType")]
        public string BloodType { get; set; }


        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "Allergies")]
        public string Allergies { get; set; }

        [UIHint("FoodType")]
        public int? FoodTypeId { get; set; }

        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "FoodType")]
        public PriceListItemModel FoodType { get; set; }


        [Display(ResourceType = typeof(RapidVet.Resources.Patient), Name = "FoodAmount")]
        public string FoodAmount { get; set; }

        [Range(0, 100)]
        public decimal Age { get; set; }

        private ICollection<PatientCommentModel> _comments;

        public ICollection<PatientCommentModel> Comments
        {
            get { return _comments ?? (_comments = new Collection<PatientCommentModel>()); }
            set { _comments = value; }
        }

        public bool Active { get; set; }


        //האם כלב נחיה
        public bool SeeingEyeDog { get; set; }

        //האם יש פטור מאגרת כלבת
        public bool Exemption { get; set; }

        //סיבת הפטור
        public string ExemptionCause { get; set; }
        public string oldAnimalType { get; set; }
        public string oldAnimalRace { get; set; }
    }
}
