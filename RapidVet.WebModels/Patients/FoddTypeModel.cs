﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.Patients
{
    public class FoddTypeModel
    {
        public int Id { get; set; }
        public int AnimalKindId { get; set; }
        public string Value { get; set; }

    }
}
