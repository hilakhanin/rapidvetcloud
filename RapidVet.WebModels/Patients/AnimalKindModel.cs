﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace RapidVet.WebModels.Patients
{
    public class AnimalKindModel
    {
        [Display(ResourceType = typeof(RapidVet.Resources.Global), Name = "Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Controllers.AnimalKindController), Name = "Value")]
        public string Value { get; set; }

        public int ClinicGroupId { get; set; }

        public ICollection<AnimalRaceModel> AnimalRaces { get; set; }

         [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public int AnimalIconId { get; set; }

         public string AnimalIconFileName { get; set; }

        ////the Clinic group that this animalKind is attached to
        //[Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "ClinicGroup")]
        //[ForeignKey("ClinicGroupId")]
        //virtual public ClinicGroup ClinicGroup { get; set; }
    }

}