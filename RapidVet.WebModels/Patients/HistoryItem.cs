﻿using System;

namespace RapidVet.WebModels.Patients
{
    public class HistoryItem
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public string DateToShortTime
        {
            get { return Date.ToShortTimeString(); }
        }

        public string Name { get; set; }

        public string ItemType { get; set; }

        public string HandlingDoctor { get; set; }

        public string DaysFromTreatment { get; set; }

        public decimal? Sum { get; set; }

        public string Color { get; set; }

        public string Url { get; set; }

        public bool? Active { get; set; }
    }
}