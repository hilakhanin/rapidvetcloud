﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.WebModels.Clients;

namespace RapidVet.WebModels.Patients
{
    public class PatientHistoryIndexModel
    {
        public ClientDetailsModel Client { get; set; }
        public List<HistoryDate> HistoryDates { get; set; }        
    }
}
