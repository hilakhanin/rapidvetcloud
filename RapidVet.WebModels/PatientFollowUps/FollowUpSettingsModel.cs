﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.PatientFollowUps
{
    public class FollowUpSettingsModel
    {
        [UIHint("_FollowUpTimeRange")]
        public int FollowUpTimeRangeId { get; set; }

        public int FollowUpPast { get; set; }

        public int FollowUpFuture { get; set; }

          [UIHint("_FollowUpTimeRange")]
        public int DefaultFollowUpTypeId { get; set; }

        public int DefaultFollowUpTime { get; set; }

        public bool ShowFollowUpsInPatientHistory { get; set; }

        public bool AutomaticallySetFollowUpsAfterVisit { get; set; }
    }
}
