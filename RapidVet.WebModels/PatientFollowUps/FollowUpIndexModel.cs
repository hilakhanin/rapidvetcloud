﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.WebModels.PatientFollowUps
{
   public class FollowUpIndexModel
    {

       public int FollowUpId { get; set; }
       public string Created { get; set; }
       public int ClientId { get; set; }
       public string ClientName { get; set; }
       public string ClientTitle { get; set; }
       public string PatientName { get; set; }
       public string PatientKind { get; set; }
       public string PatientGender { get; set; }
       public string Address { get; set; }
       public string HomePhone { get; set; }
       public string HomePhoneComment { get; set; }
       public string CellPhone { get; set; }
       public string CellPhoneComment { get; set; }
       public string Email { get; set; }
       public string Doctors { get; set; }
       public int DoctorId { get; set; }
       public string Comment { get; set; }
       public string NextAppointment { get; set; }
       public string DateTime { get; set; }
    }
}
