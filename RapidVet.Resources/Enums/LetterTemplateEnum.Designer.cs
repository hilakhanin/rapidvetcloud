﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RapidVet.Resources.Enums {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class LetterTemplateEnum {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LetterTemplateEnum() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("RapidVet.Resources.Enums.LetterTemplateEnum", typeof(LetterTemplateEnum).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to אישור בריאות - אנגלית.
        /// </summary>
        public static string BillOfHealthEn {
            get {
                return ResourceManager.GetString("BillOfHealthEn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to אישור בריאות - עברית.
        /// </summary>
        public static string BillOfHealthHe {
            get {
                return ResourceManager.GetString("BillOfHealthHe", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to רשיון לאחזקת כלב - תקנות חדשות.
        /// </summary>
        public static string DogOwnerLicense {
            get {
                return ResourceManager.GetString("DogOwnerLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to סיכום ניתוח.
        /// </summary>
        public static string MedicalProcedureSummery {
            get {
                return ResourceManager.GetString("MedicalProcedureSummery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to דיווח למשרד החקלאות.
        /// </summary>
        public static string MinistryOfAgricultureReport {
            get {
                return ResourceManager.GetString("MinistryOfAgricultureReport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מכתב שחרור.
        /// </summary>
        public static string PatientDischargePaper {
            get {
                return ResourceManager.GetString("PatientDischargePaper", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מכתב אישי.
        /// </summary>
        public static string PersonalLetter {
            get {
                return ResourceManager.GetString("PersonalLetter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to אישור חיסון כלבת.
        /// </summary>
        public static string RabiesVaccineCertificate {
            get {
                return ResourceManager.GetString("RabiesVaccineCertificate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to עיקור / סירוס.
        /// </summary>
        public static string Sterilization {
            get {
                return ResourceManager.GetString("Sterilization", resourceCulture);
            }
        }
    }
}
