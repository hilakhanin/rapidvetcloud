﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RapidVet.Resources.Controllers {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class UsersController {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UsersController() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("RapidVet.Resources.Controllers.UsersController", typeof(UsersController).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מנהל קבוצת מרפאות.
        /// </summary>
        public static string ClinicGroupManager {
            get {
                return ResourceManager.GetString("ClinicGroupManager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to משתמשים.
        /// </summary>
        public static string ClinicGroupUsers {
            get {
                return ResourceManager.GetString("ClinicGroupUsers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מרפאה.
        /// </summary>
        public static string ClinicName {
            get {
                return ResourceManager.GetString("ClinicName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to במסך זה ניתן ליצור מנהל מערכת בלבד. ליצירת משתמש לקבוצת מרפאות יש להיכנס למסך המרפאה..
        /// </summary>
        public static string CreateAdminUserMessage {
            get {
                return ResourceManager.GetString("CreateAdminUserMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to יצירת משתמש.
        /// </summary>
        public static string CreateUser {
            get {
                return ResourceManager.GetString("CreateUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to סמן הכל כלא פעילים.
        /// </summary>
        public static string DeleteAllUsers {
            get {
                return ResourceManager.GetString("DeleteAllUsers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to סמן כלא פעיל.
        /// </summary>
        public static string DeleteUser {
            get {
                return ResourceManager.GetString("DeleteUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הרשאות עבור.
        /// </summary>
        public static string EditPermissionCaption {
            get {
                return ResourceManager.GetString("EditPermissionCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to עריכת פרטים.
        /// </summary>
        public static string EditUserDetails {
            get {
                return ResourceManager.GetString("EditUserDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הרשאות.
        /// </summary>
        public static string EditUserRoles {
            get {
                return ResourceManager.GetString("EditUserRoles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מכסת המשתמשים נוצלה במלואה.
        /// </summary>
        public static string FullQuota {
            get {
                return ResourceManager.GetString("FullQuota", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to לא ניתן ליצור משתמשים נוספים. לפרטים נוספים פנו לתמיכה.
        /// </summary>
        public static string NoMoreUsersForYou {
            get {
                return ResourceManager.GetString("NoMoreUsersForYou", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to שחזר.
        /// </summary>
        public static string Reactivate {
            get {
                return ResourceManager.GetString("Reactivate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to שחזר כל המשתמשים.
        /// </summary>
        public static string ReactivateAllUsers {
            get {
                return ResourceManager.GetString("ReactivateAllUsers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to שחזור משתמש.
        /// </summary>
        public static string ReactivateUser {
            get {
                return ResourceManager.GetString("ReactivateUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הרשאות.
        /// </summary>
        public static string UserClinicRoles {
            get {
                return ResourceManager.GetString("UserClinicRoles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הרשאות צפייה בדוחות פיננסיים.
        /// </summary>
        public static string UserIssuerPermissionsCaption {
            get {
                return ResourceManager.GetString("UserIssuerPermissionsCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to יש לתת הרשאה אחת לפחות או להפוך את המשתמש ללא פעיל..
        /// </summary>
        public static string UserRolesNoPermissionSelectedError {
            get {
                return ResourceManager.GetString("UserRolesNoPermissionSelectedError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to משתמש.
        /// </summary>
        public static string Value {
            get {
                return ResourceManager.GetString("Value", resourceCulture);
            }
        }
    }
}
