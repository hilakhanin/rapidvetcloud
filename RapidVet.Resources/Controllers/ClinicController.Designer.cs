﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RapidVet.Resources.Controllers {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ClinicController {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ClinicController() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("RapidVet.Resources.Controllers.ClinicController", typeof(ClinicController).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to שחזור מרפאה.
        /// </summary>
        public static string ActivateClinic {
            get {
                return ResourceManager.GetString("ActivateClinic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הפוך משתמש למנפיק.
        /// </summary>
        public static string AddIssuer {
            get {
                return ResourceManager.GetString("AddIssuer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to משתמש {0} הוסף בהצלחה לרשימת המנפיקים.
        /// </summary>
        public static string AddIssuerSuccess {
            get {
                return ResourceManager.GetString("AddIssuerSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מרפאה לא נוצרה.
        /// </summary>
        public static string CreateFailure {
            get {
                return ResourceManager.GetString("CreateFailure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to צור מרפאה חדשה בקבוצת {0}.
        /// </summary>
        public static string CreateNewTitle {
            get {
                return ResourceManager.GetString("CreateNewTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מרפאה {0} נוצרה בהצלחה בקבוצת {1}.
        /// </summary>
        public static string CreateSuccess {
            get {
                return ResourceManager.GetString("CreateSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to המידע לא נשמר בהצלחה.
        /// </summary>
        public static string EditFailure {
            get {
                return ResourceManager.GetString("EditFailure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to המידע נשמר בהצלחה.
        /// </summary>
        public static string EditSuccess {
            get {
                return ResourceManager.GetString("EditSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הסרת מרפאה.
        /// </summary>
        public static string RemoveClinic {
            get {
                return ResourceManager.GetString("RemoveClinic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to האם את/ה בטוח/ה ששברצונך להעביר מרפאה זו לסל המחזור?.
        /// </summary>
        public static string RemoveClinicWarning {
            get {
                return ResourceManager.GetString("RemoveClinicWarning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הסר מרשימת מנפיקים.
        /// </summary>
        public static string RemoveIssuer {
            get {
                return ResourceManager.GetString("RemoveIssuer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to משתמש {0} הוסר בהצלחה לרשימת המנפיקים.
        /// </summary>
        public static string RemoveIssuerSuccess {
            get {
                return ResourceManager.GetString("RemoveIssuerSuccess", resourceCulture);
            }
        }
    }
}
