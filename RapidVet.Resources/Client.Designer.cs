﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RapidVet.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Client {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Client() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("RapidVet.Resources.Client", typeof(Client).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to לקוח פעיל.
        /// </summary>
        public static string Active {
            get {
                return ResourceManager.GetString("Active", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הוספת כתובת.
        /// </summary>
        public static string AddAddress {
            get {
                return ResourceManager.GetString("AddAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הוספת דוא&quot;ל.
        /// </summary>
        public static string AddEmail {
            get {
                return ResourceManager.GetString("AddEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הוספת חיה ללקוח זה.
        /// </summary>
        public static string AddPatient {
            get {
                return ResourceManager.GetString("AddPatient", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הוספת טלפון.
        /// </summary>
        public static string AddPhone {
            get {
                return ResourceManager.GetString("AddPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to כתובת.
        /// </summary>
        public static string Address {
            get {
                return ResourceManager.GetString("Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to כתובות.
        /// </summary>
        public static string Addresses {
            get {
                return ResourceManager.GetString("Addresses", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to גיל.
        /// </summary>
        public static string Age {
            get {
                return ResourceManager.GetString("Age", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מאשר/ת הצגת טלפון באינטרנט.
        /// </summary>
        public static string ApproveDisplayPhone {
            get {
                return ResourceManager.GetString("ApproveDisplayPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מאשר/ת העברת דוא&quot;ל לרשויות.
        /// </summary>
        public static string ApproveEmailTranfer {
            get {
                return ResourceManager.GetString("ApproveEmailTranfer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to תאריך לידה.
        /// </summary>
        public static string BirthDate {
            get {
                return ResourceManager.GetString("BirthDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to עיר.
        /// </summary>
        public static string City {
            get {
                return ResourceManager.GetString("City", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to קיים חוב ללקוח.
        /// </summary>
        public static string ClientInDebtPopupMessage {
            get {
                return ResourceManager.GetString("ClientInDebtPopupMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to סטטוס לקוח.
        /// </summary>
        public static string ClientStatus {
            get {
                return ResourceManager.GetString("ClientStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to לא פעיל.
        /// </summary>
        public static string ClientStatusInactive {
            get {
                return ResourceManager.GetString("ClientStatusInactive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מרפאת אם.
        /// </summary>
        public static string Clinic {
            get {
                return ResourceManager.GetString("Clinic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ת.קליטה.
        /// </summary>
        public static string CreatedDate {
            get {
                return ResourceManager.GetString("CreatedDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to עריכת פרטי לקוח.
        /// </summary>
        public static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to דוא&quot;ל.
        /// </summary>
        public static string Emails {
            get {
                return ResourceManager.GetString("Emails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to וטרינר חיצוני.
        /// </summary>
        public static string ExternalVet {
            get {
                return ResourceManager.GetString("ExternalVet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to שם פרטי.
        /// </summary>
        public static string FirstName {
            get {
                return ResourceManager.GetString("FirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הערה נסתרת.
        /// </summary>
        public static string HiddenComment {
            get {
                return ResourceManager.GetString("HiddenComment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ת.ז.
        /// </summary>
        public static string IdCard {
            get {
                return ResourceManager.GetString("IdCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to (לא פעיל).
        /// </summary>
        public static string InActive {
            get {
                return ResourceManager.GetString("InActive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to שם משפחה.
        /// </summary>
        public static string LastName {
            get {
                return ResourceManager.GetString("LastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מספר תיק.
        /// </summary>
        public static string LocalId {
            get {
                return ResourceManager.GetString("LocalId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to לא קיימות חיות פעילות עבור הלקוח.
        /// </summary>
        public static string NoPatients {
            get {
                return ResourceManager.GetString("NoPatients", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to --לא נבחר סטטוס--.
        /// </summary>
        public static string NoStatus {
            get {
                return ResourceManager.GetString("NoStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to לא צוין.
        /// </summary>
        public static string NotMentioned {
            get {
                return ResourceManager.GetString("NotMentioned", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to חיות.
        /// </summary>
        public static string Patients {
            get {
                return ResourceManager.GetString("Patients", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to טלפונים.
        /// </summary>
        public static string Phones {
            get {
                return ResourceManager.GetString("Phones", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to עיסוק.
        /// </summary>
        public static string Practice {
            get {
                return ResourceManager.GetString("Practice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הופנה על-ידי.
        /// </summary>
        public static string ReferredBy {
            get {
                return ResourceManager.GetString("ReferredBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to תואר.
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to רופא מטפל.
        /// </summary>
        public static string Vet {
            get {
                return ResourceManager.GetString("Vet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הערה גלויה.
        /// </summary>
        public static string VisibleComment {
            get {
                return ResourceManager.GetString("VisibleComment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מקום עבודה.
        /// </summary>
        public static string WorkePlace {
            get {
                return ResourceManager.GetString("WorkePlace", resourceCulture);
            }
        }
    }
}
