﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34003
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RapidVet.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Patient {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Patient() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("RapidVet.Resources.Patient", typeof(Patient).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הוסף הערה.
        /// </summary>
        public static string AddComment {
            get {
                return ResourceManager.GetString("AddComment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to גיל.
        /// </summary>
        public static string Age {
            get {
                return ResourceManager.GetString("Age", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to רגישויות.
        /// </summary>
        public static string Allergies {
            get {
                return ResourceManager.GetString("Allergies", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to צבע.
        /// </summary>
        public static string AnimalColor {
            get {
                return ResourceManager.GetString("AnimalColor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to חיה מסוכנת לחיות אחרות.
        /// </summary>
        public static string AnimalDangerous {
            get {
                return ResourceManager.GetString("AnimalDangerous", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to סוג.
        /// </summary>
        public static string AnimalKind {
            get {
                return ResourceManager.GetString("AnimalKind", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to גזע.
        /// </summary>
        public static string AnimalRace {
            get {
                return ResourceManager.GetString("AnimalRace", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to תאריך לידה.
        /// </summary>
        public static string BirthDate {
            get {
                return ResourceManager.GetString("BirthDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to סוג דם.
        /// </summary>
        public static string BloodType {
            get {
                return ResourceManager.GetString("BloodType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הערות.
        /// </summary>
        public static string Comments {
            get {
                return ResourceManager.GetString("Comments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to עריכת פרטי חיה.
        /// </summary>
        public static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מספר אלקטרוני.
        /// </summary>
        public static string ElectronicNumber {
            get {
                return ResourceManager.GetString("ElectronicNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to פטור.
        /// </summary>
        public static string Exemption {
            get {
                return ResourceManager.GetString("Exemption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to סיבת פטור.
        /// </summary>
        public static string ExemptionCause {
            get {
                return ResourceManager.GetString("ExemptionCause", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to הפרייה.
        /// </summary>
        public static string Fertilization {
            get {
                return ResourceManager.GetString("Fertilization", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to כמות מזון.
        /// </summary>
        public static string FoodAmount {
            get {
                return ResourceManager.GetString("FoodAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to סוג מזון.
        /// </summary>
        public static string FoodType {
            get {
                return ResourceManager.GetString("FoodType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מין.
        /// </summary>
        public static string Gender {
            get {
                return ResourceManager.GetString("Gender", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to חיה מסוכנת לבני אדם.
        /// </summary>
        public static string HumanDangerous {
            get {
                return ResourceManager.GetString("HumanDangerous", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מספר רשיון.
        /// </summary>
        public static string LicenseNumber {
            get {
                return ResourceManager.GetString("LicenseNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to שם.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to בעלים / חווה.
        /// </summary>
        public static string Owner_Farm {
            get {
                return ResourceManager.GetString("Owner_Farm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to גזעי.
        /// </summary>
        public static string PureBred {
            get {
                return ResourceManager.GetString("PureBred", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to מספר ס.ג.י.ר.
        /// </summary>
        public static string SAGIRNumber {
            get {
                return ResourceManager.GetString("SAGIRNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to כלב נחיה.
        /// </summary>
        public static string SeeingEyeDog {
            get {
                return ResourceManager.GetString("SeeingEyeDog", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to יש לבחור סוג חיה.
        /// </summary>
        public static string SelectKind {
            get {
                return ResourceManager.GetString("SelectKind", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to עיקור.
        /// </summary>
        public static string Sterilization {
            get {
                return ResourceManager.GetString("Sterilization", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to תאריך עיקור.
        /// </summary>
        public static string SterilizationDate {
            get {
                return ResourceManager.GetString("SterilizationDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to סימנים חיוניים.
        /// </summary>
        public static string VitalSigns {
            get {
                return ResourceManager.GetString("VitalSigns", resourceCulture);
            }
        }
    }
}
