﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Web;

namespace RapidVet.FilesAndStorage
{
    public static class PatientUploadHelper
    {
        private const string PROFILE_IMAGE_NAME_FORMAT = "{0}/ProfileImage.png";

        static string archivePath = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_PATH"];
        static string archiveDomain = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_DOMAIN"];
        static string archiveUser = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_USER"];
        static string archivePass = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_PASS"];

        /// <summary>
        /// uploads a patient profile image to clinic azure storage container
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <param name="patientLocalId">patient's local id</param>
        /// <param name="image">image to be uploaded</param>
        public static bool UploadPatientProfileImage(int clinicGroupId, int clinicId, int clientId ,int patientId, HttpPostedFileBase file)
        {
            bool uploaded = false;

            using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
            {
                if (unc.NetUseWithCredentials(archivePath, archiveUser, archiveDomain, archivePass))
                {

                    StringBuilder savePath = new StringBuilder(archivePath);
                    savePath.Append(@"\");
                    savePath.Append(clinicGroupId);
                    savePath.Append(@"\");
                    savePath.Append(clinicId);
                    savePath.Append(@"\");
                    savePath.Append(clientId);
                    savePath.Append(@"\");
                    savePath.Append(patientId);
                    savePath.Append(@"\");

                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(savePath.ToString());
                    fileInfo.Directory.Create(); // If the directory already exists, this method does nothing.
                    //delete old profile pic
                    string[] profileFile = Directory.GetFiles(savePath.ToString(), "profile.*");
                    savePath.Append("profile.jpg");

                    if (profileFile.Count() > 0)
                    {
                        File.Delete(savePath.ToString() + Path.GetExtension(profileFile[0]));
                    }

                    // savePath.Append(Path.GetExtension(file.FileName));

                    //file.SaveAs(savePath.ToString());

                    ImageResizer.ImageJob i = new ImageResizer.ImageJob(file, savePath.ToString(), new ImageResizer.ResizeSettings(
                    "MaxWidth=320;MaxHeight=200;quality=80;format=jpg;"));

                    i.Build();
                    unc.Dispose();
                    uploaded = true;
                }
            }

            return uploaded;
        }

        /// <summary>
        /// returns a patient profile image from azure storage container in memory stream
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <param name="patientLocalId">patient's local id</param>
        /// <returns>MemoryStream</returns>
        public static MemoryStream GetProfileImageStream(int clinicGroupId, int clinicId, int clientId, int patientId, out DateTime? lastModified)
        {
            MemoryStream stream = null;
            lastModified = null;

            using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
            {
                if (unc.NetUseWithCredentials(archivePath, archiveUser, archiveDomain, archivePass))
                {

                    StringBuilder profilePicPath = new StringBuilder(archivePath);
                    profilePicPath.Append(@"\");
                    profilePicPath.Append(clinicGroupId);
                    profilePicPath.Append(@"\");
                    profilePicPath.Append(clinicId);
                    profilePicPath.Append(@"\");
                    profilePicPath.Append(clientId);
                    profilePicPath.Append(@"\");
                    profilePicPath.Append(patientId);
                    profilePicPath.Append(@"\");

                    if (Directory.Exists(profilePicPath.ToString()))
                    {
                        string[] profileFile = Directory.GetFiles(profilePicPath.ToString(), "profile.*");

                        if (profileFile.Count() > 0)
                        {
                            profilePicPath.Append("profile");
                            profilePicPath.Append(Path.GetExtension(profileFile[0]));
                            lastModified = File.GetLastWriteTime(profilePicPath.ToString());
                            using (var imageStream = new FileStream(profilePicPath.ToString(), FileMode.Open))
                            {
                                stream = new MemoryStream();
                                imageStream.CopyTo(stream);
                                stream.Position = 0;
                                imageStream.Flush();
                                imageStream.Close();
                            }
                        }
                    }
                }

                unc.Dispose();
            }

            return stream;
           
        }

    }
}
