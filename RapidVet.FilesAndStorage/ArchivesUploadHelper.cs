﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.WindowsAzure.Storage.Blob;

namespace RapidVet.FilesAndStorage
{
    public static class ArchivesUploadHelper
    {
        static string archivePath = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_PATH"];
        static string archiveDomain = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_DOMAIN"];
        static string archiveUser = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_USER"];
        static string archivePass = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_PASS"];

        public static bool UploadDocument(int clinicGroupId, int clinicId, int clientId, int patientId, string filename, HttpPostedFileBase file, bool isAudio = false)
        {
            using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
            {
                if (unc.NetUseWithCredentials(archivePath, archiveUser, archiveDomain, archivePass))
                {
                    try
                    {
                        StringBuilder savePath = new StringBuilder(archivePath);
                        savePath.Append(@"\");
                        savePath.Append(clinicGroupId);
                        savePath.Append(@"\");
                        savePath.Append(clinicId);
                        savePath.Append(@"\");
                        savePath.Append(clientId);
                        savePath.Append(@"\");
                        savePath.Append(patientId);
                        savePath.Append(@"\");
                        savePath.Append(filename);

                        System.IO.FileInfo fileInfo = new System.IO.FileInfo(savePath.ToString());
                        fileInfo.Directory.Create(); // If the directory already exists, this method does nothing.

                        file.SaveAs(savePath.ToString());

                        return true;
                    }
                    catch (Exception ex)
                    {
                        var error = ex.Message;

                        return false;
                    }
                    finally
                    {
                        unc.Dispose();
                    }
                }
            }

            return false;
        }


        public static bool UploadProccessImage(int clinicGroupId, int clinicId, int clientId, int patientId, string filename, Stream stream)
        {
            using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
            {
                if (unc.NetUseWithCredentials(archivePath, archiveUser, archiveDomain, archivePass))
                {
                    try
                    {
                        StringBuilder savePath = new StringBuilder(archivePath);
                        savePath.Append(@"\");
                        savePath.Append(clinicGroupId);
                        savePath.Append(@"\");
                        savePath.Append(clinicId);
                        savePath.Append(@"\");
                        savePath.Append(clientId);
                        savePath.Append(@"\");
                        savePath.Append(patientId);
                        savePath.Append(@"\");
                        savePath.Append(filename);

                        System.IO.FileInfo fileInfo = new System.IO.FileInfo(savePath.ToString());
                        fileInfo.Directory.Create(); // If the directory already exists, this method does nothing.
                        stream.Seek(0, SeekOrigin.Begin);
                        var fileStream = new FileStream(savePath.ToString(), FileMode.Create, FileAccess.Write);
                        stream.CopyTo(fileStream);
                        fileStream.Dispose();
                        //  file.SaveAs(savePath.ToString());

                        return true;
                    }
                    catch (Exception ex)
                    {
                        var error = ex.Message;

                        return false;
                    }
                    finally
                    {
                        unc.Dispose();
                    }
                }
            }

            return false;
        }

        public static MemoryStream GetDocumentStream(int clinicGroupId, int clinicId, int clientId, int patientId, string filename, out DateTime? lastModified)
        {
            MemoryStream stream = null;
            lastModified = null;

            using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
            {
                if (unc.NetUseWithCredentials(archivePath, archiveUser, archiveDomain, archivePass))
                {
                    StringBuilder filePath = new StringBuilder(archivePath);
                    filePath.Append(@"\");
                    filePath.Append(clinicGroupId);
                    filePath.Append(@"\");
                    filePath.Append(clinicId);
                    filePath.Append(@"\");
                    filePath.Append(clientId);
                    filePath.Append(@"\");
                    filePath.Append(patientId);
                    filePath.Append(@"\");
                    if (Directory.Exists(filePath.ToString()))
                    {
                        filePath.Append(filename);

                        if (File.Exists(filePath.ToString()))
                        {
                            lastModified = File.GetLastWriteTime(filePath.ToString());
                            using (var imageStream = new FileStream(filePath.ToString(), FileMode.Open))
                            {
                                stream = new MemoryStream();
                                imageStream.CopyTo(stream);
                                stream.Position = 0;
                                imageStream.Flush();
                                imageStream.Close();
                            }
                        }
                    }

                    unc.Dispose();
                }
            }

            return stream;
        }

        public static void DeleteDocument(int clinicGroupId, int clinicId, int clientId, int patientId, string filename)
        {
            using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
            {
                if (unc.NetUseWithCredentials(archivePath, archiveUser, archiveDomain, archivePass))
                {
                    StringBuilder filePath = new StringBuilder(archivePath);
                    filePath.Append(@"\");
                    filePath.Append(clinicGroupId);
                    filePath.Append(@"\");
                    filePath.Append(clinicId);
                    filePath.Append(@"\");
                    filePath.Append(clientId);
                    filePath.Append(@"\");
                    filePath.Append(patientId);
                    filePath.Append(@"\");
                    filePath.Append(filename);

                    if (File.Exists(filePath.ToString()))
                    {
                        File.Delete(filePath.ToString());
                    }

                    unc.Dispose();
                }
            }
        }


        public static void MovePatientFiles(int clinicGroupId, int clinicId, int clientId, int newClientId, int patientId)
        {
            using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
            {
                if (unc.NetUseWithCredentials(archivePath, archiveUser, archiveDomain, archivePass))
                {
                    StringBuilder oldDirectoryPath = new StringBuilder(archivePath);
                    oldDirectoryPath.Append(@"\");
                    oldDirectoryPath.Append(clinicGroupId);
                    oldDirectoryPath.Append(@"\");
                    oldDirectoryPath.Append(clinicId);
                    oldDirectoryPath.Append(@"\");
                    oldDirectoryPath.Append(clientId);
                    oldDirectoryPath.Append(@"\");
                    oldDirectoryPath.Append(patientId);
                    System.IO.DirectoryInfo oldDirectoryInfo = new System.IO.DirectoryInfo(oldDirectoryPath.ToString());

                    if (oldDirectoryInfo.Exists)
                    {
                        StringBuilder newDirectoryPath = new StringBuilder(archivePath);
                        newDirectoryPath.Append(@"\");
                        newDirectoryPath.Append(clinicGroupId);
                        newDirectoryPath.Append(@"\");
                        newDirectoryPath.Append(clinicId);
                        newDirectoryPath.Append(@"\");
                        newDirectoryPath.Append(newClientId);
                        newDirectoryPath.Append(@"\");
                        newDirectoryPath.Append(patientId);
                        newDirectoryPath.Append(@"\");

                        System.IO.FileInfo newFileInfo = new System.IO.FileInfo(newDirectoryPath.ToString());
                        newFileInfo.Directory.Create(); // If the directory already exists, this method does nothing.

                        List<String> filesToMove = Directory
                       .GetFiles(oldDirectoryPath.ToString(), "*.*", SearchOption.AllDirectories).ToList();

                        foreach (string file in filesToMove)
                        {
                            FileInfo mFile = new FileInfo(file);
                            mFile.MoveTo(newDirectoryPath.ToString() + @"\" + mFile.Name);
                        }

                        oldDirectoryInfo.Delete();
                    }

                    unc.Dispose();
                }
            }
        }

        public static bool SaveExportFileInClinicGroupFolder(int clinicGroupId, int clinicId, string filename, string archivePath, Stream stream, bool zipFile)
        {
            if (!zipFile)
            {
                using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
                {
                    if (unc.NetUseWithCredentials(archivePath, archiveUser, archiveDomain, archivePass))
                    {
                        try
                        {
                            StringBuilder savePath = new StringBuilder(archivePath);

                            savePath.Append(@"\");
                            savePath.Append(clinicGroupId);
                            savePath.Append(@"\");
                            savePath.Append(clinicId);
                            savePath.Append(@"\");
                            savePath.Append(filename);

                            System.IO.FileInfo fileInfo = new System.IO.FileInfo(savePath.ToString());
                            fileInfo.Directory.Create(); // If the directory already exists, this method does nothing.
                            stream.Seek(0, SeekOrigin.Begin);
                            var fileStream = new FileStream(savePath.ToString(), FileMode.Create, FileAccess.Write);
                            stream.CopyTo(fileStream);
                            fileStream.Dispose();
                            //  file.SaveAs(savePath.ToString());

                            return true;
                        }
                        catch (Exception ex)
                        {
                            var error = ex.Message;

                            return false;
                        }
                        finally
                        {
                            unc.Dispose();
                        }

                    }
                }
            }
            else
            {
                try
                {
                    string appendPrefixForZipFile = "ClinicExportFiles";
                    StringBuilder savePath = new StringBuilder(archivePath);

                    savePath.Append(@"\");
                    savePath.Append(@"\" + appendPrefixForZipFile + @"\");
                    savePath.Append(clinicGroupId);
                    savePath.Append(@"\");
                    savePath.Append(clinicId);
                    savePath.Append(@"\");
                    savePath.Append(filename);

                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(savePath.ToString());
                    fileInfo.Directory.Create(); // If the directory already exists, this method does nothing.
                    stream.Seek(0, SeekOrigin.Begin);
                    var fileStream = new FileStream(savePath.ToString(), FileMode.Create, FileAccess.Write);
                    stream.CopyTo(fileStream);
                    fileStream.Dispose();
                  
                    return true;
                }
                catch (Exception ex)
                {
                    var error = ex.Message;

                    return false;
                }
            }
            return false;
        }

        public static void DeleteXmlFiles(int clinicGroupId, int clinicId, string archivePath)
        {
            if (archivePath != null)
            {
                StringBuilder directoryPath = new StringBuilder(archivePath);
                directoryPath.Append(@"\");
                directoryPath.Append(clinicGroupId);
                directoryPath.Append(@"\");
                directoryPath.Append(clinicId);
                directoryPath.Append(@"\");

                System.IO.FileInfo newFileInfo = new System.IO.FileInfo(directoryPath.ToString());
                newFileInfo.Directory.Create(); // If the directory already exists, this method does nothing.

                List<String> filesToDelete = Directory
               .GetFiles(directoryPath.ToString(), "*.xml", SearchOption.AllDirectories).ToList();

                foreach (string file in filesToDelete)
                {
                    File.Delete(file);
                }
            }

        }

        public static List<String> GetXmlFileList(int clinicGroupId, int clinicId, string archivePath)
        {
            using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
            {
                if (unc.NetUseWithCredentials(archivePath, archiveUser, archiveDomain, archivePass))
                {
                    StringBuilder directoryPath = new StringBuilder(archivePath);
                    directoryPath.Append(@"\");
                    directoryPath.Append(clinicGroupId);
                    directoryPath.Append(@"\");
                    directoryPath.Append(clinicId);
                    directoryPath.Append(@"\");

                    System.IO.FileInfo newFileInfo = new System.IO.FileInfo(directoryPath.ToString());
                    newFileInfo.Directory.Create(); // If the directory already exists, this method does nothing.

                    List<String> xmlFiles = Directory
                   .GetFiles(directoryPath.ToString(), "*.xml", SearchOption.TopDirectoryOnly).ToList();
                    unc.Dispose();
                    return xmlFiles;
                }
            }
            return null;
        }

        public static void DeleteClinicExportZipFiles(string archivePath)
        {
            using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
            {
                if (unc.NetUseWithCredentials(archivePath, archiveUser, archiveDomain, archivePass))
                {
                    System.IO.FileInfo newFileInfo = new System.IO.FileInfo(archivePath);
                    newFileInfo.Directory.Create(); // If the directory already exists, this method does nothing.

                    System.IO.DirectoryInfo directoryInfo = new System.IO.DirectoryInfo(archivePath);

                    if (directoryInfo.Exists)
                    {
                        List<String> filesToDelete = Directory
                       .GetFiles(archivePath, "*.zip", SearchOption.AllDirectories).ToList();

                        foreach (string file in filesToDelete)
                        {
                            if (File.GetCreationTime(file) <= DateTime.Now.AddDays(-7))
                            {
                                File.Delete(file);
                            }
                        }
                    }
                }

                unc.Dispose();
            }

        }

    }
}
