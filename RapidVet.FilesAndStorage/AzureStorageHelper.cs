﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace RapidVet.FilesAndStorage
{
    public static class AzureStorageHelper
    {
        private static CloudStorageAccount StorageAccount
        {
            get
            {
                string accessKey = System.Web.Configuration.WebConfigurationManager.AppSettings["AzureAccessKey"];
                string accountName = System.Web.Configuration.WebConfigurationManager.AppSettings["AzureStorageAccountName"];
                return
                    new CloudStorageAccount(
                        new StorageCredentials(accountName,
                                               accessKey), false);
            }
        }

        private static CloudBlobClient BlobClient
        {
            get
            {
                return StorageAccount.CreateCloudBlobClient();
            }
        }

        /// <summary>
        /// returns the azure container for clinic by id
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>CloudBlobContainer</returns>
        public static CloudBlobContainer GetClinicContainer(int clinicId)
        {
            string containerName = string.Format("clinic-{0}", clinicId);
            var container = BlobClient.GetContainerReference(containerName);
            container.CreateIfNotExists();
            return container;
        }

        /// <summary>
        /// returns logo azure container
        /// </summary>
        /// <returns>CloudBlobContainer</returns>
        public static CloudBlobContainer GetLogoContainer()
        {
            string containerName = "logos";
            var container = BlobClient.GetContainerReference(containerName);
            container.CreateIfNotExists();
            return container;
        }

        /// <summary>
        /// resizes images before storing in azure container
        /// </summary>
        /// <param name="imgToResize">the image to be resized</param>
        /// <param name="size">the desired size</param>
        /// <returns>Image</returns>
        public static Image ResizeImage(Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);


            Bitmap b = new Bitmap(destWidth, destHeight);
            using (Graphics g = Graphics.FromImage((Image)b))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            }

            return (Image)b;

        }
    }
}
