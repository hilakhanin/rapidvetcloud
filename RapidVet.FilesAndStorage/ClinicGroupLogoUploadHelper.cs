﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Web;

namespace RapidVet.FilesAndStorage
{
    public static class ClinicGroupLogoUploadHelper
    {
        private const string LOGO_NAME_FORMAT = "clinic-group-{0}.png";
        static string archivePath = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_PATH"];
        static string archiveDomain = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_DOMAIN"];
        static string archiveUser = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_USER"];
        static string archivePass = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_PASS"];
    
        /// <summary>
        /// uploads a clinic group logo
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <param name="image">the logo image to be uploaded</param>
        public static bool UploadClinicGroupLogo(int clinicGroupId, HttpPostedFileBase file)
        {
            bool uploaded = false;
            using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
            {
                if (unc.NetUseWithCredentials(archivePath, archiveUser, archiveDomain, archivePass))
                {
                    StringBuilder savePath = new StringBuilder(archivePath);
                    savePath.Append(@"\");
                    savePath.Append(clinicGroupId);
                    savePath.Append(@"\");

                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(savePath.ToString());
                    fileInfo.Directory.Create(); // If the directory already exists, this method does nothing.

                    string[] logoFile = Directory.GetFiles(savePath.ToString(), "logo.*");

                    savePath.Append("logo");
                    //delete old profile pic
                    if (logoFile.Count() > 0)
                    {
                        File.Delete(savePath.ToString() + Path.GetExtension(logoFile[0]));
                    }

                    savePath.Append(Path.GetExtension(file.FileName));

                    file.SaveAs(savePath.ToString());
                    unc.Dispose();
                    uploaded = true;
                }               
            }

            return uploaded;
        }
        /// <summary>
        /// gets clinic group logo on stream
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <returns>MemoryStream</returns>
        public static MemoryStream GetClinicGroupLogoStream(int clinicGroupId, out DateTime? lastModified)
        {
            MemoryStream stream = null;
            lastModified = null;

            using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
            {
                if (unc.NetUseWithCredentials(archivePath, archiveUser, archiveDomain, archivePass))
                {
                    StringBuilder logoPath = new StringBuilder(archivePath);
                    logoPath.Append(@"\");
                    logoPath.Append(clinicGroupId);
                    logoPath.Append(@"\");

                    if (Directory.Exists(logoPath.ToString()))
                    {
                        string[] profileFile = Directory.GetFiles(logoPath.ToString(), "logo.*");

                        if (profileFile.Count() > 0)
                        {
                            logoPath.Append("logo");
                            logoPath.Append(Path.GetExtension(profileFile[0]));
                            lastModified = File.GetLastWriteTime(logoPath.ToString());
                            using (var imageStream = new FileStream(logoPath.ToString(), FileMode.Open))
                            {
                                stream = new MemoryStream();
                                imageStream.CopyTo(stream);
                                stream.Position = 0;
                                imageStream.Flush();
                                imageStream.Close();
                            }
                            // stream = new FileStream(profilePicPath.ToString() + "logo" + Path.GetExtension(profileFile[0]), FileMode.Open);
                        }
                    }

                    unc.Dispose();
                }
            }

            return stream;           
        }

    }
}
