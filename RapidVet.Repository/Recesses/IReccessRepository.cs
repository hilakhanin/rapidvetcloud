﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.FullCalender;

namespace RapidVet.Repository.Recesses
{
    public interface IReccessRepository : IDisposable
    {
        /// <summary>
        /// get recesses of user
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="clinicId"></param>
        /// <returns>List(Room)</returns>
        List<Recess> GetRecessesForUser(int userId, int clinicId);
        
        /// <summary>
        /// create recess for user
        /// </summary>
        /// <param name="recess">recess object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Recess recess);

        /// <summary>
        /// return a recess of a specific id
        /// </summary>
        /// <param name="recessId">id of recess object</param>
        /// <returns>recess</returns>
        Recess GetRecess(int recessId);

        /// <summary>
        /// updates recess in user
        /// </summary>
        /// <param name="recess">recess object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(Recess recess);

        /// <summary>
        /// deletes recess
        /// </summary>
        /// <param name="recessId">id of recess</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Remove(int recessId);

        List<Recess> GetRecessesOfUserByDay(int docId, DayOfWeek dayOfWeek);
    }
}