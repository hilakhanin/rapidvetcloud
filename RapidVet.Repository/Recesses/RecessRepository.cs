﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.FullCalender;
using RapidVet.Repository.Holidays;

namespace RapidVet.Repository.Recesses
{
    public class RecessRepository : RepositoryBase<RapidVetDataContext>, IReccessRepository
    {
        public RecessRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public RecessRepository()
        {

        }

        public List<Recess> GetRecessesForUser(int userId, int clinicId)
        {
            return DataContext.Recesses.Where(h => h.UserId == userId && h.ClinicId == clinicId).ToList();
        }


        public OperationStatus Create(Recess recess)
        {
            DataContext.Recesses.Add(recess);
            return base.Save(recess);
        }

        public Recess GetRecess(int recessId)
        {
            return DataContext.Recesses.SingleOrDefault(h => h.Id == recessId);
        }

        public List<Recess> GetRecessesOfUserByDay(int docId, DayOfWeek dayOfWeek)
        {
            var day = (int)dayOfWeek;
            return DataContext.Recesses.Where(h => h.UserId == docId && h.DayId == day).ToList();
        }

        public OperationStatus Update(Recess recess)
        {
            return base.Save(recess);
        }

        public OperationStatus Remove(int recessId)
        {
            var recess = DataContext.Recesses.SingleOrDefault(h => h.Id == recessId);
            if (recess != null)
            {
                DataContext.Recesses.Remove(recess);
                return base.Save(recess);
            }
            return new OperationStatus() { Success = false };
        }
    }
}
