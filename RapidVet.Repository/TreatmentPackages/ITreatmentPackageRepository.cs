﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.TreatmentPackages;

namespace RapidVet.Repository.TreatmentPackages
{
    public interface ITreatmentPackageRepository : IDisposable
    {
        /// <summary>
        /// get all treatment packages for clinic
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <returns> List<TreatmentPackage></returns>
        List<TreatmentPackage> GetAllTreatmentPackages(int clinicId);

        /// <summary>
        /// create new treatment package
        /// </summary>
        /// <param name="treatmentPackage">TreatmentPackage obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(TreatmentPackage treatmentPackage);

        /// <summary>
        /// updates treatment package
        /// </summary>
        /// <param name="treatmentPackage">TreatmentPackage obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Save(TreatmentPackage treatmentPackage);

        /// <summary>
        /// updates treatment package
        /// </summary>
        /// <param name="packageId">treatment package id</param>
        /// <param name="packageItems">list of package items</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Save(int packageId, List<PackageItem> packageItems);

        /// <summary>
        /// get treatment package
        /// </summary>
        /// <param name="id"> treatment package id</param>
        /// <returns>TreatmentPackage obj</returns>
        TreatmentPackage GetPackage(int id);
    }
}
