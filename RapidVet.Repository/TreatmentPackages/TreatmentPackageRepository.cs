﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Model;
using RapidVet.Model.TreatmentPackages;

namespace RapidVet.Repository.TreatmentPackages
{
    public class TreatmentPackageRepository : RepositoryBase<RapidVetDataContext>, ITreatmentPackageRepository
    {
        public TreatmentPackageRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public TreatmentPackageRepository()
        {

        }

        public List<TreatmentPackage> GetAllTreatmentPackages(int clinicId)
        {
            return DataContext.TreatmentPackages.Where(tp => tp.ClinicId == clinicId).ToList();
        }

        public OperationStatus Create(TreatmentPackage treatmentPackage)
        {
            DataContext.TreatmentPackages.Add(treatmentPackage);
            return base.Save(treatmentPackage);
        }

        public OperationStatus Save(TreatmentPackage treatmentPackage)
        {
            return base.Save(treatmentPackage);
        }


        public OperationStatus Save(int packageId, List<PackageItem> packageItems)
        {
            var status = new OperationStatus() { Success = true };
            var dbItems = DataContext.PackageItems.Where(pi => pi.TreatmentPackageId == packageId);

            var counter = 0;

            foreach (var item in packageItems.Where(pi => pi.Id == 0 || pi.TreatmentPackageId != packageId))
            {
                item.TreatmentPackageId = packageId;
                item.Id = 0;
                DataContext.PackageItems.Add(item);
                counter++;
            }

            foreach (var item in dbItems.ToList())
            {

                var toRemove = packageItems.SingleOrDefault(pi => pi.Id == item.Id) == null;
                if (toRemove)
                {
                    var packageItem = dbItems.Single(dil => dil.Id == item.Id);
                    DataContext.PackageItems.Remove(packageItem);
                    counter++;
                }
                else
                {
                    var source = packageItems.Single(pi => pi.Id == item.Id);
                    var destination = dbItems.Single(pi => pi.Id == item.Id);

                    if (destination.Price != source.Price || destination.Amount != source.Amount || destination.Comments != source.Comments)
                    {
                        destination.Price = source.Price;
                        destination.Amount = source.Amount;
                        destination.Comments = source.Comments;
                        counter++;
                    }

                }
            }

            if (counter > 0)
            {
                status.Success = DataContext.SaveChanges() > -1;
            }

            return status;
        }

        public TreatmentPackage GetPackage(int id)
        {
            return
                DataContext.TreatmentPackages.Include(tp => tp.Treatments)
                           .Include("Treatments.PriceListItem")
                           .Include("Treatments.PriceListItem.PriceListItemType")
                           .Single(tp => tp.Id == id);
        }
    }
}
