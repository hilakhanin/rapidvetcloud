using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using System.Web.Mvc;

namespace RapidVet.Repository.Clinics
{
    public interface ITelephoneDirectoryRepository : IDisposable
    {
        /// <summary>
        /// gets all telephone directories from clinic
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <returns>IQueryable(TelephoneDirectory)</returns>
        IQueryable<TelephoneDirectory> GetList(int clinicId);


        /// <summary>
        /// get a single telephone direcotry by id
        /// </summary>
        /// <param name="telephoneDirectoryId">telephone directory id</param>
        /// <returns>TelephoneDIrectory</returns>
        TelephoneDirectory GetItem(int telephoneDirectoryId);

        /// <summary>
        /// update a telephone directory object, save to db
        /// </summary>
        /// <param name="telephoneDirectory"> telephoneDirectory object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(TelephoneDirectory telephoneDirectory);

        /// <summary>
        /// create and save a telephone directory object in db
        /// </summary>
        /// <param name="telephoneDirectory">telephoneDirectory object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(TelephoneDirectory telephoneDirectory);

        /// <summary>
        /// removes a phone direcotry from clinic
        /// </summary>
        /// <param name="telephoneDirectoryId">telephoneDirectoryId to remove</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(int telephoneDirectoryId);

        OperationStatus CreateDirectoryType(TelephoneDirectoryType directoryType);

        TelephoneDirectoryType GetDirectoryType(int typeId);

        IQueryable<TelephoneDirectoryType> GetDirectoryTypes(int clinicId);

        List<SelectListItem> GetDropdownList(int clinicId);
    }
}