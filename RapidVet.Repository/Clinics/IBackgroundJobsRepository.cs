using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using System.Web.Mvc;
using RapidVet.Model.Tools;
using RapidVet.Model.Finance;

namespace RapidVet.Repository.Clinics
{
    public interface IBackgroundJobsRepository : IDisposable
    {
        OperationStatus Create(ClinicBackgroundJob backgroundJob);
        OperationStatus Update(ClinicBackgroundJob model);
        void Cancel(string jobId, int userId);
        ClinicBackgroundJob GetBackgroundJobById(string jobId);
        ClinicBackgroundJob GetBackgroundJobByGuid(string jobGuid);
        List<ClinicBackgroundJob> GetBackgroundJobsByClinicId(int clinicId, bool showFinishedJobs = false);
    }
}