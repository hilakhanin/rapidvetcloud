﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.ClinicGroups;
using System.Globalization;
using RapidVet.WebModels.ClinicGroups;
using RapidVet.Enums;

namespace RapidVet.Repository.Clinics
{
    public class TemplateKeyWordsRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, ITemplateKeyWordsRepository
    {

        public TemplateKeyWordsRepository()
        {

        }

        public TemplateKeyWordsRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }



        public List<TemplateKeyWords> GetAllTemplateKeyWords(string culture)
        {
            return DataContext.TemplateKeyWords.Where(t => t.Culture.Equals(culture)).ToList();
        }


        public List<TemplateKeyWordsWebModel> GetLetterTemplateKeyWord(LetterTemplateType type)
        {
            List<string> commonKeywords = new List<string>(){ "ClinicAddress", "ClinicName", "ClinicPhone", "ClinicFax", "ClinicEmail",
                                                        "ClinicLogo", "ClientName", "ClientIdCard", "ClientHomePhone", "ClientMobilePhone",
                                                        "ClientAddress", "PatientName", "PatientAnimalKind", "PatientRace", "PatientColor",
                                                        "PatientSex", "Sterilization", "PatientChip", "PatientBirthDate"};
            List<string> keywords = null;

            switch (type)
            {
                case LetterTemplateType.Sterilization:
                    keywords = new List<string>(){ "SterilizationAction", "Date", "RegionalCoucil", "PatientFeatures", 
                                                        "Copies", "Comment", "ActiveDoctorLicence", "ActiveDoctorName"
                                                 };
                    break;
                case LetterTemplateType.BillOfHealthHe:
                case LetterTemplateType.BillOfHealthEn:
                    keywords = new List<string>() { "DoctorName", "ActiveDoctorLicence", "Date", "LastRabiesVaccineDate" };
                    break;
                case LetterTemplateType.DogOwnerLicense:
                    keywords = new List<string>() { "DodOwnerFormYear", "Date", "PatientDangerous", "SeeingEyeDog", "LastRabiesVaccineDate",
                                                    "LastRabiesVaccineDoctor", "LastRabiesVaccindDoctorLicense", "PatientFeatures", "DogOwnerFormPayThrough", 
                                                    "DogOwnerFormTollAmount", "DogOwnerFormNumber"};
                    break;
                case LetterTemplateType.RabiesVaccineCertificate:
                    keywords = new List<string>() { "ClientCity", "ClientStreetAndNumber", "ClientBirthdate", "PatientSagirNumber", "LastRabiesVaccineDate", 
                                                    "LastRabiesBatchNumber", "ActiveDoctorName", "ActiveDoctorLicence"};

                    break;
                case LetterTemplateType.MedicalProcedureSummery:
                    keywords = new List<string>() { "Date", "MedicalProcedureDiagnosis", "MedicalProcedureName", "MedicalProcedureDate", "MedicalProcedureStartTime",
                                                    "MedicalProcedureEndTime", "MedicalProcedureType", "MedicalProcedureLocation", "MedicalProcedureSurgeonName", 
                                                    "MedicalProcedureAnasthesiologistName", "MedicalProcedureNurseName", "MedicalProcedureAdditionalPersonnel", "Summery" 
                                                  };

                    break;
                case LetterTemplateType.PatientDischargePaper:
                    keywords = new List<string>() { "DischargePaperFrom", "Date", "DischargePaperTo", "Summery", "DischargePaperBackground",
                                                    "DischargePaperRecommendations", "DischargePaperCheckup", "PatientAgeYears", "PatientAgeMonths", 
                                                    "DoctorName"
                                                  };
                    break;
                case LetterTemplateType.PersonalLetter:
                    keywords = new List<string>() { "ActiveDoctorName", "Date", "DoctorName", "ClinicAddress",
                                                        "ClinicPhone", "ClinicFax", "ClientName", "ClientLastName", "ClientFirstName",
                                                        "ClientReferer", "PatientName", "PatientAnimalKind", "PatientRace",
                                                        "PatientColor", "PatientAge", "PatientSex", "ClientBalance", "ClientAge",
                                                        "ClientTitle", "ClientIdCardNumber", "ClientAddress", "ActiveDoctorLicence", "ClientCity", 
                                                        "ClientZip", "ClientHomePhone", "ClientMobilePhone", "ClientBirthdate",
                                                        "ClientStreetAndNumber", "ClientRecordNumber", "ClientIdCard"
                                                      };
                    break;
            }

            keywords.AddRange(commonKeywords);

            var model = GetAllTemplateKeyWords(CultureInfo.CurrentCulture.Name)
                       .Select(t => new TemplateKeyWordsWebModel { TemplateKeyWord = t.TemplateKeyWord, KeyWord = t.KeyWord, KeyWordComment = t.Comment }).ToList();

            model = model.Where(w => keywords.Contains(w.TemplateKeyWord)).OrderBy(t => t.KeyWord).ToList();

            return model;
        }

    }
}
