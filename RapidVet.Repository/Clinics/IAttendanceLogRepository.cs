﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clinics;

namespace RapidVet.Repository.Clinics
{
    public interface IAttendanceLogRepository : IDisposable
    {
        AttendanceLog GetTodayLastLogIn(int clinicId, int userId);
        DateTime? GetLastRecordedLogOut(int clinicId, int userId);
        DateTime? GetLastRecordedLogIn(int clinicId, int userId);

        OperationStatus CreateNewLog(int clinicId, int userId, DateTime? startTime, DateTime? endTime);

        List<AttendanceLog> GetAttendanceLogsOfUser(int clinicId, int userId, DateTime fromTime, DateTime toTime);

        List<AttendanceLog> GetAllUsersAttendanceLogsOfClinic(int clinicId, DateTime fromTime, DateTime toTime);

        bool SaveLogReport(int clinicId, int id, int userId, DateTime startDate, TimeSpan startTime, TimeSpan endTime);

        bool DeleteLogReport(int clinicId, int id, int userId);

        AttendanceLog GetItem(int clinicId, int id);
    }
}
