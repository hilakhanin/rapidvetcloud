﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Clinics;

namespace RapidVet.Repository.Clinics
{
    public class DefaultCardCompaniesRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IDefaultCardCompaniesRepository
    {

        public DefaultCardCompaniesRepository()
        {

        }

        public DefaultCardCompaniesRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// creates default card company in db
        /// </summary>
        /// <param name="defaultCardCompany">DefaultCardCompany object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus AddDefaultCardCompany(DefaultCardCompanies defaultCardCompany)
        {
            DataContext.DefaultCardCompanies.Add(defaultCardCompany);
            return base.Save(defaultCardCompany);
        }

        /// <summary>
        /// creates a new default Card Company in db
        /// </summary>
        /// <param name="defaultCardCompany">DefaultCardCompanies object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Create(DefaultCardCompanies defaultCardCompany)
        {
            DataContext.DefaultCardCompanies.Add(defaultCardCompany);
            OperationStatus status = base.Save(defaultCardCompany);
            if (status.Success)
            {
                status.Message = defaultCardCompany.Name + " " + defaultCardCompany.Localization;
            }

            return status;
        }

        /// <summary>
        /// returns a default Card Company object
        /// </summary>
        /// <param name="defaultCardCompanyId">id in issuers table</param>
        /// <returns>DefaultCardCompanies</returns>
        public DefaultCardCompanies GetItem(int defaultCardCompanyId)
        {
            return DataContext.DefaultCardCompanies.SingleOrDefault(i => i.Id == defaultCardCompanyId && i.Active);
        }

        /// <summary>
        /// returns a list of all active default Card Companies
        /// </summary>
        /// <returns>List<DefaultCardCompanies></returns>
        public List<DefaultCardCompanies> GetAllDefaultCardCompanies()
        {
            return DataContext.DefaultCardCompanies.Where(i => i.Active).ToList();
        }


        /// <summary>
        /// updates a default Card Company in db
        /// </summary>
        /// <param name="defaultCardCompany"></param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(DefaultCardCompanies defaultCardCompany)
        {
            return base.Save(defaultCardCompany);
        }

        /// <summary>
        /// makes Card Company not active in DefaultCardCompanies table
        /// </summary>
        /// <param name="id">id in defaultCardCompany table</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus RemoveDefaultCardCompany(int id)
        {
            var defaultCardCompany = DataContext.DefaultCardCompanies.Single(c => c.Id == id);
            defaultCardCompany.Active = false;
            return base.Update(defaultCardCompany);

        }     
    }
}
