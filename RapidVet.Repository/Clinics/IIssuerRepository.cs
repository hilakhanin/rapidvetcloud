using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;

namespace RapidVet.Repository.Clinics
{
    public interface IIssuerRepository : IDisposable
    {
        /// <summary>
        /// creates issuer in db
        /// </summary>
        /// <param name="issuer">issuer object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus AddIssuer(Issuer issuer);

        /// <summary>
        /// returns an initiated issuer object
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>Issuer</returns>
        Issuer GetCreateModel(int clinicId);

        /// <summary>
        ///  all issuers in clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <param name="userID">user id</param>
        /// <returns>clinic object</returns>
        List<Issuer> GetList(int clinicId, int userID);

        /// <summary>
        /// creates anew issure in db
        /// </summary>
        /// <param name="issuer">issuer object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Issuer issuer);

        /// <summary>
        /// returns an issuer object
        /// </summary>
        /// <param name="issuerId">id in issuers table</param>
        /// <returns>Issuer</returns>
        Issuer GetItem(int issuerId);

        /// <summary>
        /// updates an issuer in db
        /// </summary>
        /// <param name="issuer"></param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(Issuer issuer);

        /// <summary>
        /// makes issuer not active in issuers table
        /// </summary>
        /// <param name="id">id in issuers table</param>
        /// <returns>OperationStatus</returns>
        OperationStatus RemoveIssuer(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clinicId"></param>
        /// <param name="showInactiveIssuers"></param>
        /// <param name="userID">for permision, -1 will ignore</param>
        /// <returns></returns>
        List<SelectListItem> GetDropdownList(int clinicId, int userID, bool showInactiveIssuers = false);

        /// <summary>
        /// get credit types for issuer
        /// </summary>
        /// <param name="issuerId">int issuer id</param>
        /// <returns>IQueryable<IssuerCreditType></returns>
        IQueryable<IssuerCreditType> GetCreditTypes(int issuerId);

        /// <summary>
        /// get easycard terminals for issuer
        /// </summary>
        /// <param name="issuerId">int issuer id</param>
        /// <returns>IQueryable<EasyCardSettings></returns>
        IQueryable<EasyCardSettings> GetEasyCardSettings(int issuerId);

        /// <summary>
        /// creates new obj of type EasyCardSettings
        /// </summary>
        /// <param name="terminal">EasyCardSettings obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus CreateEasyCardTerminal(EasyCardSettings terminal);

        /// <summary>
        /// gets issuer's EasyCard terminals
        /// </summary>
        /// <param name="issuerId">int issuerId</param>
        /// <returns>List<EasyCardSettings></returns>
        List<EasyCardSettings> GetIssuersActiveEasyCardTerminals(int issuerId);

        /// <summary>
        /// gets issuer's EasyCard default terminals
        /// </summary>
        /// <param name="issuerId">int issuerId</param>
        /// <returns>EasyCardSettings</returns>
        EasyCardSettings GetIssuersDefaultEasyCardTerminal(int issuerId);
        /// <summary>
        /// get easycard terminal
        /// </summary>
        /// <param name="terminalId">int terminal id</param>
        /// <returns>EasyCardSettings</returns>
        EasyCardSettings GetEasyCardTerminal(int terminalId);

        /// <summary>
        /// creates new obj of type IssuerCreditType
        /// </summary>
        /// <param name="creditType">IssuerCreditType obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus CreateCreditType(IssuerCreditType creditType);

        /// <summary>
        /// return specific IssuerCreditType obj
        /// </summary>
        /// <param name="issuerCreditTypeId">int IssuerCreditType id</param>
        /// <returns>IssuerCreditType</returns>
        IssuerCreditType GetIssuerCreditType(int issuerCreditTypeId);

        /// <summary>
        /// return specific IssuerCreditType obj
        /// </summary>
        /// <param name="creditType">string creditType</param>
        /// <returns>IssuerCreditType</returns>
        IssuerCreditType GetIssuerCreditTypeByName(string creditType);

        /// <summary>
        /// get list of user id's that default issuer id match
        /// </summary>
        /// <param name="issuerId">int issuer id</param>
        /// <returns>List<int></returns>
        List<int> GetUsersIdsByIssuerId(int issuerId);
        List<Issuer> GetListByClinicGroup(int clinicGroupId);
    }
}