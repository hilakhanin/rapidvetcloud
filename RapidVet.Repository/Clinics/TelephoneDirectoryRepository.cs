﻿using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using RapidVet.Repository;
using System.Data.Entity;
using System.Web.Mvc;

namespace RapidVet.Repository.Clinics
{
    public class TelephoneDirectoryRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, ITelephoneDirectoryRepository
    {

        public TelephoneDirectoryRepository()
        {

        }

        public TelephoneDirectoryRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }
        
        IQueryable<TelephoneDirectory> ITelephoneDirectoryRepository.GetList(int clinicId)
        {
            return DataContext.TelephoneDirectories.Include(t=>t.TelephoneDirectoryType).Where(td => td.ClinicId == clinicId);
        }

        TelephoneDirectory ITelephoneDirectoryRepository.GetItem(int telephoneDirectoryId)
        {
            return DataContext.TelephoneDirectories.SingleOrDefault(td => td.Id == telephoneDirectoryId);
        }

        public OperationStatus Update(TelephoneDirectory telephoneDirectory)
        {
            return base.Save(telephoneDirectory);
        }

        public OperationStatus Create(TelephoneDirectory telephoneDirectory)
        {
            DataContext.TelephoneDirectories.Add(telephoneDirectory);
            OperationStatus status = base.Save(telephoneDirectory);
            if (status.Success)
            {
                status.ItemId = telephoneDirectory.Id;
            }
            return status;
        }

        public OperationStatus Delete(int telephoneDirectoryId)
        {
            var telephoneDirectory = DataContext.TelephoneDirectories.SingleOrDefault(td => td.Id == telephoneDirectoryId);
            if (telephoneDirectory != null)
            {
                DataContext.TelephoneDirectories.Remove(telephoneDirectory);
                return base.Save(telephoneDirectory);
            }
            return new OperationStatus() {Success = false};
        }

        public OperationStatus CreateDirectoryType(TelephoneDirectoryType directoryType)
        {
            DataContext.TelephoneDirectoryTypes.Add(directoryType);
            return base.Save(directoryType);
        }

        public TelephoneDirectoryType GetDirectoryType(int typeId)
        {
            return DataContext.TelephoneDirectoryTypes.SingleOrDefault(i => i.Id == typeId);
        }

        public IQueryable<TelephoneDirectoryType> GetDirectoryTypes(int clinicId)
        {
            return DataContext.TelephoneDirectoryTypes.Where(i => i.ClinicId == clinicId && i.Active);
        }

        public List<SelectListItem> GetDropdownList(int clinicId)
        {
            var list = new List<SelectListItem>();
            var types = DataContext.TelephoneDirectoryTypes.Where(i => i.ClinicId == clinicId && i.Active);
            foreach (var dt in types)
            {
                list.Add(new SelectListItem()
                {
                    Value = dt.Id.ToString(),
                    Text = dt.Name
                });
            }
            return list;
        }
    }
}