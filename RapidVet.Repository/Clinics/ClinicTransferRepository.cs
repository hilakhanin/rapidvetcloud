﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;
using RapidVet.Repository;
using System.Data.Entity;
using RapidVet.Repository.Clients;

namespace RapidVet.Repository.Clinics
{
    public class ClinicTransferRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IClinicTransferRepository
    {

        public ClinicTransferRepository()
        {

        }

        public ClinicTransferRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        private int saveCounter = 0;

        public void SaveChanges()
        {
            if (saveCounter > 50)
            {
                DataContext.SaveChanges();
                DataContext.Dispose();
                DataContext = new RapidVetDataContext();
                saveCounter = 0;
            }
            else
            {
                saveCounter++;
            }
        }

        public OperationStatus MoveClinicFromClinicGroup(int clinicId, int targetClinicGroupId)
        {
            var status = new OperationStatus() { Success = false };

            var clinic = DataContext.Clinics.Single(c => c.Id == clinicId);
            var targetClinicGroup = DataContext.ClinicGroups.Single(c => c.Id == targetClinicGroupId);

            if (clinic != null && targetClinicGroup != null)
            {
                //Get all relevent objects
                var clients = DataContext.Clients.Include("ClientStatus").Where(c => c.ClinicId == clinicId).ToList();
                var patients = DataContext.Patients.Include("AnimalRace.AnimalKind").Include("AnimalColor").Include("Client").Where(p => p.Client.ClinicId == clinicId).ToList();
                var visitMedicines = DataContext.VisitMedications.Include("Medication").Where(m => m.MedicationId.HasValue && m.Medication.ClinicGroupId == clinic.ClinicGroupID).ToList();

                //ClientStatus
                foreach (var client in clients)
                {
                    var clientStatusDb = DataContext.ClientStatuses.FirstOrDefault(cs => cs.Name == client.ClientStatus.Name && cs.ClinicGroupId == targetClinicGroupId);
                    //Client status is available in target clinic group
                    if(clientStatusDb != null)
                    {
                        client.StatusId = clientStatusDb.Id;
                        SaveChanges();
                    }
                    //Client status not available, create one
                    else
                    {
                        var clientStatus = new ClientStatus()
                            {
                                Active = client.ClientStatus.Active,
                                ClinicGroupId = targetClinicGroupId,
                                Name = client.ClientStatus.Name,
                                MeetingColor = "#33cccc"
                            };
                        DataContext.ClientStatuses.Add(clientStatus);
                        SaveChanges();
                        client.StatusId = clientStatus.Id;
                    }
                }

                //AnimalKinds & AnimalRaces & AnimalColor
                foreach (var patient in patients)
                {
                    //AnimalColor is available in target clinic group
                    var animalColorDb = DataContext.AnimalColors.FirstOrDefault(ac => ac.Value == patient.AnimalColor.Value && ac.ClinicGroupId == targetClinicGroupId);
                    if (animalColorDb != null)
                    {
                        patient.AnimalColorId = animalColorDb.Id;
                        SaveChanges();
                    }
                    //AnimalColor not available, create one
                    else
                    {
                        var animalColor = new AnimalColor()
                        {
                            Active = patient.AnimalColor.Active,
                            ClinicGroupId = targetClinicGroupId,
                            Value = patient.AnimalColor.Value
                        };
                        DataContext.AnimalColors.Add(animalColor);
                        SaveChanges();
                        patient.AnimalColorId = animalColor.Id;
                    }

                    var animalKindDb = DataContext.AnimalKinds.Include("AnimalRaces")
                        .FirstOrDefault(ak => ak.AnimalRaces.Any(ar => ar.Value == patient.AnimalRace.Value && ak.Value == patient.AnimalRace.AnimalKind.Value && ak.ClinicGroupId == targetClinicGroupId));
                    //AnimalKind and AnimalRace is available in target clinic group
                    if (animalKindDb != null)
                    {
                        patient.AnimalRaceId = animalKindDb.AnimalRaces.First(ar => ar.Value == patient.AnimalRace.Value).Id;
                        SaveChanges();
                    }
                 
                    else
                    {
                        animalKindDb = DataContext.AnimalKinds.FirstOrDefault(ak => ak.Value == patient.AnimalRace.AnimalKind.Value && ak.ClinicGroupId == targetClinicGroupId);
                        //AnimalKind exists but AnimalRace doesn't exist
                        if (animalKindDb != null)
                        {
                            var animalKindTargetClinicGroupId = animalKindDb.Id;
                            var animalRace = new AnimalRace()
                                {
                                    Active = patient.AnimalRace.Active,
                                    Value = patient.AnimalRace.Value,
                                    AnimalKindId = animalKindTargetClinicGroupId
                                };
                            DataContext.AnimalRaces.Add(animalRace);
                            patient.AnimalRaceId = animalRace.Id;
                            SaveChanges();
                        }
                        //AnimalKind and AnimalRace not available, create both
                        else
                        {
                             var animalKind = new AnimalKind()
                        {
                            Active = patient.AnimalRace.AnimalKind.Active,
                            ClinicGroupId = targetClinicGroupId,
                            Value = patient.AnimalRace.AnimalKind.Value,
                            AnimalIconId = patient.AnimalRace.AnimalKind.AnimalIconId,
                            AnimalRaces = new Collection<AnimalRace>()
                                {
                                    new AnimalRace()
                                        {
                                            Active = patient.AnimalRace.Active,
                                            Value = patient.AnimalRace.Value,
                                        }
                                }
                        };
                        DataContext.AnimalKinds.Add(animalKind);
                        SaveChanges();
                        patient.AnimalRaceId = animalKind.AnimalRaces.First().Id;
                        }
                    }
                }

                //Medications
                foreach (var visitMedication in visitMedicines)
                {
                    var medicationDb = DataContext.Medications.Include("VisitMedications").FirstOrDefault(m => m.Name == visitMedication.Name && m.ClinicGroupId == targetClinicGroupId);
                    //Medication is available in target clinic group
                    if (medicationDb != null)
                    {
                        visitMedication.MedicationId = medicationDb.Id;
                        SaveChanges();
                    }
                    else
                    {
                        var medication = new Medication()
                            {
                                Active = visitMedication.Medication.Active,
                                ClinicGroupId = targetClinicGroupId,
                                Name = visitMedication.Medication.Name,
                                DangerousDrug = visitMedication.Medication.DangerousDrug,
                                DoctorInfo = visitMedication.Medication.DoctorInfo,
                                Dosage = visitMedication.Medication.Dosage,
                                Insturctions = visitMedication.Medication.Insturctions,
                                MedicationAdministrationTypeId = visitMedication.Medication.MedicationAdministrationTypeId,
                                NumberOfDays = visitMedication.Medication.NumberOfDays,
                                OldId = visitMedication.Medication.OldId,
                                TimesPerDay = visitMedication.Medication.TimesPerDay,
                                UnitPrice = visitMedication.Medication.UnitPrice,
                                Updated = visitMedication.Medication.Updated
                            };
                        DataContext.Medications.Add(medication);
                        SaveChanges();
                        visitMedication.MedicationId = medication.Id;
                    }
                }
               
                //Remove clinic roles
                var roles = DataContext.UsersClinicsRoles.Where(r => r.ClinicId == clinicId).ToList();
                foreach (var usersClinicsRolese in roles)
                {
                    DataContext.UsersClinicsRoles.Remove(usersClinicsRolese);
                    SaveChanges();
                }
                
                clinic.ClinicGroupID = targetClinicGroupId;
                status.Success = DataContext.SaveChanges() > 0;
                return status;
            }
            
            return status;
        }
    }
}