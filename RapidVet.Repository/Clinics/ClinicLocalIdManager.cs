﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Repository.Clinics
{
    public class ClinicLocalId
    {
        public ClinicLocalId(int clinicId, int? maxLocalClientId, int? maxLocalPatientId)
        {
            ClinicId = clinicId;
            _localClientId = maxLocalClientId ?? 1010;
            _localPatientId = maxLocalPatientId ?? 0;
        }

        public int ClinicId { get; private set; }

        private int _localClientId;

        public int LocalClientId
        {
            get
            {
                return ++_localClientId;
            }
        }


        private int _localPatientId;

        public int LocalPatientId { get { return ++_localPatientId; } }

    }

    // Implement singel tone pattern to save local ids number
    public class ClinicLocalIdManager
    {
        private static List<ClinicLocalId> _clinicLocalIds;

        static object loker = new object();

        public static List<ClinicLocalId> ClinicLocalIds
        {
            get
            {
                if (_clinicLocalIds == null)
                {
                    lock (loker)
                    {
                        if (_clinicLocalIds == null)
                        {
                            CreateLocalIdList();
                        }
                    }
                }

                return _clinicLocalIds;
            }
        }

        static ClinicLocalId getClinicLoaclId(int clinicId)
        {
            var clinicLocalId = ClinicLocalIds.SingleOrDefault(c => c.ClinicId == clinicId);

            //force regeneration of list
            if(clinicLocalId == null)
            {
                CreateLocalIdList();               
                clinicLocalId = ClinicLocalIds.Single(c => c.ClinicId == clinicId);
            }

            return clinicLocalId;
        }

        static public int GetNextLocalClientId(int clinicId)
        {
            return getClinicLoaclId(clinicId).LocalClientId;
        }

        static public int GetNextLocalPatientId(int clinicId)
        {
            return getClinicLoaclId(clinicId).LocalPatientId;
        }

        static void CreateLocalIdList()
        {
            var list = new List<ClinicLocalId>();

            using (var reposetory = new ClinicRepository())
            {


                foreach (var clinic in reposetory.GetList().ToList())
                {
                    var maxLocalClientId = reposetory.GetMaxLocalClientId(clinic.Id);
                    var maxLocalPatientId = reposetory.GetMaxLocalPatientId(clinic.Id);
                    
                    list.Add(new ClinicLocalId(clinic.Id, maxLocalClientId, maxLocalPatientId));
                }
            }

            _clinicLocalIds = list;
        }


    }
}
