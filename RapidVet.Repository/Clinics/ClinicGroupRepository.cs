﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web.Helpers;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;
using RapidVet.Repository;
using RapidVet.Model;
using System.Data.Entity;
using RapidVet.Model.Expenses;

namespace RapidVet.Repository.Clinics
{
    public class ClinicGroupRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IClinicGroupRepository
    {
        public ClinicGroupRepository()
        {

        }

        public ClinicGroupRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// return all clinic groups
        /// </summary>
        /// <returns>IQueryable(ClinicGroup)</returns>
        public IQueryable<Model.Clinics.ClinicGroup> GetAllActive()
        {
            return DataContext.ClinicGroups.Where(c => c.Active);
        }

        /// <summary>
        /// return all clinic groups
        /// </summary>
        /// <returns>IQueryable(ClinicGroup)</returns>
        public IEnumerable<RapidVet.WebModels.ClinicGroups.ClinicGroupEdit> GetAllNotDepended()
        {
            return DataContext.ClinicGroups.Select(x => new RapidVet.WebModels.ClinicGroups.ClinicGroupEdit()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
        }

        public IQueryable<Model.Clinics.ClinicGroup> GetAllActiveIncludeClinics()
        {
            return DataContext.ClinicGroups.Include("Clinics").Where(c => c.Active);
        }

        /// <summary>
        /// create a clinic group in db
        /// </summary>
        /// <param name="clinicGroup">clinicGroup object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Create(Model.Clinics.ClinicGroup clinicGroup)
        {
            var newClinic = new Clinic()
                {
                    Name = clinicGroup.Name,
                    Active = true,
                    CreatedDate = clinicGroup.CreatedDate,
                    CreatedById = clinicGroup.CreatedById,
                    Tariffs = new List<Tariff>()
                        {
                            new Tariff()
                                {
                                    Active = true,
                                    Name = Resources.ClinicGroup.DefaultTariffName
                                }
                        },
                    Diagnoses = new List<Diagnosis>()
                        {
                            new Diagnosis()
                                {
                                    Active = true,
                                    Name = Resources.ClinicGroup.DefaultDiagnosisName
                                }
                        },
                    ShowComplaint = true,
                    ShowExaminations = true,
                    ShowDiagnosis = true,
                    ShowTreatments = true,
                    ShowMedicins = true,
                    ShowTemperature = true,
                    ShowWeight = true,
                    ShowOldVisits = true,
                    ShowVisitNote = true,
                    ExpenseGroups = new List<ExpenseGroup>()
                    {
                        new ExpenseGroup()
                        {
                            Name =  RapidConsts.ExpensesConstants.EquipmentAndPermanentAssets,
                            IsTextDeductible = true,
                            RecognitionPercent = 100
                        }
                    }
                };
            
            newClinic.DefualtTariffId = newClinic.Tariffs.First().Id;

            clinicGroup.Clinics.Add(newClinic);


            clinicGroup.Users = new Collection<User>()
                {
                    new User()
                        {
                        Username = Resources.ClinicGroup.DefaultUserUesrName,//Guid.NewGuid().ToString(),
                        FirstName = Resources.ClinicGroup.DefaultUserFirstName,
                        LastName = string.Empty,
                        Password = Crypto.HashPassword(Resources.ClinicGroup.DefaultUserPassword),
                        IsApproved = true,
                        IsLockedOut = false,
                        CreateDate = DateTime.Now,
                        LastActivityDate = DateTime.Now,
                        PasswordFailuresSinceLastSuccess = 0,
                        LastPasswordFailureDate = DateTime.Now,
                        LastPasswordChangedDate = DateTime.Now,
                        LastLockoutDate = DateTime.Now,
                        LastLoginDate = DateTime.Now,
                        Active = true
                        }
                };

            DataContext.ClinicGroups.Add(clinicGroup);
            var status = base.Save(clinicGroup);
            if (status.Success)
            {
                newClinic.DefaultNewClientStatusId = clinicGroup.ClientStatuses.First().Id;
                status.ItemId = clinicGroup.Id;
                DataContext.SaveChanges();
            }

            return status;
        }

        /// <summary>
        /// return a clinicGroup
        /// </summary>
        /// <param name="id">id in clinicGroups table</param>
        /// <returns>ClinicGroup</returns>
        public Model.Clinics.ClinicGroup GetItem(int id)
        {
            var clinicGroup = DataContext.ClinicGroups.Include(c => c.Users)
                                         .Include(c => c.Clinics)
                                       //  .Include("Clinics.TaxRates")
                                         .Include(c => c.AnimalKinds)
                                         .Include(c => c.AnimalColors)
                                         .Single(c => c.Id == id);
            var dt = DateTime.Now;
            return clinicGroup;
        }


        /// <summary>
        /// updates a clinicGroup in db
        /// </summary>
        /// <param name="clinicGroup">clinicGroup object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(Model.Clinics.ClinicGroup clinicGroup)
        {
            //return base.Update(clinicGroup);
            return base.Save(clinicGroup);
        }

        /// <summary>
        /// returns all clinics in clinicGroup
        /// </summary>
        /// <param name="clinicGroupId">clinicGroup id</param>
        /// <returns>IQueryable(Clinic)</returns>
        public IQueryable<Clinic> GetClinicList(int clinicGroupId)
        {
            return DataContext.Clinics.Where(c => c.ClinicGroupID == clinicGroupId).OrderBy(c => c.Name);
        }

        public Clinic GetFirstClinic(int clinicGroupId)
        {
            return DataContext.Clinics.FirstOrDefault(c => c.ClinicGroupID == clinicGroupId && c.Active);
        }

        /// <summary>
        /// checks if the clinic group have any child clinics
        /// </summary>
        /// <param name="clinicGroupId">id of current clinic group</param>
        /// <returns>bool</returns>
        public bool ClinicGroupHasClinics(int clinicGroupId)
        {
            return DataContext.Clinics.Any(c => c.ClinicGroupID == clinicGroupId && c.Active);
        }

        public string SendEmailMessages(int clinicGroupId, List<MailMessage> messages)
        {
            var clinicGroup = DataContext.ClinicGroups.Single(c => c.Id == clinicGroupId);

            //var smtpClient = new SmtpClient(clinicGroup.EmailSmtp, clinicGroup.EmailPort.Value)
            //    {
            //        UseDefaultCredentials = false,
            //        Credentials = new NetworkCredential(clinicGroup.EmailUserName, clinicGroup.EmailPassword),
            //        EnableSsl = clinicGroup.IsSecuredSmtp
            //    };
            //var clinicGroupEmailAddress = new MailAddress(clinicGroup.EmailAddress);

            //foreach (var m in messages)
            //{
            //    m.From = clinicGroupEmailAddress;
            //}

            //return SendMessages(smtpClient, clinicGroup.EmailMaxRecipients, clinicGroup.EmailPauseInSeconds,
            //                    messages);
            return null;
        }

        private string SendMessages(SmtpClient smtpClient, int emailMaxRecipients, int emailPauseInSeconds, List<MailMessage> messages)
        {
            var sb = new StringBuilder();
            var sendingCounter = 0;
            var milisecondPause = emailPauseInSeconds * 1000;

            foreach (var m in messages)
            {
                try
                {
                    smtpClient.Send(m);
                    sendingCounter++;
                    Thread.Sleep(milisecondPause);
                }
                catch (SmtpFailedRecipientException ex)
                {
                    sb.AppendLine(ex.Message);
                    throw ex;
                }
                catch (SmtpException ex)
                {
                    sb.AppendLine(ex.Message);
                    throw ex;
                }
                catch (InvalidOperationException ex)
                {
                    sb.AppendLine(ex.Message);
                    throw ex;
                }
                catch (Exception ex)
                {
                    sb.AppendLine(ex.Message);
                    throw ex;
                }


            }

            return sb.ToString();
        }

        public List<User> GetClinicUsers(int groupId)
        {
            var clinic = DataContext.ClinicGroups.Include(c => c.Users).SingleOrDefault(c => c.Id == groupId);
            if (clinic != null)
            {
                return clinic.Users.ToList();
            }
            return null;
        }

        public IQueryable<User> GetClinicGroupUsers(int clinicGroupId)
        {
            return
                DataContext.Users.Include(u => u.UsersClinicsRoleses)
                           .Where(u => u.ClinicGroupId == clinicGroupId);

        }

        public string GetName(int clinicId)
        {
            var clinic = DataContext.Clinics.SingleOrDefault(c => c.Id == clinicId);
            return clinic != null ? clinic.Name : string.Empty;
        }

        public int GetId(int clinicId)
        {
            return DataContext.Clinics.Single(c => c.Id == clinicId).ClinicGroupID;
        }

        public List<ClinicGroup> GetEmailTestConfigurations(int clinicGroupid)
        {
            return DataContext.ClinicGroups.Where(g => g.Id == clinicGroupid).ToList();
        }
    }
}
