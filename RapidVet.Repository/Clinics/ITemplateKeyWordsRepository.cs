using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.ClinicGroups;
using RapidVet.Enums;
using RapidVet.WebModels.ClinicGroups;

namespace RapidVet.Repository.Clinics
{
    public interface ITemplateKeyWordsRepository : IDisposable
    {
        List<TemplateKeyWords> GetAllTemplateKeyWords(string culture);
        List<TemplateKeyWordsWebModel> GetLetterTemplateKeyWord(LetterTemplateType type);
    }
}