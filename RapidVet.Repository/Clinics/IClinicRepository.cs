using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;

namespace RapidVet.Repository.Clinics
{
    public interface IClinicRepository : IDisposable
    {
        /// <summary>
        /// gets all clinics in clinicGroup
        /// </summary>
        /// <param name="clinicGroupId"> clinic group id</param>
        /// <returns>IQueryable(Clinic)</returns>
        IQueryable<Clinic> GetList(int clinicGroupId);

        

        /// <summary>
        /// get a list of all clinics in the db
        /// </summary>
        /// <returns>IQueryable(Clinic)</returns>
        IQueryable<Clinic> GetList();

        /// <summary>
        /// get a list of all clinics in the db without any dependancy with clinicgroup
        /// </summary>
        /// <returns>IQueryable(Clinic)</returns>
        IQueryable<Clinic> GetClearList(int clinicGroupID);
        IQueryable<Clinic> GetAllActiveClinicsInGroup(int clinicGroupId);
        /// <summary>
        /// get a single clinic by id
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>Clinic</returns>
        Clinic GetItem(int clinicId);
        Clinic GetClinicByClientId(int clientId);
        Clinic GetClinicWithLabsData(int clinicId);
        /// <summary>
        /// update a clinic object, save to db
        /// </summary>
        /// <param name="clinic"> clinic object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(Clinic clinic);

        /// <summary>
        /// return a CreateModelObject for clinic
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <returns>Clinic object</returns>
        Clinic GetCreateModel(int clinicGroupId);

        /// <summary>
        /// create and save a clinic object in db
        /// </summary>
        /// <param name="clinic">clinic object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Clinic clinic);

        /// <summary>
        /// returns a user object
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns>User</returns>
        User GetUser(int userId);

            /// <summary>
        /// returns a user object
        /// </summary>
        /// <param name="username">user name</param>
        /// <returns>User</returns>
        User GetUserByUN(string username);

          /// <summary>
        /// returns a user object
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns>User</returns>
        User GetUserByID(int id);
        UsersClinicsRoles GetUsersClinicsRolesNO_DIARY(int UserID);
        /// <summary>
        /// removes a user from issuers list in clinic
        /// </summary>
        /// <param name="issuerId">id in issuers table</param>
        /// <returns>OperationStatus</returns>
        OperationStatus RemoveIssuer(int issuerId);


        /// <summary>
        /// returns clinic name
        /// </summary>
        /// <param name="id">clinic id</param>
        /// <returns>string</returns>
        string GetName(int id);

        /// <summary>
        /// returns a list of all doctors in clinic
        /// </summary>
        /// <param name="clinicId"></param>
        /// <returns>List(User) </returns>
        List<User> GetClinicDoctors(int clinicId);

        List<User> GetClinicDoctorsForSchedule(int clinicId, int activeUserID = -1, bool isPrivateCalendars = false);

        /// <summary>
        /// returns all patients in clinic
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <returns>   IQueryable(Patient)</returns>
        IQueryable<Model.Patients.Patient> GetClinicPatients(int clinicId);
        double GetClinicPatientsProfilePicSize(int clinicId);

        /// <summary>
        /// gets all disgnoses for clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns> List<Diagnosis></returns>
        List<Diagnosis> GetClinicDiagnoses(int clinicId);

        int GetClinicClientsCount(int clinicId);
        List<Client> GetClinicClients(int id);
        List<Client> GetClinicClientsForExport(int clinicId, int pageIndex, int pageSize);

        List<AdvancedPaymentsPercent> GetAllAdvancePaymentPercentChanges(int clinicId, int issuerId);
        List<TaxRate> GetAllTaxRateChanges(int clinicId);

        void UpdateAdvancePaymentPercent(AdvancedPaymentsPercent advancedPaymentsPercent);
        void UpdateTaxRate(TaxRate taxRate);

        List<VisitFilter> GetVisitFilters(int clinicId);
        VisitFilter GetVisitFilter(int visitFilterId);
        void AddVisitFilter(VisitFilter visitFilter);

        /// <summary>
        /// update all clients in clinic with patient names for identification purposes
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        void UpdateClientsSearchString(int clinicId);

        /// <summary>
        /// add tax rate to DB
        /// </summary>
        /// <param name="taxRate">TaxRate</param>
        /// <returns>OperationStatus</returns>
        OperationStatus AddTaxRate(TaxRate taxRate);

        /// <summary>
        /// add advanced payment percent to DB
        /// </summary>
        /// <param name="advancedPaymentsPercent">AdvancedPaymentsPercent</param>
        /// <returns>OperationStatus</returns>
        OperationStatus AddAdvancedPaymentPercent(AdvancedPaymentsPercent advancedPaymentsPercent);
        /// <summary>
        /// get clinic
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>Clinic</returns>
        Clinic GetClinic(int clinicId);

        /// <summary>
        /// get tax rate by id
        /// </summary>
        /// <param name="taxRateId">tax rate obj id</param>
        /// <returns>TaxRate</returns>
        TaxRate GetTaxRate(int taxRateId);

        /// <summary>
        /// get advanced payment percent by id
        /// </summary>
        /// <param name="percentId">advanced payment obj id</param>
        /// <returns>AdvancedPaymentsPercent</returns>
        AdvancedPaymentsPercent GetAdvancedPaymentPercent(int percentId);

        /// <summary>
        /// removes taxrate
        /// </summary>
        /// <param name="taxRateId">int taxRateId</param>
        /// <returns>OperationStatus</returns>
        OperationStatus RemoveTaxRate(int taxRateId);

        /// <summary>
        /// removes advanced payment percent
        /// </summary>
        /// <param name="percentId">int percentId</param>
        /// <returns>OperationStatus</returns>
        OperationStatus RemoveAdvancedPaymentPercent(int percentId);

        void Save(int userID, int clinicId, List<WebModels.Users.UsersSettingsShowUserModel> list, bool isPrivateCalendars = false);

        int GetNumberOfClinics();
    }
}