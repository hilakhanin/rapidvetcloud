﻿using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.PatientLabTests;
using RapidVet.Model.Users;
using RapidVet.Repository;
using System.Data.Entity;

namespace RapidVet.Repository.Clinics
{
    public class LabTestsRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, ILabTestsRepository
    {

        public LabTestsRepository()
        {

        }

        public LabTestsRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public LabTestTemplate GetLabTemplateByLabTestType(int labTestTypeId)
        {
            return DataContext.LabTestsTemplates.Single(t => t.LabTestTypeId == labTestTypeId);
        }

        public IQueryable<LabTestType> GetLabTestTypes(int clinicId)
        {
            return DataContext.LabTestTypes.Include(l => l.AnimalKinds).Where(l => l.ClinicId == clinicId);
        }

        public LabTestType GetItem(int id)
        {
            return DataContext.LabTestTypes.SingleOrDefault(l => l.Id == id);
        }

        public OperationStatus UpdateItem(LabTestType labTestType)
        {
            return base.Save(labTestType);
        }

        public OperationStatus AddItem(LabTestType labTestType)
        {
            DataContext.LabTestTypes.Add(labTestType);
            return base.Save(labTestType);
        }

        public OperationStatus RemoveItem(int id)
        {
            var labTestType = DataContext.LabTestTypes.SingleOrDefault(l => l.Id == id);
            if (labTestType != null)
            {
                DataContext.LabTestTypes.Remove(labTestType);
                return base.Save(labTestType);
            }
            return new OperationStatus() { Success = false };
        }

        public IQueryable<LabTestTemplate> GetLabTestTemplates(int id, int animalKindId)
        {
            return DataContext.LabTestsTemplates.Where(l => l.LabTestTypeId == id && l.AnimalKindId == animalKindId);
        }

        public IQueryable<LabTestTemplate> GetLabTestTemplatesByAnimalKindId(int animalKindId)
        {
            return DataContext.LabTestsTemplates.Where(l => l.AnimalKindId == animalKindId);
        }

        public LabTestTemplate GetTemplateItem(int labTestTemplateId)
        {
            return DataContext.LabTestsTemplates.SingleOrDefault(l => l.Id == labTestTemplateId);
        }

        public OperationStatus AddTemplateItem(LabTestTemplate labTestTemplate)
        {
            DataContext.LabTestsTemplates.Add(labTestTemplate);
            return base.Save(labTestTemplate);
        }

        public OperationStatus UpdateTemplateItem(LabTestTemplate labTestTemplate)
        {
            return base.Save(labTestTemplate);
        }

        public OperationStatus RemoveTemplateItem(int id)
        {
            var labTestTemplate = DataContext.LabTestsTemplates.SingleOrDefault(l => l.Id == id);
            if (labTestTemplate != null)
            {
                DataContext.LabTestsTemplates.Remove(labTestTemplate);
                return base.Save(labTestTemplate);
            }
            return new OperationStatus() { Success = false };
        }

        public List<LabTest> GetLabTestData(int labTestTemplateId)
        {
            return DataContext.LabTests.Where(lt => lt.LabTestTemplateId == labTestTemplateId).ToList();
        }

        public string GetTemplateItemName(int id)
        {
            var template =
                DataContext.LabTestsTemplates.Include(ltt => ltt.AnimalKind)
                           .Include(ltt => ltt.LabTestType)
                           .Single(ltt => ltt.Id == id);
            return string.Format("{0} - {1} - {2}", template.AnimalKind.Value, template.LabTestType.Name, template.Name);
        }

        public OperationStatus SaveLabTestData(int labTestTemplateId, List<LabTest> testData)
        {
            var labTestDataDb = DataContext.LabTests.Where(lt => lt.LabTestTemplateId == labTestTemplateId);
            var labTestDataDbList = labTestDataDb.ToList();
            var status = new OperationStatus() { Success = false };


            foreach (var data in testData.Where(td => td.Id == 0))
            {
                DataContext.LabTests.Add(data);
            }

            foreach (var lt in labTestDataDb)
            {
                var toRemove = testData.SingleOrDefault(td => td.Id == lt.Id) == null;
                if (toRemove)
                {
                    var dataToRemove = labTestDataDbList.Single(ltdl => ltdl.Id == lt.Id);
                    DataContext.LabTests.Remove(dataToRemove);
                }

                else
                {
                    var source = testData.SingleOrDefault(td => td.Id == lt.Id);
                    if (source != null)
                    {
                        lt.Name = source.Name;
                        lt.MaxValue = source.MaxValue;
                        lt.MinValue = source.MinValue;
                    }
                }
            }

            status.Success = DataContext.SaveChanges() > -1;

            return status;
        }

        public List<LabTestType> GetLabTestTypesForPatient(int patientId, int clinicId)
        {
            var animalKindId = GetAnimalKindIdFromPatientId(patientId);

            var labtestTypes =
                DataContext.LabTestTypes.Where(
                    ltt => ltt.ClinicId == clinicId && ltt.AnimalKinds.Any(ak => ak.AnimalKindId == animalKindId));
            return labtestTypes.OrderBy(ltt => ltt.Name).ToList();
        }

        public List<LabTestTemplate> GetLabTestTemplatesForPatient(int patientId, int labTestTypeId)
        {
            var animalKindId = GetAnimalKindIdFromPatientId(patientId);
            return
                DataContext.LabTestsTemplates.Where(
                    ltt => ltt.AnimalKindId == animalKindId && ltt.LabTestTypeId == labTestTypeId)
                           .OrderBy(ltt => ltt.Name)
                           .ToList();
        }

        public List<LabTestTemplate> GetLabTestTemplatesForKind(int animalKindId)
        {
            return
                DataContext.LabTestsTemplates.Include(ltt => ltt.LabTestType).Where(ltt => ltt.AnimalKindId == animalKindId)
                           .OrderBy(ltt => ltt.Name)
                           .ToList();
        }

        public int? GetLastTesId(int patientId)
        {
            int? result = null;
            var data =
                DataContext.PatientLabTests.Where(p => p.PatientId == patientId)
                           .OrderByDescending(p => p.Id)
                           .FirstOrDefault();

            if (data != null)
            {
                result = data.Id;
            }

            return result;

        }

        public PatientLabTest GetLastLabTest(int patientId)
        {
            return
                DataContext.PatientLabTests.Include(l => l.LabTestTemplate).Include(l => l.LabTestTemplate.LabTestType)
                           .Where(l => l.PatientId == patientId)
                           .OrderByDescending(l => l.Created)
                           .FirstOrDefault();
        }

        public int GetAnimalKindIdFromPatientId(int patientId)
        {
            return
                DataContext.Patients.Include(p => p.AnimalRace).Single(p => p.Id == patientId).AnimalRace.AnimalKindId;
        }

        public OperationStatus UpdateResult(int id, string result)
        {
            var f = DataContext.PatientLabTestResults.FirstOrDefault(x => x.Id == id);
            if (f != null)
                f.Result = result;

            return base.Save();
        }
    }
}