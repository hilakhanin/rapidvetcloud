﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.Model.Users;

namespace RapidVet.Repository.Clinics
{
    public interface ILetterTemplatesRepository : IDisposable
    {
        /// <summary>
        /// returns letter template, creates if not exist
        /// </summary>
        /// <param name="letterTemplateTypeId"> letter template type id</param>
        /// <param name="clinicId"> clinic id</param>
        /// <returns>LetterTemplate</returns>
        LetterTemplate GetOrCreateTemplate(int letterTemplateTypeId, int clinicId);
        LetterTemplate GetOrCreatePersonalTemplate(int letterTemplateId, int clinicId);
        /// <summary>
        /// get letter template
        /// </summary>
        /// <param name="id">lab template id</param>
        /// <returns>LetterTemplate</returns>
        LetterTemplate GetTemplate(int id);

        /// <summary>
        /// get formatted string for steriliaztion document
        /// </summary>
        /// <param name="patient"> patient object</param>
        /// <param name="date"> string date</param>
        /// <param name="action"> atring action</param>
        /// <param name="patientFeatures"> string patient features</param>
        /// <param name="copiesTo"> string copies to</param>
        /// <param name="comments"> string comments</param>
        /// <param name="doctorName"></param>
        /// <param name="doctorLicense"></param>
        /// <returns> string </returns>
        string GetFormattedSterilizationDocument(Patient patient, string date, string action, string patientFeatures, string copiesTo, string comments, string doctorName, string doctorLicense);

        /// <summary>
        /// et formatted string for bill of health document
        /// </summary>
        /// <param name="patient">patient document</param>
        /// <param name="language"> language enum</param>
        /// <param name="doctorName"> string doctor name</param>
        /// <param name="examinationDate">string examintaion date</param>
        /// <param name="doctorLicense"></param>
        /// <returns>string</returns>
        string GetFormattedBillOfHealthDocument(Patient patient, LanguageEnum language, string doctorName, string examinationDate, string doctorLicense);

        /// <summary>
        /// get formatted string for dog owner license document
        /// </summary>
        /// <param name="patient">patient object</param>
        /// <param name="date"> string date</param>
        /// <param name="licenseNumber">string license number</param>
        /// <param name="patientFeatures">string patient features</param>
        /// <param name="payThrough"> string pay through</param>
        /// <param name="toll"> decimal toll</param>
        /// <param name="isSeeingEyeDog">boo is seeing eye dog</param>
        /// <param name="year">license year</param>
        /// <returns>string</returns>
        string GetFormattedDogOwnerLicenseDocument(Patient patient, string date, string licenseNumber, string patientFeatures, string payThrough, string toll, bool isSeeingEyeDog, string year);

        /// <summary>
        /// get formatted string for rabies vaccine certificate document
        /// </summary>
        /// <param name="patient"> patient object</param>
        /// <returns>string</returns>
        string GetFormattedRabiesVaccineCertificateDocument(Patient patient);

        /// <summary>
        /// get formatted string for steriliaztion document
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="procedure"> medical procedure object</param>
        /// <returns>string</returns>
        string GetFormattedMedicalProcedureSummeryDocument(Patient patient, MedicalProcedure procedure);

        /// <summary>
        /// get formatted string for patient discharge document
        /// </summary>
        /// <param name="patient">patient object</param>
        /// <param name="dischargePaper">discharge paper object</param>
        /// <returns>string</returns>
        string GetFormattedPatientDischargePaperDocument(Patient patient, DischargePaper dischargePaper);

        /// <summary>
        /// get client latest phone  by type
        /// </summary>
        /// <param name="phones"> collection of all client phones</param>
        /// <param name="phoneType">phone type enum</param>
        /// <returns>string</returns>
        string GetClientPhone(ICollection<Phone> phones, PhoneTypeEnum phoneType);
        string GetClientPhoneComment(ICollection<Phone> phones, string phoneNumber);

        /// <summary>
        /// get client latest address
        /// </summary>
        /// <param name="addresses">collection of all client addresses</param>
        /// <returns> string</returns>
        string GetClientAddress(ICollection<Address> addresses);
        string GetClientFirstAddress(ICollection<Address> addresses);
        /// <summary>
        /// get client latest address
        /// </summary>
        /// <param name="addresses">collection of all client addresses</param>
        /// <returns> string</returns>
        string GetClientAddressForSticker(ICollection<Address> addresses);

        /// <summary>
        /// get client latest zip code
        /// </summary>
        /// <param name="addresses">collection of all client addresses</param>
        /// <returns> string</returns>
        string GetClientZipCode(ICollection<Address> addresses);
        string GetClientCity(ICollection<Address> addresses);
        string GetClientStreetAndNumber(ICollection<Address> addresses);

        /// <summary>
        /// get formatted string
        /// </summary>
        /// <param name="nvc">name value collection</param>
        /// <param name="template">template string</param>
        /// <returns>string</returns>
        string FormatString(NameValueCollection nvc, string template);

        /// <summary>
        /// get client email address
        /// </summary>
        /// <param name="emails">ICollection<Email> emails </param>
        /// <returns></returns>
        string GetClientEmail(ICollection<Email> emails);
        IQueryable<LetterTemplate> GetClinicPersonalLetters(int clinicId);
        OperationStatus DeletePersonalTemplate(int id);
        LetterTemplate GetPersonalTemplate(int id);
        bool IsPersonalLetterExist(int clinicId);
    }
}
