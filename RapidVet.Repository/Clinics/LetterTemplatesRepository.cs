﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Users;
using RapidVet.RapidConsts;
using RapidVet.Model.ClinicGroups;
using System.Globalization;

namespace RapidVet.Repository.Clinics
{
    public class LetterTemplatesRepository : RepositoryBase<RapidVetDataContext>, ILetterTemplatesRepository
    {
        public LetterTemplatesRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

            TemplateKeyWords = DataContext.TemplateKeyWords.Where(t => t.Culture.Equals(CultureInfo.CurrentCulture.Name)).ToList();
        }
        public LetterTemplatesRepository()
        {
            TemplateKeyWords = DataContext.TemplateKeyWords.Where(t => t.Culture.Equals(CultureInfo.CurrentCulture.Name)).ToList();
        }

        List<TemplateKeyWords> TemplateKeyWords;

        public LetterTemplate GetOrCreateTemplate(int letterTemplateTypeId, int clinicId)
        {
            LetterTemplate template;

            template =
                DataContext.LetterTemplates.FirstOrDefault(
                    lt => lt.ClinicId == clinicId && lt.LetterTemplateTypeId == letterTemplateTypeId);

            if (template == null)
            {
                if (FixedLetters.TemplateKeyWords == null || FixedLetters.TemplateKeyWords.Count == 0)
                {
                    FixedLetters.TemplateKeyWords = TemplateKeyWords;
                }

                template = new LetterTemplate()
                    {
                        ClinicId = clinicId,
                        LetterTemplateTypeId = letterTemplateTypeId
                    };

                if (letterTemplateTypeId == (int)LetterTemplateType.Sterilization)
                {
                    template.Content = FixedLetters.SterilizationLetterTemplate();
                    template.TemplateName = LetterTemplateType.Sterilization.ToString();
                }
                else if (letterTemplateTypeId == (int)LetterTemplateType.DogOwnerLicense)
                {
                    template.Content = FixedLetters.DogOwnerLicense();
                    template.TemplateName = LetterTemplateType.DogOwnerLicense.ToString();
                }
                else if (letterTemplateTypeId == (int)LetterTemplateType.MedicalProcedureSummery)
                {
                    template.Content = FixedLetters.MedicalProcedureSummery();
                    template.TemplateName = LetterTemplateType.MedicalProcedureSummery.ToString();
                }
                else if (letterTemplateTypeId == (int)LetterTemplateType.PatientDischargePaper)
                {
                    template.Content = FixedLetters.DischargePaper();
                    template.TemplateName = LetterTemplateType.PatientDischargePaper.ToString();
                }
                else if (letterTemplateTypeId == (int)LetterTemplateType.BillOfHealthEn)
                {
                    template.Content = FixedLetters.BillOfHealthEn();
                    template.TemplateName = LetterTemplateType.BillOfHealthEn.ToString();
                }
                else if (letterTemplateTypeId == (int)LetterTemplateType.BillOfHealthHe)
                {
                    template.Content = FixedLetters.BillOfHealthHe();
                    template.TemplateName = LetterTemplateType.BillOfHealthHe.ToString();
                }
                else if (letterTemplateTypeId == (int)LetterTemplateType.RabiesVaccineCertificate)
                {
                    template.Content = FixedLetters.RabiesPermit();
                    template.TemplateName = LetterTemplateType.RabiesVaccineCertificate.ToString();
                }
                else
                {
                    template.Content = FixedLetters.EmptyTemplate();
                }

                DataContext.LetterTemplates.Add(template);
                base.Save(template);
            }

            return template;
        }

        public LetterTemplate GetOrCreatePersonalTemplate(int letterTemplateId, int clinicId)
        {
            LetterTemplate template;

            if (letterTemplateId == 0)
            {
                template = new LetterTemplate()
                {
                    ClinicId = clinicId,
                    LetterTemplateTypeId = (int)LetterTemplateType.PersonalLetter
                };

                if (FixedLetters.TemplateKeyWords == null || FixedLetters.TemplateKeyWords.Count == 0)
                {
                    FixedLetters.TemplateKeyWords = TemplateKeyWords;
                }

                template.Content = "";// FixedLetters.EmptyTemplate();
                DataContext.LetterTemplates.Add(template);
                base.Save(template);
            }
            else
            {
                template =
                    DataContext.LetterTemplates.Single(
                        lt => lt.ClinicId == clinicId && lt.Id == letterTemplateId);
            }

            return template;
        }

        public LetterTemplate GetTemplate(int letterTemplateId)
        {
            return DataContext.LetterTemplates.Single(lt => lt.Id == letterTemplateId);
        }

        public string GetFormattedSterilizationDocument(Patient patient, string date, string action, string patientFeatures, string copiesTo, string comments, string doctorName, string doctorLicense)
        {
            var templateContent = GetTemplateContent(patient, LetterTemplateType.Sterilization);

            var nvc = GetCommonFieldsCollection(patient);

            nvc.Add(getTemplateKeyWord("SterilizationAction"), action);
            nvc.Add(getTemplateKeyWord("Date"), date);
            nvc.Add(getTemplateKeyWord("RegionalCoucil"), GetLocalAuthority(patient.Client.Addresses));
            nvc.Add(getTemplateKeyWord("PatientFeatures"), patientFeatures);
            nvc.Add(getTemplateKeyWord("Copies"), copiesTo);
            nvc.Add(getTemplateKeyWord("Comment"), comments);
            nvc.Add(getTemplateKeyWord("ActiveDoctorLicence"), doctorLicense);
            nvc.Add(getTemplateKeyWord("ActiveDoctorName"), doctorName);

            return FormatString(nvc, templateContent);
        }

        private string getTemplateKeyWord(string keyword)
        {
            return TemplateKeyWords.Single(t => t.TemplateKeyWord.Equals(keyword)).KeyWord;
        }


        public string GetFormattedBillOfHealthDocument(Patient patient, LanguageEnum language, string doctorName, string examinationDate, string doctorLicense)
        {
            var letterTemplateType = language == LanguageEnum.Hebrew
                                         ? LetterTemplateType.BillOfHealthHe
                                         : LetterTemplateType.BillOfHealthEn;

            var templateContent = GetTemplateContent(patient, letterTemplateType);

            var nvc = GetCommonFieldsCollection(patient);

            if (string.IsNullOrWhiteSpace(doctorLicense))
            {
                doctorLicense = "_________";
            }

            nvc.Add(getTemplateKeyWord("DoctorName"), doctorName);
            nvc.Add(getTemplateKeyWord("ActiveDoctorLicence"), doctorLicense);
            nvc.Add(getTemplateKeyWord("Date"), examinationDate);
            nvc.Add(getTemplateKeyWord("LastRabiesVaccineDate"), GetLastRabiesVaccineDate(patient.PreventiveMedicine));

            return FormatString(nvc, templateContent);
        }

        public string GetFormattedDogOwnerLicenseDocument(Patient patient, string date, string licenseNumber, string patientFeatures, string payThrough, string toll, bool isSeeingEyeDog, string year)
        {
            var templateContent = GetTemplateContent(patient, LetterTemplateType.DogOwnerLicense);

            var nvc = GetCommonFieldsCollection(patient);

            nvc.Add(getTemplateKeyWord("DodOwnerFormYear"), year);
            nvc.Add(getTemplateKeyWord("Date"), date);
            nvc.Add(getTemplateKeyWord("PatientDangerous"), IsPatientDangerous(patient));
            nvc.Add(getTemplateKeyWord("SeeingEyeDog"), isSeeingEyeDog ? Resources.Global.Yes : Resources.Global.No);
            nvc.Add(getTemplateKeyWord("LastRabiesVaccineDate"), GetLastRabiesVaccineDate(patient.PreventiveMedicine));
            nvc.Add(getTemplateKeyWord("LastRabiesVaccineDoctor"), GetLastRabiesVaccineDoctorName(patient.PreventiveMedicine));
            nvc.Add(getTemplateKeyWord("LastRabiesVaccindDoctorLicense"), GetLastRabiesVaccineDoctorLicense(patient.PreventiveMedicine));
            nvc.Add(getTemplateKeyWord("PatientFeatures"), patientFeatures);
            nvc.Add(getTemplateKeyWord("DogOwnerFormPayThrough"), payThrough);
            nvc.Add(getTemplateKeyWord("DogOwnerFormTollAmount"), toll.ToString());
            nvc.Add(getTemplateKeyWord("DogOwnerFormNumber"), licenseNumber);

            return FormatString(nvc, templateContent);

        }


        public string GetFormattedRabiesVaccineCertificateDocument(Patient patient)
        {
            var templateContent = GetTemplateContent(patient, LetterTemplateType.RabiesVaccineCertificate);
            var nvc = GetCommonFieldsCollection(patient);
            var city = patient.Client.Addresses.FirstOrDefault(p => p.City != null);
            var street = patient.Client.Addresses.FirstOrDefault(p => p.Street != null);
            nvc.Add(getTemplateKeyWord("ClientCity"), city != null ? city.City.Name : "");
            nvc.Add(getTemplateKeyWord("ClientStreetAndNumber"),  street != null ? street.Street : "");
            nvc.Add(getTemplateKeyWord("ClientBirthdate"), patient.Client.BirthDate.HasValue ? patient.Client.BirthDate.Value.ToString("dd/MM/yyyy") : "");
            nvc.Add(getTemplateKeyWord("PatientSagirNumber"), patient.SAGIRNumber);
            nvc.Add(getTemplateKeyWord("LastRabiesVaccineDate"), GetLastRabiesVaccineDate(patient.PreventiveMedicine));
            nvc.Add(getTemplateKeyWord("LastRabiesBatchNumber"), GetLastRabiesVaccineBatchNumber(patient.PreventiveMedicine));
            nvc.Add(getTemplateKeyWord("ActiveDoctorName"), GetLastRabiesVaccineDoctorName(patient.PreventiveMedicine));
            nvc.Add(getTemplateKeyWord("ActiveDoctorLicence"), GetLastRabiesVaccineDoctorLicense(patient.PreventiveMedicine));

            return FormatString(nvc, templateContent);
        }

        public string GetFormattedMedicalProcedureSummeryDocument(Patient patient, MedicalProcedure procedure)
        {
            var templateContent = GetTemplateContent(patient, LetterTemplateType.MedicalProcedureSummery);

            var nvc = GetCommonFieldsCollection(patient);

            nvc.Add(getTemplateKeyWord("Date"), DateTime.Now.ToShortDateString());
            nvc.Add(getTemplateKeyWord("MedicalProcedureDiagnosis"), procedure.Diagnosis.Name);
            nvc.Add(getTemplateKeyWord("MedicalProcedureName"), procedure.ProcedureName);
            nvc.Add(getTemplateKeyWord("MedicalProcedureDate"), procedure.Start.ToShortDateString());
            nvc.Add(getTemplateKeyWord("MedicalProcedureStartTime"), procedure.Start.ToShortTimeString());
            nvc.Add(getTemplateKeyWord("MedicalProcedureEndTime"), procedure.Finish.HasValue && procedure.Finish > DateTime.MinValue ? procedure.Finish.Value.ToShortTimeString() : string.Empty);
            nvc.Add(getTemplateKeyWord("MedicalProcedureType"), Resources.Enums.MedicalProcedureType.ResourceManager.GetString(procedure.ProcedureType.ToString()));
            nvc.Add(getTemplateKeyWord("MedicalProcedureLocation"), procedure.ProcedureLocation);
            nvc.Add(getTemplateKeyWord("MedicalProcedureSurgeonName"), procedure.SurgeonName);
            nvc.Add(getTemplateKeyWord("MedicalProcedureAnasthesiologistName"), procedure.AnesthesiologistName);
            nvc.Add(getTemplateKeyWord("MedicalProcedureNurseName"), procedure.NurseName);
            nvc.Add(getTemplateKeyWord("MedicalProcedureAdditionalPersonnel"), procedure.AdditionalPersonnel);
            nvc.Add(getTemplateKeyWord("Summery"), procedure.Summery);

            return FormatString(nvc, templateContent);
        }

        public string GetFormattedPatientDischargePaperDocument(Patient patient, DischargePaper dischargePaper)
        {
            var templateContent = GetTemplateContent(patient, LetterTemplateType.PatientDischargePaper);

            var nvc = GetCommonFieldsCollection(patient);

            nvc.Add(getTemplateKeyWord("DischargePaperFrom"), dischargePaper.From);
            nvc.Add(getTemplateKeyWord("Date"), dischargePaper.DateTime.ToShortDateString());
            nvc.Add(getTemplateKeyWord("DischargePaperTo"), dischargePaper.To);
            nvc.Add(getTemplateKeyWord("Summery"), dischargePaper.Summery);
            nvc.Add(getTemplateKeyWord("DischargePaperBackground"), dischargePaper.Background);
            nvc.Add(getTemplateKeyWord("DischargePaperRecommendations"), dischargePaper.Recommendations);
            nvc.Add(getTemplateKeyWord("DischargePaperCheckup"), dischargePaper.CheckUp.HasValue && dischargePaper.CheckUp.Value > DateTime.MinValue ? dischargePaper.CheckUp.Value.ToShortDateString() : string.Empty);
            nvc.Add(getTemplateKeyWord("PatientAgeYears"), PatientYearsOfAge(patient).ToString());
            nvc.Add(getTemplateKeyWord("PatientAgeMonths"), PatientMonthsOfAge(patient).ToString());
            nvc.Add(getTemplateKeyWord("DoctorName"), dischargePaper.From);

            return FormatString(nvc, templateContent);

        }


        //###############################
        //## utalitarian section start ##
        //###############################

        public string FormatString(NameValueCollection nvc, string template)
        {
            template = template.Replace("&lt;", "<");
            template = template.Replace("&gt;", ">");
            return nvc.AllKeys.Aggregate(template, (current, key) => current.Replace(key, nvc[key]));
        }

        public string GetClientEmail(ICollection<Email> emails)
        {
            var result = string.Empty;

            if (emails.Any() && emails.Count > 0)
            {
                result = emails.First().Name;
            }
            return result;
        }

        private int PatientYearsOfAge(Patient patient)
        {
            var result = 0;
            if (patient.BirthDate.HasValue && patient.BirthDate.Value > DateTime.MinValue)
            {
                var span = DateTime.Now - patient.BirthDate.Value;
                result = span.Days / 365;
            }
            return result;
        }

        private int PatientMonthsOfAge(Patient patient)
        {
            var result = 0;
            if (patient.BirthDate.HasValue && patient.BirthDate.Value > DateTime.MinValue)
            {
                var span = DateTime.Now - patient.BirthDate.Value;
                var years = (decimal)span.TotalDays / 365;
                var yearsInt = Math.Floor(years);
                result = (int)(Math.Ceiling(years - yearsInt));
            }
            return result;
        }


        private NameValueCollection GetCommonFieldsCollection(Patient patient)
        {
            var nvc = new NameValueCollection();

            nvc.Add(getTemplateKeyWord("ClinicAddress"), patient.Client.Clinic.Address);
            nvc.Add(getTemplateKeyWord("ClinicName"), patient.Client.Clinic.Name);
            nvc.Add(getTemplateKeyWord("ClinicPhone"), patient.Client.Clinic.Phone);
            nvc.Add(getTemplateKeyWord("ClinicFax"), patient.Client.Clinic.Fax);
            nvc.Add(getTemplateKeyWord("ClinicEmail"), patient.Client.Clinic.Email);
            nvc.Add(getTemplateKeyWord("ClinicLogo"), "<img src=\"/ClinicGroups/LogoImage\" style=\"max-height: 150px;max-width: 150px;\">");
            nvc.Add(getTemplateKeyWord("ClientName"), patient.Client.Name);
            nvc.Add(getTemplateKeyWord("ClientIdCard"), patient.Client.IdCard);
            nvc.Add(getTemplateKeyWord("ClientHomePhone"), GetClientPhone(patient.Client.Phones, PhoneTypeEnum.Home));
            nvc.Add(getTemplateKeyWord("ClientMobilePhone"), GetClientPhone(patient.Client.Phones, PhoneTypeEnum.Mobile));
            nvc.Add(getTemplateKeyWord("ClientAddress"), GetClientAddress(patient.Client.Addresses));
            nvc.Add(getTemplateKeyWord("PatientName"), patient.Name);
            nvc.Add(getTemplateKeyWord("PatientAnimalKind"), patient.AnimalRace.AnimalKind.Value);
            nvc.Add(getTemplateKeyWord("PatientRace"), patient.AnimalRace.Value);
            nvc.Add(getTemplateKeyWord("PatientColor"), patient.AnimalColor != null ? patient.AnimalColor.Value : "");
            nvc.Add(getTemplateKeyWord("PatientSex"), patient.GenderId == (int)GenderEnum.Female ? Resources.Global.Female : Resources.Global.Male);
            nvc.Add(getTemplateKeyWord("Sterilization"), GetPatientSterilization(patient));
            nvc.Add(getTemplateKeyWord("PatientChip"), patient.ElectronicNumber);
            nvc.Add(getTemplateKeyWord("PatientBirthDate"), patient.BirthDate.HasValue && patient.BirthDate > DateTime.MinValue ? patient.BirthDate.Value.ToShortDateString() : string.Empty);


            return nvc;
        }

        private string IsPatientDangerous(Patient patient)
        {
            var result = Resources.Global.No;
            if (patient.HumanDangerous || patient.AnimalDangerous)
            {
                result = Resources.Global.Yes;
            }
            return result;
        }

        private string GetPatientSterilization(Patient patient)
        {
            var result = Resources.Global.No;
            if (patient.Sterilization)
            {
                result = Resources.Global.Yes;
                if (patient.SterilizationDate.HasValue && patient.SterilizationDate.Value > DateTime.MinValue)
                {
                    result = patient.SterilizationDate.Value.ToShortDateString();
                }
            }

            return result;
        }

        private string GetLastRabiesVaccineDate(ICollection<PreventiveMedicineItem> preventiveMedicineItems)
        {
            var rabiesVaccine = GetLastRabiesVaccineItem(preventiveMedicineItems);
            return rabiesVaccine != null
                       ? rabiesVaccine.Preformed.HasValue && rabiesVaccine.Preformed.Value > DateTime.MinValue
                             ? rabiesVaccine.Preformed.Value.ToShortDateString()
                             : string.Empty
                       : string.Empty;
        }

        private string GetLastRabiesVaccineDoctorName(ICollection<PreventiveMedicineItem> preventiveMedicineItems)
        {
            var rabiesVaccine = GetLastRabiesVaccineItem(preventiveMedicineItems);
            var result = string.Empty;
            if (rabiesVaccine != null)
            {
                result = rabiesVaccine.ExternalyPreformed
                             ? rabiesVaccine.ExternalDrName
                             : rabiesVaccine.PreformingUser != null ? rabiesVaccine.PreformingUser.Name : "";

            }
            return result;
        }

        private string GetLastRabiesVaccineBatchNumber(ICollection<PreventiveMedicineItem> preventiveMedicineItems)
        {
            var rabiesVaccine = GetLastRabiesVaccineItem(preventiveMedicineItems);
            var result = string.Empty;
            if (rabiesVaccine != null)
            {
                result = rabiesVaccine.BatchNumber;

            }
            return result;
        }

        private string GetLastRabiesVaccineDoctorLicense(ICollection<PreventiveMedicineItem> preventiveMedicineItems)
        {
            var rabiesVaccine = GetLastRabiesVaccineItem(preventiveMedicineItems);
            var result = string.Empty;
            if (rabiesVaccine != null)
            {
                result = rabiesVaccine.ExternalyPreformed
                             ? rabiesVaccine.Comments
                             : rabiesVaccine.PreformingUser != null ? rabiesVaccine.PreformingUser.LicenceNumber : "";

            }
            return result;
        }



        private PreventiveMedicineItem GetLastRabiesVaccineItem(ICollection<PreventiveMedicineItem> preventiveMedicineItems)
        {
            var vaccines = preventiveMedicineItems.Where(
                 pr =>
                 pr.PriceListItemTypeId == (int)PriceListItemTypeEnum.Vaccines &&
                 pr.PriceListItem.IsRabiesVaccine)
                                    .OrderByDescending(pr => pr.Preformed)
                                    .Take(1);

            return vaccines.Any() ? vaccines.First() : null;
        }


        public string GetClientPhone(ICollection<Phone> phones, PhoneTypeEnum phoneType)
        {
            var result = string.Empty;
            var orderedCellPhones =
                phones.Where(p => p.PhoneTypeId == (int)phoneType).OrderBy(p => p.Id);
            if (orderedCellPhones.Any())
            {
                result = orderedCellPhones.First().PhoneNumber;
            }
            return result;
        }

        public string GetClientPhoneComment(ICollection<Phone> phones, string phoneNumber)
        {
            var result = "";
            var phone =
                phones.FirstOrDefault(p => p.PhoneNumber.Equals(phoneNumber));

            if (phone != null)
            {
                result = phone.Comment;
            }

            return result;
        }

        public string GetClientStreetAndNumber(ICollection<Address> addresses)
        {
            var result = string.Empty;
            var orderedAddresses = addresses.OrderByDescending(a => a.Id);
            if (orderedAddresses.Any())
            {
                var first = orderedAddresses.First();
                result = string.Format("{0}", first.Street);
            }
            return result;
        }

        public string GetClientAddress(ICollection<Address> addresses)
        {
            var result = string.Empty;
            var orderedAddresses = addresses.OrderByDescending(a => a.Id);
            if (orderedAddresses.Any())
            {
                var first = orderedAddresses.First();
                result = string.Format("{0} {1}", first.Street, first.City.Name);
            }
            return result;
        }

        public string GetClientFirstAddress(ICollection<Address> addresses)
        {
            var result = string.Empty;
            var orderedAddresses = addresses.OrderBy(a => a.Id);
            if (orderedAddresses.Any())
            {
                var first = orderedAddresses.First();
                result = string.Format("{0} {1}", first.Street, first.City.Name);
            }
            return result;
        }

        public string GetClientAddressForSticker(ICollection<Address> addresses)
        {
            var result = string.Empty;
            var orderedAddresses = addresses.OrderByDescending(a => a.Id);
            if (orderedAddresses.Any())
            {
                var first = orderedAddresses.First();
                result = string.Format("{0}<br />{1}", first.Street, first.City.Name);
            }
            return result;
        }

        public string GetClientZipCode(ICollection<Address> addresses)
        {
            var result = string.Empty;
            var orderedAddresses = addresses.OrderByDescending(a => a.Id);
            if (orderedAddresses.Any())
            {
                var first = orderedAddresses.First();
                result = string.Format("{0}", first.ZipCode);
            }
            return result;
        }

        public string GetClientCity(ICollection<Address> addresses)
        {
            var result = string.Empty;
            var orderedAddresses = addresses.OrderByDescending(a => a.Id);
            if (orderedAddresses.Any())
            {
                var first = orderedAddresses.First();
                result = string.Format("{0}", first.City.Name);
            }
            return result;
        }

        private string GetLocalAuthority(ICollection<Address> addresses)
        {
            var result = string.Empty;


            var address = addresses.OrderByDescending(a => a.Id).FirstOrDefault();
            result = string.Empty;
            if (address != null)
            {
                if (address.City != null)
                {
                    if (address.City.RegionalCouncil != null)
                    {
                        result = address.City.RegionalCouncil.Name;
                    }
                }
            }
            return result;
        }


        private string GetTemplateContent(Patient patient, LetterTemplateType templateType)
        {
            var template =
                DataContext.LetterTemplates.SingleOrDefault(
                    t => t.ClinicId == patient.Client.ClinicId && t.LetterTemplateTypeId == (int)templateType) ??
                GetOrCreateTemplate((int)templateType, patient.Client.ClinicId);


            template.Content = template.Content.Replace("&lt;", "<");
            template.Content = template.Content.Replace("&gt;", ">");

            return template.Content;
        }

        public IQueryable<LetterTemplate> GetClinicPersonalLetters(int clinicId)
        {
            return DataContext.LetterTemplates.Where(c => c.ClinicId == clinicId && c.LetterTemplateTypeId == (int)LetterTemplateType.PersonalLetter)
                              .OrderBy(c => c.TemplateName);
        }

        public bool IsPersonalLetterExist(int clinicId)
        {
            return DataContext.LetterTemplates.Count(c => c.ClinicId == clinicId && c.LetterTemplateTypeId == (int)LetterTemplateType.PersonalLetter) > 0;
        }

        public OperationStatus DeletePersonalTemplate(int id)
        {
            var model = DataContext.LetterTemplates.Single(lt => lt.Id == id);

            DataContext.LetterTemplates.Remove(model);
            var status = base.Save(model);
            if (status.Success)
            {
                status.ItemId = model.Id;
            }

            return status;
        }

        public LetterTemplate GetPersonalTemplate(int id)
        {
            var template = DataContext.LetterTemplates.Single(lt => lt.Id == id);
            template.Content = template.Content.Replace("&lt;", "<");
            template.Content = template.Content.Replace("&gt;", ">");

            return template;
        }


    }
}
