using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;

namespace RapidVet.Repository.Clinics
{
    public interface IClinicGroupDuplicateRepository : IDisposable
    {
        OperationStatus DuplicateClinicGroup(int clinicGroupId, string newClinicGroupName);
    }
}