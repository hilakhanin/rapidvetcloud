using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Clinics;

namespace RapidVet.Repository.Clinics
{
    public interface IDefaultCardCompaniesRepository : IDisposable
    {
        /// <summary>
        /// creates default card company in db
        /// </summary>
        /// <param name="defaultCardCompany">DefaultCardCompany object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus AddDefaultCardCompany(DefaultCardCompanies defaultCardCompany);

        /// <summary>
        /// creates a new default Card Company in db
        /// </summary>
        /// <param name="defaultCardCompany">DefaultCardCompanies object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(DefaultCardCompanies defaultCardCompany);
        
        /// <summary>
        /// returns a default Card Company object
        /// </summary>
        /// <param name="defaultCardCompanyId">id in issuers table</param>
        /// <returns>DefaultCardCompanies</returns>
        DefaultCardCompanies GetItem(int defaultCardCompanyId);

        /// <summary>
        /// returns a list of all active default Card Companies
        /// </summary>
        /// <returns>List<DefaultCardCompanies></returns>
        List<DefaultCardCompanies> GetAllDefaultCardCompanies();

        /// <summary>
        /// updates a default Card Company in db
        /// </summary>
        /// <param name="defaultCardCompany"></param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(DefaultCardCompanies defaultCardCompany);

        /// <summary>
        /// makes Card Company not active in DefaultCardCompanies table
        /// </summary>
        /// <param name="id">id in defaultCardCompany table</param>
        /// <returns>OperationStatus</returns>
        OperationStatus RemoveDefaultCardCompany(int id);

    }
}