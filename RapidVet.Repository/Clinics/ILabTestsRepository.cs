using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.PatientLabTests;
using RapidVet.Model.Users;
using RapidVet.WebModels.VaccinesAndTreatments;

namespace RapidVet.Repository.Clinics
{
    public interface ILabTestsRepository : IDisposable
    {
        /// <summary>
        /// gets items of lab test type of a clinic
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <returns>IQueryable(VaccineOrTreatment)</returns>
        IQueryable<LabTestType> GetLabTestTypes(int clinicId);
        LabTestTemplate GetLabTemplateByLabTestType(int labTestTypeId);
        /// <summary>
        /// get a single LabTestType by it's id
        /// </summary>
        /// <param name="id">id of LabTestType</param>
        /// <returns>LabTestType</returns>
        LabTestType GetItem(int id);

        /// <summary>
        /// update a LabTestType object, save to db
        /// </summary>
        /// <param name="labTestType"> LabTestType object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus UpdateItem(LabTestType labTestType);

        /// <summary>
        /// create and save a LabTestType object in db
        /// </summary>
        /// <param name="labTestType">LabTestType object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus AddItem(LabTestType labTestType);

        /// <summary>
        /// removes a LabTestType from clinic
        /// </summary>
        /// <param name="id">LabTestType to remove</param>
        /// <returns>OperationStatus</returns>
        OperationStatus RemoveItem(int id);

        /// <summary>
        /// gets items of lab test template type of a labtest type
        /// </summary>
        /// <param name="id"> labtest type id</param>
        /// <param name="animalKindId"> animalKindId</param>
        /// <returns>IQueryable(VaccineOrTreatment)</returns>
        IQueryable<LabTestTemplate> GetLabTestTemplates(int id, int animalKindId);
        /// <summary>
        /// gets items of lab test template type of a labtest type
        /// </summary>
        /// <param name="id"> labtest type id</param>
        /// <param name="animalKindId"> animalKindId</param>
        /// <returns>IQueryable(VaccineOrTreatment)</returns>
        IQueryable<LabTestTemplate> GetLabTestTemplatesByAnimalKindId(int animalKindId);

        /// <summary>
        /// get a single LabTestType by it's id
        /// </summary>
        /// <param name="labTestTemplateId">id of labTestTemplate</param>
        /// <returns>LabTestType</returns>
        LabTestTemplate GetTemplateItem(int labTestTemplateId);

        /// <summary>
        /// create and save a labTestTemplate object in db
        /// </summary>
        /// <param name="labTestTemplate">LabTestType object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus AddTemplateItem(LabTestTemplate labTestTemplate);

        /// <summary>
        /// update a labTestTemplate object, save to db
        /// </summary>
        /// <param name="labTestTemplate"> labTestTemplate object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus UpdateTemplateItem(LabTestTemplate labTestTemplate);

        /// <summary>
        /// removes a labTestTemplate from clinic
        /// </summary>
        /// <param name="id">labTestTemplate to remove</param>
        /// <returns>OperationStatus</returns>
        OperationStatus RemoveTemplateItem(int id);

        List<LabTest> GetLabTestData(int labTestTemplateId);

        /// <summary>
        /// get template name for item
        /// </summary>
        /// <param name="id"> templateitemid</param>
        /// <returns>string template item name</returns>
        string GetTemplateItemName(int id);

        /// <summary>
        /// saves lab test data
        /// </summary>
        /// <param name="labTestTemplateId"> lab test template id</param>
        /// <param name="testData"> test data</param>
        /// <returns> operation status</returns>
        OperationStatus SaveLabTestData(int labTestTemplateId, List<LabTest> testData);

        /// <summary>
        /// returns list of lab test template type for patient
        /// </summary>
        /// <param name="patientId"> patient id</param>
        /// <param name="clinicId"> clinic id</param>
        /// <returns>  List<LabTestType></returns>
        List<LabTestType> GetLabTestTypesForPatient(int patientId, int clinicId);

        /// <summary>
        /// get lab test templates for patient
        /// </summary>
        /// <param name="patientId"> patient id</param>
        /// <param name="labTestTypeId">lab test type id</param>
        /// <returns>List<LabTestTemplate></returns>
        List<LabTestTemplate> GetLabTestTemplatesForPatient(int patientId, int labTestTypeId);

        List<LabTestTemplate> GetLabTestTemplatesForKind(int animalKindId);

        /// <summary>
        /// get id of last lab test for patient
        /// </summary>
        /// <param name="patientId">int patient id</param>
        /// <returns>int</returns>
        int? GetLastTesId(int patientId);


        /// <summary>
        /// get patient last lab test
        /// </summary>
        /// <param name="patientId">int patient id</param>
        /// <returns>PatientLabTest</returns>
        PatientLabTest GetLastLabTest(int patientId);

        /// <summary>
        /// update patient lab test result
        /// </summary>
        /// <param name="id">int PatientLabTestResultsID</param>
        /// <param name="result">value</param>
        /// <returns></returns>
        OperationStatus UpdateResult(int id, string result);
        int GetAnimalKindIdFromPatientId(int patientId);
    }
}