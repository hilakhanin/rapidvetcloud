﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using RapidVet.Model.Clinics;
using RapidVet.Model;
using RapidVet.Model.Users;

namespace RapidVet.Repository.Clinics
{
    public interface IClinicGroupRepository : IDisposable
    {
        /// <summary>
        /// return all clinic groups
        /// </summary>
        /// <returns>IQueryable(ClinicGroup)</returns>
        IQueryable<ClinicGroup> GetAllActive();
        IQueryable<Model.Clinics.ClinicGroup> GetAllActiveIncludeClinics();

        IEnumerable<RapidVet.WebModels.ClinicGroups.ClinicGroupEdit> GetAllNotDepended();

        /// <summary>
        /// create a clinic group in db
        /// </summary>
        /// <param name="clinicGroup">clinicGroup object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(ClinicGroup clinicGroup);


        /// <summary>
        /// return a clinicGroup
        /// </summary>
        /// <param name="id">id in clinicGroups table</param>
        /// <returns>ClinicGroup</returns>
        ClinicGroup GetItem(int id);

        /// <summary>
        /// updates a clinicGroup in db
        /// </summary>
        /// <param name="clinicGroup">clinicGroup object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(ClinicGroup clinicGroup);

        /// <summary>
        /// returns all clinics in clinicGroup
        /// </summary>
        /// <param name="clinicGroupId">clinicGroup id</param>
        /// <returns>IQueryable(Clinic)</returns>
        IQueryable<Clinic> GetClinicList(int clinicGroupId);
        Clinic GetFirstClinic(int clinicGroupId);
        /// <summary>
        /// checks if the clinic group have any child clinics
        /// </summary>
        /// <param name="clinicGroupId">id of current clinic group</param>
        /// <returns>bool</returns>
        bool ClinicGroupHasClinics(int clinicGroupId);


        /// <summary>
        /// send email messages
        /// </summary>
        /// <param name="clinicGroupId"></param>
        /// <param name="messages">List<MailMessage></param>
        /// <returns> bool</returns>
        string SendEmailMessages(int clinicGroupId, List<MailMessage> messages);

        /// <summary>
        /// get all users in clinic group
        /// </summary>
        /// <param name="clinicGroupId">int clinic group id</param>
        /// <returns> List<User></returns>
        List<User> GetClinicUsers(int clinicGroupId);

        /// <summary>
        /// get all users in clinic group
        /// </summary>
        /// <param name="clinicGroupId">int clinic group id</param>
        /// <returns> List<User></returns>
        IQueryable<User> GetClinicGroupUsers(int clinicGroupId);


        /// <summary>
        /// get clinic name
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>string</returns>
        string GetName(int clinicId);

        /// <summary>
        /// get clinic gropu id from clinic id
        /// </summary>
        /// <param name="clinicId"></param>
        /// <returns></returns>
        int GetId(int clinicId);

        List<ClinicGroup> GetEmailTestConfigurations(int clinicGroupid);
    }
}
