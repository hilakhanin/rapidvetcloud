using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using System.Web.Mvc;
using RapidVet.Model.Tools;
using RapidVet.Model.Finance;

namespace RapidVet.Repository.Clinics
{
    public interface IAdminRepository : IDisposable
    {
        GeneralSettings GetGeneralSettings();

        OperationStatus Update(GeneralSettings generalSettings);

        OperationStatus Create(GeneralSettings generalSettings);

        List<FinanceDocumentPayment> GetPaymentsWithoutInvoice(int clinicId, DateTime from, int issuerId);

        int PaymentsToNewIssuer(int[] paymentsIds, int newIssuerId);
    }
}