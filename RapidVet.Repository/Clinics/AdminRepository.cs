﻿using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using RapidVet.Repository;
using System.Data.Entity;
using System.Web.Mvc;
using RapidVet.Model.Tools;
using RapidVet.Model.Finance;
using RapidVet.Enums.Finances;

namespace RapidVet.Repository.Clinics
{
    public class AdminRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IAdminRepository
    {

        public AdminRepository()
        {

        }

        public AdminRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        GeneralSettings IAdminRepository.GetGeneralSettings()
        {
            return DataContext.GeneralMessage.OrderByDescending(i => i.Id).SingleOrDefault();
        }

        public OperationStatus Update(GeneralSettings model)
        {
            var generalMessage = DataContext.GeneralMessage.Single(i => i.Id == model.Id);
            generalMessage.GeneralText = model.GeneralText;
            generalMessage.IsExportingClinic = model.IsExportingClinic;
            generalMessage.ClinicId = model.ClinicId;
            generalMessage.ExportStartTime = model.ExportStartTime;
            generalMessage.Updated = DateTime.Now;
            //generalMessage.IsExportingClinic = model.IsExportingClinic;
            //generalMessage.ExportStartTime = model.ExportStartTime;

            return base.Save(generalMessage);
        }

        public OperationStatus Create(GeneralSettings generalSettings)
        {
            generalSettings.Created = DateTime.Now;
            generalSettings.Updated = DateTime.Now;

            DataContext.GeneralMessage.Add(generalSettings);

            return base.Save(generalSettings);
        }

        public List<FinanceDocumentPayment> GetPaymentsWithoutInvoice(int clinicId, DateTime from, int issuerId)
        {
            var payments = DataContext.FinanceDocumentPayments.Include(p => p.Recipt).Where(p => p.InvoiceId == null && p.RefoundId == null
                && p.ClinicId == clinicId && p.DueDate.HasValue && p.DueDate.Value >= from && p.IssuerId == issuerId).ToList();
            return payments.Where(p => p.Recipt != null && p.Recipt.ReciptPriningOnly.HasValue && p.Recipt.ReciptPriningOnly != true).ToList();
        }

        public int PaymentsToNewIssuer(int[] paymentsIds, int newIssuerId)
        {
            int converted = 0;

            foreach (var itemId in paymentsIds)
            {
                var payment = DataContext.FinanceDocumentPayments.Include(p => p.Recipt).SingleOrDefault(p => p.Id == itemId);
                var recipt = payment.Recipt;
                FinanceDocument newReciept = DataContext.FinanceDocuments.SingleOrDefault(d => d.SerialNumber == recipt.SerialNumber && d.IssuerId == newIssuerId);

                if (newReciept == null)
                {
                    newReciept = new FinanceDocument()
                    {
                        BankTransferSum = 0,
                        CashPaymentSum = 0,
                        TotalCreditPaymentSum = 0,
                        TotalChequesPayment = 0,
                        ClientId = recipt.ClientId,
                        ClientAddress = recipt.ClientAddress,
                        ClientName = recipt.ClientName,
                        ClientPhone = recipt.ClientPhone,
                        ClinicId = recipt.ClinicId,
                        Comments = recipt.Comments,
                        ConvertedFromRecipt = false,
                        ConvertedFromReciptSerialNumber = 0,
                        Created = recipt.Created,
                        FinanceDocumentStatusId = recipt.FinanceDocumentStatusId,
                        FinanceDocumentStatus = recipt.FinanceDocumentStatus,
                        FinanceDocumentType = FinanceDocumentType.Receipt,
                        FinanceDocumentTypeId = recipt.FinanceDocumentTypeId,
                        VAT = recipt.VAT,
                        UserId = recipt.UserId,
                        IssuerId = newIssuerId,
                        SerialNumber = recipt.SerialNumber,
                        Discount = recipt.Discount,
                        RoundingFactor = recipt.RoundingFactor,
                        CreatorUserId = 1,
                        ReciptPriningOnly = false
                    };

                    newReciept.Items = new List<FinanceDocumentItem>();

                    foreach (var item in DataContext.FinanceDocumentItems.Where(i => i.FinanceDocumentId == recipt.Id))
                    {
                        newReciept.Items.Add(new FinanceDocumentItem()
                            {
                                CatalogNumber = item.CatalogNumber,
                                Description = item.Description,
                                Discount = item.Discount,
                                DiscountPercentage = item.DiscountPercentage,
                                PriceListItemId = item.PriceListItemId,
                                Quantity = item.Quantity,
                                VisitId = item.VisitId,
                                UnitPrice = item.UnitPrice,
                                TotalBeforeVAT = item.TotalBeforeVAT,
                                AddedBy = item.AddedBy,
                                AddedByUserId = item.AddedByUserId                                
                            });
                    }
                    //save sums of all payment types                    
                    var totalCredit = DataContext.FinanceDocumentPayments.Include(p => p.Recipt).Where(p => p.Recipt.SerialNumber == recipt.SerialNumber && paymentsIds.Contains(p.Id) && p.PaymentTypeId == (int)PaymentType.CreditCard);
                    var totalCheques = DataContext.FinanceDocumentPayments.Include(p => p.Recipt).Where(p => p.Recipt.SerialNumber == recipt.SerialNumber && paymentsIds.Contains(p.Id) && p.PaymentTypeId == (int)PaymentType.Cheque);
                    var totalCash = DataContext.FinanceDocumentPayments.Include(p => p.Recipt).Where(p => p.Recipt.SerialNumber == recipt.SerialNumber && paymentsIds.Contains(p.Id) && p.PaymentTypeId == (int)PaymentType.Cash);
                    var totalTransfers = DataContext.FinanceDocumentPayments.Include(p => p.Recipt).Where(p => p.Recipt.SerialNumber == recipt.SerialNumber && paymentsIds.Contains(p.Id) && p.PaymentTypeId == (int)PaymentType.Transfer);

                    newReciept.TotalCreditPaymentSum = totalCredit.Any() ? totalCredit.Sum(s => s.Sum) : 0;
                    newReciept.TotalChequesPayment = totalCheques.Any() ? totalCheques.Sum(s => s.Sum) : 0;
                    newReciept.CashPaymentSum = totalCash.Any() ? totalCash.Sum(s => s.Sum) : 0;
                    newReciept.BankTransferSum = totalTransfers.Any() ? totalTransfers.Sum(s => s.Sum) : 0;
                    //reduce sums from original reciept
                    recipt.TotalCreditPaymentSum -= newReciept.TotalCreditPaymentSum;
                    recipt.TotalChequesPayment -= newReciept.TotalChequesPayment;
                    recipt.CashPaymentSum -= newReciept.CashPaymentSum;
                    recipt.BankTransferSum -= newReciept.BankTransferSum;

                    DataContext.FinanceDocuments.Add(newReciept);
                }
                payment.Recipt = newReciept;
                payment.ReciptId = newReciept.Id;
                payment.IssuerId = newIssuerId;

                DataContext.SaveChanges();
                converted++;
            }

            return converted;
        }
    }
}