using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using System.Web.Mvc;
using RapidVet.Model.Tools;
using RapidVet.Model.Finance;

namespace RapidVet.Repository.Clinics
{
    public interface IMessageDistributionRepository : IDisposable
    {
        void Increment(int ClinicId, int IncrementBy, MessageType type);
        int MailsSentThisMonth(int ClinicId);
        MessageDistributionCount MessagesSentInMonth(int ClinicId, DateTime monthStart);
    }
}