﻿using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using RapidVet.Repository;
using System.Data.Entity;
using System.Web.Mvc;
using RapidVet.Model.Tools;
using RapidVet.Model.Finance;
using RapidVet.Enums.Finances;
using Hangfire;

namespace RapidVet.Repository.Clinics
{
    public class BackgroundJobsRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IBackgroundJobsRepository
    {

        public BackgroundJobsRepository()
        {

        }

        public BackgroundJobsRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {
        }

        public void Cancel(string jobId, int userId)
        {
            if (!string.IsNullOrEmpty(jobId))
            {
                var model = DataContext.ClinicBackgroundJobs.SingleOrDefault(j => j.JobId.Equals(jobId));
                if (model != null && !model.WasCancelled)
                {
                    model.WasCancelled = BackgroundJob.Delete(jobId);
                    if (model.WasCancelled)
                    {
                        model.CancellingUserId = userId;
                        model.EndTime = DateTime.Now;
                        Update(model);
                    }
                }
            }
        }

        public OperationStatus Update(ClinicBackgroundJob model)
        {
            if (model.Id == 0)
            {
                return Create(model);
            }

            DataContext.Entry(model).State = System.Data.Entity.EntityState.Modified;
            return base.Save(model);
        }

        public OperationStatus Create(ClinicBackgroundJob backgroundJob)
        {
            backgroundJob.StartTime = DateTime.Now;
            DataContext.ClinicBackgroundJobs.Add(backgroundJob);
            return base.Save(backgroundJob);
        }

        public ClinicBackgroundJob GetBackgroundJobById(string jobId)
        {
            return DataContext.ClinicBackgroundJobs.SingleOrDefault(j => j.JobId.Equals(jobId));
        }

        public ClinicBackgroundJob GetBackgroundJobByGuid(string jobGuid)
        {
            return DataContext.ClinicBackgroundJobs.SingleOrDefault(j => j.JobGuid.Equals(jobGuid));
        }

        public List<ClinicBackgroundJob> GetBackgroundJobsByClinicId(int clinicId, bool showFinishedJobs = false)
        {
            var jobs = DataContext.ClinicBackgroundJobs.Include(j => j.InitiatingUser).Include(j => j.CancellingUser).Where(j => j.ClinicId == clinicId);

            if(!showFinishedJobs)
            {
                jobs = jobs.Where(j => !j.EndTime.HasValue);
            }

            return jobs.ToList();
        }
    }
}