﻿using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;
using RapidVet.Repository;
using System.Data.Entity;
using RapidVet.Repository.Clients;

namespace RapidVet.Repository.Clinics
{
    public class ClinicRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IClinicRepository
    {

        public ClinicRepository()
        {

        }

        public ClinicRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }
        /// <summary>
        /// gets all clinics in clinicGroup
        /// </summary>
        /// <param name="clinicGroupId"> clinic group id</param>
        /// <returns>IQueryable(Clinic)</returns>
        public IQueryable<Clinic> GetList(int clinicGroupId)
        {
            return DataContext.Clinics.Include(c => c.ClinicGroup).Where(c => c.ClinicGroupID == clinicGroupId);
        }
       
        /// <summary>
        /// get a list of all clinics in the db
        /// </summary>
        /// <returns>IQueryable(Clinic)</returns>
        public IQueryable<Clinic> GetList()
        {
            return DataContext.Clinics.Include(c => c.ClinicGroup);
        }

        public IQueryable<Clinic> GetAllActiveClinicsInGroup(int clinicGroupId)
        {
            return DataContext.Clinics.Where(c => c.Active && c.ClinicGroupID == clinicGroupId);
        }

        /// <summary>
        ///  get a list of all clinics in the db without any dependancy with clinicgroup
        /// </summary>
        /// <returns>IQueryable(Clinic)</returns>
        public IQueryable<Clinic> GetClearList(int clinicGroupID)
        {
            return DataContext.Clinics.Where(x => clinicGroupID == -1 || x.ClinicGroupID == clinicGroupID);
        }

        public int GetMaxLocalClientId(int clinicId)
        {
            if (DataContext.Clients.Any(c => c.ClinicId == clinicId))
            {
                return DataContext.Clients.Where(c => c.ClinicId == clinicId).Max(c => c.LocalId);
            }
            else
            {
                return 1010;
            }
        }

        public int GetMaxLocalPatientId(int clinicId)
        {
            if (DataContext.Patients.Any(c => c.Client.ClinicId == clinicId))
            {
                return DataContext.Patients.Where(c => c.Client.ClinicId == clinicId).Max(c => c.LocalId);
            }
            else
            {
                return 2050;
            }
        }

        /// <summary>
        /// get a single clinic by id
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>Clinic</returns>
        public Clinic GetItem(int clinicId)
        {
            return
                DataContext.Clinics.Include(c => c.ClinicGroup)
                           .Include(c => c.Issuers)
                           .Include(c => c.PriceListCategories)
                           //.Include(c => c.TaxRates)
                           .Single(c => c.Id == clinicId);
        }

        public Clinic GetClinicByClientId(int clientId)
        {
            var client = DataContext.Clients.SingleOrDefault(c => c.Id == clientId);

            return client != null ? GetItem(client.ClinicId) : null;
        }

        /// <summary>
        /// update a clinic object, save to db
        /// </summary>
        /// <param name="clinic"> clinic object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(Clinic clinic)
        {
            return base.Save(clinic);
        }

        /// <summary>
        /// return a CreateModelObject for clinic
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <returns>Clinic object</returns>
        public Clinic GetCreateModel(int clinicGroupId)
        {

            return new Clinic()
                       {
                           ClinicGroup = DataContext.ClinicGroups.Single(g => g.Id == clinicGroupId),
                           ClinicGroupID = clinicGroupId,
                           DefaultNewClientStatusId = DataContext.ClientStatuses.First(s => s.ClinicGroupId == clinicGroupId).Id
                       };
        }

        /// <summary>
        /// create and save a clinic object in db
        /// </summary>
        /// <param name="clinic">clinic object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Create(Clinic clinic)
        {
            DataContext.Clinics.Add(clinic);
            OperationStatus status = base.Save(clinic);
            if (status.Success)
            {
                status.ItemId = clinic.Id;
            }
            return status;
        }

        /// <summary>
        /// returns a user object
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns>User</returns>
        public User GetUser(int userId)
        {
            return DataContext.Users.Include(u => u.Roles).Include(u => u.ClinicGroup).Single(u => u.Id == userId);
        }
        public User GetUserByUN(string username)
        {
            //return DataContext.Users.Include(u => u.Roles).Include(u => u.ClinicGroup).Single(u => u.Username == username);
            ////return from u in DataContext.Users
            ////    join ucr in DataContext.UsersClinicsRoles on u.Id equals ucr.UserId
            ////    join r in DataContext.Roles on ucr.RoleId equals r.RoleId
            ////    where(u.Username == username && ucr.)
            ////      select u;

            var roleNodiary = DataContext.Roles.Single(r => r.RoleName == RapidConsts.RolesConsts.NO_DIARY);

            var userClinicRoles =
               DataContext.UsersClinicsRoles.Where(ucr => ucr.User.Username == username && ucr.RoleId == roleNodiary.RoleId);
            var user = userClinicRoles.Select(u => u.User).Where(usr => usr.Active).Where(usr2 => usr2.Username == username).OrderBy(u => u.LastName).Include(u => u.Roles).FirstOrDefault();

            return user;

        }
        public User GetUserByID(int id)
        {
            //return DataContext.Users.Include(u => u.Roles).Include(u => u.ClinicGroup).Single(u => u.Username == username);
            ////return from u in DataContext.Users
            ////    join ucr in DataContext.UsersClinicsRoles on u.Id equals ucr.UserId
            ////    join r in DataContext.Roles on ucr.RoleId equals r.RoleId
            ////    where(u.Username == username && ucr.)
            ////      select u;

            var roleNodiary = DataContext.Roles.Single(r => r.RoleName == RapidConsts.RolesConsts.NO_DIARY);

            var userClinicRoles =
               DataContext.UsersClinicsRoles.Where(ucr => ucr.User.Id == id && ucr.RoleId == roleNodiary.RoleId);
            var user = userClinicRoles.Select(u => u.User).Where(usr => usr.Active).Where(usr2 => usr2.Id == id).OrderBy(u => u.LastName).Include(u => u.Roles).FirstOrDefault();
        
            return user;

        }
        public UsersClinicsRoles GetUsersClinicsRolesNO_DIARY(int UserID)
        {
            var roleNodiary = DataContext.Roles.Single(r => r.RoleName == RapidConsts.RolesConsts.NO_DIARY);
            var userClinicRoles =
                   DataContext.UsersClinicsRoles.Where(ucr => ucr.User.Id == UserID && ucr.RoleId == roleNodiary.RoleId).FirstOrDefault();
            return userClinicRoles;
        }
        /// <summary>
        /// removes a user from issuers list in clinic
        /// </summary>
        /// <param name="issuerId">id in issuers table</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus RemoveIssuer(int issuerId)
        {
            var issuer = DataContext.Issuers.Single();
            DataContext.Issuers.Remove(issuer);
            return base.Save(issuer);
        }

        /// <summary>
        /// returns clinic name
        /// </summary>
        /// <param name="id">clinic id</param>
        /// <returns>string</returns>
        public string GetName(int id)
        {
            return DataContext.Clinics.Single(c => c.Id == id).Name;
        }

        /// <summary>
        /// returns a list of all doctors in clinic
        /// <param name="clinicId"></param>
        /// </summary>
        /// <returns>List(User) </returns>
        public List<User> GetClinicDoctors(int clinicId)
        {
            var roleDoctor = DataContext.Roles.Single(r => r.RoleName == RapidConsts.RolesConsts.DOCTOR);

            var userClinicRoles =
               DataContext.UsersClinicsRoles.Where(ucr => ucr.ClinicId == clinicId && ucr.RoleId == roleDoctor.RoleId);
            if (userClinicRoles != null)
            {
                var users = userClinicRoles.Select(u => u.User).Where(usr => usr.Active).OrderBy(u => u.LastName).ToList();
                return users;
            }
            return null;
        }

        public List<User> GetClinicDoctorsForSchedule(int clinicId, int activeUserID = -1, bool isPrivateCalendars = false)
        {
            var roleDoctor = DataContext.Roles.Single(r => r.RoleName == RapidConsts.RolesConsts.DOCTOR);
            var users = (from ucr in DataContext.UsersClinicsRoles
                         where ucr.ClinicId == clinicId && ucr.RoleId == roleDoctor.RoleId && ucr.User.Active
                         from udv in DataContext.UsersDoctorsView.Where(x => x.DoctorID == ucr.UserId && x.UserID == activeUserID && x.ClinicID == clinicId && x.IsPrivate == isPrivateCalendars).DefaultIfEmpty()
                         let showOnMainWin = udv == null ? true : udv.ShowOnMainWindow
                         let usersDoctorsViewId = udv == null ? -1 : udv.Id
                         select new
                         {
                             u = ucr.User,
                             udvId = usersDoctorsViewId,
                             showOMW = showOnMainWin
                         }).OrderBy(x => x.u.LastName).ToList().Select(x =>
                         {
                             x.u.ShowOnMainWin = x.showOMW;
                             x.u.UsersDoctorsViewId = x.udvId;
                             return x.u;
                         }).ToList();
            //var userClinicRoles =
            //   DataContext.UsersClinicsRoles.Where(ucr => ucr.ClinicId == clinicId && ucr.RoleId == roleDoctor.RoleId);
            //var users = userClinicRoles.Select(u => u.User).Where(usr => usr.Active).OrderBy(u => u.LastName).ToList();
            return users;
        }


        public void Save(int userID, int clinicId, List<WebModels.Users.UsersSettingsShowUserModel> list, bool isPrivateCalendars = false)
        {
            if (list == null || list.Count == 0)
                return;

            updateUsersSettingsShowUserModel(ref list, userID, clinicId, isPrivateCalendars);
            newUsersSettingsShowUserModel(list, userID, clinicId, isPrivateCalendars);
        }

        private void newUsersSettingsShowUserModel(List<WebModels.Users.UsersSettingsShowUserModel> relev, int userID, int clinicId, bool isPrivateCalendars)
        {
            foreach (var elem in relev)
                DataContext.UsersDoctorsView.Add(new UsersDoctorsView()
                {
                    ClinicID = clinicId,
                    ShowOnMainWindow = elem.Show,
                    UserID = userID,
                    DoctorID = elem.DoctorId,
                    IsPrivate = isPrivateCalendars
                });

            DataContext.SaveChanges();
        }

        private void updateUsersSettingsShowUserModel(ref List<WebModels.Users.UsersSettingsShowUserModel> relev, int userID, int clinicId, bool isPrivateCalendars)
        {
            var l = relev.FindAll(y => y.Id != -1).Select(x => x.Id).ToList();
            var l2 = relev.FindAll(y => y.DoctorId != -1).Select(x => x.DoctorId).ToList();
            if (l == null || l.Count == 0)
                return;

            var q = DataContext.UsersDoctorsView.Where(x => x.UserID == userID && x.ClinicID == clinicId && x.IsPrivate == isPrivateCalendars && (isPrivateCalendars || l2.Contains(x.DoctorID)));
            //var q = DataContext.UsersDoctorsView.Where(x => x.UserID == userID && x.ClinicID == clinicId && x.IsPrivate == isPrivateCalendars && (isPrivateCalendars || l.Contains(x.Id) || l2.Contains(x.DoctorID)));
            foreach (var i in q)
            {
                //var elem = isPrivateCalendars ?  relev.Find(x => x.DoctorId == i.DoctorID) : relev.Find(x => x.Id == i.Id);
                var elem = relev.Find(x => x.DoctorId == i.DoctorID);
                if (elem == null)
                    continue;

                i.ShowOnMainWindow = elem.Show;
                relev.Remove(elem);
            }
            DataContext.SaveChanges();
        }

        /// <summary>
        /// returns all patients in clinic
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <returns>   IQueryable(Patient)</returns>
        public IQueryable<Model.Patients.Patient> GetClinicPatients(int clinicId)
        {
            return DataContext.Patients.Where(p => p.Client.ClinicId == clinicId);
        }

        public double GetClinicPatientsProfilePicSize(int clinicId)
        {
            double totalSize = 0;
            var profilePics = DataContext.Patients.Where(p => p.Client.ClinicId == clinicId && p.ProfilePicSizeInBytes > 0);

            foreach (var file in profilePics)
            {
                totalSize += file.ProfilePicSizeInBytes;
            }

            return totalSize;
        }

        public List<Diagnosis> GetClinicDiagnoses(int clinicId)
        {
            return DataContext.Diagnoses
                .Where(d => d.ClinicId == clinicId && d.Active)
                .OrderBy(d => d.Name).ToList();
        }

        public List<Client> GetClinicClients(int clinicId)
        {
            return DataContext.Clients.Include(c=>c.ClientStatus).Where(cl => !cl.ClientStatus.Name.Equals(Resources.Client.ClientStatusInactive) && cl.ClinicId == clinicId).ToList();
        }

        public int GetClinicClientsCount(int clinicId)
        {
            return DataContext.Clients.Count(cl => cl.ClinicId == clinicId);
        }

        public List<Client> GetClinicClientsForExport(int clinicId, int pageIndex, int pageSize)
        {
            using (RapidVetDataContext dbCCFE = new RapidVetDataContext())
            {
                 return dbCCFE.Clients.Include(c => c.Addresses).Include("Addresses.City").Include(c => c.ClientStatus)
                                      .Include(c => c.Emails).Include(c => c.Phones).Include(c => c.Patients)
                                      .Include("Patients.AnimalRace").Include("Patients.AnimalRace.AnimalKind").Include("Patients.AnimalColor")
                                      .Include("Patients.Comments").Include("Patients.LabTests").Include("Patients.LabTests.Results")
                                      .Include("Patients.PreventiveMedicine").Include("Patients.PreventiveMedicine.PriceListItem")
                                      .Include("Patients.PreventiveMedicine.PriceListItemType").Include("Patients.MedicalProcedures").Include("Patients.MedicalProcedures.Diagnosis")
                                      .Include(c => c.Visits).Include("Visits.Examinations").Include("Visits.Examinations.PriceListItem")
                                      .Include("Visits.Diagnoses").Include("Visits.Diagnoses.Diagnosis")
                                      .Include("Visits.Treatments").Include("Visits.Treatments.PriceListItem").Include("Visits.Medications").Include("Visits.Medications.Medication")
                                      .Include("Visits.DangerousDrugs").Include("Visits.DangerousDrugs.Medication").Include("Visits.VitalSigns").Include("Visits.Doctor")
                                      .Include(c => c.FinanceDocuments).Include("FinanceDocuments.Items")
                                      .Where(cl => cl.ClinicId == clinicId).OrderBy(c => c.Id).Skip(pageIndex * pageSize).Take(pageSize).ToList();
            }
        }

        public List<AdvancedPaymentsPercent> GetAllAdvancePaymentPercentChanges(int clinicId, int issuerId)
        {
            return DataContext.AdvancedPaymentsPercents.Where(p => p.ClinicId == clinicId && p.IssuerId == issuerId).OrderByDescending(p => p.PercentChangedDate).OrderByDescending(x => x.PercentChangedDate).ToList();
        }

        public List<TaxRate> GetAllTaxRateChanges(int clinicId)
        {
            return DataContext.TaxRates.Where(p => p.ClinicId == clinicId).OrderByDescending(p => p.TaxRateChangedDate).OrderByDescending(x => x.TaxRateChangedDate).ToList();
        }

        public OperationStatus AddAdvancedPaymentPercent(AdvancedPaymentsPercent advancedPaymentsPercent)
        {
            DataContext.AdvancedPaymentsPercents.Add(advancedPaymentsPercent);
            return base.Save(advancedPaymentsPercent);
        }

        public void UpdateAdvancePaymentPercent(AdvancedPaymentsPercent advancedPaymentsPercent)
        {
            DataContext.AdvancedPaymentsPercents.Add(advancedPaymentsPercent);
        }

        public void UpdateTaxRate(TaxRate taxRate)
        {
            DataContext.TaxRates.Add(taxRate);
        }

        public AdvancedPaymentsPercent GetAdvancedPaymentPercent(int percentId)
        {
            return DataContext.AdvancedPaymentsPercents.Single(t => t.Id == percentId);
        }

        public OperationStatus RemoveAdvancedPaymentPercent(int percentId)
        {
            var status = new OperationStatus()
            {
                Success = false
            };
            var paymentPercent = DataContext.AdvancedPaymentsPercents.SingleOrDefault(r => r.Id == percentId);
            if (paymentPercent != null)
            {
                DataContext.AdvancedPaymentsPercents.Remove(paymentPercent);
                status = base.Save(paymentPercent);
            }

            return status;
        }


        public List<VisitFilter> GetVisitFilters(int clinicId)
        {
            return DataContext.VisitFilters.Where(f => f.ClinicId == clinicId).ToList();
        }

        public VisitFilter GetVisitFilter(int visitFilterId)
        {
            return DataContext.VisitFilters.SingleOrDefault(f => f.Id == visitFilterId);
        }

        public void AddVisitFilter(VisitFilter visitFilter)
        {
            DataContext.VisitFilters.Add(visitFilter);
        }

        public void UpdateClientsSearchString(int clinicId)
        {
            var clinicClientsIds = DataContext.Clients.Where(c => c.ClinicId == clinicId).Select(c => c.Id).ToList();
            using (var repo = new ClientRepository())
            {
                _DataContext = new RapidVetDataContext();
                int i = 0;
                foreach (var clientId in clinicClientsIds)
                {
                    i++;
                    repo.SetClientNameWithPatients(clientId, false);
                    if (i > 100)
                    {
                        DataContext.SaveChanges();
                        DataContext.Dispose();
                        _DataContext = new RapidVetDataContext();
                        i = 0;
                    }
                }
                DataContext.SaveChanges();
            }
        }

        public OperationStatus AddTaxRate(TaxRate taxRate)
        {
            DataContext.TaxRates.Add(taxRate);
            return base.Save(taxRate);
        }

        public Clinic GetClinic(int clinicId)
        {
            var clinic = DataContext.Clinics.SingleOrDefault(c => c.Id == clinicId);
            var dt = DateTime.Now;
            return clinic;
        }

        public Clinic GetClinicWithLabsData(int clinicId)
        {
            var clinic = DataContext.Clinics.Include(c => c.LabTests).Include("LabTests.AnimalKinds")
                                            .Include("LabTests.AnimalKinds.LabTestResults").Include("LabTests.AnimalKinds.AnimalKind")


                .SingleOrDefault(c => c.Id == clinicId);

            return clinic;
        }

        public TaxRate GetTaxRate(int taxRateId)
        {
            return DataContext.TaxRates.Single(t => t.Id == taxRateId);
        }

        public OperationStatus RemoveTaxRate(int taxRateId)
        {
            var status = new OperationStatus()
                {
                    Success = false
                };
            var taxRate = DataContext.TaxRates.SingleOrDefault(r => r.Id == taxRateId);
            if (taxRate != null)
            {
                DataContext.TaxRates.Remove(taxRate);
                status = base.Save(taxRate);
            }

            return status;
        }

        public int GetNumberOfClinics()
        {
            return DataContext.Clinics.Count(c => c.Active);
        }

       



    }
}