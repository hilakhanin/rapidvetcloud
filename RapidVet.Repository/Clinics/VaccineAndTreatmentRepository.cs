﻿using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.Users;
using RapidVet.Repository;
using System.Data.Entity;
using EntityFramework.Extensions;

namespace RapidVet.Repository.Clinics
{
    public class VaccineAndTreatmentRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IVaccineAndTreatmentRepository
    {

        public VaccineAndTreatmentRepository()
        {

        }

        public VaccineAndTreatmentRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public IQueryable<VaccineOrTreatment> GetVaccineAndTreatmentOfAnimalKind(int clinicId, int animalKindId)
        {
            return DataContext.VaccineAndTreatments.Include(vt => vt.PriceListItem)
                           .Where(vt => vt.AnimalKindId == animalKindId && vt.ClinicId == clinicId && vt.Active);
        }

        public VaccineOrTreatment GetItem(int id)
        {
            return DataContext.VaccineAndTreatments.Include(vot => vot.PriceListItem).SingleOrDefault(vt => vt.Id == id);
        }

        public VaccineOrTreatment GetItemByPriceListItemId(int animalKindId, int priceListItemId)
        {
            return DataContext.VaccineAndTreatments.SingleOrDefault(vt => vt.PriceListItemId == priceListItemId && vt.AnimalKindId == animalKindId && vt.Active);
        }

        public OperationStatus UpdateItem(VaccineOrTreatment vaccineOrTreatment)
        {
            return base.Save(vaccineOrTreatment);
        }

        public void AddItem(VaccineOrTreatment vaccineOrTreatment)
        {
            DataContext.VaccineAndTreatments.Add(vaccineOrTreatment);
        }

        public void RemoveItem(int id)
        {
            var vaccineOrTreatment = DataContext.VaccineAndTreatments.SingleOrDefault(vt => vt.Id == id);
            if (vaccineOrTreatment != null)
            {
                DataContext.VaccineAndTreatments.Remove(vaccineOrTreatment);
            }
        }

        public IQueryable<NextVaccineOrTreatment> GetNextVaccineAndTreatmentOfItem(int id)
        {
            return DataContext.NextVaccineAndTreatments.Include("VaccineOrTreatment.PriceListItem").Where(vt => vt.ParentVaccineOrTreatmentId == id);
        }

        public void RemoveNextVoTItem(int nextVoTId)
        {
            var nextVaccineOrTreatment = DataContext.NextVaccineAndTreatments.SingleOrDefault(nvt => nvt.Id == nextVoTId);
            if (nextVaccineOrTreatment != null)
            {
                DataContext.NextVaccineAndTreatments.Remove(nextVaccineOrTreatment);
            }
        }

        //update current price list item and remove connections
        public void UpdateVaccineByPriceListItem(int priceListItemId, int newPriceListItemId)
        {
            var VorTs = DataContext.VaccineAndTreatments.Where(v => v.PriceListItemId == priceListItemId);

            foreach(var item in VorTs)
            {
                var nextParentVorT = DataContext.NextVaccineAndTreatments.Where(n => n.ParentVaccineOrTreatmentId == item.Id);
                foreach (var nextParentItem in nextParentVorT)
                {
                    RemoveNextVoTItem(nextParentItem.Id);
                }

                var nextVorT = DataContext.NextVaccineAndTreatments.Where(n => n.VaccineOrTreatmentId == item.Id);
                foreach (var nextItem in nextVorT)
                {
                    RemoveNextVoTItem(nextItem.Id);
                }

                 if (!DataContext.VaccineAndTreatments.Any(v => v.PriceListItemId == newPriceListItemId && v.AnimalKindId == item.AnimalKindId))
                {
                    item.PriceListItemId = newPriceListItemId;
                }
                else
                {
                    RemoveItem(item.Id);
                }
            }

            DataContext.SaveChanges();
        }

        public NextVaccineOrTreatment GetNextVoTItem(int id, int nextVoTId)
        {
            return DataContext.NextVaccineAndTreatments.SingleOrDefault(nvt => nvt.VaccineOrTreatmentId == nextVoTId && nvt.ParentVaccineOrTreatmentId == id);
        }

        public void AddNextVoTItem(NextVaccineOrTreatment nextVaccineOrTreatment)
        {
            DataContext.NextVaccineAndTreatments.Add(nextVaccineOrTreatment);
        }

        public OperationStatus UpdateNextVoTItem(NextVaccineOrTreatment nextVaccineOrTreatment)
        {
            return base.Save(nextVaccineOrTreatment);
        }

        public List<VaccineOrTreatment> GetVaccineAndTreatmentByPatientId(int patientId)
        {
            var animalKindId = GetAnimalKindIdFromPatientId(patientId);
            var patient = DataContext.Patients.Include(c => c.Client).Single(p => p.Id == patientId);
            return
                DataContext.VaccineAndTreatments
                           .Where(v => v.ClinicId == patient.Client.ClinicId && v.AnimalKindId == animalKindId && v.Active)
                           .OrderBy(v => v.Order)
                           .ToList();
        }

        public List<NextVaccineOrTreatment> GetNextVaccineAndTreatmentByPatientId(int patientId)
        {
            var animalKindId = GetAnimalKindIdFromPatientId(patientId);
            var vaccinOrTreatmentIds =
                DataContext.VaccineAndTreatments.Where(vot => vot.AnimalKindId == animalKindId && vot.Active).Select(vot => vot.Id);
            return
                DataContext.NextVaccineAndTreatments.Include(n => n.VaccineOrTreatment)
                           .Include(n => n.ParentVaccineOrTreatment)
                           .Where(
                               n =>
                               vaccinOrTreatmentIds.Contains(n.ParentVaccineOrTreatmentId) ||
                               vaccinOrTreatmentIds.Contains(n.VaccineOrTreatmentId)).ToList();
        }

        public bool TemplateCahnged(int clinicId)
        {
            var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var tomorrow = today.AddDays(1);

            var vaccineOrTreatmentIds =
                DataContext.VaccineAndTreatments.Where(v => v.ClinicId == clinicId).Select(v => v.Id);

            var changedTemplates = DataContext.NextVaccineAndTreatments
                                              .Include(n => n.VaccineOrTreatment)
                                              .Include("VaccineOrTreatment.PriceListItem")
                                              .Where(
                                                  n => vaccineOrTreatmentIds.Contains(n.ParentVaccineOrTreatmentId) &&
                                                       ((n.Created >= today && n.Created < tomorrow && n.Updated == null)
                                                        ||
                                                        (n.Updated.HasValue && n.Updated.Value >= today &&
                                                         n.Updated.Value < tomorrow)
                                                       )
                );

            var clinicPatientIds =
                DataContext.Patients.Include(p => p.Client).Where(p => p.Client.ClinicId == clinicId && p.Active).Select(p => p.Id);

            var clinicPreventiveItems =
                DataContext.PreventiveMedicineItems.Include(p => p.Patient)
                           .Include(p => p.Parent)
                           .Include("Patient.AnimalRace")
                           .Where(
                               p =>
                               clinicPatientIds.Contains(p.PatientId)
                               && p.PreformingUserId == null &&
                               !p.Preformed.HasValue &&
                               p.Scheduled.HasValue &&
                               p.Scheduled.Value > DateTime.MinValue &&
                               p.ParentPreventiveItemId.HasValue &&
                               p.ParentPreventiveItemId.Value > 0 &&
                               p.Parent.Preformed.HasValue &&
                               p.Parent.Preformed.Value > DateTime.MinValue);

            foreach (var ct in changedTemplates)
            {
                var preventiveItems =
                    clinicPreventiveItems.Where(
                        c =>
                        c.Patient.AnimalRace.AnimalKindId == ct.VaccineOrTreatment.AnimalKindId &&
                        c.ParentPreventiveItemId.HasValue &&
                        c.Parent.PriceListItemId == ct.ParentVaccineOrTreatment.PriceListItemId);
                //@Nassar Start
                            foreach (var item in clinicPreventiveItems.Where(p => p.PriceListItemId == ct.VaccineOrTreatment.PriceListItemId))
                            {
                                item.Scheduled = item.Parent.Preformed.Value.AddDays(ct.DaysToNext);
                            }
               
                        //var x = preventiveItems.Where(p => p.PriceListItemId == ct.VaccineOrTreatment.PriceListItemId);
                        //    foreach (var item in x)
                        //    {

                        //        item.Scheduled = item.Parent.Preformed.Value.AddDays(ct.DaysToNext);
                        //    }
               //@Nassar End

            }

            return DataContext.SaveChanges() > -1;

        }

        private int GetAnimalKindIdFromPatientId(int patientId)
        {
            return DataContext.Patients.Include(p => p.AnimalRace).Single(p => p.Id == patientId).AnimalRace.AnimalKindId;
        }

        public void SetInactiveByPriceListItemId(int priceListItemId)
        {
            DataContext.VaccineAndTreatments.Where(p => p.PriceListItemId == priceListItemId)
                                       .Update(p => new VaccineOrTreatment { Active = false });
        }

        public string GetPriceListItemNameByVaccineId(int vaccineId)
        {
            return DataContext.VaccineAndTreatments.Include(v => v.PriceListItem).Single(v => v.Id == vaccineId).PriceListItem.Name;
        }
      
    }
}