﻿using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using RapidVet.Repository;
using System.Data.Entity;
using System.Web.Mvc;
using RapidVet.Model.Tools;
using RapidVet.Model.Finance;
using RapidVet.Enums.Finances;

namespace RapidVet.Repository.Clinics
{
    public class MessageDistributionRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IMessageDistributionRepository
    {

        public MessageDistributionRepository()
        {

        }

        public MessageDistributionRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public void Increment(int ClinicId, int IncrementBy, MessageType type)
        {
            DateTime monthStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime monthEnd = monthStart.AddMonths(1);

            var messageDistributionCount = DataContext.MessageDistribution.SingleOrDefault(m => m.ClinicId == ClinicId && m.Created >= monthStart && m.Created < monthEnd);

            if(messageDistributionCount == null)
            {
                messageDistributionCount = new MessageDistributionCount()
                {
                    ClinicId = ClinicId,
                    EmailsCount = 0,
                    SmsCount = 0,
                    Created = DateTime.Now,
                    Updated = DateTime.Now
                };

                DataContext.MessageDistribution.Add(messageDistributionCount);
                DataContext.SaveChanges();
            }

            if(type == MessageType.Email)
            {
                messageDistributionCount.EmailsCount = messageDistributionCount.EmailsCount + IncrementBy;
            }
            else
            {
                messageDistributionCount.SmsCount = messageDistributionCount.SmsCount + IncrementBy;
            }

            messageDistributionCount.Updated = DateTime.Now;

            DataContext.SaveChanges();

        }

        public int MailsSentThisMonth(int ClinicId)
        {
            int counter = 0;
            DateTime monthStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime monthEnd = monthStart.AddMonths(1);

            var messageDistributionCount = DataContext.MessageDistribution.SingleOrDefault(m => m.ClinicId == ClinicId && m.Created >= monthStart && m.Created < monthEnd);

            if (messageDistributionCount != null)
            {
                counter = messageDistributionCount.EmailsCount;
            }

            return counter;
        }

        public MessageDistributionCount MessagesSentInMonth(int ClinicId, DateTime monthStart)
        {
            DateTime monthEnd = monthStart.AddMonths(1);

            return DataContext.MessageDistribution.SingleOrDefault(m => m.ClinicId == ClinicId && m.Created >= monthStart && m.Created < monthEnd);
                        
        }
    }
}