﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;

namespace RapidVet.Repository.Clinics
{
    public class IssuerRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IIssuerRepository
    {

        public IssuerRepository()
        {

        }

        public IssuerRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// creates issuer in db
        /// </summary>
        /// <param name="issuer">issuer object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus AddIssuer(Issuer issuer)
        {
            DataContext.Issuers.Add(issuer);
            return base.Save(issuer);
        }

        /// <summary>
        /// returns an initiated issuer object
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>Issuer</returns>
        public Issuer GetCreateModel(int clinicId)
        {
            return new Issuer()
            {
                Clinic = DataContext.Clinics.Single(c => c.Id == clinicId),
                ClinicId = clinicId,
            };
        }

        /// <summary>
        ///  all issuers in clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <param name="userID">user id</param>
        /// <returns>clinic object</returns>
        public List<Issuer> GetList(int clinicId, int userID)
        {
            var clinic = DataContext.Clinics.FirstOrDefault(x => x.Id == clinicId);
            if (clinic == null)
                return new List<Issuer>();
            
            if(userID == 1) //Admin 
            {
                return DataContext.Clinics.Include(c => c.Issuers)
                                         .Include(c => c.ClinicGroup)
                                         .Single(c => c.Id == clinicId).Issuers.ToList();
            }

            var validIssuers = (from t in DataContext.UserExtendedPermissions.Include(x => x.User)
                                where t.UserId == userID && t.User.ClinicGroupId == clinic.ClinicGroupID && t.ExtendedPermission == Model.Users.ExtendedPermissions.ViewFinancialReports
                                select t.ObjectId).ToList();

            return DataContext.Issuers.Where(x => validIssuers.Contains(x.Id)).ToList();
        }

        public List<Issuer> GetListByClinicGroup(int clinicGroupId)
        {
            return DataContext.Issuers.Include(i => i.Clinic)
                                      .Where(i => i.Clinic.ClinicGroupID == clinicGroupId && i.Active).ToList();
        }

        /// <summary>
        /// creates anew issure in db
        /// </summary>
        /// <param name="issuer">issuer object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Create(Issuer issuer)
        {
            DataContext.Issuers.Add(issuer);
            OperationStatus status = base.Save(issuer);
            if (status.Success)
            {
                status.Message = issuer.Name + " " + issuer.CompanyId;
            }

            return status;
        }

        /// <summary>
        /// returns an issuer object
        /// </summary>
        /// <param name="issuerId">id in issuers table</param>
        /// <returns>Issuer</returns>
        public Issuer GetItem(int issuerId)
        {
            return DataContext.Issuers.Include(i => i.Clinic).Include(i => i.CreditTypes).FirstOrDefault(i => i.Id == issuerId);
        }



        /// <summary>
        /// updates an issuer in db
        /// </summary>
        /// <param name="issuer"></param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(Issuer issuer)
        {
            return base.Save(issuer);
        }

        /// <summary>
        /// makes issuer not active in issuers table
        /// </summary>
        /// <param name="id">id in issuers table</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus RemoveIssuer(int id)
        {
            var issuer = DataContext.Issuers.Single(c => c.Id == id);
            issuer.Active = false;
            return base.Update(issuer);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clinicId"></param>
        /// <param name="showInactiveIssuers"></param>
        /// <param name="userID">for permision, -1 will ignore</param>
        /// <returns></returns>
        public List<SelectListItem> GetDropdownList(int clinicId, int userID, bool showInactiveIssuers = false)
        {            
            var clinic = DataContext.Clinics.FirstOrDefault(x => x.Id == clinicId);
            if (clinic == null)
                return new List<SelectListItem>();

            var validIssuers = (from t in DataContext.UserExtendedPermissions.Include(x => x.User)
                                where t.UserId == userID && t.User.ClinicGroupId == clinic.ClinicGroupID && t.ExtendedPermission == Model.Users.ExtendedPermissions.ViewFinancialReports
                                select t.ObjectId).ToList();

            if(userID == 1) //Admin
            {
                userID = -1;
            }

            var list = new List<SelectListItem>();
            IQueryable<Issuer> issuers;
            issuers = DataContext.Issuers.Where(i => (userID == -1 || validIssuers.Contains(i.Id)) && i.ClinicId == clinicId && (showInactiveIssuers || i.Active));
            foreach (var issuer in issuers)
            {
                list.Add(new SelectListItem()
                    {
                        Value = issuer.Id.ToString(),
                        Text = issuer.Name
                    });
            }
            return list;
        }

        public IQueryable<IssuerCreditType> GetCreditTypes(int issuerId)
        {
            return DataContext.IssuerCreditTypes.Where(i => i.IssuerId == issuerId && i.Active);
        }

        public IQueryable<EasyCardSettings> GetEasyCardSettings(int issuerId)
        {
            return DataContext.EasyCardSettings.Where(i => i.IssuerId == issuerId && i.Active);
        }

        public OperationStatus CreateCreditType(IssuerCreditType creditType)
        {
            DataContext.IssuerCreditTypes.Add(creditType);
            return base.Save(creditType);
        }

        public IssuerCreditType GetIssuerCreditType(int issuerCreditTypeId)
        {
            return DataContext.IssuerCreditTypes.FirstOrDefault(i => i.Id == issuerCreditTypeId);
        }

        public IssuerCreditType GetIssuerCreditTypeByName(string creditType)
        {
            return DataContext.IssuerCreditTypes.FirstOrDefault(i => i.Name.Equals(creditType));
        }

        public List<int> GetUsersIdsByIssuerId(int issuerId)
        {
            return DataContext.Users.Where(u => u.DefaultIssuerEmployerId.HasValue && u.DefaultIssuerEmployerId.Value == issuerId).Select(u => u.Id).ToList();
        }

        public OperationStatus CreateEasyCardTerminal(EasyCardSettings terminal)
        {
            DataContext.EasyCardSettings.Add(terminal);
            return base.Save(terminal);
        }

        public EasyCardSettings GetEasyCardTerminal(int terminalId)
        {
            return DataContext.EasyCardSettings.Single(c => c.Id == terminalId);
        }

        public List<EasyCardSettings> GetIssuersActiveEasyCardTerminals(int issuerId)
        {
            return DataContext.EasyCardSettings.Where(u => u.IssuerId == issuerId && u.Active).ToList();
        }

        public EasyCardSettings GetIssuersDefaultEasyCardTerminal(int issuerId)
        {
            return DataContext.EasyCardSettings.FirstOrDefault(u => u.IssuerId == issuerId && u.Active && u.IsDefault);
        }
    }
}
