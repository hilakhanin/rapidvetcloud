﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using RapidVet.Model.Users;

namespace RapidVet.Repository.Clinics
{
    public class UserExtendedPermissionsRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IUserExtendedPermissionsRepository
    {

        public UserExtendedPermissionsRepository()
        {

        }

        public UserExtendedPermissionsRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public List<UserExtendedPermissions> GetUsersByClinicGroupAndPermission(int clinicGroupId, ExtendedPermissions permission)
        {
            return DataContext.UserExtendedPermissions.Include(i => i.User).Where(u => u.User.ClinicGroupId == clinicGroupId && u.ExtendedPermission == permission).ToList();
        }

        public void SetUserIssuerPermissionByClinicGroup(int clinicGroupId, int userID, int issuerID, bool val)
        {
            var f = DataContext.UserExtendedPermissions.Include(i => i.User).FirstOrDefault(x => x.User.ClinicGroupId == clinicGroupId && x.UserId == userID && x.ObjectId == issuerID);
            if (f == null)
            {
                f = new UserExtendedPermissions()
                {
                    UserId = userID,
                    ObjectId = issuerID,
                };
                DataContext.UserExtendedPermissions.Add(f);
            }

            f.ExtendedPermission = val ? ExtendedPermissions.ViewFinancialReports : ExtendedPermissions.ViewFollowUps;
            DataContext.SaveChanges();
        }
    }
}
