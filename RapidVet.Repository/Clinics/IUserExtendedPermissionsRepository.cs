using System;
using System.Collections.Generic;
using RapidVet.Model.Users;

namespace RapidVet.Repository.Clinics
{
    public interface IUserExtendedPermissionsRepository : IDisposable
    {
        List<UserExtendedPermissions> GetUsersByClinicGroupAndPermission(int clinicGroupId, ExtendedPermissions permission);
        void SetUserIssuerPermissionByClinicGroup(int clinicGroupId, int userID, int issuerID, bool val);
    }
}