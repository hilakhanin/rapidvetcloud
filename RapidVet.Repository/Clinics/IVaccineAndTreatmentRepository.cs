using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.Users;
using RapidVet.WebModels.VaccinesAndTreatments;

namespace RapidVet.Repository.Clinics
{
    public interface IVaccineAndTreatmentRepository : IDisposable
    {
        /// <summary>
        /// gets all price list items of animal of a clinic
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <param name="animalKindId"> animal kind id</param>
        /// <returns>IQueryable(VaccineOrTreatment)</returns>
        IQueryable<VaccineOrTreatment> GetVaccineAndTreatmentOfAnimalKind(int clinicId, int animalKindId);

        /// <summary>
        /// get a single VaccineOrTreatment by it's id
        /// </summary>
        /// <param name="id">id of vaccineortreatment</param>
        /// <returns>VaccineOrTreatment</returns>
        VaccineOrTreatment GetItem(int id);


        /// <summary>
        /// get a single VaccineOrTreatment by priceListItemId and animal id
        /// </summary>
        /// <param name="priceListItemId">priceListItemId id</param>
        /// <param name="animalKindId">animalKindId id</param>
        /// <returns>VaccineOrTreatment</returns>
        VaccineOrTreatment GetItemByPriceListItemId(int animalKindId, int priceListItemId);

        /// <summary>
        /// update a vaccineOrTreatment object, save to db
        /// </summary>
        /// <param name="vaccineOrTreatment"> vaccineOrTreatment object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus UpdateItem(VaccineOrTreatment vaccineOrTreatment);

        /// <summary>
        /// create and save a vaccineOrTreatment object in db
        /// </summary>
        /// <param name="vaccineOrTreatment">vaccineOrTreatment object</param>
        /// <returns>OperationStatus</returns>
        void AddItem(VaccineOrTreatment vaccineOrTreatment);

        /// <summary>
        /// removes a vaccineOrTreatment from clinic
        /// </summary>
        /// <param name="id">vaccineOrTreatment to remove</param>
        /// <returns>OperationStatus</returns>
        void RemoveItem(int id);

        /// <summary>
        /// gets all next vaccineandtreatment items that has been set
        /// </summary>
        /// <param name="id"> vaccineortreatment id</param>
        /// <returns>IQueryable(VaccineOrTreatment)</returns>
        IQueryable<NextVaccineOrTreatment> GetNextVaccineAndTreatmentOfItem(int id);

        /// <summary>
        /// removes a vaccineOrTreatment from clinic
        /// </summary>
        /// <param name="nextVoTId">nextVaccineOrTreatment to remove</param>
        /// <param name="id">parent VoT to remove</param>
        /// <returns>OperationStatus</returns>
        void RemoveNextVoTItem(int nextVoTId);

        /// <summary>
        /// get a single VaccineOrTreatment by it's id
        /// </summary>
        /// <param name="id">parent VoT</param>
        /// <param name="nextVoTId">nextVaccineOrTreatment to get</param>
        /// <returns>NextVaccineOrTreatment</returns>
        NextVaccineOrTreatment GetNextVoTItem(int id, int nextVoTId);

        /// <summary>
        /// create and save a nextVaccineOrTreatment object in db
        /// </summary>
        /// <param name="nextVaccineOrTreatment">nextVaccineOrTreatment object</param>
        /// <returns>OperationStatus</returns>
        void AddNextVoTItem(NextVaccineOrTreatment nextVaccineOrTreatment);

        /// <summary>
        /// update a nextVaccineOrTreatment object, save to db
        /// </summary>
        /// <param name="nextVaccineOrTreatment"> nextVaccineOrTreatment object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus UpdateNextVoTItem(NextVaccineOrTreatment nextVaccineOrTreatment);


        /// <summary>
        /// get list of vaccine or treatment objects for patient
        /// </summary>
        /// <param name="patientId">patient id</param>
        /// <returns>List<VaccineOrTreatment></returns>
        List<VaccineOrTreatment> GetVaccineAndTreatmentByPatientId(int patientId);

        /// <summary>
        /// Get NextVaccineAndTreatment object list for Patient
        /// </summary>
        /// <param name="patientId">patientId</param>
        /// <returns>List<NextVaccineOrTreatment></returns>
        List<NextVaccineOrTreatment> GetNextVaccineAndTreatmentByPatientId(int patientId);


        /// <summary>
        /// goes through all NextVaccineOrTreatment items for clinic and detects changes by created / updated dates
        /// if change is detected, updates all schedualed unpreformed preventive medicine items for clinic patients
        /// with the new data
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>bool</returns>
        bool TemplateCahnged(int clinicId);

        void UpdateVaccineByPriceListItem(int priceListItemId, int newPriceListItemId);
        void SetInactiveByPriceListItemId(int priceListItemId);
        string GetPriceListItemNameByVaccineId(int nextVaccineId);
    }
}