﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.CliinicInventory;
using RapidVet.Model.Clinics;
using RapidVet.Model.Expenses;
using RapidVet.Model.Finance;
using RapidVet.Model.FullCalender;
using RapidVet.Model.Patients;
using RapidVet.Model.TreatmentPackages;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;
using RapidVet.Repository;
using System.Data.Entity;
using RapidVet.Repository.Clients;

namespace RapidVet.Repository.Clinics
{
    public class ClinicGroupDuplicateRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IClinicGroupDuplicateRepository
    {

        public ClinicGroupDuplicateRepository()
        {

        }

        public ClinicGroupDuplicateRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        private int saveCounter = 0;

        public void SaveChanges()
        {
            if (saveCounter > 50)
            {
                DataContext.SaveChanges();
                DataContext.Dispose();
                DataContext = new RapidVetDataContext();
                saveCounter = 0;
            }
            else
            {
                saveCounter++;
            }
        }


        public OperationStatus DuplicateClinicGroup(int clinicGroupId, string newClinicGroupName)
        {
            var status = new OperationStatus() { Success = false };

            var clinicGroupToCopy = DataContext.ClinicGroups.Include(c => c.Clinics).Single(cg => cg.Id == clinicGroupId);

            //Creating new ClinicGroup
            var newClinicGroup = new ClinicGroup()
                {
                    Active = clinicGroupToCopy.Active,
                    Name = newClinicGroupName,
                    BirthdayEmailSubject = clinicGroupToCopy.BirthdayEmailSubject,
                    BirthdayEmailTemplate = clinicGroupToCopy.BirthdayEmailTemplate,
                    //BirthdayLetterTemplate = clinicGroupToCopy.BirthdayLetterTemplate,
                    BirthdaySmsTemplate = clinicGroupToCopy.BirthdaySmsTemplate,
                    BirthdayStickerTemplate = clinicGroupToCopy.BirthdayStickerTemplate,
                    CalenderEntryEmailSubject = clinicGroupToCopy.CalenderEntryEmailSubject,
                    CalenderEntryEmailTemplate = clinicGroupToCopy.CalenderEntryEmailTemplate,
                    CalenderEntrySmsTemplate = clinicGroupToCopy.CalenderEntrySmsTemplate,
                    ClientsEmailSubject = clinicGroupToCopy.ClientsEmailSubject,
                    ClientsEmailTemplate = clinicGroupToCopy.ClientsEmailTemplate,
                    ClientsSmsTemplate = clinicGroupToCopy.ClientsSmsTemplate,
                    ClientsStikerTemplate = clinicGroupToCopy.ClientsStikerTemplate,
                    Comments = clinicGroupToCopy.Comments,
                    CreatedDate = DateTime.Now,
                    HasLogo = false,
                    MaxConcurrentUsers = 1, //dan instructions
                    TimeToDisconnectInactiveUsersInMinutes = clinicGroupToCopy.TimeToDisconnectInactiveUsersInMinutes,
                   // PersonalLetterTemplate = clinicGroupToCopy.PersonalLetterTemplate,
                    PostCardHight = clinicGroupToCopy.PostCardHight,
                    PostCardWidth = clinicGroupToCopy.PostCardWidth,
                    PreventiveMedicineEmailSubject = clinicGroupToCopy.PreventiveMedicineEmailSubject,
                    PreventiveMedicineEmailTemplate = clinicGroupToCopy.PreventiveMedicineEmailTemplate,
                    PreventiveMedicineEmailMultiAnimalsSubject = clinicGroupToCopy.PreventiveMedicineEmailMultiAnimalsSubject,
                    PreventiveMedicineEmailMultiAnimalsTemplate = clinicGroupToCopy.PreventiveMedicineEmailMultiAnimalsTemplate,
                    PreventiveMedicineLetterTemplate = clinicGroupToCopy.PreventiveMedicineLetterTemplate,
                    PreventiveMedicineLetterMultiAnimalsTemplate = clinicGroupToCopy.PreventiveMedicineLetterMultiAnimalsTemplate,
                    PreventiveMedicinePostCardTemplate = clinicGroupToCopy.PreventiveMedicinePostCardTemplate,
                    PreventiveMedicinePostCardMultiAnimalsTemplate = clinicGroupToCopy.PreventiveMedicinePostCardMultiAnimalsTemplate,
                    PreventiveMedicineSmsTemplate = clinicGroupToCopy.PreventiveMedicineSmsTemplate,
                    PreventiveMedicineSmsMultiAnimalsTemplate = clinicGroupToCopy.PreventiveMedicineSmsMultiAnimalsTemplate,
                    PreventiveMedicineStikerTemplate = clinicGroupToCopy.PreventiveMedicineStikerTemplate,
                    SignatureSmsTemplate = clinicGroupToCopy.SignatureSmsTemplate,
                    StickerSideMargin = clinicGroupToCopy.StickerSideMargin,
                    StikerColumns = clinicGroupToCopy.StikerColumns,
                    StikerRowHight = clinicGroupToCopy.StikerRowHight,
                    StikerRows = clinicGroupToCopy.StikerRows,
                    StikerTopMargin = clinicGroupToCopy.StikerTopMargin,
                    TreatmentEmailSubject = clinicGroupToCopy.TreatmentEmailSubject,
                    TreatmentEmailTemplate = clinicGroupToCopy.TreatmentEmailTemplate,
                    TreatmentSmsTemplate = clinicGroupToCopy.TreatmentSmsTemplate,
                    TreatmentStikerTemplate = clinicGroupToCopy.TreatmentStikerTemplate,
                };

            DataContext.ClinicGroups.Add(newClinicGroup);
            DataContext.SaveChanges();

            //Get all relevent objects of clinicGroup
            var clientStatuses = DataContext.ClientStatuses.Where(c => c.ClinicGroupId == clinicGroupId && c.Active).ToList();
            var animalColors = DataContext.AnimalColors.Where(c => c.ClinicGroupId == clinicGroupId && c.Active).ToList();
            var animalKinds = DataContext.AnimalKinds.Include(ak => ak.AnimalRaces).Where(c => c.ClinicGroupId == clinicGroupId && c.Active).ToList();
            var medications = DataContext.Medications.Where(m => m.ClinicGroupId == clinicGroupId && m.Active).ToList();

            //Copy ClientStatus
            foreach (var clientStatus in clientStatuses)
            {
                var newClientStatus = new ClientStatus()
                    {
                        Active = clientStatus.Active,
                        Name = clientStatus.Name,
                        ClinicGroupId = newClinicGroup.Id,
                        MeetingColor = "#33cccc"
                    };
                DataContext.ClientStatuses.Add(newClientStatus);
                SaveChanges();
            }

            //Copy AnimalColors
            foreach (var animalColor in animalColors)
            {
                var newAnimalColor = new AnimalColor()
                {
                    Active = animalColor.Active,
                    Value = animalColor.Value,
                    ClinicGroupId = newClinicGroup.Id,
                };
                DataContext.AnimalColors.Add(newAnimalColor);
                SaveChanges();
            }

            //Copy Medications
            foreach (var medication in medications)
            {
                var newMedication = new Medication()
                            {
                                Active = medication.Active,
                                ClinicGroupId = newClinicGroup.Id,
                                Name = medication.Name,
                                DangerousDrug = medication.DangerousDrug,
                                DoctorInfo = medication.DoctorInfo,
                                Dosage = medication.Dosage,
                                Insturctions = medication.Insturctions,
                                MedicationAdministrationTypeId = medication.MedicationAdministrationTypeId,
                                NumberOfDays = medication.NumberOfDays,
                                OldId = medication.OldId,
                                TimesPerDay = medication.TimesPerDay,
                                UnitPrice = medication.UnitPrice,
                                Updated = medication.Updated
                            };
                DataContext.Medications.Add(newMedication);
                SaveChanges();
            }

            //Copy AnimalRaces & AnimalKinds
            foreach (var animalKind in animalKinds)
            {
                var newAnimalKind = new AnimalKind()
                    {
                        Active = animalKind.Active,
                        Value = animalKind.Value,
                        ClinicGroupId = newClinicGroup.Id,
                        AnimalIconId = animalKind.AnimalIconId,
                        OldId = animalKind.Id,
                    };
                var animalRaces = new Collection<AnimalRace>();
                foreach (var animalRace in animalKind.AnimalRaces)
                {
                    var newAnimalRace = new AnimalRace()
                        {
                            Active = animalRace.Active,
                            Value = animalRace.Value,
                        };
                    animalRaces.Add(newAnimalRace);
                }
                newAnimalKind.AnimalRaces = animalRaces;

                DataContext.AnimalKinds.Add(newAnimalKind);
                SaveChanges();
            }

            //Copy Clinics
            foreach (var clinic in clinicGroupToCopy.Clinics.Where(c => c.Active))
            {
                var newClinic = new Clinic()
                    {
                        Active = true,
                        Name = clinic.Name,
                        CreatedDate = DateTime.Now,
                        AdvancedPaymentsPercents = clinic.AdvancedPaymentsPercents,
                        ApplyDividingIncomeModel = clinic.ApplyDividingIncomeModel,
                        ArrangeIncomeById = clinic.ArrangeIncomeById,
                        PerformDividingIncomeWithoutConciderationOfDistributedPayment = clinic.PerformDividingIncomeWithoutConciderationOfDistributedPayment,
                        AutomaticallySetFollowUpsAfterVisit = clinic.AutomaticallySetFollowUpsAfterVisit,
                        ClinicGroupID = newClinicGroup.Id,
                        DefaultFollowUpTime = clinic.DefaultFollowUpTime,
                        DefaultFollowUpType = clinic.DefaultFollowUpType,
                        DefaultFollowUpTypeId = clinic.DefaultFollowUpTypeId,
                        DefaultNewClientStatusId = clinic.DefaultNewClientStatusId,
                        DocumentHeader = clinic.DocumentHeader,
                        DollarExchangeRate = clinic.DollarExchangeRate,
                        EntryDuration = clinic.EntryDuration,
                        FollowUpFuture = clinic.FollowUpFuture,
                        FollowUpPast = clinic.FollowUpPast,
                        FollowUpTimeRange = clinic.FollowUpTimeRange,
                        FollowUpTimeRangeId = clinic.FollowUpTimeRangeId,
                        HSInitNumber = clinic.HSInitNumber,
                        InvoiceReciptOnly = clinic.InvoiceReciptOnly,
                        IssueTaxInvoiceOnly = clinic.IssueTaxInvoiceOnly,
                        QuotationPreface = clinic.QuotationPreface,
                        RegionalCouncilId = clinic.RegionalCouncilId,
                        RegionalVet = clinic.RegionalVet,
                        ShowComplaint = clinic.ShowComplaint,
                        ShowDiagnosis = clinic.ShowDiagnosis,
                        ShowExaminations = clinic.ShowExaminations,
                        ShowFollowUpsInPatientHistory = clinic.ShowFollowUpsInPatientHistory,
                        ShowMedicins = clinic.ShowMedicins,
                        ShowTemperature = clinic.ShowTemperature,
                        ShowTreatments = clinic.ShowTreatments,
                        ShowWeight = clinic.ShowWeight,
                        ShowOldVisits = clinic.ShowOldVisits,
                        ShowVisitNote = clinic.ShowVisitNote,
                        DefualtTariffId = clinic.DefualtTariffId
                    };
                DataContext.Clinics.Add(newClinic);
                DataContext.SaveChanges(); //This line is needed for Clinic and also for AnimalKind

                var creditCardCodes = DataContext.CreditCardCodes.Where(ccc => ccc.ClinicId == clinic.Id && ccc.Active).ToList();
                var holidays = DataContext.Holidays.Where(ccc => ccc.ClinicId == clinic.Id).ToList();
                var letterTemplates = DataContext.LetterTemplates.Where(ccc => ccc.ClinicId == clinic.Id).ToList();
                var visitFilters = DataContext.VisitFilters.Where(ccc => ccc.ClinicId == clinic.Id).ToList();
                var taxRates = DataContext.TaxRates.Where(ccc => ccc.ClinicId == clinic.Id).ToList();
                var priceListCategories = DataContext.PriceListCategories.Include(plc => plc.PriceListItems).Where(ccc => ccc.ClinicId == clinic.Id).ToList();
                var tariffs = DataContext.Tariffs.Include(t => t.TariffsItems).Where(ccc => ccc.ClinicId == clinic.Id && ccc.Active).ToList();
                var treatmentPackages = DataContext.TreatmentPackages.Include(t => t.Treatments).Where(ccc => ccc.ClinicId == clinic.Id).ToList();
                var suppliers = DataContext.Suppliers.Include(s => s.PriceListItems).Where(ccc => ccc.ClinicId == clinic.Id).ToList();
                var expenseGroups = DataContext.ExpenseGroups.Where(ccc => ccc.ClinicId == clinic.Id).ToList();
                var vaccineOrTreatments = DataContext.VaccineAndTreatments.Where(ccc => ccc.ClinicId == clinic.Id).ToList();
                var labTests = DataContext.LabTestTypes.Include("AnimalKinds.LabTestResults").Where(ccc => ccc.ClinicId == clinic.Id).ToList();



                //CreditCardCodes 
                foreach (var creditCardCode in creditCardCodes)
                {
                    var newCreditCardCode = new CreditCardCode()
                        {
                            Active = creditCardCode.Active,
                            ClinicId = newClinic.Id,
                            Name = creditCardCode.Name,
                        };
                    DataContext.CreditCardCodes.Add(newCreditCardCode);
                    SaveChanges();
                }
                //Holidays 
                foreach (var holiday in holidays)
                {
                    var newHoliday = new Holiday()
                    {
                        ClinicId = newClinic.Id,
                        Name = holiday.Name,
                        StartDay = holiday.StartDay,
                        EndDay = holiday.EndDay,
                        HasHolidayEve = holiday.HasHolidayEve,
                        StartDayString = holiday.StartDayString,
                        EndDayString = holiday.EndDayString,
                    };
                    DataContext.Holidays.Add(newHoliday);
                    SaveChanges();
                }
                //LetterTemplates 
                foreach (var letterTemplate in letterTemplates)
                {
                    var newLetterTemplate = new LetterTemplate()
                    {
                        ClinicId = newClinic.Id,
                        Content = letterTemplate.Content,
                        LetterTemplateTypeId = letterTemplate.LetterTemplateTypeId,
                    };
                    DataContext.LetterTemplates.Add(newLetterTemplate);
                    SaveChanges();
                }
                //VisitFilters 
                foreach (var visitFilter in visitFilters)
                {
                    var newVisitFilter = new VisitFilter()
                        {
                            ClinicId = newClinic.Id,
                            Name = visitFilter.Name,
                            AnimalKindId = visitFilter.AnimalKindId,
                            MainComplaint = visitFilter.MainComplaint,
                            ToDate = visitFilter.ToDate,
                            ToMonth = visitFilter.ToMonth,
                            CategoryId = visitFilter.CategoryId,
                            ClientId = visitFilter.ClientId,
                            DiagnosisId = visitFilter.DiagnosisId,
                            DoctorId = visitFilter.DoctorId,
                            ExaminationId = visitFilter.ExaminationId,
                            FromDate = visitFilter.FromDate,
                            FromMonth = visitFilter.FromMonth,
                            FromSum = visitFilter.FromSum,
                            FromYear = visitFilter.FromYear,
                            ToSum = visitFilter.ToSum,
                            ToYear = visitFilter.ToYear,
                            TreatmentId = visitFilter.TreatmentId,
                            VisitDescription = visitFilter.VisitDescription,
                        };
                    DataContext.VisitFilters.Add(newVisitFilter);
                    SaveChanges();
                }
                //TaxRates 
                foreach (var taxRate in taxRates)
                {
                    var newTaxRate = new TaxRate()
                    {
                        ClinicId = newClinic.Id,
                        Rate = taxRate.Rate,
                        TaxRateChangedDate = taxRate.TaxRateChangedDate,
                    };
                    DataContext.TaxRates.Add(newTaxRate);
                    SaveChanges();
                }
                //priceListCategories
                foreach (var priceListCategory in priceListCategories)
                {
                    var newPriceListCategory = new PriceListCategory()
                    {
                        ClinicId = newClinic.Id,
                        Name = priceListCategory.Name,
                        Available = priceListCategory.Available,
                        IsInventory = priceListCategory.IsInventory,
                        OriginalName = priceListCategory.OriginalName,
                        OldId = priceListCategory.Id,
                        ShortName = priceListCategory.ShortName,
                    };
                    var priceListItems = new Collection<PriceListItem>();
                    foreach (var priceListItem in priceListCategory.PriceListItems.ToList())
                    {
                        var newPriceListItem = new PriceListItem()
                        {
                            Name = priceListItem.Name,
                            Available = priceListItem.Available,
                            OldId = priceListItem.Id,
                            Barcode = priceListItem.Barcode,
                            Catalog = priceListItem.Catalog,
                            CopyFixedPrice = priceListItem.CopyFixedPrice,
                            DefultConsuptionAmount = priceListItem.DefultConsuptionAmount,
                            FixedPrice = priceListItem.FixedPrice,
                            InventoryActive = priceListItem.InventoryActive,
                            ItemTypeId = priceListItem.ItemTypeId,
                            ItemsInUnit = priceListItem.ItemsInUnit,
                            Manufacturer = priceListItem.Manufacturer,
                            RequireBarcode = priceListItem.RequireBarcode,
                            CreatedDate = priceListItem.CreatedDate,
                            CreatedById = priceListItem.CreatedById,
                        };
                        priceListItems.Add(newPriceListItem);
                    }
                    newPriceListCategory.PriceListItems = priceListItems;
                    DataContext.PriceListCategories.Add(newPriceListCategory);
                    DataContext.SaveChanges();
                }
                //Tariffs 
                foreach (var tariff in tariffs)
                {
                    var newTariff = new Tariff()
                    {
                        ClinicId = newClinic.Id,
                        Active = tariff.Active,
                        Name = tariff.Name,
                        OldId = tariff.Id,

                    };
                    var priceListItemTariffs = new Collection<PriceListItemTariff>();
                    foreach (var tariffItem in tariff.TariffsItems.ToList())
                    {
                        var newPriceListItemTariff = new PriceListItemTariff()
                        {
                            ItemId = DataContext.PriceListItems.Include(p => p.Category).Single(p => p.Category.ClinicId == newClinic.Id && p.OldId == tariffItem.ItemId).Id,
                            Price = tariffItem.Price,
                        };
                        priceListItemTariffs.Add(newPriceListItemTariff);
                    }
                    newTariff.TariffsItems = priceListItemTariffs;
                    DataContext.Tariffs.Add(newTariff);
                    DataContext.SaveChanges();
                }
                //TreatmentPackages 
                foreach (var treatmentPackage in treatmentPackages)
                {
                    var newTreatmentPackage = new TreatmentPackage()
                    {
                        Name = treatmentPackage.Name,
                        ClinicId = newClinic.Id,
                    };
                    var packageItems = new Collection<PackageItem>();
                    foreach (var packageItem in treatmentPackage.Treatments.ToList())
                    {
                        var newPackageItem = new PackageItem()
                        {
                            PriceListItemId = DataContext.PriceListItems.Include(p => p.Category).Single(p => p.Category.ClinicId == newClinic.Id && p.OldId == packageItem.PriceListItemId).Id,
                            Amount = packageItem.Amount,
                            PercentDiscount = packageItem.PercentDiscount,
                            Discount = packageItem.Discount,
                            Comments = packageItem.Comments,
                            Price = packageItem.Price,
                        };
                        packageItems.Add(newPackageItem);
                    }
                    newTreatmentPackage.Treatments = packageItems;
                    DataContext.TreatmentPackages.Add(newTreatmentPackage);
                    SaveChanges();
                }
                //Supplier 
                foreach (var supplier in suppliers)
                {
                    var newSupplier = new Supplier()
                    {
                        Name = supplier.Name,
                        ClinicId = newClinic.Id,
                        OldId = supplier.Id,
                        Address = supplier.Address,
                        ContactNameA = supplier.ContactNameA,
                        Phone = supplier.Phone,
                        CompanyId = supplier.CompanyId,
                        ContactCommentA = supplier.ContactCommentA,
                        ContactCommentB = supplier.ContactCommentB,
                        ContactNameB = supplier.ContactNameB,
                        ContactPhoneA = supplier.ContactPhoneA,
                        ContactPhoneB = supplier.ContactPhoneB,
                        ExternalId = supplier.ExternalId,
                    };
                    var priceListItemSuppliers = new Collection<PriceListItemSupplier>();
                    foreach (var priceListItem in supplier.PriceListItems.ToList())
                    {
                        var priceListItemSupplier = new PriceListItemSupplier()
                        {
                            PriceListItemId = DataContext.PriceListItems.Include(p => p.Category).Single(p => p.Category.ClinicId == newClinic.Id && p.OldId == priceListItem.PriceListItemId).Id,
                            CostPrice = priceListItem.CostPrice,
                            DefaultSupplier = priceListItem.DefaultSupplier,
                        };
                        priceListItemSuppliers.Add(priceListItemSupplier);
                    }
                    newSupplier.PriceListItems = priceListItemSuppliers;
                    DataContext.Suppliers.Add(newSupplier);
                    DataContext.SaveChanges();
                }
                //ExpenseGroups
                foreach (var expenseGroup in expenseGroups)
                {
                    var newExpenseGroup = new ExpenseGroup()
                    {
                        Name = expenseGroup.Name,
                        ClinicId = newClinic.Id,
                        ExternalId = expenseGroup.ExternalId,
                        IsTextDeductible = expenseGroup.IsTextDeductible,
                        RecognitionPercent = expenseGroup.RecognitionPercent,
                    };
                    DataContext.ExpenseGroups.Add(newExpenseGroup);
                    SaveChanges();
                }
                //VaccineOrTreatment
                foreach (var vaccineOrTreatment in vaccineOrTreatments)
                {
                    var newVaccineOrTreatment = new VaccineOrTreatment()
                    {
                        ClinicId = newClinic.Id,
                        AnimalKindId = DataContext.AnimalKinds.Single(ak => ak.ClinicGroupId == newClinicGroup.Id && ak.OldId == vaccineOrTreatment.AnimalKindId).Id,
                        Order = vaccineOrTreatment.Order,
                        PriceListItemId = DataContext.PriceListItems.Include(p => p.Category).Single(p => p.Category.ClinicId == newClinic.Id && p.OldId == vaccineOrTreatment.PriceListItemId).Id,
                        OldId = vaccineOrTreatment.Id,
                    };
                    var nextVaccinesAndTreatments = new Collection<NextVaccineOrTreatment>();
                    foreach (var vot in vaccineOrTreatment.NextVaccinesAndTreatments.ToList())
                    {
                        var nextVaccineOrTreatment = new NextVaccineOrTreatment()
                        {
                            DaysToNext = vot.DaysToNext,
                            OldVoTId = vot.VaccineOrTreatmentId,
                            VaccineOrTreatmentId = vot.VaccineOrTreatmentId,
                            Created = vot.Created,
                            Updated = vot.Updated,
                        };
                        nextVaccinesAndTreatments.Add(nextVaccineOrTreatment);
                    }
                    newVaccineOrTreatment.NextVaccinesAndTreatments = nextVaccinesAndTreatments;
                    DataContext.VaccineAndTreatments.Add(newVaccineOrTreatment);
                    DataContext.SaveChanges();
                }
                var nextVoT = DataContext.NextVaccineAndTreatments.Include(nvot => nvot.ParentVaccineOrTreatment).Where(nvot => nvot.ParentVaccineOrTreatment.ClinicId == newClinic.Id).ToList();
                foreach (var nvot in nextVoT)
                {
                    nvot.VaccineOrTreatmentId = DataContext.VaccineAndTreatments.Single(vot => vot.ClinicId == newClinic.Id && nvot.OldVoTId == vot.OldId).Id;
                }
                SaveChanges();
                //LabTest
                foreach (var labTestType in labTests)
                {
                    var newLabTestType = new LabTestType()
                    {
                        ClinicId = newClinic.Id,
                        Name = labTestType.Name,
                    };
                    var labTestTemplates = new Collection<LabTestTemplate>();
                    foreach (var labTestTemplate in labTestType.AnimalKinds.ToList())
                    {
                        var newLabTestTemplate = new LabTestTemplate()
                            {
                                Name = labTestTemplate.Name,
                                AnimalKindId = DataContext.AnimalKinds.Single(ak => ak.ClinicGroupId == newClinicGroup.Id && ak.OldId == labTestTemplate.AnimalKindId).Id,
                            };
                       
                        var labTestResults = new Collection<LabTest>();
                        foreach (var labTest in labTestTemplate.LabTestResults.ToList())
                        {
                            var newLabTest = new LabTest()
                                {
                                    Name = labTest.Name,
                                    MaxValue = labTest.MaxValue,
                                    MinValue = labTest.MinValue,
                                };
                            labTestResults.Add(newLabTest);
                        }
                        newLabTestTemplate.LabTestResults = labTestResults;
                        labTestTemplates.Add(newLabTestTemplate);
                    }
                    newLabTestType.AnimalKinds = labTestTemplates;
                    DataContext.LabTestTypes.Add(newLabTestType);
                    SaveChanges();
                }
            }

            newClinicGroup.CreatedDate = DateTime.Now;
            status.Success = DataContext.SaveChanges() >= 0;
            return status;
        }
    }
}