﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clinics;

namespace RapidVet.Repository.Clinics
{
    public class AttendanceLogRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IAttendanceLogRepository
    {
        public AttendanceLogRepository()
        {

        }

        public AttendanceLogRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public AttendanceLog GetTodayLastLogIn(int clinicId, int userId)
        {
            var now = DateTime.Now;

            return DataContext.AttendanceLogs.Where(l => l.UserId == userId && l.ClinicId == clinicId && !l.EndTime.HasValue && l.StartTime.HasValue
                                                   && l.StartTime.Value.Year == now.Year && l.StartTime.Value.Month == now.Month && l.StartTime.Value.Day == now.Day)
                                                   .OrderByDescending(l => l.StartTime).FirstOrDefault();
        }

        public AttendanceLog GetItem(int clinicId, int id)
        {
            return DataContext.AttendanceLogs.FirstOrDefault(l => l.Id == id && l.ClinicId == clinicId);
        }

        public DateTime? GetLastRecordedLogOut(int clinicId, int userId)
        {
            var lastLogOut = DataContext.AttendanceLogs.Where(l => l.UserId == userId && l.ClinicId == clinicId && l.EndTime.HasValue).OrderByDescending(l => l.EndTime).FirstOrDefault();

            return lastLogOut != null ? lastLogOut.EndTime.Value : (DateTime?)null;
        }

        public DateTime? GetLastRecordedLogIn(int clinicId, int userId)
        {
            var lastLogIn = DataContext.AttendanceLogs.Where(l => l.UserId == userId && l.ClinicId == clinicId && l.StartTime.HasValue).OrderByDescending(l => l.StartTime).FirstOrDefault();

            return lastLogIn != null ? lastLogIn.StartTime.Value : (DateTime?)null;
        }

        public OperationStatus CreateNewLog(int clinicId, int userId, DateTime? startTime, DateTime? endTime)
        {
            var log = new AttendanceLog()
                {
                    UserId = userId,
                    ClinicId = clinicId,
                    StartTime = startTime,
                    EndTime = endTime,
                };
            DataContext.AttendanceLogs.Add(log);
            return base.Save(log);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <param name="startDate"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns>bool of result success</returns>
        public bool SaveLogReport(int clinicId, int id, int userId, DateTime startDate, TimeSpan startTime, TimeSpan endTime)
        {
            var f = DataContext.AttendanceLogs.FirstOrDefault(x => x.ClinicId == clinicId && x.Id == id && x.UserId == userId);
            if(f == null)
                return false;

            f.StartTime = startDate.Date.Add(startTime);
            f.EndTime = startDate.Date.Add(endTime);
            DataContext.SaveChanges();
            return true;
        }

        public bool DeleteLogReport(int clinicId, int id, int userId)
        {
            var f = DataContext.AttendanceLogs.FirstOrDefault(x => x.ClinicId == clinicId && x.Id == id && x.UserId == userId);
            if (f == null)
                return false;

            DataContext.AttendanceLogs.Remove(f);
            DataContext.SaveChanges();
            return true;
        }

        public List<AttendanceLog> GetAttendanceLogsOfUser(int clinicId, int userId, DateTime fromTime, DateTime toTime)
        {
            var to = (toTime.Date).AddDays(1);

            return DataContext.AttendanceLogs.Include("User").Where(l => l.ClinicId == clinicId && l.UserId == userId &&
                ((fromTime <= l.StartTime && l.StartTime <= to) || (l.EndTime.HasValue && (fromTime <= l.EndTime && l.EndTime <= to)))).OrderByDescending(l => l.StartTime)
                .OrderBy(t=>t.StartTime.HasValue ? t.StartTime.Value : t.EndTime.Value).ToList();
        }

        public List<AttendanceLog> GetAllUsersAttendanceLogsOfClinic(int clinicId, DateTime fromTime, DateTime toTime)
        {
            var to = (toTime.Date).AddDays(1);

            return DataContext.AttendanceLogs.Include("User").Where(l => l.ClinicId == clinicId &&
                ((l.StartTime.HasValue && (fromTime <= l.StartTime && l.StartTime <= to)) || (l.EndTime.HasValue && (fromTime <= l.EndTime && l.EndTime <= to)))).OrderByDescending(l => l.StartTime).ToList();
        }
    }
}
