﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Model;
using System.Data.Entity;
using RapidVet.Model.Finance;

namespace RapidVet.Repository.Finances
{
    public class PriceListItemRepository : RepositoryBase<RapidVetDataContext>, IPriceListItemRepository
    {

        public PriceListItemRepository()
        {

        }

        public PriceListItemRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// create a priceListItem in db
        /// </summary>
        /// <param name="item">PriceListItem object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Create(PriceListItem item)
        {
            // item.Category = DataContext.PriceListCategories.Single(c => c.Id == item.CategoryId);

            DataContext.PriceListItems.Add(item);

            return base.Save(item);
        }

        /// <summary>
        /// update a PriceListObject in db
        /// </summary>
        /// <param name="item"></param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(PriceListItem item)
        {
            return base.Save(item);
        }


        /// <summary>
        /// make a PriceListItem object to be not active in db
        /// </summary>
        /// <param name="id">PriceListItem id</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Delete(int id)
        {
            var item = DataContext.PriceListItems.Single(i => i.Id == id);
            item.Available = false;
            return base.Update(item);
        }

        /// <summary>
        /// returns all active PriceListItems in PriceListCategory
        /// </summary>
        /// <param name="categoryId">PriceListCategoryId</param>
        /// <returns>List(PriceListItem)</returns>
        public List<PriceListItem> GetList(int categoryId, bool displayInactiveItems)
        {
            var category = DataContext.PriceListCategories.Single(c => c.Id == categoryId);

            List<PriceListItem> data;
            
            if(!displayInactiveItems)
            {
                data = DataContext.PriceListItems
                                  .Where(i => i.CategoryId == categoryId && i.Available)
                                  .OrderBy(i => i.Name).ToList();
            
            }
            else
            {
                data = DataContext.PriceListItems
                                  .Where(i => i.CategoryId == categoryId)
                                  .OrderBy(i => i.Name).ToList();
            }

            foreach (var priceListItem in data)
            {
                priceListItem.ItemsTariffs =
                    DataContext.PriceListItemsTariff.Include(p => p.Tariff)
                               .Where(p => p.ItemId == priceListItem.Id && p.Tariff.Active)
                               .ToList();
            }

            foreach (var price in data.SelectMany(item => item.ItemsTariffs))
            {
                price.TariffName = price.Tariff.Name;
            }

            return data;
        }

        /// <summary>
        /// updates an item price in db --> PriceListItemTariff
        /// </summary>
        /// <param name="source">PriceListItemTariff </param>
        /// <returns>OperationStatus</returns>
        public OperationStatus UpdatePrice(PriceListItemTariff source)
        {
            var status = new OperationStatus();

            lock (DataContext.PriceListItemsTariff)
            {
                var target =
                        DataContext.PriceListItemsTariff.SingleOrDefault(
                            p => p.ItemId == source.ItemId && p.TariffId == source.TariffId);

                if (target == null)
                {
                    DataContext.PriceListItemsTariff.Add(source);
                    status = base.Save(source);
                }
                else
                {
                    target.Price = source.Price;
                    status = base.Update(target);
                } 
            }

            return status;
        }


        /// <summary>
        /// returns a specific PriceListItem
        /// </summary>
        /// <param name="id">PrideListItem id</param>
        /// <returns>PriceListItem</returns>
        public PriceListItem GetItem(int id)
        {
            return DataContext.PriceListItems.Include(p=>p.VaccineOrTreatment).Single(i => i.Id == id);
        }

        /// <summary>
        /// returns all PriceListItemTypes
        /// </summary>
        /// <returns>  List(PriceListItemType)</returns>
        public List<PriceListItemType> ItemTypes()
        {
            return DataContext.PriceListItemTypes.ToList();
        }

        /// <summary>
        /// return all PriceListItems that are of type 'food' in clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>   List(PriceListItem)</returns>
        public List<PriceListItem> GetClinicFoodTypes(int clinicId)
        {
            var itemType =
                DataContext.PriceListItemTypes.Single(i => i.Id == (int)PriceListItemTypeEnum.Food);

            var categories =
                DataContext.PriceListCategories.Include(c => c.PriceListItems)
                           .Where(c => c.ClinicId == clinicId)
                           .ToList();
            var foodTypes = new List<PriceListItem>();
            foreach (var category in categories)
            {
                foodTypes.AddRange(category.PriceListItems.Where(priceListItem => priceListItem.ItemTypeId == itemType.Id));
            }

            return foodTypes.OrderBy(f => f.Name).ToList();
        }

        public IQueryable<PriceListItem> GetPriceListItemsForClient(int clientId, out Dictionary<int, decimal> Dic)
        {
            var client = DataContext.Clients.Include(c => c.Clinic).Single(c => c.Id == clientId);

            var tariffId = client.TariffId.HasValue && client.TariffId > 0
                               ? client.TariffId.Value
                               : client.Clinic.DefualtTariffId;

            if (tariffId == 0)
            {
                var tariff = DataContext.Tariffs.FirstOrDefault(t => t.ClinicId == client.ClinicId && t.Active);
                if (tariff != null)
                {
                    tariffId = tariff.Id;
                }

            }

            var clinicCategoryIds =
                DataContext.PriceListCategories.Where(plc => plc.Available && plc.ClinicId == client.ClinicId)
                           .Select(plc => plc.Id);

            Dic = new Dictionary<int, decimal>();

            var itemsJoin =
                DataContext.PriceListItems.Include(pli => pli.PriceListItemType)
                           .Where(pli => pli.Available && clinicCategoryIds.Contains(pli.CategoryId) && pli.ItemTypeId != (int)PriceListItemTypeEnum.PreventiveTreatments &&
                     pli.ItemTypeId != (int)PriceListItemTypeEnum.Vaccines);

            decimal temp = 0;
            var tariffItemsPricesJoin = (DataContext.PriceListItemsTariff.Where(plit => plit.TariffId == tariffId)
                 .Select(b => b))
                 .Join(itemsJoin,
                 i => i.ItemId, p => p.Id,
                 (i, p) => new { i, p })
                 .Select(b => b);
            foreach (var item in tariffItemsPricesJoin)
            {
                
                item.p.ItemsTariffs.Add(new PriceListItemTariff()
                {
                    ItemId = item.i.ItemId,
                    TariffId = tariffId,
                    Price = item.i.Price                    
                });
                
                if (!(Dic.TryGetValue(item.i.ItemId, out temp)))
                    Dic.Add(item.i.ItemId, item.i.Price);
            }
            
            return itemsJoin;
        }

        /// <summary>
        /// return a list of all items with vaccine or treatment type
        /// </summary>
        ///  <param name="clinicId">clinic id</param>
        /// <returns>list of price list items</returns>
        public List<PriceListItem> GetAllItemsOfTypeVaccineOrTreatment(int clinicId)
        {
            var itemTypeVaccine =
               DataContext.PriceListItemTypes.Single(i => i.Id == (int)PriceListItemTypeEnum.Vaccines);

            var itemTypeTreatment =
                DataContext.PriceListItemTypes.Single(i => i.Id == (int)PriceListItemTypeEnum.PreventiveTreatments);

            var categories =
                DataContext.PriceListCategories.Include(c => c.PriceListItems)
                           .Where(c => c.ClinicId == clinicId)
                           .ToList();
            var vaccineAndTreatmentTypes = new List<PriceListItem>();
            foreach (var category in categories)
            {
                vaccineAndTreatmentTypes.AddRange(category.PriceListItems.Where(priceListItem => priceListItem.ItemTypeId == itemTypeVaccine.Id));
                vaccineAndTreatmentTypes.AddRange(category.PriceListItems.Where(priceListItem => priceListItem.ItemTypeId == itemTypeTreatment.Id));
            }

            return vaccineAndTreatmentTypes.OrderBy(f => f.Name).ToList();
        }
    }
}
