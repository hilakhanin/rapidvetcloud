﻿using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Finance;
using System.Web.Mvc;

namespace RapidVet.Repository.Finances
{
    public interface IPriceListCategoryRepository : IDisposable
    {
        /// <summary>
        /// get all price list categories in clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>IQueryable(PriceListCategory)</returns>
        IQueryable<PriceListCategory> GetList(int clinicId);

        /// <summary>
        /// create a price list category in clinic
        /// </summary>
        /// <param name="category">priceListCategory object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(PriceListCategory category);

        /// <summary>
        /// gets a specific priceListCategory object
        /// </summary>
        /// <param name="id">id in priceListcategories table</param>
        /// <returns>PriceListCategory</returns>
        PriceListCategory GetItem(int id);

        /// <summary>
        /// save (update) a PriceListCategory in db
        /// </summary>
        /// <param name="category"> PriceListCategory object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(PriceListCategory category);

        /// <summary>
        /// make a PriceListCategory ojbect inactive in db
        /// </summary>
        /// <param name="id">id in PriceListCategories table</param>
        /// <returns>OperationStatus</returns>
        OperationStatus RemoveCategoryFromClinic(int id);

        /// <summary>
        /// returns a list of all priceListCategories id's in clinic
        /// </summary>
        /// <param name="id">clinic id</param>
        /// <returns></returns>
        List<int> GeIdList(int id);

        /// <summary>
        /// return a list of priceListItem in category
        /// </summary>
        /// <param name="id">PriceListCategory Id</param>
        /// <returns>PriceListCategory</returns>
        PriceListCategory GetSimpleItem(int id);

        /// <summary>
        /// get all active PriceListCategories in clinic
        /// </summary>
        /// <param name="clinicId">clinic Id</param>
        /// <returns>IQueryable(PriceListCategory)</returns>
        IQueryable<PriceListCategory> GetClinicActiveCategories(int clinicId, bool forVisit = false);


        /// <summary>
        /// gets priceListItemCategoryId for priceListItem
        /// </summary>
        /// <param name="priceListItemId">price list item id</param>
        /// <returns>int category id</returns>
        int GetPriceListItemCategoryId(int priceListItemId);

        /// <summary>
        /// get price list category name
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns>int price list category id</returns>
        string GetName(int categoryId);

        /// <summary>
        /// gets category name for item
        /// </summary>
        /// <param name="priceListItemId">int? price list item id</param>
        /// <returns>category name</returns>
        string GetNameByPriceListItemId (int? priceListItemId);

        List<SelectListItem> GetDropdownList(int clinicId);
    }
}