﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using System.Data.Entity;
using RapidVet.RapidConsts;
using RapidVet.WebModels.Finance;
using EntityFramework.Extensions;
using RapidVet.Model.CliinicInventory;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Quotations;
using RapidVet.Model.TreatmentPackages;
using RapidVet.Model.Visits;
using System.Data.Entity.Infrastructure;
using System.Reflection;

namespace RapidVet.Repository.Finances
{
    public class PriceListRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IPriceListRepository
    {
        public PriceListRepository()
        {

        }

        public PriceListRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        //in use for new visit
        public Clinic GetClinic(int clinicId)
        {
            return DataContext.Clinics.Include(c => c.PriceListCategories)
                .Include("PriceListCategories.PriceListItems")
                .Include("PriceListCategories.PriceListItems.ItemsTariffs")
                .Single(c => c.Id == clinicId);
        }

        //currently not in use
        public OperationStatus CreatePriceListItem(PriceListItem item)
        {
            item.Category = DataContext.PriceListCategories.Single(c => c.Id == item.CategoryId);
            DataContext.PriceListItems.Add(item);
            return base.Save(item);
        }

        public PriceListItem GetPriceListItem(int itemId)
        {
            return DataContext.PriceListItems.Include(p => p.PriceListItemType).Include(p => p.Category).Include(p => p.ItemsTariffs).Single(pli => pli.Id == itemId);
        }

        //currently not in use
        public OperationStatus UpdatePriceListItem(PriceListItem source)
        {
            var target = DataContext.PriceListItems.Single(i => i.Id == source.Id);

            target.Name = source.Name;
            target.ItemTypeId = source.ItemTypeId;
            target.Available = source.Available;
            return base.Update(target);
        }


        //currently not in use
        public OperationStatus RemoveCategoryFromClinic(int categoryId)
        {
            var target = DataContext.PriceListCategories.Single(c => c.Id == categoryId);
            target.Available = false;
            OperationStatus status = base.Update(target);
            status.ItemId = target.ClinicId;
            return status;

        }

        //currently not in use
        public OperationStatus RemoveItemFromCategory(int itemId)
        {
            var target = DataContext.PriceListItems.Single(i => i.Id == itemId);
            target.Available = false;
            OperationStatus status = base.Update(target);
            status.ItemId = target.CategoryId;
            return status;
        }



        /// <summary>
        /// moves all priceListItems prices in tariff by a fixed amount
        /// </summary>
        /// <param name="tariffId"> id of the tariff </param>
        /// <param name="method">change price by numbers or by precentage</param>
        /// <param name="action">increase or decrease</param>
        /// <param name="amount">actual number</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus ShiftPrices(int tariffId, PriceShiftMethod method, PriceShiftAction action, decimal amount)
        {
            var tariff = DataContext.Tariffs.Include(t => t.TariffsItems).Include("TariffsItems.Item").Single(t => t.Id == tariffId);
            var precentage = method == PriceShiftMethod.Precentage;
            var increase = action == PriceShiftAction.Increase;
            if (precentage)
            {
                switch (action)
                {
                    case PriceShiftAction.Increase:
                        amount = (amount / 100) + 1;
                        break;
                    case PriceShiftAction.Decrease:
                        amount = 1 - (amount / 100);
                        break;
                }
            }
            foreach (var item in tariff.TariffsItems)
            {
                if (!item.Item.FixedPrice) //if the item is marked as fixed price - do not update it's price
                {
                    if (increase)
                    {
                        if (precentage)
                        {
                            item.Price = item.Price * amount;
                        }
                        else
                        {
                            item.Price += amount;
                        }
                    }
                    else //decrease
                    {
                        if (precentage)
                        {
                            item.Price = item.Price * amount;
                        }
                        else
                        {
                            if (amount > item.Price)
                            {
                                item.Price = 0;
                            }
                            else
                            {
                                item.Price -= amount;
                            }
                        }
                    }
                }
            }

            return base.Update(tariff);
        }



        /// <summary>
        /// duplicates prices between different tariffs in the same clinic
        /// </summary>
        /// <param name="sourceTariff">source tariff id</param>
        /// <param name="destinationTariff">target tariff id</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus DuplicatePricesBetweenTariffs(int sourceTariff, int destinationTariff)
        {
            var source = DataContext.Tariffs.Include(t => t.TariffsItems).Single(t => t.Id == sourceTariff);
            var target = DataContext.Tariffs.Include(t => t.TariffsItems).Include("TariffsItems.Item").Single(t => t.Id == destinationTariff);

            foreach (var sourceItem in source.TariffsItems)
            {
                var targetItem =
                    target.TariffsItems.SingleOrDefault(
                        t => t.ItemId == sourceItem.ItemId);
                if (targetItem != null && !targetItem.Item.FixedPrice) // if tarhet item is of fixed price, price stays as is
                {
                    targetItem.Price = sourceItem.Price;
                }
                else if (targetItem == null)
                {
                    targetItem = new PriceListItemTariff()
                        {
                            ItemId = sourceItem.ItemId,
                            TariffId = sourceItem.TariffId,
                            Price = sourceItem.Price
                        };
                    target.TariffsItems.Add(targetItem);
                }

            }

            return base.Update(target);
        }


        /// <summary>
        /// duplicate the all priceListCategories, PriceListItems , Tariffs and PriceListItemTariffs 
        /// between clinics in the same clinic group
        /// </summary>
        /// <param name="sourceClinicId">clinic id from which the data is taken</param>
        /// <param name="targetClinicIds">clinic id's (can be more then one) in which this data will be copied to. 
        /// all existing data in these clinics will be made not active/</param>
        /// <param name="userId">id of the preforming user</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus DuplicatePricesBetweenClinics(int sourceClinicId, List<int> targetClinicIds, int userId)
        {
            var status = new OperationStatus() { Success = false };
            var sourceCategories =
                DataContext.PriceListCategories.Where(c => c.ClinicId == sourceClinicId && c.Available);
            var counter = 0;
            foreach (var clinicId in targetClinicIds)
            {
                var disableStatus = DisableClinicExistingData(clinicId, userId);
                if (disableStatus.Success)
                {
                    var addingStatus = CopyDataToTargetClinic(sourceClinicId, clinicId, userId);
                    if (addingStatus.Success)
                    {
                        counter++;
                    }
                }

            }

            if (targetClinicIds.Count == counter)
            {
                status.Success = true;
            }
            return status;
        }

        public int GetItemTypeId(int priceListItemId)
        {
            return DataContext.PriceListItems.Single(pli => pli.Id == priceListItemId).ItemTypeId;
        }

        public List<PriceListItem> GetTreatments(int clinicId)
        {
            //25.01.16 - Shira - removing vaccines and preventive treatments
            var categoryIds = GetCategoryIds(clinicId);
            return
                DataContext.PriceListItems
                .Include(pli => pli.ItemsTariffs)
                .Include(pli => pli.PriceListItemType)
                .Where(
                    pli =>
                    pli.Available &&
                    (pli.ItemTypeId != (int)PriceListItemTypeEnum.Examinations &&
                     pli.ItemTypeId != (int)PriceListItemTypeEnum.LabTests &&
                     pli.ItemTypeId != (int)PriceListItemTypeEnum.PreventiveTreatments &&
                     pli.ItemTypeId != (int)PriceListItemTypeEnum.Vaccines) &&
                    categoryIds.Contains(pli.CategoryId)).OrderBy(pli => pli.Name).ToList();
        }

        public List<PriceListItem> GetExaminations(int clinicId)
        {
            var categoryIds = GetCategoryIds(clinicId);
            return
                DataContext.PriceListItems
                           .Include(pli => pli.PriceListItemType)
                           .Include(pli => pli.ItemsTariffs)
                           .Where(
                               pli =>
                               pli.Available &&
                               (pli.ItemTypeId == (int)PriceListItemTypeEnum.Examinations ||
                                pli.ItemTypeId == (int)PriceListItemTypeEnum.LabTests) &&
                               categoryIds.Contains(pli.CategoryId)).OrderBy(pli => pli.Name).ToList();
        }

        public List<PriceListItem> GetAllPreventiveMedicineItems(int clinicId)
        {
            var categoryIds = GetCategoryIds(clinicId);
            return
                DataContext.PriceListItems.Where(
                    pli =>
                    pli.Available &&
                    (pli.ItemTypeId == (int)PriceListItemTypeEnum.PreventiveTreatments ||
                     pli.ItemTypeId == (int)PriceListItemTypeEnum.Vaccines) &&
                    categoryIds.Contains(pli.CategoryId)).OrderBy(pli => pli.Name).ToList();
        }

        public List<PriceListItem> GetAllItems(int activeClinicId)
        {
            return DataContext.PriceListItems
                .Include(pli => pli.ItemsTariffs)
                .Include(pli => pli.PriceListItemType).Where(i => i.Category.ClinicId == activeClinicId && i.Available && i.Category.Available).ToList();
        }

        public IQueryable<PriceListItem> GetItemsByCategory(int categoryId)
        {
            return DataContext.PriceListItems.Include(p => p.Category).Include(p => p.PriceListItemType).Where(p => p.Available && p.CategoryId == categoryId).OrderBy(p => p.Name);
        }

        public IQueryable<PriceListItem> GetAllClinicItems(int clinicId)
        {
            return
                DataContext.PriceListItems.Include(p => p.Category)
                           .Include(p => p.PriceListItemType)
                           .Where(p => p.Available && p.Category.ClinicId == clinicId)
                           .OrderBy(p => p.Name);
        }


        public PriceListItem GetPriceListItemByCode(int clinicId, string treatmentCode)
        {
            PriceListItem result = null;
            var categoryIds = GetCategoryIds(clinicId);
            var items =
                DataContext.PriceListItems.Where(i => categoryIds.Contains(i.CategoryId) && i.OldCode == treatmentCode);

            if (items.Any())
            {
                result = items.First();
            }

            return result;
        }

        public IQueryable<PriceListItem> GetCategoryActiveItems(int categoryId)
        {
            return DataContext.PriceListItems.Include(i => i.PriceListItemType).Where(i => i.Available && i.CategoryId == categoryId).OrderBy(i => i.Name);
        }


        /// <summary>
        /// copies data from source clinic to target clinic
        /// </summary>
        /// <param name="sourceClinicId">source clinic id</param>
        /// <param name="targetClinicId">target user id</param>
        /// <param name="userId"> preforming user id</param>
        /// <returns>OperationStatus</returns>
        private OperationStatus CopyDataToTargetClinic(int sourceClinicId, int targetClinicId, int userId)
        {
            var source =
                DataContext.Clinics.Include(c => c.Tariffs)
                           .Include(c => c.PriceListCategories)
                           .Include("PriceListCategories.PriceListItems")
                           .Include("PriceListCategories.PriceListItems.ItemsTariffs")
                           .Include("PriceListCategories.PriceListItems.ItemsTariffs.Tariff")
                           .Single(c => c.Id == sourceClinicId);

            var target = DataContext.Clinics.Single(c => c.Id == targetClinicId);
            var dt = DateTime.Now;

            //copy tariffs from source to target
            foreach (var tariff in source.Tariffs.Where(tariff => tariff.Active))
            {
                target.Tariffs.Add(new Tariff()
                    {
                        Active = true,
                        ClinicId = target.Id,
                        Name = tariff.Name,
                        CreatedById = userId,
                        CreatedDate = dt
                    });
            }

            //copy priceListCategories from source to target
            foreach (var category in source.PriceListCategories.Where(c => c.Available))
            {
                target.PriceListCategories.Add(new PriceListCategory()
                    {
                        Available = true,
                        ClinicId = target.Id,
                        Name = category.Name,
                    });
            }

            var status = base.Update(target);

            //copy priceListItems from source to target
            if (status.Success)
            {
                foreach (var category in source.PriceListCategories.Where(c => c.Available))
                {
                    var targetCategory =
                        DataContext.PriceListCategories.Single(
                            c => c.ClinicId == target.Id && c.Name == category.Name && c.Available);
                    targetCategory.PriceListItems = new Collection<PriceListItem>();

                    foreach (var sourceItem in category.PriceListItems.Where(i => i.Available))
                    {
                        var itemTariffs = new List<PriceListItemTariff>();
                        var lastTariff = 0;
                        var targetTariff = new Tariff();
                        foreach (var sourceItemTariff in sourceItem.ItemsTariffs)
                        {
                            if (lastTariff != sourceItemTariff.TariffId)
                            {
                                targetTariff =
                                    target.Tariffs.Single(
                                        t =>
                                        t.ClinicId == target.Id &&
                                        t.Name == sourceItemTariff.Tariff.Name &&
                                        t.Active &&
                                        t.CreatedById == userId &&
                                        t.CreatedDate == dt);
                            }

                            itemTariffs.Add(new PriceListItemTariff()
                                {
                                    Price = sourceItemTariff.Price,
                                    TariffId = targetTariff.Id,
                                    TariffName = targetTariff.Name
                                });

                        }

                        targetCategory.PriceListItems.Add(new PriceListItem()
                        {
                            Available = true,
                            Catalog = sourceItem.Catalog,
                            CategoryId = targetCategory.Id,
                            CopyFixedPrice = sourceItem.CopyFixedPrice,
                            Barcode = sourceItem.Barcode,
                            CreatedById = userId,
                            CreatedDate = dt,
                            FixedPrice = sourceItem.FixedPrice,
                            ItemTypeId = sourceItem.ItemTypeId,
                            Name = sourceItem.Name,
                            ItemsTariffs = itemTariffs
                        });

                    }
                }

                status = base.Update(target);
            }

            return status;
        }


        /// <summary>
        /// updates all existing price list data in  clinic to be not active
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <param name="userId">preforming user id</param>
        /// <returns>OperationStatus</returns>
        private OperationStatus DisableClinicExistingData(int clinicId, int userId)
        {
            var clinic =
                DataContext.Clinics.Include(c => c.Tariffs)
                           .Include(c => c.PriceListCategories)
                           .Include("PriceListCategories.PriceListItems")
                           .Single(c => c.Id == clinicId);
            var dt = DateTime.Now;

            foreach (var category in clinic.PriceListCategories)
            {
                category.Available = false;
                foreach (var item in category.PriceListItems)
                {
                    item.Available = false;
                    item.UpdatedById = userId;
                    item.UpdatedDate = dt;
                }
            }

            foreach (var tariff in clinic.Tariffs)
            {
                tariff.Active = false;
                tariff.UpdatedById = userId;
                tariff.UpdatedDate = dt;
            }

            return base.Update(clinic);
        }




        private List<int> GetCategoryIds(int clinicId)
        {
            return
                DataContext.PriceListCategories.Where(plc => plc.Available && plc.ClinicId == clinicId)
                           .Select(plc => plc.Id)
                           .ToList();
        }

        public void ChangePriceListItemInAllTablesAndRemoveIt(int oldPriceListItemId, int newPriceListItemId)
        {
            //FinanceDocumentItem
            DataContext.FinanceDocumentItems.Where(p => p.PriceListItemId.HasValue && p.PriceListItemId == oldPriceListItemId)
                                            .Update(p => new FinanceDocumentItem { PriceListItemId = newPriceListItemId });

            //PriceListItemTariff
            DataContext.PriceListItemsTariff.Where(p => p.ItemId == oldPriceListItemId).Delete();
            //.Update(p => new PriceListItemTariff { ItemId = newPriceListItemId });

            //Inventory
            DataContext.Inventory.Where(p => p.PriceListItemId == oldPriceListItemId)
                                 .Update(p => new Inventory { PriceListItemId = newPriceListItemId });

            //InventoryPriceListItemOrder
            DataContext.InventoryOrderItems.Where(p => p.PriceListItemId == oldPriceListItemId)
                                           .Update(p => new InventoryPriceListItemOrder { PriceListItemId = newPriceListItemId });

            //PriceListItemSupplier
            DataContext.PriceListItemSuppliers.Where(p => p.PriceListItemId == oldPriceListItemId)
                                              .Update(p => new PriceListItemSupplier { PriceListItemId = newPriceListItemId });

            //PreventiveMedicineFilterPriceListItem
            DataContext.PreventiveMedicineFilterPriceListItems.Where(p => p.PriceListItemId == oldPriceListItemId)
                                                              .Update(p => new PreventiveMedicineFilterPriceListItem { PriceListItemId = newPriceListItemId });

            //QuotationTreatment
            DataContext.QuotationTreatments.Where(p => p.PriceListItemId.HasValue && p.PriceListItemId == oldPriceListItemId)
                                           .Update(p => new QuotationTreatment { PriceListItemId = newPriceListItemId });

            //TreatmentPackage\PackageItem
            DataContext.PackageItems.Where(p => p.PriceListItemId == oldPriceListItemId)
                                    .Update(p => new PackageItem { PriceListItemId = newPriceListItemId });

            //VisitExamination
            DataContext.VisitExaminations.Where(p => p.PriceListItemId.HasValue && p.PriceListItemId == oldPriceListItemId)
                                         .Update(p => new VisitExamination { PriceListItemId = newPriceListItemId });

            //VisitTreatment
            DataContext.VisitTreatments.Where(p => p.PriceListItemId.HasValue && p.PriceListItemId == oldPriceListItemId)
                                       .Update(p => new VisitTreatment { PriceListItemId = newPriceListItemId });

            var oldPriceListItem = DataContext.PriceListItems.SingleOrDefault(p => p.Id == oldPriceListItemId);

            if (oldPriceListItem != null)
            {
                DataContext.PriceListItems.Remove(oldPriceListItem);
            }

            DataContext.SaveChanges();
        }


    }
}
