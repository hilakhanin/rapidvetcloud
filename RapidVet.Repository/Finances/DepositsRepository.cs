﻿using System;
using System.Collections.Generic;
using RapidVet.Enums.Finances;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using System.Linq;
using RapidVet.Model.Finance;
using System.Data.Entity;

namespace RapidVet.Repository
{
    public class DepositsRepository : RepositoryBase<RapidVetDataContext>, IDepositsRepository
    {

        public DepositsRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public void Create(Deposit deposit)
        {
            DataContext.Deposits.Add(deposit);
        }

        public Deposit GetDeposit(int id)
        {
            return DataContext.Deposits.Include("Payments.Issuer").SingleOrDefault(d => d.Id == id);
        }
    }
}