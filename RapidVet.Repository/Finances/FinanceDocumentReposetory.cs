﻿using System;
using System.Collections.Generic;
using RapidVet.Enums;
using RapidVet.Enums.Finances;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using System.Linq;
using RapidVet.Model.Finance;
using System.Data.Entity;
using RapidVet.Repository.Clinics;
using System.Data.Entity.Infrastructure;
//using System.Data.Objects;

namespace RapidVet.Repository
{
    public class FinanceDocumentReposetory : RepositoryBase<RapidVetDataContext>, IFinanceDocumentReposetory
    {

        public FinanceDocumentReposetory(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        //this static list is used for lock to prevent doubel document number
        static List<Issuer> _IssuersLockers = new List<Issuer>();

        private Issuer GetIssuerLocker(int issureId)
        {
            //get static look object
            var issuer = _IssuersLockers.SingleOrDefault(i => i.Id == issureId);
            // add issuer from db to the list
            if (issuer == null)
            {
                lock (_IssuersLockers)
                {
                    issuer = _IssuersLockers.SingleOrDefault(i => i.Id == issureId);
                    // add issuer from db to the list
                    if (issuer == null)
                    {
                        issuer = DataContext.Issuers.Single(i => i.Id == issureId);
                        _IssuersLockers.Add(issuer);
                    }
                }
            }

            return issuer;
        }

        public Model.OperationStatus AddDocuments(Model.Finance.FinanceDocument doc1, Model.Finance.FinanceDocument doc2 = null)
        {


            var issuer = GetIssuerLocker(doc1.IssuerId);
            OperationStatus result;

            lock (issuer)
            {
                SetDocumentSerialNumber(doc1, issuer);
                DataContext.FinanceDocuments.Add(doc1);
                if (doc2 != null)
                {
                    SetDocumentSerialNumber(doc2, issuer);
                    DataContext.FinanceDocuments.Add(doc2);
                }
                result = Save(doc1);
            }

            return result;
        }

        public void MarkDocumentAsDeleted(Model.Finance.FinanceDocument doc)
        {
            var context = ((IObjectContextAdapter)DataContext).ObjectContext;
            try
            {
                context.DeleteObject(doc);
            }
            catch (Exception ex)
            {
            }
        }

        void SetDocumentSerialNumber(FinanceDocument newDoc, Issuer issuer)
        {
            var maxDocId =
    DataContext.FinanceDocuments.Where(
        f => f.FinanceDocumentTypeId == newDoc.FinanceDocumentTypeId && f.IssuerId == newDoc.IssuerId)
               .Max(f => (int?)f.SerialNumber);

            if (!maxDocId.HasValue)
            {
                switch (newDoc.FinanceDocumentType)
                {
                    case FinanceDocumentType.Invoice:
                        newDoc.SerialNumber = issuer.InvoiceInitialNum;
                        break;
                    case FinanceDocumentType.Receipt:
                        newDoc.SerialNumber = issuer.ReceiptInitialNum;
                        break;
                    case FinanceDocumentType.InvoiceReceipt:
                        newDoc.SerialNumber = issuer.InvoiceReceiptInitialNum;
                        break;
                    case FinanceDocumentType.Proforma:
                        newDoc.SerialNumber = issuer.ProformaInitialNum;
                        break;
                    case FinanceDocumentType.Refound:
                        newDoc.SerialNumber = issuer.RefoundInitialNum;
                        break;
                    default:
                        newDoc.SerialNumber = 0;
                        break;
                }
            }
            else
            {
                newDoc.SerialNumber = maxDocId.Value + 1;
            }
        }


        public int GetIssuerLastDepositNumber(int issuerId)
        {
            var lastDepositNumber = DataContext.FinanceDocumentPayments.Where(f => f.IssuerId == issuerId).Max(f => (int?)f.DepositNumber);

            return lastDepositNumber ?? 0;
        }



        public FinanceDocument GetItem(int financeDocumentId)
        {
            var doc = DataContext.FinanceDocuments.Include(d => d.Clinic).Include(d => d.Issuer).Include(d => d.Items).Include(d => d.Client).SingleOrDefault(d => d.Id == financeDocumentId);
            doc.Payments =
                DataContext.FinanceDocumentPayments.Include(p => p.BankCode).Include(p => p.IssuerCreditType).Include(p => p.CreditCardCode).Where(
                    p => p.ReciptId == doc.Id || p.InvoiceId == doc.Id || p.RefoundId == doc.Id).ToList();
            return doc;
        }

        public void RemoveAnyExistingFinanceDocumentDivisions(int docId)
        {
            var divisions = DataContext.FinanceDocumentDivisions.Where(d => d.DocumentId == docId);
            foreach (var division in divisions)
            {
                DataContext.FinanceDocumentDivisions.Remove(division);
            }
        }

        public void AddFinanaceDocumentDivision(FinanceDocumentDividision financeDocumentDividision)
        {
            DataContext.FinanceDocumentDivisions.Add(financeDocumentDividision);
        }

        public List<FinanceDocument> GetDocumentsFromDate(int issuerId, DateTime from, DateTime to)
        {
            return DataContext.FinanceDocuments.Where(d => d.IssuerId == issuerId && (from <= d.Created && d.Created < to)).OrderByDescending(d => d.Created).ToList();
        }

        public List<FinanceDocumentDividision> GetDevisions(int docId)
        {
            return DataContext.FinanceDocumentDivisions.Include("Doctor").Where(d => d.DocumentId == docId).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="doctorId"></param>
        /// <param name="issuerId"></param>
        /// <param name="bySearchDate">0 = by original date, 1 = by original printed date</param>
        /// <returns></returns>
        public List<FinanceDocument> GetDocumentsFromDateAndDoctorAndIssuer(DateTime from, DateTime to, int? doctorId, int? issuerId, int bySearchDate)
        {
            IQueryable<FinanceDocument> docs;
            switch (bySearchDate)
            {
                case 1:
                    docs = DataContext.FinanceDocuments.Include("FinanceDocumentDividisions").Where(d => (d.OrginalPrintedDate.HasValue && (from <= d.OrginalPrintedDate.Value && d.OrginalPrintedDate.Value < to)));
                    break;
                default:
                    docs = DataContext.FinanceDocuments.Include("FinanceDocumentDividisions").Where(d => (from <= d.Created && d.Created < to));
                    break;
            }

            if (issuerId.HasValue && issuerId.Value > 0)
            {
                docs = docs.Where(d => d.IssuerId == issuerId);
            }

            if (doctorId.HasValue && doctorId.Value > 0)
            {
                docs = docs.Where(d => d.FinanceDocumentDividisions.Any(fdd => fdd.DoctorId == doctorId)).OrderByDescending(d => d.Created).ThenBy(d => d.ClientName);
            }

            return docs.ToList();
        }

        public List<FinanceDocumentPayment> GetChecksWithDueDate(DateTime date, int clinicId, int issuerId)
        {
            return
                DataContext.FinanceDocumentPayments
                .Include(d => d.BankCode)
                .Include(d => d.Invoice)
                .Include(d => d.Recipt)
                .Where(d => d.ClinicId == clinicId && d.PaymentTypeId == 0 && d.DueDate.HasValue && (d.DueDate.Value.Year == date.Year && d.DueDate.Value.Month == date.Month && d.DueDate.Value.Day == date.Day) && d.IssuerId == issuerId)
                .ToList();
        }

        public void AddPayment(FinanceDocumentPayment payment)
        {
            payment.Created = DateTime.Now;
            DataContext.FinanceDocumentPayments.Add(payment);
        }


        public List<FinanceDocumentPayment> GetPaymentsForDeposit(int clinicId, DateTime fromDate, DateTime toDate, int? issuerId, int? paymentTypeId, int? documentTypeId, bool? isDeposited)
        {
            var payments = DataContext.FinanceDocumentPayments
                           .Include("Recipt.Client")
                           .Include(d => d.BankCode)
                           .Include(d => d.Deposit)
                           .Include(d => d.CreditCardCode)
                           .Include(d => d.IssuerCreditType)
                           .Where(p => p.ClinicId == clinicId);
            toDate = toDate.AddDays(1);

            if (isDeposited.HasValue)
            {
                if (isDeposited.Value)
                {
                    payments = payments.Where(d => d.DepositId != null);
                    payments = payments.Where(d => fromDate <= d.Deposit.Date && d.Deposit.Date < toDate);
                    payments.OrderBy(d => d.Deposit.Date).ThenBy(d => d.DepositNumber);
                }
                else
                {
                    payments = payments.Where(d => d.DepositId == null);
                    payments = payments.Where(d => fromDate <= d.Created && d.Created < toDate);
                    payments.OrderBy(d => d.Recipt.Created).ThenBy(d => d.Recipt.SerialNumber);
                }

            }
            else
            {
                payments = payments.Where(d => fromDate <= d.Created && d.Created < toDate);
            }

            if (issuerId.HasValue && issuerId.Value > 0)
            {
                payments = payments.Where(d => d.IssuerId == issuerId.Value);
            }

            if (paymentTypeId.HasValue)
            {
                payments = payments.Where(d => d.PaymentTypeId == paymentTypeId.Value);
            }

            if (documentTypeId.HasValue)
            {
                payments = payments.Where(d => d.Recipt.FinanceDocumentTypeId == documentTypeId.Value);
            }



            return payments.ToList();
        }

        public FinanceDocumentPayment GetFinancePayment(int financeDocumentPaymentId)
        {
            return DataContext.FinanceDocumentPayments.Include(p => p.Recipt).SingleOrDefault(p => p.Id == financeDocumentPaymentId);
        }

        public List<FinanceDocument> GetDocumentsForClinic(int clinicId, FinanceDocumentType? documentType)
        {
            var alldocs = DataContext.FinanceDocuments.Where(fd => fd.ClinicId == clinicId);
            if (documentType.HasValue)
            {
                var type = documentType.Value;
                return
                    alldocs.Where(fd => fd.FinanceDocumentTypeId == (int)type).OrderBy(fd => fd.SerialNumber).ToList();
            }
            return alldocs.OrderBy(fd => fd.SerialNumber).ToList();
        }

        public List<FinanceDocument> GetClientDocuments(int clientId)
        {
            var docs = DataContext.FinanceDocuments.Where(d => d.ClientId == clientId).ToList();
            foreach (var financeDocument in docs)
            {
                financeDocument.Payments =
                    DataContext.FinanceDocumentPayments.Where(
                        p =>
                        p.ReciptId == financeDocument.Id || p.InvoiceId == financeDocument.Id ||
                        p.RefoundId == financeDocument.Id).ToList();
            }
            return docs;
        }
        public List<FinanceDocument> GetClientDocumentsPaymentsHistory(int clientId)
        {
            var docs = DataContext.FinanceDocuments.Where(d => d.ClientId == clientId && d.FinanceDocumentTypeId != 3).ToList();
            foreach (var financeDocument in docs)
            {
                financeDocument.Payments =
                    DataContext.FinanceDocumentPayments.Where(
                        p =>
                        p.ReciptId == financeDocument.Id || p.InvoiceId == financeDocument.Id ||
                        p.RefoundId == financeDocument.Id).ToList();
            }
            return docs;
        }

        public List<RapidVet.WebModels.Finance.Reports.InvoiceReportPaymentWebModel> GetPaymentsForDocumentList(List<int> ids, FinanceDocumentType fDT)
        {
            return (from f in DataContext.FinanceDocumentPayments
                    from bc in DataContext.BankCodes.Where(x => x.Id == f.BankCodeId).DefaultIfEmpty()
                    from ccc in DataContext.CreditCardCodes.Where(x => x.Id == f.CreditCardCodeId).DefaultIfEmpty()
                    from ict in DataContext.IssuerCreditTypes.Where(x => x.Id == f.IssuerCreditTypeId).DefaultIfEmpty()
                    where
                     (
                    ((fDT == FinanceDocumentType.Invoice || fDT == FinanceDocumentType.InvoiceReceipt) && f.InvoiceId.HasValue && ids.Contains(f.InvoiceId.Value))
                    ||
                    (fDT == FinanceDocumentType.Receipt && f.ReciptId.HasValue && ids.Contains(f.ReciptId.Value))
                    ||
                    (fDT == FinanceDocumentType.Refound && f.RefoundId.HasValue && ids.Contains(f.RefoundId.Value))
                    ||
                    false
                    )
                    select new RapidVet.WebModels.Finance.Reports.InvoiceReportPaymentWebModel()
                    {     
                        InvoiceId = f.InvoiceId.HasValue ? f.InvoiceId.Value : -1,
                        ReciptId = f.ReciptId.HasValue ? f.ReciptId.Value : -1,
                        RefoundId = f.RefoundId.HasValue ? f.RefoundId.Value : -1,
                        PaymentTypeId = f.PaymentTypeId,
                        ChequeNumber = f.ChequeNumber,
                        BankBranch = f.BankBranch,
                        BankAccount = f.BankAccount,
                        BankCodeName = bc == null ? "" : bc.Name,
                        IssuerCreditTypeName = ict == null ? "" : ict.Name,
                        CreditCardCodeName = ccc == null ? "" : ccc.Name
                    }).ToList();
        }

        public IQueryable<FinanceDocumentPayment> GetPaymentsForDocument(FinanceDocument doc)
        {
            var sFDType = doc.FinanceDocumentType;
            var dID = doc.Id;
            return from f in DataContext.FinanceDocumentPayments
                   from bc in DataContext.BankCodes.Where(x => x.Id == f.BankCodeId).DefaultIfEmpty()
                   from ccc in DataContext.CreditCardCodes.Where(x => x.Id == f.CreditCardCodeId).DefaultIfEmpty()
                   from ict in DataContext.IssuerCreditTypes.Where(x => x.Id == f.IssuerCreditTypeId).DefaultIfEmpty()
                   where
                   ((sFDType == FinanceDocumentType.Invoice || sFDType == FinanceDocumentType.InvoiceReceipt) && f.InvoiceId == dID)
                   ||
                   (sFDType == FinanceDocumentType.Receipt && f.InvoiceId == dID)
                   ||
                   (sFDType == FinanceDocumentType.Refound && f.RefoundId == dID)
                   ||
                   false
                   select f;
        }

        public List<FinanceDocumentPayment> GetPaymentsWithoutInvoice(int clinicId, DateTime from, DateTime to, int issuerId)
        {
            to = to.AddDays(1);
            var payments = DataContext.FinanceDocumentPayments.Include(p => p.Recipt).Where(p => p.InvoiceId == null && p.RefoundId == null
                && p.ClinicId == clinicId && p.DueDate.HasValue && p.DueDate.Value >= from && p.DueDate.Value < to && p.IssuerId == issuerId).ToList();
            return payments.Where(p => p.Recipt != null && p.Recipt.ReciptPriningOnly.HasValue && p.Recipt.ReciptPriningOnly != true).ToList();
        }

        public int MonthelyReciptToInvoice(int clinicId, int userId, decimal vat)
        {
            var converted = 0;
            var monthStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1); //new DateTime(2015, 1, 1);
            var monthEnd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            monthEnd = monthEnd.AddMonths(1);
            var payments =
                DataContext.FinanceDocumentPayments.Include(p => p.Recipt).Where(
                    p => p.ClinicId == clinicId && p.InvoiceId == null && p.RefoundId == null && p.Recipt != null
                        && p.Recipt.ReciptPriningOnly != true && !p.Recipt.ConvertedFromRecipt &&
                        p.DueDate >= monthStart && p.DueDate < monthEnd
                    ).ToList();

            foreach (var financeDocumentPayment in payments)
            {
                converted += PaymentToInvoice(financeDocumentPayment, userId, vat);
            }

            return converted;
        }

        public FinanceDocument GetFinanceDocument(int documentId)
        {
            return DataContext.FinanceDocuments.Include(f => f.Items).Single(d => d.Id == documentId);
        }

        public List<FinanceDocument> GetIssuerDocumentsAndPaymentsForOpenFormat(int issuerId, DateTime startDate, DateTime endDate)
        {
            endDate = endDate.AddDays(1);
            var docs =
                DataContext.FinanceDocuments.Include(f => f.Items).Include(f => f.Client)
                           .Where(d => d.IssuerId == issuerId && d.Created >= startDate && d.Created < endDate).ToList();

            foreach (var financeDocument in docs)
            {
                var payments = GetPaymentsForDocument(financeDocument);
                financeDocument.Payments = payments == null ? new List<FinanceDocumentPayment>() : payments.ToList();
            }

            return docs;

        }

        public List<FinanceDocumentPayment> GetReciptsForHS(DateTime startDate, DateTime endDate, int issuerId)
        {
            endDate = endDate.AddDays(1);
            var docs =
                DataContext.FinanceDocuments
                           .Where(d => d.Created >= startDate && d.Created < endDate && d.IssuerId == issuerId && (d.FinanceDocumentTypeId == (int)FinanceDocumentType.Receipt || d.FinanceDocumentTypeId == (int)FinanceDocumentType.InvoiceReceipt)).ToList();

            List<FinanceDocumentPayment> payments = new List<FinanceDocumentPayment>();

            foreach (var financeDocument in docs)
            {
                payments.AddRange(DataContext.FinanceDocumentPayments.Include(f => f.BankCode).Include(f => f.CreditCardCode).Include("Recipt.Client")
                           .Include(f => f.IssuerCreditType)
                           .Where(f => f.InvoiceId == financeDocument.Id).ToList());
            }
            return payments;
        }

        public int PaymentToInvoice(FinanceDocumentPayment payment, int userId, decimal vat)
        {
            int converted = 0;

            if (payment.InvoiceId == null)
            {
                var recipt = payment.Recipt;
                var invoice = new FinanceDocument()
                    {
                        BankTransferSum = 0,
                        CashPaymentSum = 0,
                        ClientId = recipt.ClientId,
                        ClientAddress = recipt.ClientAddress,
                        ClientName = recipt.ClientName,
                        ClientPhone = recipt.ClientPhone,
                        ClinicId = recipt.ClinicId,
                        Comments = recipt.Comments,
                        ConvertedFromRecipt = true,
                        ConvertedFromReciptSerialNumber = recipt.SerialNumber,
                        Created = DateTime.Now,
                        FinanceDocumentStatus = FinanceDocumentStatus.Closed,
                        FinanceDocumentType = FinanceDocumentType.Invoice,
                        VAT = 1 + vat / 100,
                        UserId = userId,
                        IssuerId = recipt.IssuerId,
                        SumToPayForInvoice = payment.Sum
                    };

                invoice.Items = new List<FinanceDocumentItem>();

                invoice.Items.Add(new FinanceDocumentItem()
                {
                    CatalogNumber = "",
                    Description = Resources.Finance.VetTreatment,
                    Discount = 0,
                    DiscountPercentage = 0,
                    PriceListItemId = null,
                    Quantity = 1,
                    VisitId = null,
                    UnitPrice = payment.Sum / recipt.VAT,
                    TotalBeforeVAT = payment.Sum / recipt.VAT
                });
                //foreach (var item in DataContext.FinanceDocumentItems.Where(i => i.FinanceDocumentId == recipt.Id))
                //{
                //    invoice.Items.Add(new FinanceDocumentItem()
                //        {
                //            CatalogNumber = item.CatalogNumber,
                //            Description = item.Description,
                //            Discount = item.Discount,
                //            DiscountPercentage = item.DiscountPercentage,
                //            PriceListItemId = item.PriceListItemId,
                //            Quantity = item.Quantity,
                //            VisitId = item.VisitId,
                //            UnitPrice = item.UnitPrice,
                //            TotalBeforeVAT = item.TotalBeforeVAT
                //        });
                //}
                if (payment.PaymentType == PaymentType.CreditCard)
                {
                    invoice.TotalCreditPaymentSum = payment.Sum;
                }
                if (payment.PaymentType == PaymentType.Cheque)
                {
                    invoice.TotalChequesPayment = payment.Sum;
                }
                payment.Invoice = invoice;

                AddDocuments(invoice);
                converted = 1;
            }

            return converted;
        }

        public List<FinanceDocumentPayment> GetDepositsForHs(DateTime startDate, DateTime endDate, int issuerId)
        {
            endDate = endDate.AddDays(1);
            return
                DataContext.FinanceDocumentPayments.Include("Recipt.Client").Include(p => p.BankCode)
                           .Where(p => p.IssuerId == issuerId && (p.PaymentTypeId == (int)PaymentType.Cheque || p.PaymentTypeId == (int)PaymentType.Cash) && p.DepositId != null && p.Deposit.Date >= startDate && p.Deposit.Date < endDate).ToList();

        }

        public IQueryable<FinanceDocumentPayment> GetDayPayments(DateTime day, int clinicID, int issuerId)
        {
            var nextDay = day.AddDays(1);
            return
                DataContext.FinanceDocumentPayments.Include(r => r.Recipt).Include(r => r.Invoice).Include(r => r.Refound).Include(r => r.Recipt)
                                                   .Include(r => r.Recipt.Items).Where(p => p.ClinicId == clinicID && p.IssuerId == issuerId &&
                        ((p.PaymentTypeId != (int)PaymentType.Cheque && p.Created >= day && p.Created < nextDay ||
                        (p.PaymentTypeId == (int)PaymentType.Cheque && (p.DueDate >= day && p.DueDate < nextDay || p.Created >= day && p.Created < nextDay)))));


        }

        public IQueryable<FinanceDocumentPayment> GetPaymentsInRange(int clinicID, int issuerId, DateTime from, DateTime to)
        {
            to = to.AddDays(1);
            return
                DataContext.FinanceDocumentPayments.Where(
                    p => p.ClinicId == clinicID && p.IssuerId == issuerId &&
                        ((p.PaymentTypeId == (int)PaymentType.Cash && p.Created >= from && p.Created < to) ||
                        (p.PaymentTypeId != (int)PaymentType.Cash && p.DueDate >= from && p.DueDate < to)));

        }


        public List<FinanceDocument> GetDocumentsForClinicFromDate(int clinicId, DateTime @from, DateTime to, int issuerId)
        {
            return DataContext.FinanceDocuments.Where(d => d.ClinicId == clinicId && from <= d.Created && d.Created <= to
                && (issuerId == 0 || d.IssuerId == issuerId)).OrderByDescending(d => d.Created).ToList();
        }

        public List<FinanceDocument> GetInvoicesForHS(DateTime startDate, DateTime endDate, int issuerId)
        {
            endDate = endDate.AddDays(1);
            var docs =
                DataContext.FinanceDocuments.Include(f => f.Items).Include(f => f.Client)
                           .Where(d => d.Created >= startDate && d.Created < endDate && d.IssuerId == issuerId && (d.FinanceDocumentTypeId == (int)FinanceDocumentType.Invoice || d.FinanceDocumentTypeId == (int)FinanceDocumentType.InvoiceReceipt || d.FinanceDocumentTypeId == (int)FinanceDocumentType.Refound)).ToList();

            foreach (var financeDocument in docs)
            {
                financeDocument.Payments = GetPaymentsForDocument(financeDocument).ToList();
            }
            return docs;
        }

        public IQueryable<FinanceDocumentPayment> GetAllPaymentsOfDeposit(int clinicId, int depositId)
        {
            return DataContext.FinanceDocumentPayments.Include("Recipt.Issuer")
                .Include("BankCode")
                .Include("CreditCardCode")
                .Include("IssuerCreditType")
                .Where(p => p.ClinicId == clinicId && p.DepositId.HasValue && p.DepositId == depositId);
        }

        public List<FinanceDocumentDividision> CreateDivisionsForDocument(int clinicId, int docId, int arrangeIncomeById, bool isCurrentUserDoctor, int currentUserId)
        {
            var doc = DataContext.FinanceDocuments.SingleOrDefault(d => d.Id == docId);
            if (doc == null)
            {
                return null;
            }


            var clinicRepository = new ClinicRepository();
            var doctors = clinicRepository.GetClinicDoctors(clinicId);

            var divideList = doctors.Select(doctor => new FinanceDocumentDividision()
            {
                DoctorName = doctor.Name,
                DocumentId = docId,
                Amount = 0,
                DoctorId = doctor.Id
            }).ToList();

            if (arrangeIncomeById == (int)ArrangeIncomeByEnum.ActiveDoctor && isCurrentUserDoctor)
            {

                var doctor = divideList.SingleOrDefault(item => item.DoctorId == currentUserId);
                if (doctor != null)
                {
                    doctor.Amount = doc.TotalSum;
                }
                else
                {
                    doctor = divideList.SingleOrDefault(item => item.DoctorId == doc.UserId);
                    if (doctor != null)
                    {
                        doctor.Amount = doc.TotalSum;
                    }
                    else
                    {
                        divideList.First().Amount = doc.TotalSum;
                    }
                }
            }
            else
            {
                var doctor = divideList.SingleOrDefault(item => item.DoctorId == doc.UserId);
                if (doctor != null)
                {
                    doctor.Amount = doc.TotalSum;
                }
            }

            return divideList;
        }

        public OperationStatus SaveDivisions(int docId, List<FinanceDocumentDividision> divisionsList)
        {
            RemoveAnyExistingFinanceDocumentDivisions(docId);
            foreach (var financeDocumentDividision in divisionsList)
            {
                AddFinanaceDocumentDivision(financeDocumentDividision);
            }
            return base.Save();
        }

        public decimal GetTaxRateByMonth(int clinicId, DateTime docDate)
        {
            decimal result = 0;

            var taxRates =
                DataContext.TaxRates.Where(
                    a => a.ClinicId == clinicId && a.TaxRateChangedDate <= docDate)
                           .OrderByDescending(a => a.TaxRateChangedDate);
            if (taxRates.Any())
            {
                result = taxRates.First().Rate;
            }

            return result;
        }

        public bool IsDocumentSerialIssuedToIssuer(int issuerId, int serialNumber)
        {
            return DataContext.FinanceDocuments.Any(d => d.IssuerId == issuerId && d.SerialNumber == serialNumber);
        }

        public void SetInvoicedItems(List<WebModels.Finance.Invoice.jsonItem> list)
        {
            var eIDs = list.FindAll(x => x.itemExaminationID > 0).Select(x => x.itemExaminationID).ToList();
            var mIDs = list.FindAll(x => x.itemMedicationID > 0).Select(x => x.itemMedicationID).ToList();
            var pmIDs = list.FindAll(x => x.itemPreventiveMedicineID > 0).Select(x => x.itemPreventiveMedicineID).ToList();
            var tIDs = list.FindAll(x => x.itemTreatmentID > 0).Select(x => x.itemTreatmentID).ToList();
            if (eIDs != null && eIDs.Count > 0)
                setInvoiced(DataContext.VisitExaminations.Where(x => !x.Invoiced && eIDs.Contains(x.Id)));

            if (mIDs != null && mIDs.Count > 0)
                setInvoiced(DataContext.VisitMedications.Where(x => !x.Invoiced && mIDs.Contains(x.Id)));

            if (tIDs != null && tIDs.Count > 0)
                setInvoiced(DataContext.VisitTreatments.Where(x => !x.Invoiced && tIDs.Contains(x.Id)));

            if (pmIDs != null && pmIDs.Count > 0)
                setInvoiced(DataContext.PreventiveMedicineItems.Where(x => !x.Invoiced && pmIDs.Contains(x.Id)));

            DataContext.SaveChanges();
        }

        private void setInvoiced(dynamic dynamicQueryable)
        {
            foreach (var i in dynamicQueryable)
                i.Invoiced = true;
        }

        public FinanceDocumentPayment GetLastChequePayment(int clientId)
        {
            var document = DataContext.FinanceDocuments.Where(f => f.ClientId == clientId && f.TotalChequesPayment > 0).OrderByDescending(f => f.Created).FirstOrDefault();
            if (document != null)
            {
                document.Payments =
                        DataContext.FinanceDocumentPayments.Where(
                            p =>
                            p.ReciptId == document.Id || p.InvoiceId == document.Id ||
                            p.RefoundId == document.Id).ToList();
            }
            return document != null ? document.Payments.Where(p => p.PaymentTypeId == (int)PaymentType.Cheque).OrderByDescending(p => p.DueDate).FirstOrDefault() : null;
        }
    }
}