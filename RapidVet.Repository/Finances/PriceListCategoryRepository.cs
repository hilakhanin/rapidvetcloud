﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Finance;
using System.Data.Entity;
using RapidVet.Enums;
using System.Web.Mvc;
using EntityFramework.Extensions;
using RapidVet.Model.Clinics;

namespace RapidVet.Repository.Finances
{
    public class PriceListCategoryRepository : RepositoryBase<RapidVetDataContext>, IPriceListCategoryRepository
    {

        public PriceListCategoryRepository()
        {

        }

        public PriceListCategoryRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// get all price list categories in clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>IQueryable(PriceListCategory)</returns>
        public IQueryable<PriceListCategory> GetList(int clinicId)
        {
            return DataContext.PriceListCategories
                .Include(c => c.PriceListItems.Any(i => i.Available))
                .Where(c => c.ClinicId == clinicId && c.Available).OrderBy(c => c.Name);
        }

        /// <summary>
        /// create a price list category in clinic
        /// </summary>
        /// <param name="category">priceListCategory object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Create(PriceListCategory category)
        {
            DataContext.PriceListCategories.Add(category);
            var status = base.Save(category);
            if (status.Success)
            {
                status.ItemId = category.Id;
            }

            return status;
        }


        /// <summary>
        /// gets a specific priceListCategory object
        /// </summary>
        /// <param name="id">id in priceListcategories table</param>
        /// <returns>PriceListCategory</returns>
        public PriceListCategory GetItem(int id)
        {
            return DataContext.PriceListCategories.Include(c => c.PriceListItems)
                .Include(c => c.Clinic).Single(c => c.Id == id);
        }

        /// <summary>
        /// save (update) a PriceListCategory in db
        /// </summary>
        /// <param name="category"> PriceListCategory object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(PriceListCategory category)
        {
            return base.Save(category);
        }

        /// <summary>
        /// make a PriceListCategory ojbect inactive in db
        /// </summary>
        /// <param name="id">id in PriceListCategories table</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus RemoveCategoryFromClinic(int id)
        {
            var target = DataContext.PriceListCategories.Include(c => c.PriceListItems).Include("PriceListItems.VaccineOrTreatment").Single(c => c.Id == id);
            target.Available = false;
            foreach (var item in target.PriceListItems)
            {
                if(item.VaccineOrTreatment.Any())
                {
                    DataContext.VaccineAndTreatments.Where(p => p.PriceListItemId == item.Id)
                                      .Update(p => new VaccineOrTreatment { Active = false });
                    DataContext.SaveChanges();
                }
                item.Available = false;
            }
            return base.Update(target);
        }

        /// <summary>
        /// returns a list of all priceListCategories id's in clinic
        /// </summary>
        /// <param name="id">clinic id</param>
        /// <returns></returns>
        public List<int> GeIdList(int id)
        {
            return DataContext.PriceListCategories.Where(p => p.ClinicId == id && p.Available).Select(p => p.Id).ToList();
        }

        /// <summary>
        /// return a list of priceListItem in category
        /// </summary>
        /// <param name="id">PriceListCategory Id</param>
        /// <returns>PriceListCategory</returns>
        public PriceListCategory GetSimpleItem(int id)
        {
            return DataContext.PriceListCategories.Single(c => c.Id == id);
        }


        /// <summary>
        /// get all active PriceListCategories in clinic
        /// </summary>
        /// <param name="clinicId">clinic Id</param>
        /// <returns>IQueryable(PriceListCategory)</returns>
        public IQueryable<PriceListCategory> GetClinicActiveCategories(int clinicId, bool forVisit = false)
        {
            if (!forVisit)
            {
                return DataContext.PriceListCategories.Where(c => c.ClinicId == clinicId && c.Available)
                                  .OrderBy(c => c.Name);
            }
            else
            {
                var ignoredCategories = new List<int>() { (int)PriceListItemTypeEnum.Vaccines, (int)PriceListItemTypeEnum.PreventiveTreatments };
                var categories = DataContext.PriceListCategories.Include(i => i.PriceListItems).Where(c => c.ClinicId == clinicId && c.Available && c.PriceListItems.Any(y => !ignoredCategories.Contains(y.PriceListItemType.Id)));

                return categories.OrderBy(c=>c.Name);
                               
            }
        }

        public int GetPriceListItemCategoryId(int priceListItemId)
        {
            return DataContext.PriceListItems.Single(pli => pli.Id == priceListItemId).CategoryId;
        }

        public string GetName(int categoryId)
        {
            return DataContext.PriceListCategories.Single(c => c.Id == categoryId).Name;
        }

        public string GetNameByPriceListItemId(int? priceListItemId)
        {
            var result = string.Empty;
            if (priceListItemId.HasValue && priceListItemId.Value > 0)
            {
                var item =
                    DataContext.PriceListItems.Include(i => i.Category)
                               .SingleOrDefault(i => i.Id == priceListItemId.Value);
                if (item != null && item.Category != null)
                {
                    result = item.Category.Name;
                }
            }
            return result;
        }

        public List<SelectListItem> GetDropdownList(int clinicId)
        {
            var list = new List<SelectListItem>();
            IQueryable<PriceListCategory> categories;

            categories = DataContext.PriceListCategories.Where(i => i.ClinicId == clinicId && i.Available).OrderBy(c=>c.Name);
            
            foreach (var category in categories)
            {
                list.Add(new SelectListItem()
                {
                    Value = category.Id.ToString(),
                    Text = category.Name
                });
            }
            return list;
        }

    }
}
