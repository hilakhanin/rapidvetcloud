﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.WebModels.Finance;

namespace RapidVet.Repository.Finances
{
    public interface IPriceListRepository : IDisposable
    {
        //currently not in use
        Clinic GetClinic(int clinicId);

        //currently not in use
        OperationStatus CreatePriceListItem(PriceListItem item);

        //currently not in use
        PriceListItem GetPriceListItem(int itemId);

        //currently not in use
        OperationStatus UpdatePriceListItem(PriceListItem source);

        //currently not in use
        OperationStatus RemoveItemFromCategory(int itemId);

        /// <summary>
        /// moves all priceListItems prices in tariff by a fixed amount
        /// </summary>
        /// <param name="tariffId"> id of the tariff </param>
        /// <param name="method">change price by numbers or by precentage</param>
        /// <param name="action">increase or decrease</param>
        /// <param name="amount">actual number</param>
        /// <returns>OperationStatus</returns>
        OperationStatus ShiftPrices(int tariffId,
                                    PriceShiftMethod method,
                                    PriceShiftAction action,
                                    decimal amount);

        /// <summary>
        /// duplicates prices between different tariffs in the same clinic
        /// </summary>
        /// <param name="sourceTariff">source tariff id</param>
        /// <param name="destinationTariff">target tariff id</param>
        /// <returns>OperationStatus</returns>
        OperationStatus DuplicatePricesBetweenTariffs(int sourceTariff, int destinationTariff);

        /// <summary>
        /// duplicate the all priceListCategories, PriceListItems , Tariffs and PriceListItemTariffs 
        /// between clinics in the same clinic group
        /// </summary>
        /// <param name="sourceClinicId">clinic id from which the data is taken</param>
        /// <param name="targetClinicIds">clinic id's (can be more then one) in which this data will be copied to. 
        /// all existing data in these clinics will be made not active/</param>
        /// <param name="userId">id of the preforming user</param>
        /// <returns>OperationStatus</returns>
        OperationStatus DuplicatePricesBetweenClinics(int sourceClinicId,
                                                      List<int> targetClinicIds,
                                                      int userId);

        /// <summary>
        /// get type id for price list item
        /// </summary>
        /// <param name="priceListItemId">price list item id</param>
        /// <returns>int type id</returns>
        int GetItemTypeId(int priceListItemId);

        /// <summary>
        /// get all price list items that are treatments
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <returns> List<PriceListItem></returns>
        List<PriceListItem> GetTreatments(int clinicId);

        /// <summary>
        /// get all price list items that are examinations
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <returns> List<PriceListItem></returns>
        List<PriceListItem> GetExaminations(int clinicId);

        /// <summary>
        /// get all price list items that are preventive medicine items
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <returns> List<PriceListItem></returns>
        List<PriceListItem> GetAllPreventiveMedicineItems(int clinicId);

        /// <summary>
        /// get all price list items in DB
        /// </summary>
        /// <param name="activeClinicId"></param>
        /// <returns> List<PriceListItem></returns>
        List<PriceListItem> GetAllItems(int activeClinicId);

        /// <summary>
        /// get all active price list items in category id
        /// </summary>
        /// <param name="categoryId">int price list category id</param>
        /// <returns></returns>
        IQueryable<PriceListItem> GetItemsByCategory(int categoryId);

        /// <summary>
        /// gt all price list items for clinic
        /// </summary>
        /// <param name="clinicId"></param>
        /// <returns>IQueryable<PriceListItem></returns>
        IQueryable<PriceListItem> GetAllClinicItems(int clinicId);

        /// <summary>
        /// get price list item
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="treatmentCode">string treatment code</param>
        /// <returns>PriceListItem</returns>
        PriceListItem GetPriceListItemByCode(int clinicId, string treatmentCode);

        /// <summary>
        /// get all price list items in category
        /// </summary>
        /// <param name="categoryId">int price list category id</param>
        /// <returns>IQueryable<PriceListItem></returns>
        IQueryable<PriceListItem> GetCategoryActiveItems(int categoryId);

        void ChangePriceListItemInAllTablesAndRemoveIt(int oldPriceListItemId, int newPriceListItemId);
    }
}
