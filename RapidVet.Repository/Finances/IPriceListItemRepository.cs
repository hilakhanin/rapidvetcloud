﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Finance;

namespace RapidVet.Repository.Finances
{
    public interface IPriceListItemRepository : IDisposable
    {
        /// <summary>
        /// create a priceListItem in db
        /// </summary>
        /// <param name="item">PriceListItem object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(PriceListItem item);


        /// <summary>
        /// update a PriceListObject in db
        /// </summary>
        /// <param name="item"></param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(PriceListItem item);


        /// <summary>
        /// make a PriceListItem object to be not active in db
        /// </summary>
        /// <param name="id">PriceListItem id</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(int id);

        /// <summary>
        /// returns all active PriceListItems in PriceListCategory
        /// </summary>
        /// <param name="categoryId">PriceListCategoryId</param>
        /// <returns>List(PriceListItem)</returns>
        List<PriceListItem> GetList(int categoryId, bool displayInactiveItems);

        /// <summary>
        /// updates an item price in db --> PriceListItemTariff
        /// </summary>
        /// <param name="source">PriceListItemTariff </param>
        /// <returns>OperationStatus</returns>
        OperationStatus UpdatePrice(PriceListItemTariff source);

        /// <summary>
        /// returns a specific PriceListItem
        /// </summary>
        /// <param name="id">PrideListItem id</param>
        /// <returns>PriceListItem</returns>
        PriceListItem GetItem(int id);

        /// <summary>
        /// returns all PriceListItemTypes
        /// </summary>
        /// <returns>  List(PriceListItemType)</returns>
        List<PriceListItemType> ItemTypes();

        /// <summary>
        /// return all PriceListItems that are of type 'food' in clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>   List(PriceListItem)</returns>
        List<PriceListItem> GetClinicFoodTypes(int clinicId);


        /// <summary>
        /// gets list of all price list items in clinic group with price for the client tariff id or the clinic default id
        /// </summary>
        /// <param name="clientId">client id</param>
        /// <returns>IQueryable<PriceListItem></returns>
        IQueryable<PriceListItem> GetPriceListItemsForClient(int clientId, out Dictionary<int, decimal> Dic);


        /// <summary>
        /// return a list of all items with vaccine or treatment type
        /// </summary>
        ///  <param name="clinicId">clinic id</param>
        /// <returns>list of price list items</returns>
        List<PriceListItem> GetAllItemsOfTypeVaccineOrTreatment(int clinicId);

    }
}
