﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.Finances;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;

namespace RapidVet.Repository
{
    public interface IDepositsRepository
    {
        void Create(Deposit deposit);

        Deposit GetDeposit(int id);
    }
}
