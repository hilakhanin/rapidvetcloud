﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Finance;

namespace RapidVet.Repository.Finances
{
    public interface ITariffRepository : IDisposable
    {
        /// <summary>
        /// get all active tariffs in clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>List(Tariff)</returns>
        List<Tariff> GetActiveTariffList(int clinicId);

        /// <summary>
        /// create tariff in db
        /// </summary>
        /// <param name="tariff">tariff object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Tariff tariff);


        /// <summary>
        /// get a specific tariff
        /// </summary>
        /// <param name="id">id in tariff table</param>
        /// <returns>Tariff</returns>
        Tariff GetTariff(int id);

        /// <summary>
        /// update a tariff in db
        /// </summary>
        /// <param name="tariff">tariff object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(Tariff tariff);

        /// <summary>
        /// makes a tariff not active in db
        /// </summary>
        /// <param name="id">id in tariff table</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(int id);


        /// <summary>
        /// get tariff id for client by patient id
        /// </summary>
        /// <param name="patientId">patient id</param>
        /// <returns>int tariff id</returns>
        int GetTariffIdByPatientId(int patientId);
    }
}
