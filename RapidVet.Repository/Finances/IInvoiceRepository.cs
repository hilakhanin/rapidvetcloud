﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.Finances;
using RapidVet.Model.Finance;
using RapidVet.Model.Visits;

namespace RapidVet.Repository.Finances
{
    public interface IInvoiceRepository : IDisposable
    {
        List<BankCode> GetBankCodes();
        List<CreditCardCode> GetCreditCardCodesForClinic(int clinicId, int issuerId);
        List<Visit> GetUnpaidVisits(int value);

    }
}
