﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Enums.Finances;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;

namespace RapidVet.Repository
{
    public interface IFinanceDocumentReposetory
    {

        /// <summary>
        /// add new finncae doc item include serial number
        /// </summary>
        Model.OperationStatus AddDocuments(Model.Finance.FinanceDocument doc1, Model.Finance.FinanceDocument doc2 = null);
        void MarkDocumentAsDeleted(Model.Finance.FinanceDocument doc);
        FinanceDocument GetItem(int financeDocumentId);
        
        void RemoveAnyExistingFinanceDocumentDivisions(int docId);
        void AddFinanaceDocumentDivision(FinanceDocumentDividision financeDocumentDividision);
        List<FinanceDocument> GetDocumentsFromDate(int issuerId, DateTime from, DateTime to);
        List<FinanceDocumentDividision> GetDevisions(int docId);
        List<FinanceDocument> GetDocumentsFromDateAndDoctorAndIssuer(DateTime from, DateTime to, int? doctorId, int? issuerId, int bySearchDate);
        List<FinanceDocumentPayment> GetChecksWithDueDate(DateTime date, int clinicId, int issuerId);

        void AddPayment(FinanceDocumentPayment payment);
        List<FinanceDocumentPayment> GetPaymentsForDeposit(int clinicId, DateTime fromDate, DateTime toDate, int? issuerId, int? paymentTypeId, int? documentTypeId, bool? isDeposited);
        FinanceDocumentPayment GetFinancePayment(int financeDocumentPaymentId);
        List<FinanceDocument> GetDocumentsForClinic(int clinicId, FinanceDocumentType? documentType);

        List<FinanceDocument> GetClientDocuments(int clientId);

        List<FinanceDocument> GetClientDocumentsPaymentsHistory(int clientId);

        /// <summary>
        /// get all payments for finance document by id
        /// </summary>
        /// <param name="doc"></param>
        /// <returns> IQueryable<FinanceDocument></returns>
        IQueryable<FinanceDocumentPayment> GetPaymentsForDocument(FinanceDocument doc);
        List<RapidVet.WebModels.Finance.Reports.InvoiceReportPaymentWebModel> GetPaymentsForDocumentList(List<int> ids, FinanceDocumentType fDT);

        List<FinanceDocumentPayment> GetPaymentsWithoutInvoice(int clinicId, DateTime from, DateTime to, int issuerId);

        int MonthelyReciptToInvoice(int clinicId, int userId, decimal vat);

        FinanceDocument GetFinanceDocument(int documentId);

        List<FinanceDocument> GetIssuerDocumentsAndPaymentsForOpenFormat(int issuerId, DateTime startDate, DateTime endDate);

        List<FinanceDocument> GetDocumentsForClinicFromDate(int clinicId, DateTime @from, DateTime to, int issuerId=0);

        List<FinanceDocument> GetInvoicesForHS(DateTime startDate, DateTime endDate, int issuerId);

        List<FinanceDocumentPayment> GetReciptsForHS(DateTime startDate, DateTime endDate, int issuerId);

        IQueryable<FinanceDocumentPayment> GetAllPaymentsOfDeposit(int clinicId, int depositId);

        List<FinanceDocumentDividision> CreateDivisionsForDocument(int clinicId, int docId, int arrangeIncomeById, bool isCurrentUserDoctor, int currentUserId);

        OperationStatus SaveDivisions(int docId, List<FinanceDocumentDividision> divisionsList);

        int PaymentToInvoice(FinanceDocumentPayment payment, int userId ,  decimal vat);



        List<FinanceDocumentPayment> GetDepositsForHs(DateTime startDate, DateTime endDate, int issuerId);

        IQueryable<FinanceDocumentPayment> GetDayPayments(DateTime day, int clinicID, int issuerId);

        int GetIssuerLastDepositNumber(int issuerId);

        decimal GetTaxRateByMonth(int clinicId, DateTime docDate);

        bool IsDocumentSerialIssuedToIssuer(int issuerId, int serialNumber);
        IQueryable<FinanceDocumentPayment> GetPaymentsInRange(int clinicID, int issuerId, DateTime from, DateTime to);

        void SetInvoicedItems(List<WebModels.Finance.Invoice.jsonItem> list);
        FinanceDocumentPayment GetLastChequePayment(int clientId);
    }
}
