﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using RapidVet.Enums.Finances;
using RapidVet.Model.Finance;
using RapidVet.Model.Visits;

namespace RapidVet.Repository.Finances
{
    public class InvoiceRepository : RepositoryBase<RapidVetDataContext>, IInvoiceRepository
    {
        public InvoiceRepository()
        {

        }

        public InvoiceRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public List<BankCode> GetBankCodes()
        {
            return DataContext.BankCodes.ToList();
        }

        public List<CreditCardCode> GetCreditCardCodesForClinic(int clinicId, int issuerId)
        {
            var clinic = DataContext.Clinics.Include(c => c.CreditCardCodes).SingleOrDefault(c => c.Id == clinicId);
            if (clinic != null)
            {
                return clinic.CreditCardCodes.Where(c => c.IssuerId == issuerId && c.Active).ToList();
            }
            return null;
        }



        public List<Visit> GetUnpaidVisits(int clientId)
        {           
            return
                DataContext.Visits.Include(v => v.Medications)
                           .Include(v => v.PreventiveMedicineItems)
                           .Include("PreventiveMedicineItems.PriceListItem")
                           .Include("Medications.Medication")
                           .Include(v => v.Treatments)
                           .Include("Treatments.PriceListItem")
                           .Include(v => v.Examinations)
                           .Include("Examinations.PriceListItem")
                          // .Include(v => v.Patient)
                           .Where(v => v.ClientIdAtTimeOfVisit == clientId && v.Active).ToList();//&& !v.ReciptId.HasValue
        }
    }
}
