﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Finance;

namespace RapidVet.Repository.Finances
{
    public class TariffRepository : RepositoryBase<RapidVetDataContext>, ITariffRepository
    {
        public TariffRepository()
        {

        }

        public TariffRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// get all active tariffs in clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>List(Tariff)</returns>
        public List<Tariff> GetActiveTariffList(int clinicId)
        {
            return DataContext.Tariffs.Include(t => t.TariffsItems)
                              .Include("TariffsItems.Item")
                              .Where(t => t.ClinicId == clinicId && t.Active)
                              .OrderBy(t => t.Name).ToList();
        }

        /// <summary>
        /// create tariff in db
        /// </summary>
        /// <param name="tariff">tariff object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Create(Tariff tariff)
        {
            DataContext.Tariffs.Add(tariff);
            return base.Save(tariff);
        }

        /// <summary>
        /// get a specific tariff
        /// </summary>
        /// <param name="id">id in tariff table</param>
        /// <returns>Tariff</returns>
        public Tariff GetTariff(int id)
        {
            return DataContext.Tariffs.Single(t => t.Id == id);
        }

        /// <summary>
        /// update a tariff in db
        /// </summary>
        /// <param name="tariff">tariff object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(Tariff tariff)
        {
            return base.Save(tariff);
        }

        /// <summary>
        /// makes a tariff not active in db
        /// </summary>
        /// <param name="id">id in tariff table</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Delete(int id)
        {
            var tariff = DataContext.Tariffs.Single(t => t.Id == id);
            tariff.Active = false;
            return base.Update(tariff);
        }

        public int GetTariffIdByPatientId(int patientId)
        {
            var patient = DataContext.Patients.Include(p => p.Client).Include("Client.Clinic").Single(p => p.Id == patientId);
            return patient.Client.TariffId == null
                       ? patient.Client.Clinic.DefualtTariffId
                       : patient.Client.TariffId.Value;
        }
    }
}
