﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.Expenses;

namespace RapidVet.Repository.Expenses
{
    public interface IExpenseRepository : IDisposable
    {
        /// <summary>
        /// get expences for clinic in date range
        /// </summary>
        /// <param name="clinicId"> in clinic id</param>
        /// <param name="fromDate"> datetime from date</param>
        /// <param name="toDate"> datetime to date</param>
        /// <returns>IQueryable<Expense></returns>
        IQueryable<Expense> GetClinicExpenses(int clinicId, DateTime fromDate, DateTime toDate, int? issuerId, string dateType);

        /// <summary>
        /// get specific expense id
        /// </summary>
        /// <param name="expenseId">int expense id</param>
        /// <returns>Expense</returns>
        Expense GetExpense(int expenseId);

        /// <summary>
        /// checks if selected expense group and selected supplier belong to the current clinic
        /// </summary>
        /// <param name="expenseGroupId">int expense group id</param>
        /// <param name="supplierId">in supplier id</param>
        /// <param name="clinicId"> in clinic id</param>
        /// <returns>bool</returns>
        bool ValidateGroupAndSupplier(int expenseGroupId, int supplierId, int clinicId);
        
        /// <summary>
        /// creates expense obj
        /// </summary>
        /// <param name="expense"> expence obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Expense expense);

        /// <summary>
        /// deletes expense obj
        /// </summary>
        /// <param name="expense">expense obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(Expense expense);

        /// <summary>
        /// get al expenses for supplier in date range
        /// </summary>
        /// <param name="supplierId">int supplier id</param>
        /// <param name="fromDate">datetime from</param>
        /// <param name="toDate">datetime to</param>
        /// <param name="issuerId">int issuer id</param>
        /// <returns>IQueryable<Expense></returns>
        IQueryable<Expense> GetReportBySupplier(int supplierId, DateTime fromDate, DateTime toDate,int issuerId);

        /// <summary>
        /// get all expenses for clinic  in date range
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="fromDate"> datetime from</param>
        /// <param name="toDate">datetime to</param>
        /// <param name="issuerId">int issuer id</param>
        /// <returns>IQueryable<Expense></returns>
        IQueryable<Expense> GetExpenses(int clinicId, DateTime fromDate, DateTime toDate, int issuerId=0);
    }
}
