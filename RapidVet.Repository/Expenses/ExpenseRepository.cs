﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.Expenses;

namespace RapidVet.Repository.Expenses
{
    public class ExpenseRepository : RepositoryBase<RapidVetDataContext>, IExpenseRepository
    {
        public ExpenseRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public ExpenseRepository()
        {

        }

        public IQueryable<Expense> GetClinicExpenses(int clinicId, DateTime fromDate, DateTime toDate, int? issuerId, string dateType)
        {
            if (issuerId.HasValue)
            {
                if (dateType == Resources.Expences.EnteredDate)
                {
                    return
                        DataContext.Expenses
                                   .Include(e => e.ExpenseGroup)
                                   .Include(e => e.Supplier)
                                   .Where(
                                       e =>
                                       e.ExpenseGroup.ClinicId == clinicId &&
                                       e.Supplier.ClinicId == clinicId &&
                                       e.EnteredDate >= fromDate &&
                                       e.EnteredDate <= toDate && e.IssuerId == issuerId)
                                   .OrderByDescending(e => e.EnteredDate);
                }
                else
                {
                    return
                        DataContext.Expenses
                                   .Include(e => e.ExpenseGroup)
                                   .Include(e => e.Supplier)
                                   .Where(
                                       e =>
                                       e.ExpenseGroup.ClinicId == clinicId &&
                                       e.Supplier.ClinicId == clinicId &&
                                       e.InvoiceDate >= fromDate &&
                                       e.InvoiceDate <= toDate && e.IssuerId == issuerId)
                                   .OrderByDescending(e => e.InvoiceDate);
                }
            }
            else
            {
                if (dateType == Resources.Expences.EnteredDate)
                {
                    return
                    DataContext.Expenses
                               .Include(e => e.ExpenseGroup)
                               .Include(e => e.Supplier)
                               .Where(
                                   e =>
                                   e.ExpenseGroup.ClinicId == clinicId &&
                                   e.Supplier.ClinicId == clinicId &&
                                   e.EnteredDate >= fromDate &&
                                   e.EnteredDate <= toDate)
                               .OrderByDescending(e => e.EnteredDate);
                }
                else
                {
                    return
                    DataContext.Expenses
                               .Include(e => e.ExpenseGroup)
                               .Include(e => e.Supplier)
                               .Where(
                                   e =>
                                   e.ExpenseGroup.ClinicId == clinicId &&
                                   e.Supplier.ClinicId == clinicId &&
                                   e.InvoiceDate >= fromDate &&
                                   e.InvoiceDate <= toDate)
                               .OrderByDescending(e => e.InvoiceDate);
                }
            }
        }

        public Expense GetExpense(int expenseId)
        {
            return
                DataContext.Expenses.Include(e => e.Supplier)
                           .Include(e => e.ExpenseGroup)
                           .Single(e => e.Id == expenseId);
        }

        public bool ValidateGroupAndSupplier(int expenseGroupId, int supplierId, int clinicId)
        {
            var expenseGroupClinicId = DataContext.ExpenseGroups.Single(e => e.Id == expenseGroupId).ClinicId;
            var supplierClinicId = DataContext.Suppliers.Single(s => s.Id == supplierId).ClinicId;
            return clinicId == expenseGroupClinicId && clinicId == supplierClinicId;
        }

        public OperationStatus Create(Expense expense)
        {
            DataContext.Expenses.Add(expense);
            return base.Save(expense);
        }

        public OperationStatus Delete(Expense expense)
        {
            DataContext.Expenses.Remove(expense);
            return base.Save(expense);

        }

        public IQueryable<Expense> GetReportBySupplier(int supplierId, DateTime fromDate, DateTime toDate, int issuerId)
        {
            return
                DataContext.Expenses.Include(e => e.Supplier)
                           .Include(e => e.Issuer)
                           .Include(e => e.ExpenseGroup)
                           .Where(
                               e =>
                               e.SupplierId == supplierId && e.InvoiceDate >= fromDate && e.InvoiceDate < toDate &&
                               e.IssuerId == issuerId)
                           .OrderByDescending(e => e.InvoiceDate);
        }

        public IQueryable<Expense> GetExpenses(int clinicId, DateTime fromDate, DateTime toDate, int issuerId=0)
        {
            return
                DataContext.Expenses.Include(e => e.Supplier)
                            .Include(e => e.Issuer)
                           .Include(e => e.ExpenseGroup)
                           .Where(
                               e =>
                               e.InvoiceDate >= fromDate && e.InvoiceDate < toDate && e.Supplier.ClinicId == clinicId &&
                               e.ExpenseGroup.ClinicId == clinicId && 
                               (issuerId==0 || e.IssuerId == issuerId))
                           .OrderBy(e => e.ExpenseGroupId);
        }

    }
}
