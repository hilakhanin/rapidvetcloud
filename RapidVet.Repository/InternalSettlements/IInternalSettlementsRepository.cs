﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model.InternalSettlements;

namespace RapidVet.Repository.InternalSettlements
{
    public interface IInternalSettlementsRepository : IDisposable
    {
        void AddSettlement(InternalSettlement settlement);
        List<InternalSettlement> GetSettlements(int clientId);
    }
}
