﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Model.InternalSettlements;

namespace RapidVet.Repository.InternalSettlements
{
    public class InternalSettlementsRepository : RepositoryBase<RapidVetDataContext>, IInternalSettlementsRepository
    {

         public InternalSettlementsRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

         public InternalSettlementsRepository()
        {

        }

        public void AddSettlement(InternalSettlement settlement)
        {
            DataContext.InternalSettlements.Add(settlement);
        }

        public List<InternalSettlement> GetSettlements(int clientId)
        {
            var client =  DataContext.Clients.Include(c => c.InternalSettlements).SingleOrDefault(c => c.Id == clientId);
            return client == null ? new List<InternalSettlement>() : client.InternalSettlements.ToList();
        }
    }
}
