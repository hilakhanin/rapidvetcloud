﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.WebModels.PreventiveMedicine;

namespace RapidVet.Repository.PreventiveMedicine
{
    public interface IPreventiveMedicineRemindersRepository : IDisposable
    {
        /// <summary>
        /// return preventive medicine item report by filters
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="fromDate">date time from date</param>
        /// <param name="toDate">date time to date</param>
        /// <param name="regionalCouncilId"> int? regional council id</param>
        /// <param name="animalKindId">int? animal kind id</param>
        /// <param name="includeInactiveClients">bool include inactive clients</param>
        /// <param name="includeInactivePatients">bool include inactve patients</param>
        /// <param name="futureRemindersOnly">bool return future reminders only</param>
        /// <param name="preventiveItemIds"></param>
        /// <returns></returns>
        IQueryable<PreventiveMedicineItem> GetData(int clinicId, DateTime fromDate, DateTime toDate, int? regionalCouncilId, int? animalKindId, 
            bool includeInactiveClients, bool includeInactivePatients, bool futureRemindersOnly, List<int> preventiveItemIds, DateTime fromReminderDate, DateTime toReminderDate,
            List<int> filteredReminders, bool RemoveClientsWithFutureCalendarEntries, List<int> CityIds, int? doctorId);

        List<PreventiveReminderIndexModel> GetDataList(int clinicId, DateTime fromDate, DateTime toDate, int? regionalCouncilId, int? animalKindId,
            bool includeInactiveClients, bool includeInactivePatients, bool futureRemindersOnly, List<int> preventiveItemIds, DateTime fromReminderDate, DateTime toReminderDate,
            List<int> filteredReminders, bool RemoveClientsWithFutureCalendarEntries, List<int> CityIds, int? doctorId, bool updateReminders);

        List<PreventiveReminderIndexModel> GetDataListPrint(int clinicId, DateTime fromDate, DateTime toDate, int? regionalCouncilId, int? animalKindId,
            bool includeInactiveClients, bool includeInactivePatients, bool futureRemindersOnly, List<int> preventiveItemIds, DateTime fromReminderDate, DateTime toReminderDate,
            List<int> filteredReminders, bool RemoveClientsWithFutureCalendarEntries, List<int> CityIds, int? doctorId, bool updateReminders);
    }
}
