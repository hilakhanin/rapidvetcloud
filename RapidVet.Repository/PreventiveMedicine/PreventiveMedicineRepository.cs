﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.RapidConsts;
using System.Web.Mvc;
using EntityFramework.Extensions;
using RapidVet.Helpers;
using RapidVet.WebModels.GeneralReports;

namespace RapidVet.Repository.PreventiveMedicine
{
    public class PreventiveMedicineRepository : RepositoryBase<RapidVetDataContext>, IPreventiveMedicineRepository
    {
        public PreventiveMedicineRepository()
        {

        }

        public PreventiveMedicineRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public List<PreventiveMedicineItem> GetPatientHistory(int patientId)
        {
            var animalKindId = GetPatientAnimalKindId(patientId);

            var patientHistory =
                DataContext.PreventiveMedicineItems
                           .Include(p => p.PreformingUser)
                           .Include(p => p.PriceListItem)
                           .Include(p => p.PriceListItemType)
                           .Include(p => p.Parent)
                           .Where(p => p.PatientId == patientId).ToList();
            return patientHistory;
        }

        public List<PreventiveMedicineItem> GetCurrentState(int patientId)
        {
            var animalKindId = GetPatientAnimalKindId(patientId);
            var templateList = GetPatientTemplate(patientId, animalKindId);
            var result = new List<PreventiveMedicineItem>();
            var patientHistory =
                DataContext.PreventiveMedicineItems
                           .Include(p => p.PreformingUser)
                           .Include(p => p.PriceListItem)
                           .Include(p => p.PriceListItemType)
                // .Include(p => p.Parent)
                           .Where(p => p.PatientId == patientId).ToList();

            foreach (var vot in templateList)
            {
                PreventiveMedicineItem singleItem;
                var nextSchedule =
                    patientHistory.Where(
                        h =>
                        h.PriceListItemId == vot.PriceListItemId &&
                        h.Scheduled.HasValue)// &&
                    //h.ParentPreventiveItemId.HasValue && h.ParentPreventiveItemId.Value > 0)
                                  .OrderByDescending(h => h.Scheduled).Take(1).ToList();

                var lastPreformedDate = patientHistory.Where(ph => ph.PriceListItemId == vot.PriceListItemId)
                                                .OrderByDescending(ph => ph.Preformed)
                                                .Take(1).ToList();

                if (lastPreformedDate.Any() && nextSchedule.Any())
                {
                    lastPreformedDate.First().Scheduled = nextSchedule.First().Scheduled;
                }
                if (!lastPreformedDate.Any())
                {
                    lastPreformedDate = nextSchedule;
                }

                if (lastPreformedDate.Any())
                {
                    singleItem = lastPreformedDate.First();
                }
                else
                {
                    singleItem = new PreventiveMedicineItem()
                        {
                            PriceListItemId = vot.PriceListItemId,
                            PriceListItem = vot.PriceListItem,
                            PriceListItemTypeId = vot.PriceListItem.ItemTypeId,
                            PatientId = patientId,
                        };
                }

                result.Add(singleItem);
            }

            return result.OrderBy(r => r.PriceListItemTypeId).ToList();

        }

        public IQueryable<PriceListItem> GetPriceListItems(int clinicId)
        {
            var clinicCategoryIds =
                DataContext.PriceListCategories.Where(plc => plc.ClinicId == clinicId).Select(c => c.Id);
            return
                DataContext.PriceListItems.Include(pli => pli.ItemsTariffs).Where(
                    pli => pli.Available &&
                    (pli.ItemTypeId == (int)PriceListItemTypeEnum.PreventiveTreatments ||
                     pli.ItemTypeId == (int)PriceListItemTypeEnum.Vaccines) &&
                    clinicCategoryIds.Contains(pli.CategoryId));
        }

        public PreventiveMedicineItem GetPreventiveItem(int preventiveId, bool includeExtraEntities = false)
        {
            if (!includeExtraEntities)
            {
                return DataContext.PreventiveMedicineItems.Include(p => p.Patient).SingleOrDefault(p => p.Id == preventiveId);
            }
            else
            {
                return DataContext.PreventiveMedicineItems.Include(p => p.Patient.Client).Include(p => p.PriceListItem).Include("Patient.AnimalRace").SingleOrDefault(p => p.Id == preventiveId);
            }
        }

        public OperationStatus Delete(PreventiveMedicineItem preventive)
        {
            var childPreventiveMedicineItems =
                DataContext.PreventiveMedicineItems.Where(
                    p =>
                    p.ParentPreventiveItemId.HasValue && p.ParentPreventiveItemId.Value == preventive.Id &&
                    p.Preformed == null && p.PreformingUserId == null);

            foreach (var child in childPreventiveMedicineItems)
            {
                child.VisitId = null;
                DataContext.PreventiveMedicineItems.Remove(child);
            }

            DataContext.PreventiveMedicineItems.Remove(preventive);
            return base.Save(preventive);
        }

        public bool IsRabiesVaccine(int priceListItemId)
        {
            var priceListItem = DataContext.PriceListItems.Single(pli => pli.Id == priceListItemId);
            var result = false;

            if (priceListItem.ItemTypeId == (int)PriceListItemTypeEnum.Vaccines)
            {
                //old code: result = priceListItem.Name.Contains(RepositoryConsts.RABIES_VACCINE_NAME);
                result = priceListItem.IsRabiesVaccine;
            }
            return result;
        }

        public OperationStatus Create(PreventiveMedicineItem preventiveMedicineItem)
        {
            DataContext.PreventiveMedicineItems.Add(preventiveMedicineItem);
            return base.Save(preventiveMedicineItem);
        }

        public OperationStatus EditItem(PreventiveMedicineItem preventiveMedicineItem)
        {
            DataContext.PreventiveMedicineItems.Attach(preventiveMedicineItem);
            DataContext.Entry(preventiveMedicineItem).State = System.Data.Entity.EntityState.Modified;

            return base.Save(preventiveMedicineItem);
        }


        public DateTime? GetPreventiveMedicineItemNextDate(int clinicId, int priceListItemId, int patientId)
        {
            DateTime? result = null;
            var animalKindId =
                DataContext.Patients.Include(p => p.AnimalRace).Single(p => p.Id == patientId).AnimalRace.AnimalKindId;
            var vot =
                DataContext.VaccineAndTreatments.SingleOrDefault(
                    vt =>
                    vt.ClinicId == clinicId && vt.AnimalKindId == animalKindId && vt.PriceListItemId == priceListItemId);

            if (vot != null)
            {
                var childList =
                    DataContext.NextVaccineAndTreatments
                               .Include(n => n.ParentVaccineOrTreatment)
                               .Where(n => n.VaccineOrTreatmentId == vot.Id)
                               .OrderByDescending(n => n.Id)
                               .Take(1);



            }

            return result;
        }

        public IQueryable<PreventiveMedicineItem> GetAllForPatient(int patientId, int? PriceListItemId)
        {
            if (!PriceListItemId.HasValue)
            {
                return
                    DataContext.PreventiveMedicineItems.Include(pmi => pmi.PriceListItem)
                               .Where(pmi => pmi.PatientId == patientId)
                               .OrderByDescending(pmi => pmi.Preformed);
            }
            else
            {
                return
                 DataContext.PreventiveMedicineItems.Include(pmi => pmi.PriceListItem)
                            .Where(pmi => pmi.PatientId == patientId && pmi.PriceListItemId == PriceListItemId.Value)
                            .OrderByDescending(pmi => pmi.Preformed);
            }
        }


        public void UpdateItems(int clinicId)
        {

            var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var tomorrow = today.AddDays(1);

            var clinicVaccineOrTreatmentIds =
                DataContext.VaccineAndTreatments.Where(v => v.ClinicId == clinicId).Select(v => v.Id).ToList();

            var clinicPatientsIds =
                DataContext.Patients.Include(p => p.Client)
                           .Where(p => p.Client.ClinicId == clinicId && p.Active)
                           .Select(p => p.Id).ToList();

            var changedNextVaccineOrTreatmentItems =
                DataContext.NextVaccineAndTreatments.Include(n => n.ParentVaccineOrTreatment).Where(
                    n =>
                    clinicVaccineOrTreatmentIds.Contains(n.VaccineOrTreatmentId) &&
                    ((n.Created >= today && n.Created < tomorrow) ||
                     (n.Updated.HasValue && n.Updated >= today && n.Updated < tomorrow))).ToList();

            foreach (var item in changedNextVaccineOrTreatmentItems)
            {
                var preventiveMedicineItems =
                    DataContext.PreventiveMedicineItems.Include(p => p.Patient)
                               .Include("Patient.Client")
                               .Include("Patient.AnimalRace")
                               .Where(
                                   p =>
                                   clinicPatientsIds.Contains(p.Id) &&
                                   p.PriceListItemId == item.VaccineOrTreatment.PriceListItemId &&
                                   p.Patient.AnimalRace.AnimalKindId == item.VaccineOrTreatment.AnimalKindId).ToList();


            }

            DataContext.SaveChanges();

        }

        public bool ItemPreformed(int preventiveMedicineItemId, int clinicId)
        {
            var preventiveItem =
                DataContext.PreventiveMedicineItems.Include(p => p.Patient)
                           .Include("Patient.AnimalRace")
                           .Single(p => p.Id == preventiveMedicineItemId);

            var vaacineOrTreatmentIds =
                DataContext.VaccineAndTreatments.Where(
                    v =>
                    v.ClinicId == clinicId && v.AnimalKindId == preventiveItem.Patient.AnimalRace.AnimalKindId &&
                    v.PriceListItemId == preventiveItem.PriceListItemId).Select(v => v.Id).ToList();

            var nextVaccineOrTreatments =
                DataContext.NextVaccineAndTreatments
                           .Include(n => n.VaccineOrTreatment)
                           .Include("VaccineOrTreatment.PriceListItem")
                           .Where(
                               n => vaacineOrTreatmentIds.Contains(n.ParentVaccineOrTreatmentId)).ToList();

            //remove all preventive item reminders
            DataContext.PreventiveMedicineItems.Delete(p => p.PriceListItemId == preventiveItem.PriceListItemId && p.PatientId == preventiveItem.PatientId && !p.Preformed.HasValue);


            foreach (var nvt in nextVaccineOrTreatments)
            {
                var item = new PreventiveMedicineItem()
                    {
                        PatientId = preventiveItem.PatientId,
                        ParentPreventiveItemId = preventiveItem.Id,
                        PriceListItemId = nvt.VaccineOrTreatment.PriceListItemId,
                        Scheduled = preventiveItem.Preformed.HasValue ? preventiveItem.Preformed.Value.AddDays(nvt.DaysToNext) : DateTime.Now.AddDays(nvt.DaysToNext),
                        Quantity = 1,
                        PriceListItemTypeId = nvt.VaccineOrTreatment.PriceListItem.ItemTypeId,
                    };
                DataContext.PreventiveMedicineItems.Add(item);
            }

            return DataContext.SaveChanges() > -1;
        }
        public bool ReomveNextVaccineAndTreatmentsByID(int id)
        {
            try
            {
                var nextVaccineAndTreatments =
               DataContext.NextVaccineAndTreatments
                          .Include(n => n.VaccineOrTreatment)
                          .Include("VaccineOrTreatment.PriceListItem")
                          .Where(vt => vt.ParentVaccineOrTreatmentId == id).ToList();
                foreach (var obj in nextVaccineAndTreatments)
                {
                    DataContext.NextVaccineAndTreatments.Remove(obj);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;           
        }


        //update next items when parent date changes
        public bool ItemPreformedUpdate(int preventiveMedicineItemId, int clinicId)
        {
            var preventiveItem =
               DataContext.PreventiveMedicineItems.Include(p => p.Patient)
                          .Include("Patient.AnimalRace")
                          .Single(p => p.Id == preventiveMedicineItemId);

            var vaacineOrTreatmentIds =
                DataContext.VaccineAndTreatments.Where(
                    v =>
                    v.ClinicId == clinicId && v.AnimalKindId == preventiveItem.Patient.AnimalRace.AnimalKindId &&
                    v.PriceListItemId == preventiveItem.PriceListItemId).Select(v => v.Id).ToList();

            var nextVaccineOrTreatments =
                DataContext.NextVaccineAndTreatments
                           .Include(n => n.VaccineOrTreatment)
                           .Include("VaccineOrTreatment.PriceListItem")
                           .Where(
                               n => vaacineOrTreatmentIds.Contains(n.ParentVaccineOrTreatmentId)).ToList();


            foreach (var nvt in nextVaccineOrTreatments)
            {
                var preventiveNextItem = DataContext.PreventiveMedicineItems.FirstOrDefault(p => p.PatientId == preventiveItem.PatientId && p.PriceListItemId == nvt.VaccineOrTreatment.PriceListItemId && !p.Preformed.HasValue && p.Scheduled.HasValue && p.Scheduled.Value > preventiveItem.Preformed.Value);

                if (preventiveNextItem == null)
                {
                    var item = new PreventiveMedicineItem()
                    {
                        PatientId = preventiveItem.PatientId,
                        ParentPreventiveItemId = preventiveItem.Id,
                        PriceListItemId = nvt.VaccineOrTreatment.PriceListItemId,
                        Scheduled = preventiveItem.Preformed.HasValue ? preventiveItem.Preformed.Value.AddDays(nvt.DaysToNext) : DateTime.Now.AddDays(nvt.DaysToNext),
                        Quantity = 1,
                        PriceListItemTypeId = nvt.VaccineOrTreatment.PriceListItem.ItemTypeId,
                    };

                    DataContext.PreventiveMedicineItems.Add(item);
                }
                else
                {
                    preventiveNextItem.Scheduled = preventiveItem.Preformed.Value.AddDays(nvt.DaysToNext);
                }
            }

            return DataContext.SaveChanges() > -1;
        }


        public IQueryable<PreventiveMedicineItem> GetItems(int clinicId, DateTime fromDate, DateTime toDate, int? animalKindId,
                                   bool includeExternalPreformances, bool includeInactivePatients, List<int> selectedItemIds)
        {
            var patientIds = (from t in DataContext.Patients
                              where t.Client.ClinicId == clinicId &&
                              (includeInactivePatients || t.Active) &&
                              (!animalKindId.HasValue || animalKindId.Value == 0 || t.AnimalRace.AnimalKindId == animalKindId.Value)
                              select t.Id).ToList();

            toDate = toDate.AddDays(1);
            var data =
                DataContext.PreventiveMedicineItems
                .Include(p => p.PreformingUser)
                .Include(p => p.PriceListItem)
                .Include(p => p.Patient)
                .Include("Patient.AnimalColor")
                .Include("Patient.Client")
                .Include("Patient.Client.Addresses.City")
                .Include("Patient.Client.Phones.PhoneType")
                .Include("Patient.AnimalRace.AnimalKind")
                .Where(p => patientIds.Contains(p.PatientId) && p.Preformed.HasValue && p.Preformed.Value >= fromDate &&
                    (includeExternalPreformances || !p.ExternalyPreformed) &&
                    (selectedItemIds.Count == 0 || selectedItemIds.Contains(p.PriceListItemId)) &&
                    p.Preformed.Value < toDate);

            return data.OrderBy(p => p.Preformed.Value);
        }

        public List<PreformedPreventiveReportModel> GetReportDataItems(int clinicId, DateTime fromDate, DateTime toDate, int? animalKindId,
                                   bool includeExternalPreformances, bool includeInactivePatients, List<int> selectedItemIds)
        {
            toDate = toDate.AddDays(1);
            
            string strSelectedItemIds = "0";
            if (selectedItemIds.Count > 0)
                strSelectedItemIds = String.Join(", ", selectedItemIds.ToArray());

            string strAnimalKindId = "-1";
            if (animalKindId != null)
                strAnimalKindId = animalKindId.ToString();

            string Query = "SELECT ClientName = c.FirstName + ' ' + c.LastName, " +
                    " ClientIdCard = c.IdCard," +
                    " Date = CONVERT(varchar(20),pi.Preformed,103)," +
                    " PatientChip = p.ElectronicNumber," +
                    " PatientColor = ac.Value," +
                    " PatientGender = case when p.GenderId = 1 then N'זכר' ELSE N'נקבה' end," +
                    " PatientKind = ar.Value," +
                    " PatientName = p.Name," +
                    " ExternalyPreformed = pi.ExternalyPreformed," +
                    " ExternalDrName = pi.ExternalDrName," +
                    " PreformingUser = case when pi.PreformingUserId IS NULL THEN N'בוצע חיצונית' else null end," +
                    " UserTitle = case when pi.PreformingUserId IS NULL OR u.TitleId IS NULL THEN null else t.Name end," +
                    " UserFirstName = case when pi.PreformingUserId IS NULL then null else u.FirstName end," +
                    " UserLastName = case when pi.PreformingUserId IS NULL then null else u.LastName end," +
                    " PriceListItemName = pli.Name," +
                    " CellPhone = ph1.PhoneNumber," +
                    " HomePhone = ph2.PhoneNumber," +
                    " Address = rtrim(ltrim(ISNULL(a.Street,'') + ' ' + ISNULL(cc.Name,'')))," +
                    " PatientBirthDate = p.BirthDate " +
            " FROM PreventiveMedicineItems pi inner join " +
                 " Patients p on p.id = pi.PatientId inner join " +
                 " Clients c on c.id = p.ClientId inner join " +
                 " AnimalColors ac on ac.Id = p.AnimalColorId inner join " +
                 " AnimalRaces ar on ar.Id = p.AnimalRaceId left join " +
                 " Users u on u.Id = pi.PreformingUserId left join " +
                 " Titles t on t.Id = u.TitleId left join " +
                 " PriceListItems pli on pli.Id = pi.PriceListItemId left join " +
                 " Phones ph1 on ph1.ClientId = p.ClientId and ph1.PhoneTypeId=1/*mobile*/  left join " +
                 " Phones ph2 on ph2.ClientId = p.ClientId and ph1.PhoneTypeId=2/*home*/ left join " +
                 " Addresses a on a.ClientId = p.ClientId left join " +
                 " Cities cc on cc.Id = a.CityId " +
            " WHERE c.ClinicId = " + clinicId +
            " AND ("+ (includeInactivePatients ? "1" : "0") +"= 1 or p.Active = 1) "+
            " AND (" + (!animalKindId.HasValue ? "0" : "1") + " = 0 or " + strAnimalKindId + " = 0 or ar.AnimalKindId =" + strAnimalKindId + ") " +
            " AND (pi.Preformed is not null and pi.Preformed > '" + fromDate.ToString("yyyy-MM-dd") + "') " +
            " AND (" + (includeExternalPreformances ? "1" : "0") + " = 1 or pi.ExternalyPreformed = 0) " +
            " AND (" + selectedItemIds.Count.ToString() + " = 0 or pi.PriceListItemId in (" + strSelectedItemIds + ")) " +
            " AND pi.Preformed < '" + toDate.ToString("yyyy-MM-dd") + "' " +
            " ORDER BY pi.Preformed ASC; ";
            
            var res = DataContext.Database.SqlQuery<PreformedPreventiveReportModel>(Query);
            
            return res.ToList();
        }

        public IQueryable<PriceListItem> GetRabisVaccinePriceListItems(int clinicId)
        {
            var clinicCategories =
                DataContext.PriceListCategories.Where(c => c.ClinicId == clinicId && c.Available)
                           .Select(c => c.Id)
                           .ToList();
            return
                DataContext.PriceListItems.Where(
                    p => clinicCategories.Contains(p.CategoryId) && p.IsRabiesVaccine);

        }


        private int GetPatientAnimalKindId(int patientId)
        {
            return
                DataContext.Patients.Include(p => p.AnimalRace).Single(p => p.Id == patientId).AnimalRace.AnimalKindId;
        }

        private List<VaccineOrTreatment> GetPatientTemplate(int patientId, int animalKindId)
        {
            var patient = DataContext.Patients.Include(c => c.Client).Single(p => p.Id == patientId);
            return DataContext.VaccineAndTreatments.Include(vat => vat.PriceListItem)
                              .Include(vat => vat.NextVaccinesAndTreatments)
                              .Where(vat => vat.ClinicId == patient.Client.ClinicId && vat.AnimalKindId == animalKindId && vat.Active)
                              .OrderBy(vat => vat.Order).ToList();
        }

        public IQueryable<PreventiveMedicineItem> GetRemindersForHomePage(int clinicId)
        {
            var startOfToday = DateTime.Now.Date;
            var startOfTomorrow = startOfToday.AddDays(1);
            return
                DataContext.PreventiveMedicineItems.Include("Patient.Client")
                           .Include(p => p.PriceListItem)
                           .Where(
                               p =>
                               p.Patient.Client.ClinicId == clinicId &&
                               (p.Scheduled.HasValue && (startOfToday <= p.Scheduled.Value && p.Scheduled.Value < startOfTomorrow)) &&
                               !p.Preformed.HasValue);
        }

        public List<PriceListItem> GetPreventiveMedicineItemsByClinic(int clinicId)
        {
            var clinicCategoryIds =
                DataContext.PriceListCategories.Where(plc => plc.ClinicId == clinicId).Select(c => c.Id);

            return
                DataContext.PriceListItems.Include(pli => pli.ItemsTariffs).Where(
                    pli =>
                    (pli.ItemTypeId == (int)PriceListItemTypeEnum.Vaccines ||
                     pli.ItemTypeId == (int)PriceListItemTypeEnum.PreventiveTreatments)
                    &&
                    clinicCategoryIds.Contains(pli.CategoryId)).OrderBy(p => p.Name).ToList();

        }

        public List<SelectListItem> GetPreventiveMedicineItemsDropDownForClinic(int clinicId)
        {
            var list = new List<SelectListItem>();
            var priceListItems = GetPreventiveMedicineItemsByClinic(clinicId);

            foreach (var pli in priceListItems)
            {
                list.Add(new SelectListItem()
                {
                    Value = pli.Id.ToString(),
                    Text = pli.Name
                });
            }
            return list;
        }

        public List<PreventiveMedicineItem> GetPreventiveMedicineItemsByPriceListItem(int clinicId, int priceListItemId)
        {
            return
                DataContext.PreventiveMedicineItems.Include(p => p.Patient.Client).Where(
                    pmi => pmi.PriceListItemId == priceListItemId && pmi.Patient.Client.ClinicId == clinicId).ToList();

        }

        public OperationStatus ChangePriceListItemIdInAllItems(int oldPriceListItemId, int newPriceListItemId)
        {
            DataContext.PreventiveMedicineItems.Where(
                    pmi => pmi.PriceListItemId == oldPriceListItemId)
                     .Update(p => new PreventiveMedicineItem { PriceListItemId = newPriceListItemId });

            return base.Save();
        }


        public OperationStatus ChangeParentPreventiveItem(int oldParentId, int newParentId)
        {
            DataContext.PreventiveMedicineItems.Where(
                    pmi => pmi.ParentPreventiveItemId == oldParentId)
                     .Update(p => new PreventiveMedicineItem { ParentPreventiveItemId = newParentId });

            return base.Save();
        }
    }
}
