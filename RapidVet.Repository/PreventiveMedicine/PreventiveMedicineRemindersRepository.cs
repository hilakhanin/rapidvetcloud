﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Enums;

namespace RapidVet.Repository.PreventiveMedicine
{
    public class PreventiveMedicineRemindersRepository : RepositoryBase<RapidVetDataContext>, IPreventiveMedicineRemindersRepository
    {
        public PreventiveMedicineRemindersRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public PreventiveMedicineRemindersRepository()
        {

        }


        public IQueryable<PreventiveMedicineItem> GetData(int clinicId, DateTime fromDate, DateTime toDate, int? regionalCouncilId,
            int? animalKindId, bool includeInactiveClients, bool includeInactivePatients, bool futureRemindersOnly, List<int> preventiveItemIds,
            DateTime fromReminderDate, DateTime toReminderDate, List<int> filteredReminders, bool RemoveClientsWithFutureCalendarEntries, List<int> CityIds, int? doctorId)
        {
            var list = DataContext.PreventiveMedicineItems
                               .Include(p => p.PriceListItem)
                               .Include(p => p.Parent)
                               .Include(p => p.Patient)
                               .Include("Patient.Client")
                               .Include("Patient.Client.Emails")
                               .Include("Patient.Client.ClientStatus")
                               .Include("Patient.Client.Phones")
                               .Include("Patient.Client.Phones.PhoneType")
                               .Include("Patient.Client.Addresses")
                               .Include("Patient.Client.Addresses.City")
                               .Include("Patient.AnimalColor")
                               .Include("Patient.AnimalRace")
                               .Include("Patient.AnimalRace.AnimalKind")
                               .OrderBy(c => c.Scheduled)
                .Where(p => p.Patient.Client.ClinicId == clinicId && p.Scheduled.HasValue && p.PriceListItem.Available);

            if (filteredReminders.Count > 0)
            {
                list = list.Where(d => filteredReminders.Contains(d.Id));
            }

            if (fromDate > DateTime.MinValue)
            {
                list = list.Where(d => d.Scheduled.Value >= fromDate);
            }

            if (toDate > DateTime.MinValue)
            {
                toDate = toDate.AddDays(1);
                list = list.Where(d => d.Scheduled.Value < toDate);
            }

            if (fromReminderDate > DateTime.MinValue)
            {
                list = list.Where(d => d.LastReminderDate.HasValue && d.LastReminderDate.Value >= fromReminderDate);
            }

            if (toReminderDate > DateTime.MinValue)
            {
                toReminderDate = toReminderDate.AddDays(1);
                list = list.Where(d => d.LastReminderDate.HasValue && d.LastReminderDate.Value < toReminderDate);
            }

            if (regionalCouncilId.HasValue && regionalCouncilId.Value > 0)
            {
                list =
                    list.Where(
                        d => d.Patient.Client.Addresses.FirstOrDefault() != null && d.Patient.Client.Addresses.FirstOrDefault().City.RegionalCouncilId == regionalCouncilId);
                // d.Patient.Client.RegionalCounilId == regionalCouncilId);
            }

            if (animalKindId.HasValue && animalKindId.Value > 0)
            {
                list = list.Where(p => p.Patient.AnimalRace.AnimalKindId == animalKindId.Value);
            }

            if (doctorId.HasValue && doctorId.Value > 0)
            {
                list = list.Where(p => p.Patient.Client.VetId == doctorId.Value);
            }

            if (preventiveItemIds.Count > 0)
            {
                list = list.Where(d => preventiveItemIds.Contains(d.PriceListItemId));
            }

            if (CityIds.Count > 0)
            {
                list = list.Where(d => d.Patient.Client.Addresses.Any(a => CityIds.Contains(a.CityId)));
            }

            if (!includeInactiveClients)
            {
                list = list.Where(d => !d.Patient.Client.ClientStatus.Name.Equals(Resources.Client.ClientStatusInactive));
            }

            if (!includeInactivePatients)
            {
                list = list.Where(p => p.Patient.Active);
            }

            if (futureRemindersOnly)
            {
                list = list.Where(d => d.ReminderCount == 0);
            }

            foreach (var item in list)
            {
                var calendarEntriey = DataContext.CalenderEntries.Where(c => c.PatientId == item.PatientId && c.Date > DateTime.Now)
                    .OrderBy(c => c.Date)
                               .FirstOrDefault();

                item.Patient.CalenderEntries.Add(calendarEntriey);
            }

            if (RemoveClientsWithFutureCalendarEntries)
            {
                list = list.Where(d => !d.Patient.Client.CalenderEntries.Any(c => c.Date >= DateTime.Now));
            }



            return list;

            //******
            //var clinicPatientIds =
            //    DataContext.Patients.Include(p => p.Client)
            //    .Include(p => p.AnimalRace)
            //               .Where(p => p.Client.ClinicId == clinicId)
            //               .Select(p => p.Id)
            //               .ToList();

            //var data =
            //    DataContext.PreventiveMedicineItems
            //               .Include(p => p.PriceListItem)
            //               .Include(p => p.Parent)
            //               .Include(p => p.Patient)
            //               .Include("Patient.Client")
            //               .Include("Patient.Client.Emails")
            //               .Include("Patient.Client.ClientStatus")
            //               .Include("Patient.Client.Phones")
            //               .Include("Patient.Client.Phones.PhoneType")
            //               .Include("Patient.Client.Addresses")
            //               .Include("Patient.Client.Addresses.City")
            //               .Include("Patient.AnimalRace")
            //               .Include("Patient.AnimalRace.AnimalKind")
            //               .Where(
            //                   p =>
            //                   clinicPatientIds.Contains(p.PatientId) && !p.Preformed.HasValue);

            //if (fromDate > DateTime.MinValue)
            //{
            //    data =
            //        data.Where(
            //            d =>
            //            (d.Parent.LastReminderDate.HasValue && d.Parent.LastReminderDate.Value >= fromDate) ||
            //            (d.LastReminderDate.HasValue && d.LastReminderDate.Value >= fromDate));
            //}

            //if (toDate > DateTime.MinValue)
            //{
            //    toDate = toDate.AddDays(1);
            //    data =
            //         data.Where(
            //             d =>
            //             (d.Parent.LastReminderDate.HasValue && d.Parent.LastReminderDate.Value < toDate) ||
            //             (d.LastReminderDate.HasValue && d.LastReminderDate.Value < toDate));
            //}

            //if (regionalCouncilId.HasValue && regionalCouncilId.Value > 0)
            //{
            //    data =
            //        data.Where(
            //            d =>
            //            d.Patient.Client.RegionalCounilId.HasValue &&
            //            d.Patient.Client.RegionalCounilId.Value == regionalCouncilId.Value);
            //}

            //if (animalKindId.HasValue && animalKindId.Value > 0)
            //{
            //    data = data.Where(p => p.Patient.AnimalRace.AnimalKindId == animalKindId.Value);
            //}

            //if (preventiveItemIds.Count > 0)
            //{
            //    data = data.Where(d => preventiveItemIds.Contains(d.PriceListItemId));
            //}

            //if (!includeInactiveClients)
            //{
            //    data = data.Where(d => d.Patient.Client.Active);
            //}

            //if (!includeInactivePatients)
            //{
            //    data = data.Where(p => p.Patient.Active);
            //}

            //if (futureRemindersOnly)
            //{
            //    data = data.Where(d => d.Scheduled.HasValue && d.Scheduled.Value > DateTime.Now);
            //}

            //foreach (var item in data)
            //{
            //    var calendarEntries =

            //        item.Patient.CalenderEntries =
            //        DataContext.CalenderEntries.Where(c => c.PatientId == item.PatientId && c.Date > DateTime.Now)
            //                   .OrderBy(c => c.Date)
            //                   .Take(1).ToList();
            //}


            //return data;
        }
        public List<RapidVet.WebModels.PreventiveMedicine.PreventiveReminderIndexModel> GetDataListPrint(int clinicId, DateTime fromDate, DateTime toDate, int? regionalCouncilId,
            int? animalKindId, bool includeInactiveClients, bool includeInactivePatients, bool futureRemindersOnly, List<int> preventiveItemIds,
            DateTime fromReminderDate, DateTime toReminderDate, List<int> filteredReminders, bool RemoveClientsWithFutureCalendarEntries, List<int> CityIds, int? doctorId, bool updateReminders)
        {
            var data = DataContext.Database.SqlQuery<RapidVet.WebModels.PreventiveMedicine.PreventiveReminderIndexModel>(
                String.Format(
@"SELECT
pmi.Id AS PmiID,
c.Id as ClientId,
pmi.Scheduled as scheduledDateStr,
c.FirstName + ' ' + c.LastName as clientName,
p.Name as patientName,
animKind.Value as patientKind,
cStat.Name as [status],
pli.Name as priceListItemName,
addrCityTable.Street + ' ' + addrCityTable.Name as clientAddress,
(select top 1 PhoneNumber from Phones where Phones.PhoneTypeId = {0} AND Phones.ClientId = c.Id order by id desc) as clientHomePhone,
(select top 1 PhoneNumber from Phones where Phones.PhoneTypeId = {1} AND Phones.ClientId = c.Id order by id desc) as clientCellPhone,
ce.Name as clientEmail,
{17} as lastReminderDateStr,
calETable.nextAppointmentDateStr,
pmi.ReminderComments as comments,
pmi.ReminderCount{18} as reminderCount,
cast(case when pmi.ReminderCount > 0 THEN 1 ELSE 0 end as bit)  as Selected,
CAST(CASE 
                  WHEN pmi.Scheduled < GETDATE() 
                     THEN 1 
                  ELSE 0 
             END AS bit) as IsPast
from PreventiveMedicineItems pmi
left join PriceListItems pli on pmi.PriceListItemId = pli.Id
left join PreventiveMedicineItems pmiP on pmi.ParentPreventiveItemId = pmiP.Id
left join Patients p on pmi.PatientId = p.Id
left join Clients c on p.ClientId = c.Id
left join Emails ce on c.Id = ce.ClientId
left join ClientStatus cStat on c.StatusId = cStat.Id
left join AnimalRaces animRace on animRace.Id = p.AnimalRaceId
left join AnimalKinds animKind on animKind.Id = animRace.AnimalKindId
left join (select * from (select addrIN.Id, addrIN.ClientId, addrIN.Street, cityIN.Name, cityIN.RegionalCouncilId from Addresses addrIN JOIN Cities cityIN on cityIN.Id = addrIN.CityId) as addrCity) as addrCityTable on addrCityTable.ClientId = c.Id
left join (select * from (select date as nextAppointmentDateStr,PatientId from CalenderEntries where Date > GETDATE()) as calE2) as calETable on calETable.PatientId = p.Id
where
c.ClinicId = {5}
AND pmi.Scheduled >= CONVERT(datetime, '{2}', 103)
AND pmi.Scheduled <= CONVERT(datetime, '{3}', 103)
AND pli.Available = 1
AND pmi.Scheduled IS NOT NULL
{4}{6}{7}{8}{9}{10}{11}{12}{13}{14}{15}{16}"
                , (int)PhoneTypeEnum.Mobile,//{0}
                 (int)PhoneTypeEnum.Home,//{1}
                 fromDate.ToString("dd/MM/yyyy"),//{2}
                 toDate.ToString("dd/MM/yyyy"),//{3}
                 includeInactiveClients ? String.Empty : String.Format(" AND cStat.Name <> '{0}' ", Resources.Client.ClientStatusInactive),//{4} לא פעיל
                 clinicId,//{5}
                 includeInactivePatients ? String.Empty : " AND p.Active = 1 ",//{6}
                 fromReminderDate > DateTime.MinValue ? String.Format(" AND pmi.LastReminderDate >= CONVERT(DATETIME, '{0}' ,103) ", fromReminderDate.ToString("dd/MM/yyyy HH:mm")) : String.Empty,//{7}
                 toReminderDate > DateTime.MinValue ? String.Format(" AND pmi.LastReminderDate < CONVERT(DATETIME, '{0}' ,103) ", toReminderDate.ToString("dd/MM/yyyy HH:mm")) : String.Empty,//{8}
                 futureRemindersOnly ? " AND pmi.ReminderCount = 0 " : String.Empty,//{9}
                 RemoveClientsWithFutureCalendarEntries ? " AND calETable.nextAppointmentDateStr < GETDATE() " : String.Empty,//{10}
                 animalKindId.HasValue && animalKindId.Value > 0 ? String.Format(" AND animRace.AnimalKindId = {0} ", animalKindId.Value) : String.Empty,//{11}
                 doctorId.HasValue && doctorId.Value > 0 ? String.Format(" AND c.VetId = {0} ", doctorId.Value) : String.Empty,//{12}
                 filteredReminders == null || filteredReminders.Count == 0 ? String.Empty : String.Format(" AND pmi.Id in ({0}) ", getInStatementValues(filteredReminders)),//{13}
                 preventiveItemIds == null || preventiveItemIds.Count == 0 ? String.Empty : String.Format(" AND pmi.PriceListItemId in ({0}) ", getInStatementValues(preventiveItemIds)),//{14}
                 CityIds == null || CityIds.Count == 0 ? String.Empty : String.Format(" AND addrs.CityId in ({0}) ", getInStatementValues(CityIds)),//{15}
                 regionalCouncilId.HasValue && regionalCouncilId.Value > 0 ? String.Format(" AND addrCityTable.RegionalCouncilId in ({0}) ", regionalCouncilId.Value) : String.Empty, //{16}
                 updateReminders ? "GETDATE()" : "pmi.LastReminderDate",//{17}
                 updateReminders ? " + 1" : String.Empty //{18}
                 )).ToList();

            if (updateReminders)
            {
                foreach (var item in data)
                {
                    var ent = DataContext.PreventiveMedicineItems.FirstOrDefault(i => i.Id == item.PmiID);
                    if (ent != null)
                    {
                        var obj = DataContext.PreventiveMedicineItems.Attach(ent);
                        if (obj != null)
                        {
                            obj.LastReminderDate = DateTime.Now;
                            //obj.ReminderCount = item.ReminderCount + 1;
                            obj.ReminderCount = item.ReminderCount;

                            DataContext.SaveChanges();
                        }
                    }                
                }
            }

            return data;
        }


        public List<RapidVet.WebModels.PreventiveMedicine.PreventiveReminderIndexModel> GetDataList(int clinicId, DateTime fromDate, DateTime toDate, int? regionalCouncilId,
            int? animalKindId, bool includeInactiveClients, bool includeInactivePatients, bool futureRemindersOnly, List<int> preventiveItemIds,
            DateTime fromReminderDate, DateTime toReminderDate, List<int> filteredReminders, bool RemoveClientsWithFutureCalendarEntries, List<int> CityIds, int? doctorId, bool updateReminders)
        {
            return DataContext.Database.SqlQuery<RapidVet.WebModels.PreventiveMedicine.PreventiveReminderIndexModel>(
                String.Format(
                //left join (select * from (select addrIN.Id, addrIN.ClientId, addrIN.Street, cityIN.Name, cityIN.RegionalCouncilId from Addresses addrIN JOIN Cities cityIN on cityIN.Id = addrIN.CityId) as addrCity) as addrCityTable on addrCityTable.ClientId = c.Id
@"select * from( SELECT *, (select top 1 addrIN.Street + ' ' + cityIN.Name from Addresses addrIN JOIN Cities cityIN on cityIN.Id = addrIN.CityId and addrIN.ClientId = q.ClientId and addrIN.Street is not null)  as clientAddress
, (select top 1 cityIN.id as CityId from Addresses addrIN JOIN Cities cityIN on cityIN.Id = addrIN.CityId and addrIN.ClientId = q.ClientId and addrIN.Street is not null)  as CityId  
, (select top 1 cityIN.RegionalCouncilId from Addresses addrIN JOIN Cities cityIN on cityIN.Id = addrIN.CityId and addrIN.ClientId = q.ClientId and addrIN.Street is not null)  as RegionalCouncilId from 
(SELECT distinct 
pmi.Id AS PmiID,
c.Id as ClientId,
pmi.Scheduled as scheduledDateStr,
c.FirstName + ' ' + c.LastName as clientName,
p.Name as patientName,
animKind.Value as patientKind,
cStat.Name as [status],
pli.Name as priceListItemName,
pmi.PatientId as PatientIdR,
pmi.PatientId as PatientId,
pmi.Id as PreventiveMedicineItemId,
(select top 1 PhoneNumber from Phones where Phones.PhoneTypeId = {0} AND Phones.ClientId = c.Id order by id desc) as clientHomePhone,
(select top 1 PhoneNumber from Phones where Phones.PhoneTypeId = {1} AND Phones.ClientId = c.Id order by id desc) as clientCellPhone,
ce.Name as clientEmail,
{17} as lastReminderDateStr,
calETable.nextAppointmentDateStr,
pmi.ReminderComments as comments,
pmi.ReminderCount{18} as reminderCount,
cast(case when pmi.ReminderCount > 0 THEN 1 ELSE 0 end as bit)  as Selected,
CAST(CASE 
                  WHEN pmi.Scheduled < GETDATE() 
                     THEN 1 
                  ELSE 0 
             END AS bit) as IsPast
from PreventiveMedicineItems pmi
left join PriceListItems pli on pmi.PriceListItemId = pli.Id
left join PreventiveMedicineItems pmiP on pmi.ParentPreventiveItemId = pmiP.Id
left join Patients p on pmi.PatientId = p.Id
left join Clients c on p.ClientId = c.Id
left join Emails ce on c.Id = ce.ClientId
left join ClientStatus cStat on c.StatusId = cStat.Id
left join AnimalRaces animRace on animRace.Id = p.AnimalRaceId
left join AnimalKinds animKind on animKind.Id = animRace.AnimalKindId
left join (select * from (select date as nextAppointmentDateStr,PatientId from CalenderEntries where Date > GETDATE()) as calE2) as calETable on calETable.PatientId = p.Id
where
c.ClinicId = {5}
AND pmi.Scheduled >= CONVERT(datetime, '{2}', 103)
AND pmi.Scheduled <= CONVERT(datetime, '{3}', 103)
AND pli.Available = 1
AND pmi.Scheduled IS NOT NULL
{4}{6}{7}{8}{9}{10}{11}{12}{13}{14}{15}{16}) q "
                , (int)PhoneTypeEnum.Mobile,/*{0}*/
                 (int)PhoneTypeEnum.Home,/*{1}*/
                 fromDate.ToString("dd/MM/yyyy"),/*{2}*/
                 toDate.ToString("dd/MM/yyyy"),/*{3}*/
                 includeInactiveClients ? String.Empty : String.Format(" AND cStat.Name <> '{0}' ", Resources.Client.ClientStatusInactive),/*{4} לא פעיל*/
                 clinicId,/*{5}*/
                 includeInactivePatients ? String.Empty : " AND p.Active = 1 ",/*{6}*/
                 fromReminderDate > DateTime.MinValue ? String.Format(" AND pmi.LastReminderDate >= CONVERT(DATETIME, '{0}' ,103) ", fromReminderDate.ToString("dd/MM/yyyy HH:mm")) : String.Empty,/*{7}*/
                 toReminderDate > DateTime.MinValue ? String.Format(" AND pmi.LastReminderDate < CONVERT(DATETIME, '{0}' ,103) ", toReminderDate.ToString("dd/MM/yyyy HH:mm")) : String.Empty,/*{8}*/
                 futureRemindersOnly ? " AND pmi.ReminderCount = 0 " : String.Empty,/*{9}*/
                 RemoveClientsWithFutureCalendarEntries ? " AND calETable.nextAppointmentDateStr < GETDATE() " : String.Empty,/*{10}*/
                 animalKindId.HasValue && animalKindId.Value > 0 ? String.Format(" AND animRace.AnimalKindId = {0} ", animalKindId.Value) : String.Empty,/*{11}*/
                 doctorId.HasValue && doctorId.Value > 0 ? String.Format(" AND c.VetId = {0} ", doctorId.Value) : String.Empty,/*{12}*/
                 filteredReminders == null || filteredReminders.Count == 0 ? String.Empty : String.Format(" AND pmi.Id in ({0}) ", getInStatementValues(filteredReminders)),/*{13}*/
                 preventiveItemIds == null || preventiveItemIds.Count == 0 ? String.Empty : String.Format(" AND pmi.PriceListItemId in ({0}) ", getInStatementValues(preventiveItemIds)),/*{14}*/
                 "",// CityIds == null || CityIds.Count == 0 ? String.Empty : String.Format(" AND addrs.CityId in ({0}) ", getInStatementValues(CityIds)),/*{15}*/
                 "",//regionalCouncilId.HasValue && regionalCouncilId.Value > 0 ? String.Format(" AND addrCityTable.RegionalCouncilId in ({0}) ", regionalCouncilId.Value) : String.Empty, /*{16}*/
                 updateReminders ? "GETDATE()" : "pmi.LastReminderDate",/*{17}*/
                 updateReminders ? " + 1" : String.Empty /*{18}*/
                 ) + ")a where 1=1 " + string.Format("{0}{1}",
                 CityIds == null || CityIds.Count == 0 ? String.Empty : String.Format(" AND a.CityId in ({0}) ", getInStatementValues(CityIds)),/*{15}*/
                 regionalCouncilId.HasValue && regionalCouncilId.Value > 0 ? String.Format(" AND a.RegionalCouncilId in ({0}) ", regionalCouncilId.Value) : String.Empty /*{16}*/
                 )).ToList();
        }

        private string getInStatementValues(dynamic list)
        {
            try
            {
                StringBuilder sbT = new StringBuilder();
                foreach (var item in list)
                    sbT.AppendFormat("{0}{1}", sbT.Length == 0 ? String.Empty : ",", item.ToString());

                return sbT.ToString();
            }
            catch
            {
                return String.Empty;
            }
        }
    }
}