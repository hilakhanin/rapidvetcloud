﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.PreventiveMedicine;
using System.Web.Mvc;
using RapidVet.WebModels.GeneralReports;

namespace RapidVet.Repository.PreventiveMedicine
{
    public interface IPreventiveMedicineRepository : IDisposable
    {
        /// <summary>
        /// get patient current preventive medicine state
        /// </summary>
        /// <param name="patientId"> patient id</param>
        /// <returns> List<PreventiveMedicineItem></returns>
        List<PreventiveMedicineItem> GetCurrentState(int patientId);

        /// <summary>
        /// get all preventive medicine items for clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>IQueryable<PriceListItem></returns>
        IQueryable<PriceListItem> GetPriceListItems(int clinicId);

        /// <summary>
        /// get preventive medicine item
        /// </summary>
        /// <param name="preventiveId"> preventive medicine item Id</param>
        /// <returns>PreventiveMedicineItem</returns>
        PreventiveMedicineItem GetPreventiveItem(int preventiveId, bool includeExtraEntities = false);

        /// <summary>
        /// deletes preventive medicine item
        /// </summary>
        /// <param name="preventive">preventive medicine item Id</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(PreventiveMedicineItem preventive);

        /// <summary>
        /// checks if price list item is rabies vaccine
        /// </summary>
        /// <param name="priceListItemId">price list item id</param>
        /// <returns>bool</returns>
        bool IsRabiesVaccine(int priceListItemId);

        /// <summary>
        /// create PreventiveMedicineItem
        /// </summary>
        /// <param name="preventiveMedicineItem"PreventiveMedicineItem obj></param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(PreventiveMedicineItem preventiveMedicineItem);

        /// <summary>
        /// get next preformance date for preventiveMedicineItem
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <param name="priceListItemId">price List Item Id</param>
        /// <param name="patientId">patient id</param>
        /// <returns>DateTime</returns>
        DateTime? GetPreventiveMedicineItemNextDate(int clinicId, int priceListItemId, int patientId);

        /// <summary>
        /// get all preventive medicine items for patient
        /// </summary>
        /// <param name="patientId"> patient id</param>
        /// <returns>IQueryable<PreventiveMedicineItem></returns>
        IQueryable<PreventiveMedicineItem> GetAllForPatient(int patientId, int? PriceListItemId);


        /// <summary>
        /// updates rellevent items with changes made to vaccine or treatments templates
        /// </summary>
        /// <param name="clinicId"></param>
        void UpdateItems(int clinicId);

        
        /// <summary>
        /// creates new preventive medicine items for every item affected by preformance of a given preventiveMedicine item
        /// according to the treatment plan (VaccineOrTreatment, NextVaccineOrTreatment)
        /// </summary>
        /// <param name="preventiveMedicineItemId"> int preventive medicine item id</param>
        /// <param name="clinicId"> int clinic id</param>
        /// <returns>bool</returns>
        bool ItemPreformed(int preventiveMedicineItemId, int clinicId);
        bool ReomveNextVaccineAndTreatmentsByID(int id);
        bool ItemPreformedUpdate(int preventiveMedicineItemId, int clinicId);

        /// <summary>
        /// get preformed preventive medicine items by parameters
        /// </summary>
        /// <param name="clinicId"> int clinic id</param>
        /// <param name="fromDate"> datetime from</param>
        /// <param name="toDate">datetimt to</param>
        /// <param name="animalKindId">int? animal kind id</param>
        /// <param name="includeExternalPreformances">bool include external preformances</param>
        /// <param name="includeInactivePatients">bool include inactive patients</param>
        /// <param name="selectedItemIds"> list of selected preventice item ids</param>
        /// <returns>IQueryable<PreventiveMedicineItem></returns>
        IQueryable<PreventiveMedicineItem> GetItems(int clinicId, DateTime fromDate, DateTime toDate, int? animalKindId,
                                                    bool includeExternalPreformances, bool includeInactivePatients,List<int> selectedItemIds );


        /// <summary>
        /// get preformed preventive medicine items by parameters
        /// </summary>
        /// <param name="clinicId"> int clinic id</param>
        /// <param name="fromDate"> datetime from</param>
        /// <param name="toDate">datetimt to</param>
        /// <param name="animalKindId">int? animal kind id</param>
        /// <param name="includeExternalPreformances">bool include external preformances</param>
        /// <param name="includeInactivePatients">bool include inactive patients</param>
        /// <param name="selectedItemIds"> list of selected preventice item ids</param>
        /// <returns>IQueryable<PreventiveMedicineItem></returns>
        List<PreformedPreventiveReportModel> GetReportDataItems(int clinicId, DateTime fromDate, DateTime toDate, int? animalKindId,
                                  bool includeExternalPreformances, bool includeInactivePatients, List<int> selectedItemIds);

        /// <summary>
        /// get price list item for rabis vaccine by clinic id
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>PriceListItem</returns>
        IQueryable<PriceListItem> GetRabisVaccinePriceListItems(int clinicId);

        IQueryable<PreventiveMedicineItem> GetRemindersForHomePage(int clinicId);

        List<PriceListItem> GetPreventiveMedicineItemsByClinic(int clinicId);

        List<SelectListItem> GetPreventiveMedicineItemsDropDownForClinic(int clinicId);

        OperationStatus EditItem(PreventiveMedicineItem preventiveMedicineItem);

        OperationStatus ChangePriceListItemIdInAllItems(int oldPriceListItemId, int newPriceListItemId);
        List<PreventiveMedicineItem> GetPreventiveMedicineItemsByPriceListItem(int clinicId, int priceListItemId);
        OperationStatus ChangeParentPreventiveItem(int oldParentId, int newParentId);
    }
}
