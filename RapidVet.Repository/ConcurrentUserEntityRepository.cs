﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model.Users;

namespace RapidVet.Repository
{
    public class ConcurrentUserEntityRepository : RepositoryBase<RapidVetDataContext>, IConcurrentUserEntityRepository
    {

        public ConcurrentUserEntityRepository()
        {

        }

        public ConcurrentUserEntityRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }


        /// <summary>
        /// adds the connected user to the concurrent users table
        /// </summary>
        /// <param name="entity">ConcurrentUserEntity</param>
        public void Add(Model.Users.ConcurrentUserEntity entity)
        {
            var old = DataContext.ConcurrentUserEntities.FirstOrDefault(c => c.UserId == entity.UserId);
            if (old != null)
            {
                DataContext.ConcurrentUserEntities.Remove(old);
            }
            DataContext.ConcurrentUserEntities.Add(entity);
            Save(entity);
        }


        /// <summary>
        /// removes user from concurrent users table
        /// </summary>
        /// <param name="userGuid">guid that represents user</param>
        public void Remove(Guid userGuid)
        {
            var entity = DataContext.ConcurrentUserEntities.SingleOrDefault(c => c.UserName == userGuid);
            if (entity != null)
            {
                DataContext.ConcurrentUserEntities.Remove(entity);
                Save(entity);
            }
        }

        /// <summary>
        /// returns a specific ConcurrentUserEntity by user guid
        /// </summary>
        /// <param name="userGuid">guid that represents user</param>
        /// <returns>ConcurrentUserEntity</returns>
        private ConcurrentUserEntity GetItem(Guid userGuid)
        {
            return DataContext.ConcurrentUserEntities.Single(c => c.UserName == userGuid);
        }

        /// <summary>
        /// updates the active clinic id property for the current user
        /// </summary>
        /// <param name="userGuid">guid that represents user</param>
        /// <param name="clinicId"> clinic id</param>
        public void UpdateClinic(Guid userGuid, int clinicId)
        {
            var entity = GetItem(userGuid);
            entity.ActiveClinicId = clinicId;
            Save(entity);
        }

        /// <summary>
        /// updates the clinic group id property for the current user
        /// </summary>
        /// <param name="userGuid">guid that represents user</param>
        /// <param name="clinicGroupId"> clinic group id</param>
        public void UpdateClinicGroup(Guid userGuid, int clinicGroupId)
        {
            var entity = GetItem(userGuid);
            entity.ActiveClinicGroupId = clinicGroupId;
            Save(entity);
        }

        /// <summary>
        /// returns all currently connected users
        /// </summary>
        /// <returns>List(ConcurrentUserEntity)</returns>
        public List<ConcurrentUserEntity> GetList()
        {
            return DataContext.ConcurrentUserEntities.ToList();
        }
    }
}
