﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using System.Data.Entity;

namespace RapidVet.Repository.WaitingListItems
{
    public class WaitingListRepository : RepositoryBase<RapidVetDataContext>, IWaitingListRepository
    {
        public WaitingListRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public WaitingListRepository()
        {

        }

        public List<WaitingListItem> GetWaitingListForClinic(int clinicId)
        {
            return DataContext.WaitingListItems.Include(w => w.Doctor).Include("Client.Phones").Include(w => w.Animal).Where(w => w.ClinicId == clinicId).ToList();
        }

        public OperationStatus Create(WaitingListItem waitingListItem)
        {
            DataContext.WaitingListItems.Add(waitingListItem);
            return base.Save(waitingListItem);
        }

        public WaitingListItem GetWaitingListItem(int waitingListItemId)
        {
            return DataContext.WaitingListItems.SingleOrDefault(w => w.Id == waitingListItemId);
        }

        public OperationStatus Update(WaitingListItem waitingListItem)
        {
            return base.Save(waitingListItem);
        }

        public OperationStatus Remove(int waitingListItemId)
        {
            var waitingListItem = DataContext.WaitingListItems.SingleOrDefault(w => w.Id == waitingListItemId);
            if (waitingListItem != null)
            {
                DataContext.WaitingListItems.Remove(waitingListItem);
                return base.Save(waitingListItem);
            }
            return new OperationStatus() { Success = false };
        }
    }
}
