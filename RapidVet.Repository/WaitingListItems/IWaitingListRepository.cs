﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.FullCalender;


namespace RapidVet.Repository.WaitingListItems
{
    public interface IWaitingListRepository : IDisposable
    {
        /// <summary>
        /// get waitinglistitems of clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>List(Room)</returns>
        List<WaitingListItem> GetWaitingListForClinic(int clinicId);
        
        /// <summary>
        /// create WaitingListItem in clinic
        /// </summary>
        /// <param name="waitingListItem">holiday object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(WaitingListItem waitingListItem);

        /// <summary>
        /// return a WaitingListItem of a specific id
        /// </summary>
        /// <param name="waitingListItemId">id of object</param>
        /// <returns>holiday</returns>
        WaitingListItem GetWaitingListItem(int waitingListItemId);

        /// <summary>
        /// updates waitingListItem in clinic
        /// </summary>
        /// <param name="waitingListItem">waitingListItem object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(WaitingListItem waitingListItem);

        /// <summary>
        /// removes waitingListItemId
        /// </summary>
        /// <param name="waitingListItemId">id of waitingListItem</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Remove(int waitingListItemId);
    }
}