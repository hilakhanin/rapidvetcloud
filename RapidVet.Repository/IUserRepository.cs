using System;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Users;

namespace RapidVet.Repository
{
    public interface IUserRepository : IDisposable
    {
        /// <summary>
        /// return all users
        /// </summary>
        /// <returns>IQueryable(User)</returns>
        IQueryable<User> GetList();

        /// <summary>
        /// returns a specific user
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns>User</returns>
        User GetItem(int id, bool isAdministrator = false);

        /// <summary>
        /// updates a user object in db
        /// </summary>
        /// <param name="user">user object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Save(User user);


        /// <summary>
        /// returns all user in clinic group
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <returns>IQueryable(User)</returns>
        IQueryable<User> GetListByClinicGroup(int clinicGroupId, string ActiveStatus = "All");

         /// <summary>
        /// switch and save all user in clinic group
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <param name="isActive"></param>
        OperationStatus SwitchUsersIdActive(int clinicGroupId, bool isActive);

        /// <summary>
        /// returns user by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns>User</returns>
        User GetItemByUserName(string userName, int id = -1);

        /// <summary>
        /// returns users by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns>User</returns>
        List<User> GetItemsByUserName(string userName);


        /// <summary>
        /// create user object in db
        /// </summary>
        /// <param name="firstName">user first name</param>
        /// <param name="lastName">user last name</param>
        /// <param name="userName">user name</param>
        /// <param name="password">user password</param>
        /// <param name="clinicGroup">clinicgroup id</param>
        /// <returns>User object</returns>
        User Create(string firstName, string lastName, string userName, string password, int? clinicGroup, int? titleId, bool active = true);

        /// <summary>
        /// adds user to clinic
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="roleName">role name (membership)</param>
        /// <param name="clinicId">clinic id</param>
        void AddToClinic(int userId, string roleName, int clinicId);

        /// <summary>
        /// removes user from clinic
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="clinicId">clinic id</param>
        void RemoveFromClinic(int userId, int clinicId);

        /// <summary>
        /// returns all roles in db
        /// </summary>
        /// <returns> IQueryable(Role)</returns>
        IQueryable<Role> GetRoles();

        /// <summary>
        /// returns user roles in clinic
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="clinicId">clinic id</param>
        /// <returns>IQueryable(UsersClinicsRoles)</returns>
        IQueryable<UsersClinicsRoles> GetUserClinicRoles(int userId, int clinicId);

        /// <summary>
        /// return user name from user id
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns>string</returns>
        string GetUserName(int userId);

        /// <summary>
        /// returns all roles for user , cross clinics
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns>IQueryable(UsersClinicsRoles)</returns>
        IQueryable<UsersClinicsRoles> GetUserRoles(int userId);

        /// <summary>
        /// updates user roles in clinic in db
        /// </summary>
        /// <param name="userClinicRoles">list of UsersClinicsRoles objects</param>
        /// <returns>OperationStatus</returns>
        OperationStatus UpdateUserClinicsRoles(List<UsersClinicsRoles> userClinicRoles, bool isActive = true);

        /// <summary>
        /// return all approved users in clinicGroup
        /// </summary>
        /// <returns>IQueryable(User)</returns>
        IEnumerable<User> GetApprovedUsersByClinicGroup(int clinicGroupId);

        /// <summary>
        /// creat new user
        /// </summary>
        /// <returns>User</returns>
        User CreateUser(string userName, string password, string email);

        /// <summary>
        /// returns weekly schedule for user
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="clinicId"></param>
        /// <returns>DailySchedule</returns>
        List<DailySchedule> GetDailySchedules(int userId, int clinicId);


        /// <summary>
        /// returns daily schedule for user
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns>DailySchedule</returns>
        DailySchedule GetDailySchedule(int id);
        DailySchedule GetDailyScheduleOfUserByDay(int userId, DayOfWeek day);

        /// <summary>
        /// creates weekly schedule
        /// </summary>
        /// <param name="dailySchedule">DailySchedule to create</param>
        /// <returns>status</returns>
        OperationStatus CreateDailySchedule(DailySchedule dailySchedule);

        /// <summary>
        /// updates weekly schedule
        /// </summary>
        /// <param name="dailySchedule">DailySchedule to update</param>
        /// <returns>status</returns>
        OperationStatus UpdateDailySchedule(DailySchedule dailySchedule);

        /// <summary>
        /// removes daily schedule
        /// </summary>
        /// <param name="dailyScheduleId">id of DailySchedule to remove</param>
        /// <returns>status</returns>
        OperationStatus RemoveDailySchedule(int dailyScheduleId);

        /// <summary>
        /// get all preventive medicine reminder filters for user
        /// </summary>
        /// <param name="userId">int user id</param>
        /// <returns>List<UserPreventiveMedicineReminderFilter></returns>
        List<UserPreventiveMedicineReminderFilter> GetPreventiveItemReminderFilters(int userId);

        /// <summary>
        /// returns UserPreventiveMedicineReminderFilter by filter id
        /// </summary>
        /// <param name="filterId">int filter id</param>
        /// <returns>UserPreventiveMedicineReminderFilter</returns>
        UserPreventiveMedicineReminderFilter GetPreventiveReminderFilter(int filterId);

        /// <summary>
        /// adds UserPreventiveMedicineReminderFilter to datacontext 
        /// </summary>
        /// <param name="filter">UserPreventiveMedicineReminderFilter</param>
        /// <returns> void</returns>
        void AddPreventiveReminderFilter(UserPreventiveMedicineReminderFilter filter);

        /// <summary>
        /// update filter with selected price list item ids
        /// </summary>
        /// <param name="filterId">int UserPreventiveMedicineReminderFilter id</param>
        /// <param name="priceListItemsIds">list of price list item id's</param>
        /// <returns> bool</returns>
        bool UpdatePreventiveReminderFilterWithItems(int filterId, List<int> priceListItemsIds, List<int> cityIds);

        void RemoveUserRole(UsersClinicsRoles existingRole);
        void AddClinicRole(UsersClinicsRoles role);

        /// <summary>
        /// get doctor lisence number by doctor name
        /// </summary>
        /// <param name="doctorName">string doctor name</param>
        /// <param name="clinicGroupId"></param>
        /// <returns>string</returns>
        string GetDoctorLisence(string doctorName, int clinicGroupId);

        int GetNumberOfActiveClinics();
        int GetNumberOfActiveUsers();
        int GetNumberOfUsers();

        //void RefreshRepository();
    }
}