﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Archives;

namespace RapidVet.Repository.Archives
{
    public interface IArchivesRepository : IDisposable
    {
        /// <summary>
        /// get all document for patient
        /// </summary>
        /// <param name="id"> patien id</param>
        /// <returns> List<ArchiveDocument></returns>
        List<ArchiveDocument> GetPatientDocuments(int id);

        /// <summary>
        /// return single archive item
        /// </summary>
        /// <param name="id"> archive document id</param>
        /// <returns>ArchiveDocument</returns>
        ArchiveDocument GetItem(int id);

        /// <summary>
        /// deletes archive document
        /// </summary>
        /// <param name="document">ArchiveDocument</param>
        void Delete(ArchiveDocument document);

        /// <summary>
        /// searches documents in db
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <param name="queryStr">search query</param>
        /// <returns>HashSet<ArchiveDocument></returns>
        HashSet<ArchiveDocument> Search(int clinicId, string queryStr);

        /// <summary>
        /// get array of images
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <param name="imagesIds"> list if archive document's id'd</param>
        /// <returns></returns>
        Array GetImages(int clinicId, List<int> imagesIds);

        /// <summary>
        /// creates new archive document in db
        /// </summary>
        /// <param name="document">ArchiveDocument</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(ArchiveDocument document);

        /// <summary>
        /// gets all archive documents marked to be showed in patient history
        /// </summary>
        /// <param name="id"> patient id</param>
        /// <returns> List<ArchiveDocument></returns>
        List<ArchiveDocument> GetPatientDocumentsForHistory(int id);

        /// <summary>
        /// get all migrated documents for clinic
        /// </summary>
        /// <param name="clinicid">int clinic id</param>
        /// <returns>IQueryable<ArchiveDocument></returns>
        IQueryable<ArchiveDocument> GetMigratedDocumnets(int clinicid);

        double GetArchiveSizeForClinic(int clinicId);
    }
}
