﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.Archives;

namespace RapidVet.Repository.Archives
{
    public class ArchivesRepository : RepositoryBase<RapidVetDataContext>, IArchivesRepository
    {
        public ArchivesRepository()
        {

        }

        public ArchivesRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public List<ArchiveDocument> GetPatientDocuments(int id)
        {
            return DataContext.ArchiveDocuments.Include(a => a.Patient).Where(ad => ad.PatientId == id).ToList();
        }

        public List<ArchiveDocument> GetPatientDocumentsForHistory(int id)
        {
            return DataContext.ArchiveDocuments.Where(ad => ad.PatientId == id && ad.ShowInPatientHistory).ToList();
        }

        public IQueryable<ArchiveDocument> GetMigratedDocumnets(int clinicid)
        {
            return DataContext.ArchiveDocuments.Where(d => d.ClinicId == clinicid && d.FileMigrationLocation != null);
        }

        public ArchiveDocument GetItem(int id)
        {
            return DataContext.ArchiveDocuments.Include(a => a.Patient).Single(ad => ad.Id == id);
        }

        public void Delete(ArchiveDocument document)
        {

            DataContext.ArchiveDocuments.Remove(document);
        }

        public HashSet<ArchiveDocument> Search(int clinicId, string queryStr)
        {
            var results = new HashSet<ArchiveDocument>();

            var queryArr = queryStr.Split(' ');

            for (int i = 0; i < queryArr.Length; i++)
            {
                queryArr[i] = queryArr[i].Trim();
            }

            foreach (var item in queryArr.Select(q => DataContext.ArchiveDocuments.Where(
                ad =>
                ad.ClinicId == clinicId && (ad.KeyWord1.Contains(q) || ad.KeyWord2.Contains(q) ||
                                            ad.KeyWord3.Contains(q) || ad.KeyWord4.Contains(q) ||
                                            ad.Comments.Contains(q) || ad.Title.Contains(q)))).SelectMany(res => res))
            {
                results.Add(item);
            }

            return results;
        }

        public Array GetImages(int clinicId, List<int> imagesIds)
        {
            return
                DataContext.ArchiveDocuments.Where(
                    ad =>
                    ad.ClinicId == clinicId && ad.FileTypeId == (int)ArchivesFileType.Image &&
                    imagesIds.Contains(ad.Id)).ToArray();
        }

        public OperationStatus Create(ArchiveDocument document)
        {
            DataContext.ArchiveDocuments.Add(document);
            return base.Save(document);
        }

        public double GetArchiveSizeForClinic(int clinicId)
        {
            double totalSize = 0;
            var files = DataContext.ArchiveDocuments.Where(d => d.ClinicId == clinicId).ToList();

            foreach(var file in files)
            {
                totalSize += file.FileSizeInBytes;
            }

            return totalSize;
        }
    }
}
