﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Web.Security;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.Patients;
using RapidVet.Model.Visits;
using RapidVet.RapidConsts;

namespace RapidVet.Repository.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public class Configuration : DbMigrationsConfiguration<RapidVet.Repository.RapidVetDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;

        }

        protected override void Seed(RapidVet.Repository.RapidVetDataContext context)
        {

            //shortcuts
            var shortcutsCollection = new List<ShortCut>()
            {
                new ShortCut() {Name = Resources.Migration.DeleteShortCut, Link = "#"},
                new ShortCut() {Name = Resources.Migration.FollowUps, Link="/FollowUps"},
                new ShortCut() {Name = Resources.Migration.AttendanceLog, Link="/AttendanceLog/LogReport"},
                new ShortCut() {Name = Resources.Migration.ClinicTasks,Link="/ClinicTasks" },
                new ShortCut() {Name = Resources.Migration.InventoryManagement, Link="/Inventory"},
                new ShortCut() {Name = Resources.Migration.TelephoneDirectory,Link="/TelephoneDirectory" },
                new ShortCut() {Name = Resources.Migration.AttendanceRegistration, Link="/AttendanceLog"}
            };
            if (!context.ShortCuts.Any())
            {
                foreach (var shortCut in shortcutsCollection)
                {
                    context.ShortCuts.Add(shortCut);
                }
                context.SaveChanges();
            }



            //AnimalIcon
            var animalIconCollection = new List<AnimalIcon>()
            {
                new AnimalIcon() {FileName = "dog.png"},
                new AnimalIcon() {FileName = "cat.png"},
                new AnimalIcon() {FileName = "bird.png"},
                new AnimalIcon() {FileName = "horse.png"},
                new AnimalIcon() {FileName = "cow.png"},
                new AnimalIcon() {FileName = "lizard.png"},
                new AnimalIcon() {FileName = "other.png"}
            };

            if (!context.AnimalIcons.Any())
            {
                foreach (var icon in animalIconCollection)
                {
                    context.AnimalIcons.Add(icon);
                }
                context.SaveChanges();
            }



            //clinicGroups
            var clinicGroupA = new ClinicGroup() { Name = Resources.Migration.TestGroup, Active = true, MaxConcurrentUsers = 1, TimeToDisconnectInactiveUsersInMinutes = 120 };

            if (!context.ClinicGroups.Any())
            {
                context.ClinicGroups.Add(clinicGroupA);
                context.SaveChanges();
            }


            //Clinics

            var clinicAGroupA = new Clinic()
            {
                Name = Resources.Migration.Clinic1,
                ClinicGroup = clinicGroupA,
                ClinicGroupID = clinicGroupA.Id,
                Active = true,
                ShowComplaint = true,
                ShowExaminations = true,
                ShowDiagnosis = true,
                ShowTreatments = true,
                ShowMedicins = true,
                ShowTemperature = true,
                ShowWeight = true,
                ShowOldVisits = true,
                ShowVisitNote = true
            };
            var clinicBGroupA = new Clinic()
            {
                Name = Resources.Migration.Clinic2,
                ClinicGroup = clinicGroupA,
                ClinicGroupID = clinicGroupA.Id,
                Active = true,
                ShowComplaint = true,
                ShowExaminations = true,
                ShowDiagnosis = true,
                ShowTreatments = true,
                ShowMedicins = true,
                ShowTemperature = true,
                ShowWeight = true,
                ShowOldVisits = true,
                ShowVisitNote = true
            };

            if (!context.Clinics.Any())
            {
                context.Clinics.Add(clinicAGroupA);
                context.Clinics.Add(clinicBGroupA);
                context.SaveChanges();
            }

            //Roles
            if (!Roles.RoleExists(RolesConsts.ADMINISTRATOR))
            {
                Roles.CreateRole(RolesConsts.ADMINISTRATOR);
            }
            if (!Roles.RoleExists(RolesConsts.CLINICGROUPMANAGER))
            {
                Roles.CreateRole(RolesConsts.CLINICGROUPMANAGER);
            }
            if (!Roles.RoleExists(RolesConsts.CLINICMANAGER))
            {
                Roles.CreateRole(RolesConsts.CLINICMANAGER);
            }
            if (!Roles.RoleExists(RolesConsts.DOCTOR))
            {
                Roles.CreateRole(RolesConsts.DOCTOR);
            }
            if (!Roles.RoleExists(RolesConsts.SECRATERY))
            {
                Roles.CreateRole(RolesConsts.SECRATERY);
            }
            if (!Roles.RoleExists(RolesConsts.VIEW_REPORTS))
            {
                Roles.CreateRole(RolesConsts.VIEW_REPORTS);
            }
            if (!Roles.RoleExists(RolesConsts.VIEW_FINANCIAL_REPORTS))
            {
                Roles.CreateRole(RolesConsts.VIEW_FINANCIAL_REPORTS);
            }
            if (!Roles.RoleExists(RolesConsts.EXCEL_EXPORT))
            {
                Roles.CreateRole(RolesConsts.EXCEL_EXPORT);
            }
            if (!Roles.RoleExists(RolesConsts.INVENTORY_ENTITY_MANAGEMENT))
            {
                Roles.CreateRole(RolesConsts.INVENTORY_ENTITY_MANAGEMENT);
            }

            if (!Roles.RoleExists(RolesConsts.INVENTORY_CREATE_ORDER))
            {
                Roles.CreateRole(RolesConsts.INVENTORY_CREATE_ORDER);
            }

            if (!Roles.RoleExists(RolesConsts.INVENTORY_RECIEVE_ORDER))
            {
                Roles.CreateRole(RolesConsts.INVENTORY_RECIEVE_ORDER);
            }

            if (!Roles.RoleExists(RolesConsts.INVENTORY_UPDATE))
            {
                Roles.CreateRole(RolesConsts.INVENTORY_UPDATE);
            }

            if (!Roles.RoleExists(RolesConsts.INVENTORY_REPORTS))
            {
                Roles.CreateRole(RolesConsts.INVENTORY_REPORTS);
            }
            if (!Roles.RoleExists(RolesConsts.NO_DIARY))
            {
                Roles.CreateRole(RolesConsts.NO_DIARY);
            }
            var titleCollection = new List<Title>()
            {
                new Title() {Name = Resources.Migration.Mr},
                new Title() {Name = Resources.Migration.Ms},
                new Title() {Name = Resources.Migration.Dr},
                new Title() {Name = Resources.Migration.Prof},
                new Title() {Name = Resources.Migration.Lawyer},
                new Title() {Name = Resources.Migration.Cpl}
            };

            if (!context.Titles.Any())
            {
                foreach (var title in titleCollection)
                {
                    context.Titles.Add(title);
                }
                context.SaveChanges();
            }

            //default credit cards
            var DefaultCardCompanies = new List<DefaultCardCompanies>()
            {
                new DefaultCardCompanies() {Name = Resources.Migration.DefaultCardVisa, Localization = Resources.Migration.Localization, Active=true},                
                new DefaultCardCompanies() {Name = Resources.Migration.DefaultCardAmex, Localization = Resources.Migration.Localization, Active=true},
                new DefaultCardCompanies() {Name = Resources.Migration.DefaultCardDiners, Localization= Resources.Migration.Localization, Active=true},
                new DefaultCardCompanies() {Name = Resources.Migration.DefaultCardIsracard, Localization = Resources.Migration.Localization, Active=true},
                new DefaultCardCompanies() {Name = Resources.Migration.DefaultCardLeumi, Localization = Resources.Migration.Localization, Active=true},
                new DefaultCardCompanies() {Name = Resources.Migration.DefaultCardMastercard, Localization = Resources.Migration.Localization, Active=true}
            };


            if (!context.DefaultCardCompanies.Any())
            {
                foreach (var cardCompany in DefaultCardCompanies)
                {
                    context.DefaultCardCompanies.Add(cardCompany);
                }
                context.SaveChanges();
            }

            //Users

            if (Membership.GetUser("admin") == null)
            {
                MembershipCreateStatus Status;
                Membership.CreateUser("admin", "123456", "admin@demo.com", null, null, true, out Status);
                Roles.AddUserToRole("admin", RolesConsts.ADMINISTRATOR);
            }

            if (Membership.GetUser("google") == null)
            {
                MembershipCreateStatus Status;
                Membership.CreateUser("google", ".123456?", "RapidImageLTD@gmail.com", null, null, true, out Status);
                Roles.AddUserToRole("google", RolesConsts.ADMINISTRATOR);
            }

            if (Membership.GetUser("doc1") == null)
            {
                using (var repo = new UserRepository())
                {
                    var user = repo.Create("Doctor", "A", "doc1", "123456", 1, 3);
                    repo.AddToClinic(user.Id, RolesConsts.CLINICMANAGER, 1);
                    repo.AddToClinic(user.Id, RolesConsts.DOCTOR, 1);
                    Roles.AddUserToRole("doc1", RolesConsts.CLINICGROUPMANAGER);

                }
            }
            if (Membership.GetUser("doc2") == null)
            {
                using (var repo = new UserRepository())
                {
                    var user = repo.Create("Doctor", "B", "doc2", "123456", 1, 3);
                    repo.AddToClinic(user.Id, RolesConsts.CLINICMANAGER, 1);
                    repo.AddToClinic(user.Id, RolesConsts.DOCTOR, 1);
                }
                Roles.AddUserToRole("admin", RolesConsts.ADMINISTRATOR);
            }
            if (Membership.GetUser("sec1") == null)
            {
                using (var repo = new UserRepository())
                {
                    var user = repo.Create("Secretery", "S", "sec1", "123456", 1, 2);
                    repo.AddToClinic(user.Id, RolesConsts.SECRATERY, 1);
                    repo.AddToClinic(user.Id, RolesConsts.SECRATERY, 2);
                }
                Roles.AddUserToRole("admin", RolesConsts.ADMINISTRATOR);
            }


            //Finance
            var ItemTypeCollection = new List<PriceListItemType>()
            {
                new PriceListItemType() {Name = Resources.Migration.Other, Code = 1},
                new PriceListItemType() {Name = Resources.Migration.Treatments, Code = 3},
                new PriceListItemType() {Name = Resources.Migration.Examinations, Code = 4},
                new PriceListItemType() {Name = Resources.Migration.Lab, Code = 5},
                new PriceListItemType() {Name = Resources.Migration.Food, Code = 7},
                new PriceListItemType() {Name = Resources.Migration.Vaccinations, Code = 8},
                new PriceListItemType() {Name = Resources.Migration.Preventive, Code = 9}
            };

            if (!context.PriceListItemTypes.Any())
            {
                foreach (var priceListItemType in ItemTypeCollection)
                {
                    context.PriceListItemTypes.Add(priceListItemType);
                }
                context.SaveChanges();
            }




            //phoneTypes
            var phoneTypeCollection = new List<PhoneType>()
            {
                new PhoneType() {Name = Resources.Migration.Mobile},
                new PhoneType() {Name = Resources.Migration.Home},
                new PhoneType() {Name = Resources.Migration.Work},
                new PhoneType() {Name = Resources.Migration.Other},
                new PhoneType() {Name = Resources.Migration.Fax},
            };

            if (!context.PhoneTypes.Any())
            {
                foreach (var phoneType in phoneTypeCollection)
                {
                    context.PhoneTypes.Add(phoneType);
                }
                context.SaveChanges();
            }

            //RegionalCouncils
            if (!context.RegionalCouncils.Any())
            {
                context.RegionalCouncils.Add(new RegionalCouncil() { Name = "ללא", Code = "-1" });
                context.SaveChanges();
            }

            if (!context.AnimalKinds.Any() && !context.AnimalRaces.Any())
            {
                var animalKindCollection = new List<AnimalKind>()
                {
                    new AnimalKind()
                    {
                        Active = true,
                        AnimalIconId = 1,
                        Value = Resources.Migration.Dog,
                        ClinicGroupId = 1,
                    },
                    new AnimalKind()
                    {
                        Active = true,
                        AnimalIconId = 2,
                        Value = Resources.Migration.Cat,
                        ClinicGroupId = 1,
                    }
                };

                foreach (var animalKind in animalKindCollection)
                {
                    context.AnimalKinds.Add(animalKind);
                }

                context.SaveChanges();
            }


            //Tariff
            if (!context.Tariffs.Any())
            {
                var tariff = new Tariff()
                {
                    Active = true,
                    ClinicId = 1,
                    CreatedDate = DateTime.Now,
                    CreatedById = 1,
                    Name = Resources.Migration.Tariff
                };

                context.Tariffs.Add(tariff);
                context.SaveChanges();

                clinicAGroupA.DefualtTariffId = tariff.Id;
                clinicBGroupA.DefualtTariffId = tariff.Id;

                context.SaveChanges();
            }

            //PriceListCategories
            if (!context.PriceListCategories.Any())
            {
                var priceListCategory = new PriceListCategory()
                {
                    Available = true,
                    ClinicId = 1,
                    Name = Resources.Migration.Vaccinations
                };
                context.PriceListCategories.Add(priceListCategory);
                context.SaveChanges();
            }

            if (!context.Diagnoses.Any())
            {
                context.Diagnoses.Add(new Diagnosis()
                {
                    Active = true,
                    ClinicId = 1,
                    Name = Resources.Diagnosis.GeneralDiagnosis
                });

                context.SaveChanges();
            }



            if (!context.BankCodes.Any())
            {
                context.BankCodes.Add(new BankCode() { Code = 4, Name = Resources.BankCodes.Yahav });
                context.BankCodes.Add(new BankCode() { Code = 10, Name = Resources.BankCodes.Leumi });
                context.BankCodes.Add(new BankCode() { Code = 11, Name = Resources.BankCodes.Idb });
                context.BankCodes.Add(new BankCode() { Code = 12, Name = Resources.BankCodes.Poalim });
                context.BankCodes.Add(new BankCode() { Code = 13, Name = Resources.BankCodes.Igood });
                context.BankCodes.Add(new BankCode() { Code = 14, Name = Resources.BankCodes.OtsarHachayal });
                context.BankCodes.Add(new BankCode() { Code = 17, Name = Resources.BankCodes.Mercantil });
                context.BankCodes.Add(new BankCode() { Code = 20, Name = Resources.BankCodes.Mizrahi });
                context.BankCodes.Add(new BankCode() { Code = 26, Name = Resources.BankCodes.Youbank });
                context.BankCodes.Add(new BankCode() { Code = 30, Name = Resources.BankCodes.Mischar });
                context.BankCodes.Add(new BankCode() { Code = 31, Name = Resources.BankCodes.International });
                context.BankCodes.Add(new BankCode() { Code = 34, Name = Resources.BankCodes.Aravim });
                context.BankCodes.Add(new BankCode() { Code = 46, Name = Resources.BankCodes.Masad });
                context.BankCodes.Add(new BankCode() { Code = 52, Name = Resources.BankCodes.Pai });
                context.BankCodes.Add(new BankCode() { Code = 54, Name = Resources.BankCodes.Jerusalem });
                context.BankCodes.Add(new BankCode() { Code = 68, Name = Resources.BankCodes.Dexia });
                context.BankCodes.Add(new BankCode() { Code = 22, Name = Resources.BankCodes.Citybank });
                context.BankCodes.Add(new BankCode() { Code = 23, Name = Resources.BankCodes.Hsbc });
                context.BankCodes.Add(new BankCode() { Code = 27, Name = Resources.BankCodes.Plc });
                context.BankCodes.Add(new BankCode() { Code = 39, Name = Resources.BankCodes.Sbi });
                context.BankCodes.Add(new BankCode() { Code = 50, Name = Resources.BankCodes.Slika });
                context.BankCodes.Add(new BankCode() { Code = 59, Name = Resources.BankCodes.Sba });
                context.BankCodes.Add(new BankCode() { Code = 65, Name = Resources.BankCodes.Hassach });

                context.SaveChanges();
            }

            if (!context.CreditCardCodes.Any())
            {
                var banksList = new List<CreditCardCode>()
                {
                    new CreditCardCode() {Name = Resources.CreditCardCodes.Visa, ClinicId = 1},
                    new CreditCardCode() {Name = Resources.CreditCardCodes.MasterCard, ClinicId = 1},
                    new CreditCardCode() {Name = Resources.CreditCardCodes.Amex, ClinicId = 1}
                };

                foreach (var code in banksList)
                {
                    context.CreditCardCodes.Add(code);
                }

                context.SaveChanges();
            }

            if (!context.EasyCardDealTypes.Any())
            {
                var types = new List<EasyCardDealType>()
                {
                    new EasyCardDealType()
                    {
                        Id = 1,
                        Description = Resources.Credit.CreditPayment
                    },
                    new EasyCardDealType()
                    {
                        Id = 2,
                        Description = Resources.Credit.SuperCredit
                    },
                    new EasyCardDealType()
                    {
                        Id = 3,
                        Description = Resources.Credit.Payments
                    },
                    new EasyCardDealType()
                    {
                        Id = 4,
                        Description = Resources.Credit.PaymentsClub
                    },
                    new EasyCardDealType()
                    {
                        Id = 5,
                        Description = Resources.Credit.RegulatCredit
                    },
                    new EasyCardDealType()
                    {
                        Id = 6,
                        Description = Resources.Credit.ClubCredit
                    },
                    new EasyCardDealType()
                    {
                        Id = 7,
                        Description = Resources.Credit.Plus30
                    },
                    new EasyCardDealType()
                    {
                        Id = 8,
                        Description = Resources.Credit.ChargeImmidiatly
                    }
                };
                foreach (var type in types)
                {
                    context.EasyCardDealTypes.Add(type);
                }
                context.SaveChanges();
            }

            //taxRates
            //if (!context.TaxRates.Any())
            //{
            //    var clinics = context.Clinics.Where(c => !c.TaxRates.Any());

            //    foreach (var clinic in clinics)
            //    {
            //        context.TaxRates.Add(
            //            new TaxRate()
            //            {
            //                ClinicId = clinic.Id,
            //                Rate = 18,
            //                TaxRateChangedDate = DateTime.Now
            //            }
            //            );
            //    }

            //    context.SaveChanges();
            //}
        }
    }
}



