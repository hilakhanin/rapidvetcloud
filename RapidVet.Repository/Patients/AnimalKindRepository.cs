﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Patients;
using System.Data.Entity;

namespace RapidVet.Repository.Patients
{
    public class AnimalKindRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IAnimalKindRepository
    {

        public AnimalKindRepository()
        {

        }

        public AnimalKindRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// clinic group object containing all animal kinds in clinic group
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <returns> IQueryable(AnimalKind)</returns>
        public IQueryable<Model.Patients.AnimalKind> GetList(int clinicGroupId)
        {
            var data = DataContext.AnimalKinds.Include(k => k.ClinicGroup).Include(i => i.AnimalIcon)
                              .Where(k => k.ClinicGroupId == clinicGroupId && k.Active)
                              .OrderBy(k => k.Value);

            foreach (var item in data)
            {
                item.AnimalRaces =
                    DataContext.AnimalRaces.Where(r => r.AnimalKindId == item.Id && r.Active).OrderBy(r => r.Value).ToList();
            }

            return data;
        }

        /// <summary>
        /// create new animal kind entry in db
        /// </summary>
        /// <param name="animalKind">aninmal kind object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Create(AnimalKind animalKind)
        {
            animalKind.Active = true;
            DataContext.AnimalKinds.Add(animalKind);
            OperationStatus status = base.Save(animalKind);
            if (status.Success)
            {
                status.ItemId = animalKind.Id;
            }
            return status;
        }
        //currently not in use
        public AnimalKind GetNewAnimalKind(int clinicGroupId)
        {
            return new AnimalKind()
                       {
                           ClinicGroupId = clinicGroupId,
                           ClinicGroup = DataContext.ClinicGroups.Single(c => c.Id == clinicGroupId)
                       };
        }

        /// <summary>
        /// returns a specific animalKind object
        /// </summary>
        /// <param name="animalKindId">animal kind id</param>
        /// <returns>AnimalKind</returns>
        public AnimalKind GetItem(int animalKindId)
        {
            return
                DataContext.AnimalKinds.Include(k => k.ClinicGroup)
                                       .Include(k => k.AnimalRaces)
                                       .Single(k => k.Id == animalKindId);
        }

        /// <summary>
        /// returns a specific animalKind object
        /// </summary>
        /// <param name="animalKindId">animal kind id</param>
        /// <returns>AnimalKind</returns>
        public String GetKindById(int animalKindId)
        {
            return
                DataContext.AnimalKinds.Single(k => k.Id == animalKindId).Value;
        }


        /// <summary>
        /// updates an animal kind in db
        /// </summary>
        /// <param name="animalKind">animalKind object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(AnimalKind animalKind)
        {
            return base.Save(animalKind);
        }

        /// <summary>
        /// updates an animalKind in DB to be not active
        /// </summary>
        /// <param name="animalKindId">animal kind id</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Delete(int animalKindId)
        {
            AnimalKind source = DataContext.AnimalKinds.Single(k => k.Id == animalKindId);
          //  DataContext.AnimalKinds.Remove(source);
            source.Active = false;
            OperationStatus status = base.Save(source);
            if (status.Success)
            {
                status.ItemId = source.ClinicGroupId;
            }
            return status;

        }


        /// <summary>
        /// returns a list of all animal icons objects
        /// </summary>
        /// <returns>List(AnimalIcon)</returns>
        public List<AnimalIcon> GetIcons()
        {
            return DataContext.AnimalIcons.ToList();
        }

        public AnimalKind GetClinicGroupDogKind(int clinicGroupId)
        {
            return DataContext.AnimalKinds.FirstOrDefault(k => k.ClinicGroupId==clinicGroupId && k.Value ==RapidConsts.RepositoryConsts.ANIMAL_KIND_DOG_NAME);
        }

        public AnimalKind GetClinicGroupCatKind(int clinicGroupId)
        {
            return DataContext.AnimalKinds.FirstOrDefault(k => k.ClinicGroupId == clinicGroupId && k.Value == RapidConsts.RepositoryConsts.ANIMAL_KIND_CAT_NAME);
        }

        public AnimalKind GetItemByNameWithRaces(int activeClinicGroupId, string name)
        {
            return DataContext.AnimalKinds.Include(ak => ak.AnimalRaces).FirstOrDefault(ak => ak.ClinicGroupId == activeClinicGroupId && ak.Value == name);
        }
    }
}
