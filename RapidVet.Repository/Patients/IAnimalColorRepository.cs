﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.Patients
{
    public interface IAnimalColorRepository : IDisposable
    {
        /// <summary>
        /// returns clinic group object containing alll animal colors in clinic group
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <returns>ClinicGroup</returns>
        ClinicGroup GetList(int clinicGroupId);

        /// <summary>
        /// return initiated AnimalColor object
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <returns>AnimalColor</returns>
        AnimalColor GetCreateModel(int clinicGroupId);

        /// <summary>
        /// create a new animal color in clinic group
        /// </summary>
        /// <param name="animalColor">animal color object</param>
        /// <returns>operationStatus</returns>
        OperationStatus Create(AnimalColor animalColor);

        /// <summary>
        /// return a specific animalColor object
        /// </summary>
        /// <param name="animalColorId">animal color id</param>
        /// <returns>AnimalColor</returns>
        AnimalColor GetItem(int animalColorId);

        /// <summary>
        /// updates an animal color in db
        /// </summary>
        /// <param name="animalColor">animal color object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(AnimalColor animalColor);

        /// <summary>
        /// set animal color as not active in db
        /// </summary>
        /// <param name="animalColorId">animal color id</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(int animalColorId);

        AnimalColor GetItemByName(int activeClinicGroupId, string name);
    }
}
