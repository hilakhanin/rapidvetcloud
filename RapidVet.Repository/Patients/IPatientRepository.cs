﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.Patients
{
    public interface IPatientRepository : IDisposable
    {

        /// <summary>
        /// creates a patient object in db
        /// </summary>
        /// <param name="patient">patient object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Patient patient);

        /// <summary>
        /// returns a specific patient object by id
        /// </summary>
        /// <param name="id">patient object id in db</param>
        /// <returns>Patient</returns>
        Patient GetPatient(int id);

        /// <summary>
        /// updates a patient object in db
        /// </summary>
        /// <param name="patient">Patient object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(Patient patient);


        //currently not in use
        PatientComment GetComment(int commentId);

        /// <summary>
        /// returns all child patient objects of a client object by client id
        /// </summary>
        /// <param name="id">client id in db</param>
        /// <returns>IQueryable(Patient)</returns>
        IQueryable<Patient> GetClientPatients(int id);

        /// <summary>
        /// list of all active patients for client
        /// </summary>
        /// <param name="clientId">client id</param>
        /// <returns> Listof Patient objects</returns>
        List<Patient> GetClientActivePatientsSimple(int clientId);

        /// <summary>
        /// returns patient's client id
        /// </summary>
        /// <param name="id">int patient id</param>
        /// <returns>client id</returns>
        int GetClientId(int id);


        /// <summary>
        /// delete patient comment
        /// </summary>
        /// <param name="comment">PatientComment obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus DeleteComment(PatientComment comment);

        /// <summary>
        /// adds patient comment
        /// </summary>
        /// <param name="comment">PatientComment obj</param>
        void AddComment(PatientComment comment);

        /// <summary>
        /// checks if patient is a dog
        /// </summary>
        /// <param name="patientId"> patient id</param>
        /// <returns> bool</returns>
        bool IsPatientDog(int patientId);

        /// <summary>
        /// gets clinic id for patient
        /// </summary>
        /// <param name="patientId"> patient id</param>
        /// <returns> in clinic id</returns>
        int GetPatientClinicId(int patientId);

        /// <summary>
        /// checks if patient belongs to clinic
        /// </summary>
        /// <param name="patientId"> patient id</param>
        /// <param name="clinicId"> clinic id</param>
        /// <returns>bool</returns>
        bool IsPatientInClinic(int patientId, int clinicId);

        /// <summary>
        /// get patient for letter creation
        /// </summary>
        /// <param name="patientId"> patient id</param>
        /// <returns>Patient obj</returns>
        Patient GetPatientForLetter(int patientId);

        /// <summary>
        /// get patient for history view
        /// </summary>
        /// <param name="patientId"> patient id</param>
        /// <returns>Patient obj</returns>
        Patient GetPatientForHistory(int patientId);

        /// <summary>
        /// get patient name
        /// </summary>
        /// <param name="patientId"> patient id</param>
        /// <returns>string</returns>
        string GetName(int patientId);

        /// <summary>
        /// get patient last weight
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns>decimal?</returns>
        decimal? GetLastWeight(int patientId);

        /// <summary>
        /// return client identifying string in search: {client name}- {animal1}, {animal2} {...}
        /// </summary>
        /// <param name="clientId">int client id</param>
        /// <returns>string</returns>
        string GetClientPatientsNames(int clientId);

        /// <summary>
        /// get patient id from clinic id and old patient id
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="oldPatientId">int old patient id</param>
        /// <returns>int?</returns>
        int? GetPatientIdFromOldId(int clinicId, int oldPatientId);

        Patient GetPatientByElectronicNumber(string electronicNum);
    }
}
