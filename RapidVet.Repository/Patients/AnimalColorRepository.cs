﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using System.Data.Entity;

namespace RapidVet.Repository.Patients
{
    public class AnimalColorRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IAnimalColorRepository
    {

        public AnimalColorRepository()
        {

        }

        public AnimalColorRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// returns clinic group object containing alll animal colors in clinic group
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <returns>ClinicGroup</returns>
        public ClinicGroup GetList(int clinicGroupId)
        {
            ClinicGroup clinicGroup =
                DataContext.ClinicGroups.Include(c => c.AnimalKinds).Include(c => c.AnimalColors).Single(
                    c => c.Id == clinicGroupId);
            if (clinicGroup.AnimalColors.Any())
            {
               clinicGroup.AnimalColors = clinicGroup.AnimalColors.Where(c=>c.Active).OrderBy(a => a.Value).ToList();
            }
            return clinicGroup;
        }

        /// <summary>
        /// return initiated AnimalColor object
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <returns>AnimalColor</returns>
        public AnimalColor GetCreateModel(int clinicGroupId)
        {
            return new AnimalColor()
                       {
                           ClinicGroupId = clinicGroupId,
                           ClinicGroup = DataContext.ClinicGroups.Single(g => g.Id == clinicGroupId)
                       };
        }


        /// <summary>
        /// create a new animal color in clinic group
        /// </summary>
        /// <param name="animalColor">animal color object</param>
        /// <returns>operationStatus</returns>
        public OperationStatus Create(AnimalColor animalColor)
        {
            DataContext.AnimalColors.Add(animalColor);
            OperationStatus status = base.Save(animalColor);
            if (status.Success)
            {
                status.ItemId = animalColor.Id;
            }
            return status;

        }

        /// <summary>
        /// return a specific animalColor object
        /// </summary>
        /// <param name="animalColorId">animal color id</param>
        /// <returns>AnimalColor</returns>
        public AnimalColor GetItem(int animalColorId)
        {
            return
                DataContext.AnimalColors.Include(c => c.ClinicGroup)
                                        .Include(c => c.Patients)
                                        .Single(c => c.Id == animalColorId);
        }

        /// <summary>
        /// updates an animal color in db
        /// </summary>
        /// <param name="animalColor">animal color object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(AnimalColor animalColor)
        {
            return base.Save(animalColor);
        }

        /// <summary>
        /// set animal color as not active in db
        /// </summary>
        /// <param name="animalColorId">animal color id</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Delete(int animalColorId)
        {
            var source = DataContext.AnimalColors.Single(c => c.Id == animalColorId);
            DataContext.AnimalColors.Remove(source);
            return base.Save(source);
        }

        public AnimalColor GetItemByName(int activeClinicGroupId, string name)
        {
            return DataContext.AnimalColors.FirstOrDefault(ac => ac.ClinicGroupId == activeClinicGroupId && ac.Value == name);
        }
    }
}
