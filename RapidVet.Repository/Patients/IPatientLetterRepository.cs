﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.Patients
{
    public interface IPatientLetterRepository : IDisposable
    {
        /// <summary>
        /// creates patient letter
        /// </summary>
        /// <param name="letter">Letter object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Letter letter);

        /// <summary>
        /// get patient letter
        /// </summary>
        /// <param name="letterId"> letter id</param>
        /// <returns>Letter</returns>
        Letter GetLetter(int letterId);

        /// <summary>
        /// delete letter
        /// </summary>
        /// <param name="letter">Letter object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(Letter letter);

        /// <summary>
        /// creates MOA report
        /// </summary>
        /// <param name="report">MinistryOfAgricultureReport</param>
        /// <returns>OperationStatus</returns>
        OperationStatus CreateMinistryOfAgricultureReport(MinistryOfAgricultureReport report);

        /// <summary>
        /// get patient letters for patient history
        /// </summary>
        /// <param name="id">patient id</param>
        /// <returns>IQueryable<Letter></returns>
        IQueryable<Letter> GetLettersForPatientHistory(int id);
    }
}
