﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.Patients
{
    public class PatientLetterRepository : RepositoryBase<RapidVetDataContext>, IPatientLetterRepository
    {
        public PatientLetterRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public PatientLetterRepository()
        {

        }

        public OperationStatus Create(Letter letter)
        {
            DataContext.Letters.Add(letter);
            return base.Save(letter);
        }

        public Letter GetLetter(int letterId)
        {
            return DataContext.Letters.Single(l => l.Id == letterId);
        }

        public OperationStatus Delete(Letter letter)
        {
            DataContext.Letters.Remove(letter);
            return base.Save(letter);
        }

        public OperationStatus CreateMinistryOfAgricultureReport(MinistryOfAgricultureReport report)
        {
            DataContext.MinistryOfAgricultureReports.Add(report);
            return base.Save(report);
        }

        public IQueryable<Letter> GetLettersForPatientHistory(int id)
        {
            return DataContext.Letters.Where(l => l.PatientId == id && l.ShowInPatientHistory);
        }
    }
}
