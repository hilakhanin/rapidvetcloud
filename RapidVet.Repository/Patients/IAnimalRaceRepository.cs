﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.Patients
{
    public interface IAnimalRaceRepository : IDisposable
    {
        /// <summary>
        /// returns AnimalKind object containing all child animal race objects
        /// </summary>
        /// <param name="animalKindId">animal kind id</param>
        /// <returns>AnimalKind</returns>
        AnimalKind GetList(int animalKindId);

        //currently not in use
        AnimalRace GetCreateModel(int animalKindId);

        /// <summary>
        /// create a new animal race in db
        /// </summary>
        /// <param name="animalRace">Animal Race object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(AnimalRace animalRace);

        /// <summary>
        /// return a specific animalRace object
        /// </summary>
        /// <param name="animalRaceId">animal race id</param>
        /// <returns>AnimalRace</returns>
        AnimalRace GetItem(int animalRaceId);

        /// <summary>
        /// update animalRace object in db
        /// </summary>
        /// <param name="animalRace">animalRace object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(AnimalRace animalRace);

        //currently not in use
        OperationStatus Delete(AnimalRace animalRace);

        /// <summary>
        /// returns list of all child animal race objects by animal kind id(parent)
        /// </summary>
        /// <param name="animalKindId">animal kind id</param>
        /// <returns> List(AnimalRace)</returns>
        List<AnimalRace> GetListByAnimalKind(int animalKindId);

        /// <summary>
        /// returns list of all child animal race objects by a sibling animal race object id
        /// </summary>
        /// <param name="raceId">animal race id</param>
        /// <returns> List(AnimalRace)</returns>
        List<AnimalRace> GetListByRaceId(int raceId);


        /// <summary>
        /// updates an animal race object in db
        /// </summary>
        /// <param name="animalRace">animalRace object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Save(AnimalRace animalRace);

        int GetEmptyRaceID(int AnimalKindId);
       
    }
}
