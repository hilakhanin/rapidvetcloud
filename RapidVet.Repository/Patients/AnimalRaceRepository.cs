﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Model;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.Patients
{
    public class AnimalRaceRepository : RepositoryBase<RapidVet.Repository.RapidVetDataContext>, IAnimalRaceRepository
    {

        public AnimalRaceRepository()
        {

        }

        public AnimalRaceRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// returns AnimalKind object containing all child animal race objects
        /// </summary>
        /// <param name="animalKindId">animal kind id</param>
        /// <returns>AnimalKind</returns>
        public AnimalKind GetList(int animalKindId)
        {
            var animalKind = DataContext.AnimalKinds.Include(k => k.ClinicGroup)
                                                     .Include(k => k.AnimalRaces)
                                                     .Single(k => k.Id == animalKindId);

            animalKind.AnimalRaces.Where(r => r.Active).OrderBy(r => r.Value);
            return animalKind;
        }

        //currently not in use
        public AnimalRace GetCreateModel(int animalKindId)
        {
            var animalKind = DataContext.AnimalKinds.Include(k => k.ClinicGroup).Single(k => k.Id == animalKindId);
            return new AnimalRace()
            {
                AnimalKind = animalKind,
                AnimalKindId = animalKindId,
            };
        }

        /// <summary>
        /// create a new animal race in db
        /// </summary>
        /// <param name="animalRace">Animal Race object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Create(AnimalRace animalRace)
        {
            DataContext.AnimalRaces.Add(animalRace);
            OperationStatus status = base.Save(animalRace);
            if (status.Success)
            {
                status.ItemId = animalRace.Id;
            }

            return status;
        }

        /// <summary>
        /// return a specific animalRace object
        /// </summary>
        /// <param name="animalRaceId">animal race id</param>
        /// <returns>AnimalRace</returns>
        public AnimalRace GetItem(int animalRaceId)
        {
            return DataContext.AnimalRaces
                .Include(r => r.AnimalKind)
                .Single(r => r.Id == animalRaceId);
        }

        /// <summary>
        /// update animalRace object in db
        /// </summary>
        /// <param name="animalRace">animalRace object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(AnimalRace animalRace)
        {
            return base.Save(animalRace);
        }

        //currently not in use
        public OperationStatus Delete(AnimalRace animalRace)
        {
            var source = DataContext.AnimalRaces.Single(r => r.Id == animalRace.Id);
            DataContext.AnimalRaces.Remove(source);
            OperationStatus status = base.Save(source);

            if (status.Success)
            {
                status.ItemId = source.AnimalKindId;
            }
            return status;
        }

        /// <summary>
        /// returns list of all child animal race objects by animal kind id(parent)
        /// </summary>
        /// <param name="animalKindId">animal kind id</param>
        /// <returns> List(AnimalRace)</returns>
        public List<AnimalRace> GetListByAnimalKind(int animalKindId)
        {
            return DataContext.AnimalRaces.Where(r => r.AnimalKindId == animalKindId).OrderBy(r => r.Value).ToList();
        }

        /// <summary>
        /// returns list of all child animal race objects by a sibling animal race object id
        /// </summary>
        /// <param name="raceId">animal race id</param>
        /// <returns> List(AnimalRace)</returns>
        public List<AnimalRace> GetListByRaceId(int raceId)
        {
            var race = DataContext.AnimalRaces.Single(r => r.Id == raceId);
            var animalKind = DataContext.AnimalKinds.Include(k => k.AnimalRaces).Single(k => k.Id == race.AnimalKindId);
            return animalKind.AnimalRaces.OrderBy(r => r.Value).ToList();
        }


        /// <summary>
        /// updates an animal race object in db
        /// </summary>
        /// <param name="animalRace">animalRace object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus IAnimalRaceRepository.Save(AnimalRace animalRace)
        {
            return Save(animalRace);
        }
        public int GetEmptyRaceID(int AnimalKindId)
        {
            return DataContext.AnimalRaces.FirstOrDefault(i => i.AnimalKindId == AnimalKindId && i.Value == " ").Id;
        }
    }
}
