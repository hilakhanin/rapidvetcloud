﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.Patients
{
    public interface IAnimalKindRepository : IDisposable
    {
        /// <summary>
        /// clinic group object containing all animal kinds in clinic group
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <returns> IQueryable(AnimalKind)</returns>
        IQueryable<AnimalKind> GetList(int clinicGroupId);

        /// <summary>
        /// create new animal kind entry in db
        /// </summary>
        /// <param name="animalKind">aninmal kind object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(AnimalKind animalKind);

        //currently not in use
        AnimalKind GetNewAnimalKind(int clinicGroupId);

        /// <summary>
        /// returns a specific animalKind object
        /// </summary>
        /// <param name="animalKindId">animal kind id</param>
        /// <returns>AnimalKind</returns>
        AnimalKind GetItem(int animalKindId);
        String GetKindById(int animalKindId);
        /// <summary>
        /// updates an animal kind in db
        /// </summary>
        /// <param name="animalKind">animalKind object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(AnimalKind animalKind);

        /// <summary>
        /// updates an animalKind in DB to be not active
        /// </summary>
        /// <param name="animalKindId">animal kind id</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(int animalKindId);

        /// <summary>
        /// returns a list of all animal icons objects
        /// </summary>
        /// <returns>List(AnimalIcon)</returns>
        List<AnimalIcon> GetIcons();

        AnimalKind GetClinicGroupDogKind(int clinicGroupId);
        AnimalKind GetClinicGroupCatKind(int clinicGroupId);
        AnimalKind GetItemByNameWithRaces(int activeClinicGroupId, string name);
    }
}
