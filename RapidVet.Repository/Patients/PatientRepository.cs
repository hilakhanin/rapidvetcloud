﻿using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Model;
using RapidVet.Model.Patients;
using RapidVet.RapidConsts;
using RapidVet.Model.Visits;

namespace RapidVet.Repository.Patients
{
    public class PatientRepository : RepositoryBase<RapidVetDataContext>, IPatientRepository
    {
        public PatientRepository()
        {

        }

        public PatientRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// creates a patient object in db
        /// </summary>
        /// <param name="patient">patient object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Create(Patient patient)
        {
            DataContext.Patients.Add(patient);
            var status = base.Save(patient);
            if (status.Success)
            {
                status.ItemId = patient.Id;
            }
            return status;
        }

        /// <summary>
        /// returns a specific patient object by id
        /// </summary>
        /// <param name="id">patient object id in db</param>
        /// <returns>Patient</returns>
        public Patient GetPatient(int id)
        {
            var patient =
                DataContext.Patients
                           .Include(p => p.FoodType)
                           .Include(p => p.AnimalColor)
                           .Include(p => p.AnimalRace)
                           .Include("AnimalRace.AnimalKind")
                           .Include(p => p.Client).Include("Client.Phones")
                           .Include("AnimalRace.AnimalKind.AnimalIcon")
                           .Single(p => p.Id == id );

            patient.Comments =
                DataContext.PatientComments.Include(pc => pc.CreatedBy).Where(pc => pc.PatientId == patient.Id && pc.Active)
                           .OrderBy(pc => pc.CreatedDate)
                           .ToList();

            return patient;
        }

        /// <summary>
        /// updates a patient object in db
        /// </summary>
        /// <param name="patient">Patient object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(Patient patient)
        {
            return base.Save(patient);
        }

        //currently not in use
        public PatientComment GetComment(int commentId)
        {
            return DataContext.PatientComments.Include("Patient.Client").Single(p => p.Id == commentId);
        }

        /// <summary>
        /// returns all child patient objects of a client object by client id
        /// </summary>
        /// <param name="id">client id in db</param>
        /// <returns>IQueryable(Patient)</returns>
        public IQueryable<Patient> GetClientPatients(int id)
        {
            return DataContext.Patients.Include("AnimalRace")
                              .Include("AnimalRace.AnimalKind")
                              .Include("AnimalRace.AnimalKind.AnimalIcon")
                              .Include("AnimalColor")
                              .Include(p => p.Client)
                              .Where(p => p.ClientId == id)
                              .OrderBy(p => p.Name);
        }

        public List<Patient> GetClientActivePatientsSimple(int clientId)
        {
            return DataContext.Patients.Where(p => p.Active && p.ClientId == clientId).OrderBy(p => p.Name).ToList();
        }

        public int GetClientId(int id)
        {
            return DataContext.Patients.Single(p => p.Id == id).ClientId;
        }

        public OperationStatus DeleteComment(PatientComment comment)
        {
            comment.Active = false;
            return Save(comment);
        }

        public void AddComment(PatientComment comment)
        {
            DataContext.PatientComments.Add(comment);
        }

        public bool IsPatientDog(int patientId)
        {
            var patient =
                DataContext.Patients.Include(p => p.AnimalRace)
                           .Include("AnimalRace.AnimalKind")
                           .Single(p => p.Id == patientId);

            return patient.AnimalRace.AnimalKind.Value == RepositoryConsts.ANIMAL_KIND_DOG_NAME;
        }

        public int GetPatientClinicId(int patientId)
        {
            return DataContext.Patients.Include(p => p.Client).Single(p => p.Id == patientId).Client.ClinicId;
        }

        public bool IsPatientInClinic(int patientId, int clinicId)
        {
            return DataContext.Patients.Include(p => p.Client).Single(p => p.Id == patientId).Client.ClinicId ==
                   clinicId;
        }

        public Patient GetPatientForLetter(int patientId)
        {
            return
                DataContext.Patients
                           .Include(p => p.Client)
                           .Include("Client.Addresses")
                           .Include("Client.Addresses.City")
                           .Include("Client.Addresses.City.RegionalCouncil")
                           .Include("Client.Phones")
                            .Include("Client.Phones.PhoneType")
                           .Include("Client.Emails")
                           .Include("Client.Vet")
                           .Include("Client.Quotations")
                           .Include("Client.Clinic")
                           .Include(p => p.AnimalRace)
                           .Include("AnimalRace.AnimalKind")
                           .Include(p => p.AnimalColor)
                           .Include(p => p.LabTests)
                           .Include(p => p.PreventiveMedicine)
                           .Include("PreventiveMedicine.PriceListItem")
                           .Include("PreventiveMedicine.PriceListItem.PriceListItemType")
                           .Include("PreventiveMedicine.PreformingUser")
                           .Include(p => p.MedicalProcedures)
                           .Include(p => p.DischargePapers)
                           .Single(p => p.Id == patientId);
        }

        public Patient GetPatientForHistory(int patientId)
        {
            var patient =
                DataContext.Patients
                           .Include(p => p.AnimalColor)
                           .Include(p => p.LabTests)
                           .Include("Visits.Doctor")
                           .Include("Visits.Examinations.PriceListItem")
                           .Include("Visits.Diagnoses.Diagnosis")
                           .Include("Visits.Treatments.PriceListItem")
                           .Include("Visits.Medications.Medication")
                           .Include("Visits.PreventiveMedicineItems")
                            .Include("Visits.PreventiveMedicineItems.PriceListItem")
                           .Include(p => p.MinistryOfAgricultureReports)                           
                           .Include(p => p.PreventiveMedicine)
                           .Include("LabTests.LabTestTemplate")
                           .Include("LabTests.Results.LabTest")
                           .Include(p => p.MedicalProcedures)
                           .Include(p => p.DischargePapers)
                           .Include(p => p.FollowUps)
                           .Include(p => p.Client)
                           .Include("FollowUps.User")
                           .Single(p => p.Id == patientId);

            patient.Letters = DataContext.Letters.Where(l => l.PatientId == patientId && l.ShowInPatientHistory).ToList();
            patient.Archives = DataContext.ArchiveDocuments.Where(a => a.PatientId == patientId && a.ShowInPatientHistory).ToList();
            patient.Client.CalenderEntries = DataContext.CalenderEntries.Where(c => c.ClientId == patient.ClientId && c.PatientId == patient.Id && c.IsNoShow == true).ToList();

            return patient;
        }

        public string GetName(int patientId)
        {
            return DataContext.Patients.Single(p => p.Id == patientId).Name;
        }

        public decimal? GetLastWeight(int patientId)
        {
            decimal? result = null;
            var vitalSigns =
                DataContext.VitalSigns.Where(v => v.PatientId == patientId && v.VitalSignTypeId == (int)VitalSignType.Weight && v.Value > 0).OrderByDescending(v => v.Time).Take(1);
            if (vitalSigns.Any())
            {
                result = vitalSigns.First().Value;
            }
            return result;
        }

        public string GetClientPatientsNames(int clientId)
        {
            var patients =
                DataContext.Patients.Where(p => p.ClientId == clientId && p.Active).Take(3).OrderBy(p => p.Name).Select(p => p.Name);
            var clientName = DataContext.Clients.Single(c => c.Id == clientId).Name;
            var result = clientName + " - ";
            var counter = 0;
            foreach (var p in patients)
            {
                if (!string.IsNullOrWhiteSpace(p))
                {
                    if (counter == 2)
                    {
                        result += "...";
                    }
                    else
                    {
                        result += p + " ,";
                        counter++;
                    }
                }
            }
            return result.TrimEnd(',');

        }

        public int? GetPatientIdFromOldId(int clinicId, int oldPatientId)
        {
            int? result = null;
            var patient =
                DataContext.Patients.SingleOrDefault(p => p.Client.ClinicId == clinicId && p.OldId == oldPatientId);
            if (patient != null)
            {
                result = patient.Id;
            }

            return result;
        }

        public Patient GetPatientByElectronicNumber(string electronicNum)
        {
            return DataContext.Patients.FirstOrDefault(p => p.ElectronicNumber == electronicNum);
        }
    }
}
