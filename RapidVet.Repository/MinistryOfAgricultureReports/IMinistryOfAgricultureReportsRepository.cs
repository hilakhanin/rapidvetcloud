﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.MinistryOfAgricultureReports
{
    public interface IMinistryOfAgricultureReportsRepository : IDisposable
    {
        /// <summary>
        /// get all MOS reports for patient
        /// </summary>
        /// <param name="id">patient id</param>
        /// <returns> IQueryable<MinistryOfAgricultureReport></returns>
        IQueryable<MinistryOfAgricultureReport> GetMinistryOfAgricultureReportsForPatient(int id);

        List<MinistryOfAgricultureReport> GetReportsForClinic(int clinicId, DateTime fromDate, DateTime toDate, int typeId);

        List<MinistryOfAgricultureReport> GetReports(List<int> ids );

        void DeleteRabiesReportByVisitId(int visitId);
    }
}
