﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using RapidVet.Model.Patients;
using RapidVet.Repository.MinistryOfAgricultureReports;

namespace RapidVet.Repository.MinistryOfAgricultureReports
{
    public class MinistryOfAgricultureReportsRepository : RepositoryBase<RapidVetDataContext>, IMinistryOfAgricultureReportsRepository
    {
        public MinistryOfAgricultureReportsRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public MinistryOfAgricultureReportsRepository()
        {

        }

        public IQueryable<MinistryOfAgricultureReport> GetMinistryOfAgricultureReportsForPatient(int id)
        {
            return DataContext.MinistryOfAgricultureReports.Where(m => m.PatientId == id);
        }

        public List<MinistryOfAgricultureReport> GetReportsForClinic(int clinicId, DateTime fromDate, DateTime toDate, int typeId)
        {
            var res =
                DataContext.MinistryOfAgricultureReports.Include(r => r.Patient).Include(r => r.Patient.Client).Where(
                    r => r.Patient.Client.ClinicId == clinicId && fromDate <= r.DateTime && r.DateTime <= toDate && r.ReportTypeId==typeId).OrderByDescending(r=>r.DateTime);
            return  res.ToList();
        }

        public List<MinistryOfAgricultureReport> GetReports(List<int> ids)
        {
            var res = DataContext.MinistryOfAgricultureReports.Include(r => r.Patient).Where(r => ids.Contains(r.Id));
            return res.ToList();
        }

        public void DeleteRabiesReportByVisitId(int visitId)
        {
            var report = DataContext.MinistryOfAgricultureReports.SingleOrDefault(r => r.VisitId.HasValue && r.VisitId == visitId);

            if (report != null)
            {
                DataContext.MinistryOfAgricultureReports.Remove(report);
            }
        }
    }
}
