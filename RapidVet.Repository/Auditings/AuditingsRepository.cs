﻿using RapidVet.Model.Auditings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Repository.Auditings
{
    public class AuditingsRepository : RepositoryBase<RapidVetDataContext>, IAuditingsRepository
    {
        public AuditingsRepository(RapidVetDataContext DataContext)
        {
        }

        public void Audit(Model.Users.User user, string logType, string description, string oldContent)
        {
            if (user == null || String.IsNullOrEmpty(logType))
                return;

            Audit(user.ActiveClinicId, user.Id, logType, description, oldContent);
        }

        public void Audit(int activeClinicId, int userId, string logType, string description, string oldContent)
        {
            try
            {
                var f = DataContext.AuditingTypes.FirstOrDefault(x => x.TypeName == logType);
                if (f == null)
                {
                    f = new AuditingType() { TypeName = logType };
                    DataContext.AuditingTypes.Add(f);
                    DataContext.SaveChanges();
                }

                DataContext.Auditings.Add(new Model.Auditings.Auditing()
                {
                    ClinicId = activeClinicId,
                    UserId = userId,
                    DateStamp = DateTime.Now,
                    LogDescription = description,
                    OldContent = oldContent,
                    LogTypeID = f.RecordID,
                    LogType = f.TypeName
                });

                DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Auditing> GetAuditings(int clinicID, DateTime fromDate, DateTime toDate, int userId, int logType, int clinicGroupID)
        {
            toDate = toDate.AddDays(1);
            var q = from a in DataContext.Auditings
                    join u in DataContext.Users on a.UserId equals u.Id
                    join c in DataContext.Clinics on a.ClinicId equals c.Id
                    join cg in DataContext.ClinicGroups on c.ClinicGroupID equals cg.Id
                    where (clinicID == -1 || clinicID == a.ClinicId) && a.DateStamp >= fromDate && a.DateStamp <= toDate
                    && (userId == -1 || a.UserId == userId)
                    && (logType == -1 || logType == a.LogTypeID)
                    select new
                    {
                        ClinicId = a.ClinicId,
                        DateStamp = a.DateStamp,
                        LogDescription = a.LogDescription,
                        OldContent = a.OldContent,
                        LogTypeID = a.LogTypeID,
                        LogType = a.LogType,
                        RecordID = a.RecordID,
                        UserName = u.Username,
                        ClinicGroupName = cg.Name,
                        ClinicName = c.Name
                    };

            var q2 = clinicGroupID == -1 ? null : (from cg in DataContext.Clinics
                                                   where cg.ClinicGroupID == clinicGroupID
                                                   select cg.Id).ToList();

            var qEnumerable = q.AsEnumerable();
            if (q2 != null && q2.Count > 0)
                qEnumerable = qEnumerable.Where(x => q2.Contains(x.ClinicId));

            return qEnumerable.Select(x => new Auditing()
            {
                ClinicId = x.ClinicId,
                DateStamp = x.DateStamp,
                LogDescription = x.LogDescription,
                LogTypeID = x.LogTypeID,
                LogType = x.LogType,
                RecordID = x.RecordID,
                UserName = x.UserName,
                ClinicGroupName = x.ClinicGroupName,
                ClinicName = x.ClinicName,
                OldContent = x.OldContent
            }).ToList();
        }

        public List<AuditingType> GetAuditingsTypes()
        {
            return DataContext.AuditingTypes.ToList();
        }
    }
}
