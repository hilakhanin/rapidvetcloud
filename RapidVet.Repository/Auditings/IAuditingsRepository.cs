﻿using RapidVet.Model.Auditings;
using RapidVet.Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Repository.Auditings
{
    public interface IAuditingsRepository : IDisposable
    {
        void Audit(RapidVet.Model.Users.User user, string logType, string description, string oldContent);

        void Audit(int activeClinicId, int userId, string logType, string description, string oldContent);

        List<Auditing> GetAuditings(int clinicID, DateTime fromDate, DateTime toDate, int userId, int logType, int clinicGroupint);

        List<AuditingType> GetAuditingsTypes();
    }
}
