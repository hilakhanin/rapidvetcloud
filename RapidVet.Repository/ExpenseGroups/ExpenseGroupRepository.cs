﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Expenses;

namespace RapidVet.Repository.ExpenseGroups
{
    public class ExpenseGroupRepository : RepositoryBase<RapidVetDataContext>, IExpenseGroupRepository
    {

        public ExpenseGroupRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public ExpenseGroupRepository()
        {

        }

        public IQueryable<ExpenseGroup> GetClinicExpenseGroups(int clinicId)
        {
            return DataContext.ExpenseGroups.Where(e => e.ClinicId == clinicId).OrderByDescending(e => e.Name);
        }

        public ExpenseGroup GetGroup(int expenseGroupId)
        {
            return DataContext.ExpenseGroups.Single(e => e.Id == expenseGroupId);
        }

        public OperationStatus Create(ExpenseGroup expenseGroup)
        {
            DataContext.ExpenseGroups.Add(expenseGroup);
            return base.Save(expenseGroup);
        }

        public OperationStatus Delete(ExpenseGroup expenseGroup)
        {
            DataContext.ExpenseGroups.Remove(expenseGroup);
            return base.Save(expenseGroup);
        }
    }
}
