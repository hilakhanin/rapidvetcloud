﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Expenses;

namespace RapidVet.Repository.ExpenseGroups
{
    public interface IExpenseGroupRepository : IDisposable
    {
        /// <summary>
        /// get all expense groups in clinic
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>IQueryable<ExpenseGroup></returns>
        IQueryable<ExpenseGroup> GetClinicExpenseGroups(int clinicId);

        /// <summary>
        /// get specific expense group
        /// </summary>
        /// <param name="expenseGroupId">int expense group id</param>
        /// <returns>ExpenseGroup</returns>
        ExpenseGroup GetGroup(int expenseGroupId);

        /// <summary>
        /// create new ExpenseGroup obj
        /// </summary>
        /// <param name="expenseGroup">ExpenseGroup obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(ExpenseGroup expenseGroup);

        /// <summary>
        /// delete expense group
        /// </summary>
        /// <param name="expenseGroup">ExpenseGroup obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(ExpenseGroup expenseGroup);
    }
}
