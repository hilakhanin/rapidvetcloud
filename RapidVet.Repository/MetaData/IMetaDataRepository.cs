﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Model.Visits;

namespace RapidVet.Repository.MetaData
{
   public interface IMetaDataRepository : IDisposable
    {
       /// <summary>
       /// checks if user name exists
       /// </summary>
       /// <param name="username"> string user name</param>
       /// <param name="id"> int user id</param>
       /// <returns> bool</returns>
       bool IsUserNameExist(string username, int id);

       /// <summary>
       /// get all regional councils
       /// </summary>
       /// <returns> List<RegionalCouncil></returns>
       List<RegionalCouncil> GetRegionalCouncils();

       /// <summary>
       /// checks if id card exists in clinic
       /// </summary>
       /// <param name="idcard"> string client id card</param>
       /// <param name="userId"> int user id</param>
       /// <param name="clinicId">int clinic id</param>
       /// <returns>bool</returns>
       bool IsClientIdCardExistInClinic(string idcard, int userId, int clinicId);

       /// <summary>
       /// Gets medications which are dangerous drugs in clinic group
       /// </summary>
       /// <param name="clinicGroupId"> int clinic group id</param>
       /// <returns>List Medication</returns>
       List<Medication> GetDangerousDrugMedications(int clinicGroupId);

       /// <summary>
       /// check if catalog exist in clinic
       /// </summary>
       /// <param name="catalog">string catalog</param>
       /// <param name="priceListItemId">int price list item id</param>
       /// <param name="clinicId">int clinic id</param>
       /// <returns>bool</returns>
       bool IsCatalogExist(string catalog, int priceListItemId, int clinicId);

       /// <summary>
       /// get all price list item types
       /// </summary>
       /// <returns> IQueryable<PriceListItemType></returns>
       IQueryable<PriceListItemType> GetPriceListItemTypes();
    }
}
