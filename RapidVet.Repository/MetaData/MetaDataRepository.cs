﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Model.Visits;
using System.Data.Entity.SqlServer;

namespace RapidVet.Repository.MetaData
{
    public class MetaDataRepository : RepositoryBase<RapidVetDataContext>, IMetaDataRepository
    {
        public MetaDataRepository()
        {

        }

        public MetaDataRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public bool IsUserNameExist(string username, int id)
        {
            return DataContext.Users.Any(u => u.Username == username && u.Id != id);
        }

        public List<RegionalCouncil> GetRegionalCouncils()
        {
            return DataContext.RegionalCouncils.OrderBy(r => r.Name).ToList();
        }

        public bool IsClientIdCardExistInClinic(string idcard, int userId, int clinicId)
        {
            //pad db value with leading zeros upto 9 chars
            return DataContext.Clients.FirstOrDefault(c => c.ClinicId == clinicId && idcard.Equals(SqlFunctions.Replicate("0", 9 - c.IdCard.Length) + c.IdCard) && c.Id != userId) != null;
        }

        public List<Medication> GetDangerousDrugMedications(int clinicGroupId)
        {
            return DataContext.Medications.Where(m => m.ClinicGroupId == clinicGroupId && m.DangerousDrug).ToList();
        }

        public bool IsCatalogExist(string catalog, int priceListItemId, int clinicId)
        {
            return
                DataContext.PriceListItems.SingleOrDefault(
                    i => i.Category.ClinicId == clinicId && i.Catalog == catalog && i.Id != priceListItemId) != null;
        }

        public IQueryable<PriceListItemType> GetPriceListItemTypes()
        {
            return DataContext.PriceListItemTypes.OrderBy(t => t.Name);
        }
    }
}
