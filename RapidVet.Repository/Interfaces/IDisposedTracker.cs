﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Repository.Interfaces
{
    public interface IDisposedTracker : IDisposable
    {
        bool IsDisposed { get; set; }
    }
}
