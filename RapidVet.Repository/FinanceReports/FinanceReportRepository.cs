﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using RapidVet.Enums.Finances;
using RapidVet.Model.Clients;
using RapidVet.Model.Finance;
using System.Data.Entity.SqlServer;
using RapidVet.WebModels.Finance.Reports;
using System.Data.Entity.Core.Objects;

namespace RapidVet.Repository.FinanceReports
{
    public class FinanceReportRepository : RepositoryBase<RapidVetDataContext>, IFinanceReportRepository
    {

        public FinanceReportRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public FinanceReportRepository()
        {

        }

        public List<FinanceDocument> SearchDocuments(int clinicId, int? issuerId, int? documentTypeId, int? documentSerial, int? clientId,
                                          decimal? totalToPay, string chequeNumber)
        {
            var documents =
                DataContext.FinanceDocuments
                           .Include(f => f.Client)
                           .Where(d => d.ClinicId == clinicId);

            if (issuerId.HasValue && issuerId.Value > 0)
            {
                documents = documents.Where(d => d.IssuerId == issuerId);
            }

            if (documentTypeId.HasValue)
            {
                documents = documents.Where(d => d.FinanceDocumentTypeId == documentTypeId.Value);
            }

            if (documentSerial.HasValue)
            {
                documents = documents.Where(d => d.SerialNumber == documentSerial.Value);
            }

            if (clientId.HasValue && clientId.Value > 0)
            {
                documents = documents.Where(d => d.ClientId == clientId.Value);
            }
            if (clientId.HasValue && clientId.Value == 0)
            {
                documents = documents.Where(d => !d.ClientId.HasValue);
            }
            if (!string.IsNullOrWhiteSpace(chequeNumber))
            {
                var payments =
                    DataContext.FinanceDocumentPayments.Where(
                        p =>
                        p.ChequeNumber == chequeNumber && p.ClinicId == clinicId);
                documents =
                    documents.Where(
                        d =>
                        payments.Any(p => p.InvoiceId == d.Id) || payments.Any(p => p.ReciptId == d.Id) ||
                        payments.Any(p => p.RefoundId == d.Id));
            }
            var docList = documents.ToList();
            if (totalToPay.HasValue)
            {
                docList = docList.Where(d => d.TotalSum == totalToPay.Value).ToList();
            }

            return docList;
        }

        public IQueryable<FinanceDocument> GetIncomeReport(int clinicId, DateTime fromDate, DateTime toDate, int? issuerId, int[] selectedpaymentTypeId, int[] notSelectedDocumentTypeIds)
        {
            var documents =
                DataContext.FinanceDocuments
                           .Include(f => f.Client)
                           .Where(d => d.ClinicId == clinicId);
            toDate = toDate.AddDays(1);

            if (fromDate > DateTime.MinValue)
            {
                documents = documents.Where(d => d.Created >= fromDate);
            }

            if (toDate < DateTime.Now && toDate > DateTime.MinValue)
            {
                documents = documents.Where(d => d.Created < toDate);
            }

            if (issuerId.HasValue && issuerId.Value > 0)
            {
                documents = documents.Where(d => d.IssuerId == issuerId);
            }

            if (notSelectedDocumentTypeIds != null)
            {
                foreach (var notSelecteddocumentTypeId in notSelectedDocumentTypeIds)
                {
                    documents = documents.Where(d => d.FinanceDocumentTypeId != notSelecteddocumentTypeId);
                }
            }

            if (selectedpaymentTypeId != null)
            {
                Expression<Func<FinanceDocument, bool>> getAlways =
                    d =>
                    d.FinanceDocumentTypeId == (int)FinanceDocumentType.Refound ||
                    d.FinanceDocumentTypeId == (int)FinanceDocumentType.Proforma;

                Expression combined = getAlways.Body;
                foreach (var item in selectedpaymentTypeId)
                {
                    Expression<Func<FinanceDocument, bool>> predict = null;
                    switch (item)
                    {
                        case 0:
                            predict = d => d.TotalChequesPayment > 0;
                            break;
                        case 1:
                            predict = d => d.CashPaymentSum > 0;
                            break;
                        case 2:
                            predict = d => d.TotalCreditPaymentSum > 0;
                            break;
                        case 3:
                            predict = d => d.BankTransferSum > 0;
                            break;
                    }
                    if (predict != null)
                    {
                        //if (combined == null)
                        //{
                        //    combined = predict.Body;
                        //}
                        //else
                        //{
                        combined = Expression.OrElse(combined, predict.Body);
                        //}

                    }
                }
                var param = Expression.Parameter(typeof(FinanceDocument), "d");
                var replacer = new ParameterReplacer(param);
                combined = replacer.Visit(combined);
                var lambda = Expression.Lambda<Func<FinanceDocument, bool>>(combined, param);
                documents = documents.Where(lambda);
                documents = documents.OrderBy(c => c.Created);
            }
            return documents;
        }

        // Helper class to replace all parameters with the specified one
        class ParameterReplacer : ExpressionVisitor
        {
            private readonly ParameterExpression parameter;

            internal ParameterReplacer(ParameterExpression parameter)
            {
                this.parameter = parameter;
            }

            protected override Expression VisitParameter
                (ParameterExpression node)
            {
                return parameter;
            }
        }

        public IQueryable<Client> GetClientsInDebt(int clinicId)
        {
            return
                DataContext.Clients.Include(c => c.Patients)
                           .Include("Patients.Visits")
                           .Where(c => c.Patients.Any(p => p.Visits.Any(v => v.ReciptId == null)));
        }

        public IQueryable<FinanceDocumentPayment> GetPayments(int clinicId, DateTime fromDate, DateTime todate, bool getByCreatedDate, int issuerId)
        {
            IQueryable<FinanceDocumentPayment> results;
            todate = todate.AddDays(1);
            if (getByCreatedDate)
            {
                results =
                    DataContext.FinanceDocumentPayments.Where(
                        p => p.ClinicId == clinicId && p.Created >= fromDate && p.Created < todate && (issuerId == 0 || p.IssuerId == issuerId));
            }
            else
            {
                results =
                    DataContext.FinanceDocumentPayments.Where(
                        p => p.ClinicId == clinicId && p.DueDate >= fromDate && p.DueDate < todate && (issuerId == 0 || p.IssuerId == issuerId));
            }


            return results;
        }

        public IEnumerable<FinanceDocument> GetInvoices(DateTime fromDate, DateTime toDate, int clinicId)
        {
            var invoice = (int)FinanceDocumentType.Invoice;
            var invoiceReceipt = (int)FinanceDocumentType.InvoiceReceipt;
            var refund = (int)FinanceDocumentType.Refound;

            return
                DataContext.FinanceDocuments.Where(
                    d =>
                    d.ClinicId == clinicId && d.Created >= fromDate && d.Created <= toDate &&
                    (d.FinanceDocumentTypeId == invoice || d.FinanceDocumentTypeId == invoiceReceipt || d.FinanceDocumentTypeId == refund));
        }
        public IQueryable<FinanceDocumentPayment> GetCheques(int clinicId, DateTime fromDate, DateTime toDate, int documentTypeId, bool paidCheques, int issuerId)
        {
            toDate = toDate.AddDays(1);

            var documentIds =
                DataContext.FinanceDocuments.Where(
                    d =>
                    d.ClinicId == clinicId && d.TotalChequesPayment > 0 &&
                    (d.FinanceDocumentTypeId == (int)FinanceDocumentType.InvoiceReceipt ||
                     d.FinanceDocumentTypeId == documentTypeId) &&
                    d.Created >= fromDate && d.Created < toDate &&
                    (issuerId == 0 || d.IssuerId == issuerId)).Select(d => d.Id);



            IQueryable<FinanceDocumentPayment> payments = null;

            if (documentIds.Any())
            {
                payments =
                    DataContext.FinanceDocumentPayments
                    .Include(p => p.Invoice)
                    .Include(p => p.Recipt)
                    .Include(p => p.BankCode)
                    .Where(
                        p =>
                        p.PaymentTypeId == (int)PaymentType.Cheque &&
                        (
                            (p.InvoiceId != null && documentIds.Contains(p.InvoiceId.Value))
                            ||
                            (p.ReciptId != null && documentIds.Contains(p.ReciptId.Value))
                        )
                        );

                payments = paidCheques
                               ? payments.Where(p => p.DueDate < toDate)
                               : payments.Where(p => p.DueDate > toDate);

            }

            return payments;
        }

        public decimal GetPermanentAssetsInputTax(int clinicId, DateTime from, DateTime to)
        {
            var equipment = RapidConsts.ExpensesConstants.EquipmentAndPermanentAssets;
            var expenses = DataContext.Expenses.Where(
                    e =>
                    e.EnteredDate >= from && e.EnteredDate <= to && e.ExpenseGroup.Name == equipment &&
                    e.ExpenseGroup.ClinicId == clinicId);
            return expenses.Any() ? expenses.Sum(e => e.Vat) : 0;
        }

        public decimal GetOtherInputTax(int clinicId, DateTime from, DateTime to)
        {
            var equipment = RapidConsts.ExpensesConstants.EquipmentAndPermanentAssets;
            var expenses = DataContext.Expenses.Where(
                e =>
                e.EnteredDate >= from && e.EnteredDate <= to && e.ExpenseGroup.Name != equipment &&
                e.ExpenseGroup.ClinicId == clinicId);
            return expenses.Any() ? expenses.Sum(e => e.Vat) : 0;
        }

        public decimal PriceListItemUnitsSold(int priceListItemId, DateTime fromDate, DateTime toDate)
        {
            decimal result = 0;

            var data =
                DataContext.FinanceDocumentItems.Include(f => f.FinanceDocument)
                           .Where(
                               f =>
                               f.FinanceDocument.Created >= fromDate && f.FinanceDocument.Created < toDate &&
                               f.PriceListItemId == priceListItemId);

            foreach (var d in data)
            {
                var quantity = d.Quantity ?? 1;
                result += quantity;
            }

            return result;
        }

        public List<RapidVet.WebModels.Finance.Reports.InvoiceReportWebModel> GetInvoiceReportData(int clinicId, DateTime? fromDate, DateTime? toDate,
                                                                bool onlyConverted, int issuerId)
        {
            return (from f in DataContext.FinanceDocuments
                    where f.ClinicId == clinicId &&
                    (
                    (!fromDate.HasValue || !toDate.HasValue)
                    ||
                    (f.Created >= fromDate.Value && f.Created < toDate.Value)
                    )
                    &&
                    (!onlyConverted || f.ConvertedFromRecipt)
                    &&
                    (f.FinanceDocumentTypeId == (int)FinanceDocumentType.Invoice ||
                     f.FinanceDocumentTypeId == (int)FinanceDocumentType.InvoiceReceipt ||
                     f.FinanceDocumentTypeId == (int)FinanceDocumentType.Refound) &&
                    (issuerId == 0 || f.IssuerId == issuerId)
                    //let convertedCreated = SqlFunctions.StringConvert((decimal)f.Created.Day, 2) + "/" + SqlFunctions.StringConvert((decimal)f.Created.Month, 2) + "/" + SqlFunctions.StringConvert((decimal)f.Created.Year, 4)
                    select new RapidVet.WebModels.Finance.Reports.InvoiceReportWebModel()
                    {
                        VAT = f.VAT,
                        FinanceDocumentId = f.Id,
                        Created = f.Created,
                        //Date = convertedCreated,
                        FinanceDocumentTypeId = f.FinanceDocumentTypeId,
                        FinanceDocumentSerialNumber = f.SerialNumber,
                        ClientName = f.ClientName,
                        SumToPayForInvoice = f.SumToPayForInvoice,
                        Cash = f.CashPaymentSum,
                        Transfer = f.BankTransferSum,
                        ChequeAmount = f.TotalChequesPayment,
                        CreditAmount = f.TotalCreditPaymentSum,
                        TotalAmount = f.FinanceDocumentTypeId == (int)FinanceDocumentType.Invoice ? f.SumToPayForInvoice :
                        (f.CashPaymentSum + f.BankTransferSum + f.TotalCreditPaymentSum + f.TotalChequesPayment)
                    }).OrderBy(f => f.FinanceDocumentTypeId).ThenBy(f => f.FinanceDocumentSerialNumber).ToList();

        }

        public IQueryable<FinanceDocument> GetReciptReportData(int clinicId, DateTime? fromDate, DateTime? toDate, int issuerId)
        {
            IQueryable<FinanceDocument> data;
            if (!fromDate.HasValue || !toDate.HasValue)
            {
                data =
                    DataContext.FinanceDocuments.Where(
                        f =>
                        f.ClinicId == clinicId &&
                        f.FinanceDocumentTypeId == (int)FinanceDocumentType.Receipt &&
                        (issuerId == 0 || f.IssuerId == issuerId)).OrderBy(f => f.Created);
            }
            else
            {
                data =
                    DataContext.FinanceDocuments.Where(
                        f =>
                        f.ClinicId == clinicId && f.Created >= fromDate.Value && f.Created < toDate.Value &&
                        f.FinanceDocumentTypeId == (int)FinanceDocumentType.Receipt &&
                        (issuerId == 0 || f.IssuerId == issuerId)).OrderBy(f => f.Created);
            }

            return data;
        }


        public List<RapidVet.WebModels.Finance.Reports.InvoiceReportWebModel> GetReciptReportDataAll(int clinicId, DateTime? fromDate, DateTime? toDate, int issuerId)
        {
            StringBuilder query = new StringBuilder();
            query.Append(" Select  Distinct "+
                "      Date = Convert(varchar(30),f.Created,103),"+
                     " ClientName = f.ClientName,"+
                     " Cash = f.CashPaymentSum,"+
                     " Transfer = f.BankTransferSum,"+
                     " CreditCardCompany = c.Name,"+
                     " CreditCardBusinessType = ict.Name,"+
                     " CreditAmount = isnull(f.[TotalCreditPaymentSum],0), "+
                     " BankName = b.Name,"+
                     " BankBranchName = fd.BankBranch,"+
                     " BankAccountNumber = fd.BankAccount,"+
                     " ChequeNumber = fd.ChequeNumber,"+
                     " ChequeAmount = CASE WHEN fd.ChequeNumber is not null THEN fd.Sum Else 0 END,"+
                     " TotalAmount = isnull(f.[TotalCreditPaymentSum],0) + isnull(f.[TotalChequesPayment],0) + isnull(f.[CashPaymentSum],0) + isnull(f.[BankTransferSum],0)," +
                     " VAT = f.VAT,"+
                     " FinanceDocumentType =   N'קבלה'," +
                     " FinanceDocumentSerialNumber = f.SerialNumber,"+
                     " FinanceDocumentTypeId = f.FinanceDocumentTypeId,"+
                     " SumToPayForInvoice = f.SumToPayForInvoice,"+
                     " FinanceDocumentId = f.Id,"+
                     " Created = f.Created "+
                " From FinanceDocuments f left join "+
                " FinanceDocumentPayments fd on  (f.FinanceDocumentTypeId = 1 and fd.ReciptId = f.Id)   left outer join " +
                " CreditCardCodes c on fd.CreditCardCodeId = c.Id left outer join " +
                " IssuerCreditTypes ict on ict.Id = fd.IssuerCreditTypeId left outer join " +
                " BankCodes b on fd.BankCodeId = b.Code " +
                " Where f.FinanceDocumentTypeId = 1 AND f.ClinicId = " + clinicId.ToString());
               
            if(fromDate.HasValue && toDate.HasValue)
            {
                query.Append(" and f.Created >= '" + fromDate.Value.ToString("yyyy-MM-dd") + "' and f.Created < '" + toDate.Value.ToString("yyyy-MM-dd") + "' ");
            }

            query.Append(" and (" + issuerId.ToString() + " = 0 or f.IssuerId = " + issuerId.ToString() + " )");

            var data = DataContext.Database.SqlQuery<RapidVet.WebModels.Finance.Reports.InvoiceReportWebModel>(query.ToString()).OrderBy(f => f.Created).ToList();

            return data;
        }

        public decimal GetLastAdvanceItemInMonth(int clinicId, DateTime monthStart, int issuerId)
        {
            decimal result = 0;
            var nextMonth = monthStart.AddMonths(1);
            var advances =
                DataContext.AdvancedPaymentsPercents.Where(
                    a => a.ClinicId == clinicId && a.PercentChangedDate < nextMonth && a.IssuerId == issuerId)
                           .OrderByDescending(a => a.PercentChangedDate);
            if (advances.Any())
            {
                result = advances.First().Percent;
            }

            return result;
        }


        public decimal GetAdvancedPaymentPercentByDate(int clinicId, DateTime paymentDate, int issuerId)
        {
            decimal result = 0;

            var advances =
                DataContext.AdvancedPaymentsPercents.Where(
                    a => a.ClinicId == clinicId && a.PercentChangedDate <= paymentDate && a.IssuerId == issuerId)
                           .OrderByDescending(a => a.PercentChangedDate);
            if (advances.Any())
            {
                result = advances.First().Percent;
            }

            return result * (decimal)0.01;
        }

        public IQueryable<FinanceDocument> GetIssuerDocuments(int issuerId, DateTime monthStart)
        {
            var nextMonth = monthStart.AddMonths(1);
            return
                DataContext.FinanceDocuments.Where(
                    d =>
                    d.IssuerId == issuerId && d.Created >= monthStart && d.Created < nextMonth &&
                    (d.FinanceDocumentTypeId == (int)FinanceDocumentType.Invoice ||
                     d.FinanceDocumentTypeId == (int)FinanceDocumentType.InvoiceReceipt ||
                     d.FinanceDocumentTypeId == (int)FinanceDocumentType.Refound));
        }

        public IQueryable<FinanceDocument> GetIssuerDocuments(int? issuerId, DateTime monthStart, int clinicId)
        {
            if (issuerId.HasValue && issuerId.Value > 0)
            {
                return GetIssuerDocuments(issuerId.Value, monthStart);
            }

            var clinicIssuersIds = DataContext.Issuers.Where(i => i.ClinicId == clinicId).Select(i => i.Id).ToList();

            var nextMonth = monthStart.AddMonths(1);
            var data = DataContext.FinanceDocuments.Where(
                d =>
                clinicIssuersIds.Contains(d.IssuerId) && d.Created >= monthStart && d.Created < nextMonth &&
                (d.FinanceDocumentTypeId == (int)FinanceDocumentType.Invoice ||
                 d.FinanceDocumentTypeId == (int)FinanceDocumentType.InvoiceReceipt ||
                 d.FinanceDocumentTypeId == (int)FinanceDocumentType.Refound));

            return data;
        }

        public decimal SumPermanentAssetExpensesInMonth(int clinicId, DateTime monthStart)
        {
            decimal result = 0;
            var nextMonth = monthStart.AddMonths(1);
            var expenses =
                DataContext.Expenses.Include(e => e.Supplier)
                           .Include(e => e.ExpenseGroup)
                           .Where(
                               e =>
                               e.Supplier.ClinicId == clinicId && e.ExpenseGroup.ClinicId == clinicId &&
                               e.ExpenseGroup.Name == RapidConsts.ExpensesConstants.EquipmentAndPermanentAssets
                               && e.EnteredDate >= monthStart && e.EnteredDate < nextMonth);

            if (expenses.Any())
            {
                result = expenses.Sum(e => e.TotalAmount);
            }

            return result;
        }

        public decimal GetNotPermanentExpensesInMonth(int clinicId, DateTime monthStart)
        {
            decimal result = 0;
            var nextMonth = monthStart.AddMonths(1);
            var expenses =
                DataContext.Expenses.Include(e => e.Supplier)
                           .Include(e => e.ExpenseGroup)
                           .Where(
                               e =>
                               e.Supplier.ClinicId == clinicId && e.ExpenseGroup.ClinicId == clinicId &&
                               e.ExpenseGroup.Name != RapidConsts.ExpensesConstants.EquipmentAndPermanentAssets
                                && e.EnteredDate >= monthStart && e.EnteredDate < nextMonth);

            if (expenses.Any())
            {
                result = expenses.Sum(e => e.TotalAmount);
            }

            return result;
        }

        public IQueryable<FinanceDocumentItem> GetSalesReportData(int clinicId, DateTime fromDate, DateTime toDate, int? categoryId, int? priceListItemId, int? issuerId, bool groupBySeller, bool showAllItems)
        {
            toDate = toDate.AddDays(1);

            var data =
                DataContext.FinanceDocumentItems
                           .Include(f => f.FinanceDocument)
                            .Include(f => f.Visit.Doctor)
                           .Include("FinanceDocument.User")
                           .Where(
                               f =>

                               f.FinanceDocument.ClinicId == clinicId &&
                               f.FinanceDocument.Created >= fromDate &&
                               f.FinanceDocument.Created <= toDate &&
                               (f.FinanceDocument.FinanceDocumentTypeId == (int)FinanceDocumentType.Invoice ||
                                f.FinanceDocument.FinanceDocumentTypeId == (int)FinanceDocumentType.InvoiceReceipt)); //|| removed recipt only according to Dan's instructions.
            //f.FinanceDocument.FinanceDocumentTypeId == (int)FinanceDocumentType.Receipt));


            if (categoryId.HasValue && categoryId.Value > 0)
            {
                var categoryItemIds =
                    DataContext.PriceListItems.Where(p => p.CategoryId == categoryId.Value).Select(p => p.Id);
                data =
                    data.Where(d => categoryItemIds.Contains(d.PriceListItemId.Value));

            }

            if (priceListItemId.HasValue && priceListItemId.Value > 0)
            {
                data =
                    data.Where(d => d.PriceListItemId.Value == priceListItemId.Value);

            }

            if (issuerId.HasValue && issuerId.Value > 0)
            {
                data = data.Where(d => d.FinanceDocument.IssuerId == issuerId.Value);

            }

            if (!showAllItems)
            {
                data = data.Where(f => f.PriceListItemId.HasValue && f.PriceListItemId.Value > 0);
            }

            if (groupBySeller)
            {
                data = data.OrderBy(d => d.AddedBy).ThenBy(d => d.PriceListItemId).ThenBy(d => d.FinanceDocument.Created);
            }
            else
            {
                data = data.OrderBy(d => d.PriceListItemId).ThenBy(d => d.FinanceDocument.Created);
            }

            return data;
        }

        public IQueryable<FinanceDocument> IncommingsReportData(int clinicId, DateTime fromDate, DateTime toDate, int issuerId)
        {
            toDate = toDate.AddMinutes(1);
            var data =
                DataContext.FinanceDocuments.Where(
                    f =>
                    f.ClinicId == clinicId && f.Created >= fromDate && f.Created < toDate && f.IssuerId == issuerId &&
                    (f.FinanceDocumentTypeId == (int)FinanceDocumentType.InvoiceReceipt ||
                     f.FinanceDocumentTypeId == (int)FinanceDocumentType.Receipt));




            foreach (var document in data)
            {
                document.Payments = DataContext.FinanceDocumentPayments.Where(
                            p =>
                            p.ReciptId == document.Id || p.InvoiceId == document.Id ||
                            p.RefoundId == document.Id).ToList();
            }

            return data;
        }

        public int GetNumberOfCreditPayments(int financeDocumentId)
        {
            var data =
                DataContext.FinanceDocumentPayments.Where(
                    f => f.ReciptId == financeDocumentId && f.PaymentTypeId == (int)PaymentType.CreditCard);

            return data.Any() ? data.Count() : 0;
        }

        public List<ClientsInDebtWebModel> GetClientsInMinusBalance(int activeClinicId, double fromSum, double toSum, long vetID, DateTime dtNotVisitedUfter, DateTime dtFrom, DateTime dtTo)
        {
            var s = (from c in DataContext.Clients
                     where ((c.Balance.HasValue) &&
                     (c.Balance < 0) &&
                     (c.ClinicId == activeClinicId) &&
                     ((double)(c.Balance) * -1.0 >= fromSum) &&
                     ((double)(c.Balance) * -1.0 <= toSum) &&
                     (((vetID > 0 && c.VetId.HasValue && c.VetId == vetID)) || vetID == 0))
                     join p in DataContext.Patients on c.Id equals p.ClientId
                     join v in DataContext.Visits on p.Id equals v.PatientId 
                     where
                     (((dtNotVisitedUfter.Year < 1905) ||
                     (dtNotVisitedUfter.Year > 1905 && SqlFunctions.DateDiff("day", v.VisitDate, dtNotVisitedUfter) >= 0)) &&
                     ((dtFrom.Year < 1905) ||
                     (dtFrom.Year > 1905 && SqlFunctions.DateDiff("day", v.VisitDate, dtFrom) <= 0 && SqlFunctions.DateDiff("day", v.VisitDate, dtTo) > 0)))
                     select new ClientsInDebtWebModel()
                          {
                              ClientName = c.FirstName + " " + c.LastName,
                              DebtAmount = c.Balance.Value * (decimal)-1.0,
                              ClientUrl = "../Clients/Patients/" + SqlFunctions.StringConvert((double)p.ClientId).Trim(),
                              LastVisitStr = SqlFunctions.DateName("day", v.VisitDate) + "/" + SqlFunctions.StringConvert((double)v.VisitDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", v.VisitDate),
                              LastVisitDate = EntityFunctions.TruncateTime(v.VisitDate),
                          });

            //return s.Distinct().OrderByDescending(t => t.LastVisitDate).ToList();

            var query = s.GroupBy(t => t.ClientName)//get sidtict rows without the date
                   .Select(grp => new ClientsInDebtWebModel()
                   {
                       ClientName = grp.Max(a => a.ClientName),
                       DebtAmount = grp.Max(q => q.DebtAmount),
                       ClientUrl = grp.Max(e => e.ClientUrl),
                       LastVisitStr = grp.Max(r => r.LastVisitStr),
                       LastVisitDate = EntityFunctions.TruncateTime(grp.Max(r => r.LastVisitDate)),
                   });

            return query.OrderBy(a => a.ClientName).ToList();
        }
    }
}
;