﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.WebModels.Finance.Reports;

namespace RapidVet.Repository.FinanceReports
{
    public interface IFinanceReportRepository : IDisposable
    {
        /// <summary>
        /// serach finance documents by various paramentes
        /// </summary>
        /// <param name="clinicId"> int clinic id</param>
        /// <param name="issuerId">int? issuer id</param>
        /// <param name="documentTypeId">int? document type id</param>
        /// <param name="documentSerial">int? document serial</param>
        /// <param name="clientId"> int? client id</param>
        /// <param name="totalToPay">decimal? total to pay</param>
        /// <param name="chequeNumber">string cheque number</param>
        /// <returns>IQueryable<FinanceDocument></returns>
        List<FinanceDocument> SearchDocuments(int clinicId, int? issuerId, int? documentTypeId,
                                                    int? documentSerial, int? clientId, decimal? totalToPay,
                                                    string chequeNumber);

        /// <summary>
        /// return finance documents by search parameters
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="fromDate">date time from</param>
        /// <param name="toDate"> datetime ti</param>
        /// <param name="issuerId">int? issuer id</param>
        /// <param name="notSelectedDocIds"></param>
        /// <param name="notSelecteddocumentTypeIds"></param>
        /// <param name="paymentTypeId">int? payment type id</param>
        /// <returns>IQueryable<FinanceDocument></returns>
        IQueryable<FinanceDocument> GetIncomeReport(int clinicId, DateTime fromDate, DateTime toDate, int? issuerId, int[] SelectedpaymentTypeId, int[] notSelectedDocumentTypeIds);

        /// <summary>
        /// get all clinic clients that are in debt
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>IQueryable<Client></returns>
        IQueryable<Client> GetClientsInDebt(int clinicId);


        /// <summary>
        /// return finance documents payments for clinic id where due date in date range
        /// </summary>
        /// <param name="clinicId"></param>
        /// <param name="fromDate">date time from</param>
        /// <param name="todate">date time to</param>
        /// <param name="getByCreatedDate"> get payments by created date</param>
        /// <param name="issuerId">issuer id, default=0</param>
        /// <returns>IQueryable<FinanceDocumentPayment></returns>
        IQueryable<FinanceDocumentPayment> GetPayments(int clinicId, DateTime fromDate, DateTime todate, bool getByCreatedDate,int issuerId=0);

        /// <summary>
        /// get cheques for clinic by parameters
        /// </summary>
        /// <param name="clinicId"></param>
        /// <param name="fromDate">date time from</param>
        /// <param name="toDate">date time to</param>
        /// <param name="documentTypeId">int? document type id</param>
        /// <param name="paidCheques">get paid || unpaid cheques</param>
        /// <param name="issuerId">issuer id - default: 0</param>
        /// <returns> IQueryable<FinanceDocumentPayment></returns>
        IQueryable<FinanceDocumentPayment> GetCheques(int clinicId, DateTime fromDate, DateTime toDate, int documentTypeId, bool paidCheques,int issuerId=0);

        /// <summary>
        /// Gets invoices & invoices receipts  for given clinic id in given dates
        /// </summary>
        /// <param name="fromDate">date time from date</param>
        /// <param name="toDate">date time to date</param>
        /// <param name="clinicId">int clinic id </param>
        /// <returns></returns>
        IEnumerable<FinanceDocument> GetInvoices(DateTime fromDate, DateTime toDate, int clinicId);

        decimal GetPermanentAssetsInputTax(int clinicId,DateTime from, DateTime to );

        decimal GetOtherInputTax(int clinicId, DateTime from, DateTime to);

        /// <summary>
        /// get number of units sold for price list item id in date range
        /// </summary>
        /// <param name="priceListItemId">int price list item id</param>
        /// <param name="fromDate"> date time from</param>
        /// <param name="toDate"> date time to</param>
        /// <returns>int</returns>
        decimal PriceListItemUnitsSold(int priceListItemId, DateTime fromDate, DateTime toDate);


        /// <summary>
        /// get all invoices or invoice / recipt / invoiceRecipt  for clinic by parameters
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="fromDate">datetime from date</param>
        /// <param name="toDate">datetime to date</param>
        /// <param name="onlyConverted"></param>
        /// <param name="issuerId">issuer id, default: 0 </param>
        /// <returns>IQueryable<FinanceDocument></returns>
        List<RapidVet.WebModels.Finance.Reports.InvoiceReportWebModel> GetInvoiceReportData(int clinicId, DateTime? fromDate, DateTime? toDate, bool onlyConverted, int issuerId = 0);


        /// <summary>
        /// get all recipts for clinic by parameters
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="fromDate">datetime from date</param>
        /// <param name="toDate">datetime to date</param>       
        /// <param name="issuerId">issuer id, default: 0 </param>
        /// <returns>IQueryable<FinanceDocument></returns>
        IQueryable<FinanceDocument> GetReciptReportData(int clinicId, DateTime? fromDate, DateTime? toDate, int issuerId);

        List<InvoiceReportWebModel> GetReciptReportDataAll(int clinicId, DateTime? fromDate, DateTime? toDate, int issuerId);

        /// <summary>
        /// get last AdvancedPaymentsPercent in month for clinic and issuer
        /// </summary>
        /// <param name="clinicId"></param>
        /// <param name="monthStart">datetimt monrh</param>
        /// <param name="issuerId"></param>
        /// <returns>decimal</returns>
        decimal GetLastAdvanceItemInMonth(int clinicId, DateTime monthStart, int issuerId);

        /// <summary>
        /// get last AdvancedPaymentsPercent in month for clinic and issuer
        /// </summary>
        /// <param name="clinicId"></param>
        /// <param name="monthStart">datetimt monrh</param>
        /// <param name="issuerId"></param>
        /// <returns>decimal</returns>
        decimal GetAdvancedPaymentPercentByDate(int clinicId, DateTime paymentDate, int issuerId);

        /// <summary>
        /// get all finance documents for issuer in month
        /// </summary>
        /// <param name="issuerId">int issuer id</param>
        /// <param name="monthStart">datetime month start</param>
        /// <returns> IQueryable<FinanceDocument></returns>
        IQueryable<FinanceDocument> GetIssuerDocuments(int issuerId, DateTime monthStart);

        /// <summary>
        /// get all finance documents for issuer / all clinic issuers in month
        /// </summary>
        /// <param name="issuerId">int? issuer id</param>
        /// <param name="monthStart">datetime month start</param>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>IQueryable<FinanceDocument></returns>
        IQueryable<FinanceDocument> GetIssuerDocuments(int? issuerId, DateTime monthStart, int clinicId);

        /// <summary>
        /// get sum of all permanent asset expenses for clinic in month
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="monthStart">datetime month start</param>
        /// <returns>decimal</returns>
        decimal SumPermanentAssetExpensesInMonth(int clinicId, DateTime monthStart);


        /// <summary>
        /// get sum of all not permanent asset expenses for clinic in month
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="monthStart">datetime month start</param>
        /// <returns>decimal</returns>
        decimal GetNotPermanentExpensesInMonth(int clinicId, DateTime monthStart);


        /// <summary>
        /// get all finance document items for invoice/ /invoice-recipt in date rande and/or category id / price list item id for clinic
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="fromDate">date time from</param>
        /// <param name="toDate">date time to</param>
        /// <param name="categoryId">int? price list category id</param>
        /// <param name="priceListItemId">int? price list item id</param>
        /// <returns>IQueryable<FinanceDocumentItem></returns>
        IQueryable<FinanceDocumentItem> GetSalesReportData(int clinicId, DateTime fromDate, DateTime toDate,
                                                           int? categoryId, int? priceListItemId,int? issuerId, bool groupBySeller, bool showAllItems);

        /// <summary>
        /// get finance documents for clinic by date range
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="fromDate">datetime from</param>
        /// <param name="toDate">date time to</param>
        /// <returns>IQueryable<FinanceDocument> </returns>
        IQueryable<FinanceDocument> IncommingsReportData(int clinicId, DateTime fromDate, DateTime toDate, int issuerId);

        /// <summary>
        /// get number of credit payments for finance document id
        /// </summary>
        /// <param name="financeDocumentId">int finance document id</param>
        /// <returns>int</returns>
        int GetNumberOfCreditPayments(int financeDocumentId);

        List<ClientsInDebtWebModel> GetClientsInMinusBalance(int activeClinicId, double fromSum, double toSum, long vetID, DateTime dtNotVisitedUfter, DateTime dtFrom, DateTime dtTo);
    }
}
