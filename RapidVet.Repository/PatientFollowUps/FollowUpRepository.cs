﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.PatientFollowUp;

namespace RapidVet.Repository.PatientFollowUps
{
    public class FollowUpRepository : RepositoryBase<RapidVetDataContext>, IFollowUpRepository
    {
        public FollowUpRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public FollowUpRepository()
        {

        }

        public IQueryable<FollowUp> GetFollowUps(int clinicId, DateTime? fromDate, DateTime? toDate, bool includeInactivePatients, int docID)
        {
            //  var patientIds = GetClinicPatientIds(clinicId, includeInactivePatients);
            if (!fromDate.HasValue || !toDate.HasValue)
            {
                return DataContext.FollowUps
                                  .Include(f => f.Patient)
                                  .Include(f => f.Patient.Client.CalenderEntries)
                                  .Include("Patient.AnimalRace.AnimalKind")
                                  .Include("Patient.Client")
                                  .Include("Patient.Client.Addresses")
                                  .Include("Patient.Client.Addresses.City")
                                  .Include("Patient.Client.Phones")
                                  .Include("Patient.Client.Phones.PhoneType")
                                  .Include("Patient.Client.Emails")
                                  .Include("Patient.Client.Title")
                                  .Include(f => f.User)
                                  .Where(f => f.Patient.Client.ClinicId == clinicId && (docID == -1 || f.UserId == docID))
                                  .OrderByDescending(f => f.DateTime);
                //f => patientIds.Contains(f.PatientId));
            }

            toDate = toDate.Value.AddDays(1);
            return
                DataContext.FollowUps
                           .Include(f => f.Patient)
                           .Include("Patient.AnimalRace.AnimalKind")
                           .Include("Patient.Client")
                           .Include("Patient.Client.Addresses")
                           .Include("Patient.Client.Addresses.City")
                           .Include("Patient.Client.Phones")
                           .Include("Patient.Client.Phones.PhoneType")
                           .Include("Patient.Client.Emails")
                           .Include("Patient.Client.Title")
                           .Include(f => f.Patient.Client.CalenderEntries)
                           .Include(f => f.User)
                           .Where(
                               f => f.Patient.Client.ClinicId == clinicId && f.DateTime >= fromDate && f.DateTime < toDate && (docID == -1 || f.UserId == docID))
                           .OrderByDescending(f => f.DateTime);
        }

        public OperationStatus Create(FollowUp followUp)
        {
            DataContext.FollowUps.Add(followUp);
            return base.Save(followUp);
        }

        public DateTime? GetLastCalendarEntryDate(int patientId)
        {
            DateTime? result = null;
            var entries =
                DataContext.CalenderEntries.Where(c => c.PatientId == patientId).OrderByDescending(c => c.Date).Take(1);
            var entry = entries.FirstOrDefault();
            if (entry != null)
            {
                result = entry.Date;
            }
            return result;
        }

        public bool RenewAllFollowUps(int clinicId, int minDaysFromFollowUpWithoutAppointment, int remindersAddMonths)
        {
            var patientIds = GetClinicPatientIds(clinicId);
            var now = DateTime.Now;
            var counter = 0;
            var result = true;

            //foreach (var patientId in patientIds)
            //{
            //    var lastFollowUp =
            //        DataContext.FollowUps.Where(f => f.PatientId == patientId)
            //                   .OrderByDescending(f => f.DateTime)
            //                   .FirstOrDefault();

            //    if (lastFollowUp != null)
            //    {
            //var lastEntry = GetLastCalendarEntryDate(patientId);
            //        var overMin = false;
            //        if (lastEntry != null && lastEntry.HasValue && lastEntry.Value > DateTime.MinValue)
            //        {
            //            overMin = (lastFollowUp.DateTime - lastEntry.Value).Days > minDaysFromFollowUpWithoutAppointment;
            //        }

            //        if (overMin || lastEntry == null || lastEntry == DateTime.MinValue)
            //        {
            //            lastFollowUp.DateTime = lastFollowUp.DateTime.AddMonths(remindersAddMonths);
            //            lastFollowUp.Updated = now;
            //            counter++;
            //        }
            //    }
            //}

            var followUps = DataContext.FollowUps.Where(f => patientIds.Contains(f.PatientId));

            foreach (var followUp in followUps)
            {
                if (minDaysFromFollowUpWithoutAppointment > 0)
                {
                    var lastEntry = GetLastCalendarEntryDate(followUp.PatientId);
                    if (!(lastEntry.HasValue && (lastEntry.Value - followUp.DateTime).Days >= minDaysFromFollowUpWithoutAppointment))
                    {
                        continue;
                    }
                }

                followUp.DateTime = followUp.DateTime.AddMonths(remindersAddMonths);
                followUp.Updated = now;
                counter++;
            }

            if (counter > 0)
            {
                result = DataContext.SaveChanges() > -1;
            }

            return result;

        }

        public FollowUp GetFollowUp(int followUpId)
        {
            return DataContext.FollowUps
                              .Include(f => f.Patient)
                              .Include("Patient.AnimalRace.AnimalKind")
                              .Include("Patient.Client")
                              .Include("Patient.Client.Title")
                              .Include("Patient.Client.Addresses")
                              .Include("Patient.Client.Addresses.City")
                              .Include("Patient.Client.Phones")
                              .Include("Patient.Client.Phones.PhoneType")
                              .Include("Patient.Client.Emails")
                              .Include(f => f.User)
                              .Single(f => f.Id == followUpId);
        }

        public OperationStatus Delete(int followUpId)
        {
            var followUp = DataContext.FollowUps.Single(f => f.Id == followUpId);
            DataContext.FollowUps.Remove(followUp);
            return base.Save(followUp);
        }

        public DateTime FollowUpDefaultDate(int clinicId)
        {
            var result = DateTime.Now;
            var clinic = DataContext.Clinics.Single(c => c.Id == clinicId);
            switch (clinic.DefaultFollowUpType)
            {
                case FollowUpTimeRange.Days:
                    result = result.AddDays(clinic.DefaultFollowUpTime);
                    break;
                case FollowUpTimeRange.Months:
                    result = result.AddMonths(clinic.DefaultFollowUpTime);
                    break;
            }
            return result;
        }

        private List<int> GetClinicPatientIds(int clinicId, bool includeInactive = false)
        {
            var result = DataContext.Patients.Where(p => p.Client.ClinicId == clinicId);
            if (!includeInactive)
            {
                result = result.Where(p => p.Active);
            }
            return result.Select(p => p.Id).ToList();

        }
    }
}
