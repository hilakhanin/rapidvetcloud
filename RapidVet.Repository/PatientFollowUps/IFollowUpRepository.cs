﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.PatientFollowUp;

namespace RapidVet.Repository.PatientFollowUps
{
    public interface IFollowUpRepository : IDisposable
    {
        /// <summary>
        /// get followups for clinic for date range
        /// </summary>
        /// <param name="clinicId"> int clinic id</param>
        /// <param name="fromDate"> date time from</param>
        /// <param name="toDate"> date time to </param>
        /// <param name="includeInactivePatients">bool to include inactive patients</param>
        /// <returns>IQueryable<FollowUp></returns>
        IQueryable<FollowUp> GetFollowUps(int clinicId, DateTime? fromDate, DateTime? toDate, bool includeInactivePatients, int docID);

        /// <summary>
        /// create followup obj
        /// </summary>
        /// <param name="followUp">FollowUp obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(FollowUp followUp);

        /// <summary>
        /// get last calendar entry date for patient
        /// </summary>
        /// <param name="patientId">int patient id</param>
        /// <returns>DateTime?</returns>
        DateTime? GetLastCalendarEntryDate(int patientId);

        /// <summary>
        /// updates all followups in clinic by parameters
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="minDaysFromFollowUpWithoutAppointment"> minimum of days after last follow up without a calendar entry</param>
        /// <param name="remindersAddMonths">set reminders to X months from now</param>
        /// <returns>bool</returns>
        bool RenewAllFollowUps(int clinicId, int minDaysFromFollowUpWithoutAppointment, int remindersAddMonths);

        /// <summary>
        /// get specific followUp obj
        /// </summary>
        /// <param name="followUpId">int followup id</param>
        /// <returns>FollowUp</returns>
        FollowUp GetFollowUp(int followUpId);

        /// <summary>
        /// delete follow up obj
        /// </summary>
        /// <param name="followUpId">int follow up id</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(int followUpId);

        /// <summary>
        /// get date for followup by clinic id
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>datetime</returns>
        DateTime FollowUpDefaultDate(int clinicId);
    }
}
