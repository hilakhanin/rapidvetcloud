﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.FullCalender;
using RapidVet.Repository.Holidays;
using System.Data.Entity;

namespace RapidVet.Repository.Calender
{
    public class CalenderRepository : RepositoryBase<RapidVetDataContext>, ICalenderRepository
    {
        public CalenderRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public CalenderRepository()
        {

        }

        public List<CalenderEntry> GetDocsCalenderEntry(int clinicId, int doctorId, DateTime startDate, DateTime endDate, bool getCancelled = false)
        {
            return DataContext.CalenderEntries.Include(c => c.Client).Include(r => r.Room).Include(p => p.Patient).Where(c => c.DoctorId == doctorId && c.ClinicId == clinicId && c.Date >= startDate && c.Date <= endDate && (getCancelled || !c.IsCancelled)).ToList();            
        }

        public OperationStatus CreateCalenderEntry(CalenderEntry calenderEntry)
        {
            DataContext.CalenderEntries.Add(calenderEntry);
            return base.Save(calenderEntry);
        }

        public CalenderEntry GetCalenderEntry(int calenderEntryId, bool includeExtraEntities = false)
        {
            if (includeExtraEntities)
            {
                return DataContext.CalenderEntries
                .Include(c => c.Doctor)
                .Include("Client")
                .Include("Client.Phones")
                .Include("Client.Emails")
                .Include(c => c.Patient).SingleOrDefault(c => c.Id == calenderEntryId);
            }
            return DataContext.CalenderEntries.SingleOrDefault(c => c.Id == calenderEntryId);
        }

        public List<CalenderEntry> GetRoomsCalenderEntries(int roomId)
        {
            return DataContext.CalenderEntries.Include(c => c.Client).Where(c => c.RoomId == roomId).ToList();
        }

        public List<CalenderEntry> GetCalenderEntriesForClient(int clientId)
        {
            return DataContext.CalenderEntries.Include(c => c.Client).Include(c=>c.Patient).Where(c => c.ClientId == clientId).ToList();
        }

        public OperationStatus AddBlockedDay(BlockedDay blockedDay)
        {
            DataContext.BlockedDays.Add(blockedDay);
            return base.Save(blockedDay);
        }

        public OperationStatus RemoveBlockedDay(int blockedDayId)
        {

            var blockedDay = DataContext.BlockedDays.SingleOrDefault(b => b.Id == blockedDayId);
            if (blockedDay != null)
            {
                DataContext.BlockedDays.Remove(blockedDay);
                return base.Save(blockedDay);
            }
            return new OperationStatus() { Success = false };
        }

        public string GetHolidayNameIfExists(int clinicId, DateTime date)
        {
            var dateAddedDay = date.AddDays(1);
            var holiday = DataContext.Holidays.FirstOrDefault(h => h.ClinicId == clinicId &&
                ((!h.HasHolidayEve && h.StartDay <= date && date <= h.EndDay) || (h.HasHolidayEve && h.StartDay <= dateAddedDay && dateAddedDay <= h.EndDay)));
            return holiday != null ? holiday.Name : null;
        }

        public List<CalenderEntry> GetShownClientsOnDayCalenderEntries(int activeClinicId, DateTime date)
        {
            return DataContext.CalenderEntries.Include(c => c.Doctor).Include(c => c.Client)
                 .Where(c => c.Client.ClinicId == activeClinicId && c.Date.Year == date.Year && c.Date.Month == date.Month && c.Date.Day == date.Day && c.IsNoShow.HasValue && !c.IsNoShow.Value)
                 .ToList();
        }

        public int GetBlockedDayIdForDoc(int activeClinicId, int doctorId, DateTime parsedDate)
        {
            var blockedDay =
                DataContext.BlockedDays.FirstOrDefault(
                    b => b.DoctorId == doctorId && b.ClinicId == activeClinicId && (parsedDate.Day == b.DayDate.Day &&
                                                                                    parsedDate.Month == b.DayDate.Month &&
                                                                                    parsedDate.Year == b.DayDate.Year));
            return blockedDay != null ? blockedDay.Id : 0;
        }

        public List<BlockedDay> GetBlockedDaysOfDoc(int clinicId, int doctorId)
        {
            return DataContext.BlockedDays.Where(b => b.DoctorId == doctorId && b.ClinicId == clinicId).ToList();
        }

        public bool IsDateRangeBlockedForDoctor(int clinicId, int doctorId, DateTime parsedDateStart, DateTime parsedDateEnd, int? calenderEntryId)
        {
            if (calenderEntryId.HasValue && calenderEntryId.Value > 0)
            {
                var calenderEntry = GetCalenderEntry(calenderEntryId.Value);
                if (calenderEntry != null && (calenderEntry.Date.Date == parsedDateStart.Date && calenderEntry.Date.Date == parsedDateEnd.Date))
                {
                    return false;
                }
            }

            return DataContext.BlockedDays.FirstOrDefault(
                b => b.DoctorId == doctorId && b.ClinicId == clinicId && ((parsedDateStart.Day == b.DayDate.Day &&
                                                                           parsedDateStart.Month == b.DayDate.Month &&
                                                                           parsedDateStart.Year == b.DayDate.Year) ||
                                                                          (parsedDateEnd.Day == b.DayDate.Day &&
                                                                           parsedDateEnd.Month == b.DayDate.Month &&
                                                                           parsedDateEnd.Year == b.DayDate.Year))) != null;
        }

        public List<CalenderEntry> GetAllCalenderEntriesFromDayDate(int clinicId, DateTime date)
        {
            return DataContext.CalenderEntries
                .Include(c => c.Doctor)
                .Include("Client.Phones")
                .Include(c => c.Patient)
                .Where(c => date.Day == c.Date.Day && date.Month == c.Date.Month && date.Year == c.Date.Year && c.ClinicId == clinicId && !c.IsCancelled)
                .OrderBy(c => c.Date)
                .ToList();
        }

        public List<CalenderEntry> GetCalenderEntriesByParams(int clinicId, int? doctorId, int? clientId, int searchTimeType, bool searchCancelled, string fromDate, string toDate)
        {
            var calenderEntries = DataContext.CalenderEntries.Include(c => c.Doctor).Include(c => c.Client).Where(c => c.Client.ClinicId == clinicId && (searchCancelled || !c.IsCancelled));
            if (doctorId.HasValue && doctorId.Value > 0)
            {
                calenderEntries = calenderEntries.Where(c => c.DoctorId == doctorId.Value);
            }
            if (clientId.HasValue && clientId.Value > 0)
            {
                calenderEntries = calenderEntries.Where(c => c.ClientId == clientId.Value);
            }
            switch ((SearchTimeEnum)searchTimeType)
            {
                case SearchTimeEnum.Past:
                    calenderEntries = calenderEntries.Where(c => c.Date <= DateTime.Now);
                    break;
                case SearchTimeEnum.Future:
                    calenderEntries = calenderEntries.Where(c => c.Date >= DateTime.Now);
                    break;
                case SearchTimeEnum.BetweenDates:
                    DateTime fromD;
                    DateTime toD;
                    if (!DateTime.TryParse(toDate, out toD))
                        toD = DateTime.Now.AddYears(1);

                    if (!DateTime.TryParse(fromDate, out fromD))
                        fromD = DateTime.Now.AddYears(-1);

                    calenderEntries = calenderEntries.Where(c => c.Date >= fromD && c.Date <= toD);
                    break;
            }
            return calenderEntries.ToList();
        }

        public OperationStatus RemoveCalenderEntry(int calenderEntryId)
        {
            var calenderEntry = DataContext.CalenderEntries.SingleOrDefault(c => c.Id == calenderEntryId);
            if (calenderEntry != null)
            {
                calenderEntry.IsCancelled = true;
                //DataContext.CalenderEntries.Remove(calenderEntry); removed on issue 482
                return base.Save(calenderEntry);
            }
            return new OperationStatus() { Success = false };
        }

        public List<CalenderEntry> GetCalenderEntriesByYear(int clinicId, int year)
        {
            return DataContext.CalenderEntries.Include(c => c.Doctor).Include(c => c.Client).Where(c => c.Client.ClinicId == clinicId && c.Date.Year == year).ToList();
        }

        public List<CalenderEntry> GetNoShowAndCancelsCalenderEntriesByDateRange(int clinicId, DateTime fromDate, DateTime toDate, int ShowSelectionType)
        {
            var l = DataContext.CalenderEntries.Include(c => c.Doctor).Include(c => c.Client).Include(c => c.Patient)
                .Where(c => c.Client.ClinicId == clinicId && (fromDate <= c.Date && c.Date <= toDate) && (c.IsNoShow.HasValue && c.IsNoShow.Value || c.IsCancelled)).ToList();

            switch (ShowSelectionType)
            {
                case 1:
                    return l.FindAll(x => x.IsNoShow.HasValue && x.IsNoShow.Value);
                case 2:
                    return l.FindAll(x => x.IsCancelled);
                default:
                    return l;
            }
        }

        public OperationStatus UpdateCalenderEntry(CalenderEntry calenderEntry)
        {
            return base.Save(calenderEntry);
        }

        public string GetNextClientAppointment(int clientId)
        {
            var nextAppointment = DataContext.CalenderEntries.FirstOrDefault(c => c.ClientId == clientId && c.Date > DateTime.Now && !c.IsCancelled);

            return nextAppointment != null ? nextAppointment.Date.ToShortDateString() : "";
        }
        public string GetNextClientWatch(int PatientId)
        {
            var nexWatch = DataContext.FollowUps.FirstOrDefault(c => c.PatientId == PatientId && c.DateTime > DateTime.Now);

            return nexWatch != null ? nexWatch.DateTime.ToShortDateString() : "";
        }
        public int GetCalenderEntryId(int PatientId)
        {
            DateTime now = DateTime.Now;
            var ret = DataContext.CalenderEntries.Where(i => i.PatientId == PatientId && i.Date >= now).OrderBy(i => i.Date).FirstOrDefault();
            if(ret != null)
                return ret.Id;
            return 0;
        }
        public int GetCalenderWatchId(int PatientId)
        {
            DateTime now = DateTime.Now;
            var ret = DataContext.FollowUps.Where(i => i.PatientId == PatientId && i.DateTime >= now).OrderBy(i => i.DateTime).FirstOrDefault();
            if (ret != null)
                return ret.Id;
            return 0;
        }
    }
}
