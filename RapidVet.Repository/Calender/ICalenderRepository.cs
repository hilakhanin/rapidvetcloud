﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.FullCalender;


namespace RapidVet.Repository.Calender
{
    public interface ICalenderRepository : IDisposable
    {
        /// <summary>
        /// get calenderEntries of doctor
        /// </summary>
        /// <param name="clinicId"></param>
        /// <param name="doctorId">doctorId</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>List(CalenderEntry)</returns>
        List<CalenderEntry> GetDocsCalenderEntry(int clinicId, int doctorId , DateTime startDate , DateTime endDate, bool getCancelled = false);
        
        /// <summary>
        /// create calenderEntry
        /// </summary>
        /// <param name="calenderEntry">calenderEntry</param>
        /// <returns>OperationStatus</returns>
        OperationStatus CreateCalenderEntry(CalenderEntry calenderEntry);

        /// <summary>
        /// updates calenderEntry
        /// </summary>
        /// <param name="calenderEntry">calenderEntry object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus UpdateCalenderEntry(CalenderEntry calenderEntry);

        /// <summary>
        /// returns calenderEntry
        /// </summary>
        /// <param name="calenderEntryId">calenderEntry id</param>
        /// <returns>CalenderEntry</returns>
        CalenderEntry GetCalenderEntry(int calenderEntryId, bool includeExtraEntities = false);

        /// <summary>
        /// get calenderEntries of room
        /// </summary>
        /// <param name="roomId">roomId</param>
        /// <returns>List(CalenderEntry)</returns>
        List<CalenderEntry> GetRoomsCalenderEntries(int roomId);
        List<CalenderEntry> GetCalenderEntriesForClient(int clientId);
        /// <summary>
        /// add blockedDay to doctor
        /// </summary>
        /// <param name="blockedDay">blockedDay</param>
        /// <returns>List(CalenderEntry)</returns>
        OperationStatus AddBlockedDay(BlockedDay blockedDay);

        /// <summary>
        /// get blockedDays of doctor
        /// </summary>
        /// <param name="clinicId"></param>
        /// <param name="doctorId">doctorId</param>
        /// <returns>List(BlockedDay)</returns>
        List<BlockedDay> GetBlockedDaysOfDoc(int clinicId, int doctorId);

        /// <summary>
        /// return true if blocked else false
        /// </summary>
        /// <param name="clinicId"></param>
        /// <param name="doctorId"></param>
        /// <param name="parsedDateStart"></param>
        /// <param name="parsedDateEnd"></param>
        /// <param name="calenderEntryId"></param>
        /// <returns>bool</returns>
        bool IsDateRangeBlockedForDoctor(int clinicId, int doctorId, DateTime parsedDateStart, DateTime parsedDateEnd, int? calenderEntryId);

        /// <summary>
        /// get calenderEntries from day date
        /// </summary>
        /// <param name="clinicId"></param>
        /// <param name="date">date</param>
        /// <returns>List(CalenderEntry)</returns>
        List<CalenderEntry> GetAllCalenderEntriesFromDayDate(int clinicId, DateTime date);

        List<CalenderEntry> GetCalenderEntriesByParams(int clinicId, int? doctorId, int? clientId, int searchTimeType, bool searchCancelled, string fromDate, string toDate);

        OperationStatus RemoveCalenderEntry(int calenderEntryId);

        List<CalenderEntry> GetCalenderEntriesByYear(int clinicId, int year);

        List<CalenderEntry> GetNoShowAndCancelsCalenderEntriesByDateRange(int clinicId, DateTime fromDate, DateTime toDate, int ShowSelectionType);

        OperationStatus RemoveBlockedDay(int blockedDayId);

        string GetHolidayNameIfExists(int clinicId, DateTime date);

        List<CalenderEntry> GetShownClientsOnDayCalenderEntries(int activeClinicId, DateTime date);

        int GetBlockedDayIdForDoc(int activeClinicId, int doctorId, DateTime parsedDate);
        
        string GetNextClientAppointment(int clientId);

        string GetNextClientWatch(int PatientId);

        int GetCalenderEntryId(int PatientId);

        int GetCalenderWatchId(int PatientId);
    }
}