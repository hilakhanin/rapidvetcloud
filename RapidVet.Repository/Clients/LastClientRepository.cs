﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using RapidVet.Model.Clients;
using System.Linq;

namespace RapidVet.Repository.Clients
{
    class LastClientRepository : RepositoryBase<RapidVetDataContext>, ILastClientRepository
    {
        public LastClientRepository(RapidVetDataContext context):base(context)
        {
                
        }

        public IQueryable<LastClient> GetLastClients(int userId, int clinicId, int items = 10)
        {
            return DataContext.LastClients.Where(l => l.UserId == userId && l.ClinicId == clinicId).Include(l=>l.Client).OrderByDescending(l => l.ViewDate).Take(items);
        }


        public void SetViewClient(int userID, int clinicId, int clientId)
        {

            foreach (var item in DataContext.LastClients.Where(l => l.UserId == userID && l.ClientId == clientId).ToList())
            {
                DataContext.LastClients.Remove(item);
            }

            var lastClient = new LastClient()
                {
                    UserId = userID,
                    ClientId = clientId,
                    ClinicId = clinicId,
                    ViewDate = DateTime.Now
                };
            DataContext.LastClients.Add(lastClient);
            DataContext.SaveChanges();
        }
    }
}