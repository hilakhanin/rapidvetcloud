﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model.Clients;

namespace RapidVet.Repository.Clients
{
   public interface ILastClientRepository
    {
       /// <summary>
       /// gets the last 10 clients for user
       /// </summary>
       /// <param name="userId"> user id</param>
       /// <param name="clinicId">clinic id</param>
       /// <param name="items"> num of items to return</param>
        /// <returns>IQueryable<LastClient></returns>
        IQueryable<LastClient> GetLastClients(int userId , int clinicId , int items = 10);

       /// <summary>
       /// set client for view
       /// </summary>
       /// <param name="userID"> user id</param>
       /// <param name="clinicId">clinic id</param>
       /// <param name="clientId">client id</param>
        void SetViewClient(int userID, int clinicId, int clientId);
    }
}
