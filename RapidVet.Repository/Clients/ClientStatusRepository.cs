﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clients;

namespace RapidVet.Repository.Clients
{
    public class ClientStatusRepository : RepositoryBase<RapidVetDataContext>, IClientStatusRepository
    {

        public ClientStatusRepository()
        {

        }

        public ClientStatusRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }
        /// <summary>
        /// return all client statuses in the clinic Group
        /// </summary>
        /// <param name="clinicGroupId"> clinic group id</param>
        /// <returns>IQueryable(ClientStatus)</returns>
        public IQueryable<ClientStatus> GetAll(int clinicGroupId)
        {
            return
                DataContext.ClientStatuses.Where(s => s.ClinicGroupId == clinicGroupId && s.Active).OrderBy(s => s.Name);
        }

        /// <summary>
        /// creates a clientStatus object in the clinic group (saving to db)
        /// </summary>
        /// <param name="clientStatus">clientStatus object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Create(ClientStatus clientStatus)
        {
            DataContext.ClientStatuses.Add(clientStatus);
            return base.Save(clientStatus);
        }

        /// <summary>
        /// gets a specific clientStatus object
        /// </summary>
        /// <param name="id"> id in clientStatuses table</param>
        /// <returns>ClientStatus</returns>
        public ClientStatus GetItem(int id)
        {
            return DataContext.ClientStatuses.Single(c => c.Id == id);
        }

        /// <summary>
        /// updates an entry in clientStatuses table in the db
        /// </summary>
        /// <param name="clientStatus">clientStatus object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(ClientStatus clientStatus)
        {
            return base.Save(clientStatus);
        }

        public ClientStatus GetStatusByName(int clinicGroupId, string status)
        {
            return DataContext.ClientStatuses.SingleOrDefault(c => c.ClinicGroupId == clinicGroupId && c.Name.Equals(status));
        }
    }
}
