﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clients;

namespace RapidVet.Repository.Clients
{
    public interface IClientStatusRepository : IDisposable
    {
        /// <summary>
        /// return all client statuses in the clinic Group
        /// </summary>
        /// <param name="clinicGroupId"> clinic group id</param>
        /// <returns>IQueryable(ClientStatus)</returns>
        IQueryable<ClientStatus> GetAll(int clinicGroupId);

        /// <summary>
        /// creates a clientStatus object in the clinic group (saving to db)
        /// </summary>
        /// <param name="clientStatus">clientStatus object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(ClientStatus clientStatus);

        /// <summary>
        /// gets a specific clientStatus object
        /// </summary>
        /// <param name="id"> id in clientStatuses table</param>
        /// <returns>ClientStatus</returns>
        ClientStatus GetItem(int id);

        /// <summary>
        /// updates an entry in clientStatuses table in the db
        /// </summary>
        /// <param name="clientStatus">clientStatus object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(ClientStatus clientStatus);
        ClientStatus GetStatusByName(int clinicGroupId, string status);
    }
}
