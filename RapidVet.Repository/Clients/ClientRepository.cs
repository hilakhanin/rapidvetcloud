﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Enums.Finances;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Clients.Details;
using System.Data.Entity;
using RapidVet.Model.Finance;
using RapidVet.Model.Patients;
using RapidVet.Model.Visits;
using RapidVet.Repository.Clinics;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels;
using System.Web;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Data.Entity.SqlServer;

namespace RapidVet.Repository.Clients
{
    public class ClientRepository : RepositoryBase<RapidVetDataContext>, IClientRepository
    {
        private const string baseUrl = "http://www.israelpost.co.il/zip_data.nsf/";
        private static Encoding hebrew = Encoding.GetEncoding("Windows-1255");

        public ClientRepository()
        {

        }

        public ClientRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }
        /// <summary>
        /// returns all clients in clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>IQueryable(Client)</returns>
        public IQueryable<Client> GetAllClients(int clinicId)
        {
            return DataContext.Clients.Include(c => c.ClientStatus)
                .Where(c => c.ClinicId == clinicId && !c.ClientStatus.Name.Equals(Resources.Client.ClientStatusInactive)).OrderBy(c => c.LastName);
        }


        /// <summary>
        /// gets all titles from the titles table (mr, mrs, dr...)
        /// </summary>
        /// <returns> List(Title)</returns>
        public List<Title> GetAllTitles()
        {
            return DataContext.Titles.OrderBy(t => t.Name).ToList();
        }


        /// <summary>
        /// gets all phone types from phoneTypes table
        /// </summary>
        /// <returns>List(PhoneType)</returns>
        public List<PhoneType> GetPhoneTypes()
        {
            return DataContext.PhoneTypes.OrderBy(p => p.Name).ToList();
        }

        /// <summary>
        /// gets all cities from the Cities table
        /// </summary>
        /// <returns>List(City)</returns>
        public List<City> GetCities(string term = null)
        {
            return DataContext.Cities.Where(x => term == null || x.Name.Contains(term)).Include(c => c.RegionalCouncil).Where(c => c.Active).OrderBy(c => c.Name).ToList();
        }

        public List<SelectListItem> GetCitiesOfClinicById(int clinicId)
        {
            var clinicClientIds = DataContext.Clients.Where(x => x.ClinicId == clinicId).Select(c => c.Id).ToList();
            var clinicCities = DataContext.Addresses.Where(a => clinicClientIds.Contains(a.ClientId) && a.CityId > 0).Select(a => a.CityId).Distinct().ToList();

            return DataContext.Cities.Where(a => clinicCities.Contains(a.Id)).Select(a => new SelectListItem()
            {
                Text = a.Name.Trim(),
                Value = SqlFunctions.StringConvert((double)a.Id).Trim()
            }).OrderBy(a => a.Text).ToList();
        }

        /// <summary>
        /// creates a new client in the db
        /// </summary>
        /// <param name="client"> Client entity object</param>
        /// <returns>OperationStatus object</returns>
        public OperationStatus Create(Client client)
        {
            client.Id = DataContext.Clients.Max(x => x.Id) + 1;
            DataContext.Clients.Add(client);
            DataContext.SaveChanges();
            var status = base.Save(client);
            if (status.Success)
            {
                status.ItemId = client.Id;
            }
            return status;

        }

        /// <summary>
        /// verifies if the client belongs to the clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <param name="clientId">client id</param>
        /// <returns>boolean</returns>
        public bool IsClientInClinic(int clinicId, int clientId)
        {
            return DataContext.Clients.Single(c => c.Id == clientId).ClinicId == clinicId;
        }


        /// <summary>
        /// returns object of type client containing all relevant info
        /// </summary>
        /// <param name="id"> client id</param>
        /// <returns>client</returns>
        public Client GetClient(int id)
        {
            if (id == 0)
                return null;
            return
                DataContext.Clients.Include(c => c.Title)
                           .Include(c => c.ClientStatus)
                           .Include(c => c.Clinic)
                           .Include(c => c.Addresses)
                           .Include("Addresses.City")
                           .Include(c => c.Emails)
                           .Include(c => c.Phones)
                           .Include("Phones.PhoneType")
                           .Include(c => c.ClientStatus)
                           .Include(c => c.Patients)
                           .Single(c => c.Id == id);
        }


        /// <summary>
        /// saves (updates) a client object to db
        /// </summary>
        /// <param name="client">client object</param>
        /// <param name="emails">list of email objects from view</param>
        /// <param name="phones">list of phone objects from view</param>
        /// <param name="addresses">list of address objects from view</param>
        /// <returns>operationStatus</returns>
        public OperationStatus Update(Client client, List<Email> emails, List<Phone> phones, List<Address> addresses)
        {
            var clientEmails = DataContext.Emails.Where(e => e.ClientId == client.Id).ToList();

            //update or delete emails
            foreach (var clientEmail in clientEmails)
            {
                var toRemove = emails.SingleOrDefault(e => e.Id == clientEmail.Id) == null;
                if (toRemove)
                {
                    DataContext.Emails.Remove(clientEmail);
                }
                else
                {
                    var email = emails.SingleOrDefault(e => e.Id == clientEmail.Id);
                    if (email != null && !string.IsNullOrWhiteSpace(email.Name) && email.Name != clientEmail.Name)
                    {
                        clientEmail.Name = email.Name;
                    }
                }
            }

            //add new emails
            foreach (var email in emails.Where(email => email.Id == 0))
            {
                email.ClientId = client.Id;
                DataContext.Emails.Add(email);
            }


            var clientAddresses = DataContext.Addresses.Where(a => a.ClientId == client.Id);

            //update or delete addresses
            foreach (var clientAddress in clientAddresses)
            {
                var toRemove = addresses.SingleOrDefault(a => a.Id == clientAddress.Id) == null;
                if (toRemove)
                {
                    DataContext.Addresses.Remove(clientAddress);
                }
                else
                {
                    var address = addresses.SingleOrDefault(e => e.Id == clientAddress.Id);
                    if (address != null)
                    {
                        clientAddress.CityId = address.CityId;
                        clientAddress.Street = address.Street;
                        clientAddress.ZipCode = address.ZipCode;
                    }
                }
            }


            //add new addresses
            foreach (var address in addresses.Where(address => address.Id == 0))
            {
                address.ClientId = client.Id;
                DataContext.Addresses.Add(address);
            }

            var clientPhones = DataContext.Phones.Where(p => p.ClientId == client.Id);

            //update or delete phones
            foreach (var clientPhone in clientPhones)
            {
                var toRemove = phones.SingleOrDefault(p => p.Id == clientPhone.Id) == null;
                if (toRemove)
                {
                    DataContext.Phones.Remove(clientPhone);
                }
                else
                {
                    var phone = phones.SingleOrDefault(p => p.Id == clientPhone.Id);
                    if (phone != null)
                    {
                        clientPhone.PhoneNumber = phone.PhoneNumber;
                        clientPhone.PhoneTypeId = phone.PhoneTypeId;
                    }
                }
            }

            //add new phones
            foreach (var phone in phones.Where(phone => phone.Id == 0))
            {
                phone.ClientId = client.Id;
                DataContext.Phones.Add(phone);
            }

            var status = new OperationStatus() { Success = false };
            status.Success = DataContext.SaveChanges() > -1;
            return status;

        }

        public int GetClientTariffId(int clientId)
        {
            var client = DataContext.Clients.Single(c => c.Id == clientId);
            return client.TariffId == null
                               ? DataContext.Clinics.Single(c => c.Id == client.ClinicId).DefualtTariffId
                               : client.TariffId.Value;
        }


        public int GetClientTariffIdFromPatientId(int patientId)
        {
            var clientId = DataContext.Patients.Single(p => p.Id == patientId).ClientId;
            return GetClientTariffId(clientId);
        }

        public IEnumerable<Client> GetAllClientsIncludingForInactive(int clinicId)
        {
            return DataContext.Clients.Include(c => c.Phones).Include(c => c.Patients).Include("Addresses.City").Include(c => c.ClientStatus)
                .Where(c => c.ClinicId == clinicId && !c.ClientStatus.Name.Equals(Resources.Client.ClientStatusInactive));
        }

        public OperationStatus DeactivateClient(int clientId)
        {
            var client = DataContext.Clients.Include(c => c.ClientStatus).Include(c => c.Clinic).SingleOrDefault(c => c.Id == clientId);
            if (client == null)
            {
                return new OperationStatus() { Success = false };
            }
            else
            {
                //client.Active = false;
                using (var clientStatusRepo = new ClientStatusRepository())
                {
                    var clientStatus = clientStatusRepo.GetStatusByName(client.Clinic.ClinicGroupID, Resources.Client.ClientStatusInactive);
                    if (clientStatus != null)
                    {
                        client.ClientStatus = clientStatus;
                    }
                }
                return Save(client);
            }
        }

        public decimal GetClientBalance(int clientId)
        {
            decimal result = 0;
            var patients = DataContext.Patients.Include(p => p.Visits).Where(p => p.ClientId == clientId);
            foreach (var p in patients.Where(p => p.Visits.Any()))
            {
                var temp = p.Visits.Sum(v => v.Price);
                if (temp.Value > 0)
                {
                    result = result - temp.Value;
                }
            }
            return result;
        }

        public string GetClientIdCard(int clientId)
        {
            return DataContext.Clients.Single(c => c.Id == clientId).IdCard;
        }

        public Client GetClientWithFinanceDocuments(int id)
        {
            return
               DataContext.Clients.Include(c => c.Title)
                          .Include(c => c.ClientStatus)
                          .Include(c => c.Addresses)
                          .Include("Addresses.City")
                          .Include(c => c.Emails)
                          .Include(c => c.Phones)
                          .Include("Phones.PhoneType")
                          .Include(c => c.ClientStatus)
                          .Include(c => c.Patients)
                          .Include(c => c.FinanceDocuments)
                          .Single(c => c.Id == id);
        }


        public void UpdateClientBalance(Client client)
        {
            client.Balance = GetClientBalance(client);
        }

        private decimal GetClientBalance(Client client)
        {
            decimal sum = 0;

            var openVisitsSum = DataContext.Visits.Where(v => v.ClientIdAtTimeOfVisit == client.Id && v.Active && !v.DebtVisit.HasValue).Sum(v => v.Price);
            if (openVisitsSum.HasValue)
            {
                sum -= openVisitsSum.Value;
            }

            var recipts =
                DataContext.FinanceDocuments.Where(
                    d =>
                    d.ClientId == client.Id &&
                    (d.FinanceDocumentTypeId == (int)FinanceDocumentType.Receipt ||
                     d.FinanceDocumentTypeId == (int)FinanceDocumentType.InvoiceReceipt ||
                     d.FinanceDocumentTypeId == (int)FinanceDocumentType.Refound));
            foreach (var financeDocument in recipts)
            {
                sum += financeDocument.TotalSum;
            }
            //get relevant internal settlements...
            var settlements = DataContext.InternalSettlements.Where(s => s.ClientId == client.Id);
            if (settlements.Any())
            {
                var settlementsSum = settlements.Sum(s => s.Sum);
                sum += settlementsSum;
            }

            if (client.Balance != sum)
            {
                client.Balance = sum;
                DataContext.SaveChanges();
            }


            return sum;
        }

        public decimal GetClientCurrentBalance(Client client)
        {
            return GetClientBalance(client);
        }

        //public void UpdateAllClinicClientsBalance(int clinicId)
        //{
        //    var clients = DataContext.Clients.Where(c => c.ClinicId == clinicId);
        //    foreach (var client in clients)
        //    {
        //        UpdateClientBalance(client);
        //    }
        //    DataContext.SaveChanges();
        //}

        public List<Client> GetClinetsForHS(int clinicId)
        {
            return DataContext.Clients.Include(c => c.Phones).Include(c => c.Emails).Include("Addresses.City").Where(c => c.ClinicId == clinicId).ToList();
        }

        public DateTime GetLastVisitDate(int clientId)
        {
            var clientPatientsIds = DataContext.Patients.Where(p => p.ClientId == clientId).Select(p => p.Id).ToList();
            var visits =
                DataContext.Visits.Where(v => clientPatientsIds.Contains(v.PatientId.Value))
                           .OrderByDescending(v => v.VisitDate)
                           .Take(1);
            var result = DateTime.MinValue;
            if (visits.Any())
            {
                result = visits.First().VisitDate;
            }
            return result;
        }

        public Client GetClientFromPatientId(int id)
        {
            var patient =
                DataContext.Patients.Include(p => p.Client)
                           .Include("Client.Title")
                           .Include("Client.ClientStatus")
                           .Include("Client.Addresses")
                           .Include("Client.Emails")
                           .Include("Client.Phones")
                           .Include("Client.Phones.PhoneType")
                           .Include("Client.ClientStatus")
                           .Single(p => p.Id == id);
            return patient.Client;
        }

        public IQueryable<Address> GetAllClientAddresses(int clientId)
        {
            return DataContext.Addresses.Include(a => a.City).Where(a => a.ClientId == clientId).OrderByDescending(a => a.Id);
        }



        /// <summary>
        /// returns a single email
        /// </summary>
        /// <param name="emailId">id in email table</param>
        /// <returns>Email object</returns>
        public Email GetEmail(int emailId)
        {
            return DataContext.Emails.Single(e => e.Id == emailId);
        }

        /// <summary>
        /// returns a phone object
        /// </summary>
        /// <param name="phoneId">id in phones table</param>
        /// <returns>Phone object</returns>
        public Phone GetPhone(int phoneId)
        {
            return DataContext.Phones.Single(p => p.Id == phoneId);
        }

        /// <summary>
        /// get address from addresses table
        /// </summary>
        /// <param name="addressId">id in addresses table</param>
        /// <returns>Address object</returns>
        public Address GetAddress(int addressId)
        {
            return DataContext.Addresses.Single(a => a.Id == addressId);
        }


        /// <summary>
        /// searches for a client in clients table
        /// </summary>
        /// <param name="query"> client first and/or last name</param>
        /// <returns>IEnumerable<Client></returns>
        public IEnumerable<ClientSearchResult> Search(string query, int clinicId)
        {
            var allClinicsClients = DataContext.Clients.Where(c =>
               c.ClinicId == clinicId);

            var clinicsClients = allClinicsClients.Select(c => new ClientSearchResult { value = c.Id, label = c.FirstName + " " + c.LastName + " - " + (c.PatientString != null ? c.PatientString : "") });

            var clients = allClinicsClients.Where(c =>

           (
               //c.FirstName.StartsWith(query) ||
               //c.LastName.StartsWith(query) ||

                    c.IdCard.StartsWith(query) ||
                    c.Phones.Any(p => p.PhoneNumber.StartsWith(query))// ||                    
               //c.Patients.Any(p => p.Name.StartsWith(query)) ||
               //c.PatientNames.Contains(query)
                    )
                    ).Select(c => new ClientSearchResult { value = c.Id, label = c.FirstName + " " + c.LastName + " - " + (c.PatientString != null ? c.PatientString : "") });//.OrderBy(c => c.LastName).ThenBy(c => c.FirstName);

            var clientsWithPatients = clinicsClients.Where(c => c.label.Contains(query));

            return clients.Union(clientsWithPatients).OrderBy(c => c.label);

        }

        public OperationStatus SetClientNameWithPatients(int clientId, bool save = true)
        {

            var client =
                DataContext.Clients.Include(c => c.Patients)
                           .FirstOrDefault(c => c.Id == clientId);

            if (client != null)// && client.Patients.Any(p => p.Active))
            {
                var patientNames = GetClientPatientName(client);
                client.PatientNames = patientNames;
                client.UpdatedDate = DateTime.Now;
                return base.Save(client);
            }
            return new OperationStatus();
        }

        public string GetClientPatientName(Client client)
        {
            var result = string.Empty;
            var patientNames = string.Empty;
            var counter = 0;
            client.PatientNames = string.Empty;

            foreach (var p in client.Patients.OrderBy(p => p.Name).Select(p => p.Name))
            {
                if (!string.IsNullOrWhiteSpace(p))
                {
                    patientNames = string.Format("{0},{1}", patientNames, p);
                    //if (counter == 2)
                    //{
                    //    result = result.TrimEnd(',');
                    //    result = string.Format("{0}...", result);
                    //    client.PatientString = result;
                    //}
                    //else if (counter < 3)
                    //{
                    //    result = string.Format("{0},{1}", result, p);
                    //}
                    //counter++;
                }
            }
            patientNames = patientNames.Trim();
            result = patientNames.TrimEnd(',');
            result = patientNames.TrimStart(',');

            if (client.PatientString != result)
            {
                client.PatientString = result;//.TrimEnd(',');
            }

            return result;
        }

        public int? GetClientIdFromHsId(int clinicId, int hsId)
        {
            var client = DataContext.Clients.SingleOrDefault(c => c.ClinicId == clinicId && c.HsId == hsId);
            int? result = null;
            if (client != null)
            {
                result = client.Id;
            }
            return result;
        }

        public Client GetClientFromHsId(int clinicId, int hsId)
        {
            return
                DataContext.Clients.Include(c => c.Patients)
                           .SingleOrDefault(c => c.ClinicId == clinicId && c.HsId == hsId);
        }

        public City GetCity(int cityId)
        {
            return DataContext.Cities.SingleOrDefault(c => c.Id == cityId);
        }

        public OperationStatus CreateCity(City city)
        {
            DataContext.Cities.Add(city);
            return base.Save(city);
        }

        public OperationStatus UpdateCity(City city)
        {
            return base.Save(city);
        }

        public OperationStatus RemoveCity(int cityId)
        {
            var city = DataContext.Cities.SingleOrDefault(c => c.Id == cityId);
            if (city != null)
            {
                city.Active = false;
                return base.Save(city);
            }
            return new OperationStatus() { Success = false };
        }

        public List<AdvancedSearchWebModel> AdvancedSearch(int clinicGroupId, int clinicId, string firstName, string lastName, string idCard, string email, string phone, int cityId, string clientStatus,
            string street, string patientName, int animalKindId, string patientLicense, string chip, string sagir, bool includeInactivePatients, string templatePatientsURL, string templateClientsURL,
            bool searchByContaining)
        {
            IQueryable<AdvancedSearchWebModel> filteredClients;
            if (searchByContaining)
            {
                int clientStatusID = clientStatus == null ? -1 : Convert.ToInt32(clientStatus);
                firstName = String.IsNullOrWhiteSpace(firstName) ? null : firstName.Trim();
                lastName = String.IsNullOrWhiteSpace(lastName) ? null : lastName.Trim();
                idCard = String.IsNullOrWhiteSpace(idCard) ? null : idCard.Trim();
                email = String.IsNullOrWhiteSpace(email) ? null : email.Trim();
                phone = String.IsNullOrWhiteSpace(phone) ? null : phone.Replace("-", "").Trim();
                street = String.IsNullOrWhiteSpace(street) ? null : street.Trim();
                patientName = String.IsNullOrWhiteSpace(patientName) ? null : patientName.Trim();
                patientLicense = String.IsNullOrWhiteSpace(patientLicense) ? null : patientLicense.Trim();
                chip = String.IsNullOrWhiteSpace(chip) ? null : chip.Trim();
                sagir = String.IsNullOrWhiteSpace(sagir) ? null : sagir.Trim();
                templatePatientsURL = templatePatientsURL.Substring(0, templatePatientsURL.Length - 1);
                templateClientsURL = templateClientsURL.Substring(0, templateClientsURL.Length - 1);
                filteredClients = from c in DataContext.Clients
                                  join cg in DataContext.ClinicGroups on clinicGroupId equals cg.Id
                                  from mail in DataContext.Emails.Where(m => m.ClientId == c.Id).Take(1).DefaultIfEmpty()
                                  from p in DataContext.Patients.Where(m => m.ClientId == c.Id).DefaultIfEmpty()
                                  //  from ph in DataContext.Phones.Where(m => m.ClientId == c.Id).DefaultIfEmpty()
                                  from adrs in DataContext.Addresses.Where(m => m.ClientId == c.Id).OrderBy(a => a.Id).Take(1).DefaultIfEmpty()
                                  from ct in DataContext.Cities.Where(m => m.Id == adrs.CityId).DefaultIfEmpty()
                                  from ar in DataContext.AnimalRaces.Where(m => m.Id == p.AnimalRaceId).Take(1).DefaultIfEmpty()
                                  from ak in DataContext.AnimalKinds.Where(m => m.Id == ar.AnimalKindId).DefaultIfEmpty()
                                  let ph = DataContext.Phones.Where(pq => pq.ClientId == c.Id && pq.PhoneNumber != null).Select(pq => pq.PhoneNumber)//.AsEnumerable().Aggregate((a, b) => a + ", " + b)//GetAllClientPhonesString(c.Id)
                                  where
                                  c.ClinicId == clinicId &&
                                  (cityId < 1 || (adrs != null && adrs.CityId == cityId)) &&
                                  (animalKindId < 1 || (ak != null && ak.Id == animalKindId)) &&
                                  (patientName == null || (p != null && p.Name.Contains(patientName))) &&
                                  (patientLicense == null || (p != null && p.LicenseNumber == patientLicense)) &&
                                  (chip == null || (p != null && p.ElectronicNumber == chip)) &&
                                  (sagir == null || (p != null && p.SAGIRNumber == sagir)) &&
                                  (street == null || (adrs != null && adrs.Street.Contains(street))) &&
                                  (email == null || (mail != null && mail.Name.StartsWith(email))) &&
                                      //(phone == null || (ph != null && ph.Contains(phone))) &&
                                  (firstName == null || c.FirstName.Contains(firstName)) &&
                                  (lastName == null || c.LastName.Contains(lastName)) &&
                                  (idCard == null || c.IdCard.Equals(idCard)) &&
                                  (clientStatusID == -1 || c.StatusId == clientStatusID) &&
                                  (includeInactivePatients || p == null || p.Active)
                                  select new AdvancedSearchWebModel()
                                  {
                                      ClientId = c.Id,
                                      FirstName = c.FirstName,
                                      LastName = c.LastName,
                                      IdCard = c.IdCard,
                                      AllPatientNames = p == null ? null : p.Name,
                                      PatientId = p == null ? -1 : p.Id,
                                      ClientUrl = templateClientsURL + System.Data.Entity.SqlServer.SqlFunctions.StringConvert((double)c.Id).Trim(),
                                      PatientUrl = p == null ? null : templatePatientsURL + System.Data.Entity.SqlServer.SqlFunctions.StringConvert((double)p.Id).Trim(),
                                      ClientUrlVisible = p == null,
                                      Phones = ph.ToList(),
                                      //Phone = p == null ? null : ph,
                                      Address = adrs == null ? null : (ct == null ? (adrs.Street ?? "") : (adrs.Street ?? "") + " " + ct.Name),
                                      CityId = adrs == null ? -1 : adrs.CityId,
                                      Street = adrs == null ? null : adrs.Street,
                                      AnimalKind = ak == null ? null : ak.Value,
                                      AnimalKindId = ak == null ? -1 : ak.Id
                                      //ClientStatus = cs == null ? null : cs.Name
                                  };
            }
            else
            {
                int clientStatusID = clientStatus == null ? -1 : Convert.ToInt32(clientStatus);
                firstName = String.IsNullOrWhiteSpace(firstName) ? null : firstName.Trim();
                lastName = String.IsNullOrWhiteSpace(lastName) ? null : lastName.Trim();
                idCard = String.IsNullOrWhiteSpace(idCard) ? null : idCard.Trim();
                email = String.IsNullOrWhiteSpace(email) ? null : email.Trim();
                phone = String.IsNullOrWhiteSpace(phone) ? null : phone.Replace("-", "").Trim();
                street = String.IsNullOrWhiteSpace(street) ? null : street.Trim();
                patientName = String.IsNullOrWhiteSpace(patientName) ? null : patientName.Trim();
                patientLicense = String.IsNullOrWhiteSpace(patientLicense) ? null : patientLicense.Trim();
                chip = String.IsNullOrWhiteSpace(chip) ? null : chip.Trim();
                sagir = String.IsNullOrWhiteSpace(sagir) ? null : sagir.Trim();
                templatePatientsURL = templatePatientsURL.Substring(0, templatePatientsURL.Length - 1);
                templateClientsURL = templateClientsURL.Substring(0, templateClientsURL.Length - 1);
                filteredClients = from c in DataContext.Clients
                                  join cg in DataContext.ClinicGroups on clinicGroupId equals cg.Id
                                  from mail in DataContext.Emails.Where(m => m.ClientId == c.Id).Take(1).DefaultIfEmpty()
                                  from p in DataContext.Patients.Where(m => m.ClientId == c.Id).DefaultIfEmpty()
                                  // from ph in DataContext.Phones.Where(m => m.ClientId == c.Id).DefaultIfEmpty()
                                  from adrs in DataContext.Addresses.Where(m => m.ClientId == c.Id).OrderBy(a => a.Id).Take(1).DefaultIfEmpty()
                                  from ct in DataContext.Cities.Where(m => m.Id == adrs.CityId).DefaultIfEmpty()
                                  from ar in DataContext.AnimalRaces.Where(m => m.Id == p.AnimalRaceId).Take(1).DefaultIfEmpty()
                                  from ak in DataContext.AnimalKinds.Where(m => m.Id == ar.AnimalKindId).DefaultIfEmpty()
                                  let ph = DataContext.Phones.Where(pq => pq.ClientId == c.Id && pq.PhoneNumber != null).Select(pq => pq.PhoneNumber)//.AsEnumerable().Aggregate((a, b) => a + ", " + b)//GetAllClientPhonesString(c.Id)
                                  where
                                  c.ClinicId == clinicId &&
                                  (cityId < 1 || (adrs != null && adrs.CityId == cityId)) &&
                                  (animalKindId < 1 || (ak != null && ak.Id == animalKindId)) &&
                                  (patientName == null || (p != null && p.Name.StartsWith(patientName))) &&
                                  (patientLicense == null || (p != null && p.LicenseNumber == patientLicense)) &&
                                  (chip == null || (p != null && p.ElectronicNumber == chip)) &&
                                  (sagir == null || (p != null && p.SAGIRNumber == sagir)) &&
                                  (street == null || (adrs != null && adrs.Street.Contains(street))) &&
                                  (email == null || (mail != null && mail.Name.StartsWith(email))) &&
                                      //  (phone == null || (ph != null && ph.PhoneNumber.Contains(phone))) &&
                                  (firstName == null || c.FirstName.StartsWith(firstName)) &&
                                  (lastName == null || c.LastName.StartsWith(lastName)) &&
                                  (idCard == null || c.IdCard.Equals(idCard)) &&
                                  (clientStatusID == -1 || c.StatusId == clientStatusID) &&
                                  (includeInactivePatients || p == null || p.Active)
                                  select new AdvancedSearchWebModel()
                                  {
                                      ClientId = c.Id,
                                      FirstName = c.FirstName,
                                      LastName = c.LastName,
                                      IdCard = c.IdCard,
                                      AllPatientNames = p == null ? null : p.Name,
                                      PatientId = p == null ? -1 : p.Id,
                                      ClientUrl = templateClientsURL + System.Data.Entity.SqlServer.SqlFunctions.StringConvert((double)c.Id).Trim(),
                                      PatientUrl = p == null ? null : templatePatientsURL + System.Data.Entity.SqlServer.SqlFunctions.StringConvert((double)p.Id).Trim(),
                                      ClientUrlVisible = p == null,
                                      Phones = ph.ToList(),
                                      //Phone = p == null ? null : ph,
                                      Address = adrs == null ? null : (ct == null ? (adrs.Street ?? "") : (adrs.Street ?? "") + " " + ct.Name),
                                      CityId = adrs == null ? -1 : adrs.CityId,
                                      Street = adrs == null ? null : adrs.Street,
                                      AnimalKind = ak == null ? null : ak.Value,
                                      AnimalKindId = ak == null ? -1 : ak.Id
                                      //ClientStatus = cs == null ? null : cs.Name
                                  };
            }

            var result = filteredClients.ToList().FindAll(x => phone == null || (x.Phones != null && x.Phones.Count > 0 && x.Phones.Contains(phone))).ToList();
            foreach (var item in result)
            {
                var l = item.Phones;
                if (l == null || l.Count == 0)
                    continue;

                item.Phone = String.Join(", ", l);
            }

            return result;
        }

        public List<ClientReportItem> GetFilteredClients(ClientFilter filter, int clinicId)
        {
            var patients =
              DataContext.Patients
              .Include("Client.Vet")
              .Include("AnimalRace.AnimalKind")
              .Include("AnimalColor")
              .Include("Client.Phones")
              .Include("Client.Addresses.City")
              .Include("Client.Patients")
              .Include("Client.Emails")
              .Include("Client.CalenderEntries")
              .Include("Comments")
              .Include("Visits.Treatments")
              .Include("Client.FinanceDocuments")
              .Include("Client.ClientStatus")
              .Include("Client.Title")
              .Where(v => v.Client.ClinicId == clinicId);
            //.Where(v => !v.Client.ClientStatus.Name.Equals(Resources.Client.ClientStatusInactive) && v.Client.ClinicId == clinicId);

            var patientlessClients = DataContext.Clients
              .Include("Vet")
              .Include("Phones")
              .Include("Addresses.City")
              .Include("Patients")
              .Include("Emails")
              .Include("FinanceDocuments")
              .Include("ClientStatus")
              .Include("Title")
              .Include("CalenderEntries")
              .Include(c => c.ClientStatus)
              .Where(v => v.ClinicId == clinicId && !v.Patients.Any());
            //.Where(v => !v.ClientStatus.Name.Equals(Resources.Client.ClientStatusInactive) && v.ClinicId == clinicId && !v.Patients.Any());

            //ClientStatus Filter
            if (!string.IsNullOrEmpty(filter.ClientStatusId))
            {
                var idsList = filter.ClientStatusId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();

                patients = patients.Where(p => idsList.Any(s => s == p.Client.StatusId));
                patientlessClients = patientlessClients.Where(p => idsList.Any(s => s == p.StatusId));
            }

            //InactivePatients Filter
            if (filter.OnlyWithInactivePatients)
            {
                patients = patients.Where(p => p.Client.Patients.Count(patientOfClient => !patientOfClient.Active) > 0);
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();

                //Added to show only inactive patients
                patients = patients.Where(p => !p.Active);
            }

            //PatientMoreThan Filter
            if (filter.OnlyWithNumPatientsMoreThan.HasValue)
            {
                patients = patients.Where(p => p.Client.Patients.Count() > filter.OnlyWithNumPatientsMoreThan.Value);
                patientlessClients = patientlessClients.Where(p => p.Patients.Count() > filter.OnlyWithNumPatientsMoreThan.Value);

            }

            //City Filter
            if (!string.IsNullOrEmpty(filter.CityId))
            {
                var idsList = filter.CityId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();
                //show only client who's  first address is contained in the city list
                
                patients = patients.Where(p => p.Client.Addresses.Count() > 0 && idsList.Contains(p.Client.Addresses.FirstOrDefault().CityId));
                patientlessClients = patientlessClients.Where(p => p.Addresses.Count() > 0 && idsList.Contains(p.Addresses.FirstOrDefault().CityId));
            }

            //Email Keyword Filter
            if (!string.IsNullOrWhiteSpace(filter.EmailKeyWord))
            {
                patients = patients.Where(v => v.Client.Emails.Any(e => e.Name.Contains(filter.EmailKeyWord)));
                patientlessClients = patientlessClients.Where(v => v.Emails.Any(e => e.Name.Contains(filter.EmailKeyWord)));
            }

            //Client CreatedDate Filter
            if (!string.IsNullOrWhiteSpace(filter.CreatedFrom) && !string.IsNullOrWhiteSpace(filter.CreatedTo))
            {
                var fromDate = StringUtils.ParseStringToDateTime(filter.CreatedFrom);
                var toDate = StringUtils.ParseStringToDateTime(filter.CreatedTo).AddDays(1);

                patients = patients.Where(p => fromDate <= p.Client.CreatedDate && p.Client.CreatedDate <= toDate);
                patientlessClients = patientlessClients.Where(p => fromDate <= p.CreatedDate && p.CreatedDate <= toDate);
            }

            //Client VisitedDate Filter
            if (!string.IsNullOrWhiteSpace(filter.VisitedFrom) && !string.IsNullOrWhiteSpace(filter.VisitedTo))
            {
                var fromDate = StringUtils.ParseStringToDateTime(filter.VisitedFrom);
                var toDate = StringUtils.ParseStringToDateTime(filter.VisitedTo).AddDays(1);
                patients = patients.Where(p => p.Visits.Any(v => fromDate <= v.VisitDate && v.VisitDate <= toDate));
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //Client Age Filter
            if (!string.IsNullOrWhiteSpace(filter.AgeFrom) && !string.IsNullOrWhiteSpace(filter.AgeTo))
            {
                var birthFromDate = StringUtils.ParseStringToDateTime(filter.AgeFrom);
                var birthToDate = StringUtils.ParseStringToDateTime(filter.AgeTo).AddDays(1);

                patients = patients.Where(v => v.Client.BirthDate.HasValue && birthToDate <= v.Client.BirthDate.Value && v.Client.BirthDate.Value <= birthFromDate);
                patientlessClients = patientlessClients.Where(v => v.BirthDate.HasValue && birthToDate <= v.BirthDate.Value && v.BirthDate.Value <= birthFromDate);
            }

            //TreatingDoc Filter
            if (filter.TreatingDoctorId.HasValue && filter.TreatingDoctorId.Value > 0)
            {
                patients = patients.Where(p => p.Client.VetId.HasValue && p.Client.VetId.Value == filter.TreatingDoctorId.Value);
                patientlessClients = patientlessClients.Where(p => p.VetId.HasValue && p.VetId.Value == filter.TreatingDoctorId.Value);
            }

            //ExternalVet Keyword Filter
            if (!string.IsNullOrWhiteSpace(filter.ExternalVetKeyWord))
            {
                patients = patients.Where(v => v.Client.ExternalVet.Contains(filter.ExternalVetKeyWord));
                patientlessClients = patientlessClients.Where(v => v.ExternalVet.Contains(filter.ExternalVetKeyWord));
            }

            //Future Appointments Filter
            if (filter.OnlyWithOrWithoutFutureAppointment.HasValue)
            {
                patients = filter.OnlyWithOrWithoutFutureAppointment.Value ?
                    patients.Where(v => v.Client.CalenderEntries.Any(c => c.Date >= DateTime.Now)) : patients.Where(v => !v.Client.CalenderEntries.Any(c => c.Date >= DateTime.Now));

                patientlessClients = filter.OnlyWithOrWithoutFutureAppointment.Value ?
                    patientlessClients.Where(v => v.CalenderEntries.Any(c => c.Date >= DateTime.Now)) : patientlessClients.Where(v => !v.CalenderEntries.Any(c => c.Date >= DateTime.Now));
            }

            //Reference Filter
            if (filter.NoReference.HasValue)
            {
                if (filter.NoReference.Value)
                {
                    patients = patients.Where(v => v.Client.ReferredBy == null);
                    patientlessClients = patientlessClients.Where(v => v.ReferredBy == null);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(filter.ReferredByKeyWord))
                    {
                        patients = patients.Where(v => v.Client.ReferredBy.Contains(filter.ReferredByKeyWord));
                        patientlessClients = patientlessClients.Where(v => v.ReferredBy.Contains(filter.ReferredByKeyWord));
                    }
                }
            }

            //Client NoVisitsFrom Filter
            if (!string.IsNullOrWhiteSpace(filter.OnlyWithNoVisitFrom))
            {
                var fromDate = StringUtils.ParseStringToDateTime(filter.OnlyWithNoVisitFrom);
                patients = patients.Where(p => !p.Visits.Any(v => fromDate <= v.VisitDate));
                // patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //AnimalKind Id Filter
            if (!string.IsNullOrEmpty(filter.AnimalKindId))
            {
                var idsList = filter.AnimalKindId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();
                patients = patients.Where(p => idsList.Any(a => a == p.AnimalRace.AnimalKindId));
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //AnimalColor Id Filter
            if (!string.IsNullOrEmpty(filter.AnimalColorId))
            {
                var idsList = filter.AnimalColorId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();
                patients = patients.Where(p => idsList.Any(a => a == p.AnimalColorId));
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //PatientActiveStatus Id Filter
            if (filter.PatientActiveStatus.HasValue && filter.PatientActiveStatus > 0)
            {
                patients = filter.PatientActiveStatus == 1 ? patients.Where(p => p.Active) : patients.Where(p => !p.Active);
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //AnimalRace Id Filter
            if (filter.AnimalRaceId.HasValue && filter.AnimalRaceId.Value > 0)
            {
                patients = patients.Where(p => p.AnimalRaceId == filter.AnimalRaceId.Value);
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //PureBred Filter
            if (filter.PureBred)
            {
                patients = patients.Where(p => p.PureBred);
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //Patient Gender Filter
            if (filter.AnimalGender.HasValue && filter.AnimalGender.Value > 0)
            {
                patients = patients.Where(p => p.GenderId == filter.AnimalGender.Value);
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //Sterilized Filter
            if (filter.Sterilized.HasValue)
            {
                patients = filter.Sterilized.Value ? patients.Where(p => p.Sterilization) : patients.Where(p => !p.Sterilization);
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //Fertilized Filter
            if (filter.Fertilized.HasValue)
            {
                patients = filter.Fertilized.Value ? patients.Where(p => p.Fertilization) : patients.Where(p => !p.Fertilization);
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //patient Age Filter
            if (!string.IsNullOrWhiteSpace(filter.PatientAgeFrom) && !string.IsNullOrWhiteSpace(filter.PatientAgeTo))
            {
                var birthFromDate = StringUtils.ParseStringToDateTime(filter.PatientAgeFrom);
                var birthToDate = StringUtils.ParseStringToDateTime(filter.PatientAgeTo).AddDays(1);

                patients = patients.Where(v => v.BirthDate.HasValue && birthToDate <= v.BirthDate.Value && v.BirthDate.Value <= birthFromDate);
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //Client TreatedDate Filter
            if (!string.IsNullOrWhiteSpace(filter.TreatedFrom) && !string.IsNullOrWhiteSpace(filter.TreatedTo))
            {
                var fromDate = StringUtils.ParseStringToDateTime(filter.TreatedFrom);
                var toDate = StringUtils.ParseStringToDateTime(filter.TreatedTo);
                patients = patients.Where(p => p.Visits.Where(v => v.Treatments.Any()).Any(v => fromDate <= v.VisitDate && v.VisitDate <= toDate));
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //Client NoTreatmentsFrom Filter
            if (!string.IsNullOrWhiteSpace(filter.OnlyWithNoTreatmentFrom))
            {
                var fromDate = StringUtils.ParseStringToDateTime(filter.OnlyWithNoTreatmentFrom);
                patients = patients.Where(p => !p.Visits.Any(v => v.Treatments.Any() && fromDate <= v.VisitDate));
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //OnlyNotVaccined Filter
            if (filter.OnlyNotVaccined)
            {
                patients = patients.Where(p => p.PreventiveMedicine.All(pm => pm.PriceListItemTypeId != (int)PriceListItemTypeEnum.Vaccines));
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //OnlyWithElectronicNumber Filter
            if (filter.OnlyWithElectornicNumber)
            {
                patients = patients.Where(p => p.ElectronicNumber != null);
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //OnlyGoneThroughTreatment Filter
            if (filter.OnlyGoneThroughTreatment.HasValue)
            {
                if (filter.TreatmentId.HasValue && filter.TreatmentId > 0)
                {
                    patients = filter.OnlyGoneThroughTreatment.Value
                                   ? patients.Where(p => p.Visits.Any(v => v.Treatments.Any(t => t.PriceListItemId == filter.TreatmentId.Value)))
                                   : patients.Where(p => !p.Visits.Any(v => v.Treatments.Any(t => t.PriceListItemId == filter.TreatmentId.Value)));
                }
                patientlessClients = Enumerable.Empty<Client>().AsQueryable();
            }

            //Balance Filter
            if (filter.OnlyWithPositiveBalance.HasValue)
            {
                patients = filter.OnlyWithPositiveBalance.Value ? patients.Where(p => p.Client.Balance >= 0) : patients.Where(p => p.Client.Balance < 0);
                patientlessClients = filter.OnlyWithPositiveBalance.Value ? patientlessClients.Where(p => p.Balance >= 0) : patientlessClients.Where(p => p.Balance < 0);
            }

            //Balance Range Filter
            if (filter.FromBalance.HasValue && filter.ToBalance.HasValue)
            {
                var minVal = Math.Min(filter.FromBalance.Value, filter.ToBalance.Value);
                var maxVal = Math.Max(filter.FromBalance.Value, filter.ToBalance.Value);

                patients = patients.Where(p => minVal <= p.Client.Balance && p.Client.Balance <= maxVal);
                patientlessClients = patientlessClients.Where(p => minVal <= p.Balance && p.Balance <= maxVal);
            }

            //Tariif Filter
            if (filter.PriceListId.HasValue && filter.PriceListId > 0)
            {
                patients = patients.Where(p => p.Client.TariffId.HasValue && p.Client.TariffId == filter.PriceListId.Value);
                patientlessClients = patientlessClients.Where(p => p.TariffId.HasValue && p.TariffId == filter.PriceListId.Value);
            }

            //Payments Filter
            if (filter.PaymentsNumMoreThan.HasValue &&
                (!string.IsNullOrWhiteSpace(filter.PaymentFrom) && !string.IsNullOrWhiteSpace(filter.PaymentTo)))
            {
                var fromDate = StringUtils.ParseStringToDateTime(filter.PaymentFrom);
                var toDate = StringUtils.ParseStringToDateTime(filter.PaymentTo);

                patients = patients.Where(p => p.Client.FinanceDocuments
                                                .Count(
                                                    d =>
                                                    d.FinanceDocumentTypeId == (int)FinanceDocumentType.Receipt &&
                                                    (fromDate <= d.Created && d.Created <= toDate))
                                               > filter.PaymentsNumMoreThan.Value);

                patientlessClients = patientlessClients.Where(p => p.FinanceDocuments
                                                                    .Count(
                                                                        d =>
                                                                        d.FinanceDocumentTypeId ==
                                                                        (int)FinanceDocumentType.Receipt &&
                                                                        (fromDate <= d.Created && d.Created <= toDate))
                                                                   > filter.PaymentsNumMoreThan.Value);

            }

            //Global Filters
            if (filter.OnlyClientsWithoutVisits)
            {
                patients = patients.Where(p => !p.Client.Patients.Any(cp => cp.Visits.Any()));
            }
            if (filter.OnlyNonFilteredClients)
            {
                var allPatients = DataContext.Patients
                                             .Include("Client.Vet")
                                             .Include("AnimalRace.AnimalKind")
                                             .Include("AnimalColor")
                                             .Include("Client.Phones")
                                             .Include("Client.Addresses.City")
                                             .Include("Client.Patients")
                                             .Include("Client.Emails")
                                             .Include("Comments")
                                             .Include("Visits.Treatments")
                                             .Include("Client.FinanceDocuments")
                                             .Include("Client.ClientStatus")
                                             .Include("Client.Title")
                                             .Where(v => v.Client.ClinicId == clinicId);
                var tmpPatients = patients;
                patients = allPatients.Where(p => !tmpPatients.Contains(p));

                var allPatientlessClients = DataContext.Clients
                                                       .Include("Vet")
                                                       .Include("Phones")
                                                       .Include("Addresses.City")
                                                       .Include("Patients")
                                                       .Include("Emails")
                                                       .Include("FinanceDocuments")
                                                       .Include("ClientStatus")
                                                       .Include("Title")
                                                       .Where(v => v.ClinicId == clinicId && !v.Patients.Any());
                var tmpPatientlessClients = patientlessClients;
                patientlessClients = allPatientlessClients.Where(p => !tmpPatientlessClients.Contains(p));
            }

            //Arrange Data
            var reports = new List<ClientReportItem>();
            using (var letterTemplatesRepository = new LetterTemplatesRepository())
            {
                foreach (var p in patients)
                {
                    var reportItem = new ClientReportItem();
                    reportItem.clientId = p.Client.Id;
                    reportItem.idCard = p.Client.IdCard;
                    reportItem.workPhone = letterTemplatesRepository.GetClientPhone(p.Client.Phones, PhoneTypeEnum.Work);
                    reportItem.homePhone = letterTemplatesRepository.GetClientPhone(p.Client.Phones, PhoneTypeEnum.Home);
                    reportItem.cellPhone = letterTemplatesRepository.GetClientPhone(p.Client.Phones, PhoneTypeEnum.Mobile);
                    reportItem.address = letterTemplatesRepository.GetClientFirstAddress(p.Client.Addresses);
                    reportItem.client_city = letterTemplatesRepository.GetClientCity(p.Client.Addresses);
                    reportItem.client_street_and_number = letterTemplatesRepository.GetClientStreetAndNumber(p.Client.Addresses);
                    reportItem.zipcode = letterTemplatesRepository.GetClientZipCode(p.Client.Addresses);
                    reportItem.treatingDoc = (p.Client.Vet != null) ? string.Format("{0} {1}", p.Client.Vet.FirstName, p.Client.Vet.LastName) : string.Empty;
                    reportItem.email = letterTemplatesRepository.GetClientEmail(p.Client.Emails);
                    reportItem.localId = p.Client.LocalId;
                    reportItem.referedBy = p.Client.ReferredBy;
                    reportItem.clientStatus = p.Client.StatusId.HasValue ? p.Client.ClientStatus.Name : string.Empty;
                    reportItem.clientTitle = p.Client.TitleId.HasValue ? p.Client.Title.Name : string.Empty;
                    reportItem.clientName = string.Format("{0} {1}", p.Client.FirstName, p.Client.LastName);
                    reportItem.clientFirstName = p.Client.FirstName;
                    reportItem.clientLastName = p.Client.LastName;
                    reportItem.clientCreateDate = p.Client.CreatedDate;
                    reportItem.clientDetailsUpdatedDate = p.Client.UpdatedDate;
                    if (p.Client.BirthDate.HasValue)
                    {
                        AgeHelper age = new AgeHelper(p.Client.BirthDate.Value, DateTime.Today);
                        reportItem.clientAge = age.Years + "." + age.Months;
                        reportItem.client_birthdate = p.Client.BirthDate.Value.ToShortDateString();
                    }
                    else
                    {
                        reportItem.clientAge = "";
                        reportItem.client_birthdate = "";
                    }
                    // reportItem.clientAge = p.Client.BirthDate.HasValue ? (int?)((DateTime.Now - p.Client.BirthDate.Value).Days / (decimal)365.242199) : null;
                    reportItem.balance = p.Client.Balance;
                    reportItem.patientName = p.Name;
                    reportItem.animalKind = p.AnimalRace.AnimalKind.Value;
                    reportItem.animalRace = p.AnimalRace.Value;
                    reportItem.animalGender = p.GenderId == 1
                                                  ? (Resources.Gender.Male)
                                                  : (p.GenderId == 2
                                                         ? Resources.Gender.Female
                                                         : Resources.Gender.UndefinedSex);
                    reportItem.externalVet = p.Client.ExternalVet;
                    reportItem.animalColor = p.AnimalColorId.HasValue ? p.AnimalColor.Value : string.Empty;
                    if (p.BirthDate.HasValue)
                    {
                        // reportItem.animalAge = ((DateTime.Now - p.BirthDate.Value).Days / (decimal)365.0);AgeHelper age = new AgeHelper(p.Client.BirthDate.Value, DateTime.Today);
                        AgeHelper age = new AgeHelper(p.BirthDate.Value, DateTime.Today);
                        reportItem.animalAge = age.Years + "." + age.Months;
                    }
                    else
                    {
                        reportItem.animalAge = "";
                    }
                    reportItem.pureBred = p.PureBred ? Resources.Global.Yes : Resources.Global.No;
                    reportItem.sterilized = p.Sterilization ? Resources.Global.Yes : Resources.Global.No;
                    reportItem.electronicNum = p.ElectronicNumber;
                    reportItem.SAGIRNum = p.SAGIRNumber;
                    reportItem.licenseNum = p.LicenseNumber;
                    reportItem.owner_farm = p.Owner_Farm;

                    //CreateCommentsString
                    var commentsList = p.Comments.Select(c => c.CommentBody + " ").ToArray();
                    reportItem.patientComments = commentsList.Any() ? commentsList.Aggregate((a, b) => a + ',' + b) : string.Empty;

                    //Calc treatment and weight
                    var visits = p.Visits.Where(v => v.PatientId == p.Id).OrderByDescending(v => v.VisitDate);
                    if (visits.Any())
                    {
                        reportItem.patientWeight = visits.First().Weight.ToString();
                        foreach (var visit in visits.Where(visit => visit.Treatments.Any()))
                        {
                            reportItem.lastTreatmentDate = visit.VisitDate;
                            break;
                        }
                    }

                    reports.Add(reportItem);
                }
                foreach (var p in patientlessClients)
                {
                    var reportItem = new ClientReportItem();
                    reportItem.clientId = p.Id;
                    reportItem.idCard = p.IdCard;
                    reportItem.workPhone = letterTemplatesRepository.GetClientPhone(p.Phones, PhoneTypeEnum.Work);
                    reportItem.homePhone = letterTemplatesRepository.GetClientPhone(p.Phones, PhoneTypeEnum.Home);
                    reportItem.cellPhone = letterTemplatesRepository.GetClientPhone(p.Phones, PhoneTypeEnum.Mobile);
                    reportItem.address = letterTemplatesRepository.GetClientFirstAddress(p.Addresses);
                    reportItem.client_city = letterTemplatesRepository.GetClientCity(p.Addresses);
                    reportItem.client_street_and_number = letterTemplatesRepository.GetClientStreetAndNumber(p.Addresses);
                    reportItem.zipcode = letterTemplatesRepository.GetClientZipCode(p.Addresses);
                    reportItem.treatingDoc = p.Vet != null ? p.Vet.FirstName + " " + p.Vet.LastName : string.Empty;
                    reportItem.email = letterTemplatesRepository.GetClientEmail(p.Emails);
                    reportItem.localId = p.LocalId;
                    reportItem.referedBy = p.ReferredBy;
                    reportItem.clientStatus = p.StatusId.HasValue ? p.ClientStatus.Name : string.Empty;
                    reportItem.clientTitle = p.TitleId.HasValue ? p.Title.Name : string.Empty;
                    reportItem.clientName = string.Format("{0} {1}", p.FirstName, p.LastName);
                    reportItem.clientFirstName = p.FirstName;
                    reportItem.clientLastName = p.LastName;
                    reportItem.clientCreateDate = p.CreatedDate;
                    reportItem.clientDetailsUpdatedDate = p.UpdatedDate;
                    if (p.BirthDate.HasValue)
                    {
                        AgeHelper age = new AgeHelper(p.BirthDate.Value, DateTime.Today);
                        reportItem.clientAge = age.Years + "." + age.Months;
                        reportItem.client_birthdate = p.BirthDate.Value.ToShortDateString();
                    }
                    else
                    {
                        reportItem.clientAge = "";
                        reportItem.client_birthdate = "";
                    }
                    // reportItem.clientAge = p.BirthDate.HasValue ? (int?)((DateTime.Now - p.BirthDate.Value).Days / (decimal)365.0) : null;
                    reportItem.balance = p.Balance;
                    reportItem.externalVet = p.ExternalVet;
                    reportItem.animalAge = "";

                    reports.Add(reportItem);
                }
            }

            if (filter.ShowOnlyClientInfo)
            {
                reports = reports.Distinct(new CompareByClientId()).ToList();
                foreach (var reportItem in reports)
                {
                    reportItem.patientName = string.Empty;
                    reportItem.animalKind = string.Empty;
                    reportItem.animalRace = string.Empty;
                    reportItem.animalGender = string.Empty;
                    reportItem.externalVet = string.Empty;
                    reportItem.animalColor = string.Empty;
                    reportItem.animalAge = null;
                    reportItem.pureBred = string.Empty;
                    reportItem.sterilized = string.Empty;
                    reportItem.electronicNum = string.Empty;
                    reportItem.SAGIRNum = string.Empty;
                    reportItem.licenseNum = string.Empty;
                    reportItem.owner_farm = string.Empty;
                }
            }

            return reports.OrderBy(r => r.clientName).ThenBy(r => r.patientName).ToList();

        }


        public ZipCode GetZipCodeByAddress(string city, string street)
        {
            string data = DownloadString(MikudAction.Zip, "Location={0}&Street={1}", city, street);

            return GetZipCode(data);
        }

        public ZipCode GetZipCodeByPOB(string city, string pob)
        {
            string data = DownloadString(MikudAction.Zip, "Location={0}&POB={1}", city, pob);

            return GetZipCode(data);
        }

        private string DownloadString(MikudAction action, string format, params object[] args)
        {
            var parameters = args.Select(x => HttpUtility.HtmlEncode(x)).ToArray();
            WebClient wc = new WebClient();

            string actionString = GetActionString(action);

            var result = wc.DownloadData(baseUrl + actionString + string.Format(format, parameters));

            return hebrew.GetString(result);
        }

        private static string GetActionString(MikudAction action)
        {
            string actionString = null;
            if (action == MikudAction.City)
            {
                actionString = "CreateLocationsforAutoComplete?OpenAgent&";
            }
            else if (action == MikudAction.Street)
            {
                actionString = "CreateStreetsforAutoComplete?OpenAgent&";
            }
            else if (action == MikudAction.Zip)
            {
                actionString = "SearchZip?OpenAgent&";
            }
            else if (action == MikudAction.Address)
            {
                actionString = "SearchAddress7?OpenAgent&";
            }
            return actionString;
        }

        private static ZipCode GetZipCode(string data)
        {
            var match = Regex.Match(data, @"RES[0-9]*\d");
            var result = match.Value.Substring(4);

            ZipCode zipCode = new ZipCode()
            {
                Status = true,
                Value = result
            };

            if (result == "11")
            {
                zipCode.Status = false;
                zipCode.Value = "00000";
            }
            else if (result == "12" || match.Value == "RES2")
            {
                zipCode.Status = false;
                zipCode.Value = "00000";
            }
            else if (result == "13" || match.Value == "RES013")
            {
                zipCode.Status = false;
                zipCode.Value = "00000";
            }
            else if (match.Value == "RES5")
            {
                zipCode.Status = false;
                zipCode.Value = "00000";
            }
            else if (result == "")
            {
                zipCode.Status = false;
                zipCode.Value = data;
            }
            return zipCode;
        }

        public enum MikudAction
        {
            City,
            Street,
            Zip,
            Address
        }
    }

    public class CompareByClientId : IEqualityComparer<ClientReportItem>
    {
        public bool Equals(ClientReportItem x, ClientReportItem y)
        {
            return x.clientId == y.clientId;
        }

        public int GetHashCode(ClientReportItem obj)
        {
            return 0;
        }
    }


}
