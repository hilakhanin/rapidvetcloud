﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Finance;
using RapidVet.WebModels.Clients;
using RapidVet.Model.Patients;
using RapidVet.WebModels;
using System.Web.Mvc;

namespace RapidVet.Repository.Clients
{
    public interface IClientRepository : IDisposable
    {

        /// <summary>
        /// returns all clients in clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>IQueryable(Client)</returns>
        IQueryable<Client> GetAllClients(int clinicId);

        /// <summary>
        /// gets all titles from the titles table (mr, mrs, dr...)
        /// </summary>
        /// <returns> List(Title)</returns>
        List<Title> GetAllTitles();

        /// <summary>
        /// gets all phone types from phoneTypes table
        /// </summary>
        /// <returns>List(PhoneType)</returns>
        List<PhoneType> GetPhoneTypes();

        /// <summary>
        /// gets all cities from the Cities table
        /// </summary>
        /// <returns>List(City)</returns>
        List<City> GetCities(string term = null);
        List<SelectListItem> GetCitiesOfClinicById(int clinicId);
        /// <summary>
        /// creates a new client in the db
        /// </summary>
        /// <param name="client"> Client entity object</param>
        /// <returns>OperationStatus object</returns>
        OperationStatus Create(Client client);

        /// <summary>
        /// verifies if the client belongs to the clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <param name="clientId">client id</param>
        /// <returns>boolean</returns>
        bool IsClientInClinic(int clinicId, int clientId);

        /// <summary>
        /// returns object of type client containing all relevant info
        /// </summary>
        /// <param name="id"> client id</param>
        /// <returns>client</returns>
        Client GetClient(int id);
        //  OperationStatus Update(Client client);

        /// <summary>
        /// returns a single email
        /// </summary>
        /// <param name="emailId">id in email table</param>
        /// <returns>Email object</returns>
        Email GetEmail(int emailId);

        /// <summary>
        /// returns a phone object
        /// </summary>
        /// <param name="phoneId">id in phones table</param>
        /// <returns>Phone object</returns>
        Phone GetPhone(int phoneId);

        /// <summary>
        /// get address from addresses table
        /// </summary>
        /// <param name="addressId">id in addresses table</param>
        /// <returns>Address object</returns>
        Address GetAddress(int addressId);

        /// <summary>
        /// searches for a client in clients table
        /// </summary>
        /// <param name="query"> client first and/or last name</param>
        /// <returns>IEnumerable<Client></returns>
        IEnumerable<ClientSearchResult> Search(string query, int clinicId);

        /// <summary>
        /// saves (updates) a client object to db
        /// </summary>
        /// <param name="client">client object</param>
        /// <param name="emails"></param>
        /// <param name="phones"></param>
        /// <param name="addresses"></param>
        /// <returns>operationStatus</returns>
        OperationStatus Update(Client client, List<Email> emails, List<Phone> phones, List<Address> addresses);


        /// <summary>
        /// return the tariff id for client
        /// </summary>
        /// <param name="clientId">client id</param>
        /// <returns> int tariff id</returns>
        int GetClientTariffId(int clientId);

        /// <summary>
        /// get client from patient
        /// </summary>
        /// <param name="id" patientId></param>
        /// <returns>client object</returns>
        Client GetClientFromPatientId(int id);

        /// <summary>
        /// get all client addresses
        /// </summary>
        /// <param name="clientId" client id></param>
        /// <returns> IQueryable<Address></returns>
        IQueryable<Address> GetAllClientAddresses(int clientId);

        /// <summary>
        /// get the client's tariff id from patientId
        /// </summary>
        /// <param name="patientId"> patient id</param>
        /// <returns>int tariff id</returns>
        int GetClientTariffIdFromPatientId(int patientId);

        /// <summary>
        /// get all clinic clients, active & not active
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <returns> list of clients</returns>
        IEnumerable<Client> GetAllClientsIncludingForInactive(int clinicId);

        /// <summary>
        /// deactivates client
        /// </summary>
        /// <param name="clientId">client id</param>
        /// <returns>operationstatus object</returns>
        OperationStatus DeactivateClient(int clientId);

        decimal GetClientBalance(int clientId);

        /// <summary>
        /// returns client id card
        /// </summary>
        /// <param name="clientId"client id></param>
        /// <returns> int?</returns>
        string GetClientIdCard(int clientId);



        /// <summary>
        /// returns client object w. associated financial documents
        /// </summary>
        /// <param name="clientId"client id></param>
        /// <returns> Client</returns>
        Client GetClientWithFinanceDocuments(int clientId);

        void UpdateClientBalance(Client client);

        /// <summary>
        /// returns the client current balance
        /// </summary>
        /// <param name="client">Client</param>
        /// <returns>decimal</returns>
        decimal GetClientCurrentBalance(Client client);

        //void UpdateAllClinicClientsBalance(int clinicId);

        List<Client> GetClinetsForHS(int clinicId);
        /// <summary>
        /// get last visit date for client
        /// </summary>
        /// <param name="clientId">int client id</param>
        /// <returns>date time obj</returns>
        DateTime GetLastVisitDate(int clientId);

        List<ClientReportItem> GetFilteredClients(ClientFilter filter, int clinicId);

        OperationStatus SetClientNameWithPatients(int clientId, bool save = true);

        /// <summary>
        /// get client id from client HsId
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="hsId">int hs id</param>
        /// <returns>int?</returns>
        int? GetClientIdFromHsId(int clinicId, int hsId);

        /// <summary>
        /// get client from client HsId
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="hsId">int hs id</param>
        /// <returns>Client</returns>
        Client GetClientFromHsId(int clinicId, int hsId);

        City GetCity(int cityId);
        OperationStatus CreateCity(City city);
        OperationStatus UpdateCity(City city);
        OperationStatus RemoveCity(int cityId);

        /// <summary>
        /// clients advanced search
        /// </summary>
        /// <param name="clinicGroupId"></param>
        /// <param name="clinicId"></param>
        /// <param name="firstName">client first name</param>
        /// <param name="lastName">client last name</param>
        /// <param name="idCard">client id card</param>
        /// <param name="email">client email</param>
        /// <param name="phone">client phone</param>
        /// <param name="cityId">client city id</param>
        /// <param name="street">client street</param>
        /// <param name="patientName">patient name</param>
        /// <param name="animalKindId">patient animal kind id</param>
        /// <param name="patientLicense">patient license</param>
        /// <param name="chip">patient chip</param>
        /// <param name="includeInactivePatients"></param>
        /// <returns> IQueryable<Client></returns>
        List<AdvancedSearchWebModel> AdvancedSearch(int clinicGroupId, int clinicId, string firstName, string lastName,
                                          string idCard, string email, string phone, int cityId, string clientStatus, string street,
                                          string patientName, int animalKindId, string patientLicense, string chip, string sagir,
                                          bool includeInactivePatients, string templatePatientsURL, string templateClientsURL, bool searchByContaining);

        /// <summary>
        /// calculates the client patient names string
        /// </summary>
        /// <param name="client">Client client</param>
        /// <returns>string</returns>
        string GetClientPatientName(Client client);
        ZipCode GetZipCodeByAddress(string city, string street);
    }
}
