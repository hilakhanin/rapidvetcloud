﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Model.CliinicInventory;

namespace RapidVet.Repository.ClinicInventory
{
    public interface IInventoryRepository : IDisposable
    {

        /// <summary>
        /// get all  price list items that it's category is relevant to inventory
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>IQueryable<PriceListItem></returns>
        IQueryable<PriceListItem> GetAllInventoryRelatedPriceListItems(int clinicId);

        /// <summary>
        /// get all price list items that it's category is relevant to inventory
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="inventoryActiveOnly">bool display unmanaged items</param>
        /// <returns>IQueryable<PriceListItem></returns>
        IQueryable<PriceListItem> GetInventoryRelatedPriceListItems(int clinicId, bool displayUnmanagedItems);

        /// <summary>
        /// get all active price list items that it's category is relevant to inventory
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>IQueryable<PriceListItem></returns>
        IQueryable<PriceListItem> GetInventoryActivePriceListItems(int clinicId);
        /// <summary>
        /// get all suppliers connected to this price list item
        /// </summary>
        /// <param name="priceListItemId">int price list item id</param>
        /// <returns>IQueryable<PriceListItemSupplier></returns>
        IQueryable<PriceListItemSupplier> GetPriceListItemSuppliers(int priceListItemId);

        /// <summary>
        /// update suppliers for price list item
        /// </summary>
        /// <param name="priceListItemId">int price list item id</param>
        /// <param name="priceListItemSuppliers">List<PriceListItemSupplier></param>
        bool UpdateItemSuppliers(int priceListItemId, List<PriceListItemSupplier> priceListItemSuppliers);

        /// <summary>
        /// get all orders for clinic
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>IQueryable<InventoryOrder></returns>
        IQueryable<InventoryOrder> GetAllOrdersForClinic(int clinicId);

        /// <summary>
        /// add new order
        /// </summary>
        /// <param name="order">InventoryOrder obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus CreateOrder(InventoryOrder order);

        /// <summary>
        /// get inventory order by id
        /// </summary>
        /// <param name="orderId">int order id</param>
        /// <returns>InventoryOrder</returns>
        InventoryOrder GetOrder(int orderId);

        /// <summary>
        /// add item to existing order
        /// </summary>
        /// <param name="orderItem">InventoryPriceListItemOrder</param>
        /// <returns>OperationStatus</returns>
        void AddOrderItem(InventoryPriceListItemOrder orderItem);

        /// <summary>
        /// create new inventortItem
        /// </summary>
        /// <param name="inventoryItem">Inventory obj</param>
        void CreateInventoryItem(Inventory inventoryItem);

        /// <summary>
        /// get all inventory items for price list item id
        /// </summary>
        /// <param name="clinicId"> in clinic id</param>
        /// <param name="priceListItemId">int price list item id</param>
        /// <returns> IQueryable<Inventory></returns>
        IQueryable<Inventory> GetInventoryItems(int clinicId, int priceListItemId);


        /// <summary>
        /// get incentory items form pricelistitemid in clinic by dates
        /// </summary>
        /// <param name="clinicId"> int clinic id</param>
        /// <param name="priceListItemId">int price list item id</param>
        /// <param name="fromDate"> datetime from</param>
        /// <param name="toDate"> datetime to</param>
        /// <returns>  IQueryable<Inventory></returns>
        IQueryable<Inventory> GetInventoryItems(int clinicId, int? priceListItemId, DateTime fromDate, DateTime toDate);

        /// <summary>
        /// checks if user has inventory role
        /// </summary>
        /// <param name="clinicGroupId">int clinic group id</param>
        /// <param name="roleName"> string role name</param>
        /// <param name="userId"></param>
        /// <returns>bool</returns>
        bool IsUserInInventoryRole(int clinicGroupId, string roleName, int userId);

        void RemoveOrder(int orderId);
        
        /// <summary>
        /// gets priceListItem price by supplier
        /// </summary>
        /// <param name="priceListItemId">int priceListItem id</param>
        /// <param name="supplierId">int supplier Id</param>
        /// <returns>decimal</returns>
        decimal GetPriceListItemSupplierPrice(int priceListItemId, int supplierId);

        IQueryable<Inventory> GetInventoryUpdatesSum (int priceListItemId);
    }
}
