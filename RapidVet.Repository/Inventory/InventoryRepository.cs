﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Model.CliinicInventory;

namespace RapidVet.Repository.ClinicInventory
{
    public class InventoryRepository : RepositoryBase<RapidVetDataContext>, IInventoryRepository
    {
        public InventoryRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public InventoryRepository()
        {

        }

        public IQueryable<PriceListItem> GetAllInventoryRelatedPriceListItems(int clinicId)
        {
            var inventoryCategoryIds =
                DataContext.PriceListCategories.Where(c => c.ClinicId == clinicId && c.IsInventory)
                           .Select(c => c.Id);
            return
                DataContext.PriceListItems
                           .Include(i => i.Category)
                           .Include(i => i.Suppliers)
                           .Include("Suppliers.Supplier")
                           .Where(i => inventoryCategoryIds.Contains(i.CategoryId) && i.InventoryActive)
                           .OrderBy(i => i.Category.Name)
                           .ThenBy(i => i.Name);
        }

        public IQueryable<PriceListItem> GetInventoryRelatedPriceListItems(int clinicId, bool displayUnmanagedItems)
        {
            var inventoryCategoryIds =
                DataContext.PriceListCategories.Where(c => c.Available && c.ClinicId == clinicId && c.IsInventory)
                           .Select(c => c.Id);

            var items =
                 DataContext.PriceListItems
                 .Include(i => i.Category)
                 .Include(i => i.Suppliers)
                 .Include("Suppliers.Supplier")
                 .Where(i => inventoryCategoryIds.Contains(i.CategoryId) && i.Available); //&& i.InventoryActive)
                            
            if(!displayUnmanagedItems)
            {
               items = items.Where(i => i.InventoryActive);
            }

            return items.OrderBy(i => i.Category.Name)
                            .ThenBy(i => i.Name); 
        }

        public IQueryable<PriceListItem> GetInventoryActivePriceListItems(int clinicId)
        {
            var inventoryCategoryIds =
                DataContext.PriceListCategories.Where(c => c.Available && c.ClinicId == clinicId && c.IsInventory)
                           .Select(c => c.Id);

            return
                 DataContext.PriceListItems
                 .Include(i => i.Category)
                 .Include(i => i.Suppliers)
                 .Include("Suppliers.Supplier")
                 .Where(i => inventoryCategoryIds.Contains(i.CategoryId) && i.Available && i.InventoryActive)
                            .OrderBy(i => i.Category.Name)
                            .ThenBy(i => i.Name);
        }

        public IQueryable<PriceListItemSupplier> GetPriceListItemSuppliers(int priceListItemId)
        {
            return
                DataContext.PriceListItemSuppliers.Where(s => s.PriceListItemId == priceListItemId)
                           .Include(s => s.Supplier);
        }

        public bool UpdateItemSuppliers(int priceListItemId, List<PriceListItemSupplier> priceListItemSuppliers)
        {
            var current = DataContext.PriceListItemSuppliers.Where(s => s.PriceListItemId == priceListItemId);

            foreach (var item in current.ToList())
            {
                DataContext.PriceListItemSuppliers.Remove(item);
            }

            foreach (var item in priceListItemSuppliers)
            {
                DataContext.PriceListItemSuppliers.Add(item);
            }

            return DataContext.SaveChanges() > -1;
        }

        public IQueryable<InventoryOrder> GetAllOrdersForClinic(int clinicId)
        {
            return
                DataContext.InventoryOrders
                           .Include(o => o.Items)
                           .Include("Items.PriceListItem")
                           .Include(o => o.Supplier)
                           .Where(o => o.ClinicId == clinicId)
                           .OrderByDescending(o => o.Created)
                           .ThenBy(o => o.Recived);
        }

        public OperationStatus CreateOrder(InventoryOrder order)
        {
            DataContext.InventoryOrders.Add(order);
            return base.Save(order);
        }

        public InventoryOrder GetOrder(int orderId)
        {
            return
                DataContext.InventoryOrders.Include(o => o.Supplier)
                           .Include(o => o.Items)
                           .Include(o => o.Supplier)
                           .Include("Items.PriceListItem.Category")
                           .Single(o => o.Id == orderId);
        }

        public void AddOrderItem(InventoryPriceListItemOrder orderItem)
        {
            DataContext.InventoryOrderItems.Add(orderItem);
        }

        public void CreateInventoryItem(Inventory inventoryItem)
        {
            DataContext.Inventory.Add(inventoryItem);
        }

        public IQueryable<Inventory> GetInventoryItems(int clinicId, int priceListItemId)
        {
            return DataContext.Inventory.Where(i => i.ClinicId == clinicId && i.PriceListItemId == priceListItemId);
        }

        public IQueryable<Inventory> GetInventoryItems(int clinicId, int? priceListItemId, DateTime fromDate, DateTime toDate)
        {
            var data = DataContext.Inventory.Include(i => i.User).Include(i => i.PriceListItem).Where(i => i.ClinicId == clinicId);

            if (priceListItemId.HasValue && priceListItemId.Value > 0)
            {
                data = data.Where(i => i.PriceListItemId == priceListItemId);
            }

            if (fromDate > DateTime.MinValue)
            {
                data = data.Where(i => i.Created >= fromDate);
            }

            if (toDate > DateTime.MinValue)
            {
                toDate = toDate.AddDays(1);
                data = data.Where(i => i.Created < toDate);
            }

            return data;
        }

        public bool IsUserInInventoryRole(int clinicGroupId, string roleName, int userId)
        {
            var clinicIds = DataContext.Clinics.Where(c => c.ClinicGroupID == clinicGroupId).Select(c => c.Id);
            var userRoles = DataContext.UsersClinicsRoles.Where(u => u.UserId == userId && clinicIds.Contains(u.ClinicId));
            var role = DataContext.Roles.Single(r => r.RoleName == roleName);
            var isInventoryRole = userRoles.SingleOrDefault(r => r.RoleId == role.RoleId) != null;
            var user = DataContext.Users.Include(u => u.Roles).Single(u => u.Id == userId);
            var isAdmin = user.IsAdministrator;
            return isInventoryRole || isAdmin;

        }

        public void RemoveOrder(int orderId)
        {
            var order = DataContext.InventoryOrders.SingleOrDefault(o => o.Id == orderId);
            if (order != null)
            {
                DataContext.InventoryOrders.Remove(order);
            }
        }

        public decimal GetPriceListItemSupplierPrice(int priceListItemId, int supplierId)
        {
            var data =
                DataContext.PriceListItemSuppliers.FirstOrDefault(
                    p => p.SupplierId == supplierId && p.PriceListItemId == priceListItemId);
            return data != null ? data.CostPrice : 0;
        }

        public IQueryable<Inventory> GetInventoryUpdatesSum(int priceListItemId)
        {
            //decimal result = 0;
            return
                DataContext.Inventory.Where(
                    i => i.PriceListItemId == priceListItemId && i.VisitId == null && i.OrderId == null);
        }

       
    }
}
