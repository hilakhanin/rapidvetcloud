﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.DischargePapers
{
    public interface IDischargePaperRepository : IDisposable
    {
        /// <summary>
        /// get all discharge papers for patient
        /// </summary>
        /// <param name="id">patient id</param>
        /// <returns> IQueryable<DischargePaper></returns>
        IQueryable<DischargePaper> GetDischargePaperForPatient(int id);

        /// <summary>
        /// get discharge paper element
        /// </summary>
        /// <param name="paperId"> discharge paper id</param>
        /// <returns>DischargePaper</returns>
        DischargePaper GetDischargePaper(int paperId);

        /// <summary>
        /// creates new dischargepaper
        /// </summary>
        /// <param name="dischargePaper">DischargePaper</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(DischargePaper dischargePaper);

        /// <summary>
        /// get discharge papers for clinic for a certaine date range
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <param name="fromDate">dateTime from</param>
        /// <param name="toDate">dateTime to</param>
        /// <returns> IQueryable<DischargePaper></returns>
        IQueryable<DischargePaper> GetDischargePapers(int clinicId, DateTime fromDate, DateTime toDate);

        /// <summary>
        /// deletes discharge paper from db
        /// </summary>
        /// <param name="dischargePaper">DischargePaper</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(DischargePaper dischargePaper);
    }
}
