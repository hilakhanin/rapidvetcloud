﻿using System;
using System.Linq;
using System.Data.Entity;
using RapidVet.Model;
using RapidVet.Model.Patients;
using RapidVet.Repository.MinistryOfAgricultureReports;

namespace RapidVet.Repository.DischargePapers
{
    public class DischargePaperRepository : RepositoryBase<RapidVetDataContext>, IDischargePaperRepository
    {
        public DischargePaperRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public DischargePaperRepository()
        {

        }

        public IQueryable<DischargePaper> GetDischargePaperForPatient(int id)
        {
            return DataContext.DischargePapers.Where(m => m.PatientId == id);
        }

        public DischargePaper GetDischargePaper(int paperId)
        {
            return DataContext.DischargePapers.Single(d => d.Id == paperId);
        }

        public OperationStatus Create(DischargePaper dischargePaper)
        {
            DataContext.DischargePapers.Add(dischargePaper);
            return base.Save(dischargePaper);
        }

        public IQueryable<DischargePaper> GetDischargePapers(int clinicId, DateTime fromDate, DateTime toDate)
        {
            var clinicPatientIds = DataContext.Patients.Where(p => p.Client.ClinicId == clinicId).Select(p => p.Id);
            toDate = toDate.AddDays(1);
            return
                DataContext.DischargePapers.Include(dp => dp.Patient)
                           .Include("Patient.Client")
                           .Where(
                               dp =>
                               dp.DateTime >= fromDate && dp.DateTime < toDate &&
                               clinicPatientIds.Contains(dp.PatientId));
        }

        public OperationStatus Delete(DischargePaper dischargePaper)
        {
            DataContext.DischargePapers.Remove(dischargePaper);
            return base.Save(dischargePaper);
        }
    }
}
