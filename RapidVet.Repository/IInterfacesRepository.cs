﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model.Tools;

namespace RapidVet.Repository
{
    public interface IInterfacesRepository : IDisposable
    {
        /// <summary>
        /// get specific Interface obj by name
        /// </summary>
        /// <param name="name">string inteface name</param>       
        /// <returns>Interface</returns>
        Interface GetInterfaceByName(string name);       
    }
}
