﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Enums.Finances;
using RapidVet.Model;
using RapidVet.Model.Archives;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.DataMigration;
using RapidVet.Model.Expenses;
using RapidVet.Model.Finance;
using RapidVet.Model.FullCalender;
using RapidVet.Model.InternalSettlements;
using RapidVet.Model.CliinicInventory;
using RapidVet.Model.PatientFollowUp;
using RapidVet.Model.PatientLabTests;
using RapidVet.Model.Patients;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Quotations;
using RapidVet.Model.TreatmentPackages;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;
using RapidVet.Repository.Interfaces;
using RapidVet.Model.Auditings;
using System.Data.Entity.ModelConfiguration.Conventions;
using RapidVet.Model.Tools;
using RapidVet.Model.ClinicGroups;

namespace RapidVet.Repository
{
    /// <summary>
    /// a class that is in charge of creating the DataBase at first run.
    /// holds all the DB table enteties that exist in the project.
    /// </summary>
    public class RapidVetDataContext : DbContext, IDisposedTracker
    {

        public bool IsDisposed { get; set; }

        //Google
        public DbSet<Model.Google.GoogleToken> GoogleTokens { get; set; }
        public DbSet<Model.Google.GoogleSettings> GoogleSettings { get; set; }
        public DbSet<Model.Google.ClientGoogleIDs> ClientGoogleIDs { get; set; }
        public DbSet<Model.Google.GoogleReports> GoogleSyncReports { get; set; }

        //Clients
        public DbSet<Model.Clients.Client> Clients { get; set; }
        public DbSet<Model.Clients.ClientComment> ClientComments { get; set; }
        public DbSet<Model.Clients.Title> Titles { get; set; }
        public DbSet<Model.Clients.ClientStatus> ClientStatuses { get; set; }
        public DbSet<Model.Clients.Details.City> Cities { get; set; }
        public DbSet<Model.Clients.LastClient> LastClients { get; set; }

        //Clinics
        public DbSet<Model.Clinics.Clinic> Clinics { get; set; }
        public DbSet<Model.Clinics.ClinicGroup> ClinicGroups { get; set; }
        public DbSet<Model.Clinics.Issuer> Issuers { get; set; }
        public DbSet<IssuerCreditType> IssuerCreditTypes { get; set; }
        public DbSet<Model.Clinics.TelephoneDirectory> TelephoneDirectories { get; set; }
        public DbSet<Model.Clinics.TelephoneDirectoryType> TelephoneDirectoryTypes { get; set; }
        public DbSet<Model.Clinics.VaccineOrTreatment> VaccineAndTreatments { get; set; }
        public DbSet<Model.Clinics.NextVaccineOrTreatment> NextVaccineAndTreatments { get; set; }
        public DbSet<Model.Clinics.LabTestType> LabTestTypes { get; set; }
        public DbSet<Model.Clinics.LabTestTemplate> LabTestsTemplates { get; set; }
        public DbSet<Model.Clinics.LabTest> LabTests { get; set; }
        public DbSet<Model.Clinics.LetterTemplate> LetterTemplates { get; set; }
        public DbSet<RegionalCouncil> RegionalCouncils { get; set; }
        public DbSet<ClinicBackgroundJob> ClinicBackgroundJobs { get; set; }

        public DbSet<TaxRate> TaxRates { get; set; }
        public DbSet<AdvancedPaymentsPercent> AdvancedPaymentsPercents { get; set; }

        public DbSet<AttendanceLog> AttendanceLogs { get; set; }
        public DbSet<DefaultCardCompanies> DefaultCardCompanies { get; set; }

        public DbSet<MessageDistributionCount> MessageDistribution { get; set; }
        public DbSet<UserExtendedPermissions> UserExtendedPermissions { get; set; }
        
        //Quotations
        public DbSet<TreatmentPackage> TreatmentPackages { get; set; }
        public DbSet<PackageItem> PackageItems { get; set; }
        public DbSet<QuotationTreatment> QuotationTreatments { get; set; }
        public DbSet<Quotation> Quotations { get; set; }


        //Details
        public DbSet<Model.Clients.Details.Address> Addresses { get; set; }
        public DbSet<Model.Clients.Details.Email> Emails { get; set; }
        public DbSet<Model.Clients.Details.Phone> Phones { get; set; }
        public DbSet<Model.Clients.Details.PhoneType> PhoneTypes { get; set; }


        //Patients
        public DbSet<Model.Patients.AnimalColor> AnimalColors { get; set; }
        public DbSet<Model.Patients.AnimalKind> AnimalKinds { get; set; }
        public DbSet<Model.Patients.AnimalRace> AnimalRaces { get; set; }
        public DbSet<Model.Patients.Patient> Patients { get; set; }
        public DbSet<Model.Patients.PatientComment> PatientComments { get; set; }
        public DbSet<Model.Patients.AnimalIcon> AnimalIcons { get; set; }
        public DbSet<Model.Patients.MedicalProcedure> MedicalProcedures { get; set; }
        public DbSet<Model.Patients.DischargePaper> DischargePapers { get; set; }
        public DbSet<Model.Patients.Letter> Letters { get; set; }
        public DbSet<Model.Patients.MinistryOfAgricultureReport> MinistryOfAgricultureReports { get; set; }
        public DbSet<Referral> Referrals { get; set; }
        public DbSet<FollowUp> FollowUps { get; set; }


        //Visits
        public DbSet<Model.Visits.Diagnosis> Diagnoses { get; set; }
        public DbSet<Model.Visits.Examination> Examinations { get; set; }
        //public DbSet<Model.Visits.Lab> Labs { get; set; }
        public DbSet<Model.Visits.Medication> Medications { get; set; }
        // public DbSet<Model.Visits.Treatment> Treatments { get; set; }
        public DbSet<Model.Visits.Visit> Visits { get; set; }
        public DbSet<Model.Visits.VitalSign> VitalSigns { get; set; }



        public DbSet<Model.Visits.VisitExamination> VisitExaminations { get; set; }
        public DbSet<Model.Visits.VisitMedication> VisitMedications { get; set; }
        public DbSet<Model.Visits.VisitDiagnosis> VisitDiagnoses { get; set; }
        public DbSet<Model.Visits.VisitTreatment> VisitTreatments { get; set; }
        public DbSet<Model.Visits.DangerousDrug> DangerousDrugs { get; set; }

        //Users
        //public DbSet<Model.Users.TimeClockEntry> TimeClockEntries { get; set; }
        public DbSet<Model.Users.User> Users { get; set; }
        public DbSet<RapidVet.Model.Users.Role> Roles { get; set; }
        public DbSet<RapidVet.Model.Users.UsersClinicsRoles> UsersClinicsRoles { get; set; }
        public DbSet<RapidVet.Model.Users.UsersDoctorsView> UsersDoctorsView { get; set; }

        public DbSet<RapidVet.Model.ShortCut> ShortCuts { get; set; }

        public DbSet<Model.Finance.PriceListCategory> PriceListCategories { get; set; }
        public DbSet<Model.Finance.PriceListItem> PriceListItems { get; set; }
        public DbSet<Model.Finance.Tariff> Tariffs { get; set; }
        public DbSet<Model.Finance.PriceListItemTariff> PriceListItemsTariff { get; set; }
        public DbSet<Model.Finance.PriceListItemType> PriceListItemTypes { get; set; }
        public DbSet<Model.Users.ConcurrentUserEntity> ConcurrentUserEntities { get; set; }

        public DbSet<ArchiveDocument> ArchiveDocuments { get; set; }
        public DbSet<Auditing> Auditings { get; set; }
        public DbSet<AuditingType> AuditingTypes { get; set; }


        //patientLabTests
        public DbSet<PatientLabTest> PatientLabTests { get; set; }
        public DbSet<PatientLabTestResult> PatientLabTestResults { get; set; }

        //Calender
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Holiday> Holidays { get; set; }
        public DbSet<DailySchedule> DailySchedules { get; set; }
        public DbSet<Recess> Recesses { get; set; }
        public DbSet<CalenderEntry> CalenderEntries { get; set; }
        public DbSet<BlockedDay> BlockedDays { get; set; }
        public DbSet<WaitingListItem> WaitingListItems { get; set; }

        //finances
        public DbSet<BankCode> BankCodes { get; set; }
        public DbSet<CreditCardCode> CreditCardCodes { get; set; }
        public DbSet<EasyCardSettings> EasyCardSettings { get; set; }

        public DbSet<FinanceDocumentPayment> FinanceDocumentPayments { get; set; }
        public DbSet<FinanceDocument> FinanceDocuments { get; set; }
        public DbSet<FinanceDocumentItem> FinanceDocumentItems { get; set; }
        public DbSet<EasyCardDealType> EasyCardDealTypes { get; set; }

        public DbSet<FinanceDocumentDividision> FinanceDocumentDivisions { get; set; }

        public DbSet<Deposit> Deposits { get; set; }

        //preventive medicine
        public DbSet<PreventiveMedicineItem> PreventiveMedicineItems { get; set; }
        public DbSet<UserPreventiveMedicineReminderFilter> UserPreventiveMedicineReminderFilters { get; set; }
        public DbSet<PreventiveMedicineFilterPriceListItem> PreventiveMedicineFilterPriceListItems { get; set; }
        public DbSet<PreventiveMedicineFilterCity> PreventiveMedicineFilterCity { get; set; }

        //Expenses
        public DbSet<ExpenseGroup> ExpenseGroups { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Expense> Expenses { get; set; }

        //clinic tasks
        public DbSet<ClinicTask> ClinicTasks { get; set; }

        //Visit Reports
        public DbSet<VisitFilter> VisitFilters { get; set; }

        //Inventory
        public DbSet<PriceListItemSupplier> PriceListItemSuppliers { get; set; }
        public DbSet<InventoryOrder> InventoryOrders { get; set; }
        public DbSet<InventoryPriceListItemOrder> InventoryOrderItems { get; set; }
        public DbSet<Inventory> Inventory { get; set; }

        //Internal Settlements
        public DbSet<InternalSettlement> InternalSettlements { get; set; }

        //data migration log
        public DbSet<DataMigrationLog> DataMigrationLogs { get; set; }
        public DbSet<ArchiveMigrationLog> ArchiveMigrationLogs { get; set; }
        public DbSet<Interface> Interfaces { get; set; }

        //Tools
        public DbSet<GeneralSettings> GeneralMessage { get; set; }
        public DbSet<TemplateKeyWords> TemplateKeyWords { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {            
            modelBuilder.Entity<Tariff>()
                        .HasRequired(t => t.Clinic)
                        .WithMany(c => c.Tariffs)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<PriceListItemTariff>()
                        .HasRequired(p => p.Tariff)
                        .WithMany(t => t.TariffsItems)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                        .HasMany(c => c.Patients)
                        .WithRequired(p => p.Client)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Patient>()
                        .HasMany(p => p.Visits)
                        .WithOptional(v => v.Patient)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Patient>()
                        .HasMany(p => p.Quotations)
                        .WithRequired(q => q.Patient)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Clinic>()
                        .HasMany(c => c.TreatmentPackages)
                        .WithRequired(tp => tp.Clinic)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<PriceListItem>()
                        .HasMany(p => p.VaccineOrTreatment)
                        .WithRequired(v => v.PriceListItem)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<AnimalKind>()
                        .HasMany(p => p.VaccineOrTreatments)
                        .WithRequired(v => v.AnimalKind)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<AnimalKind>()
                        .HasMany(p => p.LabTestForAnimals)
                        .WithRequired(v => v.AnimalKind)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<PriceListItem>()
                        .HasMany(pli => pli.PreventiveMedicineItems)
                        .WithRequired(pmi => pmi.PriceListItem)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<VaccineOrTreatment>()
                        .HasMany(v => v.NextVaccinesAndTreatments)
                        .WithRequired(n => n.ParentVaccineOrTreatment)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Patient>()
                        .HasMany(p => p.Archives)
                        .WithRequired(a => a.Patient)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<LabTestTemplate>()
                        .HasMany(ltt => ltt.PatientLabTests)
                        .WithRequired(plt => plt.LabTestTemplate)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<LabTest>()
                        .HasMany(lt => lt.PatientResults)
                        .WithRequired(pr => pr.LabTest)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Diagnosis>()
                        .HasMany(d => d.MedicalProcedures)
                        .WithRequired(mp => mp.Diagnosis)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Issuer>()
                        .HasMany(iss => iss.FinanceDocuments)
                        .WithRequired(fd => fd.Issuer)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<Clinic>()
                        .HasMany(c => c.CalenderEntries)
                        .WithRequired(tp => tp.Clinic)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                        .HasMany(c => c.WaitingListItems)
                        .WithRequired(tp => tp.Client)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserPreventiveMedicineReminderFilter>()
                        .HasMany(u => u.PreventiveMedicineItems)
                        .WithRequired(p => p.Filter)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<PriceListItem>()
                        .HasMany(p => p.ReminderFilters)
                        .WithRequired(r => r.PriceListItem)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Clinic>()
                        .HasMany(c => c.ExpenseGroups)
                        .WithRequired(e => e.Clinic)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Clinic>()
                        .HasMany(c => c.Suppliers)
                        .WithRequired(s => s.Clinic)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<FinanceDocument>()
                        .HasMany(c => c.FinanceDocumentDividisions)                        
                        .WithRequired(s => s.Document)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Clinic>()
                        .HasMany(c => c.Orders)
                        .WithRequired(o => o.Clinic)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                        .HasMany(u => u.Orders)
                        .WithRequired(o => o.User)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<PriceListItem>()
                        .HasMany(p => p.Inventories)
                        .WithRequired(i => i.PriceListItem)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                .HasMany(c => c.Visits)
                .WithRequired(v => v.ClientAtTimeOfVisit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TemplateKeyWords>().HasKey(x => new { x.TemplateKeyWord, x.Culture });

            modelBuilder.Entity<FinanceDocument>().Property(x => x.VAT).HasPrecision(18, 4);

            modelBuilder.Ignore<Resources.Clinic>(); //ef 6 fix
            //modelBuilder.Entity<TelephoneDirectory>()
            //    .HasOptional(t => t.TelephoneDirectoryType)
            //     .WithOptionalPrincipal();
            //modelBuilder.Entity<Visit>()
            //    .HasOptional<Patient>(u => u.Patient)
            //    .WithOptionalPrincipal();
        }

    }
}

