﻿using RapidVet.Enums.Google.Results;
using RapidVet.Model.Google;
using RapidVet.WebModels.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Repository.Google
{
    public interface IGoogleTokenRepository : IDisposable
    {
        GoogleToken GetGoogleToken(string email);

        void AddUpdateGoogleToken(GoogleToken googleToken, int userID, DateTime changeDate);

        List<GoogleSyncSet> GetGoogleSettings(int clinicGroupID);

        SyncCalResult AddUpdateGoogleSetting(GoogleSettings model, int userID, int clinicID, int clinicGroupID, string googleSyncEmailAccount);

        List<GoogleReportModel> GetHistory(string googleSettingId, DateTime fromDate, DateTime toDate, bool withErrorsOnly, bool hasChangesOnly);

        string ResetGoogleCalendarID(int settingID);
    }
}
