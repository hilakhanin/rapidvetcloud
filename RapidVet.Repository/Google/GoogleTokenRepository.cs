﻿using RapidVet.Enums.Google.Results;
using RapidVet.Model.Google;
using RapidVet.WebModels.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Repository.Google
{
    public class GoogleTokenRepository : RepositoryBase<RapidVetDataContext>, IGoogleTokenRepository
    {
        public GoogleTokenRepository()
        {

        }

        public GoogleTokenRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// return Google Token for email of exist
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>IQueryable(Client)</returns>
        public GoogleToken GetGoogleToken(string email)
        {
            return (from t in DataContext.GoogleTokens
                    where t.EMail == email
                    select t).FirstOrDefault();
        }

        public List<GoogleSyncSet> GetGoogleSettings(int clinicGroupID)
        {
            return (from t in DataContext.GoogleSettings
                    join gt in DataContext.GoogleTokens on t.GoogleTokenId equals gt.Id
                    join d in DataContext.Users on t.DoctorID equals d.Id
                    join clnc in DataContext.Clinics on t.ClinicID equals clnc.Id
                    where t.ClinicGroupID == clinicGroupID
                    select new GoogleSyncSet()
                    {
                        GoogleTokenId = t.GoogleTokenId,
                        AllowSync = t.AllowSync,
                        AllowSyncCalendar = t.AllowSyncCalendar,
                        AllowSyncContacts = t.AllowSyncContacts,
                        ClinicGroupID = t.ClinicGroupID,
                        ClinicID = t.ClinicID,
                        ClinicName = clnc.Name,
                        DoctorID = t.DoctorID,
                        DoctorName = d.FirstName + " " + d.LastName,
                        GoogleCalName = t.GoogleCalName,
                        Id = t.Id,
                        SyncCalBackDays = t.SyncCalBackDays,
                        SyncCalForwardDays = t.SyncCalForwardDays,
                        SyncCalFromGoogle = t.SyncCalFromGoogle,
                        SyncCalToGoogle = t.SyncCalToGoogle,
                        SyncPeriod = t.SyncPeriod,
                        AccountEmail = gt.EMail,
                        HasGoogleCalendarID = t.GoogleCalendarID != null
                    }).ToList();
        }

        public void AddUpdateGoogleToken(GoogleToken googleToken, int userID, DateTime changeDate)
        {
            if (googleToken == null || String.IsNullOrEmpty(googleToken.EMail))
                return;

            var f = DataContext.GoogleTokens.FirstOrDefault(x => x.EMail == googleToken.EMail);
            if (f == null)
            {
                f = new GoogleToken()
                    {
                        AccessTokenExpirationUtc = googleToken.AccessTokenExpirationUtc,
                        AccessTokenIssueDateUtc = googleToken.AccessTokenIssueDateUtc,
                        AccessToken = googleToken.AccessToken,
                        RefreshToken = googleToken.RefreshToken,
                        EMail = googleToken.EMail,
                        CreatedById = userID,
                        CreatedDate = changeDate
                    };

                DataContext.GoogleTokens.Add(f);
            }
            else
            {
                f.AccessTokenExpirationUtc = googleToken.AccessTokenExpirationUtc;
                f.AccessTokenIssueDateUtc = googleToken.AccessTokenIssueDateUtc;
                f.AccessToken = googleToken.AccessToken;
                f.RefreshToken = googleToken.RefreshToken;
                f.UpdatedById = userID;
                f.UpdatedDate = changeDate;
            }

            DataContext.SaveChanges();
        }

        public SyncCalResult AddUpdateGoogleSetting(GoogleSettings model, int userID, int clinicID, int clinicGroupID, string googleSyncEmailAccount)
        {
            if (String.IsNullOrEmpty(googleSyncEmailAccount))
                return SyncCalResult.GoogleSyncEmailAccountNotProvided;

            var fGToken = DataContext.GoogleTokens.FirstOrDefault(x => x.EMail == googleSyncEmailAccount);
            if (fGToken == null)
                return SyncCalResult.GoogleSyncEmailAccountNotValid;

            int maxGoogleSyncAccounts = DataContext.ClinicGroups.FirstOrDefault(x => x.Id == clinicGroupID).MaxGoogleSyncAllowed;
            var f = DataContext.GoogleSettings.FirstOrDefault(x => (model.Id > 0 && x.Id == model.Id)//update
                ||
                (model.Id < 1 && x.GoogleTokenId == fGToken.Id && x.ClinicGroupID == clinicGroupID && x.ClinicID == clinicID && x.DoctorID == model.DoctorID)
);//new
            int updateID = f == null ? -1 : f.Id;
            var allClinicGroupIDSyncs = DataContext.GoogleSettings.Where(x => x.ClinicGroupID == clinicGroupID && x.AllowSync && (updateID == -1 || updateID != x.Id)).Select(y => y.Id).ToList();
            if (maxGoogleSyncAccounts < (allClinicGroupIDSyncs.Count + (f == null ? 0 : model.AllowSync ? 1 : 0)))// חריגה בכמות החשבונות המותרת
                return SyncCalResult.MaxSyncAcoountsExeeded;

            if (DataContext.GoogleSettings.Any(x => x.GoogleTokenId == fGToken.Id && x.GoogleCalName.Equals(model.GoogleCalName) && (updateID == -1 || updateID != x.Id)))// שם יומן בגוגל קיים כבר
                return SyncCalResult.CalendarNameAlreadyExist;

            var now = DateTime.Now;
            if (f == null)
            {
                f = new GoogleSettings()
                {
                    GoogleTokenId = fGToken.Id,
                    ClinicGroupID = clinicGroupID,
                    ClinicID = clinicID,
                    CreatedDate = now,
                    CreatedById = userID,
                    DoctorID = model.DoctorID
                };
                DataContext.GoogleSettings.Add(f);
            }

            f.AllowSync = model.AllowSync;
            f.AllowSyncCalendar = model.AllowSyncCalendar;
            f.AllowSyncContacts = model.AllowSyncContacts;
            f.UpdatedDate = now;
            f.UpdatedById = userID;
            f.GoogleCalName = model.GoogleCalName;
            f.SyncCalBackDays = model.SyncCalBackDays;
            f.SyncCalForwardDays = model.SyncCalForwardDays;
            f.SyncCalFromGoogle = model.SyncCalFromGoogle;
            f.SyncCalToGoogle = model.SyncCalToGoogle;
            f.NextSync = model.AllowSync ? getNextSyncDateTime(f.NextSync, f.SyncPeriod, model.SyncPeriod, now) : null;
            f.SyncPeriod = model.SyncPeriod;
            DataContext.SaveChanges();
            return SyncCalResult.OK;
        }

        private DateTime? getNextSyncDateTime(DateTime? oldNextSync, int oldPeriod, int newPeriod, DateTime now)
        {
            return !oldNextSync.HasValue || oldPeriod != newPeriod ? now.AddMinutes(newPeriod) : oldNextSync;
        }

        public List<GoogleReportModel> GetHistory(string googleSettingId, DateTime fromDate, DateTime toDate, bool withErrorsOnly, bool hasChangesOnly)
        {
            toDate = toDate.Date.AddDays(1);
            int gsId;
            if (!int.TryParse(googleSettingId, out gsId))
                return new List<GoogleReportModel>();

            var setting = (from t in DataContext.GoogleSettings
                           where t.Id == gsId
                           select new
                           {
                               TokenId = t.GoogleTokenId,
                               DoctorId = t.DoctorID
                           }).FirstOrDefault();
            if (setting == null)
                return new List<GoogleReportModel>();

            var notEndedDate = new DateTime(1900, 1, 1);
            return (from t in DataContext.GoogleSyncReports
                    where t.DoctorID == setting.DoctorId && t.GoogleTokenID == setting.TokenId &&
                    t.SyncStarted >= fromDate &&
                    (t.SyncEnded <= toDate || t.SyncEnded == notEndedDate) &&
                    (!withErrorsOnly || t.ErrorMsg != null) &&
                    (!hasChangesOnly ||
                    (t.EventsCreatedInGoogle > 0 || t.EventsCreatedInRapid > 0 || t.EventsDeletedInGoogle > 0 || t.EventsDeletedInRapid > 0 || t.EventsUpdatedInGoogle > 0 || t.EventsUpdatedInRapid > 0 || t.ContactsCreatedInGoogle > 0 || t.ContactsUpdatedInGoogle > 0))
                    select new GoogleReportModel()
                    {
                        ContactsCreatedInGoogle = t.ContactsCreatedInGoogle,
                        ContactsUpdatedInGoogle = t.ContactsUpdatedInGoogle,
                        ErrorMsg = t.ErrorMsg,
                        EventsCreatedInGoogle = t.EventsCreatedInGoogle,
                        EventsCreatedInRapid = t.EventsCreatedInRapid,
                        EventsDeletedInGoogle = t.EventsDeletedInGoogle,
                        EventsDeletedInRapid = t.EventsDeletedInRapid,
                        EventsUpdatedInGoogle = t.EventsUpdatedInGoogle,
                        EventsUpdatedInRapid = t.EventsUpdatedInRapid,
                        SyncStarted = t.SyncStarted,
                        SyncEnded = t.SyncEnded == notEndedDate ? new Nullable<DateTime>() : t.SyncEnded
                    }).ToList();
        }

        public string ResetGoogleCalendarID(int settingID)
        {
            try
            {
                var f = DataContext.GoogleSettings.FirstOrDefault(x => x.Id == settingID);
                if (f != null)
                {
                    f.GoogleCalendarID = null;
                    DataContext.SaveChanges();
                    return "ok";
                }
            }
            catch
            {
            }
            return null;
        }
    }
}
