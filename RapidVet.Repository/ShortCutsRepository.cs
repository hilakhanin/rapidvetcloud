﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;

namespace RapidVet.Repository
{
    public class ShortCutsRepository : RepositoryBase<RapidVetDataContext>
    {
        static ShortCutsRepository()
        {
            using (var reposetory = new ShortCutsRepository())
            {
                ShortCuts = reposetory.GetList();
            }
        }

        /// <summary>
        /// returns list of shirtcuts
        /// </summary>
        /// <returns></returns>
        List<ShortCut> GetList()
        {
            return DataContext.ShortCuts.OrderBy(s => s.Name).ToList();
        }

        static public List<ShortCut> ShortCuts { get; private set; }

        /// <summary>
        /// returns
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        static public ShortCut GetShortCut(int id)
        {
            return ShortCuts.Single(s => s.Id == id);
        }

    }
}
