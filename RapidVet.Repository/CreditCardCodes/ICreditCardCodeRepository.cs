﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Finance;

namespace RapidVet.Repository.CreditCardCodes
{
    public interface ICreditCardCodeRepository : IDisposable
    {
        /// <summary>
        /// get all credit card codes for clinic
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>IQueryable<CreditCardCode></returns>
        IQueryable<CreditCardCode> GetCodes(int clinicId);
        
        /// <summary>
        /// get all credit card codes for clinic by issuer
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="issuerId">int issuer id</param>
        /// <returns>IQueryable<CreditCardCode></returns>
        IQueryable<CreditCardCode> GetCodesByIssuer(int clinicId, int issuerId);

        IQueryable<CreditCardCode> GetExternalCodesByIssuer(int clinicId, int issuerId, int cardId);

        /// <summary>
        /// create credit card code fot clinic
        /// </summary>
        /// <param name="code"> CreditCardCode obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(CreditCardCode code);

        /// <summary>
        /// delete CreditCardCode obj
        /// </summary>
        /// <param name="code">CreditCardCode obj </param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(CreditCardCode code);

        /// <summary>
        /// get specific credit card code id
        /// </summary>
        /// <param name="creditCardCodeId">int credit card code id</param>
        /// <returns>CreditCardCode</returns>
        CreditCardCode GetCreditCardCode(int creditCardCodeId);
    }
}
