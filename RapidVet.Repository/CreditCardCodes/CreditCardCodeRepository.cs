﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Finance;

namespace RapidVet.Repository.CreditCardCodes
{
    public class CreditCardCodeRepository : RepositoryBase<RapidVetDataContext>, ICreditCardCodeRepository
    {
        public CreditCardCodeRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public CreditCardCodeRepository()
        {

        }

        public IQueryable<CreditCardCode> GetCodes(int clinicId)
        {
            return DataContext.CreditCardCodes.Where(c => c.ClinicId == clinicId && c.Active);
        }

        public IQueryable<CreditCardCode> GetCodesByIssuer(int clinicId, int issuerId)
        {
            return DataContext.CreditCardCodes.Where(c => c.ClinicId == clinicId && c.Active && c.IssuerId == issuerId);
        }

        public IQueryable<CreditCardCode> GetExternalCodesByIssuer(int clinicId, int issuerId, int cardId)
        {
            return DataContext.CreditCardCodes.Where(c => c.ClinicId == clinicId && c.Active && c.IssuerId == issuerId && c.Id == cardId);
        }

        public OperationStatus Create(CreditCardCode code)
        {
            DataContext.CreditCardCodes.Add(code);
            return base.Save(code);
        }

        public OperationStatus Delete(CreditCardCode code)
        {
            DataContext.CreditCardCodes.Remove(code);
            return base.Save(code);
        }

        public CreditCardCode GetCreditCardCode(int creditCardCodeId)
        {
            return DataContext.CreditCardCodes.Single(c => c.Id == creditCardCodeId);
        }
    }
}
