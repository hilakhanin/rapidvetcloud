﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Model;
using RapidVet.Model.Quotations;

namespace RapidVet.Repository.Quotations
{
    public class QuotationRepository : RepositoryBase<RapidVetDataContext>, IQuotationRepository
    {
        public QuotationRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public QuotationRepository()
        {

        }

        public List<Quotation> GetClientQuotations(int id)
        {
            var clientPatientsIds = DataContext.Patients.Where(p => p.ClientId == id).Select(p => p.Id).ToList();
            return DataContext.Quotations.Include(q => q.Patient).Include(q => q.Treatments)
                              .Where(q => clientPatientsIds.Contains(q.PatientId)).OrderBy(q => q.Updated).ToList();
        }


        public List<Quotation> GetPatientQuotationsForHistory(int id)
        {
            return DataContext.Quotations.Where(q => q.PatientId == id).ToList();
        }

        public Quotation GetQuotation(int quotationId)
        {

            return DataContext.Quotations
                              .Include(q => q.Treatments)
                              .Include("Treatments.PriceListItem")
                              .Include("Treatments.PriceListItem.Category")
                              .Include("Treatments.PriceListItem.PriceListItemType")
                              .Include(q => q.Patient)
                              .Include(q => q.Patient.AnimalRace)
                              .Include(q => q.Patient.AnimalRace.AnimalKind)
                              .Include("Patient.Client")
                              .Include(q => q.User)
                              .SingleOrDefault(q => q.Id == quotationId);
        }

        public string GetClinicQuotaionPrefix(int clinicId)
        {
            return DataContext.Clinics.Single(c => c.Id == clinicId).QuotationPreface;
        }

        public OperationStatus Create(Quotation quotation)
        {
            DataContext.Quotations.Add(quotation);
            return base.Save(quotation);
        }

        public OperationStatus Save(Quotation quotation)
        {
            return base.Save(quotation);
        }

        public OperationStatus UpdateQuotationTreatments(int quotationId, List<QuotationTreatment> treatments, bool executeTreatments, out List<string> logContents)
        {
            var status = new OperationStatus() { Success = false };
            var existingTreatments = DataContext.QuotationTreatments.Where(qt => qt.QuotationId == quotationId);
            logContents = new List<string>();
            foreach (var et in existingTreatments.ToList())
            {
                var updatedTreatment = treatments.SingleOrDefault(q => q.Id == et.Id);

                if (updatedTreatment != null)
                {
                    et.Amount = updatedTreatment.Amount;
                    et.PercentDiscount = updatedTreatment.PercentDiscount;
                    et.Price = updatedTreatment.Price;
                    et.Comments = updatedTreatment.Comments;
                    et.Discount = updatedTreatment.Discount;
                    if (executeTreatments)
                        et.WasTreatmentMovedToExecution = updatedTreatment.WasTreatmentMovedToExecution;

                    et.VisitId = updatedTreatment.VisitId;

                    //remove existing item from list
                    treatments.Remove(updatedTreatment);
                }
                else //treatment was removed client side.
                {
                    logContents.Add(String.Format("פריט- {0}, הצעה- {1}, חיה- {2}, לקוח- {3}", et.Name, et.Quotation.Name, et.Quotation.Patient.Name, et.Quotation.Patient.Client.Name));
                    DataContext.QuotationTreatments.Remove(et);
                }
            }
            status.Success = DataContext.SaveChanges() > -1;

            if (status.Success)
            {
                foreach (var t in treatments)
                {
                    t.Id = 0;
                    t.QuotationId = quotationId;
                    if (!executeTreatments)
                        t.WasTreatmentMovedToExecution = false;

                    DataContext.QuotationTreatments.Add(t);
                }

                status.Success = DataContext.SaveChanges() > -1;
            }

            return status;
        }

        public List<QuotationTreatment> GetTreatments(int quotationId)
        {
            return DataContext.QuotationTreatments.Where(qt => qt.QuotationId == quotationId).ToList();
        }

        public QuotationTreatment GetQuotationTreatment(int quotationTreatmentId)
        {
            return DataContext.QuotationTreatments.SingleOrDefault(qt => qt.Id == quotationTreatmentId);
        }

        //public List<QuotationTreatment> ClearAllTreatmentsDiscounts(int quotationId)
        //{
        //    List<QuotationTreatment> treatments = DataContext.QuotationTreatments.Where(qt => qt.QuotationId == quotationId).ToList();

        //    foreach (var t in treatments)
        //    {
        //        t.Discount = 0;
        //        t.PercentDiscount = 0;
        //    }

        //    DataContext.SaveChanges();

        //    return treatments;
        //}

        public bool Delete(Quotation quotation)
        {
            if (quotation.Treatments.Count > 0)
            {
                SetQuotationItemsForRemoval(quotation.Treatments.ToList());
                for (int i = quotation.Treatments.Count - 1; i >= 0; i--)
                {
                    DataContext.QuotationTreatments.Remove(quotation.Treatments.ToList()[i]);
                }
            }
            DataContext.Quotations.Remove(quotation);
            return DataContext.SaveChanges() > -1;
        }

        public void SetQuotationItemsForRemoval(List<QuotationTreatment> list)
        {
            var quotationTreatmentIds = list.Select(x=>x.Id);
            var eIDs = DataContext.VisitExaminations.Where(e => e.QuotationTreatmentId.HasValue && quotationTreatmentIds.Contains(e.QuotationTreatmentId.Value)).Select(x => x.Id).ToList();
            var mIDs = DataContext.VisitMedications.Where(e => e.QuotationTreatmentId.HasValue && quotationTreatmentIds.Contains(e.QuotationTreatmentId.Value)).Select(x => x.Id).ToList();
            var pmIDs = DataContext.PreventiveMedicineItems.Where(e => e.QuotationTreatmentId.HasValue && quotationTreatmentIds.Contains(e.QuotationTreatmentId.Value)).Select(x => x.Id).ToList();
            var tIDs = DataContext.VisitTreatments.Where(e => e.QuotationTreatmentId.HasValue && quotationTreatmentIds.Contains(e.QuotationTreatmentId.Value)).Select(x => x.Id).ToList();
            if (eIDs != null && eIDs.Count > 0)
                removeQuotation(DataContext.VisitExaminations.Where(x => eIDs.Contains(x.Id)));

            if (mIDs != null && mIDs.Count > 0)
                removeQuotation(DataContext.VisitMedications.Where(x => !x.Invoiced && mIDs.Contains(x.Id)));

            if (tIDs != null && tIDs.Count > 0)
                removeQuotation(DataContext.VisitTreatments.Where(x => !x.Invoiced && tIDs.Contains(x.Id)));

            if (pmIDs != null && pmIDs.Count > 0)
                removeQuotation(DataContext.PreventiveMedicineItems.Where(x => !x.Invoiced && pmIDs.Contains(x.Id)));

            DataContext.SaveChanges();
        }

        private void removeQuotation(dynamic dynamicQueryable)
        {
            foreach (var i in dynamicQueryable)
                i.QuotationTreatmentId = null;
        }
    }
}
