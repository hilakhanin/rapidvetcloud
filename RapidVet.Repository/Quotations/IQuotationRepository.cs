﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Quotations;

namespace RapidVet.Repository.Quotations
{
    public interface IQuotationRepository : IDisposable
    {
        /// <summary>
        /// get all quotations for client
        /// </summary>
        /// <param name="id"> client id</param>
        /// <returns>  List<Quotation></returns>
        List<Quotation> GetClientQuotations(int id);

        /// <summary>
        /// get single quotations
        /// </summary>
        /// <param name="quotationId">quotation id</param>
        /// <returns>Quotation obj</returns>
        Quotation GetQuotation(int quotationId);

        QuotationTreatment GetQuotationTreatment(int quotationTreatmentId);

        /// <summary>
        /// gets clinic quotation preface text
        /// </summary>
        /// <param name="clinicId"> clinic id</param>
        /// <returns>string</returns>
        string GetClinicQuotaionPrefix(int clinicId);

        /// <summary>
        /// create new Quotation
        /// </summary>
        /// <param name="quotation">Quotation obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Quotation quotation);

        /// <summary>
        /// updates quotation obj
        /// </summary>
        /// <param name="quotation">Quotation obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Save(Quotation quotation);

        /// <summary>
        /// updates treatments in quotation
        /// </summary>
        /// <param name="quotationId">quotation id</param>
        /// <param name="treatments"> list of quotation treatments</param>
        /// <returns>OperationStatus</returns>
        OperationStatus UpdateQuotationTreatments(int quotationId, List<QuotationTreatment> treatments, bool executeTreatments, out List<string> logContents);

        /// <summary>
        /// get all treatments for quotation
        /// </summary>
        /// <param name="quotationId">quotation id</param>
        /// <returns> List<QuotationTreatment></returns>
        List<QuotationTreatment> GetTreatments(int quotationId);

        ///// <summary>
        ///// clear all treatment discounts for quotation
        ///// </summary>
        ///// <param name="quotationId">quotation id</param>
        ///// <returns> List<QuotationTreatment></returns>
        //List<QuotationTreatment> ClearAllTreatmentsDiscounts(int quotationId);

        /// <summary>
        /// delete quotation
        /// </summary>
        /// <param name="quotation">Quotation obj</param>
        /// <returns>bool</returns>
        bool Delete(Quotation quotation);

        /// <summary>
        /// get all quotations for patient
        /// </summary>
        /// <param name="id">patient id</param>
        /// <returns> List<Quotation></returns>
        List<Quotation> GetPatientQuotationsForHistory(int id);
    }
}
