﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Repository.Archives;
using RapidVet.Repository.Calender;
using RapidVet.Repository.Clients;
using RapidVet.Repository.ClinicTasks;
using RapidVet.Repository.Clinics;
using RapidVet.Repository.CreditCardCodes;
using RapidVet.Repository.DataMigrationRepository;
using RapidVet.Repository.DischargePapers;
using RapidVet.Repository.ExpenseGroups;
using RapidVet.Repository.Expenses;
using RapidVet.Repository.FinanceReports;
using RapidVet.Repository.Finances;
using RapidVet.Repository.GeneralReports;
using RapidVet.Repository.Holidays;
using RapidVet.Repository.InternalSettlements;
using RapidVet.Repository.ClinicInventory;
using RapidVet.Repository.MedicalProcedures;
using RapidVet.Repository.MetaData;
using RapidVet.Repository.MinistryOfAgricultureReports;
using RapidVet.Repository.PatientFollowUps;
using RapidVet.Repository.PatientLabTests;
using RapidVet.Repository.Patients;
using RapidVet.Repository.PreventiveMedicine;
using RapidVet.Repository.Quotations;
using RapidVet.Repository.Recesses;
using RapidVet.Repository.Referrals;
using RapidVet.Repository.RegionalCouncils;
using RapidVet.Repository.Rooms;
using RapidVet.Repository.Statistics;
using RapidVet.Repository.Suppliers;
using RapidVet.Repository.TreatmentPackages;
using RapidVet.Repository.Visits;
using RapidVet.Repository.WaitingListItems;
using RapidVet.Repository.Auditings;
using RapidVet.Repository.Google;

namespace RapidVet.Repository
{
    public class RapidVetUnitOfWork : UnitOfWorkBase<RapidVetDataContext>
    {
        //Google
        private IGoogleTokenRepository _googleRepository;
        public IGoogleTokenRepository GoogleRepository
        {
            get { return _googleRepository ?? (_googleRepository = new GoogleTokenRepository(DataContext)); }
        }

        //Clients
        private IClientRepository _clientRepository;
        public IClientRepository ClientRepository
        {
            get { return _clientRepository ?? (_clientRepository = new ClientRepository(DataContext)); }
        }

        private IClientStatusRepository _clientStatusRepository;
        public IClientStatusRepository ClientStatusRepository
        {
            get { return _clientStatusRepository ?? (_clientStatusRepository = new ClientStatusRepository(DataContext)); }
        }

        //Clinics
        private IClinicGroupRepository _clinicGroupRepository;
        public IClinicGroupRepository ClinicGroupRepository
        {
            get { return _clinicGroupRepository ?? (_clinicGroupRepository = new ClinicGroupRepository(DataContext)); }
        }

        private IClinicRepository _clinicRepository;
        public IClinicRepository ClinicRepository
        {
            get { return _clinicRepository ?? (_clinicRepository = new ClinicRepository(DataContext)); }
        }

        private IIssuerRepository _issuerRepository;
        public IIssuerRepository IssuerRepository
        {
            get { return _issuerRepository ?? (_issuerRepository = new IssuerRepository(DataContext)); }
        }

        private IDefaultCardCompaniesRepository _defaultCardCompaniesRepository;
        public IDefaultCardCompaniesRepository DefaultCardCompaniesRepository
        {
            get { return _defaultCardCompaniesRepository ?? (_defaultCardCompaniesRepository = new DefaultCardCompaniesRepository(DataContext)); }
        }

        private IBackgroundJobsRepository _backgroundJobsRepository;
        public IBackgroundJobsRepository BackgroundJobsRepository
        {
            get { return _backgroundJobsRepository ?? (_backgroundJobsRepository = new BackgroundJobsRepository(DataContext)); }
        }

        //Finance
        private IPriceListCategoryRepository _categoryRepository;
        public IPriceListCategoryRepository CategoryRepository
        {
            get { return _categoryRepository ?? (_categoryRepository = new PriceListCategoryRepository(DataContext)); }
        }

        private IPriceListItemRepository _itemsRepository;
        public IPriceListItemRepository ItemRepository
        {
            get { return _itemsRepository ?? (_itemsRepository = new PriceListItemRepository(DataContext)); }
        }

        private ITariffRepository _tariffRepository;
        public ITariffRepository TariffRepository
        {
            get { return _tariffRepository ?? (_tariffRepository = new TariffRepository(DataContext)); }
        }

        private IPriceListRepository _priceListRepository;
        public IPriceListRepository PriceListRepository
        {
            get { return _priceListRepository ?? (_priceListRepository = new PriceListRepository(DataContext)); }
        }

        //Patients
        private IPatientRepository _patientRepository;
        public IPatientRepository PatientRepository
        {
            get { return _patientRepository ?? (_patientRepository = new PatientRepository(DataContext)); }
        }

        private IAnimalKindRepository _animalKindRepository;
        public IAnimalKindRepository AnimalKindRepository
        {
            get { return _animalKindRepository ?? (_animalKindRepository = new AnimalKindRepository(DataContext)); }
        }
        
        private IAnimalRaceRepository _animalRaceRepository;
        public IAnimalRaceRepository AnimalRaceRepository
        {
            get { return _animalRaceRepository ?? (_animalRaceRepository = new AnimalRaceRepository(DataContext)); }
        }

        private IAnimalColorRepository _animalColorRepository;
        public IAnimalColorRepository AnimalColorRepository
        {
            get { return _animalColorRepository ?? (_animalColorRepository = new AnimalColorRepository(DataContext)); }
        }

        //Auditing
        private IAuditingsRepository _auditingRepository;
        public IAuditingsRepository AuditingRepository
        {
            get { return _auditingRepository ?? (_auditingRepository = new AuditingsRepository(DataContext)); }
        }

        //Visits

        private IVisitRepository _visitRepository;
        public IVisitRepository VisitRepository
        {
            get { return _visitRepository ?? (_visitRepository = new VisitRepository(DataContext)); }
        }

        private IDiagnosisRepository _diagnosisRepository;
        public IDiagnosisRepository DiagnosisRepository
        {
            get { return _diagnosisRepository ?? (_diagnosisRepository = new DiagnosisRepository(DataContext)); }
        }

        //private ITreatmentRepository _treatmentRepository;
        //public ITreatmentRepository TreatmentRepository
        //{
        //    get { return _treatmentRepository ?? (_treatmentRepository = new TreatmentRepository(DataContext)); }
        //}

        private IMedicationReposetory _medicationRepository;
        public IMedicationReposetory MedicationRepository
        {
            get { return _medicationRepository ?? (_medicationRepository = new MedicationReposetory(DataContext)); }
        }

        //Users
        private IConcurrentUserEntityRepository _concurrentUserEntityRepository;
        public IConcurrentUserEntityRepository ConcurrentUserEntityRepository
        {
            get
            {
                return _concurrentUserEntityRepository ??
                       (_concurrentUserEntityRepository = new ConcurrentUserEntityRepository(DataContext));
            }
        }

        private IUserRepository _userRepository;
        public IUserRepository UserRepository
        {
            get { return _userRepository ?? (_userRepository = new UserRepository(DataContext)); }
        }

        private ILastClientRepository _lastClientRepository;
        public ILastClientRepository LastClientRepository
        {
            get { return _lastClientRepository ?? (_lastClientRepository = new LastClientRepository(DataContext)); }
        }

        private IMetaDataRepository _metaDataRepository;
        public IMetaDataRepository MetaDataRepository
        {
            get { return _metaDataRepository ?? (_metaDataRepository = new MetaDataRepository(DataContext)); }
        }

        private ITelephoneDirectoryRepository _telephoneDirectoryRepository;
        public ITelephoneDirectoryRepository TelephoneDirectoryRepository
        {
            get { return _telephoneDirectoryRepository ?? (_telephoneDirectoryRepository = new TelephoneDirectoryRepository(DataContext)); }
        }

        private IQuotationRepository _quotationRepository;
        public IQuotationRepository QuotationRepository
        {
            get { return _quotationRepository ?? (_quotationRepository = new QuotationRepository(DataContext)); }
        }

        private IVaccineAndTreatmentRepository _vaccineAndTreatmentRepository;
        public IVaccineAndTreatmentRepository VaccineAndTreatmentRepository
        {
            get { return _vaccineAndTreatmentRepository ?? (_vaccineAndTreatmentRepository = new VaccineAndTreatmentRepository(DataContext)); }
        }

        private ILabTestsRepository _labTestsRepository;
        public ILabTestsRepository LabTestsRepository
        {
            get { return _labTestsRepository ?? (_labTestsRepository = new LabTestsRepository(DataContext)); }
        }

        private ITreatmentPackageRepository _treatmentPackageRepository;
        public ITreatmentPackageRepository TreatmentPackageRepository
        {
            get
            {
                return _treatmentPackageRepository ??
                       (_treatmentPackageRepository = new TreatmentPackageRepository(DataContext));
            }
        }

        private IPreventiveMedicineRepository _preventiveMedicineRepository;
        public IPreventiveMedicineRepository PreventiveMedicineRepository
        {
            get
            {
                return _preventiveMedicineRepository ??
                       (_preventiveMedicineRepository = new PreventiveMedicineRepository(DataContext));
            }
        }

        private IArchivesRepository _archivesRepository;
        public IArchivesRepository ArchivesRepository
        {
            get { return _archivesRepository ?? (_archivesRepository = new ArchivesRepository(DataContext)); }
        }

        private IPatientLabTestRepository _patientLabTestRepository;
        public IPatientLabTestRepository PatientLabTestRepository
        {
            get { return _patientLabTestRepository ?? (_patientLabTestRepository = new PatientLabTestRepository(DataContext)); }
        }

        private ILetterTemplatesRepository _letterTemplatesRepository;
        public ILetterTemplatesRepository LetterTemplatesRepository
        {
            get
            {
                return _letterTemplatesRepository ??
                       (_letterTemplatesRepository = new LetterTemplatesRepository(DataContext));
            }
        }

        private IPatientLetterRepository _patientLetterRepository;
        public IPatientLetterRepository PatientLetterRepository
        {
            get { return _patientLetterRepository ?? (_patientLetterRepository = new PatientLetterRepository(DataContext)); }
        }

        private IMinistryOfAgricultureReportsRepository _ministryOfAgricultureReportsRepository;
        public IMinistryOfAgricultureReportsRepository MinistryOfAgricultureReportsRepository
        {
            get { return _ministryOfAgricultureReportsRepository ?? (_ministryOfAgricultureReportsRepository = new MinistryOfAgricultureReportsRepository(DataContext)); }
        }

        private IMedicalProcedureRepository _medicalProcedureRepository;
        public IMedicalProcedureRepository MedicalProcedureRepository
        {
            get { return _medicalProcedureRepository ?? (_medicalProcedureRepository = new MedicalProcedureRepository(DataContext)); }
        }

        private IDischargePaperRepository _dischargePaperRepository;
        public IDischargePaperRepository DischargePaperRepository
        {
            get { return _dischargePaperRepository ?? (_dischargePaperRepository = new DischargePaperRepository(DataContext)); }
        }

        private IRoomRepository _roomRepository;
        public IRoomRepository RoomRepository
        {
            get { return _roomRepository ?? (_roomRepository = new RoomRepository(DataContext)); }
        }

        private IHolidayRepository _holidayRepository;
        public IHolidayRepository HolidayRepository
        {
            get { return _holidayRepository ?? (_holidayRepository = new HolidayRepository(DataContext)); }
        }

        private IReccessRepository _recessRepository;
        public IReccessRepository RecessRepository
        {
            get { return _recessRepository ?? (_recessRepository = new RecessRepository(DataContext)); }
        }

        private IReferralRepository _referralRepository;
        public IReferralRepository ReferralRepository
        {
            get { return _referralRepository ?? (_referralRepository = new ReferralRepository(DataContext)); }
        }

        private ICalenderRepository _calenderRepository;
        public ICalenderRepository CalenderRepository
        {
            get { return _calenderRepository ?? (_calenderRepository = new CalenderRepository(DataContext)); }
        }

        private IInvoiceRepository _invoiceRepository;
        public IInvoiceRepository InvoiceRepository
        {
            get { return _invoiceRepository ?? (_invoiceRepository = new InvoiceRepository(DataContext)); }
        }

        private IWaitingListRepository _waitingListRepository;
        public IWaitingListRepository WaitingListRepository
        {
            get { return _waitingListRepository ?? (_waitingListRepository = new WaitingListRepository(DataContext)); }
        }

        private IPreventiveMedicineRemindersRepository _preventiveMedicineRemindersRepository;
        public IPreventiveMedicineRemindersRepository PreventiveMedicineRemindersRepository
        {
            get
            {
                return _preventiveMedicineRemindersRepository ??
                       (_preventiveMedicineRemindersRepository = new PreventiveMedicineRemindersRepository(DataContext));
            }
        }


        private IFinanceDocumentReposetory _financeDocumentReposetory;
        public IFinanceDocumentReposetory FinanceDocumentReposetory
        {
            get
            {
                return _financeDocumentReposetory ??
                       (_financeDocumentReposetory = new FinanceDocumentReposetory(DataContext));
            }
        }

        private IExpenseGroupRepository _expenseGroupRepository;
        public IExpenseGroupRepository ExpenseGroupRepository
        {
            get { return _expenseGroupRepository ?? (_expenseGroupRepository = new ExpenseGroupRepository(DataContext)); }
        }

        private ISupplierRepository _supplierRepository;
        public ISupplierRepository SupplierRepository
        {
            get { return _supplierRepository ?? (_supplierRepository = new SupplierRepository(DataContext)); }
        }

        private IExpenseRepository _expenseRepository;
        public IExpenseRepository ExpenseRepository
        {
            get { return _expenseRepository ?? (_expenseRepository = new ExpenseRepository(DataContext)); }
        }

        private IFinanceReportRepository _financeReportRepository;
        public IFinanceReportRepository FinanceReportRepository
        {
            get { return _financeReportRepository ?? (_financeReportRepository = new FinanceReportRepository(DataContext)); }
        }

        private IDepositsRepository _depositsRepository;
        public IDepositsRepository DepositsRepository
        {
            get { return _depositsRepository ?? (_depositsRepository = new DepositsRepository(DataContext)); }
        }

        private ICreditCardCodeRepository _creditCardCodeRepository;
        public ICreditCardCodeRepository CreditCardCodeRepository
        {
            get
            {
                return _creditCardCodeRepository ??
                       (_creditCardCodeRepository = new CreditCardCodeRepository(DataContext));
            }
        }

        private IClinicTaskRepository _clinicTaskRepository;
        public IClinicTaskRepository ClinicTaskRepository
        {
            get { return _clinicTaskRepository ?? (_clinicTaskRepository = new ClinicTaskRepository(DataContext)); }
        }

        private IAttendanceLogRepository _attendanceLogRepository;
        public IAttendanceLogRepository AttendanceLogRepository
        {
            get { return _attendanceLogRepository ?? (_attendanceLogRepository = new AttendanceLogRepository(DataContext)); }
        }

        private IStatisticsRepository _statisticsRepository;
        public IStatisticsRepository StatitsticsRepository
        {
            get { return _statisticsRepository ?? (_statisticsRepository = new StatisticsRepository(DataContext)); }
        }

        private IInventoryRepository _inventoryRepository;
        public IInventoryRepository InventoryRepository
        {
            get { return _inventoryRepository ?? (_inventoryRepository = new InventoryRepository(DataContext)); }
        }

        private IInternalSettlementsRepository _internalSettlementsRepository;
        public IInternalSettlementsRepository InternalSettlementsRepository
        {
            get
            {
                return _internalSettlementsRepository ??
                       (_internalSettlementsRepository = new InternalSettlementsRepository(DataContext));
            }
        }

        private IGeneralReportsRepository _generalReportsRepository;
        public IGeneralReportsRepository GeneralReportsRepository
        {
            get
            {
                return _generalReportsRepository ??
                       (_generalReportsRepository = new GeneralReportsRepository(DataContext));
            }
        }

        private IFollowUpRepository _followUpRepository;
        public IFollowUpRepository FollowUpRepository
        {
            get { return _followUpRepository ?? (_followUpRepository = new FollowUpRepository(DataContext)); }
        }

        private IDataMigrationRepository _dataMigrationRepository;
        public IDataMigrationRepository DataMigrationRepository
        {
            get
            {
                return _dataMigrationRepository ??
                       (_dataMigrationRepository = new DataMigrationRepository.DataMigrationRepository(DataContext));
            }
        }

        private IRegionalCouncilsRepository _regionalCouncilsRepository;
        public IRegionalCouncilsRepository RegionalCouncilsRepository
        {
            get
            {
                return _regionalCouncilsRepository ??
                       (_regionalCouncilsRepository = new RegionalCouncilsRepository(DataContext));
            }
        }


        private IClinicTransferRepository _clinicTransferRepository;
        public IClinicTransferRepository ClinicTransferRepository
        {
            get
            {
                return _clinicTransferRepository ??
                       (_clinicTransferRepository = new ClinicTransferRepository(DataContext));
            }
        }

        private IClinicGroupDuplicateRepository _clinicGroupDuplicateRepository;
        public IClinicGroupDuplicateRepository ClinicGroupDuplicateRepository
        {
            get
            {
                return _clinicGroupDuplicateRepository ??
                       (_clinicGroupDuplicateRepository = new ClinicGroupDuplicateRepository(DataContext));
            }
        }

        private IAdminRepository _adminRepository;
        public IAdminRepository AdminRepository
        {
            get
            {
                return _adminRepository ??
                       (_adminRepository = new AdminRepository(DataContext));
            }
        }

        private ITemplateKeyWordsRepository _templateKeyWordsRepository;
        public ITemplateKeyWordsRepository TemplateKeyWordsRepository
        {
            get
            {
                return _templateKeyWordsRepository ??
                       (_templateKeyWordsRepository = new TemplateKeyWordsRepository(DataContext));
            }
        }

        private IMessageDistributionRepository _messageDistributionRepository;
        public IMessageDistributionRepository MessageDistributionRepository
        {
            get
            {
                return _messageDistributionRepository ??
                       (_messageDistributionRepository = new MessageDistributionRepository(DataContext));
            }
        }

        private IUserExtendedPermissionsRepository _userExtendedPermissionsRepository;
        public IUserExtendedPermissionsRepository UserExtendedPermissionsRepository
        {
            get
            {
                return _userExtendedPermissionsRepository ??
                       (_userExtendedPermissionsRepository = new UserExtendedPermissionsRepository(DataContext));
            }
        }       
    }
}
