﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Web.Configuration;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.CliinicInventory;
using RapidVet.Model.Finance;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Visits;
using RapidVet.Repository.Clinics;
using RapidVet.Repository.PreventiveMedicine;
using RapidVet.WebModels.Visits;
using RapidVet.Repository.MinistryOfAgricultureReports;

namespace RapidVet.Repository.Visits
{
    public class VisitRepository : RepositoryBase<RapidVetDataContext>, IVisitRepository
    {
        public VisitRepository()
        {

        }

        public VisitRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }



        public IEnumerable<PriceListCategory> GetVisitPriceListData(int clientId, int[] itemTypesId, int clientTariff)
        {
            var client = DataContext.Clients.Single(c => c.Id == clientId);

            var categories =
                DataContext.PriceListCategories
                           .Include(plc => plc.PriceListItems)
                           .Where(
                               plc =>
                               plc.Available && plc.ClinicId == client.ClinicId &&
                               plc.PriceListItems.Any(pli => itemTypesId.Contains(pli.ItemTypeId)))
                           .OrderBy(plc => plc.Name);

            foreach (var category in categories)
            {
                foreach (var item in category.PriceListItems)
                {
                    var itemTariff =
                        DataContext.PriceListItemsTariff.SingleOrDefault(
                            plit => plit.ItemId == item.Id && plit.TariffId == clientTariff);
                    if (itemTariff != null)
                    {
                        item.ItemsTariffs.Add(itemTariff);
                    }
                }
            }

            return categories;
        }

        public OperationStatus AddNewVisit(Visit visit, bool performItems = true)
        {
            DataContext.Visits.Add(visit);
            visit.CreatedDate = DateTime.Now;

            var status = base.Save(visit);
            if (status.Success)
            {
                UpdateInventoryForNewVisit(visit);

                if (performItems)
                {
                    if (visit.PreventiveMedicineItems.Any())
                    {
                        var clinicId = GetClinicIdForClient(visit.ClientIdAtTimeOfVisit);
                        using (var repo = new PreventiveMedicineRepository())
                        {
                            foreach (var item in visit.PreventiveMedicineItems)
                            {
                                if (item.Preformed.HasValue)
                                {
                                    repo.ItemPreformed(item.Id, clinicId);
                                }
                            }
                        }
                    }
                }
            }
            return status;
        }

        public void UpdateVitalSignsForVisit(Visit visit)
        {
            bool SaveChanges = false;
            if (visit.Temp != 0)
            {
                var TempVitalSign = DataContext.VitalSigns.SingleOrDefault(v => v.PatientId == visit.PatientId && v.VitalSignTypeId == (int)VitalSignType.Temp && v.VisitId == visit.Id);
                if (TempVitalSign != null)
                {
                    if (TempVitalSign.Value != visit.Temp)
                    {
                        TempVitalSign.Value = visit.Temp;
                        SaveChanges = true;
                    }
                }
                else
                {
                    SaveManualVitalSign(visit.PatientId.Value, (int)VitalSignType.Temp, DateTime.Now, visit.Temp, visit.Id);
                }
            }

            if (visit.Pulse != 0)
            {
                var PulseVitalSign = DataContext.VitalSigns.SingleOrDefault(v => v.PatientId == visit.PatientId &&
                                                                            v.VitalSignTypeId == (int)VitalSignType.Pulse &&
                                                                            v.VisitId == visit.Id);
                if (PulseVitalSign != null)
                {
                    if (PulseVitalSign.Value != visit.Pulse)
                    {
                        PulseVitalSign.Value = visit.Pulse;
                        SaveChanges = true;
                    }
                }
                else
                {
                    SaveManualVitalSign(visit.PatientId.Value, (int)VitalSignType.Pulse, DateTime.Now, visit.Pulse, visit.Id);
                }
            }

            if (visit.Weight != 0)
            {
                var WeightVitalSign = DataContext.VitalSigns.SingleOrDefault(v => v.PatientId == visit.PatientId &&
                                                                             v.VitalSignTypeId == (int)VitalSignType.Weight &&
                                                                             v.VisitId == visit.Id);
                if (WeightVitalSign != null)
                {
                    if (WeightVitalSign.Value != visit.Weight)
                    {
                        WeightVitalSign.Value = visit.Weight;
                        SaveChanges = true;
                    }
                }
                else
                {
                    SaveManualVitalSign(visit.PatientId.Value, (int)VitalSignType.Weight, DateTime.Now, visit.Weight, visit.Id);
                }
            }

            if (visit.BCS != 0)
            {
                var BCSVitalSign = DataContext.VitalSigns.SingleOrDefault(v => v.PatientId == visit.PatientId &&
                                                                             v.VitalSignTypeId == (int)VitalSignType.BCS &&
                                                                             v.VisitId == visit.Id);
                if (BCSVitalSign != null)
                {
                    if (BCSVitalSign.Value != visit.BCS)
                    {
                        BCSVitalSign.Value = visit.BCS;
                        SaveChanges = true;
                    }
                }
                else
                {
                    SaveManualVitalSign(visit.PatientId.Value, (int)VitalSignType.BCS, DateTime.Now, visit.BCS, visit.Id);
                }
            }

            if (visit.BPM != 0)
            {
                var BPMVitalSign = DataContext.VitalSigns.SingleOrDefault(v => v.PatientId == visit.PatientId &&
                                                                             v.VitalSignTypeId == (int)VitalSignType.Respiration &&
                                                                             v.VisitId == visit.Id);
                if (BPMVitalSign != null)
                {
                    if (BPMVitalSign.Value != visit.BPM)
                    {
                        BPMVitalSign.Value = visit.BPM;
                        SaveChanges = true;
                    }
                }
                else
                {
                    SaveManualVitalSign(visit.PatientId.Value, (int)VitalSignType.Respiration, DateTime.Now, visit.BPM, visit.Id);
                }
            }

            if (SaveChanges)
            {
                DataContext.SaveChanges();
            }
        }



        private void UpdateInventoryForNewVisit(Visit visit)
        {
            var clinicId = visit.PatientId != null ? GetClinicIdForPatient(visit.PatientId.Value) : GetClinicIdForClient(visit.ClientIdAtTimeOfVisit);
            var userName = DataContext.Users.Single(u => u.Id == visit.DoctorId).Name;

            foreach (var treatment in visit.Treatments.Where(v => v.PriceListItemId.HasValue && v.PriceListItemId.Value > 0))
            {
                if (IsInventoryManaged(clinicId, treatment.PriceListItemId.Value))
                {
                    var priceListItem = DataContext.PriceListItems.Single(i => i.Id == treatment.PriceListItemId.Value);
                    var quantity = treatment.Quantity * priceListItem.DefultConsuptionAmount;

                    var inventory = UpdateInventory(clinicId, treatment.PriceListItemId.Value, quantity,
                                                    treatment.UnitPrice, visit.DoctorId, visit.VisitDate, userName, visit.Id);
                    if (inventory != null)
                    {
                        inventory.VisitId = visit.Id;
                        DataContext.Inventory.Add(inventory);
                    }
                }
            }

            foreach (var examination in visit.Examinations.Where(v => v.PriceListItemId.HasValue && v.PriceListItemId.Value > 0))
            {
                if (IsInventoryManaged(clinicId, examination.PriceListItemId.Value))
                {
                    var inventory = UpdateInventory(clinicId, examination.PriceListItemId.Value, examination.Quantity,
                                                    examination.UnitPrice, visit.DoctorId, visit.VisitDate, userName, visit.Id);
                    if (inventory != null)
                    {
                        inventory.VisitId = visit.Id;
                        DataContext.Inventory.Add(inventory);
                    }
                }
            }

            foreach (var preventiveItem in visit.PreventiveMedicineItems)
            {
                if (IsInventoryManaged(clinicId, preventiveItem.PriceListItemId))
                {
                    var inventory = UpdateInventory(clinicId, preventiveItem.PriceListItemId, preventiveItem.Quantity,
                                                    preventiveItem.Price, visit.DoctorId, visit.VisitDate, userName, visit.Id);
                    if (inventory != null)
                    {
                        inventory.VisitId = visit.Id;
                        DataContext.Inventory.Add(inventory);
                    }
                }
            }

            DataContext.SaveChanges();
        }

        public Visit GetAndRemoveVisit(int id)
        {
            var oldVisit = DataContext.Visits
                .Include(v => v.Examinations)
                .Include(v => v.Diagnoses)
                .Include(v => v.Treatments)
                .Include(v => v.Medications)
                .Include(v => v.PreventiveMedicineItems)
                .Include(v => v.DangerousDrugs)
                .SingleOrDefault(v => v.Id == id);

            if (oldVisit == null)
            {
                return null;
            }
            //examinations
            foreach (var oe in oldVisit.Examinations.ToList())
            {
                DataContext.VisitExaminations.Remove(oe);
            }
            //treatments
            foreach (var ot in oldVisit.Treatments.ToList())
            {
                DataContext.VisitTreatments.Remove(ot);
            }
            //diagnoses
            foreach (var od in oldVisit.Diagnoses.ToList())
            {
                DataContext.VisitDiagnoses.Remove(od);
            }
            //medication 
            foreach (var m in oldVisit.Medications.ToList())
            {
                DataContext.VisitMedications.Remove(m);
            }
            DataContext.Visits.Remove(oldVisit);
            var res = base.Save(DataContext);
            if (!res.Success)
            {
                return null;
            }
            return oldVisit;
        }

        public List<Visit> GetPatientVisits(int patientId)
        {
            return DataContext.Visits.Where(v => v.PatientId == patientId).ToList();
        }

        public List<VitalSign> GetPatientManualVisits(int patientId)
        {
            return DataContext.VitalSigns.Where(v => v.PatientId == patientId).ToList();
        }

        public void SaveManualVitalSign(int id, int vitalSignID, DateTime dateTime, decimal val, int? visitId)
        {
            DataContext.VitalSigns.Add(new VitalSign()
            {
                PatientId = id,
                VitalSignTypeId = vitalSignID,
                Time = dateTime,
                Value = val,
                VisitId = null
            });
            DataContext.SaveChanges();
        }

        public void SaveVitalSign(int id, int manualID, VitalSignType vitalSignType, decimal val, DateTime vitalDate)
        {
            try
            {
                var vitalSign = DataContext.VitalSigns.Include(v => v.Visit).SingleOrDefault(v => v.Id == manualID);

                if (vitalSign != null)
                {
                    vitalSign.Value = val;
                    if (vitalDate != DateTime.MinValue)
                    {
                        vitalSign.Time = vitalDate;
                    }

                    if (vitalSign.Visit != null)
                    {
                        switch (vitalSignType)
                        {
                            case VitalSignType.Temp:
                                vitalSign.Visit.Temp = val;
                                break;
                            case VitalSignType.BCS:
                                vitalSign.Visit.BCS = (int)val;
                                break;
                            case VitalSignType.Pulse:
                                vitalSign.Visit.Pulse = (int)val;
                                break;
                            case VitalSignType.Respiration:
                                vitalSign.Visit.BPM = (int)val;
                                break;
                            case VitalSignType.Weight:
                                vitalSign.Visit.Weight = val;
                                break;
                        }
                    }
                }

                //if (manualID > 0)
                //{
                //    var f = DataContext.VitalSigns.FirstOrDefault(x => x.Id == manualID);
                //    if (f != null)
                //    {
                //        f.Value = val;
                //        if (vitalDate != DateTime.MinValue)
                //            f.Time = vitalDate;
                //    }

                //    return;
                //}

                //var f2 = DataContext.Visits.FirstOrDefault(x => x.Id == id);
                //if (f2 == null)
                //    return;

                //switch (vitalSignType)
                //{
                //    case VitalSignType.Temp:
                //        f2.Temp = val;
                //        break;
                //    case VitalSignType.BCS:
                //        f2.BCS = (int)val;
                //        break;
                //    case VitalSignType.Pulse:
                //        f2.Pulse = (int)val;
                //        break;
                //    case VitalSignType.Respiration:
                //        f2.BPM = (int)val;
                //        break;
                //    case VitalSignType.Weight:
                //        f2.Weight = val;
                //        break;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DataContext.SaveChanges();
            }
        }

        public OperationStatus DeleteVitalSign(int visitID, int manualId, VitalSignType vitalSignType)
        {
            OperationStatus status
                = new OperationStatus()
            {
                Success = false
            };
            try
            {
                var vitalSign = DataContext.VitalSigns.Include(v => v.Visit).SingleOrDefault(v => v.Id == manualId);

                if (vitalSign != null)
                {
                    if (vitalSign.Visit != null)
                    {
                        switch (vitalSignType)
                        {
                            case VitalSignType.Temp:
                                vitalSign.Visit.Temp = 0;
                                break;
                            case VitalSignType.BCS:
                                vitalSign.Visit.BCS = 0;
                                break;
                            case VitalSignType.Pulse:
                                vitalSign.Visit.Pulse = 0;
                                break;
                            case VitalSignType.Respiration:
                                vitalSign.Visit.BPM = 0;
                                break;
                            case VitalSignType.Weight:
                                vitalSign.Visit.Weight = 0;
                                break;
                        }
                    }

                    vitalSign.Visit = null;
                    vitalSign.VisitId = null;
                }

                DataContext.VitalSigns.Remove(vitalSign);

                //if (manualId > 0)
                //{
                //    var f = DataContext.VitalSigns.FirstOrDefault(x => x.Id == manualId);
                //    DataContext.VitalSigns.Remove(f);
                //    DataContext.SaveChanges();
                //}
                //else
                //{
                //    var f2 = DataContext.Visits.FirstOrDefault(x => x.Id == visitID);
                //    if (f2 == null)
                //        return status;

                //    switch (vitalSignType)
                //    {
                //        case VitalSignType.Temp:
                //            f2.Temp = 0;
                //            break;
                //        case VitalSignType.BCS:
                //            f2.BCS = 0;
                //            break;
                //        case VitalSignType.Pulse:
                //            f2.Pulse = 0;
                //            break;
                //        case VitalSignType.Respiration:
                //            f2.BPM = 0;
                //            break;
                //        case VitalSignType.Weight:
                //            f2.Weight = 0;
                //            break;
                //    }
                //    DataContext.SaveChanges();
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DataContext.SaveChanges();
            }
            return status;
        }

        public Visit GetVisit(int id)
        {
            return
                DataContext.Visits
                           .Include(v => v.Examinations)
                           .Include(v => v.Diagnoses)
                           .Include(v => v.Treatments)
                           .Include(v => v.Medications)
                           .Include("Medications.Medication")
                           .Include(v => v.PreventiveMedicineItems)
                           .Include("PreventiveMedicineItems.PriceListItem")
                           .Include("PreventiveMedicineItems.PriceListItem.Category")
                           .Include(v => v.DangerousDrugs)
                           .Include(v => v.Doctor)
                           .Include(v => v.Patient)
                           .Include("Patient.AnimalRace.AnimalKind")
                           .SingleOrDefault(v => v.Id == id);
        }

        public Visit GetLastVisitIfOpen(int patientId)
        {
            Visit visit;
            var visits =
                DataContext.Visits
                           .Include(v => v.Examinations)
                           .Include(v => v.Diagnoses)
                           .Include(v => v.Treatments)
                           .Include(v => v.Medications)
                           .Include("Medications.Medication")
                           .Include(v => v.PreventiveMedicineItems)
                           .Include("PreventiveMedicineItems.PriceListItem")
                           .Include("PreventiveMedicineItems.PriceListItem.Category")
                           .Include(v => v.DangerousDrugs)
                           .Include(v => v.Doctor)
                           .Include(v => v.Patient)
                           .Where(v => v.PatientId == patientId)
                           .OrderByDescending(v => v.VisitDate)
                           .Take(1).ToList();

            if (visits.Count() == 1 && !visits.First().Close && visits.First().VisitDate.Date.Equals(DateTime.Now.Date))
            {
                visit = visits.First();
            }
            else
            {
                visit = new Visit()
                    {
                        Id = 0,
                        PatientId = patientId,
                        VisitDate = DateTime.Now,
                        Close = false,
                        Active = true
                    };
            }

            return visit;
        }

        public DateTime? GetLastPatientVisitDate(int patientId, out int? visitId)
        {
            var lastVisit = DataContext.Visits.Where(v => v.PatientId == patientId && v.Active).OrderByDescending(v => v.VisitDate).FirstOrDefault();
            if (lastVisit == null)
            {
                visitId = null;
                return null;
            }

            visitId = lastVisit.Id;

            return lastVisit.VisitDate;
        }

        public OperationStatus UpdateVisit(Visit visit)
        {
            var status = new OperationStatus() { Success = false };
            var visitDb = DataContext.Visits
                .Include(v => v.Examinations)
                .Include(v => v.Diagnoses)
                .Include(v => v.Treatments)
                .Include(v => v.Medications)
                .Include(v => v.PreventiveMedicineItems)
                .Include(v => v.DangerousDrugs)
                .Include(v => v.Patient)
                .Include("Patient.Client")
                .SingleOrDefault(v => v.Id == visit.Id);

            if (visitDb == null)
            {
                return status;
            }

            visitDb.PatientId = visit.PatientId;
            visitDb.VisitDate = visit.VisitDate;
            visitDb.MainComplaint = visit.MainComplaint;
            visitDb.Weight = visit.Weight;
            visitDb.Temp = visit.Temp;
            visitDb.BPM = visit.BPM;
            visitDb.BCS = visit.BCS;
            visitDb.Pulse = visit.Pulse;
            visitDb.Mucosa = visit.Mucosa;
            visitDb.Color = visit.Color;
            visitDb.Price = visit.Price;
            visitDb.DoctorId = visit.DoctorId;
            visitDb.Close = visit.Close;
            visitDb.UpdatedDate = DateTime.Now;
            visitDb.Active = visit.Active;
            visitDb.VisitNote = visit.VisitNote;

            UpdateExaminationsForVisit(visitDb, visit);
            UpdateDiagnosesForVisit(visitDb, visit);
            UpdateTreatmentsForVisit(visitDb, visit);
            UpdateMedicationsForVisit(visitDb, visit);
            UpdatePreventiveMedicineItemsForVisit(visitDb, visit);
            UpdateDangerousDrugsItemsForVisit(visitDb, visit);
            status.Success = DataContext.SaveChanges() > -1;

            if (status.Success)
            {
                var preventiveItems =
                    DataContext.PreventiveMedicineItems.Where(
                        p => p.VisitId == visit.Id && (p.Scheduled == null || p.Scheduled.Value == DateTime.MinValue));

                if (preventiveItems.Any())
                {
                    var clinicId = visit.PatientId != null ? GetClinicIdForPatient(visit.PatientId.Value) : GetClinicIdForClient(visit.ClientIdAtTimeOfVisit);
                    using (var repo = new PreventiveMedicineRepository())
                    {
                        foreach (var item in visit.PreventiveMedicineItems)
                        {
                            repo.ItemPreformed(item.Id, clinicId);
                        }
                    }
                }
            }


            return status;
        }

        public IQueryable<Visit> GetVisitsOfDay(DateTime date, int clinicId, int issuerId, out int relatedDocotrs)
        {
            List<int> issuerDoctors = new List<int>();

            using (var repo = new ClinicRepository())
            {
                var clinicDoctors = repo.GetClinicDoctors(clinicId);

                foreach (var doctor in clinicDoctors)
                {
                    if (doctor.DefaultIssuerEmployerId == issuerId)
                    {
                        issuerDoctors.Add(doctor.Id);
                    }
                }
            }

            relatedDocotrs = issuerDoctors.Count;
            //var visitsWithoutPatientsIDs = DataContext.Visits.Include(v => v.Recipt).Where(v => v.ClientAtTimeOfVisit == clientId).AsEnumerable().Where(x => x.PatientId == null).Select(x => x.Id).ToList();
            //var visitsWithoutPatients = DataContext.Visits.Where(v => visitsWithoutPatientsIDs.Contains(v.Id));
            var visits = DataContext.Visits
                .Include(v => v.Recipt)
                .Include("Patient.Client")
                .Include(v => v.ClientAtTimeOfVisit)
                .Include("Doctor")
                .Include(v => v.ClientAtTimeOfVisit)
                //.Include(d => d.Recipt)
                .Where(v => v.ClientAtTimeOfVisit.ClinicId == clinicId &&
                    (
                    (v.VisitDate.Year == date.Year && v.VisitDate.Month == date.Month && v.VisitDate.Day == date.Day)// && !v.UpdatedDate.HasValue)
                    ||
                    (v.UpdatedDate.HasValue && v.UpdatedDate.Value.Year == date.Year && v.UpdatedDate.Value.Month == date.Month
                    && v.UpdatedDate.Value.Day == date.Day && v.Recipt != null && v.Recipt.FinanceDocumentTypeId != (int)Enums.Finances.FinanceDocumentType.Proforma)
                    )
                    &&
                    (
                    (issuerDoctors.Contains(v.DoctorId) && v.Recipt == null)
                    ||
                    (v.Recipt != null && v.Recipt.IssuerId == issuerId && v.Recipt.FinanceDocumentTypeId != (int)Enums.Finances.FinanceDocumentType.Proforma)
                    )
                    );

            foreach (var visit in visits.Where(v => v.Recipt != null))
            {
                var rec = visit.Recipt;
                if (rec != null)
                    visit.Recipts = DataContext.FinanceDocuments.Where(x => x.IssuerId == rec.IssuerId && x.ClientId == rec.ClientId && x.ClinicId == rec.ClinicId &&
                        x.UserId == rec.UserId && x.Created.Year == date.Year && x.Created.Month == date.Month && x.Created.Day == date.Day).ToList();

                visit.Recipt.Payments = DataContext.FinanceDocumentPayments.Include(p => p.BankCode).Where(p => p.ReciptId == visit.ReciptId || p.InvoiceId == visit.ReciptId || p.RefoundId == visit.ReciptId).ToList();
            }
            return visits;
        }

        public Client GetVisitClient(int visitId)
        {
            return
                DataContext.Visits.Include(v => v.Patient)
                           .Include("Patient.Client")
                           .Single(v => v.Id == visitId)
                           .Patient.Client;
        }

        public IQueryable<Visit> GetClintVisits(int clientId, bool includeAllVisitDetails = false)
        {
            if (!includeAllVisitDetails)
            {
                return DataContext.Visits.Include(v => v.Patient).Where(v => v.ClientIdAtTimeOfVisit == clientId && !v.DebtVisit.HasValue);
            }
            else
            {
                return DataContext.Visits.Include(v => v.Patient)
                                         .Include(v => v.Patient.LabTests)
                                         .Include(v => v.Diagnoses)
                                         .Include(v => v.Medications)
                                         .Include(v => v.PreventiveMedicineItems)
                                         .Include("PreventiveMedicineItems.PriceListItem")
                                         .Include(v => v.Examinations)
                                         .Include("Examinations.PriceListItem")
                                         .Include(v => v.Treatments)
                                         .Include("Treatments.PriceListItem")
                                         .Where(v => v.ClientIdAtTimeOfVisit == clientId && !v.DebtVisit.HasValue);
            }
        }


        //use to include NULL patients
        public IQueryable<Visit> GetClientVisitsForPayment(int clientId)
        {
            //var visitsWithoutPatients = (from visit in DataContext.Visits where visit.PatientId == null select visit);
            var visitsWithoutPatientsIDs = DataContext.Visits.Where(v => v.ClientIdAtTimeOfVisit == clientId).AsEnumerable().Where(x => x.PatientId == null).Select(x => x.Id).ToList();
            var visitsWithoutPatients = DataContext.Visits.Where(v => visitsWithoutPatientsIDs.Contains(v.Id));

            return
                DataContext.Visits.Include(v => v.Patient)
                           .Where(v => v.ClientIdAtTimeOfVisit == clientId).Union(visitsWithoutPatients);
        }

        private void UpdateExaminationsForVisit(Visit dataBaseModel, Visit webModel)
        {
            var clinicId = webModel.PatientId != null ? GetClinicIdForPatient(webModel.PatientId.Value) : GetClinicIdForClient(webModel.ClientIdAtTimeOfVisit);
            var userId = webModel.DoctorId;
            var userName = DataContext.Users.Single(u => u.Id == webModel.DoctorId).Name;

            var dbModelList = dataBaseModel.Examinations.ToList();
            // Delete removed webmodel examinations
            foreach (var examinationDb in dbModelList)
            {
                var examinationWebModel = webModel.Examinations.SingleOrDefault(e => e.Id == examinationDb.Id);
                if (examinationWebModel == null)
                {
                    if (examinationDb.QuotationTreatmentId != null)
                    {
                        removeQuotationTreatment(examinationDb.QuotationTreatmentId.Value);
                    }
                    DataContext.VisitExaminations.Remove(examinationDb);
                }
            }
            // Add/Update added/changed webmodel examinations
            foreach (var examinationWebModel in webModel.Examinations)
            {
                // Update existing examination
                if (examinationWebModel.Id > 0)
                {
                    var examinationDb = dataBaseModel.Examinations.SingleOrDefault(e => e.Id == examinationWebModel.Id);
                    if (examinationDb != null)
                    {
                        examinationDb.Name = examinationWebModel.Name;
                        examinationDb.VisitId = webModel.Id;
                        examinationDb.PriceListItemId = examinationWebModel.PriceListItemId;
                        examinationDb.CategoryName = examinationWebModel.CategoryName;
                        examinationDb.Quantity = examinationWebModel.Quantity;
                        examinationDb.UnitPrice = examinationWebModel.UnitPrice;
                        examinationDb.Discount = examinationWebModel.Discount;
                        examinationDb.DiscountPercent = examinationWebModel.DiscountPercent;
                        examinationDb.Price = examinationWebModel.Price;
                        examinationDb.AddedBy = examinationWebModel.AddedBy;
                        examinationDb.AddedByUserId = examinationWebModel.AddedByUserId;
                    }
                }
                // Add new examination
                else
                {
                    examinationWebModel.VisitId = webModel.Id;
                    DataContext.VisitExaminations.Add(examinationWebModel);

                    if (examinationWebModel.PriceListItemId.HasValue && examinationWebModel.PriceListItemId.Value > 0)
                    {
                        var inventory = UpdateInventory(clinicId, examinationWebModel.PriceListItemId.Value, examinationWebModel.Quantity,
                                         examinationWebModel.UnitPrice, userId, webModel.VisitDate, userName, dataBaseModel.Id);
                        if (inventory != null)
                        {
                            inventory.VisitId = dataBaseModel.Id;
                            DataContext.Inventory.Add(inventory);
                        }
                    }
                }
            }

            if (!webModel.Active)
            {
                foreach (var examination in dataBaseModel.Examinations)
                {
                    if (examination.QuotationTreatmentId != null)
                    {
                        removeQuotationTreatment(examination.QuotationTreatmentId.Value);
                        examination.QuotationTreatmentId = null;
                    }
                }
            }

        }

        private void removeQuotationTreatment(int quotationTreatmentId)
        {
            var quotationTreatment = DataContext.QuotationTreatments.SingleOrDefault(qt => qt.Id == quotationTreatmentId);

            if (quotationTreatment != null)
            {
                quotationTreatment.WasTreatmentMovedToExecution = false;
                quotationTreatment.VisitId = null;
                DataContext.SaveChanges();
            }
        }

        private void UpdateDiagnosesForVisit(Visit dataBaseModel, Visit webModel)
        {
            var dbModelList = dataBaseModel.Diagnoses.ToList();
            // Delete removed webmodel Diagnoses
            foreach (var diagnosisDb in dbModelList)
            {
                var diagnosisWebModel = webModel.Diagnoses.SingleOrDefault(d => d.Id == diagnosisDb.Id);
                if (diagnosisWebModel == null)
                    DataContext.VisitDiagnoses.Remove(diagnosisDb);
            }
            // Add/Update added/changed webmodel Diagnoses
            foreach (var diagnosisWebModel in webModel.Diagnoses)
            {
                // Update existing Diagnoses
                if (diagnosisWebModel.Id > 0)
                {
                    var diagnosisDb = dataBaseModel.Diagnoses.SingleOrDefault(e => e.Id == diagnosisWebModel.Id);
                    if (diagnosisDb != null)
                    {
                        diagnosisDb.Name = diagnosisWebModel.Name;
                        diagnosisDb.VisitId = webModel.Id;
                        diagnosisDb.DiagnosisId = diagnosisWebModel.DiagnosisId;
                        diagnosisDb.CategoryName = diagnosisWebModel.CategoryName;
                        diagnosisDb.AddedBy = diagnosisWebModel.AddedBy;
                    }
                }
                // Add new Diagnosis
                else
                {
                    diagnosisWebModel.VisitId = webModel.Id;
                    DataContext.VisitDiagnoses.Add(diagnosisWebModel);
                }
                DataContext.SaveChanges();
            }
        }

        private void UpdateTreatmentsForVisit(Visit dataBaseModel, Visit webModel)
        {
            var clinicId = webModel.PatientId != null ? GetClinicIdForPatient(webModel.PatientId.Value) : GetClinicIdForClient(webModel.ClientIdAtTimeOfVisit);
            var userId = webModel.DoctorId;
            var userName = DataContext.Users.Single(u => u.Id == webModel.DoctorId).Name;

            var dbModelList = dataBaseModel.Treatments.ToList();
            // Delete removed webmodel Treatments
            foreach (var treatmentDb in dbModelList)
            {
                var treatmentWebModel = webModel.Treatments.SingleOrDefault(e => e.Id == treatmentDb.Id);
                if (treatmentWebModel == null)
                {
                    if (treatmentDb.QuotationTreatmentId != null)
                    {
                        removeQuotationTreatment(treatmentDb.QuotationTreatmentId.Value);
                    }
                    DataContext.VisitTreatments.Remove(treatmentDb);
                }
            }
            // Add/Update added/changed webmodel Treatments
            foreach (var treatmentWebModel in webModel.Treatments)
            {
                // Update existing Treatments
                if (treatmentWebModel.Id > 0)
                {
                    var treatmentDb = dataBaseModel.Treatments.SingleOrDefault(e => e.Id == treatmentWebModel.Id);
                    if (treatmentDb != null)
                    {
                        treatmentDb.Name = treatmentWebModel.Name;
                        treatmentDb.VisitId = webModel.Id;
                        treatmentDb.PriceListItemId = treatmentWebModel.PriceListItemId;
                        treatmentDb.CategoryName = treatmentWebModel.CategoryName;
                        treatmentDb.Quantity = treatmentWebModel.Quantity;
                        treatmentDb.UnitPrice = treatmentWebModel.UnitPrice;
                        treatmentDb.Discount = treatmentWebModel.Discount;
                        treatmentDb.DiscountPercent = treatmentWebModel.DiscountPercent;
                        treatmentDb.Price = treatmentWebModel.Price;
                        treatmentDb.AddedBy = treatmentWebModel.AddedBy;
                    }
                }
                // Add new Treatment
                else
                {
                    treatmentWebModel.VisitId = webModel.Id;
                    DataContext.VisitTreatments.Add(treatmentWebModel);
                    if (treatmentWebModel.PriceListItemId.HasValue && treatmentWebModel.PriceListItemId.Value > 0)
                    {
                        var inventory = UpdateInventory(clinicId, treatmentWebModel.PriceListItemId.Value, treatmentWebModel.Quantity,
                                         treatmentWebModel.UnitPrice, userId, webModel.VisitDate, userName, dataBaseModel.Id);
                        if (inventory != null)
                        {
                            inventory.VisitId = dataBaseModel.Id;
                            DataContext.Inventory.Add(inventory);
                        }
                    }
                }
            }

            if (!webModel.Active)
            {
                foreach (var treatment in dataBaseModel.Treatments)
                {
                    if (treatment.QuotationTreatmentId != null)
                    {
                        removeQuotationTreatment(treatment.QuotationTreatmentId.Value);
                        treatment.QuotationTreatmentId = null;
                    }
                }
            }
        }
        private void UpdateMedicationsForVisit(Visit dataBaseModel, Visit webModel)
        {
            var dbModelList = dataBaseModel.Medications.ToList();
            // Delete removed webmodel Medications
            foreach (var medicationDb in dbModelList)
            {
                var medicationWebModel = webModel.Medications.SingleOrDefault(e => e.Id == medicationDb.Id);
                if (medicationWebModel == null)
                {
                    DataContext.VisitMedications.Remove(medicationDb);
                }
            }
            // Add/Update added/changed webmodel Medications
            foreach (var medicationWebModel in webModel.Medications)
            {
                // Update existing Medications
                if (medicationWebModel.Id > 0)
                {
                    var medicationDb = dataBaseModel.Medications.SingleOrDefault(e => e.Id == medicationWebModel.Id);
                    if (medicationDb != null)
                    {
                        medicationDb.VisitId = webModel.Id;
                        medicationDb.MedicationId = medicationWebModel.MedicationId;
                        medicationDb.Name = medicationWebModel.Name;
                        medicationDb.DrugAdministrationId = medicationWebModel.DrugAdministrationId;
                        medicationDb.Dosage = medicationWebModel.Dosage;
                        medicationDb.TimesPerDay = medicationWebModel.TimesPerDay;
                        medicationDb.NumberOfDays = medicationWebModel.NumberOfDays;
                        medicationDb.UnitPrice = medicationWebModel.UnitPrice;
                        medicationDb.Total = medicationWebModel.Total;
                        medicationDb.Instructions = medicationWebModel.Instructions;
                        medicationDb.Discount = medicationWebModel.Discount;
                        medicationDb.DiscountPercent = medicationWebModel.DiscountPercent;
                        medicationDb.AddedBy = medicationWebModel.AddedBy;
                        medicationDb.AddedByUserId = medicationWebModel.AddedByUserId;
                    }
                }
                // Add new Medication
                else
                {
                    medicationWebModel.VisitId = webModel.Id;
                    DataContext.VisitMedications.Add(medicationWebModel);
                }
            }
        }
        private void UpdatePreventiveMedicineItemsForVisit(Visit dataBaseModel, Visit webModel)
        {
            var clinicId = webModel.PatientId != null ? GetClinicIdForPatient(webModel.PatientId.Value) : GetClinicIdForClient(webModel.ClientIdAtTimeOfVisit);
            var userId = webModel.DoctorId;
            var userName = DataContext.Users.Single(u => u.Id == webModel.DoctorId).Name;

            var dbModelList = dataBaseModel.PreventiveMedicineItems.ToList();
            // Delete removed webmodel PreventiveMedicineItems
            foreach (var preventiveMedicineItemDb in dbModelList)
            {
                var preventiveMedicineItemWebModel = webModel.PreventiveMedicineItems.SingleOrDefault(e => e.Id == preventiveMedicineItemDb.Id);
                if (preventiveMedicineItemWebModel == null)
                {
                    if (preventiveMedicineItemDb.QuotationTreatmentId != null)
                    {
                        removeQuotationTreatment(preventiveMedicineItemDb.QuotationTreatmentId.Value);
                    }
                    //DataContext.PreventiveMedicineItems.Remove(preventiveMedicineItemDb);
                    RemovePreventiveMedicineItemFromVisit(preventiveMedicineItemDb);
                }
            }
            // Add/Update added/changed webmodel PreventiveMedicineItems
            foreach (var preventiveMedicineItemWebModel in webModel.PreventiveMedicineItems)
            {
                // Update existing PreventiveMedicineItems
                if (preventiveMedicineItemWebModel.Id > 0)
                {
                    // Update existing PreventiveMedicineItems
                    var preventiveMedicineItemDb = dataBaseModel.PreventiveMedicineItems.SingleOrDefault(e => e.Id == preventiveMedicineItemWebModel.Id);
                    if (preventiveMedicineItemDb != null)
                    {
                        preventiveMedicineItemDb.PatientId = preventiveMedicineItemWebModel.PatientId;
                        preventiveMedicineItemDb.PriceListItemTypeId = preventiveMedicineItemWebModel.PriceListItemTypeId;
                        preventiveMedicineItemDb.DocumentId = preventiveMedicineItemWebModel.DocumentId;
                        preventiveMedicineItemDb.PriceListItemId = preventiveMedicineItemWebModel.PriceListItemId;
                        preventiveMedicineItemDb.VisitId = webModel.Id;
                        preventiveMedicineItemDb.PreformingUserId = preventiveMedicineItemWebModel.PreformingUserId;
                        preventiveMedicineItemDb.Comments = preventiveMedicineItemWebModel.Comments;
                        preventiveMedicineItemDb.ExternalyPreformed = preventiveMedicineItemWebModel.ExternalyPreformed;
                        preventiveMedicineItemDb.Price = preventiveMedicineItemWebModel.Price;
                    }
                }
                // Add new PreventiveMedicineItem
                else
                {
                    preventiveMedicineItemWebModel.VisitId = webModel.Id;
                    DataContext.PreventiveMedicineItems.Add(preventiveMedicineItemWebModel);


                    var inventory = UpdateInventory(clinicId, preventiveMedicineItemWebModel.PriceListItemId, preventiveMedicineItemWebModel.Quantity,
                                         preventiveMedicineItemWebModel.Price, userId, webModel.VisitDate, userName, dataBaseModel.Id);
                    if (inventory != null)
                    {
                        inventory.VisitId = dataBaseModel.Id;
                        DataContext.Inventory.Add(inventory);
                    }
                }
            }

            using (var pmr = new PreventiveMedicineRepository())
            using (var mar = new MinistryOfAgricultureReportsRepository())
            {
                if (!webModel.Active)
                {
                    var dbModelPreventiveMedicineItems = dataBaseModel.PreventiveMedicineItems.ToList();

                    foreach (var preventiveItem in dbModelPreventiveMedicineItems)
                    {
                        if (preventiveItem.QuotationTreatmentId != null)
                        {
                            removeQuotationTreatment(preventiveItem.QuotationTreatmentId.Value);
                            preventiveItem.QuotationTreatmentId = null;
                        }

                        var isRabiesVaccine = pmr.IsRabiesVaccine(preventiveItem.PriceListItemId);

                        if (isRabiesVaccine)
                        {
                            mar.DeleteRabiesReportByVisitId(dataBaseModel.Id);
                        }
                        RemovePreventiveMedicineItemFromVisit(preventiveItem);
                    }
                }
            }
        }

        private void RemovePreventiveMedicineItemFromVisit(PreventiveMedicineItem preventiveItem)
        {
            var childItems =
                DataContext.PreventiveMedicineItems.Where(p => p.ParentPreventiveItemId == preventiveItem.Id);
            if (childItems.Any())
            {
                foreach (var child in childItems)
                {
                    if (child.Preformed.HasValue && child.Preformed.Value > DateTime.MinValue)
                    {
                        child.Parent = null;
                        child.ParentPreventiveItemId = null;
                    }
                    else
                    {
                        DataContext.PreventiveMedicineItems.Remove(child);
                    }
                }
            }
            DataContext.PreventiveMedicineItems.Remove(preventiveItem);
        }

        private void UpdateDangerousDrugsItemsForVisit(Visit dataBaseModel, Visit webModel)
        {
            var dbModelList = dataBaseModel.DangerousDrugs.ToList();
            // Delete removed webmodel DangerousDrugsItems
            foreach (var dangerousDrugDb in dbModelList)
            {
                var dangerousDrugWebModel = webModel.DangerousDrugs.SingleOrDefault(e => e.Id == dangerousDrugDb.Id);
                if (dangerousDrugWebModel == null)
                {
                    DataContext.DangerousDrugs.Remove(dangerousDrugDb);
                }
            }
            // Add/Update added/changed webmodel DangerousDrugsItems
            foreach (var dangerousDrugWebModel in webModel.DangerousDrugs)
            {
                // Update existing DangerousDrugsItems
                if (dangerousDrugWebModel.Id > 0)
                {
                    var dangerousDrugDb = dataBaseModel.DangerousDrugs.SingleOrDefault(e => e.Id == dangerousDrugWebModel.Id);
                    if (dangerousDrugDb != null)
                    {
                        dangerousDrugDb.VisitId = webModel.Id;
                        dangerousDrugDb.MedicationId = dangerousDrugWebModel.MedicationId;
                        dangerousDrugDb.AmountInjected = dangerousDrugWebModel.AmountInjected;
                        dangerousDrugDb.AmountDestroyed = dangerousDrugWebModel.AmountDestroyed;
                    }
                }
                // Add new DangerousDrugsItem
                else
                {
                    dangerousDrugWebModel.VisitId = webModel.Id;
                    DataContext.DangerousDrugs.Add(dangerousDrugWebModel);
                }
            }
        }

        public List<PriceListItem> GetAllPriceListItems(int clinicId)
        {
            return DataContext.PriceListItems.Include(p => p.Category).Where(p => p.Category.ClinicId == clinicId && p.Available).ToList();
        }

        public List<PriceListItem> GetAllTreatments(int clinicId)
        {
            return DataContext.PriceListItems.Include(p => p.Category).Where(p => p.Category.ClinicId == clinicId && p.Available && p.ItemTypeId == (int)PriceListItemTypeEnum.Treatments).ToList();
        }

        public List<PriceListItem> GetAllExaminations(int clinicId)
        {
            return DataContext.PriceListItems.Include(p => p.Category).Where(p => p.Category.ClinicId == clinicId && p.Available && p.ItemTypeId == (int)PriceListItemTypeEnum.Examinations).ToList();
        }

        public List<Medication> GetAllMedications(int clinicGroupId)
        {
            return DataContext.Medications.Where(p => p.ClinicGroupId == clinicGroupId && p.Active).ToList();
        }

        public List<VisitReportItem> GetFilteredVisits(VisitFilterWebModel filter, int clinicId)
        {
            var visits = DataContext.Visits
                        .Include("Patient.Client")
                        .Include("Patient.Client.Title")
                        .Include("Recipt")
                        .Include("Doctor")
                        .Include("Patient.AnimalRace.AnimalKind")
                        .Include("Patient.AnimalColor")
                        .Include("Treatments.PriceListItem")
                        .Include("Examinations.PriceListItem")
                        .Include("PreventiveMedicineItems.PriceListItem")
                        .Include("Treatments.PriceListItem.Category")
                        .Include("Examinations.PriceListItem.Category")
                        .Include("PreventiveMedicineItems.PriceListItem.Category")
                        .Include("Patient.Client.Phones")
                        .Include("Patient.Client.Addresses.City")
                        .Include("Examinations")
                        .Include("Diagnoses")
                        .Include(v => v.Treatments)
                        .Include(v => v.PreventiveMedicineItems)
                        .Include(v => v.Medications)
                        .Where(v => v.Patient.Client.ClinicId == clinicId && v.Active);

            List<int> categoriesIds = null;
            List<int> treatmentsIds = null;

            if (!string.IsNullOrEmpty(filter.TreatmentId))
            {
                treatmentsIds = filter.TreatmentId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();
            }

            if (!string.IsNullOrEmpty(filter.CategoryId))
            {
                categoriesIds = filter.CategoryId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();
            }

            //DateFromFilter
            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var fromDate = StringUtils.ParseStringToDateTime(filter.FromDate);
                visits = visits.Where(v => fromDate <= v.VisitDate);
            }


            //DateToFilter
            if (!string.IsNullOrWhiteSpace(filter.ToDate))
            {
                var toDate = StringUtils.ParseStringToDateTime(filter.ToDate).AddDays(1);//add days on order to look all this todate times
                visits = visits.Where(v => v.VisitDate <= toDate);
            }

            //ClientFilter
            if (filter.ClientId.HasValue && filter.ClientId.Value > 0)
            {
                visits = visits.Where(v => v.Patient.ClientId == filter.ClientId.Value);
            }

            //AnimalKind Filter
            if (filter.AnimalKindId.HasValue && filter.AnimalKindId.Value > 0)
            {
                visits = visits.Where(v => v.Patient.AnimalRace.AnimalKindId == filter.AnimalKindId.Value);
            }

            //AgeFilter
            if ((filter.FromYear.HasValue || filter.FromMonth.HasValue) && (filter.ToYear.HasValue || filter.ToMonth.HasValue))
            {
                if (!filter.FromYear.HasValue)
                {
                    filter.FromYear = 0;
                }
                if (!filter.FromMonth.HasValue)
                {
                    filter.FromMonth = 0;
                }
                if (!filter.ToYear.HasValue)
                {
                    filter.ToYear = 0;
                }
                if (!filter.ToMonth.HasValue)
                {
                    filter.ToMonth = 0;
                }
                var birthFromdate = DateTime.Now.AddYears(-filter.FromYear.Value).AddMonths(-filter.FromMonth.Value);
                var birthToDate = DateTime.Now.AddYears(-filter.ToYear.Value).AddMonths(-filter.ToMonth.Value);

                visits = visits.Where(v => birthToDate <= v.Patient.BirthDate && v.Patient.BirthDate <= birthFromdate);
            }

            //Doctor Filter
            if (!string.IsNullOrEmpty(filter.DoctorId))
            {
                var idsList = filter.DoctorId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();
                visits = visits.Where(v => idsList.Any(s => s == v.DoctorId));
            }

            //Sum Filter
            if (filter.FromSum.HasValue && filter.ToSum.HasValue)
            {
                visits = visits.Where(v => filter.FromSum.Value <= v.Price && v.Price <= filter.ToSum.Value);
                //visitTreatments = visitTreatments.Where(v => filter.FromSum.Value <= v.Price && v.Price <= filter.ToSum.Value);
                //preventiveMedicines = preventiveMedicines.Where(v => filter.FromSum.Value <= v.Price && v.Price <= filter.ToSum.Value);
                //visitExaminations = visitExaminations.Where(v => filter.FromSum.Value <= v.Price && v.Price <= filter.ToSum.Value);
                //visitMedications = visitMedications.Where(v => filter.FromSum.Value <= v.Total && v.Total <= filter.ToSum.Value);
            }

            //Keyword Filter
            if (!string.IsNullOrWhiteSpace(filter.VisitDescription))
            {
                visits = visits.Where(v => v.MainComplaint.Contains(filter.VisitDescription));
            }

            //arrange data
            var reports = new List<VisitReportItem>();
            using (var letterTemplatesRepository = new LetterTemplatesRepository())
            {
                foreach (var visit in visits)
                {
                    foreach (var v in visit.Treatments) //category, treatments
                    {
                        var relevant = true;
                        if (categoriesIds != null && treatmentsIds != null)
                        {
                            if (!v.PriceListItemId.HasValue)
                            {
                                relevant = false;
                            }
                            else if (!treatmentsIds.Contains(v.PriceListItemId.Value) || !categoriesIds.Contains(v.PriceListItem.CategoryId))
                            {
                                relevant = false;
                            }
                        }
                        else if (categoriesIds != null)
                        {
                            if (!categoriesIds.Contains(v.PriceListItem.CategoryId))
                            {
                                relevant = false;
                            }
                        }
                        else if (treatmentsIds != null)
                        {
                            if (!treatmentsIds.Contains(v.PriceListItemId ?? -1))
                            {
                                relevant = false;
                            }
                        }
                        else if (filter.DiagnosisId.HasValue && filter.DiagnosisId.Value > 0 || filter.MedicationId.HasValue && filter.MedicationId.Value > 0)
                        {
                            relevant = false;
                        }

                        if (relevant)
                        {
                            if (string.IsNullOrEmpty(v.Name) || !v.Name.Equals(Resources.Visit.OpenBalance))
                            {
                                var reportItem = new VisitReportItem();
                                reportItem.nameVisit = v.Name;
                                reportItem.datePerformed = v.Visit.VisitDate;
                                reportItem.clientName = string.Format("{0} {1}", v.Visit.Patient.Client.FirstName, v.Visit.Patient.Client.LastName);
                                reportItem.clientFirstName = v.Visit.Patient.Client.FirstName;
                                reportItem.clientLastName = v.Visit.Patient.Client.LastName;
                                reportItem.patientName = v.Visit.Patient.Name;
                                reportItem.serial = v.Visit.Patient.Client.IdCard;
                                reportItem.address = letterTemplatesRepository.GetClientAddress(v.Visit.Patient.Client.Addresses);
                                reportItem.homePhone = letterTemplatesRepository.GetClientPhone(v.Visit.Patient.Client.Phones,
                                                                                                PhoneTypeEnum.Home);
                                reportItem.cellPhone = letterTemplatesRepository.GetClientPhone(v.Visit.Patient.Client.Phones,
                                                                                                PhoneTypeEnum.Mobile);
                                reportItem.animalKind = v.Visit.Patient.AnimalRace.AnimalKind.Value;
                                reportItem.animalRace = v.Visit.Patient.AnimalRace.Value;
                                reportItem.animalGender = v.Visit.Patient.GenderId == 1
                                                              ? (Resources.Gender.Male)
                                                              : (v.Visit.Patient.GenderId == 2 ? Resources.Gender.Female : Resources.Gender.UndefinedSex);
                                reportItem.animalColor = v.Visit.Patient.AnimalColorId.HasValue
                                                             ? v.Visit.Patient.AnimalColor.Value
                                                             : string.Empty;
                                if (v.Visit.Patient.BirthDate.HasValue)
                                {
                                    //reportItem.animalAge = ((DateTime.Now - v.Visit.Patient.BirthDate.Value).Days / (decimal)365.0);
                                    AgeHelper age = new AgeHelper(v.Visit.Patient.BirthDate.Value, DateTime.Today);
                                    reportItem.animalAge = age.Years + "." + age.Months;
                                }
                                else
                                {
                                    reportItem.animalAge = "";
                                }

                                reportItem.sterilization = v.Visit.Patient.Sterilization ? Resources.Global.Yes : Resources.Global.No;
                                reportItem.electronicNum = v.Visit.Patient.ElectronicNumber;
                                reportItem.birthDate = v.Visit.Patient.BirthDate;
                                reportItem.licenseNum = v.Visit.Patient.LicenseNumber;
                                reportItem.chargeFee = v.Price;
                                reportItem.performingDoc = string.Format("{0} {1}", v.Visit.Doctor.FirstName, v.Visit.Doctor.LastName);
                                reportItem.numOfUnits = v.Quantity;
                                //reportItem.plateNum = "";
                                reportItem.visitDesc = v.Visit.MainComplaint;
                                reportItem.treatingDoctor = string.Format("{0} {1}", v.Visit.Doctor.FirstName, v.Visit.Doctor.LastName);
                                reportItem.receipt = v.Visit.ReciptId.HasValue ? v.Visit.Recipt.SerialNumber : (int?)null;
                                reportItem.clientId = v.Visit.Patient.Client.Id;
                                reportItem.patientId = v.Visit.Patient.Id;
                                reportItem.visitId = v.Visit.Id;
                                reportItem.patientId = v.Visit.Patient.Id;
                                reportItem.visitId = v.Visit.Id;

                                reportItem.clientReferer = v.Visit.Patient.Client.ReferredBy;
                                reportItem.clientBalance = v.Visit.Patient.Client.Balance;
                                if (v.Visit.Patient.Client.BirthDate.HasValue)
                                {
                                    AgeHelper age = new AgeHelper(v.Visit.Patient.Client.BirthDate.Value, DateTime.Today);
                                    reportItem.clientAge = age.Years + "." + age.Months;
                                    reportItem.clientBirthdate = v.Visit.Patient.Client.BirthDate.Value.ToShortDateString();
                                }
                                else
                                {
                                    reportItem.clientAge = "";
                                    reportItem.clientBirthdate = "";
                                }

                                reportItem.clientTitle = v.Visit.Patient.Client.TitleId.HasValue ? v.Visit.Patient.Client.Title.Name : string.Empty;
                                reportItem.clientZip = letterTemplatesRepository.GetClientZipCode(v.Visit.Patient.Client.Addresses);
                                reportItem.clientCity = letterTemplatesRepository.GetClientCity(v.Visit.Patient.Client.Addresses);
                                reportItem.clientStreetAndNumber = letterTemplatesRepository.GetClientStreetAndNumber(v.Visit.Patient.Client.Addresses);
                                reportItem.clientRecordNumber = v.Visit.Patient.Client.LocalId;

                                reports.Add(reportItem);
                            }
                        }
                    }
                    foreach (var v in visit.PreventiveMedicineItems) //vaccines, category
                    {
                        var pmiRelevant = true;

                        if (categoriesIds != null && treatmentsIds != null)
                        {
                            if (!treatmentsIds.Contains(v.PriceListItemId) || !categoriesIds.Contains(v.PriceListItem.CategoryId))
                            {
                                pmiRelevant = false;
                            }
                        }
                        else if (categoriesIds != null)
                        {
                            if (!categoriesIds.Contains(v.PriceListItem.CategoryId))
                            {
                                pmiRelevant = false;
                            }
                        }
                        else if (treatmentsIds != null)
                        {
                            if (!treatmentsIds.Contains(v.PriceListItemId))
                            {
                                pmiRelevant = false;
                            }
                        }
                        else if (filter.DiagnosisId.HasValue && filter.DiagnosisId.Value > 0 || filter.MedicationId.HasValue && filter.MedicationId.Value > 0)
                        {
                            pmiRelevant = false;
                        }


                        if (pmiRelevant)
                        {
                            var reportItem = new VisitReportItem();
                            reportItem.nameVisit = v.PriceListItem.Name;
                            reportItem.datePerformed = v.Visit.VisitDate;
                            reportItem.clientName = string.Format("{0} {1}", v.Visit.Patient.Client.FirstName, v.Visit.Patient.Client.LastName);
                            reportItem.clientFirstName = v.Visit.Patient.Client.FirstName;
                            reportItem.clientLastName = v.Visit.Patient.Client.LastName;
                            reportItem.patientName = v.Visit.Patient.Name;
                            reportItem.serial = v.Visit.Patient.Client.IdCard;
                            reportItem.address = letterTemplatesRepository.GetClientAddress(v.Visit.Patient.Client.Addresses);
                            reportItem.homePhone = letterTemplatesRepository.GetClientPhone(v.Visit.Patient.Client.Phones,
                                                                                            PhoneTypeEnum.Home);
                            reportItem.cellPhone = letterTemplatesRepository.GetClientPhone(v.Visit.Patient.Client.Phones,
                                                                                            PhoneTypeEnum.Mobile);
                            reportItem.animalKind = v.Visit.Patient.AnimalRace.AnimalKind.Value;
                            reportItem.animalRace = v.Visit.Patient.AnimalRace.Value;
                            reportItem.animalGender = v.Visit.Patient.GenderId == 1
                                                          ? (Resources.Gender.Male)
                                                          : (v.Visit.Patient.GenderId == 2 ? Resources.Gender.Female : Resources.Gender.UndefinedSex);
                            reportItem.animalColor = v.Visit.Patient.AnimalColorId.HasValue
                                                         ? v.Visit.Patient.AnimalColor.Value
                                                         : "";
                            if (v.Visit.Patient.BirthDate.HasValue)
                            {
                                // reportItem.animalAge = ((DateTime.Now - v.Visit.Patient.BirthDate.Value).Days / (decimal)365.0);
                                AgeHelper age = new AgeHelper(v.Visit.Patient.BirthDate.Value, DateTime.Today);
                                reportItem.animalAge = age.Years + "." + age.Months;
                            }
                            else
                            {
                                reportItem.animalAge = "";
                            }

                            reportItem.sterilization = v.Visit.Patient.Sterilization ? Resources.Global.Yes : Resources.Global.No;
                            reportItem.electronicNum = v.Visit.Patient.ElectronicNumber;
                            reportItem.birthDate = v.Visit.Patient.BirthDate;
                            reportItem.licenseNum = v.Visit.Patient.LicenseNumber;
                            reportItem.chargeFee = (v.Price * v.Quantity) - (v.Price * v.Quantity) * (v.DiscountPercent / 100) - v.Discount;
                            reportItem.performingDoc = string.Format("{0} {1}", v.Visit.Doctor.FirstName, v.Visit.Doctor.LastName);
                            reportItem.numOfUnits = v.Quantity;
                            //reportItem.plateNum = "";
                            reportItem.visitDesc = v.Visit.MainComplaint;
                            reportItem.treatingDoctor = string.Format("{0} {1}", v.Visit.Doctor.FirstName, v.Visit.Doctor.LastName);
                            reportItem.receipt = v.Visit.ReciptId.HasValue ? v.Visit.Recipt.SerialNumber : (int?)null;
                            reportItem.clientId = v.Visit.Patient.Client.Id;
                            reportItem.patientId = v.Visit.Patient.Id;
                            reportItem.visitId = v.Visit.Id;

                            reportItem.clientReferer = v.Visit.Patient.Client.ReferredBy;
                            reportItem.clientBalance = v.Visit.Patient.Client.Balance;
                            if (v.Visit.Patient.Client.BirthDate.HasValue)
                            {
                                AgeHelper age = new AgeHelper(v.Visit.Patient.Client.BirthDate.Value, DateTime.Today);
                                reportItem.clientAge = age.Years + "." + age.Months;
                                reportItem.clientBirthdate = v.Visit.Patient.Client.BirthDate.Value.ToShortDateString();
                            }
                            else
                            {
                                reportItem.clientAge = "";
                                reportItem.clientBirthdate = "";
                            }

                            reportItem.clientTitle = v.Visit.Patient.Client.TitleId.HasValue ? v.Visit.Patient.Client.Title.Name : string.Empty;
                            reportItem.clientZip = letterTemplatesRepository.GetClientZipCode(v.Visit.Patient.Client.Addresses);
                            reportItem.clientCity = letterTemplatesRepository.GetClientCity(v.Visit.Patient.Client.Addresses);
                            reportItem.clientStreetAndNumber = letterTemplatesRepository.GetClientStreetAndNumber(v.Visit.Patient.Client.Addresses);
                            reportItem.clientRecordNumber = v.Visit.Patient.Client.LocalId;

                            reports.Add(reportItem);
                        }
                    }
                    foreach (var v in visit.Examinations)
                    {
                        var exmRelevant = true;

                        if (categoriesIds != null && treatmentsIds != null)
                        {
                            if (!v.PriceListItemId.HasValue)
                            {
                                exmRelevant = false;
                            }
                            else if (!treatmentsIds.Contains(v.PriceListItemId.Value) || !categoriesIds.Contains(v.PriceListItem.CategoryId))
                            {
                                exmRelevant = false;
                            }
                        }
                        else if (categoriesIds != null)
                        {
                            if (!categoriesIds.Contains(v.PriceListItem.CategoryId))
                            {
                                exmRelevant = false;
                            }
                        }
                        else if (treatmentsIds != null)
                        {
                            if (!treatmentsIds.Contains(v.PriceListItemId ?? -1))
                            {
                                exmRelevant = false;
                            }
                        }
                        else if (filter.DiagnosisId.HasValue && filter.DiagnosisId.Value > 0 || filter.MedicationId.HasValue && filter.MedicationId.Value > 0)
                        {
                            exmRelevant = false;
                        }

                        if (exmRelevant)
                        {
                            var reportItem = new VisitReportItem();
                            reportItem.nameVisit = v.Name;
                            reportItem.datePerformed = v.Visit.VisitDate;
                            reportItem.clientName = string.Format("{0} {1}", v.Visit.Patient.Client.FirstName, v.Visit.Patient.Client.LastName);
                            reportItem.clientFirstName = v.Visit.Patient.Client.FirstName;
                            reportItem.clientLastName = v.Visit.Patient.Client.LastName;
                            reportItem.patientName = v.Visit.Patient.Name;
                            reportItem.serial = v.Visit.Patient.Client.IdCard;
                            reportItem.address = letterTemplatesRepository.GetClientAddress(v.Visit.Patient.Client.Addresses);
                            reportItem.homePhone = letterTemplatesRepository.GetClientPhone(v.Visit.Patient.Client.Phones,
                                                                                            PhoneTypeEnum.Home);
                            reportItem.cellPhone = letterTemplatesRepository.GetClientPhone(v.Visit.Patient.Client.Phones,
                                                                                            PhoneTypeEnum.Mobile);
                            reportItem.animalKind = v.Visit.Patient.AnimalRace.AnimalKind.Value;
                            reportItem.animalRace = v.Visit.Patient.AnimalRace.Value;
                            reportItem.animalGender = v.Visit.Patient.GenderId == 1
                                                          ? (Resources.Gender.Male)
                                                          : (v.Visit.Patient.GenderId == 2 ? Resources.Gender.Female : Resources.Gender.UndefinedSex);
                            reportItem.animalColor = v.Visit.Patient.AnimalColorId.HasValue
                                                         ? v.Visit.Patient.AnimalColor.Value
                                                         : string.Empty;
                            if (v.Visit.Patient.BirthDate.HasValue)
                            {
                                // reportItem.animalAge = ((DateTime.Now - v.Visit.Patient.BirthDate.Value).Days / (decimal)365.0);
                                AgeHelper age = new AgeHelper(v.Visit.Patient.BirthDate.Value, DateTime.Today);
                                reportItem.animalAge = age.Years + "." + age.Months;
                            }
                            else
                            {
                                reportItem.animalAge = "";
                            }

                            reportItem.sterilization = v.Visit.Patient.Sterilization ? Resources.Global.Yes : Resources.Global.No;
                            reportItem.electronicNum = v.Visit.Patient.ElectronicNumber;
                            reportItem.birthDate = v.Visit.Patient.BirthDate;
                            reportItem.licenseNum = v.Visit.Patient.LicenseNumber;
                            reportItem.chargeFee = v.Price * v.Quantity - (v.Price * v.Quantity) * (v.DiscountPercent / 100) - v.Discount;
                            reportItem.performingDoc = string.Format("{0} {1}", v.Visit.Doctor.FirstName, v.Visit.Doctor.LastName);
                            reportItem.numOfUnits = v.Quantity;
                            //reportItem.plateNum = "";
                            reportItem.visitDesc = v.Visit.MainComplaint;
                            reportItem.treatingDoctor = string.Format("{0} {1}", v.Visit.Doctor.FirstName, v.Visit.Doctor.LastName);
                            reportItem.receipt = v.Visit.ReciptId.HasValue ? v.Visit.Recipt.SerialNumber : (int?)null;
                            reportItem.clientId = v.Visit.Patient.Client.Id;
                            reportItem.patientId = v.Visit.Patient.Id;
                            reportItem.visitId = v.Visit.Id;
                            reportItem.clientReferer = v.Visit.Patient.Client.ReferredBy;
                            reportItem.clientBalance = v.Visit.Patient.Client.Balance;
                            if (v.Visit.Patient.Client.BirthDate.HasValue)
                            {
                                AgeHelper age = new AgeHelper(v.Visit.Patient.Client.BirthDate.Value, DateTime.Today);
                                reportItem.clientAge = age.Years + "." + age.Months;
                                reportItem.clientBirthdate = v.Visit.Patient.Client.BirthDate.Value.ToShortDateString();
                            }
                            else
                            {
                                reportItem.clientAge = "";
                                reportItem.clientBirthdate = "";
                            }

                            reportItem.clientTitle = v.Visit.Patient.Client.TitleId.HasValue ? v.Visit.Patient.Client.Title.Name : string.Empty;
                            reportItem.clientZip = letterTemplatesRepository.GetClientZipCode(v.Visit.Patient.Client.Addresses);
                            reportItem.clientCity = letterTemplatesRepository.GetClientCity(v.Visit.Patient.Client.Addresses);
                            reportItem.clientStreetAndNumber = letterTemplatesRepository.GetClientStreetAndNumber(v.Visit.Patient.Client.Addresses);
                            reportItem.clientRecordNumber = v.Visit.Patient.Client.LocalId;

                            reports.Add(reportItem);
                        }
                    }
                    foreach (var v in visit.Diagnoses)
                    {
                        var diagRelevant = true;

                        if (categoriesIds != null || treatmentsIds != null)
                        {
                            diagRelevant = filter.DiagnosisId.HasValue && filter.DiagnosisId.Value > 0 && filter.DiagnosisId.Value == v.DiagnosisId;
                        }
                        else if (filter.DiagnosisId.HasValue && filter.DiagnosisId.Value > 0)
                        {
                            if (filter.DiagnosisId.Value != v.DiagnosisId)
                            {
                                diagRelevant = false;
                            }
                        }
                        else if (filter.MedicationId.HasValue && filter.MedicationId.Value > 0)
                        {
                            diagRelevant = false;
                        }

                        if (diagRelevant)
                        {
                            var reportItem = new VisitReportItem();
                            reportItem.nameVisit = v.Name;
                            reportItem.datePerformed = v.Visit.VisitDate;
                            reportItem.clientName = string.Format("{0} {1}", v.Visit.Patient.Client.FirstName, v.Visit.Patient.Client.LastName);
                            reportItem.clientFirstName = v.Visit.Patient.Client.FirstName;
                            reportItem.clientLastName = v.Visit.Patient.Client.LastName;
                            reportItem.patientName = v.Visit.Patient.Name;
                            reportItem.serial = v.Visit.Patient.Client.IdCard;
                            reportItem.address = letterTemplatesRepository.GetClientAddress(v.Visit.Patient.Client.Addresses);
                            reportItem.homePhone = letterTemplatesRepository.GetClientPhone(v.Visit.Patient.Client.Phones,
                                                                                            PhoneTypeEnum.Home);
                            reportItem.cellPhone = letterTemplatesRepository.GetClientPhone(v.Visit.Patient.Client.Phones,
                                                                                            PhoneTypeEnum.Mobile);
                            reportItem.animalKind = v.Visit.Patient.AnimalRace.AnimalKind.Value;
                            reportItem.animalRace = v.Visit.Patient.AnimalRace.Value;
                            reportItem.animalGender = v.Visit.Patient.GenderId == 1
                                                          ? (Resources.Gender.Male)
                                                          : (v.Visit.Patient.GenderId == 2 ? Resources.Gender.Female : Resources.Gender.UndefinedSex);
                            reportItem.animalColor = v.Visit.Patient.AnimalColorId.HasValue
                                                         ? v.Visit.Patient.AnimalColor.Value
                                                         : "";
                            if (v.Visit.Patient.BirthDate.HasValue)
                            {
                                // reportItem.animalAge = ((DateTime.Now - v.Visit.Patient.BirthDate.Value).Days / (decimal)365.0);
                                AgeHelper age = new AgeHelper(v.Visit.Patient.BirthDate.Value, DateTime.Today);
                                reportItem.animalAge = age.Years + "." + age.Months;
                            }
                            else
                            {
                                reportItem.animalAge = "";
                            }

                            reportItem.sterilization = v.Visit.Patient.Sterilization ? Resources.Global.Yes : Resources.Global.No;
                            reportItem.electronicNum = v.Visit.Patient.ElectronicNumber;
                            reportItem.birthDate = v.Visit.Patient.BirthDate;
                            reportItem.licenseNum = v.Visit.Patient.LicenseNumber;
                            reportItem.chargeFee = 0;
                            reportItem.performingDoc = string.Format("{0} {1}", v.Visit.Doctor.FirstName, v.Visit.Doctor.LastName);
                            reportItem.numOfUnits = 1;
                            //reportItem.plateNum = "";
                            reportItem.visitDesc = v.Visit.MainComplaint;
                            reportItem.treatingDoctor = string.Format("{0} {1}", v.Visit.Doctor.FirstName, v.Visit.Doctor.LastName);
                            reportItem.receipt = v.Visit.ReciptId.HasValue ? v.Visit.Recipt.SerialNumber : (int?)null;
                            reportItem.clientId = v.Visit.Patient.Client.Id;
                            reportItem.patientId = v.Visit.Patient.Id;
                            reportItem.visitId = v.Visit.Id;

                            reportItem.clientReferer = v.Visit.Patient.Client.ReferredBy;
                            reportItem.clientBalance = v.Visit.Patient.Client.Balance;
                            if (v.Visit.Patient.Client.BirthDate.HasValue)
                            {
                                AgeHelper age = new AgeHelper(v.Visit.Patient.Client.BirthDate.Value, DateTime.Today);
                                reportItem.clientAge = age.Years + "." + age.Months;
                                reportItem.clientBirthdate = v.Visit.Patient.Client.BirthDate.Value.ToShortDateString();
                            }
                            else
                            {
                                reportItem.clientAge = "";
                                reportItem.clientBirthdate = "";
                            }

                            reportItem.clientTitle = v.Visit.Patient.Client.TitleId.HasValue ? v.Visit.Patient.Client.Title.Name : string.Empty;
                            reportItem.clientZip = letterTemplatesRepository.GetClientZipCode(v.Visit.Patient.Client.Addresses);
                            reportItem.clientCity = letterTemplatesRepository.GetClientCity(v.Visit.Patient.Client.Addresses);
                            reportItem.clientStreetAndNumber = letterTemplatesRepository.GetClientStreetAndNumber(v.Visit.Patient.Client.Addresses);
                            reportItem.clientRecordNumber = v.Visit.Patient.Client.LocalId;

                            reports.Add(reportItem);
                        }
                    }
                    foreach (var v in visit.Medications)
                    {
                        var medRelevant = true;

                        if (categoriesIds != null || treatmentsIds != null)
                        {
                            medRelevant = filter.MedicationId.HasValue && filter.MedicationId.Value > 0 && filter.MedicationId.Value == v.MedicationId;
                        }
                        else if (filter.DiagnosisId.HasValue && filter.DiagnosisId.Value > 0)
                        {
                            medRelevant = false;
                        }
                        else if (filter.MedicationId.HasValue && filter.MedicationId.Value > 0)
                        {
                            if (filter.MedicationId.Value != v.MedicationId)
                            {
                                medRelevant = false;
                            }
                        }

                        if (medRelevant)
                        {
                            var reportItem = new VisitReportItem();
                            reportItem.nameVisit = v.Name;
                            reportItem.datePerformed = v.Visit.VisitDate;
                            reportItem.clientName = string.Format("{0} {1}", v.Visit.Patient.Client.FirstName, v.Visit.Patient.Client.LastName);
                            reportItem.clientFirstName = v.Visit.Patient.Client.FirstName;
                            reportItem.clientLastName = v.Visit.Patient.Client.LastName;
                            reportItem.patientName = v.Visit.Patient.Name;
                            reportItem.serial = v.Visit.Patient.Client.IdCard;
                            reportItem.address = letterTemplatesRepository.GetClientAddress(v.Visit.Patient.Client.Addresses);
                            reportItem.homePhone = letterTemplatesRepository.GetClientPhone(v.Visit.Patient.Client.Phones,
                                                                                            PhoneTypeEnum.Home);
                            reportItem.cellPhone = letterTemplatesRepository.GetClientPhone(v.Visit.Patient.Client.Phones,
                                                                                            PhoneTypeEnum.Mobile);
                            reportItem.animalKind = v.Visit.Patient.AnimalRace.AnimalKind.Value;
                            reportItem.animalRace = v.Visit.Patient.AnimalRace.Value;
                            reportItem.animalGender = v.Visit.Patient.GenderId == 1
                                                          ? (Resources.Gender.Male)
                                                          : (v.Visit.Patient.GenderId == 2 ? Resources.Gender.Female : Resources.Gender.UndefinedSex);
                            reportItem.animalColor = v.Visit.Patient.AnimalColorId.HasValue
                                                         ? v.Visit.Patient.AnimalColor.Value
                                                         : "";
                            if (v.Visit.Patient.BirthDate.HasValue)
                            {
                                //reportItem.animalAge = ((DateTime.Now - v.Visit.Patient.BirthDate.Value).Days / (decimal)365.0);
                                AgeHelper age = new AgeHelper(v.Visit.Patient.BirthDate.Value, DateTime.Today);
                                reportItem.animalAge = age.Years + "." + age.Months;
                            }
                            else
                            {
                                reportItem.animalAge = "";
                            }

                            reportItem.sterilization = v.Visit.Patient.Sterilization ? Resources.Global.Yes : Resources.Global.No;
                            reportItem.electronicNum = v.Visit.Patient.ElectronicNumber;
                            reportItem.birthDate = v.Visit.Patient.BirthDate;
                            reportItem.licenseNum = v.Visit.Patient.LicenseNumber;
                            reportItem.chargeFee = v.Total * v.OverallAmount - (v.Total * v.OverallAmount) * (v.DiscountPercent / 100) - v.Discount;
                            reportItem.performingDoc = string.Format("{0} {1}", v.Visit.Doctor.FirstName, v.Visit.Doctor.LastName);
                            reportItem.numOfUnits = v.OverallAmount;
                            //reportItem.plateNum = "";
                            reportItem.visitDesc = v.Visit.MainComplaint;
                            reportItem.treatingDoctor = string.Format("{0} {1}", v.Visit.Doctor.FirstName, v.Visit.Doctor.LastName);
                            reportItem.receipt = v.Visit.ReciptId.HasValue ? v.Visit.Recipt.SerialNumber : (int?)null;
                            reportItem.clientId = v.Visit.Patient.Client.Id;
                            reportItem.patientId = v.Visit.Patient.Id;
                            reportItem.visitId = v.Visit.Id;
                            reportItem.clientReferer = v.Visit.Patient.Client.ReferredBy;
                            reportItem.clientBalance = v.Visit.Patient.Client.Balance;
                            if (v.Visit.Patient.Client.BirthDate.HasValue)
                            {
                                AgeHelper age = new AgeHelper(v.Visit.Patient.Client.BirthDate.Value, DateTime.Today);
                                reportItem.clientAge = age.Years + "." + age.Months;
                                reportItem.clientBirthdate = v.Visit.Patient.Client.BirthDate.Value.ToShortDateString();
                            }
                            else
                            {
                                reportItem.clientAge = "";
                                reportItem.clientBirthdate = "";
                            }

                            reportItem.clientTitle = v.Visit.Patient.Client.TitleId.HasValue ? v.Visit.Patient.Client.Title.Name : string.Empty;
                            reportItem.clientZip = letterTemplatesRepository.GetClientZipCode(v.Visit.Patient.Client.Addresses);
                            reportItem.clientCity = letterTemplatesRepository.GetClientCity(v.Visit.Patient.Client.Addresses);
                            reportItem.clientStreetAndNumber = letterTemplatesRepository.GetClientStreetAndNumber(v.Visit.Patient.Client.Addresses);
                            reportItem.clientRecordNumber = v.Visit.Patient.Client.LocalId;

                            reports.Add(reportItem);
                        }
                    }
                }
            }
            return reports.OrderBy(r => r.nameVisit).ToList();
        }

        public string GetVisitTreatmentsStringByReciept(int id)
        {
            var visit = DataContext.Visits.Include("Treatments").FirstOrDefault(v => v.ReciptId == id);
            if (visit == null)
            {
                return null;
            }
            var treatmentsStringList = visit.Treatments.Select(t => t.Name).ToList();
            return treatmentsStringList.Any() ? treatmentsStringList.Aggregate((a, b) => a + ',' + b) : string.Empty;
        }

        public IQueryable<VisitTreatment> GetAllVisitTreatmentsOfDocAtDateRange(int activeClinicId, int? doctorId, DateTime fromDate, DateTime toDate)
        {
            if (doctorId.HasValue)
            {
                return
                    DataContext.VisitTreatments.Include(v => v.Visit)
                    .Where(v => v.Visit.DoctorId == doctorId.Value && (fromDate <= v.Visit.VisitDate && v.Visit.VisitDate < toDate));
            }
            else
            {
                return
                    DataContext.VisitTreatments.Include(v => v.Visit)
                                  .Where(v => (fromDate <= v.Visit.VisitDate && v.Visit.VisitDate < toDate));
            }
        }

        public Inventory UpdateInventory(int clinicId, int priceListItemId, decimal quantity, decimal unitPrice, int userId, DateTime visitDate, string userName, int visitId)
        {
            Inventory inventoryItem = null;
            if (IsInventoryManaged(clinicId, priceListItemId))
            {
                inventoryItem = new Inventory()
                    {
                        ClinicId = clinicId,
                        PriceListItemId = priceListItemId,
                        Quantity = quantity * -1,
                        UnitPrice = unitPrice,
                        UserId = userId,
                        UserName = userName,
                        Created = visitDate,
                        VisitId = visitId,
                        InventoryOperationTypeId = (int)InventoryOperationType.Visit
                    };
            }
            return inventoryItem;
        }

        public bool IsInventoryManaged(int clinicId, int priceListItemId)
        {
            var priceListItem =
                DataContext.PriceListItems.Include(p => p.Category)
                           .Single(p => p.Id == priceListItemId && p.Category.ClinicId == clinicId);
            return priceListItem.Category.IsInventory && priceListItem.InventoryActive;

        }

        public int GetClinicIdForPatient(int patientId)
        {
            return DataContext.Patients.Include(p => p.Client).Single(p => p.Id == patientId).Client.ClinicId;
        }

        public int GetClinicIdForClient(int clientId)
        {
            return DataContext.Clients.Single(p => p.Id == clientId).ClinicId;
        }
    }
}
