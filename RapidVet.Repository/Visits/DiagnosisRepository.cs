﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Visits;

namespace RapidVet.Repository.Visits
{
    public class DiagnosisRepository : RepositoryBase<RapidVetDataContext>, IDiagnosisRepository
    {

        public DiagnosisRepository()
        {

        }

        public DiagnosisRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// returns all diagnoseses in clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>  List(Diagnosis)</returns>
        public List<Diagnosis> GetList(int clinicId)
        {
            return DataContext.Diagnoses.Where(d => d.ClinicId == clinicId && d.Active).OrderBy(d => d.Name).ToList();
        }

        /// <summary>
        /// returns a specific diagnosis object by id
        /// </summary>
        /// <param name="id">diagnosis object id</param>
        /// <returns></returns>
        public Diagnosis GetItem(int id)
        {
            return DataContext.Diagnoses.Single(d => d.Id == id);
        }

        /// <summary>
        /// creates a diagnosis object in db
        /// </summary>
        /// <param name="diagnosis">Diagnosis object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Create(Diagnosis diagnosis)
        {
            DataContext.Diagnoses.Add(diagnosis);
            return base.Save(diagnosis);
        }

        /// <summary>
        /// updates a diagnosis object in db
        /// </summary>
        /// <param name="diagnosis">Diagnosis object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Update(Diagnosis diagnosis)
        {
            return base.Save(diagnosis);
        }

        /// <summary>
        /// updates a diagnosis object in db to be not active
        /// </summary>
        /// <param name="diagnosis">Diagnosis object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Delete(Diagnosis diagnosis)
        {
            diagnosis.Active = false;
            return base.Save(diagnosis);
        }
    }
}
