﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Visits;

namespace RapidVet.Repository.Visits
{
    public interface IDiagnosisRepository : IDisposable
    {
        /// <summary>
        /// returns all diagnoseses in clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>  List(Diagnosis)</returns>
        List<Diagnosis> GetList(int clinicId);

        /// <summary>
        /// returns a specific diagnosis object by id
        /// </summary>
        /// <param name="id">diagnosis object id</param>
        /// <returns></returns>
        Diagnosis GetItem(int id);

        /// <summary>
        /// creates a diagnosis object in db
        /// </summary>
        /// <param name="diagnosis">Diagnosis object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Diagnosis diagnosis);

        /// <summary>
        /// updates a diagnosis object in db
        /// </summary>
        /// <param name="diagnosis">Diagnosis object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(Diagnosis diagnosis);

        /// <summary>
        /// updates a diagnosis object in db to be not active
        /// </summary>
        /// <param name="diagnosis">Diagnosis object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(Diagnosis diagnosis);
    }
}
