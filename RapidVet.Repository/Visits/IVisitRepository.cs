﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.CliinicInventory;
using RapidVet.Model.Finance;
using RapidVet.Model.Visits;
using RapidVet.WebModels.Visits;

namespace RapidVet.Repository.Visits
{
    public interface IVisitRepository : IDisposable
    {
        /// <summary>
        /// get visit obj
        /// </summary>
        /// <param name="id"> visit id</param>
        /// <returns>visit obj</returns>
        Visit GetVisit(int id);

        /// <summary>
        /// get price list data for visit
        /// </summary>
        /// <param name="clientId"> client id</param>
        /// <param name="itemTypesId"> item types ids</param>
        /// <param name="clientTariff"> client tariff id</param>
        /// <returns>IEnumerable<PriceListCategory></returns>
        IEnumerable<PriceListCategory> GetVisitPriceListData(int clientId, int[] itemTypesId, int clientTariff);

        /// <summary>
        /// create new visit
        /// </summary>
        /// <param name="visit">Visit obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus AddNewVisit(Visit visit, bool performItems = true);

        /// <summary>
        /// remove visit
        /// </summary>
        /// <param name="id">visit id</param>
        /// <returns>Visit</returns>
        Visit GetAndRemoveVisit(int id);


        /// <summary>
        /// get patient visits
        /// </summary>
        /// <param name="patientId">patient id</param>
        /// <returns>List<Visit></returns>
        List<Visit> GetPatientVisits(int patientId);

        /// <summary>
        /// get patient manual vital sign visits
        /// </summary>
        /// <param name="patientId">patient id</param>
        /// <returns>List<VitalSign></returns>
        List<VitalSign> GetPatientManualVisits(int patientId);

        /// <summary>
        /// get last visit if visit is open, else return blank visit
        /// </summary>
        /// <param name="patientId">patient id</param>
        /// <returns>Visit obj</returns>
        Visit GetLastVisitIfOpen(int patientId);

        /// <summary>
        /// get last visit date for patient, and id in out parameter
        /// </summary>
        /// <param name="patientId">patient id</param>
        /// <returns>DateTime?></returns>
        DateTime? GetLastPatientVisitDate(int patientId, out int? visitId);

        /// <summary>
        /// updates visit obj
        /// </summary>
        /// <param name="visit">Visit obj</param>
        /// <returns>Operation status obj</returns>
        OperationStatus UpdateVisit(Visit visit);

        IQueryable<Visit> GetVisitsOfDay(DateTime date, int clinicId, int issuerId, out int relatedDocotrs);

        Client GetVisitClient(int visitId);

        IQueryable<Visit> GetClintVisits(int clientId, bool includeAllVisiTDetails = false);

        List<PriceListItem> GetAllPriceListItems(int clinicId);
        List<PriceListItem> GetAllTreatments(int clinicId);
        List<PriceListItem> GetAllExaminations(int clinicId);
        List<Medication> GetAllMedications(int clinicGroupId);

        List<VisitReportItem> GetFilteredVisits(VisitFilterWebModel filter, int clinicId);
        string GetVisitTreatmentsStringByReciept(int id);
        IQueryable<VisitTreatment> GetAllVisitTreatmentsOfDocAtDateRange(int activeClinicId, int? doctorId, DateTime fromDate, DateTime toDate);

        /// <summary>
        /// create inventory item
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="priceListItemId">int price list item id</param>
        /// <param name="quantity">int quantity</param>
        /// <param name="unitPrice"> decimal unit price</param>
        /// <param name="userId">int user id</param>
        /// <param name="visitDate"></param>
        /// <param name="userName">string user name</param>
        /// <param name="visitId"></param>
        /// <returns>Inventory obj</returns>
        Inventory UpdateInventory(int clinicId, int priceListItemId, decimal quantity, decimal unitPrice, int userId, DateTime visitDate, string userName, int visitId);

        /// <summary>
        /// check if pricelistitem is managed in inventory
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="priceListItemId">int price list item id</param>
        /// <returns>bool</returns>
        bool IsInventoryManaged(int clinicId, int priceListItemId);

        /// <summary>
        /// get clinic id for patient
        /// </summary>
        /// <param name="patientId">int patient id</param>
        /// <returns>int</returns>
        int GetClinicIdForPatient(int patientId);

        /// <summary>
        /// gets visits client is responsible for payment of.
        /// </summary>
        /// <param name="clientId">int client id</param>
        /// <returns>      IQueryable</returns>
        IQueryable<Visit> GetClientVisitsForPayment(int clientId);

        void SaveManualVitalSign(int id, int vitalSignID, DateTime dateTime, decimal val, int? visitId);

        void SaveVitalSign(int id, int manualID, VitalSignType vitalSignType, decimal val, DateTime vitalDate);

        OperationStatus DeleteVitalSign(int visitID, int manualId, VitalSignType vitalSignType);

        void UpdateVitalSignsForVisit(Visit visit);
    }
}
