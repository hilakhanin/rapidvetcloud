﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Helpers;
using RapidVet.Model;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Users;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data;
//using System.Data.Objects;

namespace RapidVet.Repository
{
    public class UserRepository : RepositoryBase<RapidVetDataContext>, IUserRepository
    {

        public UserRepository()
        {

        }

        public UserRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        /// <summary>
        /// return all users
        /// </summary>
        /// <returns>IQueryable(User)</returns>
        public IQueryable<User> GetList()
        {
            return DataContext.Users;
        }

        /// <summary>
        /// returns a specific user
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns>User</returns>
        public User GetItem(int id, bool isAdministrator = false)
        {
            if (isAdministrator)
            {
                return
                DataContext.Users.Include(u => u.Roles)
                           .Include("ClinicGroup.Clinics")
                      //     .Include("ClinicGroup.Clinics.TaxRates")
                           .Include("UsersClinicsRoleses.Role")
                           .FirstOrDefault(u => u.Id == id);
            }
            else
            {
                return
                    DataContext.Users.Include(u => u.Roles)
                               .Include("ClinicGroup.Clinics")
                    //           .Include("ClinicGroup.Clinics.TaxRates")
                               .Include("UsersClinicsRoleses.Role")
                               .SingleOrDefault(u => u.Id == id);
            }
        }

        /// <summary>
        /// updates a user object in db
        /// </summary>
        /// <param name="user">user object</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus Save(User user)
        {
            return base.Save(user);

        }

        /// <summary>
        /// returns all user in clinic group
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <returns>IQueryable(User)</returns>
        public IQueryable<User> GetListByClinicGroup(int clinicGroupId, string ActiveStatus)
        {
            bool Active = true;
            switch (ActiveStatus)
	        {
                case "OnlyActive":
                        return DataContext.Users.Where(u => u.ClinicGroupId == clinicGroupId && u.Active == true);
                    break;
                    case "OnlyDontActive":
                        return DataContext.Users.Where(u => u.ClinicGroupId == clinicGroupId && u.Active == false);
                    break;
                    case "All":
                        return DataContext.Users.Where(u => u.ClinicGroupId == clinicGroupId);
                    break;
                    default:
                        return DataContext.Users.Where(u => u.ClinicGroupId == clinicGroupId);
                    break;
	        }
            
        }

        /// <summary>
        /// switch and save all user in clinic group
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        /// <param name="isActive"></param>
        public OperationStatus SwitchUsersIdActive(int clinicGroupId, bool isActive)
        {
            try
            {
                using (RapidVetDataContext db = new RapidVetDataContext())
                {
                    var q = db.Users.Where(u => u.ClinicGroupId == clinicGroupId && u.Active == !isActive);
                    foreach (var u in q.ToList())
                    {
                        u.IsApproved = isActive;
                        u.IsLockedOut = !isActive;
                        u.Active = isActive;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationStatus(){
                    Success = false,
                    Message = ex.Message
                };
            }
            return new OperationStatus()
            {
                Success = true
            };
        }

        /// <summary>
        /// returns user by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns>User</returns>
        public User GetItemByUserName(string userName, int id = -1)
        {
            return id == -1
                ?
            DataContext.Users.Include(u => u.Roles).Include("ClinicGroup.Clinics").Include("UsersClinicsRoleses.Role").SingleOrDefault(u => u.Username == userName.ToLower())
                :
            DataContext.Users.Include(u => u.Roles).Include("ClinicGroup.Clinics").Include("UsersClinicsRoleses.Role").SingleOrDefault(u => u.Id == id);
        }

        /// <summary>
        /// returns users by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns>User</returns>
        public List<User> GetItemsByUserName(string userName)
        {
            return DataContext.Users.Where(u => u.Username == userName.ToLower()).ToList();
        }

        public User CreateUser(string userName, string password, string email)
        {
            var user = new User()
                {
                    Username = userName,
                    Email = email,
                    Password = password,
                    IsApproved = true,
                    CreateDate = DateTime.Now
                };

            DataContext.Users.Add(user);
            DataContext.SaveChanges();
            return user;
        }

        /// <summary>
        /// create user object in db
        /// </summary>
        /// <param name="firstName">user first name</param>
        /// <param name="lastName">user last name</param>
        /// <param name="userName">user name</param>
        /// <param name="password">user password</param>
        /// <param name="clinicGroup">clinicgroup id</param>
        /// <param name="titleId">id of title</param>
        /// <returns>User object</returns>
        public User Create(string firstName, string lastName, string userName, string password, int? clinicGroupId, int? titleId, bool active = true)
        {
            var user = GetItemByUserName(userName);
            if (user == null)
            {
                user = new User()
                    {
                        Username = userName.ToLower(),
                        FirstName = firstName,
                        LastName = lastName,
                        Password = password,
                        ClinicGroupId = clinicGroupId,
                        TitleId = titleId,
                        IsApproved = true,
                        IsLockedOut = false,
                        CreateDate = DateTime.Now,
                        LastActivityDate = DateTime.Now,
                        PasswordFailuresSinceLastSuccess = 0,
                        LastPasswordFailureDate = DateTime.Now,
                        LastPasswordChangedDate = DateTime.Now,
                        LastLockoutDate = DateTime.Now,
                        LastLoginDate = DateTime.Now,
                        Active = active
                    };
                DataContext.Users.Add(user);
                DataContext.SaveChanges();
            }
            return user;
        }


        /// <summary>
        /// adds user to clinic
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="roleName">role name (membership)</param>
        /// <param name="clinicId">clinic id</param>
        public void AddToClinic(int userId, string roleName, int clinicId)
        {
            var user = GetItem(userId);
            var role = DataContext.Roles.First(r => r.RoleName == roleName);
            if (user.ClinicGroup.Clinics.Any(c => c.Id == clinicId))
            {
                if (!user.UsersClinicsRoleses.Any(c => c.ClinicId == clinicId && c.RoleId == role.RoleId))
                {
                    var usersClinicsRole = new UsersClinicsRoles()
                        {
                            ClinicId = clinicId,
                            UserId = userId,
                            RoleId = role.RoleId
                        };
                    DataContext.UsersClinicsRoles.Add(usersClinicsRole);
                    Save(usersClinicsRole);
                }
            }
        }

        /// <summary>
        /// removes user from clinic
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="clinicId">clinic id</param>
        public void RemoveFromClinic(int userId, int clinicId)
        {
            var usersClinicsRoles =
                DataContext.UsersClinicsRoles.SingleOrDefault(r => r.ClinicId == clinicId && r.UserId == userId);
            if (usersClinicsRoles != null)
            {
                DataContext.UsersClinicsRoles.Remove(usersClinicsRoles);
                Save(usersClinicsRoles);
            }
        }

        /// <summary>
        /// returns all roles in db
        /// </summary>
        /// <returns> IQueryable(Role)</returns>
        public IQueryable<Role> GetRoles()
        {
            return
                DataContext.Roles;
        }

        /// <summary>
        /// returns user roles in clinic
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="clinicId">clinic id</param>
        /// <returns>IQueryable(UsersClinicsRoles)</returns>
        public IQueryable<UsersClinicsRoles> GetUserClinicRoles(int userId, int clinicId)
        {
            return DataContext.UsersClinicsRoles.Where(r => r.ClinicId == clinicId && r.UserId == userId);
        }

        /// <summary>
        /// return user name from user id
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns>string</returns>
        public string GetUserName(int userId)
        {
            return DataContext.Users.Single(u => u.Id == userId).Username;
        }


        /// <summary>
        /// returns all roles for user , cross clinics
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns>IQueryable(UsersClinicsRoles)</returns>
        public IQueryable<UsersClinicsRoles> GetUserRoles(int userId)
        {
            return
            DataContext.UsersClinicsRoles.Include(r => r.Role).Where(u => u.UserId == userId).OrderBy(u => u.ClinicId);
        }


        /// <summary>
        /// updates user roles in clinic in db
        /// </summary>
        /// <param name="userClinicRoles">list of UsersClinicsRoles objects</param>
        /// <returns>OperationStatus</returns>
        public OperationStatus UpdateUserClinicsRoles(List<UsersClinicsRoles> userClinicRoles, bool isActive = true)
        {
            var status = new OperationStatus();
            var userId = userClinicRoles.First().UserId;
            var currentRoles = DataContext.UsersClinicsRoles.Where(u => u.UserId == userId).OrderBy(c => c.ClinicId);
            var currentRoleList = currentRoles.ToList();
            userClinicRoles = userClinicRoles.OrderBy(r => r.ClinicId).ToList();
            var lastClinic = 0;
            var inClinicGroup = false;
            var userClinicGroup = DataContext.Users.Single(u => u.Id == userId).ClinicGroupId;

            var user = DataContext.Users.Single(u => u.Id == userId);
            if (user != null)
            {
                user.Active = isActive;
                DataContext.SaveChanges();
            }

            foreach (var role in currentRoleList)
            {
                if (role.ClinicId != lastClinic)
                {
                    inClinicGroup = IsClinicInGroup(role.ClinicId, userClinicGroup.Value);
                    lastClinic = role.ClinicId;
                }
                if (inClinicGroup)
                {
                    var remove =
                        userClinicRoles.SingleOrDefault(
                            u => u.ClinicId == role.ClinicId && u.RoleId == role.RoleId && u.UserId == role.UserId) ==
                        null;
                    if (remove)
                    {
                        DataContext.UsersClinicsRoles.Remove(role);
                        status = base.Save(role);
                    }
                }
            }
            lastClinic = 0;
            foreach (var role in userClinicRoles)
            {
                var isRoleExist =
                    currentRoles.SingleOrDefault(r => r.ClinicId == role.ClinicId && r.RoleId == role.RoleId) != null;
                if (!isRoleExist)
                {
                    if (role.ClinicId != lastClinic)
                    {
                        inClinicGroup = IsClinicInGroup(role.ClinicId, userClinicGroup.Value);
                        lastClinic = role.ClinicId;
                    }
                    if (inClinicGroup)
                    {
                        var newClinicRole =
                            currentRoleList.SingleOrDefault(
                                u => u.ClinicId == role.ClinicId && u.RoleId == role.RoleId && u.UserId == role.UserId) ==
                            null;
                        if (newClinicRole)
                        {
                            DataContext.UsersClinicsRoles.Add(role);
                            status = base.Save(role);
                        }
                    }
                }
            }

            return status;
        }

        public IEnumerable<User> GetApprovedUsersByClinicGroup(int clinicGroupId)
        {
            return
                DataContext.Users.Where(u => u.ClinicGroupId == clinicGroupId && u.IsApproved && !u.IsLockedOut)
                           .OrderBy(u => u.LastName)
                           .ThenBy(u => u.FirstName);
        }


        /// <summary>
        /// checks if the user belongs to a clinic in the clinic group
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <param name="userClinicGroup">clinic group id</param>
        /// <returns></returns>
        private bool IsClinicInGroup(int clinicId, int userClinicGroup)
        {
            return DataContext.Clinics.Single(c => c.Id == clinicId).ClinicGroupID == userClinicGroup;
        }



        public List<DailySchedule> GetDailySchedules(int userId, int clinicId)
        {
            return DataContext.DailySchedules.Where(ds => ds.UserId == userId && ds.ClinicId == clinicId).ToList();
        }

        public DailySchedule GetDailySchedule(int id)
        {
            return DataContext.DailySchedules.SingleOrDefault(ds => ds.Id == id);
        }

        public DailySchedule GetDailyScheduleOfUserByDay(int userId, DayOfWeek day)
        {
            var dayId = (int)day;
            return DataContext.DailySchedules.SingleOrDefault(ds => ds.UserId == userId && ds.DayId == dayId);
        }

        public OperationStatus CreateDailySchedule(DailySchedule dailySchedule)
        {
            DataContext.DailySchedules.Add(dailySchedule);
            return base.Save(dailySchedule);
        }

        public OperationStatus UpdateDailySchedule(DailySchedule dailySchedule)
        {
            return base.Save(dailySchedule);
        }

        public OperationStatus RemoveDailySchedule(int dailyScheduleId)
        {
            var dailySchedule = DataContext.DailySchedules.SingleOrDefault(ds => ds.Id == dailyScheduleId);
            if (dailySchedule != null)
            {
                DataContext.DailySchedules.Remove(dailySchedule);
                return base.Save(dailySchedule);
            }
            return new OperationStatus() { Success = false };
        }

        public List<UserPreventiveMedicineReminderFilter> GetPreventiveItemReminderFilters(int userId)
        {
            return
                DataContext.UserPreventiveMedicineReminderFilters.Where(u => u.UserId == userId)
                           .OrderByDescending(u => u.Name).ToList();
        }

        public UserPreventiveMedicineReminderFilter GetPreventiveReminderFilter(int filterId)
        {
            return
                DataContext.UserPreventiveMedicineReminderFilters.Include(f => f.PreventiveMedicineItems)
                           .Include("PreventiveMedicineItems.PriceListItem")
                           .Include(p=>p.PreventiveMedicineFilterCity)
                           .Single(f => f.Id == filterId);
        }

        public void AddPreventiveReminderFilter(UserPreventiveMedicineReminderFilter filter)
        {
            DataContext.UserPreventiveMedicineReminderFilters.Add(filter);
        }

        public bool UpdatePreventiveReminderFilterWithItems(int filterId, List<int> priceListItemsIds, List<int> cityIds)
        {
            var existing = DataContext.PreventiveMedicineFilterPriceListItems.Where(p => p.FilterId == filterId);
            foreach (var item in existing.ToList())
            {
                DataContext.PreventiveMedicineFilterPriceListItems.Remove(item);
            }

            foreach (var id in priceListItemsIds)
            {
                DataContext.PreventiveMedicineFilterPriceListItems.Add(new PreventiveMedicineFilterPriceListItem()
                    {
                        FilterId = filterId,
                        PriceListItemId = id
                    });
            }

            foreach (var id in cityIds)
            {
                DataContext.PreventiveMedicineFilterCity.Add(new PreventiveMedicineFilterCity()
                {
                    FilterId = filterId,
                    CityId = id
                });
            }

            return DataContext.SaveChanges() > -1;
        }

        public void RemoveUserRole(UsersClinicsRoles existingRole)
        {
            DataContext.UsersClinicsRoles.Remove(existingRole);
        }

        public void AddClinicRole(UsersClinicsRoles role)
        {
            DataContext.UsersClinicsRoles.Add(role);
        }

        public string GetDoctorLisence(string doctorName, int clinicGroupId)
        {
            var doctors = DataContext.Users.Where(u => u.ClinicGroupId == clinicGroupId && u.IsApproved);
            var result = string.Empty;

            foreach (var doctor in doctors)
            {
                if (doctorName == doctor.Name)
                {
                    result = doctor.LicenceNumber;
                    break;
                }
            }

            return result;
        }

        public int GetNumberOfActiveClinics()
        {
            DateTime from = DateTime.Now.AddHours(-24);
            return DataContext.Users.Where(c => c.Active && c.LastLoginDate >= from).Select(c => c.ClinicGroupId).Distinct().Count();
        }

        public int GetNumberOfActiveUsers()
        {
            DateTime from = DateTime.Now.AddHours(-24);
            return DataContext.Users.Where(c => c.Active && c.LastLoginDate >= from).Select(c => c.Id).Distinct().Count();
        }

        public int GetNumberOfUsers()
        {
            return DataContext.Users.Count(c => c.Active);
        }

        //public void RefreshRepository()
        //{
        //    var context = ((IObjectContextAdapter)DataContext).ObjectContext;

        //    // detach all added entities
        //    DataContext.ChangeTracker.Entries().Where(e => e.State == EntityState.Added).ToList().ForEach(e => e.State = EntityState.Detached);

        //    // select entities
        //    var refreshableObjects = DataContext.ChangeTracker.Entries().Select(e => e.Entity).ToList();

        //    // refresh each refreshable object
        //    foreach (var @object in refreshableObjects)
        //    {
        //        // refresh each collection of the object
        //        context.ObjectStateManager.GetRelationshipManager(@object).GetAllRelatedEnds().Where(r => r.IsLoaded).ToList().ForEach(c => c.Load());

        //        // refresh the object
        //        context.Refresh(RefreshMode.StoreWins, @object);
        //    }
        //}
    }
}
