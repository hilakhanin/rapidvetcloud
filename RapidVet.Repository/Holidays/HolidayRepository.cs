﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.FullCalender;
using RapidVet.Repository.Holidays;

namespace RapidVet.Repository.Holidays
{
    public class HolidayRepository : RepositoryBase<RapidVetDataContext>, IHolidayRepository
    {
        public HolidayRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public HolidayRepository()
        {

        }

        public List<Holiday> GetHolidaysForClinic(int clinicId)
        {
            return DataContext.Holidays.Where(h => h.ClinicId == clinicId).ToList();
        }


        public OperationStatus Create(Holiday holiday)
        {
            DataContext.Holidays.Add(holiday);
            return base.Save(holiday);
        }

        public Holiday GetHoliday(int holidayId)
        {
            return DataContext.Holidays.SingleOrDefault(h => h.Id == holidayId);
        }

        public OperationStatus Update(Holiday holiday)
        {
            return base.Save(holiday);
        }

        public OperationStatus Remove(int holidayId)
        {
            var holiday = DataContext.Holidays.SingleOrDefault(h => h.Id == holidayId);
            if (holiday != null)
            {
                DataContext.Holidays.Remove(holiday);
                return base.Save(holiday);
            }
            return new OperationStatus() { Success = false };
        }
    }
}
