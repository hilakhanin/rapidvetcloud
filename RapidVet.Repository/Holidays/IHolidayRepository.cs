﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.FullCalender;


namespace RapidVet.Repository.Holidays
{
    public interface IHolidayRepository : IDisposable
    {
        /// <summary>
        /// get rooms of clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>List(Room)</returns>
        List<Holiday> GetHolidaysForClinic(int clinicId);
        
        /// <summary>
        /// create holiday in clinic
        /// </summary>
        /// <param name="holiday">holiday object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Holiday holiday);

        /// <summary>
        /// return a holiday of a specific id
        /// </summary>
        /// <param name="holidayId">id of room object</param>
        /// <returns>holiday</returns>
        Holiday GetHoliday(int holidayId);

        /// <summary>
        /// updates holiday in clinic
        /// </summary>
        /// <param name="holiday">holiday object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(Holiday holiday);

        /// <summary>
        /// makes holiday inactive
        /// </summary>
        /// <param name="holidayId">id of holiday</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Remove(int holidayId);
    }
}