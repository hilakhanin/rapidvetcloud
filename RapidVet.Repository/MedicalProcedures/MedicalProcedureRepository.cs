﻿using System;
using System.Linq;
using System.Data.Entity;
using RapidVet.Model;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.MedicalProcedures
{
    public class MedicalProcedureRepository : RepositoryBase<RapidVetDataContext>, IMedicalProcedureRepository
    {
        public MedicalProcedureRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public MedicalProcedureRepository()
        {

        }

        public IQueryable<MedicalProcedure> GetMedicalProcedures(int clinicId, DateTime fromDt, DateTime toDt)
        {
            var clinicPatientIds = DataContext.Patients.Where(p => p.Client.ClinicId == clinicId).Select(p => p.Id);
            return
                DataContext.MedicalProcedures.Include(mp => mp.Patient)
                    .Include("Patient.Client.Phones.PhoneType")
                    .Include("Patient.Client.Addresses.City")
                    .Include("Patient.Client.Clinic")
                    .Include("Patient.AnimalRace.AnimalKind")
                    .Include("Patient.AnimalColor")
                    .Include(mp => mp.Diagnosis)
                    .Where(
                        mp => clinicPatientIds.Contains(mp.PatientId) && mp.Start >= fromDt && mp.Start <= toDt);
        }

        public IQueryable<MedicalProcedure> GetMedicalProceduresForPatient(int id)
        {
            return DataContext.MedicalProcedures.Where(mp => mp.PatientId == id);
        }

        public OperationStatus Create(MedicalProcedure medicalProcedure)
        {
            DataContext.MedicalProcedures.Add(medicalProcedure);
            return base.Save(medicalProcedure);
        }

        public MedicalProcedure GetMedicalProcedure(int medicalProcedureId)
        {
            return
                DataContext.MedicalProcedures.Include(mp => mp.Diagnosis)
                    .Include("Patient.Client.Phones.PhoneType")
                    .Include("Patient.Client.Addresses.City")
                    .Include("Patient.Client.Clinic")
                    .Include("Patient.AnimalRace.AnimalKind")
                    .Include("Patient.AnimalColor")
                    .Include(mp => mp.Diagnosis)
                    .Single(mp => mp.Id == medicalProcedureId);
        }

        public OperationStatus Delete(MedicalProcedure medicalProcedure)
        {
            DataContext.MedicalProcedures.Remove(medicalProcedure);
            return base.Save(medicalProcedure);
        }
    }
}
