﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.MedicalProcedures
{
    public interface IMedicalProcedureRepository : IDisposable
    {

        /// <summary>
        /// returns medical procedures in clinic for a date range
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="fromDt"> date time from</param>
        /// <param name="toDt">date time to</param>
        /// <returns>IQueryable<MedicalProcedure></returns>
        IQueryable<MedicalProcedure> GetMedicalProcedures(int clinicId, DateTime fromDt, DateTime toDt);

        /// <summary>
        /// get all medical procedures for patient
        /// </summary>
        /// <param name="id"> patient id</param>
        /// <returns></returns>
        IQueryable<MedicalProcedure> GetMedicalProceduresForPatient(int id);

        /// <summary>
        /// creates new medical procedure in db
        /// </summary>
        /// <param name="medicalProcedure">MedicalProcedure</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(MedicalProcedure medicalProcedure);

        /// <summary>
        /// get medical procedure element
        /// </summary>
        /// <param name="medicalProcedureId">medical procedure id</param>
        /// <returns>MedicalProcedure</returns>
        MedicalProcedure GetMedicalProcedure(int medicalProcedureId);

        /// <summary>
        /// deletes medical procedure entity from DB
        /// </summary>
        /// <param name="medicalProcedure">MedicalProcedure</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(MedicalProcedure medicalProcedure);
    }
}
