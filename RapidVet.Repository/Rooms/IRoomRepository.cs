﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.FullCalender;

namespace RapidVet.Repository.Rooms
{
    public interface IRoomRepository : IDisposable
    {
        /// <summary>
        /// get rooms of clinic
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        /// <returns>List(Room)</returns>
        List<Room> GetRoomsForClinic(int clinicId);

       
        /// <summary>
        /// create room in clinic
        /// </summary>
        /// <param name="room">room object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Room room);

        /// <summary>
        /// return a room of a specific id
        /// </summary>
        /// <param name="roomId">id of room object</param>
        /// <returns>Room</returns>
        Room GetRoom(int roomId);

        /// <summary>
        /// updates room in clinic
        /// </summary>
        /// <param name="room">room object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Update(Room room);

        /// <summary>
        /// makes room inactive
        /// </summary>
        /// <param name="roomId">id of room</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Remove(int roomId);
    }
}