﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.FullCalender;

namespace RapidVet.Repository.Rooms
{
    public class RoomRepository : RepositoryBase<RapidVetDataContext>, IRoomRepository
    {
        public RoomRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public RoomRepository()
        {

        }

        public List<Room> GetRoomsForClinic(int clinicId)
        {
            return DataContext.Rooms.Where(r => (r.ClinicId == clinicId) && r.Active).ToList();
        }


        public OperationStatus Create(Room room)
        {
            DataContext.Rooms.Add(room);
            return base.Save(room);
        }

        public Room GetRoom(int roomId)
        {
            return DataContext.Rooms.SingleOrDefault(r => r.Id == roomId);
        }

        public OperationStatus Update(Room room)
        {
            return base.Save(room);
        }

        public OperationStatus Remove(int roomId)
        {
            var room = DataContext.Rooms.SingleOrDefault(r => r.Id == roomId);
            if (room != null)
            {
                room.Active = false;
                return base.Save(room);
            }
            return new OperationStatus() { Success = false };
        }
    }
}
