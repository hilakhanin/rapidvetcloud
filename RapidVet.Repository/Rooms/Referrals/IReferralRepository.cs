﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.Referrals
{
    public interface IReferralRepository : IDisposable
    {
        /// <summary>
        /// get all refferals fot clinic for date range
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="from"> datetime from</param>
        /// <param name="to"> datetime to</param>
        /// <param name="months"></param>
        /// <param name="searchMethod">enum representing search methos - date range or month number</param>
        /// <returns>IQueryable<Referral></returns>
        IQueryable<Referral> GetRefferals(int clinicId, DateTime @from, DateTime to, int? months, ReferralsSearchMethod searchMethod);

        /// <summary>
        /// gets referral entry
        /// </summary>
        /// <param name="refferalId">int referral id</param>
        /// <returns>Referral</returns>
        Referral GetReferral(int refferalId);

        /// <summary>
        /// create new referral obj
        /// </summary>
        /// <param name="refferal">Referral obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Referral refferal);


        /// <summary>
        /// deletes referral
        /// </summary>
        /// <param name="refferal">Referral obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(Referral refferal);

    }
}
