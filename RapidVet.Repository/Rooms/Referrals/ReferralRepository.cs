﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.Patients;

namespace RapidVet.Repository.Referrals
{
    public class ReferralRepository : RepositoryBase<RapidVetDataContext>, IReferralRepository
    {
        public ReferralRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public ReferralRepository()
        {

        }

        public IQueryable<Referral> GetRefferals(int clinicId, DateTime @from, DateTime to, int? months, ReferralsSearchMethod searchMethod)
        {
            var clinicPatientIds =
                DataContext.Patients.Include(p => p.Client).Where(p => p.Client.ClinicId == clinicId).Select(p => p.Id);

            IQueryable<Referral> data;


            if (searchMethod == ReferralsSearchMethod.DateRange )
            {
                data =
                    DataContext.Referrals.Include(r => r.Patient)
                               .Include("Patient.Client")
                               .Include(r => r.RefferingUser)
                               .Include(r => r.RefferedToUser)
                               .Where(
                                   r => r.DateTime >= from && r.DateTime < to && clinicPatientIds.Contains(r.PatientId))
                               .OrderByDescending(r => r.DateTime);
            }
            else
            {
                if (!months.HasValue)
                {
                    months = 0;
                }
                var past = DateTime.Now.AddMonths(-1*months.Value);
                data =
            DataContext.Referrals.Include(r => r.Patient)
                       .Include("Patient.Client")
                       .Include(r => r.RefferingUser)
                       .Include(r => r.RefferedToUser)
                       .Where(
                           r => r.DateTime >= past && r.DateTime < DateTime.Now && clinicPatientIds.Contains(r.PatientId))
                       .OrderByDescending(r => r.DateTime);
            }

            return data;
        }

        public Referral GetReferral(int refferalId)
        {
            return
                DataContext.Referrals.Include(r => r.Patient)
                           .Include("Patient.Client")
                           .Include(r => r.RefferedToUser)
                           .Include(r => r.RefferingUser)
                           .Single(r => r.Id == refferalId);
        }

        public OperationStatus Create(Referral refferal)
        {
            DataContext.Referrals.Add(refferal);
            return base.Save(refferal);
        }

        public OperationStatus Delete(Referral refferal)
        {
            DataContext.Referrals.Remove(refferal);
            return base.Save(refferal);
        }
    }
}
