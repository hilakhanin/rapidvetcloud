﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;

namespace RapidVet.Repository.ClinicTasks
{
    public interface IClinicTaskRepository : IDisposable
    {
        void AddTask(ClinicTask clinicTask);

        ClinicTask GetTask(int taskId);
        List<ClinicTask> GetClinicTasks(int clinicId);
        bool RemoveTask(int taskId);
        List<ClinicTask> GetTasks(List<int> ids);
        List<ClinicTask> GetClinicOpenTasks(int clinicId);
    }
}
