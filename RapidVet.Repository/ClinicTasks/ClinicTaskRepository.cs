﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using RapidVet.Model;

namespace RapidVet.Repository.ClinicTasks
{
    public class ClinicTaskRepository : RepositoryBase<RapidVetDataContext>, IClinicTaskRepository
    {
        public ClinicTaskRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public ClinicTaskRepository()
        {

        }

        public void AddTask(ClinicTask clinicTask)
        {
            DataContext.ClinicTasks.Add(clinicTask);
        }

        public ClinicTask GetTask(int taskId)
        {
            return DataContext.ClinicTasks.SingleOrDefault(ct => ct.Id == taskId);
        }

        public List<ClinicTask> GetClinicTasks(int clinicId)
        {
            return DataContext.ClinicTasks.Where(ct => ct.ClinicId == clinicId).ToList();
        }

        public List<ClinicTask> GetClinicOpenTasks(int clinicId)
        {
            return DataContext.ClinicTasks.Where(ct => ct.ClinicId == clinicId && ct.StatusId == 0).ToList();
        }

        public bool RemoveTask(int taskId)
        {
            var task = DataContext.ClinicTasks.SingleOrDefault(ct => ct.Id == taskId);
            if (task != null)
            {
                DataContext.ClinicTasks.Remove(task);
                return true;
            }
            return false;
        }

        public List<ClinicTask> GetTasks(List<int> ids)
        {
            var res = DataContext.ClinicTasks.Include(ct=>ct.DesignatedUser).Where(ct => ids.Contains(ct.Id));
            return res.ToList();
        }
    }
}
