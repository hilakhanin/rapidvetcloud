﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model.Patients;
using RapidVet.WebModels.GeneralReports;

namespace RapidVet.Repository.GeneralReports
{
    public interface IGeneralReportsRepository : IDisposable
    {
        List<Patient> GetBirthdays(int clinicId, int month, int? day);
        //rabis id is ID of rabies vaccine price list item 
        List<RabiesReportItem> GetRabiesVaccinated(DateTime fromDate, DateTime toDate, int? animalKindId, bool? sterilization, int? regionalCouncilId, int clinicId, List<int> rabiesIds, int? cityId);
    }
}
