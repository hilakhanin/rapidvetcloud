﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Model.Patients;
using RapidVet.WebModels.GeneralReports;
using System.Data.Entity.SqlServer;

namespace RapidVet.Repository.GeneralReports
{
    public class GeneralReportsRepository : RepositoryBase<RapidVetDataContext>, IGeneralReportsRepository
    {

        public GeneralReportsRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public GeneralReportsRepository()
        {

        }

        public List<Patient> GetBirthdays(int clinicId, int month, int? day)
        {
            return DataContext.Patients
                              .Include(p => p.Client)
                              .Include("Client.Vet")
                              .Include("Client.ClientStatus")
                              .Include("Client.Phones")
                              .Include("Client.Addresses")
                              .Include("Client.Addresses.City")
                              .Include("Client.Emails")
                              .Include("Client.Title")
                              .Include(p => p.AnimalRace.AnimalKind)
                              .Include(p => p.AnimalColor)
                              .Where(p => p.Active && p.BirthDate.HasValue && p.Client.ClinicId == clinicId && p.BirthDate > DateTime.MinValue && p.BirthDate.Value.Month == month && (!day.HasValue || p.BirthDate.Value.Day == day.Value))
                              .OrderByDescending(p => p.BirthDate.Value)
                              .ToList();
        }

        public List<RabiesReportItem> GetRabiesVaccinated(DateTime fromDate, DateTime toDate, int? animalKindId, bool? sterilization, int? regionalCouncilId, int clinicId, List<int> rabiesIds, int? cityId)
        {
            var vaccinatePatients = from pmi in DataContext.PreventiveMedicineItems.Where(m => rabiesIds.Contains(m.PriceListItemId) && m.Preformed.HasValue).DefaultIfEmpty()
                                    join p in DataContext.Patients on pmi.PatientId equals p.Id
                                    //join cg in DataContext.ClinicGroups on clinicGroupId equals cg.Id
                                    from c in DataContext.Clients.Where(m => m.Id == p.ClientId).Take(1).DefaultIfEmpty()
                                    from clnc in DataContext.Clinics.Where(m => m.Id == c.ClinicId).Take(1).DefaultIfEmpty()
                                    from adrs in DataContext.Addresses.Where(m => m.ClientId == c.Id).OrderBy(a => a.Id).Take(1).DefaultIfEmpty()
                                    from cell in DataContext.Phones.Where(m => m.ClientId == c.Id && m.PhoneTypeId == 1).Take(1).DefaultIfEmpty() //cell phone
                                    from home in DataContext.Phones.Where(m => m.ClientId == c.Id && m.PhoneTypeId == 2).Take(1).DefaultIfEmpty() //home phone
                                    from mail in DataContext.Emails.Where(m => m.ClientId == p.ClientId).Take(1).DefaultIfEmpty()
                                    from ct in DataContext.Cities.Where(m => m.Id == adrs.CityId).Take(1).DefaultIfEmpty()
                                    from rc in DataContext.RegionalCouncils.Where(m => m.Id == ct.RegionalCouncilId).DefaultIfEmpty()
                                    from ar in DataContext.AnimalRaces.Where(m => m.Id == p.AnimalRaceId).Take(1).DefaultIfEmpty()
                                    from ak in DataContext.AnimalKinds.Where(m => m.Id == ar.AnimalKindId).Take(1).DefaultIfEmpty()
                                    from color in DataContext.AnimalColors.Where(m => m.Id == p.AnimalColorId).Take(1).DefaultIfEmpty()
                                    from usr in DataContext.Users.Where(m => pmi.PreformingUserId.HasValue && pmi.PreformingUserId == m.Id).Take(1).DefaultIfEmpty()
                                    where
                                       (c.ClinicId == clinicId) &&
                                        fromDate <= pmi.Preformed && pmi.Preformed <= toDate &&
                                       (!regionalCouncilId.HasValue || rc.Id == regionalCouncilId.Value) &&
                                       (!cityId.HasValue || ct.Id == cityId.Value) &&
                                       (!sterilization.HasValue || p.Sterilization == sterilization.Value) &&
                                       (!animalKindId.HasValue || p.AnimalRace.AnimalKindId == animalKindId.Value)
                                    select new RabiesReportItem()
                                  {
                                      VaccinationDate = pmi.Preformed.HasValue ? SqlFunctions.DateName("day", pmi.Preformed.Value) + "/" + SqlFunctions.StringConvert((double)pmi.Preformed.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", pmi.Preformed.Value) : "",
                                      MonthOfVaccination = pmi.Preformed.HasValue ? pmi.Preformed.Value.Month : 0,
                                      OwnerFirstName = c.FirstName,
                                      OwnerLastName = c.LastName,
                                      OwnerBirthDate = c.BirthDate.HasValue ? SqlFunctions.DateName("day", c.BirthDate.Value) + "/" + SqlFunctions.StringConvert((double)c.BirthDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", c.BirthDate.Value) : "",
                                      OwnerIdCard = c.IdCard,
                                      DisplayPhone = c.ApproveDisplayPhone ? Resources.Global.Yes : Resources.Global.No,
                                      Address = adrs != null ? adrs.Street + " " + ( ct != null ? ct.Name : "") : "",
                                      City = adrs != null ? ct != null ? ct.Name : "" : "",
                                      CityId = adrs != null ? adrs.CityId : 0,
                                      AreaCode = adrs != null ? adrs.ZipCode : "",
                                      RegionalCouncilId = rc != null ? rc.Id : 0,
                                      RegionalCouncil = rc != null && rc.Id != 1 ? rc.Name : ct != null ? ct.Name : "",
                                      CellPhone = cell != null ? cell.PhoneNumber : "",
                                      HomePhone = home != null ? home.PhoneNumber : "",
                                      Email = mail != null ? mail.Name : "",
                                      AnimalName = p.Name,
                                      AnimalRace = ar.Value,
                                      AnimalKind = ak.Value,
                                      AnimalKindId = ak.Id,
                                      Gender = p.GenderId == 1 ? (Resources.Gender.Male) : (p.GenderId == 2 ? Resources.Gender.Female : Resources.Gender.UndefinedSex),
                                      AnimalGenderTypeId = p.GenderId,
                                      Birthdate = p.BirthDate.HasValue ? SqlFunctions.DateName("day", p.BirthDate.Value) + "/" + SqlFunctions.StringConvert((double)p.BirthDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", p.BirthDate.Value) : "-",
                                      Color = color != null ? color.Value : "",
                                      LicenceNumber = p.LicenseNumber,
                                      ElectronicNumber = p.ElectronicNumber,
                                      SterilizationBool = p.Sterilization,
                                      IsDangerous = p.AnimalDangerous ? Resources.Global.Yes : Resources.Global.No,
                                      Exemption = p.Exemption,
                                      ExemptionCause = p.ExemptionCause,
                                      VaccinatingVetName = usr != null ? usr.FirstName + " " + usr.LastName : null,
                                      SAGIRNumber = p.SAGIRNumber,
                                      BirthYear = p.BirthDate.HasValue ? SqlFunctions.DateName("year", p.BirthDate.Value) : "",
                                      BirthMonth = p.BirthDate.HasValue ? p.BirthDate.Value.Month : 0,
                                      Scheduled = pmi.Scheduled.HasValue ? SqlFunctions.DateName("day", pmi.Scheduled) + "/" + SqlFunctions.StringConvert((double)pmi.Scheduled.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", pmi.Scheduled) : "",
                                      BatchNumber = pmi.BatchNumber,
                                      ClientPaidOrReturnedTollVaucher = pmi.ClientPaidOrReturnedTollVaucher ? "+" : "-",
                                      VetLicenseNum = usr != null ? usr.LicenceNumber : "",
                                      ClientId = p.ClientId
                                  };

            return vaccinatePatients.ToList();
            //return DataContext.Patients.Include(p => p.PreventiveMedicine)
            //                  .Include("PreventiveMedicine.PreformingUser")
            //                                               .Include(p => p.AnimalColor)
            //                                               .Include(p => p.Client)
            //                                               .Include("Client.Clinic")
            //                                               .Include("Client.Phones")
            //                                               .Include("Client.Addresses.City")
            //                                               .Include("Client.Addresses.City.RegionalCouncil")
            //                                               .Include("Client.Emails")
            //                                               .Include(p => p.AnimalRace)
            //                                               .Include("AnimalRace.AnimalKind")
            //                  .Where(
            //                      p =>
            //                      p.Client.ClinicId == clinicId &&
            //                      (!regionalCouncilId.HasValue ||
            //                       p.Client.Addresses.Any(a => a.City.RegionalCouncilId == regionalCouncilId.Value)) &&
            //                      (!sterilization.HasValue || p.Sterilization == sterilization.Value) &&
            //                      (!animalKindId.HasValue || p.AnimalRace.AnimalKindId == animalKindId.Value) &&
            //                      p.PreventiveMedicine.Any(
            //                          m =>
            //                          rabiesIds.Contains(m.PriceListItemId) && fromDate <= m.Preformed &&
            //                          m.Preformed <= toDate))
            //                  .ToList();
        }
    }
}
