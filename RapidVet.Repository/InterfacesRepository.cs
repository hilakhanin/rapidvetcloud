﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model.Tools;


namespace RapidVet.Repository
{
    public class InterfacesRepository : RepositoryBase<RapidVetDataContext>, IInterfacesRepository
    {
        public InterfacesRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public InterfacesRepository()
        {

        }

        public Interface GetInterfaceByName(string name)
        {
            return DataContext.Interfaces.SingleOrDefault(i => i.Name.ToLower().Equals(name.ToLower()));
        }
       
    }
}
