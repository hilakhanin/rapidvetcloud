﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Model;
using RapidVet.Model.PatientLabTests;

namespace RapidVet.Repository.PatientLabTests
{
    public class PatientLabTestRepository : RepositoryBase<RapidVetDataContext>, IPatientLabTestRepository
    {
        public PatientLabTestRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public PatientLabTestRepository()
        {

        }

        public List<PatientLabTest> GetTestPreformances(int patientId, int labTestTemplateId, int clinicId)
        {
            var animalKindId =
                DataContext.Patients.Include(p => p.AnimalRace).Single(p => p.Id == patientId).AnimalRace.AnimalKindId;

            //var labTestTemplateIds =
            //    DataContext.LabTestsTemplates.Where(
            //        ltt => ltt.AnimalKindId == animalKindId && ltt.LabTestTypeId == labTestTypeId).Select(ltt => ltt.Id);

            var preformances =
                DataContext.PatientLabTests.Where(
                    plt => plt.PatientId == patientId && plt.LabTestTemplateId == labTestTemplateId);

            return preformances.OrderByDescending(p => p.Created).ToList();
        }

        public PatientLabTest GetPatientLabTest(int patientLabTestId)
        {
            return
                DataContext.PatientLabTests.Include(plt => plt.LabTestTemplate)
                           .Include(plt => plt.Results)
                           .Include("Results.LabTest")
                           .Single(plt => plt.Id == patientLabTestId);
        }

        public List<PatientLabTest> GetPatientGroupLabTest(int patientId, int labTestTemplateId)
        {
            return
                DataContext.PatientLabTests.Include(plt => plt.LabTestTemplate)
                           .Include(plt => plt.Results)
                           .Include("Results.LabTest")
                           .Where(plt => plt.LabTestTemplateId == labTestTemplateId && plt.PatientId == patientId).OrderBy(d=>d.Created).Take(4).ToList();
        }

        public OperationStatus Create(PatientLabTest patientLabTest)
        {
            DataContext.PatientLabTests.Add(patientLabTest);
            return base.Save(patientLabTest);
        }

        public OperationStatus UpdateDate(PatientLabTest patientLabTest)
        {
            DataContext.PatientLabTests.Attach(patientLabTest);
            DataContext.Entry(patientLabTest).State = System.Data.Entity.EntityState.Modified;

            return base.Save(patientLabTest);
        }

        public OperationStatus SaveResults(List<PatientLabTestResult> patientLabTestResults, int patientLabTestId)
        {
            var status = new OperationStatus() {Success = false};

            foreach (var plr in patientLabTestResults)
            {
                plr.PatientLabTestId = patientLabTestId;
                DataContext.PatientLabTestResults.Add(plr);
            }

            status.Success = DataContext.SaveChanges() == patientLabTestResults.Count();

            return status;
        }

        public List<PatientLabTestResult> GetAllSimilarLabTestResultsForPatient(int patientId, int patientLabTestId)
        {
            var patientTests = DataContext.PatientLabTests
                .Include(t => t.Results)
                .Include("LabTestTemplate.LabTestResults")
                .Where(t => t.PatientId == patientId && t.LabTestTemplate.LabTestResults.FirstOrDefault(r => r.Id == patientLabTestId) != null);
            var results = new List<PatientLabTestResult>();
            foreach (var patientLabTest in patientTests)
            {
                results.AddRange(patientLabTest.Results.Where(r => r.LabTestId == patientLabTestId));
            }
            return results;
        }
    }
}
