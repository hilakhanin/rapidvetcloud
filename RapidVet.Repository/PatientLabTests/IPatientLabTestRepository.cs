﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.PatientLabTests;

namespace RapidVet.Repository.PatientLabTests
{
    public interface IPatientLabTestRepository : IDisposable
    {
        /// <summary>
        /// get test preformances for patient id
        /// </summary>
        /// <param name="patientId"> patient id</param>
        /// <param name="labTestTemplateId"> lab test template id</param>
        /// <param name="clinicId">clinic id</param>
        /// <returns>List<PatientLabTest></returns>
        List<PatientLabTest> GetTestPreformances(int patientId, int labTestTemplateId, int clinicId);

        /// <summary>
        /// get patient lab test
        /// </summary>
        /// <param name="patientLabTestId">patient lab test id</param>
        /// <returns></returns>
        PatientLabTest GetPatientLabTest(int patientLabTestId);
        
         /// <summary>
        /// get last 3 labs of same type
        /// </summary>
        /// <param name="patientId">patient id</param>
        /// <param name="labTestTemplateId">lab test template type id</param>
        /// <returns></returns>
        List<PatientLabTest> GetPatientGroupLabTest(int patientId, int labTestTemplateId);

        /// <summary>
        /// create patient lab test
        /// </summary>
        /// <param name="patientLabTest">PatientLabTest object</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(PatientLabTest patientLabTest);

        /// <summary>
        /// saves lab test results fot patient lab test
        /// </summary>
        /// <param name="patientLabTestResults"> patient lab test results</param>
        /// <param name="patientLabTestId"> patient lab test id</param>
        /// <returns>OperationStatus</returns>
        OperationStatus SaveResults(List<PatientLabTestResult> patientLabTestResults, int patientLabTestId);
        OperationStatus UpdateDate(PatientLabTest patientLabTest);
        List<PatientLabTestResult> GetAllSimilarLabTestResultsForPatient(int patientId, int patientLabTestId);
    }
}
