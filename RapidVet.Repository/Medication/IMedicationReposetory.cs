﻿using System;
using System.Collections.Generic;
using System.Text;
using RapidVet.Model.Visits;
using RapidVet.Model;

namespace RapidVet.Repository
{
    /// <summary>
    /// Take car of the medications data base interactions
    /// </summary>
  public  interface IMedicationReposetory
    {
        /// <summary>
        /// Return all active medication for this clinic group
        /// </summary>
        /// <param name="clinicGroup"></param>
        /// <returns></returns>
        IEnumerable<Medication> GetClinicGroupMedications(int clinicGroup);

        /// <summary>
        /// return the medication base on the id
        /// </summary>
        /// <param name="medicationId"></param>
        /// <returns></returns>
        Medication GetMedication(int medicationId);

        /// <summary>
        /// Add new medication to the list
        /// </summary>
        /// <param name="medication"></param>
        /// <returns></returns>
        OperationStatus AddMedication(Medication medication);

      /// <summary>
      /// saves entity to DB
      /// </summary>
        /// <param name="medication">Medication object</param>
      /// <returns></returns>
        OperationStatus Save(Medication medication);
    }
}
