﻿using System.Collections.Generic;
using System.Linq;
using RapidVet.Model;
using RapidVet.Model.Visits;

namespace RapidVet.Repository
{
    public class MedicationReposetory : RepositoryBase<RapidVetDataContext>, IMedicationReposetory
    {
        public MedicationReposetory(RapidVetDataContext context)
            : base(context)
        {

        }

        public IEnumerable<Medication> GetClinicGroupMedications(int clinicGroup)
        {
            return DataContext.Medications.Where(m => m.Active && m.ClinicGroupId == clinicGroup).OrderBy(m => m.Name);
        }

        public Medication GetMedication(int medicationId)
        {
            return DataContext.Medications.Single(m => m.Id == medicationId);
        }

        public OperationStatus AddMedication(Medication medication)
        {
            DataContext.Medications.Add(medication);
            return Save(medication);
        }

        public OperationStatus Save(Medication medication)
        {
            return base.Save(medication);
        }
    }
}