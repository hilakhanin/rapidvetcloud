﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Clients.Details;

namespace RapidVet.Repository.RegionalCouncils
{
   public interface IRegionalCouncilsRepository : IDisposable
   {
       /// <summary>
       /// get all regional councils, ordered by codes
       /// </summary>
       /// <returns></returns>
       IQueryable<RegionalCouncil> GetAll();

       /// <summary>
       /// get all regional councils w. cities, ordered by code
       /// </summary>
       /// <returns></returns>
       IQueryable<RegionalCouncil> GetAllWithCities();
       List<RegionalCouncil> GetAllWithEmails();
       List<RegionalCouncil> GetAllWithoutEmails();
           /// <summary>
       /// create new regional council
       /// </summary>
       /// <param name="regionalCouncil">Regional council obj</param>
       /// <returns>Operation status obj</returns>
       OperationStatus Create(RegionalCouncil regionalCouncil);

       /// <summary>
       /// remove regional council obj
       /// </summary>
       /// <param name="regionalCouncil">regional council obj</param>
       void Delete(RegionalCouncil regionalCouncil);

       /// <summary>
       /// get regional council obj by id
       /// </summary>
       /// <param name="regionalCouncilId">int regional council id</param>
       /// <returns>regional council obj</returns>
       RegionalCouncil GetRegionalCoucil(int regionalCouncilId);

       List<City> GetRegionalCouncilCities(int regionalCouncilId);
   }
}
