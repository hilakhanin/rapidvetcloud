﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Model;
using RapidVet.Model.Clients.Details;

namespace RapidVet.Repository.RegionalCouncils
{
    public class RegionalCouncilsRepository : RepositoryBase<RapidVetDataContext>, IRegionalCouncilsRepository
    {
        public RegionalCouncilsRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public RegionalCouncilsRepository()
        {

        }

        public IQueryable<RegionalCouncil> GetAll()
        {
            return DataContext.RegionalCouncils.OrderBy(r => r.Code);
        }

        public IQueryable<RegionalCouncil> GetAllWithCities()
        {
            return DataContext.RegionalCouncils.Include(r => r.Cities).OrderBy(r=>r.Code);
        }

        public List<RegionalCouncil> GetAllWithEmails()
        {
            return DataContext.RegionalCouncils.Where(r=>r.Email != null && r.Email.Length > 0).OrderBy(r => r.Name).ToList();
        }

        public List<RegionalCouncil> GetAllWithoutEmails()
        {
            var councilsWithMailIds = GetAllWithEmails().Select(c=>c.Id);
            return DataContext.RegionalCouncils.Where(r => !councilsWithMailIds.Contains(r.Id)).ToList();
        }

        public OperationStatus Create(RegionalCouncil regionalCouncil)
        {
            DataContext.RegionalCouncils.Add(regionalCouncil);
            return base.Save(regionalCouncil);
        }

        public void Delete(RegionalCouncil regionalCouncil)
        {
            DataContext.RegionalCouncils.Remove(regionalCouncil);
        }

        public RegionalCouncil GetRegionalCoucil(int regionalCouncilId)
        {
            return DataContext.RegionalCouncils.Single(r => r.Id == regionalCouncilId);
        }

        public List<City> GetRegionalCouncilCities(int regionalCouncilId)
        {
            return DataContext.Cities.Where(c => c.RegionalCouncilId == regionalCouncilId).ToList();
        }
    }
}
