﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model.Users;

namespace RapidVet.Repository
{
    public interface IConcurrentUserEntityRepository : IDisposable
    {
        /// <summary>
        /// adds the connected user to the concurrent users table
        /// </summary>
        /// <param name="entity">ConcurrentUserEntity</param>
        void Add(ConcurrentUserEntity entity);

        /// <summary>
        /// removes user from concurrent users table
        /// </summary>
        /// <param name="userGuid">guid that represents user</param>
        void Remove(Guid userGuid);

        /// <summary>
        /// updates the active clinic id property for the current user
        /// </summary>
        /// <param name="userGuid">guid that represents user</param>
        /// <param name="clinicId"> clinic id</param>
        void UpdateClinic(Guid userGuid, int clinicId);

        /// <summary>
        /// updates the clinic group id property for the current user
        /// </summary>
        /// <param name="userGuid">guid that represents user</param>
        /// <param name="clinicGroupId"> clinic group id</param>
        void UpdateClinicGroup(Guid userGuid, int clinicGroupId);

        /// <summary>
        /// returns all currently connected users
        /// </summary>
        /// <returns>List(ConcurrentUserEntity)</returns>
        List<ConcurrentUserEntity> GetList();
    }
}
