﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;

namespace RapidVet.Repository.DataMigrationRepository
{
    public interface IDataMigrationRepository : IDisposable
    {
        /// <summary>
        /// update / create DataMigrationLog obj
        /// </summary>
        /// <param name="entry">DataMigrationLog</param>
        /// <returns>OperationStatus</returns>
        OperationStatus UpdateLog(DataMigrationLog entry);

        /// <summary>
        /// get specific DataMigrationLog obj by params
        /// </summary>
        /// <param name="clinicGroupId">int clinic group id</param>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>DataMigrationLog</returns>
        DataMigrationLog GetEntry(int clinicGroupId, int clinicId);

        /// <summary>
        /// update archive migration log
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <param name="archiveDocumentItemId">int archive document id</param>
        /// <param name="oldLocation">string old file location</param>
        /// <returns>OperationStatus</returns>
        OperationStatus LogFailedFile(int clinicId, int? archiveDocumentItemId, string oldLocation);

    }
}
