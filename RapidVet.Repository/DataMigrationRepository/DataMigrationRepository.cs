﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model;
using RapidVet.Model.DataMigration;

namespace RapidVet.Repository.DataMigrationRepository
{
    public class DataMigrationRepository : RepositoryBase<RapidVetDataContext>, IDataMigrationRepository
    {
        public DataMigrationRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public DataMigrationRepository()
        {

        }

        public OperationStatus UpdateLog(DataMigrationLog entry)
        {
            var existing =
                DataContext.DataMigrationLogs.SingleOrDefault(
                    d => d.ClinicGroupId == entry.ClinicGroupId && d.ClinicId == entry.ClinicId);
            if (existing != null)
            {
                existing.LastSuccessfullStageId = entry.LastSuccessfullStageId;
                existing.DateTime = entry.DateTime;
                existing.Exception = string.Empty;

                var isLast = entry.LastSuccessfullStage == Enum.GetValues(typeof(MigrationOrder)).Cast<MigrationOrder>().Last();
                if (isLast)
                {
                    entry.MigrationFinished = true;
                }
            }
            else
            {
                existing = entry;
                DataContext.DataMigrationLogs.Add(existing);
            }
            return base.Save(existing);
        }

        public DataMigrationLog GetEntry(int clinicGroupId, int clinicId)
        {
            return
                DataContext.DataMigrationLogs.SingleOrDefault(
                    d => d.ClinicGroupId == clinicGroupId && d.ClinicId == clinicId);
        }

        public OperationStatus LogFailedFile(int clinicId, int? archiveDocumentItemId, string oldLocation)
        {
            var logEntry = new ArchiveMigrationLog()
                {
                    ClinicId = clinicId,
                    ArchiveDocumentId = archiveDocumentItemId,
                    LocalFilePath = oldLocation,
                    DateTime = DateTime.Now
                };

            DataContext.ArchiveMigrationLogs.Add(logEntry);
            return base.Save(logEntry);
        }
    }
}
