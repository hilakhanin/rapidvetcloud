﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Expenses;

namespace RapidVet.Repository.Suppliers
{
    public interface ISupplierRepository : IDisposable
    {
        /// <summary>
        /// get all clinic suppliers
        /// </summary>
        /// <param name="clinicId">int clinic id</param>
        /// <returns>IQueryable<Supplier></returns>
        IQueryable<Supplier> GetClinicSuppliers(int clinicId);

        /// <summary>
        /// get supplier object by id
        /// </summary>
        /// <param name="supplierId"> int supplier id</param>
        /// <returns>Supplier obj</returns>
        Supplier GetSupplier(int supplierId);

        /// <summary>
        /// create supplier obj
        /// </summary>
        /// <param name="supplier"> Supplier obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Create(Supplier supplier);

        /// <summary>
        /// deletes supplier obj
        /// </summary>
        /// <param name="supplier">Supplier obj</param>
        /// <returns>OperationStatus</returns>
        OperationStatus Delete(Supplier supplier);

        /// <summary>
        /// check if supplier is in clini
        /// </summary>
        /// <param name="clinicId">clinic id</param>
        ///  /// <param name="supplierId">supplier id</param>
        /// <returns>bool</returns>
        bool IsSupplierInClinic(int clinicId, int supplierId);

        /// <summary>
        /// get suopplier name
        /// </summary>
        /// <param name="supplierId">int supplier id</param>
        /// <returns>string</returns>
        string GetName(int supplierId);
    }
}
