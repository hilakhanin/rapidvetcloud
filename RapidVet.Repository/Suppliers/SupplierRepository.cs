﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model;
using RapidVet.Model.Expenses;

namespace RapidVet.Repository.Suppliers
{
    public class SupplierRepository : RepositoryBase<RapidVetDataContext>, ISupplierRepository
    {

        public SupplierRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

        public SupplierRepository()
        {

        }

        public IQueryable<Supplier> GetClinicSuppliers(int clinicId)
        {
            return DataContext.Suppliers.Where(s => s.ClinicId == clinicId).OrderBy(s => s.Name);
        }

        public Supplier GetSupplier(int supplierId)
        {
            return DataContext.Suppliers.Single(s => s.Id == supplierId);
        }

        public OperationStatus Create(Supplier supplier)
        {
            DataContext.Suppliers.Add(supplier);
            return base.Save(supplier);
        }

        public OperationStatus Delete(Supplier supplier)
        {
            DataContext.Suppliers.Remove(supplier);
            return base.Save(supplier);
        }

        public bool IsSupplierInClinic(int clinicId, int supplierId)
        {
            return DataContext.Suppliers.Single(s => s.Id == supplierId).ClinicId == clinicId;
        }

        public string GetName(int supplierId)
        {
            return DataContext.Suppliers.Single(s => s.Id == supplierId).Name;
        }
    }
}
