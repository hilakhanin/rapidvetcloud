using System;
using RapidVet.Model;

namespace RapidVet.Exeptions
{
    public class OperationStatusException : Exception
    {
        private OperationStatus _result;

        public OperationStatusException(OperationStatus result)
        {
            _result = result;
        }

        public override string Message
        {
            get
            {
                return _result.ExceptionMessage;
            }
        }

        public override string StackTrace
        {
            get
            {
                return _result.ExceptionInnerStackTrace;
            }
        }

    }
}