﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Model.Visits;

namespace RapidVet.Repository.Statistics
{
    public interface IStatisticsRepository
    {
        IQueryable<DangerousDrug> GetDangerousDrugs(DateTime fromDate, DateTime toDate, int medicationId, int clinicGroupId);
    }
}
