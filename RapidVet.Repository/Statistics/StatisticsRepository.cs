﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using RapidVet.Model.Visits;

namespace RapidVet.Repository.Statistics
{
    public class StatisticsRepository : RepositoryBase<RapidVetDataContext>, IStatisticsRepository
    {

         public StatisticsRepository(RapidVetDataContext dataContext)
            : base(dataContext)
        {

        }

         public StatisticsRepository()
        {

        }

        public IQueryable<DangerousDrug> GetDangerousDrugs(DateTime fromDate, DateTime toDate, int medicationId, int clinicGroupId)
        {

            return DataContext.DangerousDrugs
                .Include(d=>d.Medication)
                .Include(d=>d.Visit)
                .Include("Visit.Doctor")
                .Include("Visit.Patient")
                .Include("Visit.Patient.Client")
                .Where(d =>
                                                    d.Medication.ClinicGroupId == clinicGroupId &&
                                                    (medicationId == 0 || d.MedicationId == medicationId) &&
                                                    fromDate <= d.Visit.VisitDate && d.Visit.VisitDate <= toDate);

        }
    }
}
