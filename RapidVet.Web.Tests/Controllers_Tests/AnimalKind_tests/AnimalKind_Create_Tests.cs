﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Tests.Controllers_Tests.AnimalKind_tests
{
    //return a blank AnimalKindCreateModel
    //saves animal kind
    //administrator OR clinicGroupManager only
    //clinicGroupId is required
    //value is required

    [TestClass]
    public class AnimalKind_Create_Tests
    {

        [TestMethod]
        public void returns_blank_AnimalKindCreateModel_view()
        {
            //arrange
            var mockAnimalKindRepository = new Mock<IAnimalKindRepository>();
            var clinicGroupId = 1;
            var controller = new AnimalKindsController(mockAnimalKindRepository.Object);

            //act
            var result = (PartialViewResult)controller.Create(clinicGroupId);

            //assert
            Assert.AreEqual("", result.ViewName);

            mockAnimalKindRepository.VerifyAll();
        }


        [TestMethod]
        public void successfully_save_AnimalKind()
        {
            //arrange
            int clinicGroupId = 1;
            var model = new AnimalKindModel()
                            {
                                ClinicGroupId = clinicGroupId,
                                Value = "Rabbit"
                            };
            var mockAnimalKindRepository = new Mock<IAnimalKindRepository>();

            mockAnimalKindRepository.Setup(r => r.Create(It.IsAny<AnimalKind>()))
                                                 .Returns(() => new OperationStatus()
                                                                    {
                                                                        Success = true
                                                                    });

            var controller = new AnimalKindsController(mockAnimalKindRepository.Object);

            //act
            var result = (JsonResult) controller.Create(model);


            //assert
            mockAnimalKindRepository.VerifyAll();
        }

        [TestMethod]
        public void value_is_required()
        {
            //arrange
            int clinicGroupId = 1;
            var model = new AnimalKindModel()
            {
                ClinicGroupId = clinicGroupId,
                Value = ""
            };
            var mockAnimalKindRepository = new Mock<IAnimalKindRepository>();

            var controller = new AnimalKindsController(mockAnimalKindRepository.Object);
            controller.ModelState.AddModelError("Value", "this Field is Required");

            //act
            var result = (JsonResult)controller.Create(model);

            //assert
            result.AssertViewRendered();
            mockAnimalKindRepository.VerifyAll();
        }
    }
}
