﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Tests.Controllers_Tests.AnimalKind_tests
{
    //returns animalKindEdit view with animal kind data
    //value is required
    //administrator OR clinicGroupManager
    [TestClass]
    public class AnimalKind_Edit_Tests
    {
        [TestMethod]
        public void returns_edit_view_with_AnimalKind_data()
        {
            //arrange
            var mockAnimalKindRepository = new Mock<IAnimalKindRepository>();

            var animalKindId = 1;
            mockAnimalKindRepository.Setup(k => k.GetItem(It.IsAny<int>()))
                .Returns(() => new AnimalKind()
                                   {
                                       ClinicGroupId = 1,
                                       Id = animalKindId,
                                       Value = "Rabbit"
                                   });
            var controller = new AnimalKindsController(mockAnimalKindRepository.Object);

            //act
            // var result = (ViewResult)controller.ClinicEdit(animalKindId , null);

            //assert
            //Assert.AreEqual("", result.ViewName);

            //result.AssertViewRendered();

            //mockAnimalKindRepository.VerifyAll();

        }

        [TestMethod]
        public void successfully_save_animalKind()
        {
            //arrange
            var mockAnimalKindRepository = new Mock<IAnimalKindRepository>();
            var clinicGroupId = 1;
            var animalKindId = 1;
            mockAnimalKindRepository.Setup(k => k.GetItem(It.IsAny<int>()))
                .Returns(() => new AnimalKind()
                                   {
                                       ClinicGroupId = clinicGroupId,
                                       Id = animalKindId,
                                       Value = "Rabbit"
                                   });

            mockAnimalKindRepository.Setup(k => k.Update(It.IsAny<AnimalKind>()))
                .Returns(() => new OperationStatus()
                                   {
                                       Success = true
                                   });
            TestControllerBuilder builder = new TestControllerBuilder();
            var controller = builder.CreateController<AnimalKindsController>(mockAnimalKindRepository.Object);

            var model = new AnimalKindModel()
                {
                    ClinicGroupId = clinicGroupId,
                    Id = animalKindId,
                    Value = "Rabbit " + DateTime.Now.Ticks
                };

            //act
            var result = (JsonResult)controller.Edit(model);

            //assert
            result.AssertActionRedirect();
            mockAnimalKindRepository.VerifyAll();

        }

        [TestMethod]
        public void value_is_required()
        {
            //arrange
            var mockAnimalKindRepository = new Mock<IAnimalKindRepository>();

            var animalKindId = 1;
            mockAnimalKindRepository.Setup(k => k.GetItem(It.IsAny<int>()))
                .Returns(() => new AnimalKind()
                {
                    ClinicGroupId = 1,
                    Id = animalKindId,
                    Value = "Rabbit"
                });

            TestControllerBuilder builder = new TestControllerBuilder();
            var controller = builder.CreateController<AnimalKindsController>(mockAnimalKindRepository.Object);


            var model = new AnimalKindModel()
            {
                ClinicGroupId = 1,
                Id = animalKindId,
                Value = " "
            };



            //act
            var result = (JsonResult)controller.Edit(model);

            //assert
            result.AssertViewRendered();
            mockAnimalKindRepository.VerifyAll();
        }
    }
}
