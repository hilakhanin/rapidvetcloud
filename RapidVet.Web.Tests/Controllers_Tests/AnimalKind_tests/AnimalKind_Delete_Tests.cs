﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.AnimalKind_tests
{
    [TestClass]
    public class AnimalKind_Delete_Tests
    {
        //returns view eith animalKind data
        //successfylle delete animalKind

        [TestMethod]
        public void successfully_deletes_animalKind_data()
        {
            //arrange
            var mockAnimalKindRepository = new Mock<IAnimalKindRepository>();
            int clinicGroupId = 1;
            int animalKindId = 1;
            mockAnimalKindRepository.Setup(x => x.GetItem(animalKindId))
                .Returns(new AnimalKind()
                             {
                                 Id = animalKindId,
                                 ClinicGroupId = clinicGroupId,
                                 Value = "Rabbit"
                             });

            mockAnimalKindRepository.Setup(k => k.Delete(It.IsAny<int>()))
                .Returns(() => new OperationStatus()
                                   {
                                       Success = true,
                                       ItemId = clinicGroupId
                                   });


            var controller = new AnimalKindsController(mockAnimalKindRepository.Object);

            AnimalKind animalKind = new AnimalKind()
                                        {
                                            Id = animalKindId,
                                            ClinicGroupId = clinicGroupId,
                                            Value = "Rabbit"
                                        };

            //act
            var result = (RedirectToRouteResult)controller.Delete(animalKind.Id);

            //assert
            Assert.AreEqual("ClinicEdit", result.RouteValues["action"]);
            Assert.AreEqual(clinicGroupId, result.RouteValues["id"]);
            mockAnimalKindRepository.VerifyAll();
        }
    }
}
