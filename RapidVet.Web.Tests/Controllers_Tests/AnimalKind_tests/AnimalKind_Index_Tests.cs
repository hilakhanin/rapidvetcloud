﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.AnimalKind_tests
{
    //return view with data [all animal kinds in clinicGroup]
    //accecible to administrator OR clinicGroupManager

    [TestClass]
    public class AnimalKind_Index_Tests
    {
        private IQueryable<AnimalKind> animalKindList;

        public AnimalKind_Index_Tests()
        {
            animalKindList = new List<AnimalKind>()
                                 {
                                     new AnimalKind() {Id = 1, Value = "Dog", ClinicGroupId = 1},
                                     new AnimalKind() {Id = 2, Value = "Cat", ClinicGroupId = 1},
                                     new AnimalKind() {Id = 3, Value = "Mouse", ClinicGroupId = 1},
                                     new AnimalKind() {Id = 3, Value = "Lizard", ClinicGroupId = 1}
                                 }.AsQueryable();
        }

        [TestMethod]
        public void returns_Index_view_with_data()
        {
            //arrange
            var mockAnimalKindRepository = new Mock<IAnimalKindRepository>();
            mockAnimalKindRepository.Setup(r => r.GetList(It.IsAny<int>())).Returns(() => animalKindList);
            var animalKindController = new AnimalKindsController(mockAnimalKindRepository.Object);
            int clinicGroupId = 1;

            //act
            var view = (PartialViewResult)animalKindController.Index(clinicGroupId,new RedirectData());

            //assert
            Assert.AreEqual("", view.ViewName);
            mockAnimalKindRepository.VerifyAll();
        }
    }
}
