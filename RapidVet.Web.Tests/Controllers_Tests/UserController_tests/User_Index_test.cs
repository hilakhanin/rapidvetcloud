﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcContrib.TestHelper;
using RapidVet.Model.Users;
using Moq;
using RapidVet.Repository;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.UserController_tests
{

    //Retuerns a list of all the users.
    //Only active users are visable
    [TestClass]
    public class User_Index_test
    {
        [TestMethod]
        public void view_return_list_of_all_users_when_list_is_empty()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            IQueryable<User> emptyList = new List<User>().AsQueryable();

            userReposetorMock.Setup(r => r.GetList()).Returns(() => emptyList);

            var usersController = new UsersController(userReposetorMock.Object);

            var view = (ViewResult)usersController.Index(null);

            view.AssertViewRendered();

            Assert.AreEqual("", view.ViewName);

            userReposetorMock.VerifyAll();
        }

        [TestMethod]
        public void view_return_list_of_all_users()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            var usersList = new List<User>()
                                       {
new User(){Username = "user 1"},
new User(){Username = "user 2"},
new User(){Username = "user 3"},
                                       }.AsQueryable();

            userReposetorMock.Setup(r => r.GetList()).Returns(() => usersList);

            var usersController = new UsersController(userReposetorMock.Object);

            var view = (ViewResult)usersController.Index(null);

            view.AssertViewRendered();

            Assert.AreEqual("", view.ViewName);

            userReposetorMock.VerifyAll();
        }


        [TestMethod]
        public void view_return_list_of_users_by_clinic_id()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            var usersList = new List<User>()
                                       {
new User(){Username = "user 1"},
new User(){Username = "user 2"},
new User(){Username = "user 3"},
                                       }.AsQueryable();

            userReposetorMock.Setup(r => r.GetListByClinicGroup(It.IsAny<int>())).Returns(() => usersList);

            var usersController = new UsersController(userReposetorMock.Object);

            var view = (ViewResult)usersController.Index(1);

            view.AssertViewRendered();

            Assert.AreEqual("", view.ViewName);

            userReposetorMock.VerifyAll();
        }


    }
}
