﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Exeptions;
using RapidVet.Model;
using RapidVet.Model.Users;
using RapidVet.Repository;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.UserController_tests
{
    [TestClass]
    public class User_Edit_test :ControllerTestBase
    {
        //user can edit user details
        //no user threw exeption
        //save the user data
        //on faile save threw exeption



        [TestMethod]
        public void user_can_edit_user_details()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            IQueryable<User> emptyList = new List<User>().AsQueryable();

            userReposetorMock.Setup(r => r.GetItem(It.IsAny<int>())).Returns(() => new User());

            var usersController = new UsersController(userReposetorMock.Object);

            var view = (ViewResult)usersController.Edit(1);

            view.AssertViewRendered();

            Assert.AreEqual("", view.ViewName);

            userReposetorMock.VerifyAll();
        }

        [TestMethod]
        [ExpectedException(typeof(UserNotFoundException))]
        public void no_user_threw_exeption()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            IQueryable<User> emptyList = new List<User>().AsQueryable();

            userReposetorMock.Setup(r => r.GetItem(It.IsAny<int>()));

            var usersController = new UsersController(userReposetorMock.Object);

            var view = (ViewResult)usersController.Edit(1);

            view.AssertViewRendered();

            Assert.AreEqual("", view.ViewName);

            userReposetorMock.VerifyAll();
        }


        [TestMethod]
        public void save_the_user_data()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            IQueryable<User> emptyList = new List<User>().AsQueryable();

            userReposetorMock.Setup(r => r.GetItem(It.IsAny<int>())).Returns(() => new User());

            userReposetorMock.Setup(r => r.Save(It.IsAny<User>())).Returns(() => new OperationStatus() { Success = true });

            var formCollection = new FormCollection();
            formCollection.Add("UserId", "1");
            formCollection.Add("Username", "User");
            formCollection.Add("Email", "User@mail.com");
            formCollection.Add("Password", "123456");

            TestControllerBuilder builder = new TestControllerBuilder();

            var usersController = builder.CreateController<UsersController>(userReposetorMock.Object);

            var view = (ViewResult)usersController.Edit(formCollection, 1);

            view.AssertViewRendered();

            Assert.AreEqual("", view.ViewName);

            userReposetorMock.VerifyAll();
        }

        [ExpectedException(typeof(OperationStatusException))]
        [TestMethod]
        public void on_faile_save_threw_exeption()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            IQueryable<User> emptyList = new List<User>().AsQueryable();

            userReposetorMock.Setup(r => r.GetItem(It.IsAny<int>())).Returns(() => new User(){Id=1});

            userReposetorMock.Setup(r => r.Save(It.IsAny<User>())).Returns(() => new OperationStatus() { Success = false });

            var formCollection = new FormCollection();
            formCollection.Add("UserId", "1");
            formCollection.Add("Username", "User");
            formCollection.Add("Email", "User@mail.com");
            formCollection.Add("Password", "123456");

            TestControllerBuilder builder = new TestControllerBuilder();

            var usersController = builder.CreateController<UsersController>(userReposetorMock.Object);

            //var membershipMoq = MembershipServiceNoClinicGroup;

            //usersController.MembershipService = membershipMoq.Object;

            var view = (ViewResult)usersController.Edit(formCollection, 1);

            //membershipMoq.VerifyAll();

        }

    }
}
