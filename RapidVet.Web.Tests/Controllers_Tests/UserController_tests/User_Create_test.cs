﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Users;
using RapidVet.Repository;
using RapidVet.Web.Controllers;
using RapidVet.WebModels;
using System.Web.Security;
using RapidVetMembership;

namespace RapidVet.Web.Tests.Controllers_Tests.UserController_tests
{
    [TestClass]
    public class User_Create_test
    {
        [TestMethod]
        public void create_view_is_renderd()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            var usersController = new UsersController(userReposetorMock.Object);

            var view = (ViewResult)usersController.Create();

            view.AssertViewRendered();

            Assert.AreEqual("", view.ViewName);

            userReposetorMock.VerifyAll();
        }

        [TestMethod]
        public void new_user_is_created_no_clinic_group()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            var MembershipServiceMock = new Mock<IMembershipService>();

            TestControllerBuilder builder = new TestControllerBuilder();

            var usersController = builder.CreateController<UsersController>(userReposetorMock.Object);

            usersController.MembershipService = MembershipServiceMock.Object;

            var mebershipResult = MembershipCreateStatus.Success;

            var membershipUser = new MembershipUser(System.Web.Security.Membership.Provider.Name, "uname", null, "email",null, null, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);


            MembershipServiceMock.Setup(
                m =>
                m.CreateUser(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                             It.IsAny<string>(), It.IsAny<bool>(), out mebershipResult)).Returns(() => membershipUser);

            var view = (RedirectToRouteResult)usersController.Create(new CreateUserModel(){Password = "123456" , ConfirmPassword = "123456" , UserName = "uname" , Email = "email"});

            view.AssertActionRedirect();

            userReposetorMock.VerifyAll();

            MembershipServiceMock.VerifyAll();
        }

        [TestMethod]
        public void new_user_is_created_under_clinic_group()
        {
            string userName = "uname";

            var userReposetorMock = new Mock<IUserRepository>();

            userReposetorMock.Setup(x => x.GetItemByUserName(userName)).Returns(new User() {Username = userName});

            userReposetorMock.Setup(x => x.Save(It.IsAny<User>())).Returns(new OperationStatus(){Success = true});

            var MembershipServiceMock = new Mock<IMembershipService>();

            TestControllerBuilder builder = new TestControllerBuilder();

            var usersController = builder.CreateController<UsersController>(userReposetorMock.Object);

            usersController.MembershipService = MembershipServiceMock.Object;

            var mebershipResult = MembershipCreateStatus.Success;

            var membershipUser = new MembershipUser(System.Web.Security.Membership.Provider.Name, userName, null, "email", null, null, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);


            MembershipServiceMock.Setup(
                m =>
                m.CreateUser(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                             It.IsAny<string>(), It.IsAny<bool>(), out mebershipResult)).Returns(() => membershipUser);

            var view = (RedirectToRouteResult)usersController.Create(new CreateUserModel() { Password = "123456", ConfirmPassword = "123456", UserName = userName, Email = "email", ClinicId = 1 });

            view.AssertActionRedirect();

            userReposetorMock.VerifyAll();

            MembershipServiceMock.VerifyAll();



        }

        [TestMethod]
        public void duplicated_user_is_not_saved()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            var MembershipServiceMock = new Mock<IMembershipService>();

            TestControllerBuilder builder = new TestControllerBuilder();

            var usersController = builder.CreateController<UsersController>(userReposetorMock.Object);

            usersController.MembershipService = MembershipServiceMock.Object;

            var mebershipResult = MembershipCreateStatus.DuplicateUserName;

            var membershipUser = new MembershipUser(System.Web.Security.Membership.Provider.Name, "uname", null, "email", null, null, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);


            MembershipServiceMock.Setup(
                m =>
                m.CreateUser(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                             It.IsAny<string>(), It.IsAny<bool>(), out mebershipResult)).Returns(() => membershipUser);

            var view = (ViewResult)usersController.Create(new CreateUserModel() { Password = "123456", ConfirmPassword = "123456", UserName = "uname", Email = "email" });

            view.AssertViewRendered();

            Assert.IsTrue(!view.ViewData.ModelState.IsValid);

            userReposetorMock.VerifyAll();

            MembershipServiceMock.VerifyAll();
        }

    }
}
