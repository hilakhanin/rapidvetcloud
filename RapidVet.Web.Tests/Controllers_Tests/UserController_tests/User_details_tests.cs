﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Exeptions;
using RapidVet.Model.Users;
using RapidVet.Repository;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.UserController_tests
{
    [TestClass]
    public class User_details_tests
    {
        //returens view withe all the users details
        //when no user threw exeption

        [TestMethod]
        public void retuerns_view_with_all_the_details()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            userReposetorMock.Setup(r => r.GetItem(It.IsAny<int>())).Returns(() => new User());

            var usersController = new UsersController(userReposetorMock.Object);

            var view = (ViewResult)usersController.Details(1);

            view.AssertViewRendered();

            Assert.AreEqual("", view.ViewName);

            userReposetorMock.VerifyAll();
        }

        [TestMethod]
        [ExpectedException(typeof(UserNotFoundException))]
        public void when_no_user_threw_exeption()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            userReposetorMock.Setup(r => r.GetItem(It.IsAny<int>()));

            var usersController = new UsersController(userReposetorMock.Object);

            var view = (ViewResult)usersController.Details(1);

            view.AssertViewRendered();

            Assert.AreEqual("", view.ViewName);

            userReposetorMock.VerifyAll();
        }
    }
}
