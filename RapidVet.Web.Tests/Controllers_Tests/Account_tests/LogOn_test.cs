﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Repository;
using RapidVet.Web.Controllers;
using RapidVet.WebModels;
using RapidVetMembership;

namespace RapidVet.Web.Tests.Controllers_Tests.Account_tests
{
    [TestClass]
    public class LogOn_tests
    {
        [TestMethod]
        public void valid_user_can_login_to_returnUrl()
        {
            var userReposetorMock = new Mock<IUserRepository>();

            var membershipServiceMock = new Mock<IMembershipService>();

            var formsAuthenticationServiceMock = new Mock<IFormsAuthenticationService>();

            formsAuthenticationServiceMock.Setup(m => m.SetAuthCookie(It.IsAny<string>(), It.IsAny<bool>()));

            TestControllerBuilder builder = new TestControllerBuilder();

            var accountController = builder.CreateController<AccountController>(userReposetorMock.Object, formsAuthenticationServiceMock.Object);

            accountController.MembershipService = membershipServiceMock.Object;

          //  membershipServiceMock.Setup(m => m.ValidateUser(It.IsAny<string>(), It.IsAny<string>())).Returns(() => true);

            var mebershipResult = MembershipCreateStatus.Success;

            var returnUrl = "/Home/About";

            var view =
                (RedirectResult)
                accountController.LogOn(
                    new LogOnModel() { Password = "123456", UserName = "userName", RememberMe = false }, returnUrl);

            view.AssertHttpRedirect();

            Assert.AreEqual(returnUrl, view.Url);

            userReposetorMock.VerifyAll();
        }

        [TestMethod]
        public void valid_user_with_invalid_password_cant_login()
        {

        }

        [TestMethod]
        public void invalid_user_cant_login()
        {
        }

        [TestMethod]
        public void user_is_redirected_()
        {


        }


    }
}
