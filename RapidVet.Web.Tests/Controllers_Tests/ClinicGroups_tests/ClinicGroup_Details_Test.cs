﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model.Clinics;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.ClinicGroups_tests
{
    //returns view with clinicGroup Data
    [TestClass]
    public class ClinicGroup_Details_Test
    {
        private ClinicGroup clinicGroup;

        public ClinicGroup_Details_Test()
        {
            var clinicList = new List<Clinic>()
                                          {
                                              new Clinic()
                                                  {Id = 1, CreatedById = 1, Name = "Clinic1", ClinicGroupID = 1},
                                              new Clinic()
                                                  {Id = 1, CreatedById = 1, Name = "Clinic2", ClinicGroupID = 1},
                                              new Clinic()
                                                  {Id = 1, CreatedById = 1, Name = "Clinic3", ClinicGroupID = 1},
                                              new Clinic()
                                                  {Id = 1, CreatedById = 1, Name = "Clinic4", ClinicGroupID = 1},
                                              new Clinic()
                                                  {Id = 1, CreatedById = 1, Name = "Clinic5", ClinicGroupID = 1}
                                          };

            clinicGroup = new ClinicGroup()
                              {
                                  Id = 1,
                                  Name = DateTime.Now.Ticks.ToString(),
                                  CreatedDate = DateTime.Now,
                                  CreatedById = 1,
                                  Clinics = clinicList
                              };
        }

        //[TestMethod]
        //public void returns_view_with_clinicGroup_Data()
        //{
        //    //arrange
        //    var mockClinicGroupRepository = new Mock<IClinicGroupRepository>();
        //    mockClinicGroupRepository.Setup(g => g.GetItem(It.IsAny<int>())).Returns(() => clinicGroup);
        //    var controller = new ClinicGroupsController(mockClinicGroupRepository.Object, null);

        //    //act
        //    var result = (ViewResult)controller.Details(clinicGroup.Id);

        //    //assert
        //    Assert.AreEqual("", result.ViewName);
        //    mockClinicGroupRepository.VerifyAll();
        //}
    }
}
