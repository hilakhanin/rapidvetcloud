﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.WebModels.ClinicGroups;

namespace RapidVet.Web.Tests.Controllers_Tests.ClinicGroups_tests
{

    // returns a blank ClinicGroupCreate view
    // gets data from view 
    // creates a new ClinicGroup entity
    // clinicGroup.Name is required
    //Only admin can ClinicGroupCreate


    [TestClass]
    public class ClinicGroups_Create :ControllerTestBase
    {

        public ClinicGroups_Create()
        {

        }

        [TestMethod]
        public void returns_clinicGroupCreate_view()
        {

            //arrange
            var clinicGroupRepsitoryMock = new Mock<IClinicGroupRepository>();
            var controller = new ClinicGroupsController(clinicGroupRepsitoryMock.Object, null);

            //act
            var result = (PartialViewResult)controller.Create();

            //assert
            Assert.AreEqual("", result.ViewName);

            clinicGroupRepsitoryMock.VerifyAll();

        }


        [TestMethod]
        public void successfully_create_new_clinicGroup_item()
        {

            //arrange
            var model = new ClinicGroupCreate()
                            {
                                Name = "ClinicGroup name create test" + DateTime.Now.Ticks,
                                Comments = "comment test - Clinic group create"
                            };

            var clinicGroupRepsitoryMock = new Mock<IClinicGroupRepository>();

            clinicGroupRepsitoryMock.Setup(r => r.Create(It.IsAny<ClinicGroup>())).Returns(() => new OperationStatus() { Success = true, ItemId = 1 });

            var controller = new ClinicGroupsController(clinicGroupRepsitoryMock.Object, null);
           
            var membershipMoq = MembershipServiceNoClinicGroup;

            controller.MembershipService = membershipMoq.Object;
            //act
            var result = (RedirectToRouteResult)controller.Create(model);

            //assert
            Assert.AreEqual("ClinicEdit", result.RouteValues["action"]);

            Assert.AreEqual(1, result.RouteValues["id"]);

            clinicGroupRepsitoryMock.VerifyAll();

            membershipMoq.VerifyAll();

        }

        [TestMethod]
        public void fail_create_new_clinicGroup_item_becouse_of_validation_errors()
        {
            // clinicGroup.Name is required

            //arrange
            var model = new ClinicGroupCreate()
            {
                Comments = "comment test - Clinic group create"
            };

            var clinicGroupRepsitoryMock = new Mock<IClinicGroupRepository>();

            var controller = new ClinicGroupsController(clinicGroupRepsitoryMock.Object, null);

            controller.ModelState.AddModelError("Name", "Name is Requierd!!");

            //act
            var result = (ViewResult)controller.Create(model);

            //assert
            Assert.IsNotNull(result.ViewData.ModelState["Name"]);

            Assert.AreEqual("", result.ViewName);

            clinicGroupRepsitoryMock.VerifyAll();

        }

        [TestMethod]
        public void fail_create_new_clinicGroup_item_save_to_db_problem()
        {

            //arrange
            var model = new ClinicGroupCreate()
            {
                Name = "ClinicGroup name create test" + DateTime.Now.Ticks,
                Comments = "comment test - Clinic group create"
            };

            var clinicGroupRepsitoryMock = new Mock<IClinicGroupRepository>();

            clinicGroupRepsitoryMock.Setup(r => r.Create(It.IsAny<ClinicGroup>())).Returns(() => new OperationStatus() { Success = false });

            var controller = new ClinicGroupsController(clinicGroupRepsitoryMock.Object, null);
            var membershipMoq = MembershipServiceNoClinicGroup;

            controller.MembershipService = membershipMoq.Object;
            //act
            var result = (ViewResult)controller.Create(model);

            //assert
            Assert.AreEqual("Error", result.ViewName);

            clinicGroupRepsitoryMock.VerifyAll();

            membershipMoq.VerifyAll();

        }
    }
}
