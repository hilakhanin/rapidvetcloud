﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.ClinicGroups;

namespace RapidVet.Web.Tests.Controllers_Tests.ClinicGroups_tests
{
    // returns view with clinicGroup details
    //get clinicGroup model from View, send to ClinicGroup. returns clinicGroup object with false values in all 'active' fields
    // (clinicGroup and all 
    // makes clinicGroup and all child clinics to not active




    [TestClass]
    public class ClinicGroup_Remove_Tests
    {
        private ClinicGroup clinicGroup;
        private ClinicGroup deactivatedClinicGroup;

        public ClinicGroup_Remove_Tests()
        {
            clinicGroup = new ClinicGroup()
                              {
                                  Id = 1,
                                  Name = DateTime.Now.Ticks.ToString(),
                                  Comments = "no comment",
                                  CreatedDate = DateTime.Now,
                                  CreatedById = 1,
                                  Active = true,
                                  Clinics = new List<Clinic>()
                                                {
                                                    new Clinic()
                                                        {
                                                            Id = 1,
                                                            ClinicGroupID = 1,
                                                            Name = "Clinic" + DateTime.Now.Ticks,
                                                            Active = true
                                                        },
                                                    new Clinic()
                                                        {
                                                            Id = 2,
                                                            ClinicGroupID = 1,
                                                            Name = "Clinic" + DateTime.Now.Ticks,
                                                            Active = true
                                                        },
                                                    new Clinic()
                                                        {
                                                            Id = 3,
                                                            ClinicGroupID = 1,
                                                            Name = "Clinic" + DateTime.Now.Ticks,
                                                            Active = true
                                                        },
                                                    new Clinic()
                                                        {
                                                            Id = 4,
                                                            ClinicGroupID = 1,
                                                            Name = "Clinic" + DateTime.Now.Ticks,
                                                            Active = true
                                                        }
                                                }

                              };

            deactivatedClinicGroup = new ClinicGroup()
            {
                Id = 1,
                Name = DateTime.Now.Ticks.ToString(),
                Comments = "no comment",
                CreatedDate = DateTime.Now,
                CreatedById = 1,
                Active = false,
                Clinics = new List<Clinic>()
                                                {
                                                    new Clinic()
                                                        {
                                                            Id = 1,
                                                            ClinicGroupID = 1,
                                                            Name = "Clinic" + DateTime.Now.Ticks,
                                                            Active = false
                                                        },
                                                    new Clinic()
                                                        {
                                                            Id = 2,
                                                            ClinicGroupID = 1,
                                                            Name = "Clinic" + DateTime.Now.Ticks,
                                                            Active = false
                                                        },
                                                    new Clinic()
                                                        {
                                                            Id = 3,
                                                            ClinicGroupID = 1,
                                                            Name = "Clinic" + DateTime.Now.Ticks,
                                                            Active = false
                                                        },
                                                    new Clinic()
                                                        {
                                                            Id = 4,
                                                            ClinicGroupID = 1,
                                                            Name = "Clinic" + DateTime.Now.Ticks,
                                                            Active = false
                                                        }
                                                }

            };



        }


        [TestMethod]
        public void returns_view_with_clinicGroup_details()
        {
            //arrange
            var mockClinicGroupRepository = new Mock<IClinicGroupRepository>();
            mockClinicGroupRepository.Setup(g => g.GetItem(It.IsAny<int>())).Returns(() => clinicGroup);
            var controller = new ClinicGroupsController(mockClinicGroupRepository.Object, null);

            //act
            var result = (ViewResult)controller.Remove(clinicGroup.Id);

            //assert
            Assert.AreEqual("", result.ViewName);
            mockClinicGroupRepository.VerifyAll();
        }



        [TestMethod]
        public void marks_clinicGroup_and_child_clinics_as_not_Active()
        {
            //arrange
            var deactivated = clinicGroup;

            //act
            deactivated.Deactivate();

            //assert
            foreach (var item in deactivated.Clinics)
            {
                Assert.IsFalse(item.Active);
            }
            Assert.IsFalse(deactivated.Active);
        }

        [TestMethod]
        public void successfully_update_clinicGroup_and_child_clinics_to_inactive()
        {
            //arrange
            var mockClinicGroupRepository = new Mock<IClinicGroupRepository>();
            mockClinicGroupRepository.Setup(g => g.GetItem(It.IsAny<int>())).Returns(() => deactivatedClinicGroup);
            mockClinicGroupRepository.Setup(g => g.Update(deactivatedClinicGroup)).Returns(new OperationStatus()
                                                                                               {
                                                                                                   Success = true
                                                                                               });

            var controller = new ClinicGroupsController(mockClinicGroupRepository.Object, null);
            

            //act
            var result = (RedirectToRouteResult)controller.Delete(clinicGroup.Id);

            //assert
            result.AssertActionRedirect();
            Assert.AreEqual("", result.RouteName);
            mockClinicGroupRepository.VerifyAll();
        }

    }

}
