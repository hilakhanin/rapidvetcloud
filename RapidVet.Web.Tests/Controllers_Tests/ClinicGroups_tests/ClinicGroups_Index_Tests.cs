﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RapidVet.Model.Clinics;
using Moq;
using RapidVet.Repository;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.ClinicGroups_tests
{

    //View by admin only

    /// <summary>
    /// Summary description for ClinicGroups_Index
    /// </summary>
    [TestClass]
    public class ClinicGroups_Index
    {

        IQueryable<ClinicGroup> clinicGroupsList;


        public ClinicGroups_Index()
        {

            clinicGroupsList = new List<ClinicGroup>()
                                       {
                                           new ClinicGroup() {Name = "ClinicGroup1"},
                                           new ClinicGroup() {Name = "ClinicGroup2"}
                                       }.AsQueryable();

        }


        [TestMethod]
        public void index_return_view()
        {

            var clinicGroupRepsitoryMock = new Mock<IClinicGroupRepository>();

            clinicGroupRepsitoryMock.Setup(r => r.GetList()).Returns(() => clinicGroupsList);

            var clinicGroupController = new ClinicGroupsController(clinicGroupRepsitoryMock.Object,null);

            var view = (ViewResult)clinicGroupController.Index();

            Assert.AreEqual("", view.ViewName);

            clinicGroupRepsitoryMock.VerifyAll();

        }

        [TestMethod]
        public void index_get_list_of_clinicGroups()
        {
            var clinicGroupRepsitoryMock = new Mock<IClinicGroupRepository>();

            clinicGroupRepsitoryMock.Setup(r => r.GetList()).Returns(clinicGroupsList);

            var clinicGroupController = new ClinicGroupsController(clinicGroupRepsitoryMock.Object , null);

            var view = (ViewResult)clinicGroupController.Index();

            var model = view.ViewData.Model as IEnumerable<ClinicGroup>;

            Assert.IsTrue(clinicGroupsList.SequenceEqual(model));
        }
    }
}
