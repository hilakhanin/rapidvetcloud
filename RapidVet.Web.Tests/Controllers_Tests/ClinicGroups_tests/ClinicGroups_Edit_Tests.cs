﻿//using System;
//using System.Text;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using System.Web.Routing;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using MvcContrib.TestHelper.Fakes;
//using RapidVet.Helpers;
//using RapidVet.Model.Users;
//using RapidVet.Repository.Clinics;
//using RapidVet.Web.Controllers;
//using RapidVet.Model.Clinics;
//using RapidVet.Model;
//using MvcContrib.TestHelper;


//namespace RapidVet.Web.Tests.Controllers_Tests.ClinicGroups_tests
//{
//    [TestClass]
//    public class ClinicGroups_Edit :ControllerTestBase
//    {
//        //returns a edit view
//        //sucssfulay save without file
//        //sucssfuly save with file
//        //file must be image
//        //name is required
//        //returns error when no data is saved to data base
//        //only admin or Clinic group manager can edit


//        [TestMethod]
//        public void edit_returns_view_with_clinic_group_data()
//        {
//            //arrange
//            var clinicGroupRepsitoryMock = new Mock<IClinicGroupRepository>();

//            clinicGroupRepsitoryMock.Setup(m => m.GetItem(It.IsAny<int>())).Returns(() => new ClinicGroup() { Name = "groupName" });

//            var controller = new ClinicGroupsController(clinicGroupRepsitoryMock.Object, null);


//            //act
//            var result = (ViewResult)controller.ClinicEdit(1 , null);

//            //assert
//            Assert.AreEqual("", result.ViewName);

//            result.AssertViewRendered();

//            clinicGroupRepsitoryMock.VerifyAll();
//        }

//        [TestMethod]
//        public void successfuly_save_without_file()
//        {
//            //arrange
//            var clinicGroupRepsitoryMock = new Mock<IClinicGroupRepository>();

//            clinicGroupRepsitoryMock.Setup(m => m.GetItem(It.IsAny<int>()))
//                .Returns(() => new ClinicGroup() { Id = 1, Name = "originelName" });

//            clinicGroupRepsitoryMock.Setup(m => m.Update(It.IsAny<ClinicGroup>())).Returns(
//                () => new OperationStatus() { Success = true });

//            TestControllerBuilder builder = new TestControllerBuilder();
//            var controller = builder.CreateController<ClinicGroupsController>(clinicGroupRepsitoryMock.Object, null);

//            var formCollection = new FormCollection();

//            var membershipMoq = MembershipServiceNoClinicGroup;

//            controller.MembershipService = membershipMoq.Object;

//            int id = 1;

//            formCollection.Add("Name", "Clinic Group Name");
//            formCollection.Add("Comments", "Clinic Group Comment");
//            formCollection.Add("Id", id.ToString());

//            //act
//            var result = (RedirectToRouteResult)controller.ClinicEdit(formCollection, id);

//            //assert
//            Assert.AreEqual("Index",result.RouteValues["action"]);
//            membershipMoq.VerifyAll();
//            clinicGroupRepsitoryMock.VerifyAll();
//        }

//        [TestMethod]
//        public void successfuly_save_with_file()
//        {
//            //arrange
//            var clinicGroupRepsitoryMock = new Mock<IClinicGroupRepository>();

//            clinicGroupRepsitoryMock.Setup(m => m.GetItem(It.IsAny<int>()))
//                .Returns(() => new ClinicGroup() { Id = 1, Name = "originelName" });

//            clinicGroupRepsitoryMock.Setup(m => m.Update(It.IsAny<ClinicGroup>())).Returns(
//                () => new OperationStatus() { Success = true });

//            TestControllerBuilder builder = new TestControllerBuilder();


//            var file1Mock = new Mock<HttpPostedFileBase>();
//            file1Mock.Setup(x => x.ContentType).Returns("image/jpeg");


//            builder.Files["LogoImage"] = file1Mock.Object;

//            var fileUploaderMock = new Mock<IFileUploader>();

//            fileUploaderMock.Setup(x => x.Upload(It.IsAny<HttpPostedFileBase>(), It.IsAny<int>(), It.IsAny<BlobName>())).Returns(() => new OperationStatus() { Success = true });

//            var controller = builder.CreateController<ClinicGroupsController>(clinicGroupRepsitoryMock.Object, fileUploaderMock.Object);

//            var membershipMoq = MembershipServiceNoClinicGroup;

//            controller.MembershipService = membershipMoq.Object;

//            var formCollection = new FormCollection();

//            formCollection.Add("Name", "Clinic Group Name");
//            formCollection.Add("Comments", "Clinic Group Comment");
//            formCollection.Add("Id", "1");


//            //act
//            var result = (RedirectToRouteResult)controller.ClinicEdit(formCollection, 1);

//            //assert
//           Assert.AreEqual("Index", result.RouteValues["action"]);
//            membershipMoq.VerifyAll();
//            clinicGroupRepsitoryMock.VerifyAll();
//            fileUploaderMock.VerifyAll();
//            file1Mock.VerifyAll();
//        }

//        [TestMethod]
//        public void file_must_be_image()
//        {
//            //arrange
//            var clinicGroupRepsitoryMock = new Mock<IClinicGroupRepository>();

//            clinicGroupRepsitoryMock.Setup(m => m.GetItem(It.IsAny<int>()))
//                .Returns(() => new ClinicGroup() { Id = 1, Name = "originelName" });

//            TestControllerBuilder builder = new TestControllerBuilder();


//            var file1Mock = new Mock<HttpPostedFileBase>();
//            file1Mock.Setup(x => x.ContentType).Returns("image/xxx");


//            builder.Files["LogoImage"] = file1Mock.Object;

//            var fileUploaderMock = new Mock<IFileUploader>();

//            var controller = builder.CreateController<ClinicGroupsController>(clinicGroupRepsitoryMock.Object, fileUploaderMock.Object);

//            var membershipMoq = MembershipServiceNoClinicGroup;

//            controller.MembershipService = membershipMoq.Object;

//            var formCollection = new FormCollection();

//            int id = 1;

//            formCollection.Add("Name", "Clinic Group Name");
//            formCollection.Add("Comments", "Clinic Group Comment");
//            formCollection.Add("Id", id.ToString());


//            //act
//            var result = (ViewResult)controller.ClinicEdit(formCollection, id);

//            //assert
//            result.AssertViewRendered();

//            Assert.IsTrue(result.ViewData.ModelState.ContainsKey("LogoImage"));

//            clinicGroupRepsitoryMock.VerifyAll();

//            fileUploaderMock.VerifyAll();

//            file1Mock.VerifyAll();

//            membershipMoq.VerifyAll();
//        }

//        [TestMethod]
//        public void name_is_required()
//        {
//            //arrange
//            var clinicGroupRepsitoryMock = new Mock<IClinicGroupRepository>();

//            clinicGroupRepsitoryMock.Setup(m => m.GetItem(It.IsAny<int>()))
//                .Returns(() => new ClinicGroup() { Id = 1, Name = "originelName" });

//            TestControllerBuilder builder = new TestControllerBuilder();
//            var controller = builder.CreateController<ClinicGroupsController>(clinicGroupRepsitoryMock.Object, null);

//            var formCollection = new FormCollection();

//            int id = 1;

//            formCollection.Add("Name", "");
//            formCollection.Add("Comments", "Clinic Group Comment");
//            formCollection.Add("Id", id.ToString());

//            //act
//            var result = (ViewResult)controller.ClinicEdit(formCollection, id);

//            //assert
//            Assert.IsTrue(result.ViewData.ModelState.ContainsKey("Name"));

//            clinicGroupRepsitoryMock.VerifyAll();
//        }



//        [TestMethod]
//        public void returns_error_when_no_data_is_saved_to_data_base()
//        {
//            //arrange
//            var clinicGroupRepsitoryMock = new Mock<IClinicGroupRepository>();

//            clinicGroupRepsitoryMock.Setup(m => m.GetItem(It.IsAny<int>()))
//                .Returns(() => new ClinicGroup() { Id = 1, Name = "originelName" });

//            clinicGroupRepsitoryMock.Setup(m => m.Update(It.IsAny<ClinicGroup>())).Returns(
//                () => new OperationStatus() { Success = false });

//            TestControllerBuilder builder = new TestControllerBuilder();
//            var controller = builder.CreateController<ClinicGroupsController>(clinicGroupRepsitoryMock.Object, null);
//            var membershipMoq = MembershipServiceNoClinicGroup;

//            controller.MembershipService = membershipMoq.Object;
//            var formCollection = new FormCollection();

//            int id = 1;

//            formCollection.Add("Name", "Clinic Group Name");
//            formCollection.Add("Comments", "Clinic Group Comment");
//            formCollection.Add("Id", id.ToString());

//            //act
//            var result = (ViewResult)controller.ClinicEdit(formCollection, id);

//            //assert
//            result.AssertViewRendered().ForView("Error");
//            membershipMoq.VerifyAll();
//            clinicGroupRepsitoryMock.VerifyAll();
//        }


        
//    }
//}
