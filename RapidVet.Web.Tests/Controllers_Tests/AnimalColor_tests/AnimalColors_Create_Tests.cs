﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Tests.Controllers_Tests.AnimalColor_tests
{
    //return a blank AnimalcolorCreateModel
    //saves animal color
    //administrator OR clinicGroupManager only
    //clinicGroupId is required
    //value is required
    [TestClass]
    public class AnimalColors_Create_Tests
    {
        [TestMethod]
        public void returns_a_blank_AnimalcolorCreateModel_view()
        {
            //arrange
            var mockAnimalColorRepository = new Mock<IAnimalColorRepository>();
            int clinicGroupId = 1;
            int animalKindId = 1;
            mockAnimalColorRepository.Setup(c => c.GetCreateModel(It.IsAny<int>()))
                                     .Returns(new AnimalColor()
                                                   {
                                                       Id = 1,
                                                       ClinicGroupId = 1,
                                                       Value = "Brown"
                                                   });

            var controller = new AnimalColorsController(mockAnimalColorRepository.Object);

            //act
            var result = (PartialViewResult)controller.Create(clinicGroupId);

            //assert
            Assert.AreEqual("", result.ViewName);
            mockAnimalColorRepository.VerifyAll();

        }

        [TestMethod]
        public void successfully_save_new_animalColor()
        {
            //arrange
            var mockAnimalColorRepository = new Mock<IAnimalColorRepository>();
            int clinicGroupId = 1;
            mockAnimalColorRepository.Setup(c => c.Create(It.IsAny<AnimalColor>()))
                                     .Returns(() => new OperationStatus()
                                                        {
                                                            Success = true
                                                        });
            var animalColor = new AnimalColorModel()
                                          {
                                              Id = 1,
                                              ClinicGroupId = clinicGroupId,
                                              Value = "Brown"
                                          };
            var controller = new AnimalColorsController(mockAnimalColorRepository.Object);

            //act
            var result = (JsonResult) controller.Create(animalColor);

            //assert
            mockAnimalColorRepository.VerifyAll();
        }

        [TestMethod]
        public void value_is_required()
        {
            //arrange
            var mockAnimalColorRepository = new Mock<IAnimalColorRepository>();
            int clinicGroupId = 1;

            var animalColor = new AnimalColorModel()
            {
                ClinicGroupId = clinicGroupId,
                Value = ""
            };
            var controller = new AnimalColorsController(mockAnimalColorRepository.Object);
            controller.ModelState.AddModelError("Value", "this field is required");

            //act
            var result = (JsonResult)controller.Create(animalColor);

            //assert
            result.AssertViewRendered();
     
            mockAnimalColorRepository.VerifyAll();
        }
    }
}
