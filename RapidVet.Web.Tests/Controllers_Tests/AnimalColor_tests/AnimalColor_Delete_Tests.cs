﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.AnimalColor_tests
{
    //returns view with selected animalColor item data
    //successfully deletes animalColor item
    [TestClass]
    public class AnimalColor_Delete_Tests
    {
        
        [TestMethod]
        public void successfully_delete_animalColor()
        {
            //assert
            int clinicGroupId = 1;
            AnimalColor animalColor = new AnimalColor()
                                          {
                                              Id = 1,
                                              ClinicGroupId = clinicGroupId,
                                              Value = "Groovy Green"
                                          };
            var mockAnimalColorRepository = new Mock<IAnimalColorRepository>();
            mockAnimalColorRepository.Setup(x => x.GetItem(animalColor.Id)).Returns(() => animalColor);
            mockAnimalColorRepository.Setup(c => c.Delete(It.IsAny<int>())).Returns(() => new OperationStatus()
                                                                                                      {
                                                                                                          Success = true,
                                                                                                          ItemId = clinicGroupId
                                                                                                      });
            

          
            var controller = new AnimalColorsController(mockAnimalColorRepository.Object);

            //act
            var result = (RedirectToRouteResult) controller.Delete(animalColor.Id);

            //assert
            Assert.AreEqual("ClinicEdit", result.RouteValues["action"]);
            Assert.AreEqual(clinicGroupId, result.RouteValues["id"]);
            mockAnimalColorRepository.VerifyAll();

        }
    }
}
