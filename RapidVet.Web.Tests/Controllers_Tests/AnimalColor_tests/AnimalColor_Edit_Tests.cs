﻿using System;
using System.Diagnostics;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Tests.Controllers_Tests.AnimalColor_tests
{
    //returns animalColorEdit view with animal color data
    //value is required
    //administrator OR clinicGroupManager
    [TestClass]
    public class AnimalColor_Edit_Tests
    {
        [TestMethod]
        public void returns_view_with_AnimalColor_data()
        {
            //arrange
            var mockAnimalColorRepository = new Mock<IAnimalColorRepository>();
            int clinicGroupId = 1;
            int animalColorId = 1;
            mockAnimalColorRepository.Setup(c => c.GetItem(It.IsAny<int>()))
                                     .Returns(() => new AnimalColor()
                                                        {
                                                            Id = 1,
                                                            ClinicGroupId = clinicGroupId,
                                                            Value = "Brown"
                                                        });

            var controller = new AnimalColorsController(mockAnimalColorRepository.Object);

            //act
            var result = (ViewResult)controller.Edit(animalColorId);

            //assert
            Assert.AreEqual("", result.ViewName);
            result.AssertViewRendered();
            mockAnimalColorRepository.VerifyAll();
        }

        [TestMethod]
        public void successfully_save_AnimalColor()
        {
            //arrange
            var mockAnimalColorRepository = new Mock<IAnimalColorRepository>();
            int clinicGroupId = 1;
            int animalColorId = 1;
            mockAnimalColorRepository.Setup(c => c.GetItem(It.IsAny<int>()))
                                     .Returns(() => new AnimalColor()
                                     {
                                         Id = 1,
                                         ClinicGroupId = clinicGroupId,
                                         Value = "Brown"
                                     });

            mockAnimalColorRepository.Setup(k => k.Update(It.IsAny<AnimalColor>()))
                .Returns(() => new OperationStatus()
                {
                    Success = true
                });
            TestControllerBuilder builder = new TestControllerBuilder();
            var controller = builder.CreateController<AnimalColorsController>(mockAnimalColorRepository.Object);

            var model = new AnimalColorModel()
                {
                    Id = animalColorId,
                    ClinicGroupId = clinicGroupId,
                    Value = "brown"
                };

            //act
            var result = (JsonResult)controller.Edit(model);

            //assert
            result.AssertActionRedirect();
            mockAnimalColorRepository.VerifyAll();
        }

        [TestMethod]
        public void value_is_required()
        {
            //arrange
            var mockAnimalColorRepository = new Mock<IAnimalColorRepository>();
            int clinicGroupId = 1;
            int animalColorId = 1;
            mockAnimalColorRepository.Setup(c => c.GetItem(It.IsAny<int>()))
                                     .Returns(() => new AnimalColor()
                                     {
                                         Id = 1,
                                         ClinicGroupId = clinicGroupId,
                                         Value = "Brown"
                                     });
            TestControllerBuilder builder = new TestControllerBuilder();
            var controller = builder.CreateController<AnimalColorsController>(mockAnimalColorRepository.Object);


            var model = new AnimalColorModel()
            {
                Id = animalColorId,
                ClinicGroupId = clinicGroupId,
                Value = " "
            };

            //act
            var result = (JsonResult)controller.Edit(model);

            //assert
            result.AssertViewRendered();
            mockAnimalColorRepository.VerifyAll();
        }
    }
}
