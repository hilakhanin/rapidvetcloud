﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.AnimalColor_tests
{
    //return view with data [all animal colors in clinicGroup]
    //accecible to administrator OR clinicGroupManager

    [TestClass]
    public class AnimalColor_Index_Tests
    {
        private ClinicGroup clinicGroup;

        public AnimalColor_Index_Tests()
        {
            var colorsList = new List<AnimalColor>()
                                 {
                                     new AnimalColor() {Id = 1, ClinicGroupId = 1, Value = "Brown"},
                                     new AnimalColor() {Id = 2, ClinicGroupId = 1, Value = "Black"},
                                     new AnimalColor() {Id = 3, ClinicGroupId = 1, Value = "Grey"},
                                     new AnimalColor() {Id = 4, ClinicGroupId = 1, Value = "tri-color"},
                                     new AnimalColor() {Id = 5, ClinicGroupId = 1, Value = "white"}
                                 };
            clinicGroup = new ClinicGroup()
                              {
                                  Id = 1,
                                  CreatedDate = DateTime.Now,
                                  CreatedById = 1,
                                  Name = DateTime.Now.Ticks.ToString(),
                                  AnimalColors = colorsList
                              };

        }

        [TestMethod]
        public void returns_edit_view_with_AnimalColor_data()
        {
            //arrange
            var mockAnimalColorRepository = new Mock<IAnimalColorRepository>();

            mockAnimalColorRepository.Setup(c => c.GetList(It.IsAny<int>()))
                                     .Returns(() => clinicGroup);
            var controller = new AnimalColorsController(mockAnimalColorRepository.Object);
            int clinicGroupId = 1;
       

            //act
            var result = (PartialViewResult)controller.Index(clinicGroupId, new RedirectData());

            //assert
            Assert.AreEqual("", result.ViewName);
            mockAnimalColorRepository.VerifyAll();

        }
    }
}
