﻿using System;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Repository.Finances;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.PriceList_Tests.Taariff
{
    // displays create tariff view
    //name field is required
    // adds new tariff to clinic
    [TestClass]
    public class Tariff_Create_Tests : ControllerTestBase
    {
        [TestMethod]
        public void returns_create_tariff_view()
        {
            //arrange
            int clinicId = 3;

            var mockPriceListRepository = new Mock<IPriceListRepository>();
            var mockCategoryRepository = new Mock<IPriceListCategoryRepository>();
            mockPriceListRepository.Setup(x => x.GetCreateTariffModel(It.IsAny<int>()))
                .Returns(() => new Tariff()
                                   {
                                       ClinicId = clinicId,
                                       TariffsItems = new Collection<PriceListItemTariff>()
                                   });

            var controller = new PriceListsController(mockPriceListRepository.Object, mockCategoryRepository.Object);

            //act
            var result = (ViewResult)controller.CreateTariff(clinicId);

            //assert
            Assert.AreEqual("", result.ViewName);
            mockPriceListRepository.VerifyAll();
        }

        [TestMethod]
        public void successfully_add_tariff_to_clinic()
        {
            //arrange
            var mockPriceListRepository = new Mock<IPriceListRepository>();
            var mockMembership = MembershipServiceNoClinicGroup;
            var builder = new TestControllerBuilder();

            Tariff tar = new Tariff()
                             {
                                 Name = "tarr 1",
                                 ClinicId = 9,
                                 TariffsItems = new Collection<PriceListItemTariff>()
                             };
           

            mockPriceListRepository.Setup(x => x.CreateTariff(It.IsAny<Tariff>()))
                .Returns(() => new OperationStatus() { Success = true });

            var controller = builder.CreateController<PriceListsController>(mockPriceListRepository.Object);
            controller.MembershipService = mockMembership.Object;

            //act
            var result = (RedirectToRouteResult) controller.CreateTariff(tar);


            //assert
            Assert.AreEqual("ClinicEdit", result.RouteValues["action"]);
            Assert.AreEqual("Clinics", result.RouteValues["controller"]);
            mockPriceListRepository.VerifyAll();
            mockMembership.VerifyAll();
        }

        [TestMethod]
        public void Name_field_is_required()
        {
            //arrange

            var mockPriceListCategoryRepository = new Mock<IPriceListCategoryRepository>();
            var mockPriceListRepository = new Mock<IPriceListRepository>();
            Tariff tar = new Tariff()
            {
                Name = "",
                ClinicId = 9,
                TariffsItems = new Collection<PriceListItemTariff>()
            };

            var controller = new PriceListsController(mockPriceListRepository.Object, mockPriceListCategoryRepository.Object);
            controller.ModelState.AddModelError("Name", "This field is required");

            //act
            var result = (ViewResult) controller.CreateTariff(tar);

            //assert
            Assert.IsNotNull(result.ViewData.ModelState["Name"]);
            Assert.AreEqual("", result.ViewName);
            mockPriceListRepository.VerifyAll();
        }
    }
}
