﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Repository.Finances;
using RapidVet.Model.Finance;
using RapidVet.Web.Controllers;
using RapidVet.Web.Tests.Controllers_Tests;


namespace RapidVet.Web.Tests.Controllers_Tests.PriceList_Tests.Taariff
{
   // return view with all tariffs in clinic

    [TestClass]
    public class Tariff_Index_Tests : ControllerTestBase
    {
        private List<Tariff> tarrs;
        private int clinicId = 1;

        public Tariff_Index_Tests()
        {
            tarrs = new List<Tariff>()
                {
                    new Tariff() {Id = 1, Name = "friends", ClinicId = clinicId},
                    new Tariff() {Id = 2, Name = "family", ClinicId = clinicId},
                    new Tariff() {Id = 3, Name = "mates", ClinicId = clinicId}
                };

        }
        [TestMethod]
        public void returns_view_with_all_tariffs_in_clinic()
        {
            //arrange
            var mockPriceListRepository = new Mock<IPriceListRepository>();
            var mockCategoryRepository = new Mock<IPriceListCategoryRepository>();

            mockPriceListRepository.Setup(x => x.GetTariffList(It.IsAny<int>()))
                .Returns(() => tarrs);

            var controller = new PriceListsController(mockPriceListRepository.Object,mockCategoryRepository.Object);


            //act
            var result = (ViewResult)controller.Tariffs(clinicId);

            //assert
            Assert.AreEqual("", result.ViewName);
            mockPriceListRepository.VerifyAll();
        }

    }
}
