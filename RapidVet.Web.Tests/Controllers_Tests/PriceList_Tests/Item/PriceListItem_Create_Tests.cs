﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Repository.Finances;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Finance;

namespace RapidVet.Web.Tests.Controllers_Tests.PriceList_Tests.Item
{
    // returns create PriceListItem View with PriceListCategory & Clinic Details
    // successfully saves new priceListItem
    // Name field is required

    [TestClass]
    public class PriceListItem_Create_Tests : ControllerTestBase
    {
        private PriceListCategory category;
        private PriceListItem item;

        public PriceListItem_Create_Tests()
        {
            item = new PriceListItem()
            {
                Id = 3,
                Name = "washing",
                CreatedById = 1,
                CreatedDate = DateTime.Now,
                CategoryId = 1
            };

            category = new PriceListCategory()
                           {
                               Id = 1,
                               Name = "ear",
                               ClinicId = 2,
                               PriceListItems = new List<PriceListItem>()
                                                    {
                                                        new PriceListItem(){Id = 1, Name = "cleaning",CreatedById = 1, CreatedDate = DateTime.Now},
                                                        new PriceListItem(){Id = 2, Name = "waxing",CreatedById = 1, CreatedDate = DateTime.Now},
                                                        item
                                                    }
                           };

        }

        [TestMethod]
        public void returns_create_view_with_clinic_and_category_details()
        {
            //arrange
            var mockPriceListCategory = new Mock<IPriceListCategoryRepository>();
            var mockPriceListRepository = new Mock<IPriceListRepository>();
            var controller = new PriceListsController(mockPriceListRepository.Object, mockPriceListCategory.Object);

            //assert
            var result = (ViewResult)controller.CreateItem(category.Id);

            //act
            Assert.AreEqual("", result.ViewName);
            mockPriceListCategory.VerifyAll();
        }

        [TestMethod]
        public void successfully_save_priceListItem()
        {
            //arrange
            var mockPriceListCategoryRepository = new Mock<IPriceListCategoryRepository>();
            var mockPriceListRepository = new Mock<IPriceListRepository>();
            var membershipMoq = MembershipServiceNoClinicGroup;
            var controller = new PriceListsController(mockPriceListRepository.Object, mockPriceListCategoryRepository.Object);
            controller.MembershipService = membershipMoq.Object;

            mockPriceListRepository.Setup(x => x.CreatePriceListItem(It.IsAny<PriceListItem>()))
                .Returns(() => new OperationStatus() { Success = true });

            //act
            var result = (RedirectToRouteResult)controller.CreateItem(item);

            //assert
            Assert.AreEqual("Items", result.RouteValues["action"]);
            Assert.AreEqual(category.Id, result.RouteValues["id"]);
            mockPriceListRepository.VerifyAll();
            membershipMoq.VerifyAll();
        }

        [TestMethod]
        public void name_field_is_required()
        {
            //arrange
            var mockPriceListCategory = new Mock<IPriceListCategoryRepository>();
            var controller = new PriceListCategoriesController(mockPriceListCategory.Object);
            controller.ModelState.AddModelError("Name", "This field is required");
            var model = new PriceListCategoryModel()
            {
                Name = " ",
                ClinicId = 1
            };
            //act
            var result = (JsonResult)controller.Create(model);

            //assert
        Assert.AreEqual("false",result.Data);
            mockPriceListCategory.VerifyAll();
        }

    }
}
