﻿using System;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model.Finance;
using RapidVet.Repository.Finances;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.PriceList_Tests.Item
{
    [TestClass]
    public class PriceListItem_Index_Tests
    {
        private PriceListCategory category;

        public PriceListItem_Index_Tests()
        {
            category = new PriceListCategory()
                           {
                               Id = 1,
                               ClinicId = 1,
                               Name = "ears",
                               PriceListItems = new Collection<PriceListItem>()
                                                    {
                                                        new PriceListItem() {Id = 1, Name = "cleaning"},
                                                        new PriceListItem() {Id = 2, Name = "waxing"},
                                                        new PriceListItem() {Id = 3, Name = "washing"}
                                                    }
                           };
        }

    }
}
