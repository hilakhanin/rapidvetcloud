﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Repository.Finances;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.PriceList_Tests.Item
{
    // returns view with  PriceListsItem details
    // successfully remove PriceListItem from PriceListCategory

    [TestClass]
    public class PriceListItem_Remove_Tests
    {
        private PriceListCategory category;
        private PriceListItem item;

        public PriceListItem_Remove_Tests()
        {
            item = new PriceListItem()
           {
               Id = 3,
               Name = "washing",
               CreatedById = 1,
               CreatedDate = DateTime.Now,
               CategoryId = 1
           };

            category = new PriceListCategory()
                           {
                               Id = 1,
                               Name = "ear",
                               ClinicId = 2,
                               PriceListItems = new List<PriceListItem>()
                                                    {
                                                        new PriceListItem(){Id = 1, Name = "cleaning",CreatedById = 1, CreatedDate = DateTime.Now},
                                                        new PriceListItem(){Id = 2, Name = "waxing",CreatedById = 1, CreatedDate = DateTime.Now},
                                                        item
                                                    }
                           };

        }

        [TestMethod]
        public void returns_view_with_PriceListItem_details()
        {
            //assert
          
            var mockPriceListCategory = new Mock<IPriceListCategoryRepository>();
            var mockPriceListRepository = new Mock<IPriceListRepository>();
            mockPriceListRepository.Setup(x => x.GetPriceListItem(It.IsAny<int>()))
           .Returns(item);
            var controller = new PriceListsController(mockPriceListRepository.Object, mockPriceListCategory.Object);

            //act
            var result = (ViewResult) controller.ItemDetails(item.Id);

            //assert
            Assert.AreEqual("", result.ViewName);
            mockPriceListCategory.VerifyAll();
        }

        [TestMethod]
        public void successfully_remove_PriceListItem_from_PriceListCategory()
        {
            //arrange
            var mockPriceListCategory = new Mock<IPriceListRepository>();
            TestControllerBuilder builder = new TestControllerBuilder();
            var controller = builder.CreateController<PriceListsController>(mockPriceListCategory.Object);

            mockPriceListCategory.Setup(x => x.RemoveItemFromCategory(It.IsAny<int>())).
            Returns(() => new OperationStatus() { Success = true, ItemId = category.Id });

            //act
            var result = (RedirectToRouteResult)controller.RemoveItem(item.Id , category.Id );

            //assert
            Assert.AreEqual("ItemDetails", result.RouteValues["action"]);
            Assert.AreEqual(category.Id, result.RouteValues["id"]);
            mockPriceListCategory.VerifyAll();
        }
    }
}

