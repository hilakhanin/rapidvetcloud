﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Repository.Finances;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.PriceList_Tests.Item
{
    // returns edit view with priceListItem details
    // successfully save PriceListItem
    // Name field is required

    [TestClass]
    public class PriceListItem_Edit_Tests : ControllerTestBase
    {
        private PriceListCategory category;
        private PriceListItem item;

        public PriceListItem_Edit_Tests()
        {
            item = new PriceListItem()
            {
                Id = 3,
                Name = "washing",
                CreatedById = 1,
                CreatedDate = DateTime.Now,
                CategoryId = 1
            };

            category = new PriceListCategory()
                           {
                               Id = 1,
                               Name = "ear",
                               ClinicId = 2,
                               PriceListItems = new List<PriceListItem>()
                                                    {
                                                        item,
                                                        new PriceListItem(){Id = 1, Name = "cleaning",CreatedById = 1, CreatedDate = DateTime.Now},
                                                        new PriceListItem(){Id = 2, Name = "waxing",CreatedById = 1, CreatedDate = DateTime.Now}
                                                    }
                           };

        }

        [TestMethod]
        public void returns_View_With_priceListItem_details()
        {
            //arrange
            var mockPriceListCategoryRepository = new Mock<IPriceListCategoryRepository>();
            var mockPriceListRepository = new Mock<IPriceListRepository>();
            mockPriceListRepository.Setup(x => x.GetPriceListItem(It.IsAny<int>())).Returns(item);
            var controller = new PriceListsController(mockPriceListRepository.Object, mockPriceListCategoryRepository.Object);


            //assert
            var result = (ViewResult)controller.EditItem(item.Id);

            //act
            Assert.AreEqual("", result.ViewName);
            result.AssertViewRendered();
            mockPriceListCategoryRepository.VerifyAll();
        }

        [TestMethod]
        public void successfully_save_edited_PriceListItem_View()
        {
            //arrange
            var mockPriceListCategory = new Mock<IPriceListRepository>();


            mockPriceListCategory.Setup(x => x.UpdatePriceListItem(It.IsAny<PriceListItem>()))
                .Returns(() => new OperationStatus() { Success = true });

            mockPriceListCategory.Setup(x => x.GetPriceListItem(item.Id)).Returns(item);

            TestControllerBuilder builder = new TestControllerBuilder();
            var controller = builder.CreateController<PriceListsController>(mockPriceListCategory.Object);

            var membershipMoq = MembershipServiceNoClinicGroup;
            controller.MembershipService = membershipMoq.Object;


            var formCollection = new FormCollection();
            formCollection.Add("Name", "washing123");
            formCollection.Add("CostPrice", "320");
            formCollection.Add("Id", item.Id.ToString());

            //act
            var result = (RedirectToRouteResult)controller.EditItem(formCollection, item.Id);

            //assert
            Assert.AreEqual("Items", result.RouteValues["action"]);
            Assert.AreEqual(category.Id, result.RouteValues["id"]);
            membershipMoq.VerifyAll();
            mockPriceListCategory.VerifyAll();

        }

        [TestMethod]
        public void name_field_is_required()
        {
            var mockPriceListCategory = new Mock<IPriceListRepository>();


            mockPriceListCategory.Setup(x => x.UpdatePriceListItem(It.IsAny<PriceListItem>()))
                .Returns(() => new OperationStatus() { Success = true });

            mockPriceListCategory.Setup(x => x.GetPriceListItem(item.Id)).Returns(item);

            TestControllerBuilder builder = new TestControllerBuilder();
            var controller = builder.CreateController<PriceListsController>(mockPriceListCategory.Object);

            var formCollection = new FormCollection();
            formCollection.Add("Name", "");
            formCollection.Add("CostPrice", "320");
            formCollection.Add("Id", item.Id.ToString());

            //act
            var result = (ViewResult)controller.EditItem(formCollection, item.Id);

            //assert
            Assert.IsTrue(result.ViewData.ModelState.ContainsKey("Name"));
        }
    }
}
