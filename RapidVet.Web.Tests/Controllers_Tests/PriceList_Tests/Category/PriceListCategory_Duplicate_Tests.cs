﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Repository.Finances;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.PriceList_Tests.Category
{
    // displays all PriceListCategory & PriceListItem in Clinic
    // copies all PriceListCategory & PriceListItem to other Clinic in group 
    [TestClass]
    public class PriceListCategory_Duplicate_Tests : ControllerTestBase
    {
        private PriceListCategory category;
        private PriceListItem item;

        public PriceListCategory_Duplicate_Tests()
        {
            item = new PriceListItem()
            {
                Id = 3,
                Name = "washing",
                CreatedById = 1,
                CreatedDate = DateTime.Now,
                CategoryId = 1
            };

            category = new PriceListCategory()
            {
                Id = 1,
                Name = "ear",
                ClinicId = 2,
                PriceListItems = new List<PriceListItem>()
                                                    {
                                                        new PriceListItem(){Id = 1, Name = "cleaning",CreatedById = 1, CreatedDate = DateTime.Now},
                                                        new PriceListItem(){Id = 2, Name = "waxing",CreatedById = 1, CreatedDate = DateTime.Now},
                                                        item
                                                    }
            };

        }

        [TestMethod]
        public void returns_view_with_all_PriceListCategory_and_PriceListItems()
        {
            //arrange
            var mockPriceListCategory = new Mock<IPriceListCategoryRepository>();
            var controller = new PriceListCategoriesController(mockPriceListCategory.Object);

            mockPriceListCategory.Setup(x => x.GetItem(category.Id))
                .Returns(() => category);

            //act
            var result = (PartialViewResult)controller.Delete(category.Id,"","");

            //assert
            Assert.AreEqual("", result.ViewName);
            result.AssertViewRendered();
            mockPriceListCategory.VerifyAll();
            
        }

        [TestMethod]
        public  void successfully_copy_PriceListCtaegories_and_PriceListItems_to_another_clinic_in_group()
        {
            //arrange
            var mockPriceListCategoryRepository = new Mock<IPriceListCategoryRepository>();
            var mockPriceListRepository = new Mock<IPriceListRepository>();

            mockPriceListRepository.Setup(x => x.DuplicatePrices(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(() => new OperationStatus(){Success = true});


            var controller = new PriceListsController(mockPriceListRepository.Object, mockPriceListCategoryRepository.Object);
            var membershipMoq = MembershipServiceNoClinicGroup;
            controller.MembershipService = membershipMoq.Object;
            int targetClinicId = 9;

            //act
            var result = (RedirectToRouteResult) controller.DuplicatePrices(category.ClinicId , targetClinicId);

            //assert
            Assert.AreEqual("Categories", result.RouteValues["action"]);
            Assert.AreEqual(category.ClinicId, result.RouteValues["id"]);
            mockPriceListRepository.VerifyAll();
            membershipMoq.VerifyAll();


        }


    }
}
