﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Repository.Finances;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Finance;

namespace RapidVet.Web.Tests.Controllers_Tests.PriceList_Tests.Category
{
    // show view of PriceListCategory and all child PriceListItem
    // successfully remove PriceListCategory and all child PriceListItem from Clinic

    [TestClass]
    public class PriceListCategory_Remove_Tests
    {
         private PriceListCategory category;

        public PriceListCategory_Remove_Tests()
        {
            category = new PriceListCategory()
                           {
                               Id = 1,
                               Name = "ear",
                               ClinicId = 2
                           };
        }

        

        [TestMethod]
        public void successfully_remove_PriceListCategory_and_children_from_clinic()
        {
            //arrange
            var mockPriceListCategory = new Mock<IPriceListCategoryRepository>();
            TestControllerBuilder builder = new TestControllerBuilder();
            var controller = builder.CreateController<PriceListCategoriesController>(mockPriceListCategory.Object);

            mockPriceListCategory.Setup(x => x.RemoveCategoryFromClinic(It.IsAny<int>())).Returns(() => new OperationStatus(){Success = true, ItemId = category.ClinicId});

            var model = new PriceListCategoryModel()
                {
                    Name = "name",
                    ClinicId = 1,
                    Id = 1
                };
            //act
            var result = (JsonResult) controller.Delete(model);

            //assert
            mockPriceListCategory.VerifyAll();
        }

    }
}
