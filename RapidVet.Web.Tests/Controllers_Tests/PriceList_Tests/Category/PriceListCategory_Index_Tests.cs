﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Repository.Finances;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.PriceList_Tests.Category
{
    //returns list of all priceList Category for a Clinic

    [TestClass]
    public class PriceListCategory_Index_Tests
    {
        private IQueryable<PriceListCategory> categories;

        public PriceListCategory_Index_Tests()
        {
            categories = new List<PriceListCategory>()
                             {
                                   new PriceListCategory(){Id = 1 , Name = "ears",ClinicId = 1},
                                                        new PriceListCategory(){Id = 2 , Name = "throat",ClinicId = 1},
                                                        new PriceListCategory(){Id = 3 , Name = "nose",ClinicId = 1}
                             }.AsQueryable();
        }

       
    }
}
