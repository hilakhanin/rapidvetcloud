﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Repository.Finances;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Finance;

namespace RapidVet.Web.Tests.Controllers_Tests.PriceList_Tests.Category
{
    //creares new PriceListCategorys|Category in Clinic
    //name field is required

    [TestClass]
    public class PriceListCategory_Create_Tests : ControllerTestBase
    {
        private PriceListCategory category;

        public PriceListCategory_Create_Tests()
        {
            category = new PriceListCategory()
                           {
                               Id = 1,
                               Name = "ear",
                               ClinicId = 2
                           };
        }

        [TestMethod]
        public void returns_blank_create_priceListCategory_view_with_clinic_details()
        {
            //arrange
            var mockPriceListCategory = new Mock<IPriceListCategoryRepository>();
            var controller = new PriceListCategoriesController(mockPriceListCategory.Object);
            int clinicId = 1;

            //act
            var result = (PartialViewResult)controller.Create(clinicId,"","");

            //assert
            Assert.AreEqual("", result.ViewName);
            mockPriceListCategory.VerifyAll();
        }


        [TestMethod]
        public void successfully_add_priceListCategory_to_clinic()
        {
            //arrange
            var mockPriceListCategory = new Mock<IPriceListCategoryRepository>();
            var controller = new PriceListCategoriesController(mockPriceListCategory.Object);
            mockPriceListCategory.Setup(x => x.Create(It.IsAny<PriceListCategory>()))
                .Returns(() => new OperationStatus() { Success = true, ItemId = category.Id });
            var model = new PriceListCategoryModel()
            {
                Name = "מקורבים " + DateTime.Now.Ticks.ToString(),
                ClinicId = 1
            };
            //act
            var result = (JsonResult)controller.Create(model);

            //assert
            mockPriceListCategory.VerifyAll();
        }

        [TestMethod]
        public void name_field_is_required()
        {
            //arrange
            var mockPriceListCategory = new Mock<IPriceListCategoryRepository>();
            var controller = new PriceListCategoriesController(mockPriceListCategory.Object);
            controller.ModelState.AddModelError("Name", "Name Field Is Required");
          
            var model = new PriceListCategoryModel()
                {
                    Name = "",
                    ClinicId = 1
                };
            //act
            var result = (JsonResult)controller.Create(model);

            //assert
            mockPriceListCategory.VerifyAll();

        }


    }
}
