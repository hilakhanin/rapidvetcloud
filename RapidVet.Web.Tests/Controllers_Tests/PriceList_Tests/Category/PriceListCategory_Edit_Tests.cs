﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Repository.Finances;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Finance;

namespace RapidVet.Web.Tests.Controllers_Tests.PriceList_Tests.Category
{
    // displays edit view with PriceListCategory details
    // successfully saves edited PriceListCategory item
    // Name field is required

     [TestClass]
    public class PriceListCategory_Edit_Tests : ControllerTestBase
    {
         private PriceListCategory category;

        public PriceListCategory_Edit_Tests()
        {
            category = new PriceListCategory()
                           {
                               Id = 1,
                               Name = "ear",
                               ClinicId = 2
                           };
        }

        


    }
}
