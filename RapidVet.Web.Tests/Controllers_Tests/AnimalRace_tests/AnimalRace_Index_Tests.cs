﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model.Patients;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.AnimalRace_tests
{
    [TestClass]
    public class AnimalRace_Index_Tests
    {
        private Collection<AnimalRace> animalRaceCollection;


        public AnimalRace_Index_Tests()
        {
            animalRaceCollection = new Collection<AnimalRace>()
                                                              {
                                     new AnimalRace() {Id = 1 , Value = "Labrador Retriever" , AnimalKindId  = 1},
                                     new AnimalRace() {Id = 1 , Value = "German Shepherd" , AnimalKindId  = 1},
                                     new AnimalRace() {Id = 1 , Value = "Boxer" , AnimalKindId  = 1}
                                                              };

        }

        [TestMethod]
        public void returns_Index_view_from_animalKind_data()
        {
            //arrange
            var mockAnimalRaceRepository = new Mock<RapidVet.Repository.Patients.IAnimalRaceRepository>();
            mockAnimalRaceRepository.Setup(r => r.GetList(It.IsAny<int>())).Returns(() => new AnimalKind()
                                                                                              {
                                                                                                  Id = 1,
                                                                                                  ClinicGroupId = 1,
                                                                                                  Value = "Dog",
                                                                                                  AnimalRaces = animalRaceCollection
                                                                                              });

            var animalKindController = new AnimalRacesController(mockAnimalRaceRepository.Object);
            int animalKindId = 1;

            //act
            var view = (ViewResult)animalKindController.Index(animalKindId);

            //assert
            Assert.AreEqual("", view.ViewName);
            mockAnimalRaceRepository.VerifyAll();
        }
    }
}
