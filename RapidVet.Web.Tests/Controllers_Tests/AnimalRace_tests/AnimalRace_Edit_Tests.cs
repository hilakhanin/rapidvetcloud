﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Tests.Controllers_Tests.AnimalRace_tests
{
    //returns AnimalRaceEdit view with animal kind data
    //value is required
    //administrator OR clinicGroupManager

    [TestClass]
    public class AnimalRace_Edit_Tests
    {
        [TestMethod]
        public void returns_edit_view_with_AnimalRace_data()
        {
            //arrange
            var mockAnimalRaceRepository = new Mock<IAnimalRaceRepository>();
            int animalKindId = 1;

            AnimalRace animalRace = new AnimalRace()
                                                 {
                                                     Id = 1,
                                                     AnimalKindId = animalKindId
                                                 };
            int clinicGroupId = 1;

            mockAnimalRaceRepository.Setup(r => r.GetItem(It.IsAny<int>())).Returns(animalRace);
            var controller = new AnimalRacesController(mockAnimalRaceRepository.Object);

            //act
            var result = (PartialViewResult) controller.Edit(animalRace.Id,clinicGroupId);

            //assert
            Assert.AreEqual("", result.ViewName);
            result.AssertViewRendered();
            mockAnimalRaceRepository.VerifyAll();
        }


        [TestMethod]
        public void successfully_save_animalRace()
        {
            var mockAnimalRaceRepository = new Mock<IAnimalRaceRepository>();
            int animalKindId = 1;

            mockAnimalRaceRepository.Setup(k => k.GetItem(It.IsAny<int>()))
                                                 .Returns(() => new AnimalRace()
                                                 {
                                                     Id = 1,
                                                     AnimalKindId = animalKindId,
                                                     Value = "Golder Retriver"
                                                 });

            mockAnimalRaceRepository.Setup(r => r.Update(It.IsAny<AnimalRace>()))
                                    .Returns(() => new OperationStatus()
                                                       {
                                                        Success   = true
                                                       });
         
            TestControllerBuilder builder = new TestControllerBuilder();
            var controller = builder.CreateController<AnimalRacesController>(mockAnimalRaceRepository.Object);

            var animalRaceModel = new AnimalRaceModel()
                {
                    Id = 1,
                    AnimalKindId = animalKindId,
                    Value = "Bedlington Terrier"
                };

            //act
            var result = (JsonResult) controller.Edit(animalRaceModel);

            //assert
            result.AssertActionRedirect();
            var wrapper = new System.Web.Routing.RouteValueDictionary(result.Data); 
            Assert.AreEqual(wrapper["success"],"true");
            mockAnimalRaceRepository.VerifyAll();


        }

        [TestMethod]
        public void value_is_required()
        {
            var mockAnimalRaceRepository = new Mock<IAnimalRaceRepository>();
            int animalKindId = 1;

            mockAnimalRaceRepository.Setup(k => k.GetItem(It.IsAny<int>()))
                                                 .Returns(() => new AnimalRace()
                                                 {
                                                     Id = 1,
                                                     AnimalKindId = animalKindId,
                                                     Value = "Golder Retriver"
                                                 });

            TestControllerBuilder builder = new TestControllerBuilder();
            var controller = builder.CreateController<AnimalRacesController>(mockAnimalRaceRepository.Object);

            var animalRaceModel = new AnimalRaceModel()
                {
                    Id = 1,
                    AnimalKindId = animalKindId,
                    Value = ""
                };

            //act
            var result = (JsonResult)controller.Edit(animalRaceModel);

            //assert
            result.AssertViewRendered();
            var wrapper = new System.Web.Routing.RouteValueDictionary(result.Data);
            Assert.AreEqual(wrapper["success"],"false");

            mockAnimalRaceRepository.VerifyAll();
            
        }
    }
}
