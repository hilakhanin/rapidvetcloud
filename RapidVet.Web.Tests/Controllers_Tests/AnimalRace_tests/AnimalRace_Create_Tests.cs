﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Patients;
using Rhino.Mocks.Constraints;

namespace RapidVet.Web.Tests.Controllers_Tests.AnimalRace_tests
{
    //returns a blank AnimalRaceCreateModel view with animalKind data
    //saves animalRace
    //value is required
    //administrator OR clinicGroupManager

    [TestClass]
    public class AnimalRace_Create_Tests
    {

        [TestMethod]
        public void returns_blank_AnimalRaceCreate_view_with_animalKind_data()
        {
            //arrange
            var mockAnimalRaceRepository = new Mock<IAnimalRaceRepository>();
            int animalKindId = 1;
            int clinicGroupId = 1;

            mockAnimalRaceRepository.Setup(r => r.GetCreateModel(It.IsAny<int>()))
                                    .Returns(new AnimalRace()
                                                 {
                                                     Id = 1,
                                                     AnimalKindId = animalKindId
                                                 });

            var controller = new AnimalRacesController(mockAnimalRaceRepository.Object);

            //act
            var resule = (PartialViewResult)controller.Create(animalKindId,clinicGroupId);

            //assert
            Assert.AreEqual("", resule.ViewName);
            mockAnimalRaceRepository.VerifyAll();

        }

        [TestMethod]
        public void successfully_save_AnimalRace()
        {
            //arrange
            var mockAnimalRaceRepository = new Mock<IAnimalRaceRepository>();
            int animalKindId = 1;

            var animalRace = new AnimalRaceModel()
                                        {
                                            AnimalKindId = animalKindId,
                                            Value = "Labrador Retriver"
                                        };

            mockAnimalRaceRepository.Setup(r => r.Create(It.IsAny<AnimalRace>()))
                                    .Returns(new OperationStatus()
                                    {
                                        Success = true
                                    });

            var controller = new AnimalRacesController(mockAnimalRaceRepository.Object);

            //act
            var result = (JsonResult)controller.Create(animalRace);

            //assert
            var wrapper = new System.Web.Routing.RouteValueDictionary(result.Data);
            Assert.AreEqual(wrapper["success"], "true");
            mockAnimalRaceRepository.VerifyAll();
        }

        [TestMethod]
        public void value_is_required()
        {
            //arrange
            var mockAnimalRaceRepository = new Mock<IAnimalRaceRepository>();
            int animalKindId = 1;

            var animalRace = new AnimalRaceModel()
            {
                AnimalKindId = animalKindId,
                Value = ""
            };

            var controller = new AnimalRacesController(mockAnimalRaceRepository.Object);
            controller.ModelState.AddModelError("Value", "This field is required");

            //act
            var result = (JsonResult)controller.Create(animalRace);

            //assert
            result.AssertViewRendered();
              var wrapper = new System.Web.Routing.RouteValueDictionary(result.Data);
            Assert.AreEqual(wrapper["success"],"false");
            mockAnimalRaceRepository.VerifyAll();
        }


    }
}

