﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Tests.Controllers_Tests.AnimalRace_tests
{
    [TestClass]
    public class AnimalRace_Delete_Tests
    {
        //returns view with data animalRace item selected for deletion
        //successfully deletes item

        [TestMethod]
        public void returns_view_with_animalRace_item_data()
        {
            //assert
            int animalKindId = 1;
            int animalRaceId = 1;
            var mockAnimalRaceRepository = new Mock<IAnimalRaceRepository>();
            mockAnimalRaceRepository.Setup(r => r.GetItem(It.IsAny<int>()))
                .Returns(new AnimalRace()
                             {
                                 Id = 1,
                                 AnimalKindId = animalKindId,
                                 Value = "labrador"
                             });
            int clinicGroupId = 1;


            var controller = new AnimalRacesController(mockAnimalRaceRepository.Object);

            //act
            var result = (PartialViewResult)controller.Delete(animalRaceId, clinicGroupId);

            //assert
            Assert.AreEqual("", result.ViewName);
            mockAnimalRaceRepository.VerifyAll();

        }

        [TestMethod]
        public void successfully_delete_animalRace_item()
        {
            //assert
            int animalRaceId = 1;
            int animalKindId = 1;
            var mockAnimalRaceRepository = new Mock<IAnimalRaceRepository>();
            AnimalRace animalRace = new AnimalRace()
            {
                Id = animalRaceId,
                AnimalKindId = animalKindId,
                Value = "labrador"
            };

            mockAnimalRaceRepository.Setup(x => x.GetItem(animalRace.Id))
                .Returns(() => animalRace);

            mockAnimalRaceRepository.Setup(r => r.Delete(It.IsAny<AnimalRace>()))
                .Returns(() => new OperationStatus()
                                        {
                                            Success = true,
                                            ItemId = animalKindId
                                        });

            var controller = new AnimalRacesController(mockAnimalRaceRepository.Object);
            var animalRaceModel = new AnimalRaceModel()
                {
                    Id = 1,
                    AnimalKindId = 1,
                    Value = "labrador"
                };

         
            //act
            var result = (JsonResult)controller.Delete(animalRaceModel);

            //assert
            var wrapper = new System.Web.Routing.RouteValueDictionary(result.Data); 
            Assert.AreEqual(wrapper["success"],true);
            mockAnimalRaceRepository.VerifyAll();
        }
    }
}

