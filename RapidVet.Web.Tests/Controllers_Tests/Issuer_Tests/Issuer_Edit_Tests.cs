﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.ActionResults;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Clinics;

namespace RapidVet.Web.Tests.Controllers_Tests.Issuer_Tests
{
    //returns view with issuer details
    //successfully update existing issuer
    // Name is required
    // CompanyId is required
    [TestClass]
    public class Issuer_Edit_Tests:ControllerTestBase
    {
        [TestMethod]
        public void returns_view_with_issuer_details()
        {
            //arrange
            var mockIssuerRepository = new Mock<IIssuerRepository>();
            int issuerId = 1;
            mockIssuerRepository.Setup(i => i.GetItem(It.IsAny<int>())).Returns(() => new Issuer()
                                                                                          {
                                                                                              Id = issuerId,
                                                                                              ClinicId = 1,
                                                                                               Name = "issuer1",
                                                                                               CompanyId = "company1",
                                                                                               CreatedById = 2,
                                                                                               CreatedDate = DateTime.Now
                                                                                          });
            var controller = new IssuersController(mockIssuerRepository.Object);

            //act
            var result = (PartialViewResult)controller.Edit(issuerId);

            //assert
            result.AssertViewRendered();
            Assert.IsNotNull(result.Model);
            mockIssuerRepository.VerifyAll();
        }

        [TestMethod]
        public void successfully_save()
        {
            //arrange
            var mockIssuerRepository = new Mock<IIssuerRepository>();
            int clinicId = 1;
            mockIssuerRepository.Setup(m => m.GetItem(It.IsAny<int>()))
                                .Returns(() => new Issuer()
                                {
                                    ClinicId = clinicId,
                                    Name = "Issuer1",
                                    CompanyId = "company1",
                                    CreatedById = 1,
                                    CreatedDate = DateTime.Now
                                });

            mockIssuerRepository.Setup(m => m.Update(It.IsAny<Issuer>()))
                .Returns(() => new OperationStatus()
                                   {
                                       Success = true
                                   });

            TestControllerBuilder builder = new TestControllerBuilder();
            var formCollection = new FormCollection();
            var issuer = new IssuerCreate()
                {
                    Name = "issuerName" + DateTime.Now.Ticks.ToString(),
                    ClinicId = 1,
                    Id = 1
                };
      

            var controller = builder.CreateController<IssuersController>(mockIssuerRepository.Object);
            var membershipMoq = MembershipServiceNoClinicGroup;

            controller.MembershipService = membershipMoq.Object;
          

            //act
            var result = (JsonResult)controller.Edit(issuer);

            //assert
            result.AssertActionRedirect();
            membershipMoq.VerifyAll();
            mockIssuerRepository.VerifyAll();
        }

        [TestMethod]
        public void name_is_required()
        {
            //arrange
            var mockIssuerRepository = new Mock<IIssuerRepository>();
            int issuerId = 1;
            mockIssuerRepository.Setup(m => m.GetItem(It.IsAny<int>()))
                                .Returns(() => new Issuer()
                                {
                                    Id = issuerId,
                                    ClinicId = 1,
                                    Name = "Issuer1",
                                    CompanyId = "company1",
                                    CreatedById = 1,
                                    CreatedDate = DateTime.Now
                                });
            TestControllerBuilder builder = new TestControllerBuilder();
            var controller = builder.CreateController<IssuersController>(mockIssuerRepository.Object);

            var issuer = new IssuerCreate()
                {
                    Name = "",
                    CompanyId = "CompanyId1",
                    Id = 1
                };

            //act
            var result = (JsonResult)controller.Edit(issuer);

            //assert
            mockIssuerRepository.VerifyAll();
        }

        [TestMethod]
        public void companyId_is_required()
        {
            //arrange
            var mockIssuerRepository = new Mock<IIssuerRepository>();
            int issuerId = 1;
            mockIssuerRepository.Setup(m => m.GetItem(It.IsAny<int>()))
                                .Returns(() => new Issuer()
                                {
                                    Id = issuerId,
                                    ClinicId = 1,
                                    Name = "Issuer1",
                                    CompanyId = "company1",
                                    CreatedById = 1,
                                    CreatedDate = DateTime.Now
                                });
            TestControllerBuilder builder = new TestControllerBuilder();
            var controller = builder.CreateController<IssuersController>(mockIssuerRepository.Object);
            var issuer = new IssuerCreate()
                {
                    Id = 1,
                    CompanyId = "",
                    Name = "IssuerNAme1"
                };

            //act
            var result = (JsonResult)controller.Edit(issuer);

            //assert
            mockIssuerRepository.VerifyAll();
        }
    }
}
