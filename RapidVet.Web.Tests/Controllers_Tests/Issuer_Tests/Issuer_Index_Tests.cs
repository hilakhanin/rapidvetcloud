﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model.Clinics;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.Issuer_Tests
{
    //return view with all issuers in Clinic
    [TestClass]
    public class Issuer_Index_Tests
    {
        [TestMethod]
        public void returns_view_with_all_issuers_in_clinic()
        {
            //arrange
            var mockIssuerRepository = new Mock<IIssuerRepository>();
            mockIssuerRepository.Setup(i => i.GetList(It.IsAny<int>()))
                .Returns(() => new Clinic()
                                   {
                                       Id = 1,
                                       ClinicGroupID = 1,
                                       Issuers =
                                           new Collection<Issuer>
                                           ()
                                               {
                                                   new Issuer()
                                                       {
                                                           Id = 1,
                                                           Name =
                                                               "issuer1",
                                                           ClinicId
                                                               = 1,
                                                           CompanyId
                                                               =
                                                               "comp1"
                                                       },
                                                   new Issuer()
                                                       {
                                                           Id = 2,
                                                           Name =
                                                               "issuer2",
                                                           ClinicId
                                                               = 1,
                                                           CompanyId
                                                               =
                                                               "comp1"
                                                       }
                                               }
                                   });

            var controller = new IssuersController(mockIssuerRepository.Object);
            int clinicId = 1;

            //act
            var result = (PartialViewResult)controller.Index(clinicId,null);

            //assert
            Assert.AreEqual("", result.ViewName);
            mockIssuerRepository.VerifyAll();


        }
    }
}
