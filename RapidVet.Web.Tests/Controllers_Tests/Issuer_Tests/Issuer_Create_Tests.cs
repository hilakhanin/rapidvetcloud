﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Clinics;

namespace RapidVet.Web.Tests.Controllers_Tests.Issuer_Tests
{
    // returns blank create issuer view with Clinic details
    //successfully add new issuer
    // Name is required
    // CompanyId is required
    [TestClass]
    public class Issuer_Create_Tests: ControllerTestBase
    {
        [TestMethod]
        public void returns_blank_create_view_with_clinic_details()
        {
            //arrange
            int clinicId = 1;
            var mockIssuerRepository = new Mock<IIssuerRepository>();
            mockIssuerRepository.Setup(i => i.GetCreateModel(It.IsAny<int>())).Returns(() => new Issuer()
                                                                                                 {
                                                                                                     ClinicId = clinicId,
                                                                                                 });
            var controller = new IssuersController(mockIssuerRepository.Object);

            //act
            var result = (PartialViewResult) controller.Create(clinicId);

            //assert
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.ViewData.Model);
            mockIssuerRepository.VerifyAll();
        }

        [TestMethod]
        public void successfully_save_new_issuer()
        {
            //arrange
            var mockIssuerRepository = new Mock<IIssuerRepository>();
            mockIssuerRepository.Setup(i => i.Create(It.IsAny<Issuer>())).Returns(() => new OperationStatus()
                                                                                            {
                                                                                                Success = true
                                                                                            });
            var controller = new IssuersController(mockIssuerRepository.Object);

            var membershipMoq = MembershipServiceNoClinicGroup;

            controller.MembershipService = membershipMoq.Object;

            int clinicId = 1;

            IssuerCreate issuer = new IssuerCreate()
                                {
                                    ClinicId = clinicId,
                                    Name = "Issuer1",
                                    CompanyId = "company1",
                                };

            //act
            var result = (JsonResult) controller.Create(issuer);

            //assert
            membershipMoq.VerifyAll();
            mockIssuerRepository.VerifyAll();
        }

        [TestMethod]
        public void name_is_required()
        {
            //arrange
            var mockIssuerRepository = new Mock<IIssuerRepository>();
            var controller = new IssuersController(mockIssuerRepository.Object);
            int clinicId = 1;

            IssuerCreate issuer = new IssuerCreate()
            {
                ClinicId = clinicId,
                Name = "",
                CompanyId = "company1",
            };

            controller.ModelState.AddModelError("Name", "Required");

            //act
            var result = (JsonResult) controller.Create(issuer);

            //assert
            result.AssertViewRendered();
            mockIssuerRepository.VerifyAll();

        }

        [TestMethod]
        public void companyId_is_required()
        {
            //arrange
            var mockIssuerRepository = new Mock<IIssuerRepository>();
            var controller = new IssuersController(mockIssuerRepository.Object);
            int clinicId = 1;
            Issuer issuer = new Issuer()
            {
                ClinicId = clinicId,
                Name = "Issuer1",
                CompanyId = "",
                CreatedById = 1,
                CreatedDate = DateTime.Now
            };

            controller.ModelState.AddModelError("CompanyId", "Required");

            //act
            var result = (PartialViewResult) controller.Create(clinicId);

            //assert
            result.AssertViewRendered();
            Assert.IsFalse(result.ViewData.ModelState.IsValid);
            Assert.IsFalse(result.ViewData.ModelState.IsValidField("CompanyId"));
            mockIssuerRepository.VerifyAll();
        }
    }
}
