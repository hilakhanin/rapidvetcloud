﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.Issuer_Tests
{
    // returns view with issuer details
    //get clinicGroup model from View, send to ClinicGroup. 
    // removes issuer from Clinic group issuers list


    [TestClass]
    public class Issuer_Delete_Tests
    {
        private Clinic clinic;
        private ClinicGroup clinicGroup;

        public Issuer_Delete_Tests()
        {
            clinicGroup = new ClinicGroup()
                              {
                                  Id = 1,
                                  Name = "test clinic Group",
                              };
            int clinicId = 1;
            clinic = new Clinic()
                         {
                             Id = 1,
                             CreatedById = 1,
                             CreatedDate = DateTime.Now,
                             ClinicGroup = clinicGroup,
                             Name = "test_clinic",
                             Active = true,
                             Issuers = new Collection<Issuer>()
                                           {
                                               new Issuer()
                                                   {
                                                       Id = 1,
                                                       CreatedById = 1,
                                                       Name = "test Issuer 1",
                                                       CreatedDate = DateTime.Now,
                                                       ClinicId = clinicId,
                                                   },
                                                   new Issuer()
                                                   {
                                                       Id = 2,
                                                       CreatedById = 1,
                                                       Name = "test Issuer 2",
                                                       CreatedDate = DateTime.Now,
                                                       ClinicId = clinicId,
                                                   },
                                                   new Issuer()
                                                   {
                                                       Id = 3,
                                                       CreatedById = 1,
                                                       Name = "test Issuer 3",
                                                       CreatedDate = DateTime.Now,
                                                       ClinicId = clinicId,
                                                   }
                                           }

                         };

            clinicGroup.Clinics.Add(clinic);
        }

        [TestMethod]
        public void returns_view_with_issuers_details()
        {
            //arrange
            int issuerId = 1;
            var mockIssuerRepository = new Mock<IIssuerRepository>();
            mockIssuerRepository.Setup(x => x.GetItem(issuerId)).Returns(
                () => clinic.Issuers.Single(i => i.Id == issuerId));
            var controller = new IssuersController(mockIssuerRepository.Object);

            //act
            var result = (PartialViewResult)controller.Delete(issuerId);

            //assert
            Assert.AreEqual("", result.ViewName);
            mockIssuerRepository.VerifyAll();
        }

        [TestMethod]
        public void remove_issuer_from_clincGroup_list()
        {
            //arrange
            var mockIssuerRepository = new Mock<IIssuerRepository>();
            int issuerIdToRemove = 1;

            Issuer issuer = new Issuer()
                                {
                                    Id = 1,
                                    Name = "jhon",
                                    ClinicId = 2,
                                    CompanyId = "3",
                                    Avilable = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedById = 9
                                };

            mockIssuerRepository.Setup(x => x.GetItem(issuer.Id)).Returns(() => issuer);

            mockIssuerRepository.Setup(x => x.RemoveIssuer(It.IsAny<int>())).Returns(
                () => new OperationStatus() { Success = true });

            var controller = new IssuersController(mockIssuerRepository.Object);

            //act
            var result = (PartialViewResult)controller.Delete(issuerIdToRemove);

            //assert
           
            mockIssuerRepository.VerifyAll();
        }
    }
}
