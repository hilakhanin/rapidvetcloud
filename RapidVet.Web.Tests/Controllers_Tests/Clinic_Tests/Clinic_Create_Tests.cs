﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Clinics;

namespace RapidVet.Web.Tests.Controllers_Tests.Clinic_Tests
{
    //returns blank view for clinic creation
    //saves Clinic
    //name field is required
    //administrator Only
    [TestClass]
    public class Clinic_Create_Tests :ControllerTestBase
    {
        [TestMethod]
        public void return_blank_clinic_view()
        {
            //arrange
            var mockClinicRepository = new Mock<IClinicRepository>();
            int clinicGroupId = 1;

            mockClinicRepository.Setup(c => c.GetCreateModel(It.IsAny<int>()))
                .Returns(() => new Clinic
                                   {
                                       ClinicGroupID = clinicGroupId
                                   });
            var controller = new ClinicsController(mockClinicRepository.Object);

            //act
            var result = (PartialViewResult)controller.Create(clinicGroupId);

            //assert
            Assert.AreEqual("", result.ViewName);
            mockClinicRepository.VerifyAll();
        }

        [TestMethod]
        public void successfully_save_clinic()
        {
            //arrange
            int clinicGroupId = 1;
            var mockClinicRepository = new Mock<IClinicRepository>();

            mockClinicRepository.Setup(c => c.Create(It.IsAny<Clinic>()))
                .Returns(() => new OperationStatus()
                                   {
                                       Success = true
                                   });

            var controller = new ClinicsController(mockClinicRepository.Object);
            var membershipMoq = MembershipServiceNoClinicGroup;

            controller.MembershipService = membershipMoq.Object;
            Clinic clinic = new Clinic()
                                {
                                    ClinicGroupID = clinicGroupId,
                                    CreatedDate = DateTime.Now,
                                    Name = "New Clinic" + DateTime.Now.Ticks,
                                    CreatedById = 1,
                                    HasLogo = false,
                                    MainVetId = 1,
                                    Active = true
                                };

            WebModels.Clinics.ClinicCreate clinicCreateModel = new ClinicCreate()
                {
                    Name = "מרפאה חדשה",
                    ClinicGroupId = clinic.Id
                };

            //act
            var result = (JsonResult) controller.Create(clinicCreateModel);

            //assert
            mockClinicRepository.VerifyAll();
            membershipMoq.VerifyAll();

        }

        [TestMethod]
        public void name_field_is_required()
        {
            //arrange
            int clinicGroupId = 1;
            var mockClinicRepository = new Mock<IClinicRepository>();

            var controller = new ClinicsController(mockClinicRepository.Object);

            Clinic clinic = new Clinic()
            {
                ClinicGroupID = clinicGroupId,
                CreatedDate = DateTime.Now,
                Name = "",
                CreatedById = 1,
                HasLogo = false,
                MainVetId = 1,
            };
            WebModels.Clinics.ClinicCreate clinicCreateModel = new ClinicCreate()
            {
                Name = "מרפאה חדשה",
                ClinicGroupId = clinic.Id
            };

            controller.ModelState.AddModelError("Name", "this field is required");

            //act
            var result = (JsonResult)controller.Create(clinicCreateModel);

            //assert
            result.AssertViewRendered();
            mockClinicRepository.VerifyAll();
        }
    }
}
