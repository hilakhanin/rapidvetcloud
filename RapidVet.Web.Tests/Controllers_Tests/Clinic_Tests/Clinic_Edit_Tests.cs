﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;
using RapidVet.WebModels.Clinics;

namespace RapidVet.Web.Tests.Controllers_Tests.Clinic_Tests
{

    //returns  edit view with Clinic data
    //Name is required
    //returns error if operation failed
    //Only admin or Clinic group manager can edit

    [TestClass]
    public class Clinic_Edit_Tests :ControllerTestBase
    {


        [TestMethod]
        public void returns_edit_view_with_clinic_data()
        {
            //arrange
            int clinicId = 1;
            var clinicRepositoryMock = new Mock<IClinicRepository>();
            clinicRepositoryMock.Setup(m => m.GetItem(It.IsAny<int>())).Returns(() => new Clinic() { Id = clinicId, Name = "Clinic 1", CreatedById = 1, MainVetId = 1, CreatedDate = DateTime.Now });
            var controller = new ClinicsController(clinicRepositoryMock.Object);

            //act
            var result = (PartialViewResult)controller.Edit(clinicId);

            //assert
            Assert.AreEqual("", result.ViewName);
            result.AssertViewRendered();
            clinicRepositoryMock.VerifyAll();
        }

        [TestMethod]
        public void successfully_save()
        {
            //arrange
            var clinicRepositoryMock = new Mock<IClinicRepository>();

            clinicRepositoryMock.Setup(m => m.GetItem(It.IsAny<int>()))
                                .Returns(() => new Clinic()
                                {
                                    Id = 1,
                                    Name = "Clinic name",
                                    ClinicGroupID = 2,
                                    CreatedDate = DateTime.Now,
                                    MainVetId = 1
                                });

            clinicRepositoryMock.Setup(m => m.Update(It.IsAny<Clinic>()))
                .Returns(() => new OperationStatus()
                                   {
                                       Success = true
                                   });

            TestControllerBuilder builder = new TestControllerBuilder();
            var formCollection = new FormCollection();
            int id = 1;
            var controller = builder.CreateController<ClinicsController>(clinicRepositoryMock.Object);
            var membershipMoq = MembershipServiceNoClinicGroup;

            controller.MembershipService = membershipMoq.Object;

            var model = new ClinicEdit()
                {
                    Name = "Clinic name" + DateTime.Now.Ticks,
                    Id = 1,
                    ClinicGroupId = 1
                };

            //act
            var result = (PartialViewResult)controller.Edit(model);

            //assert
            result.AssertActionRedirect();
            membershipMoq.VerifyAll();
            clinicRepositoryMock.VerifyAll();
        }

        [TestMethod]
        public void name_is_requiered()
        {
            //arrange
            var clinicRepositoryMock = new Mock<IClinicRepository>();

            clinicRepositoryMock.Setup(m => m.GetItem(It.IsAny<int>()))
                                .Returns(() => new Clinic()
                                {
                                    Id = 1,
                                    Name = "Clinic name",
                                    ClinicGroupID = 2,
                                    CreatedDate = DateTime.Now,
                                    MainVetId = 1
                                });

            TestControllerBuilder builder = new TestControllerBuilder();
            var formCollection = new FormCollection();
            int id = 1;
            var controller = builder.CreateController<ClinicsController>(clinicRepositoryMock.Object);

            var model = new ClinicEdit()
            {
                Name = " ",
                Id = 1,
                ClinicGroupId = 1
            };

            //act
            var result = (PartialViewResult)controller.Edit(model);

            //assert
            Assert.IsFalse(result.ViewData.ModelState.IsValid);
            Assert.IsTrue(result.ViewData.ModelState.ContainsKey("Name"));
            clinicRepositoryMock.VerifyAll();
        }

        [TestMethod]
        public void returns_error_if_operation_failed()
        {
            //arrange
            var clinicRepositoryMock = new Mock<IClinicRepository>();

            clinicRepositoryMock.Setup(m => m.GetItem(It.IsAny<int>()))
                                .Returns(() => new Clinic()
                                {
                                    Id = 1,
                                    Name = "Clinic name",
                                    ClinicGroupID = 2,
                                    CreatedDate = DateTime.Now,
                                    MainVetId = 1
                                });

            clinicRepositoryMock.Setup(m => m.Update(It.IsAny<Clinic>()))
               .Returns(() => new OperationStatus()
               {
                   Success = false
               });

            TestControllerBuilder builder = new TestControllerBuilder();
            var formCollection = new FormCollection();
            int id = 1;
            var controller = builder.CreateController<ClinicsController>(clinicRepositoryMock.Object);
            var membershipMoq = MembershipServiceNoClinicGroup;

            controller.MembershipService = membershipMoq.Object;
          
            var model = new ClinicEdit()
            {
                Name = "Clinic name" + DateTime.Now.Ticks,
                Id = 1,
                ClinicGroupId = 1
            };

            //act
            var result = (PartialViewResult)controller.Edit(model);

            //assert
            result.AssertViewRendered().ForView("Error");
            membershipMoq.VerifyAll();
            clinicRepositoryMock.VerifyAll();
        }
    }
}
