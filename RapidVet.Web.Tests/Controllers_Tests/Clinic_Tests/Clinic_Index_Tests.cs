﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model.Clinics;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.Clinic_Tests
{
    //View by admin or ClinicGroupManager

    [TestClass]
    public class Clinic_Index_Tests
    {
        private IQueryable<Clinic> mixedClinicList;
        private IQueryable<Clinic> pureClinicList;


        public Clinic_Index_Tests()
        {
            mixedClinicList = new List<Clinic>()
                             {
                                 new Clinic()
                                     {ClinicGroupID = 1, CreatedById = 1, Name = "Clinic 1", CreatedDate = DateTime.Now},
                                 new Clinic()
                                     {ClinicGroupID = 1, CreatedById = 1, Name = "Clinic 2", CreatedDate = DateTime.Now},
                                 new Clinic()
                                     {ClinicGroupID = 1, CreatedById = 1, Name = "Clinic 3", CreatedDate = DateTime.Now},
                                 new Clinic()
                                     {ClinicGroupID = 2, CreatedById = 1, Name = "Clinic 4", CreatedDate = DateTime.Now}
                             }.AsQueryable();

            pureClinicList = new List<Clinic>()
                             {
                                 new Clinic()
                                     {ClinicGroupID = 1, CreatedById = 1, Name = "Clinic 1", CreatedDate = DateTime.Now},
                                 new Clinic()
                                     {ClinicGroupID = 1, CreatedById = 1, Name = "Clinic 2", CreatedDate = DateTime.Now},
                                 new Clinic()
                                     {ClinicGroupID = 1, CreatedById = 1, Name = "Clinic 3", CreatedDate = DateTime.Now}
                             }.AsQueryable();


        }

        [TestMethod]
        public void returns_view_with_List_of_all_clinics()
        {
            //arrange
            var clinicRepositoryMock = new Mock<RapidVet.Repository.Clinics.IClinicRepository>();
            clinicRepositoryMock.Setup(r => r.GetList()).Returns(() => mixedClinicList);
            var clinicController = new ClinicsController(clinicRepositoryMock.Object);

            //act
            var view = (ViewResult)clinicController.Index(1);

            //assert
            Assert.AreEqual("", view.ViewName);
            clinicRepositoryMock.VerifyAll();
        }

        //[TestMethod]
        //public void returns_view_with_List_of_all_clinics_in_clinicGroup()
        //{
        //    //arrange
        //    int clinicGroupId = 1;
        //    var clinicRepositoryMock = new Mock<RapidVet.Repository.Clinics.IClinicRepository>();
        //    clinicRepositoryMock.Setup(r => r.GetList(clinicGroupId)).Returns(() => pureClinicList);
        //    var clinicController = new ClinicsController(clinicRepositoryMock.Object);

        //    //act
        //    var view = (ViewResult) clinicController.Index(clinicGroupId);
        //    var model = view.ViewData.Model as IEnumerable<Clinic>;

        //    //assert
        //    Assert.IsTrue(pureClinicList.SequenceEqual(model));
        //    clinicRepositoryMock.VerifyAll();
        //}
    }
}


