﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.Clinic_Tests
{
    //return view with Clinic details
    [TestClass]
    public class Clinic_Details_Tests
    {
        private Clinic clinic;

        public Clinic_Details_Tests()
        {
            clinic = new Clinic()
                         {
                             Id = 1,
                             Name = DateTime.Now.Ticks.ToString(),
                             CreatedDate = DateTime.Now,
                             CreatedById = 1,
                             Users = new Collection<User>()
                                         {
                                             new User()
                                                 {
                                                     Id = 2,
                                                     ClinicGroupId = 1,
                                                     FirstName = "jane",
                                                     LastName = "doe",
                                                     Username = "jane",
                                                     Roles = new Collection<Role>()
                                                                 {
                                                                     new Role(){RoleId = Guid.NewGuid() , RoleName = "doctor"}
                                                                 },
                                                     Email = "jane@doe.com",
                                                     Password = "123456"
                                                 }
                                         },
                             Issuers = new List<Issuer>()
                                           {
                                               //new Issuer() {Id = 1,  ClinicId = 1, Name = "iss1", CompanyId = "123"}
                                           },
                             Active = true
                         };
        }
        [TestMethod]
        public void returns_view_with_clinic_details()
        {
            //arrange
            var mockClinicRepository = new Mock<IClinicRepository>();
            mockClinicRepository.Setup(c => c.GetItem(It.IsAny<int>())).Returns(new Clinic()
                                                                                    {
                                                                                        Id = 1,
                                                                                        ClinicGroupID = 1
                                                                                    });
            var controller = new ClinicsController(mockClinicRepository.Object);

            //act
            var result = (ViewResult)controller.Details(clinic.Id);

            //assert
            Assert.AreEqual("", result.ViewName);
            mockClinicRepository.VerifyAll();
        }
    }
}
