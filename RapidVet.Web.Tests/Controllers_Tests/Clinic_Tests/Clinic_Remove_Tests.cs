﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcContrib.TestHelper;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Repository.Clinics;
using RapidVet.Web.Controllers;

namespace RapidVet.Web.Tests.Controllers_Tests.Clinic_Tests
{
    //returns view with clinic data
    // successfully removes Clinic --> updates "active" value to "false"
    [TestClass]
    public class Clinic_Remove_Tests:ControllerTestBase
    {
        [TestMethod]
        public void returns_View_with_Clinic_Data()
        {
            //arrange
            int clinicId = 1;
            var mockClinicRepository = new Mock<IClinicRepository>();
            mockClinicRepository.Setup(c => c.GetItem(It.IsAny<int>())).Returns(new Clinic()
                                                                                    {
                                                                                        Id = clinicId,
                                                                                        ClinicGroupID = 1,
                                                                                        MainVetId = 1,
                                                                                        Name = DateTime.Now.Ticks.ToString(),
                                                                                        Active = true
                                                                                    });

            var controller = new ClinicsController(mockClinicRepository.Object);

            //act
            var result = (ViewResult)controller.Remove(clinicId);

            //assert
            Assert.AreEqual("", result.ViewName);
            mockClinicRepository.VerifyAll();

        }

        [TestMethod]
        public void successfully_remove_clinic_from_group()
        {
            //arrange
            int clinicGroupId = 1;
            int clinicId = 1;
            Clinic clinic = new Clinic()
                              {
                                  Id = clinicId,
                                  ClinicGroupID = clinicGroupId,
                                  MainVetId = 1,
                                  Name = DateTime.Now.Ticks.ToString(),
                                  Active = true
                              };
            var mockClinicRepository = new Mock<IClinicRepository>();
            mockClinicRepository.Setup(c => c.GetItem(It.IsAny<int>())).Returns(() => clinic);
            mockClinicRepository.Setup(c => c.Update(It.IsAny<Clinic>())).Returns(() => new OperationStatus()
                                                                                            {
                                                                                                Success = true
                                                                                            });


            var controller = new ClinicsController(mockClinicRepository.Object);
            var membershipMoq = MembershipServiceNoClinicGroup;

            controller.MembershipService = membershipMoq.Object;
            //act
            var result = (RedirectToRouteResult)controller.Remove(clinic);

            //assert
            result.AssertActionRedirect();
            membershipMoq.VerifyAll();
            Assert.AreEqual("", result.RouteName);
            mockClinicRepository.VerifyAll();
        }
    }
}
