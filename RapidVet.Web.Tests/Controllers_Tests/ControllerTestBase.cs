﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using RapidVet.Model.Users;
using RapidVetMembership;

namespace RapidVet.Web.Tests.Controllers_Tests
{
    public class ControllerTestBase
    {

        public Mock<IMembershipService> MembershipServiceNoClinicGroup
        {
            get
            {
                var _mock = new Mock<IMembershipService>();
                var user = new User() { Id = 1 ,FirstName = "test_admin"};
                _mock.Setup(x => x.CurrentUser).Returns(user);
                return _mock;
            }
        }
    }
}
