﻿//using System;
//using System.Text;
//using System.Collections.Generic;
//using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using MvcContrib.TestHelper;
//using RapidVet.Exeptions;
//using RapidVet.Model.Users;
//using RapidVet.Repository;
//using RapidVet.Web.Controllers;
//using RapidVetMembership;

//namespace RapidVet.Web.Tests
//{
//    [TestClass]
//    public class MembershipService_test
//    {

//        [TestMethod]
//        [ExpectedException(typeof(NotAuthenticatedException))]
//        public void not_autheniticated_user_throws_exeption()
//        {
//            var userReposetorMock = new Mock<IUserRepository>();

//            TestControllerBuilder builder = new TestControllerBuilder();

//            var service = new MembershipService(userReposetorMock.Object, false);

//            var user = service.GetUser("test");
//        }

//        [TestMethod]
//        public void get_user()
//        {
//            var userReposetorMock = new Mock<IUserRepository>();

//            userReposetorMock.Setup(r => r.GetItemByUserName(It.IsAny<string>())).Returns(() => new User());

//            TestControllerBuilder builder = new TestControllerBuilder();

//            var service = new MembershipService(userReposetorMock.Object, true);

//            var user = service.GetUser("test");

//            Assert.IsNotNull(user);

//            userReposetorMock.Verify(r => r.GetItemByUserName(It.IsAny<string>()), Times.Exactly(1));

//            userReposetorMock.VerifyAll();
//        }

//        [TestMethod]
//        public void get_user_for_the_second_time()
//        {
//            string userName = "test";

//            var userReposetorMock = new Mock<IUserRepository>();

//            userReposetorMock.Setup(r => r.GetItemByUserName(It.IsAny<string>())).Returns(() => new User() { Username = userName });

//            TestControllerBuilder builder = new TestControllerBuilder();

//            var service = new MembershipService(userReposetorMock.Object, true);

//            var user = service.GetUser(userName);

//            Assert.IsNotNull(user);

//            user = service.GetUser(userName);

//            Assert.IsNotNull(user);

//            userReposetorMock.Verify(r => r.GetItemByUserName(It.IsAny<string>()), Times.Exactly(1));

//            userReposetorMock.VerifyAll();
//        }


//        [TestMethod]
//        [ExpectedException(typeof(UserNotFoundException))]
//        public void get_not_found_user()
//        {
//            var userReposetorMock = new Mock<IUserRepository>();

//            userReposetorMock.Setup(r => r.GetItemByUserName(It.IsAny<string>()));

//            TestControllerBuilder builder = new TestControllerBuilder();

//            var service = new MembershipService(userReposetorMock.Object, true);

//            var user = service.GetUser("test");
//        }
//    }
//}
