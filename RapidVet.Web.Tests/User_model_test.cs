﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RapidVet.Model.Users;

namespace RapidVet.Web.Tests
{
    [TestClass]
    public class User_model_test
    {
        private string password = "password123";

        [TestMethod]
        public void set_password_create_new_salt()
        {
            var user = new User();
            user.Password = password;

            Assert.IsTrue(user.Salt != null );
        }

        [TestMethod]
        public void valid_password_return_true()
        {
            
            var user = new User();
            user.Password = password;

            Assert.IsTrue(user.IsPasswordValid(password));
        }

        [TestMethod]
        public void inValid_password_return_fals()
        {
            var user = new User();
            user.Password = password;

            Assert.IsFalse(user.IsPasswordValid("123456"));
        }
    }
}
