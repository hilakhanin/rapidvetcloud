﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVetMembership.Membership
{
   public class UserLogOffExeption:Exception
    {
       public override string Message
       {
           get { return RapidVet.Resources.Membership.UseLogOut; }
       }
    }
}
