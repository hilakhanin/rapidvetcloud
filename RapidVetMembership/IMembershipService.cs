using System;
using System.Collections.Generic;
using System.Web.Security;
using RapidVet.Model.Users;

namespace RapidVetMembership
{
    public interface IMembershipService : IDisposable
    {
        /// <summary>
        /// returns user by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns>User object</returns>
        User GetUser(string userName, int id = -1);

        /// <summary>
        /// returns users by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns>User object</returns>
        List<User> GetUsers(string userName);


        User CurrentUser { get; }

        /// <summary>
        /// validates user when attempting to connect
        /// </summary>
        /// <param name="username">user name</param>
        /// <param name="password">password</param>
        /// <param name="status"> UserValidationStatus object</param>
        /// <returns>ConcurrentUserInfo object</returns>
        ConcurrentUserInfo ValidateUser(string username, string password, out UserValidationStatus status);

        /// <summary>
        /// flag is user is authenticated
        /// </summary>
        bool IsAuthenticated { get; }

        //currently not in use
        MembershipUser CreateUser(string username, string password, string email,
                                                string passwordQuestion, string passwordAnswer, bool isApproved,
                                                 out MembershipCreateStatus status);
    }
}