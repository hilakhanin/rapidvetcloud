﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web;
using RapidVet.Exeptions;
using RapidVet.Model.Users;
using RapidVet.Repository;

namespace RapidVetMembership
{
    public enum UserValidationStatus
    {
        Valid, IncorrectPassword, Blocked, TooManyUsers, IncorrectUserName, WithNoPermission, TrialExpired
    }
    public class MembershipService : IMembershipService
    {
        private IUserRepository _userRepository;

        private IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                { UserRepository = new UserRepository(); }
                return _userRepository;
            }
            set { _userRepository = value; }
        }

        //private static List<User> _usersList;

        private static object _userListLock = new object();

        /// <summary>
        /// flag is user is authenticated
        /// </summary>
        public bool IsAuthenticated { get { return HttpContext.Current.User.Identity.IsAuthenticated; } }

        public void Dispose()
        {
            UserRepository.Dispose();
        }

        /// <summary>
        /// returns user by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns>User object</returns>
        public User GetUser(string userName, int id = -1)
        {
            var user = UserRepository.GetItemByUserName(userName, id);
            return user;
        }

        /// <summary>
        /// returns users by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns>User object</returns>
        public List<User> GetUsers(string userName)
        {
            var users = UserRepository.GetItemsByUserName(userName);
            return users;
        }

        /// <summary>
        /// validates user when attempting to connect
        /// </summary>
        /// <param name="username">user name</param>
        /// <param name="password">password</param>
        /// <param name="status"> UserValidationStatus object</param>
        /// <returns>ConcurrentUserInfo object</returns>
        public ConcurrentUserInfo ValidateUser(string username, string password, out UserValidationStatus status)
        {
            var allUsers = GetUsers(username);
            User user = null;
            foreach (var u in allUsers)
            {
                if (!u.IsPasswordValid(password))
                    continue;

                user = GetUser(null, u.Id); ;
                break;
            }

            if (user == null)
            {
                status = UserValidationStatus.IncorrectUserName;
                return null;
            }

            //if (user.IsPasswordValid(password))
            //{                
            ConcurrentUserInfo concurentUserInfo = null;
            try //fix for typeInitializationException. do not remove try catch!
            {
                concurentUserInfo = ConcurrentUserInfo.GetConcurrentUserByUserName(username);
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
            if (concurentUserInfo != null && !user.IsAdministrator)
            {
                // Delete (LogOut) the user and creat new
                ConcurrentUserInfo.RemoveByGuid(concurentUserInfo.UserName);
            }
            if (user.ClinicGroupId != null &&
                (ConcurrentUserInfo.GetClinicGroupUserCount(user.ClinicGroupId.Value) >= //current connected count
                 user.ClinicGroup.MaxConcurrentUsers)) //max current connections allowed
            {
                //Else try to clean the list
                ConcurrentUserInfo.ClearInactiveUsers(user);
                if (ConcurrentUserInfo.GetClinicGroupUserCount(user.ClinicGroupId.Value) <
                 user.ClinicGroup.MaxConcurrentUsers || user.IsAdministrator)
                {
                    status = UserValidationStatus.Valid;
                    concurentUserInfo = new ConcurrentUserInfo(user);
                }
                //Else show message
                status = UserValidationStatus.TooManyUsers;
            }
            else
            {
                if (!user.IsAdministrator && user.ClinicGroup.TrialPeriod && user.ClinicGroup.ExpirationDate.Value.AddHours(23).AddMinutes(59) < DateTime.Now)
                {
                    status = UserValidationStatus.TrialExpired;
                }
                else
                {
                    status = UserValidationStatus.Valid;
                    concurentUserInfo = new ConcurrentUserInfo(user);
                }
            }


            return concurentUserInfo;
            //}
            //else
            //{

            //    status = UserValidationStatus.IncorrectPassword;
            //    return null;
            //}
        }

        public User CurrentUser
        {
            get { return GetUser(HttpContext.Current.User.Identity.Name); }
        }




        public MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }
    }
}
