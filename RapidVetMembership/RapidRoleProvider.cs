﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using RapidVet.Model.Users;
using RapidVet.Repository;

namespace RapidVetMembership
{
    public class RapidRoleProvider : RoleProvider
    {
        protected List<ConcurrentUserInfo> ConcurrentUsers
        {
            get { return ConcurrentUserInfo.ConcurrentUsers; }
        }

        public override string ApplicationName
        {
            get
            {
                return this.GetType().Assembly.GetName().Name.ToString();
            }
            set
            {
                this.ApplicationName = this.GetType().Assembly.GetName().Name.ToString();
            }
        }
        /// <summary>
        /// checks if the role exists 
        /// </summary>
        /// <param name="roleName">role name</param>
        /// <returns>boolean</returns>
        public override bool RoleExists(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                return false;
            }
            using (var context = new RapidVetDataContext())
            {
                Role role = null;
                role = context.Roles.FirstOrDefault(Rl => Rl.RoleName == roleName);
                return role != null;
            }
        }

        /// <summary>
        /// checks if the user has role
        /// </summary>
        /// <param name="concurentGuid">guid represents connected user</param>
        /// <param name="roleName">role name</param>
        /// <returns>boolean</returns>
        public override bool IsUserInRole(string concurentGuid, string roleName)
        {

            // Change this function to recive Guid of concurent user insted of userName
            if (string.IsNullOrEmpty(concurentGuid))
            {
                return false;
            }
            if (string.IsNullOrEmpty(roleName))
            {
                return false;
            }

            var user = ConcurrentUserInfo.GetConcurrentUserInfoByGuid(concurentGuid);

            if (user != null)
            {
                return user.IsUserInRole(roleName);
            }
            return false;

        }

        /// <summary>
        /// returns all roles
        /// </summary>
        /// <returns>string[]</returns>
        public override string[] GetAllRoles()
        {
            using (var context = new RapidVetDataContext())
            {
                return context.Roles.Select(rl => rl.RoleName).ToArray();
            }
        }

        /// <summary>
        /// get users by role
        /// </summary>
        /// <param name="roleName">role name</param>
        /// <returns>string[]</returns>
        public override string[] GetUsersInRole(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                return null;
            }
            using (var context = new RapidVetDataContext())
            {
                Role role = null;
                role = context.Roles.FirstOrDefault(rl => rl.RoleName == roleName);
                return role != null ? role.Users.Select(usr => usr.Username).ToArray() : null;
            }
        }


        /// <summary>
        /// get roles for user
        /// </summary>
        /// <param name="username">user name</param>
        /// <returns>string[]</returns>
        public override string[] GetRolesForUser(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return null;
            }
            using (RapidVetDataContext Context = new RapidVetDataContext())
            {
                User User = null;
                User = Context.Users.FirstOrDefault(Usr => Usr.Username == username);
                if (User != null)
                {
                    return User.Roles.Select(Rl => Rl.RoleName).ToArray();
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// find users that have this role
        /// </summary>
        /// <param name="roleName">role name</param>
        /// <param name="usernameToMatch">user name</param>
        /// <returns>string[]</returns>
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                return null;
            }

            if (string.IsNullOrEmpty(usernameToMatch))
            {
                return null;
            }

            using (var Context = new RapidVetDataContext())
            {

                return (from Rl in Context.Roles from Usr in Rl.Users where Rl.RoleName == roleName && Usr.Username.Contains(usernameToMatch) select Usr.Username).ToArray();
            }
        }

        /// <summary>
        /// creates role in db
        /// </summary>
        /// <param name="roleName">role name</param>
        public override void CreateRole(string roleName)
        {
            if (string.IsNullOrEmpty(roleName)) return;
            using (var Context = new RapidVetDataContext())
            {
                Role role = null;
                role = Context.Roles.FirstOrDefault(Rl => Rl.RoleName == roleName);
                if (role != null) return;
                var newRole = new Role
                    {
                        RoleId = Guid.NewGuid(),
                        RoleName = roleName
                    };
                Context.Roles.Add(newRole);
                Context.SaveChanges();
            }
        }


        /// <summary>
        /// deletes role from db
        /// </summary>
        /// <param name="roleName">role name</param>
        /// <param name="throwOnPopulatedRole">flag that is raised if there are users in this role</param>
        /// <returns>boolean</returns>
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                return false;
            }
            using (var Context = new RapidVetDataContext())
            {
                Role role = null;
                role = Context.Roles.FirstOrDefault(rl => rl.RoleName == roleName);
                if (role == null)
                {
                    return false;
                }
                if (throwOnPopulatedRole)
                {
                    if (role.Users.Any())
                    {
                        return false;
                    }
                }
                else
                {
                    role.Users.Clear();
                }
                Context.Roles.Remove(role);
                Context.SaveChanges();
                return true;
            }
        }

        /// <summary>
        /// adds users to roles
        /// </summary>
        /// <param name="usernames"> string[] user names</param>
        /// <param name="roleNames">string[] role names</param>
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            using (var context = new RapidVetDataContext())
            {
                if (context.Users != null)
                {
                    var users = context.Users.Where(Usr => usernames.Contains(Usr.Username)).ToList();
                    var roles = context.Roles.Where(Rl => roleNames.Contains(Rl.RoleName)).ToList();
                    foreach (User user in users)
                    {
                        foreach (Role role in roles.Where(role => !user.Roles.Contains(role)))
                        {
                            user.Roles.Add(role);
                        }
                    }
                }
                context.SaveChanges();
            }
        }

        /// <summary>
        /// remove users from roles
        /// </summary>
        /// <param name="usernames">string[] user names</param>
        /// <param name="roleNames">string[] role names</param>
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            using (var context = new RapidVetDataContext())
            {
                foreach (var username in usernames)
                {
                    var us = username;
                    var user = context.Users.FirstOrDefault(U => U.Username == us);
                    if (user == null) continue;
                    foreach (var roleName in roleNames)
                    {
                        var rl = roleName;
                        var role = user.Roles.FirstOrDefault(R => R.RoleName == rl);
                        if (role != null)
                        {
                            user.Roles.Remove(role);
                        }
                    }
                }
                context.SaveChanges();
            }
        }
    }
}