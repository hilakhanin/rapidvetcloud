﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;

namespace RapidVetMembership
{

    public class FormsAuthenticationService : IFormsAuthenticationService
    {

        /// <summary>
        /// sets suthentication cookie
        /// </summary>
        /// <param name="userName">user name</param>
        /// <param name="createPersistentCookie"> presistance flag</param>
        public virtual void SetAuthCookie(string userName, bool createPersistentCookie)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot     be null or empty.", "userName");                        
            FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
        }

        /// <summary>
        /// sign out user from system
        /// </summary>
        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}
