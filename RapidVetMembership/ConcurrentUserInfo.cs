﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RapidVet.Model.Users;
using RapidVet.Repository;
using RapidVetMembership.Membership;

namespace RapidVetMembership
{
    public class ConcurrentUserInfo
    {

        public Guid UserName { get; private set; }

        public DateTime LoginTime { get; private set; }

        public DateTime LastActivity { get; private set; }

        public User User { get; set; }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="user">User object</param>
        public ConcurrentUserInfo(User user)
        {
            UserName = Guid.NewGuid(); //user.Username.ToLower().Equals("admin") ? Guid.Empty : Guid.NewGuid();
            LoginTime = DateTime.Now;
            LastActivity = DateTime.Now;
            User = user;
            var f = ConcurrentUsers.Find(x => x.User.Id == user.Id);
            ConcurrentUsers.Add(this);
            using (var userEntityRepository = new ConcurrentUserEntityRepository())
            {
                if(f != null)
                {
                    userEntityRepository.Remove(f.UserName);
                    ConcurrentUsers.Remove(f);
                }

                userEntityRepository.Add(new ConcurrentUserEntity()
                    {
                        LoginTime = LoginTime,
                        UserId = User.Id,
                        UserName = UserName,
                        ActiveClinicGroupId = User.ActiveClinicGroupId
                    });
            }

        }

        private ConcurrentUserInfo() { }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="userName">user name</param>
        /// <param name="login">login dateTime</param>
        public ConcurrentUserInfo(int userId, Guid userName, DateTime login)
        {
            UserName = userName;
            LoginTime = login;
            LastActivity = DateTime.Now;
            using (var userRepository = new UserRepository())
            {
                User = userRepository.GetItem(userId, userId == 1);
            }
            ConcurrentUsers.Add(this);
        }

        /// <summary>
        /// update last activity, return false in case of user disconnected
        /// </summary>
        public bool UpdateUserActivity()
        {
            var uID = User.Id;
            using (var userRepository = new UserRepository())
            {
                if (!userRepository.DataContext.ConcurrentUserEntities.Any(x => x.UserId == uID && x.UserName == UserName))
                    return false;
            }
            LastActivity = DateTime.Now;
            return true;
        }

        static object locker = new object();
        private const string ConcurrentUsersString = "ConcurrentUsers";

        /// <summary>
        /// returns concurrentUserInfo object by user guid
        /// </summary>
        /// <param name="concurentGuid">guid that identifies user</param>
        /// <param name="updateUserActivity">flag to update user activity in db</param>
        /// <returns>User object</returns>
        public static User GetConcurrentUserInfoByGuid(string concurentGuid, bool updateUserActivity = true)
        {
            var concurrentUser = ConcurrentUsers.FirstOrDefault(u => u.UserName.ToString() == concurentGuid);
            if (concurrentUser == null)
            {
                return null;
            }
            if (updateUserActivity)
            {
                if (!concurrentUser.UpdateUserActivity())
                    return null;
            }
            return concurrentUser.User;
        }

        //static ConcurrentUserInfo()
        //{
        //    using (var concurrentUserEntityRepository = new ConcurrentUserEntityRepository())
        //    {
        //        foreach (var item in concurrentUserEntityRepository.GetList())
        //        {
        //            var u = new ConcurrentUserInfo(item.UserId, item.UserName, item.LoginTime);
        //        }
        //    }
        //}
        /// <summary>
        /// returns cuncurrentUserInfo object by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns>ConcurrentUserInfo</returns>
        public static ConcurrentUserInfo GetConcurrentUserByUserName(string userName)
        {
            return ConcurrentUsers.FirstOrDefault(u => u.User != null ? u.User.Username.Equals(userName) : false);
        }

        /// <summary>
        /// count connected concurrent users in clinic group
        /// </summary>
        /// <param name="clinicGroupID"> clinic group id</param>
        /// <returns>int</returns>
        public static int GetClinicGroupUserCount(int clinicGroupID)
        {
            if (ConcurrentUsers == null)
            {
                return 0;
            }
            return ConcurrentUsers.Count(u => u.User.ClinicGroupId == clinicGroupID);
        }

        /// <summary>
        /// returns all concurrent users in the system
        /// </summary>
        public static List<ConcurrentUserInfo> ConcurrentUsers
        {
            get
            {
                var concurrentUsers = HttpContext.Current.Application.Get(ConcurrentUsersString) as List<ConcurrentUserInfo>;
                if (concurrentUsers == null)
                {
                    lock (locker)
                    {
                        concurrentUsers = HttpContext.Current.Application.Get(ConcurrentUsersString) as List<ConcurrentUserInfo>;
                        if (concurrentUsers == null)
                        {
                            concurrentUsers = new List<ConcurrentUserInfo>();

                            using (var concurrentUserEntityRepository = new ConcurrentUserEntityRepository())
                            using (var userRepository = new UserRepository())
                            {
                                foreach (var item in concurrentUserEntityRepository.GetList())
                                {
                                    var user = new ConcurrentUserInfo() { LastActivity = item.LoginTime, LoginTime = item.LoginTime, UserName = item.UserName };
                                    user.User = userRepository.GetItem(item.UserId, item.UserId == 1);

                                    concurrentUsers.Add(user);
                                }
                            }
                            HttpContext.Current.Application.Add(ConcurrentUsersString, concurrentUsers);
                        }
                    }

                }
                return concurrentUsers;
            }
        }

        /// <summary>
        /// clears inactive users  in clinic group
        /// </summary>
        /// <param name="clinicGroupId">clinic group id</param>
        public static void ClearInactiveUsers(User user)
        {
            int inactiveUserTimeInMinutes = user.ActiveClinicGroup.TimeToDisconnectInactiveUsersInMinutes;
            TimeSpan disconnectTime;

            if (inactiveUserTimeInMinutes < 5)
                disconnectTime = new TimeSpan(0, MembershipConsts.INACTIVE_USER_TIME_SPAN, 0);
            else
                disconnectTime = new TimeSpan(0, inactiveUserTimeInMinutes, 0);


            foreach (var item in ConcurrentUsers.Where(u => u.User.ClinicGroupId == user.ClinicGroupId).ToList())
            {
                if (DateTime.Now - item.LastActivity > disconnectTime)
                {
                    RemoveItem(item);
                }
            }
        }

        /// <summary>
        /// remove user from cuncurrent by user guid
        /// </summary>
        /// <param name="guid">guid that identifies user</param>
        public static void RemoveByGuid(Guid guid)
        {
            var item = ConcurrentUsers.FirstOrDefault(u => u.UserName == guid);
            if (item != null)
            {
                RemoveItem(item);
            }
        }


        /// <summary>
        /// remove user from cuncurrent by user name
        /// </summary>
        /// <param name="userName">user name</param>
        public static void RemoveByUserName(string userName)
        {
            var item = ConcurrentUsers.FirstOrDefault(u => u.User.Username == userName);
            if (item != null)
            {
                RemoveItem(item);
            }
        }

        /// <summary>
        /// removes a ConcurrentUserInfo from being concurrent
        /// </summary>
        /// <param name="item">ConcurrentUserInfo object</param>
        static void RemoveItem(ConcurrentUserInfo item)
        {
            ConcurrentUsers.Remove(item);
            using (var userEntityRepository = new ConcurrentUserEntityRepository())
            {
                userEntityRepository.Remove(item.UserName);
            }
        }
    }
}