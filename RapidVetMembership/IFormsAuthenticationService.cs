namespace RapidVetMembership
{
    public interface IFormsAuthenticationService
    {
        /// <summary>
        /// sets suthentication cookie
        /// </summary>
        /// <param name="userName">user name</param>
        /// <param name="createPersistentCookie"> presistance flag</param>
        void SetAuthCookie(string userName, bool createPersistentCookie);

        /// <summary>
        /// sign out user from system
        /// </summary>
        void SignOut();
    }
}