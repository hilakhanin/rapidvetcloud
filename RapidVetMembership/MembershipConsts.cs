﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVetMembership
{
    public static class MembershipConsts
    {
        public static int INACTIVE_USER_TIME_SPAN
        {
            get
            {
                return 120;
            }
        }
    }
}
