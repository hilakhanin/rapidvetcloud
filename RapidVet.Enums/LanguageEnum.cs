﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums
{
    public enum LanguageEnum
    {
        Hebrew, English
    }
}
