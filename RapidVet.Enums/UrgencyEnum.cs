﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums
{
    public enum UrgencyEnum
    {
        NotUrgent,
        Urgent,
        Critical
    }
}
