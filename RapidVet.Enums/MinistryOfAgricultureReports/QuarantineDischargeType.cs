﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums.MinistryOfAgricultureReports
{
    public enum QuarantineDischargeType
    {
        ToOwner = 1,
        Adoption,
        OtherBody,
        PutToSleep,
        Deceased,
        Disappeared,
        InWaiting
    }
}
