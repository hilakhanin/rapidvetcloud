﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums.MinistryOfAgricultureReports
{
    public enum OffenceType
    {
        NoMuzzle=1,
        NoTying=2,
        NoSterilization=3,
        MinimalAgeViolation=4
    }
}
