﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums.MinistryOfAgricultureReports
{
    public enum ComplaintType
    {
        BiteAttack = 1,
        SupervisionLawViolation = 2,
        AnimalRightsLawViolation=3
    }
}
