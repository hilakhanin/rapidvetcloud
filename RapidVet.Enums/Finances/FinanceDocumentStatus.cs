﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums.Finances
{
    public enum FinanceDocumentStatus
    {
        Open, Closed , PartlyClose
    }
}
