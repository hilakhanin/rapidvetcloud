﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums
{
    public enum LetterTemplateType
    {
        Sterilization = 1,
        BillOfHealthHe,
        BillOfHealthEn,
        DogOwnerLicense,
        RabiesVaccineCertificate,
        MedicalProcedureSummery,
        PatientDischargePaper,
        MinistryOfAgricultureReport,
        PersonalLetter
    }
}
