﻿namespace RapidVet.Enums
{
    public enum PatientActivityEnum
    {
        All, Active, Inactive
    }
}