﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums
{
    public enum RabiesReportOptions
    {
        Excel, Print, PrintAshdod, Sorin, Meloona, Rapid, Doctorat, ReverseDoctorat, Dolittle, SendToRegionalCouncilByMail
    }
}
