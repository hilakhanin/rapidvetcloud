﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums
{
    public enum PriceListItemTypeEnum
    {
        Other = 1, Treatments, Examinations, LabTests, Food, Vaccines, PreventiveTreatments
    }
}
