﻿namespace RapidVet.Enums
{
    public enum TreatmentPrintOutput
    {
        Stiker, Letter, Sms, Email
    }
}