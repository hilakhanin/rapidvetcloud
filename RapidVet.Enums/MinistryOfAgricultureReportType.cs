﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums
{
    public enum MinistryOfAgricultureReportType
    {
        ChipImplant = 1,
        RabiesVaccination = 2,
        ComplaintReport = 9,
        DangerousDog = 8,
        RevokingLicense = 7,
        QuarantineDischarge = 6,
        DogLicenseGranting = 5,
        QuarantineAdmission = 4,
        Attack = 3,
        FindingADog = 10
    }
}
