﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums
{
    public enum PreventiveReminderPrintOutput
    {
        Stiker, Letter, PostCard, Sms, Email
    }
}
