﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums
{
    public enum ArchivesFileType
    {
        Image = 1, Video, Document, Audio
    }
}
