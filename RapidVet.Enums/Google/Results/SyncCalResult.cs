﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums.Google.Results
{
    public enum SyncCalResult
    {
        CalendarNameAlreadyExist,
        MaxSyncAcoountsExeeded,
        GoogleSyncEmailAccountNotProvided,
        GoogleSyncEmailAccountNotValid,
        OK
    }
}
