namespace RapidVet.Enums
{
    public enum MedicationAdministrationType
    {
        Caps, Drops, Tabs, Sol, Ung
    }
}