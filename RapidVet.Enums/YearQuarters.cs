﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums
{
    public enum YearQuarters
    {
        Q1, Q2, Q3, Q4
    }
}
