﻿namespace RapidVet.Enums.DataMigration
{
    public enum MigrationOrder
    {
        Clinic = 1,
        Doctors,
        Employees,
        PriceList,
        PreventiveMedicinePriceListItems,
        Clients,
        Patients,
        Medications,
        Diagnoses,
        Visits,
        OldRapidVetTreatments,
        DangerousDrugs,
        MedicalProcedures,
        Referrals,
        Suppliers,
        ExpenseGroups,
        Quotations,
        TelephoneDirectory,
        Calendars,
        Holidays,
        Attendance,
        PreventiveMedicineRecords,
        PreventiveMedicineTemplates,
        PreventiveMedicineReminders,
        LabsTemplates,
        LabResults,
        InvoiceAndRecipts,
        FollowUp,
        Expenses,
        UpdateClients,
        Finish

    }
}
