﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums.DataMigration
{
    public enum DataBases
    {
        doctors,
        rapidmed,
        pricelst,
        phnbook,
        Forms,
        Presence,
        medprob,
        trtnotes,
        Income,
        trtplans,
        applog,
        Holidays,
        archive,
        treatmnt,
        recalls
    }
}
