﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums.DataMigration
{
    public enum MigrationStageStatusEnum
    {
        Success, InProgress, Waiting, Failed, Aborted
    }
}
