﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Enums
{
    public enum PhoneTypeEnum
    {
        Mobile = 1, Home = 2, Work = 3, Other= 4,
        Fax = 5
    }
}
