﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RapidVet.Enums.Finances;
using System.Data;
using System.ComponentModel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text.html;

namespace RapidVet.Helpers
{
    public static class PrintHelper
    {
        private static readonly Object obj = new Object();
        public static string GetHTMLDoc(string LogoUrl, DataTable dt, string docCaption)
        {
            StringBuilder strHTML = new StringBuilder();
            lock (obj)
            {
                try
                {
                    strHTML.Append("<div style='direction:rtl;text-align:right;color:#5A3928;'>");

                    strHTML.Append("<div style='height:25px;'><h2>");
                    strHTML.Append(docCaption);
                    strHTML.Append("</h2></div>");

                    strHTML.Append("<div style='height:25px;'><h6>");
                    strHTML.Append(DateTime.Now.ToString("HH:mm:ss dd/MM/yyyy"));
                    strHTML.Append("</h6></div>");

                    strHTML.Append("<table border='1' cellpadding='0' cellspacing='0'>");
                    
                    strHTML.Append("<tr>");
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        strHTML.Append("<td style='text-align:center;padding:5px;background-color:#8AC119;' >");
                        strHTML.Append(dt.Columns[i].Caption);
                        strHTML.Append("</td>");
                    }
                    strHTML.Append("</tr>");

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strHTML.Append("<tr>");
                        for (int t = 0; t < dt.Columns.Count; t++)
                        {
                            strHTML.Append("<td style='padding:5px;'>");
                            strHTML.Append(dt.Rows[i][t].ToString());
                            strHTML.Append("</td>");
                        }
                        strHTML.Append("</tr>");
                    }

                    strHTML.Append("</table>");

                    strHTML.Append("/<div>");

                    return strHTML.ToString();
                }
                catch (Exception ex)
                {
                    return "ERROR";
                }
            }
            return "";
        }
    }
}
