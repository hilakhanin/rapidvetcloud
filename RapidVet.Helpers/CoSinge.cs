﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using CS_Buffer_Sign;

namespace RapidVet.Helpers
{
  public  static class CoSinge
    {
      public static string Singe(string docContent , string coSingeUser , string coSingePassword )
      {
          if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(@"\SingedDocs")))
          {
              System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(@"\SingedDocs"));
          }
          var guid = Guid.NewGuid();
          var path = System.Web.HttpContext.Current.Server.MapPath(@"\SingedDocs\" + guid.ToString());
          System.IO.Directory.CreateDirectory(path);
          var htmlPath = path + @"\index.html";
          var p7bPath = path + @"\signature.p7b";

          try
          {

              //Create the file to singe
              System.IO.File.WriteAllText(htmlPath, docContent, Encoding.UTF8);
              //Get file Stream
              using (Stream dataToSign = File.OpenRead(htmlPath),
                            Signature = new MemoryStream())
              {
                  //Sign Stream data
                  SAPIWrapper.SignStream(
                      coSingeUser,
                      "",
                      coSingePassword,
                      dataToSign, Signature);

                  //Close input stream
                  dataToSign.Close();
                  File.WriteAllBytes(p7bPath,
                      (Signature as MemoryStream).ToArray());
                  Signature.Close();
              }

              //use the cosinge server to singe the files

          }
          catch (Exception)
          {
              return string.Empty;
          }

          return guid.ToString();
      }

    }
}
