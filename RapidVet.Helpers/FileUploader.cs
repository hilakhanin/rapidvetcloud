﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.WindowsAzure.StorageClient;
using RapidVet.Model;
using Microsoft.WindowsAzure;

namespace RapidVet.Helpers
{
    public enum BlobName
    {
        ClinicGroups, Clinic, Patient
    }

    public interface IFileUploader
    {
        OperationStatus Upload(HttpPostedFileBase file, int id, BlobName name);
    }

    public  class FileUploader : IFileUploader
    {
        /// <summary>
        /// uploads file to storage account
        /// </summary>
        /// <param name="file">file to be uploaded</param>
        /// <param name="id">pateint id</param>
        /// <param name="name">blob name</param>
        /// <returns>OperationStatus</returns>
        public  OperationStatus Upload(HttpPostedFileBase file, int id, BlobName name)
        {
            var status = new OperationStatus()
                                         {
                                             Success = true
                                         };

            var fileName = Path.GetFileName(file.FileName);
            var storageAccount = new CloudStorageAccount
                (new StorageCredentialsAccountAndKey("muzetest", "FHdANApoWXqUhhZqi1C4XQlJehCwCkce6AlwUna0Wjh2ZIZAJPp82eKHGx/jwPAO3A4wn/uVhWQg7jaRfWefdg=="), true);

            var blobClient = storageAccount.CreateCloudBlobClient();

            var container = blobClient.GetContainerReference(name.ToString());

            container.CreateIfNotExist();


            var blobName = id + "_" + Path.GetExtension(fileName);

            var blob = container.GetBlobReference(blobName);

            blob.Properties.ContentType = file.ContentType;

            try
            {
                blob.UploadFromStream(file.InputStream);

            }
            catch (Exception ex)
            {

                status = OperationStatus.CreateFromException("could not store file in blob", ex);
            }

            return status;
        }
    }
}
