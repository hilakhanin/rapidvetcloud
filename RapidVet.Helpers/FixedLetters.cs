﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Resources;
using RapidVet.Model.ClinicGroups;

namespace RapidVet.Helpers
{
    public static class FixedLetters
    {

        public static List<TemplateKeyWords> TemplateKeyWords { get; set; }

        private static string getTemplateKeyWord(string keyword)
        {
            return TemplateKeyWords.Single(t => t.TemplateKeyWord.Equals(keyword)).KeyWord;
        }


        //טופס עיקור-סירוס
        public static string SterilizationLetterTemplate()
        {
            var sb = new StringBuilder();
            sb.Append("<div class=\"container\">");
            //header
            sb.Append(DocumentHeader());
            //header end

            sb.Append("<div class=\"row\">");
            sb.Append("<div class=\"span12\">");
            sb.Append("<br/><br/><br/><br/><br/><br/>");
            sb.Append("<h2 style=\"text-align: center;\">אישור וטרינרי -עיקור/סירוס כלבים/חתולים</h2>");

            sb.Append("<p>");
            sb.AppendFormat("  החמ {0} מס' רשיון {1} <br/>", getTemplateKeyWord("ActiveDoctorName"), getTemplateKeyWord("ActiveDoctorLicence"));
            sb.AppendFormat("כתובת המרפאה: {0} <br/>", getTemplateKeyWord("ClinicAddress"));
            sb.AppendFormat("מצהיר בזאת כי בתאריך {0} {1} של ה{2}", getTemplateKeyWord("Date"),
                            getTemplateKeyWord("SterilizationAction"), getTemplateKeyWord("PatientAnimalKind"));
            sb.Append("</p>");

            sb.Append("<p>");
            sb.AppendFormat("שם הבעלים: {0}  <br/>", getTemplateKeyWord("ClientName"));
            sb.AppendFormat("ת.ז: {0}  <br/>", getTemplateKeyWord("ClientIdCard"));
            sb.AppendFormat("כתובת: {0}, סלולארי: {1}", getTemplateKeyWord("ClientAddress"), getTemplateKeyWord("ClientMobilePhone"));
            sb.Append("</p>");

            sb.Append("<p>");
            sb.AppendFormat("   שם ה{0}: {1}, תאריך הלידה: {2} <br/>", getTemplateKeyWord("PatientAnimalKind"),
                            getTemplateKeyWord("PatientName"), getTemplateKeyWord("PatientBirthDate"));
            sb.AppendFormat(" גזע: {0}, צבע: {1}, מין: {2}  <br/>", getTemplateKeyWord("PatientRace"), getTemplateKeyWord("PatientColor"),
                            getTemplateKeyWord("PatientSex") );
            sb.AppendFormat(" מספר שבב: {0}  <br/>", getTemplateKeyWord("PatientChip"));
            sb.AppendFormat("רשות מקומית: {0}  <br/>", getTemplateKeyWord("RegionalCoucil"));
            sb.AppendFormat("סימנים מיוחדים: {0}  <br/>", getTemplateKeyWord("PatientFeatures"));
            sb.AppendFormat("הערות: {0}", getTemplateKeyWord("Comment"));
            sb.Append("</p>");

            sb.Append("<p>");
            sb.AppendFormat("תאריך: {0}", getTemplateKeyWord("Date"));
            sb.Append("&nbsp;&nbsp;");
            sb.Append(" חתימה:___________________    חותמת:___________________");
            sb.Append("</p>");

            sb.Append("<p>");
            sb.AppendFormat(" העתק: ל.ו {0}", getTemplateKeyWord("Copies"));
            sb.Append("</p>");
            sb.Append("</div></div></div>");
            return sb.ToString();
        }


        //רשיון אחזקת כלב - תקנות חדשות
        public static string DogOwnerLicense()
        {
            var sb = new StringBuilder();
            sb.Append("<div class=\"container\">");

            //header
            sb.Append(DocumentHeader());
            //header end
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");           
            sb.Append("<br/><br/>");
            //title
            sb.Append("<p style='text-align: center;'><span style='font-size:20px;'><strong>");
            sb.AppendFormat("רשיון מספר {0} להחזקת כלב לשנת {1} &nbsp; ", getTemplateKeyWord("DogOwnerFormNumber"),
                            getTemplateKeyWord("DodOwnerFormYear"));
            sb.Append("</strong></span><span style=\"font-weight: normal; font-size: 12px;\">טופס 2 תקנה 13א'</span></p>");
          
            //sb.Append("<h2 style=\"text-align: center;\">");
            //sb.AppendFormat("רשיון מספר {0} להחזקת כלב לשנת {1} &nbsp; ", Resources.TemplateKeyWord.DogOwnerFormNumber,
            //                Resources.TemplateKeyWord.DodOwnerFormYear);
            //sb.Append("<span style=\"font-weight: normal; font-size: 12px;\">טופס 2 תקנה 13א'</span></h2>");
            
            //section 1
            sb.Append("<div class=\"row\">");
            sb.Append("<div class=\"span12\">");
            sb.Append("  1. פרטי בעל הכלב והצהרת בעליו:");
            sb.Append(@"<table class='table table-bordered'  style='width:100%'>");
            sb.Append("<tr>");
            sb.Append("<th class='tableCellCenterAlign'>שם פרטי ושם משפחה</th>");
            sb.Append("<th class='tableCellCenterAlign'>מס&#39; ת.זהות או דרכון</th>");
            sb.Append("<th class='tableCellCenterAlign'>מס&#39; טלפון</th>");
            sb.Append("<th class='tableCellCenterAlign'>כתובת</th>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("ClientName"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("ClientIdCard"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("ClientMobilePhone"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("ClientAddress"));
            sb.Append("</tr></table>");
            sb.Append(
                "<p>אני מצהיר כי הפרטים הרשומים לעיל נכונים, וכי הכלב לא נשך אדם או בעל חיים בעשרת (10) הימים האחרונים.</p>");
            sb.Append("</div></div>");

            sb.Append("<div class=\"row\">");
            sb.Append(@"<table style='border-collapse:collapse; border:none; width:100%;margin-bottom:10px'>");
            sb.Append("<tbody>");
            sb.Append("<tr>");
            sb.AppendFormat("<th class='tableCellNoBorder tableCellCenterAlign' style='font-weight: normal;'><u>{0}</u></th>", getTemplateKeyWord("Date"));
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'>&nbsp; &nbsp; &nbsp;</th>");
            sb.AppendFormat("<th class='tableCellNoBorder tableCellCenterAlign' style='font-weight: normal;'><u>{0}</u></th>", getTemplateKeyWord("ClientName"));
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</th>");
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</th>");
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'>________</th>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp; תאריך</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>שם המצהיר</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>חתימה</td>");
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("</div>");
           
            //sb.Append("<div class=\"row\">");
            //sb.Append("<div class=\"span12\">");
            //sb.AppendFormat("<div class=\"span4\"><span style=\"text-decoration: underline\">{0}</span><br/>תאריך</div>", Resources.TemplateKeyWord.Date);
            //sb.AppendFormat(
            //    "<div class=\"span3\">  <span style=\"text-decoration: underline\">{0}</span><br/>שם המצהיר</div>", Resources.TemplateKeyWord.ClientName);
            //sb.Append("<div class=\"span4\">  <span>__________</span><br/>חתימה</div>");
            //sb.Append("</div></div>");

            //sb.Append("<br /><br />");

            //-------------------section 1 end

            //section 2
            sb.Append("<div class=\"row\"><span style='line-height:1.6'>");
            sb.Append("2. פרטי הכלב");
            sb.Append("</span></div>");
            sb.Append("<div class=\"row\">");
            sb.Append("<div class=\"span12\">");
            sb.Append("<table class=\"table table-bordered\" style='width:100%'>");
            sb.Append("<tbody>");
            sb.Append("<tr>");
            sb.Append("<th class='tableCellCenterAlign'>שם הכלב</th>");
            sb.Append("<th class='tableCellCenterAlign'>גזע</th>");
            sb.Append("<th class='tableCellCenterAlign'>צבע</th>");
            sb.Append("<th class='tableCellCenterAlign'>מין (זכר/נקבה)</th>");
            sb.Append("<th class='tableCellCenterAlign'>מעוקר/מסורס</th>");
            sb.Append("<th class='tableCellCenterAlign'>תאריך לידה</th>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("PatientName"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("PatientRace"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("PatientColor"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("PatientSex"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("Sterilization"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("PatientBirthDate"));
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<th style='text-align:center; vertical-align:middle'>מס&#39; שבב</th>");
            sb.Append("<th colspan='3' style='text-align:center; vertical-align:middle'>מס&#39; קעקוע או סימון מיוחד אחר</th>");
            sb.Append("<th class='tableCellCenterAlign'>כלב נחייה</th>");
            sb.Append("<th class='tableCellCenterAlign'>כלב מסוכן</th>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("PatientChip"));
            sb.AppendFormat("<td colspan=\"3\" class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("PatientFeatures"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("SeeingEyeDog"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("PatientDangerous"));
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("</div></div>");
            //-----------------------section 2 end
            sb.Append("<div class='row'>&nbsp;</div>");
            //section 3
            sb.Append("<div class=\"row\"><span style='line-height:1.6'>");
            sb.Append("3. אישור סימון בשבב (יש לסמן את החלופה המתאימה ביותר)");
            sb.Append("</span></div>");
            sb.Append("<div class=\"row\">");
            sb.Append("<div class=\"span12\">");
           // sb.Append("<br />");
            sb.Append("&nbsp; &nbsp;א. בדקתי את הכלב ומצאתי שהוא מסומן בשבב שפרטיו רשומים לעיל:<br />");
            sb.Append("&nbsp; &nbsp;ב. מכיוון שלפי בדיקתי הכלב לא היה מסומן. סימנתי אותו בשבב שפרטיו מסומנים לעיל:");
            sb.Append("<table style='border-collapse:collapse; border:none; width:100%;margin-bottom:10px;margin-top:10px'>");
            sb.Append("<tbody>");
            sb.Append("<tr>");
            sb.AppendFormat("<th style='border:none;text-align:center; vertical-align:middle;font-weight: normal;' ><u>{0}</u></th>", getTemplateKeyWord("Date"));
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'</th>");
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'</th>");
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'</th>");
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'</th>");
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'><strong><u>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</u></strong></th>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp; תאריך</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>חתימה וחותמת המסמן</td>");
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("</div></div>");

            //-------------------------------section 3 end

            //section 4
            sb.Append("<div class=\"row\"><div class=\"span12\">");
            sb.Append(" 4. אישור חיסון נגד מחלת הכלבת");
            sb.Append("<table class=\"table table-bordered\" style='margin-bottom:10px;width:100%;'>");
            sb.Append("<tbody>");
            sb.Append("<tr>");
            sb.Append("<th class='tableCellCenterAlign'>תאריך החיסון</th>");
            sb.Append("<th class='tableCellCenterAlign'>שם הרופא המחסן</th>");
            sb.Append("<th class='tableCellCenterAlign'>מס&#39; רשיון</th>");
            sb.Append("<th class='tableCellCenterAlign'>חתימה וחותמת המחסן</th>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("LastRabiesVaccineDate"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("LastRabiesVaccineDoctor"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("LastRabiesVaccindDoctorLicense"));
            sb.Append("<td></td>");
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("</div></div>");
         
            sb.Append("<table style='border-collapse:collapse; border:none; width:100%'>");
            sb.Append("<tbody>");
            sb.Append("<tr>");
            sb.AppendFormat("<th style='border:none;text-align:center; vertical-align:middle;font-weight: normal;'><u>{0}</u></th>", getTemplateKeyWord("Date"));
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'</th>");
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'</th>");
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'</th>");
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'</th>");
            sb.Append("<th class='tableCellNoBorder tableCellCenterAlign'><strong><u>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</u></strong></th>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp; תאריך</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder tableCellCenterAlign'>שם הרופא המחסן</td>");
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");

            //----------------------------------section 4 end

            //section 5
            sb.Append("<div class=\"row\"><div class=\"span12\">");
            sb.Append(" 5. אגרה");
            sb.Append("<table class=\"table table-bordered\" style='margin-bottom:10px;width:100%;'>");
            sb.Append("<tbody>");
            sb.Append("<tr>");
            sb.Append("<th class='tableCellCenterAlign'>סכום האגרה</th>");
            sb.Append("<th class='tableCellCenterAlign'>לתשלום עד</th>");
            sb.Append("<th class='tableCellCenterAlign'>אישור התשלום וחותמת</th>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("DogOwnerFormTollAmount"));
            sb.AppendFormat("<td class='tableCellCenterAlign'>{0}</td>", getTemplateKeyWord("DogOwnerFormPayThrough"));
            sb.Append("<td></td></tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("</div></div>");
            //---------------------section 5 end
          
            //footer
            sb.Append("<div><strong>* רשיון זה בר תוקף רק אם התייקמו בו התנאים הבאים:</strong></div>");
            sb.Append("<div>1.&nbsp;הוטבעה בו חותמת הבנק/קופת הרשות המקומית.</div>");
            sb.Append("<div>2. מסמך בהגדרתו בחוק להסדרת הפיקוח על כלבים, התשס&#39;&#39;ג - 2002 אישר את דבר סימון הכלב.</div>");
            sb.Append("<div>3.&nbsp;רופא וטרינר מחסן מורשה אישר בחתימתו כי חיסן את הכלב בהתאם לפקודת הכלבת.</div>");
            //------------------------footer end

            //document end
            sb.Append("</div>");


            return sb.ToString();
        }

        //אישור על חיסון כלבת
        public static string RabiesPermit()
        {
            var sb = new StringBuilder();
            sb.Append("<div class=\"container\">");

            //header
            sb.Append(DocumentHeader());
            //header end
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            //title
            sb.Append("<p style='text-align: center;'><span style='font-size:20px;'><strong><u>");
            sb.Append("אישור על חיסון כלבת");
            sb.Append("</u></strong></span></p>");

            //sb.Append("<h2 style=\"text-align: center;\">");
            //sb.AppendFormat("רשיון מספר {0} להחזקת כלב לשנת {1} &nbsp; ", Resources.TemplateKeyWord.DogOwnerFormNumber,
            //                Resources.TemplateKeyWord.DodOwnerFormYear);
            //sb.Append("<span style=\"font-weight: normal; font-size: 12px;\">טופס 2 תקנה 13א'</span></h2>");

            //section 1
            sb.Append("<div class=\"row\">");
            sb.Append("<div class=\"span12\">");
            sb.Append(@"<table align='right' style='border-collapse:collapse; border:none; width:60%;margin-bottom:10px'>");
            sb.Append("<tr>");
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>שם הבעלים:</td>");
            sb.AppendFormat("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>{0}</td>", getTemplateKeyWord("ClientName"));
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>סוג:</td>");
            sb.AppendFormat("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>{0}</td>", getTemplateKeyWord("PatientAnimalKind"));
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>ת.ז:</td>");
            sb.AppendFormat("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>{0}</td>", getTemplateKeyWord("ClientIdCard"));
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>רחוב ומספר:</td>");
            sb.AppendFormat("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>{0}</td>", getTemplateKeyWord("ClientStreetAndNumber"));
            sb.Append("</tr>");            
            sb.Append("<tr>");
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>יישוב:</td>");
            sb.AppendFormat("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>{0}</td>", getTemplateKeyWord("ClientCity"));
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>צבע:</td>");
            sb.AppendFormat("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>{0}</td>", getTemplateKeyWord("PatientColor"));
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>טלפון:</td>");
            sb.AppendFormat("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>{0}</td>", getTemplateKeyWord("ClientMobilePhone"));
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>מין:</td>");
            sb.AppendFormat("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>{0}</td>", getTemplateKeyWord("PatientSex"));
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>תאריך לידה:</td>");
            sb.AppendFormat("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>{0}</td>", getTemplateKeyWord("ClientBirthdate"));
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>מס' שבב:</td>");
            sb.AppendFormat("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>{0}</td>", getTemplateKeyWord("PatientChip"));
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>מס' ס.ג.י.ר:</td>");
            sb.AppendFormat("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>{0}</td>", getTemplateKeyWord("PatientSagirNumber"));
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>&nbsp;</td>");
            sb.Append("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>תאריך לידה:</td>");
            sb.AppendFormat("<td class='tableCellNoBorder' style='text-align: right; vertical-align: middle;'>{0}</td>", getTemplateKeyWord("PatientBirthDate"));
            sb.Append("</tr>");   
            sb.Append("</table>");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            sb.Append("<br/>");
            sb.AppendFormat("הריני לאשר כי בתאריך: {0}",getTemplateKeyWord("LastRabiesVaccineDate"));
            sb.Append(" חיסנתי את הבע\"ח שפרטיו רשומים מעלה חיסון נגד מחלת הכלבת. ");
            sb.AppendFormat("מספר אצווה: {0} ", getTemplateKeyWord("LastRabiesBatchNumber"));
            sb.Append("<br/><br/>");

            sb.Append("<div class=\"row\">");
            sb.Append("<div class=\"span12\">");
            sb.Append(@"<table class='table table-bordered'  style='width:100%'>");            
            sb.Append("<tbody>");
            sb.Append("<tr>");
            sb.AppendFormat("<td class='tableCellCenterAlign' style='font-weight: normal;'><u>{0}</u></td>", getTemplateKeyWord("ActiveDoctorName"));
            sb.AppendFormat("<td class='tableCellCenterAlign' style='font-weight: normal;'><u>{0}</u></td>", getTemplateKeyWord("ActiveDoctorLicence"));
            sb.Append("<td class='tableCellCenterAlign'><br/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</td>");            
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='tableCellCenterAlign'>שם הרופא</td>");
            sb.Append("<td class='tableCellCenterAlign'>מס' רישיון</td>");
            sb.Append("<td class='tableCellCenterAlign'>חתימה וחותמת</td>");            
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("<br />");
            sb.Append("<hr />");
           
            //section 2
            sb.Append("<strong><u>הוראות לבעלי הכלב:</u></strong><br />");
            sb.Append("1. כל כלב מגיל 3 חודשים חייב לקבל חיסון נגד כלבת ולהוציא רישיון אחזקה.<br />");
            sb.Append("2. כלב במקום ציבורי חייב להיות קשור ברצועה ומחסום לפיו.<br />");
            sb.Append("3. במקרה נשיכה להודיע תוך 24 שעות ללשכת הבריאות/לרופא הוטרינרי הרשותי ולהסגירו בתחנת הסגר.<br />");
            sb.Append("4. ראש הראשות רשאי סרב לתת רישיון או לבטלו במקרים אלו:");
            sb.Append("<div style='margin-right: 40px;'>א. הכלב הוא בעל מזג פראי.<br />");
            sb.Append("ב. הכלב מהווה סכנה לביטחון הציבור.<br />");
            sb.Append("ג. הכלב לא קיבל זריקת חיסון נגד כלבת.<br />");
            sb.Append("ד. הכלב מקים רעש גדול שהוא מפגע לשכנים.<br />");
            sb.Append("ה. הכלב מוחזק בניגוד להוראות 2,3.</div>");
            sb.Append("<div>5. חובה להודיע לרשות אם הכלב אינו יותר ברשות הבעלים.<br />");
            sb.Append("<span style='margin-right: 15px;'>(מתוך חוק עזר - פיקוח על כלבים).</span></div>");
            sb.Append("</div>");
           

            //document end
           // sb.Append("</div>");


            return sb.ToString();
        }

        //סיכום ניתוח
        public static string MedicalProcedureSummery()
        {
            var sb = new StringBuilder();

            sb.Append(DocumentHeader());
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            //-----------document title----------
            sb.AppendFormat("<h2 style=\"text-align: center;\">דו\"ח {0}</h2>", getTemplateKeyWord("MedicalProcedureType"));
            sb.AppendFormat("<h5 style=\"text-align: left;\">תאריך הדפסה: {0}</h5>", getTemplateKeyWord("Date"));
            sb.Append("<h3 style=\"text-align: center; text-decoration: underline\">");
            sb.AppendFormat(" הנדון: {0},", getTemplateKeyWord("PatientName"));
            sb.AppendFormat(" בבעלות: {0} ", getTemplateKeyWord("ClientName"));
            sb.AppendFormat("ת.ז: {0} ", getTemplateKeyWord("ClientIdCard"));
            sb.Append("<br />");
            sb.AppendFormat("{0}", getTemplateKeyWord("ClientAddress"));
            sb.AppendFormat("  טל': {0} ", getTemplateKeyWord("ClientHomePhone"));
            sb.Append("</h3>");
            //------- title end----------------

            sb.Append("<table style=\"width: 100%;border:none;\">");

            //---------diagnosis
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"><strong>אבחנה</strong></td>");
            sb.AppendFormat("<td style=\"width: 88%;border: none;\">{0}</td>", getTemplateKeyWord("MedicalProcedureDiagnosis"));
            sb.Append("</tr>");

            //---------separator
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"></td>");
            sb.Append("<td style=\"width: 88%;border: none;\"></td>");
			sb.Append("</tr>");

            //---------procedure name
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"><strong>הניתוח</strong></td>");
            sb.AppendFormat("<td style=\"width: 88%;border: none;\">{0}</td>", getTemplateKeyWord("MedicalProcedureName"));
            sb.Append("</tr>");

            //---------separator
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"></td>");
            sb.Append("<td style=\"width: 88%;border: none;\"></td>");
            sb.Append("</tr>");

            //--------- procedure location
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"><strong>מקום הניתוח</strong></td>");
            sb.AppendFormat("<td style=\"width: 88%;border: none;\">{0}</td>", getTemplateKeyWord("MedicalProcedureLocation"));
            sb.Append("</tr>");

            //---------separator
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"></td>");
            sb.Append("<td style=\"width: 88%;border: none;\"></td>");
            sb.Append("</tr>");

            //---------procedure time
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"><strong>תאריך הניתוח</strong></td>");
            sb.Append("<td style=\"width: 88%;border: none;\">");
            sb.AppendFormat("{0}", getTemplateKeyWord("MedicalProcedureDate"));
            sb.Append("<br/>");
            sb.AppendFormat("משעה {0} ", getTemplateKeyWord("MedicalProcedureStartTime"));
            sb.AppendFormat("ועד {0} ", getTemplateKeyWord("MedicalProcedureEndTime"));
            sb.Append("</td>");
            sb.Append("</tr>");

            //---------separator
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"></td>");
            sb.Append("<td style=\"width: 88%;border: none;\"></td>");
            sb.Append("</tr>");

            //---------surgeon
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"><strong>רופא מנתח</strong></td>");
            sb.AppendFormat("<td style=\"width: 88%;border: none;\">{0}</td>", getTemplateKeyWord("MedicalProcedureSurgeonName"));
            sb.Append("</tr>");

            //---------anasthesiologist
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"><strong>רופא מרדים</strong></td>");
            sb.AppendFormat("<td style=\"width: 88%;border: none;\">{0}</td>", getTemplateKeyWord("MedicalProcedureAnasthesiologistName"));
            sb.Append("</tr>");

            //---------nurse
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"><strong>אחות</strong></td>");
            sb.AppendFormat("<td style=\"width: 88%;border: none;\">{0}</td>", getTemplateKeyWord("MedicalProcedureNurseName"));
            sb.Append("</tr>");

            //---------additional personnel
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"><strong>עוזר נוסף</strong></td>");
            sb.AppendFormat("<td style=\"width: 88%;border: none;\">{0}</td>", getTemplateKeyWord("MedicalProcedureAdditionalPersonnel"));
            sb.Append("</tr>");

            //---------separator
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"></td>");
            sb.Append("<td style=\"width: 88%;border: none;\"></td>");
            sb.Append("</tr>");

            //---------summery
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 12%;border: none;\"><strong>סיכום</strong></td>");
            sb.AppendFormat("<td style=\"width: 88%;border: none;\">{0}</td>", getTemplateKeyWord("Summery"));
            sb.Append("</tr>");

            sb.Append("</table>");


            return sb.ToString();
        }

        //מכתב שחרור
        public static string DischargePaper()
        {
            var sb = new StringBuilder();

            sb.Append(DocumentHeader());
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            //-----title--------------
            sb.Append("<h2 style=\"text-align: center;\">מכתב שחרור</h2>");
            sb.AppendFormat("<h5 style=\"text-align: left;\">תאריך : {0}</h5>", getTemplateKeyWord("Date"));
            sb.AppendFormat("<h3>לכבוד: {0}</h3>", getTemplateKeyWord("DischargePaperTo"));
            sb.AppendFormat("<h3>מאת: {0}</h3>", getTemplateKeyWord("DischargePaperFrom"));
            sb.Append("<h3 style=\"text-align: center; text-decoration: underline\">");
            sb.AppendFormat("הנדון: {0},", getTemplateKeyWord("PatientName"));
            sb.Append("גיל: ");
            sb.AppendFormat("ש:{0}  ", getTemplateKeyWord("PatientAgeYears"));
            sb.AppendFormat("ח:{0}", getTemplateKeyWord("PatientAgeMonths"));
            sb.Append("<br />");
            sb.AppendFormat("שם הבעלים: {0}", getTemplateKeyWord("ClientName"));
            sb.Append("<br />");
            sb.AppendFormat("{0},", getTemplateKeyWord("ClientAddress"));
            sb.AppendFormat("  טל': {0}", getTemplateKeyWord("ClientHomePhone"));
            sb.Append("</h3>");
           
            //-------medical summery-------
            sb.Append("<div>");
            sb.Append("<p><strong>סיכום רפואי</strong><br />");
            sb.AppendFormat("{0}", getTemplateKeyWord("Summery"));
            sb.Append("</p>");
            sb.Append("</div>");
            
            //-------medical background----
            sb.Append("<div>");
            sb.Append("<p><strong>רקע רפואי</strong><br />");
            sb.AppendFormat("{0}", getTemplateKeyWord("DischargePaperBackground"));
            sb.Append("</p>");
            sb.Append("</div>");
           
            //------recommendations--------
            sb.Append("<div>");
            sb.Append("<p><strong>המלצות להמשך טיפול</strong><br />");
            sb.AppendFormat("{0}", getTemplateKeyWord("DischargePaperRecommendations"));
            sb.Append("</p>");
            sb.Append("</div>");
           
            //-----checkup------
            sb.Append("<div>");
            sb.Append("<p><strong>תאריך ביקורת: ");
            sb.AppendFormat("{0}", getTemplateKeyWord("DischargePaperCheckup"));
            sb.Append("</strong></p>");
            sb.Append("</div>");

            //------signiture-------
            sb.Append("<div style=\"text-align: center;\">");
            sb.Append("בברכה,");
            sb.Append("<br />");
            sb.AppendFormat("{0}", getTemplateKeyWord("DoctorName"));
            sb.Append("</div>");

            return sb.ToString();
        }

        public static string BillOfHealthHe()
        {
            var sb = new StringBuilder();

            sb.Append(DocumentHeader());
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            //-----title--------------
            sb.Append("<h1 style=\"text-align: center;\">אישור בריאות וטרינרי</h1>");
           

            //-----content--------------
            sb.Append("<div>");
            sb.AppendFormat("אני הח\"מ {0} ", getTemplateKeyWord("DoctorName"));
            sb.AppendFormat("מס' רשיון {0}",getTemplateKeyWord("ActiveDoctorLicence"));
            sb.Append("<br/>");
            sb.AppendFormat("מצהיר בזאת כי בתאריך {0} ", getTemplateKeyWord("Date"));
            sb.AppendFormat("בדקתי את ה{0} ", getTemplateKeyWord("PatientAnimalKind"));
            sb.AppendFormat("{0} ", getTemplateKeyWord("PatientName"));
            sb.AppendFormat("מגזע {0} ", getTemplateKeyWord("PatientRace"));
            sb.AppendFormat("מספר שבב {0}", getTemplateKeyWord("PatientChip"));
            sb.Append("<br/>");
            sb.AppendFormat("תאריך חיסון כלבת אחרון: {0}", getTemplateKeyWord("LastRabiesVaccineDate"));
            sb.Append("<br/>");
            sb.AppendFormat("השייך ל{0} ",getTemplateKeyWord("ClientName"));
            sb.AppendFormat("ת.ז {0} ",getTemplateKeyWord("ClientIdCard"));
            sb.AppendFormat("מ{0}",getTemplateKeyWord("ClientAddress"));
            sb.Append("<br/><br/>");
            sb.Append("ומצאתי כי החיה בריאה ונקיה מכל סימני מחלה");
            sb.Append("</div>");

            //------signiture-------
            sb.Append("<br/><br/>");
            sb.Append("<div style=\"text-align: right;\">");
            sb.Append("בברכה,");
            sb.Append("<br />");
            sb.AppendFormat("{0}", getTemplateKeyWord("DoctorName"));
            sb.Append("<br />");
            sb.AppendFormat("{0}", getTemplateKeyWord("ClinicAddress"));
            sb.Append("<br />");
            sb.AppendFormat("מס' רשיון: {0}", getTemplateKeyWord("ActiveDoctorLicence"));
            sb.Append("</div>");


            return sb.ToString();
        }

        public static string BillOfHealthEn()
        {
            var sb = new StringBuilder();

            sb.Append(DocumentHeader());
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            sb.Append("<br/><br/>");
            //-----title--------------
            sb.Append("<h1 style=\"text-align: center;\">Health Certificate</h1>");            

            //-----content--------------
            sb.Append("<div style=\"direction: ltr;\">");
            sb.Append("On the date of issue of this certificate of health");
            sb.Append("<br />");
            sb.AppendFormat("I examined the {0} ", getTemplateKeyWord("PatientAnimalKind"));
            sb.AppendFormat("of race {0} ", getTemplateKeyWord("PatientRace"));
            sb.AppendFormat("named {0} ", getTemplateKeyWord("PatientName"));
            sb.AppendFormat("owned by {0} ", getTemplateKeyWord("ClientName"));
            sb.AppendFormat("id {0} ", getTemplateKeyWord("ClientIdCard"));
            sb.AppendFormat("from {0} ", getTemplateKeyWord("ClientAddress"));
            sb.Append("<br />");
            sb.AppendFormat("Electronic chip number: {0}", getTemplateKeyWord("PatientChip"));
            sb.Append("<br />");
            sb.AppendFormat("Last rabies vaccine date: {0}", getTemplateKeyWord("LastRabiesVaccineDate"));
            sb.Append("<br />");
            sb.Append("and found no signs or symptoms of any contagious or infectious disease.");
            sb.Append("</div>");

            //------signiture-------
            sb.Append("<br/><br/>");
            sb.Append("<div style=\"text-align: left;\">");
            sb.Append("<br />");
            sb.AppendFormat("Date: {0}", getTemplateKeyWord("Date"));
            sb.Append("<br />");
            sb.AppendFormat("{0}", getTemplateKeyWord("DoctorName"));
            sb.Append("<br />");
            sb.AppendFormat("License No. {0}", getTemplateKeyWord("ActiveDoctorLicence"));
            sb.Append("<br />");
            sb.AppendFormat("{0}", getTemplateKeyWord("ClinicAddress"));
            sb.Append("</div>");

            return sb.ToString();
        }

        private static string DocumentHeader()
        {
            var sb = new StringBuilder();
            sb.Append("<div style=\"width: 100%; margin-right: auto; margin-left: auto;\">");
                sb.Append("<div style=\"float:left; text-align: left;\">");
                sb.AppendFormat("{0}", getTemplateKeyWord("ClinicLogo"));
                sb.Append("</div>");
            //sb.Append("<div class=\"row\">");
            sb.Append("<div style=\"float: right; text-align: right;\">");
            sb.AppendFormat("<p><b>{0}</b><br />", getTemplateKeyWord("ClinicName"));           
            sb.AppendFormat("{0}<br />", getTemplateKeyWord("ClinicAddress"));
            sb.AppendFormat("טל: {0} <br/>", getTemplateKeyWord("ClinicPhone"));
            sb.AppendFormat("פקס: {0} <br/>", getTemplateKeyWord("ClinicFax"));
            sb.AppendFormat("{0}</p><br />", getTemplateKeyWord("ClinicEmail"));
           // sb.Append("</div>");
           // sb.Append("<div style=\"width: 40%; float: left; text-align: left\">");
            sb.Append("</div></div></div>");
            return sb.ToString();
        }

        public static string EmptyTemplate()
        {
            var sb = new StringBuilder();
            sb.Append("<div class=\"container\">");            
            sb.Append(DocumentHeader());            

            sb.Append("</div>");

            return sb.ToString();
        }


    }
}
