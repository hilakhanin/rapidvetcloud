﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Helpers
{
    public static class PatientHelper
    {
        public static string CalculateAge(DateTime? birthDate)
        {
            if (!birthDate.HasValue)
                return "0";

            int years;
            int month;
            calcYM(birthDate, out years, out month);
            return month >= 12 || month <= 0 ? years.ToString() : String.Format("{0}.{1}", years, month);
        }

        public static decimal CalculateAge(DateTime? birthDate, int decimals)
        {
            if (!birthDate.HasValue)
                return 0;

            int years;
            int month;
            calcYM(birthDate, out years, out month);
            return Math.Round(years + (month >= 12 || month <= 0 ? (decimal)0 : (decimal)(month / 12)), decimals);
        }

        private static void calcYM(DateTime? birthDate, out int years, out int month)
        {
            DateTime now = DateTime.Now;
            years = now.Year - birthDate.Value.Year;
            month = now.Month - birthDate.Value.Month;
            if (month >= 0)
                return;

            years--;
            month = 12 + month;
        }
    }
}
