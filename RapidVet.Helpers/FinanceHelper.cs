﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RapidVet.Enums.Finances;
using System.Data;
using System.ComponentModel;

namespace RapidVet.Helpers
{
    public static class FinanceHelper
    {
        public static List<SelectListItem> CreditPaymentTypeOptions()
        {
            var rm = Resources.Enums.Finances.CreditPaymentType.ResourceManager;
            var creditOptions = new List<SelectListItem>();
            foreach (CreditPaymentType option in Enum.GetValues(typeof (CreditPaymentType)))
            {
                creditOptions.Add(new SelectListItem()
                    {
                        Selected = false,
                        Text = rm.GetString(option.ToString()),
                        Value = ((int)option).ToString()
                    });
            }
            return creditOptions;
        }

        public static List<SelectListItem> PaymentTypeOptions()
        {
            var rm = Resources.Enums.Finances.PaymentType.ResourceManager;
            var options = new List<SelectListItem>();
            foreach (PaymentType option in Enum.GetValues(typeof(PaymentType)))
            {
                options.Add(new SelectListItem()
                {
                    Selected = false,
                    Text = rm.GetString(option.ToString()),
                    Value = ((int)option).ToString()
                });
            }
            return options;
        }
        public static DataTable ToDataTable<T>(this IList<T> list)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in list)
            {
                for (int i = 0; i < values.Length; i++)
                    values[i] = props[i].GetValue(item) ?? DBNull.Value;
                table.Rows.Add(values);
            }
            return table;
        }
    }
}
