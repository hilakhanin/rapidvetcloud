﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Helpers
{
    public static class UiMessages
    {
        /// <summary>
        /// creates a message to be shows to user regarding success / failure of action preformed
        /// </summary>
        /// <param name="entity">tntity type (user, client, clinic..)</param>
        /// <param name="name">the entity name</param>
        /// <param name="operation">operation type (created, updated, deleted)</param>
        /// <param name="error">did the action succeed?</param>
        /// <returns>string</returns>
        public static string CreateMessage(EntityType entity, string name, OperationType operation, bool error = false)
        {
            string message = string.Empty;

            if (error)
            {
                message = string.Format(
                 Resources.OperationMessgaes.GenericMessage,
                 Resources.EntityType.ResourceManager.GetString(entity.ToString()),
                 name,
                 Resources.OperationType.ResourceManager.GetString(operation.ToString())
                 );
            }
            else
            {
                message = string.Format(
                    Resources.OperationMessgaes.GenericMessage,
                    Resources.EntityType.ResourceManager.GetString(entity.ToString()),
                    name,
                    Resources.OperationType.ResourceManager.GetString(operation.ToString())
                    );
            }
            return message;
        }


    }
}
