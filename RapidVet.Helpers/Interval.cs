﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Helpers
{
    public enum Interval
    {
        Min, Hour, Day, Week, Month, Year
    }
}
