﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Helpers
{
    public enum OperationType
    {
        Created, Updated, Deleted, Error
    }
}
