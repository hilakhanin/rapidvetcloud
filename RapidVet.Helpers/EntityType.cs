﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Helpers
{
    public enum  EntityType
    {
        ClinicGroup,
        Clinic,
        Issuer,
        PriceListCategory,
        AnimalColor,
        AnimalKind,
        User
    }
}
