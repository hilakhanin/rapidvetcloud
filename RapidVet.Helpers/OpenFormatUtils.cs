﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Helpers
{
    public static class OpenFormatUtils
    {
        public static string SystemConst
        {
            get { return "&OF1.31&"; }
        }

        public static string GetDecimalString(decimal number, int length)
        {


            var numberString = number.ToString("F").Replace(".", "");
            numberString = numberString.TrimStart('-');
            if (numberString.Length > length)
            {
                throw new Exception("תוכן חורג מהשדה המוקצה");
            }
            else
            {
                return number >= 0
                           ? "+" + numberString.PadLeft(length - 1, '0')
                           : "-" + numberString.PadLeft(length - 1, '0');
            }
        }

        public static string GetDecimalStringForHS(decimal number)
        {


            var numberString = Math.Abs(number).ToString("F");
            if (numberString.Length > 12)
            {
                throw new Exception("תוכן חורג מהשדה המוקצה");
            }
            else
            {
                return numberString.PadLeft(12);
            }
        }

        public static string GetIntString(int number, int length)
        {
            return GetIntString(number.ToString(), length);
        }

        public static string GetIntString(string number, int length)
        {
            if (string.IsNullOrEmpty(number))
            {
                return GetEmptyString(length, true);
            }
            var numberString = number.ToString();
            numberString = numberString.Replace("\r\n", " ");
            if (numberString.Length > length)
            {
                return numberString.Substring(0, length);
                //throw new Exception("תוכן חורג מהשדה המוקצה");
            }
            else
            {
                return numberString.PadLeft(length, '0');
            }
        }
        public static string GetAlphaString(int number, int length)
        {
            return GetAlphaString(number.ToString(), length);
        }

        public static string GetAlphaString(string alphaString, int length)
        {
            if (string.IsNullOrEmpty(alphaString))
            {
                return GetEmptyString(length, false);
            }
            else
            {
                alphaString = alphaString.Replace("\r\n", " ");
            }
            if (alphaString.Length > length)
            {
                return alphaString.Substring(0, length);
            }
            else
            {
                return alphaString.PadRight(length);
            }
        }

        public static string GetEmptyString(int length, bool numeric)
        {
            var s = "";
            return !numeric ? s.PadRight(length) : s.PadRight(length, '0');
        }

        public static string GetDate(DateTime date)
        {
            return date.Year + GetIntString(date.Month, 2) + GetIntString(date.Day, 2);
        }

        public static string GetDateForHS(DateTime date)
        {
            // string dateString = GetIntString(date.Day, 2) + GetIntString(date.Month, 2) + date.Year;
            return GetAlphaString(date.ToString("ddMMyy"), 10);
            //return GetIntString(date.Day, 2) + "/" + GetIntString(date.Month, 2) + "/" + date.Year;
        }

        public static string GetShortDateForHS(DateTime date)
        {
            return GetIntString(date.Day, 2) + "/" + GetIntString(date.Month, 2) + "/" + date.Year.ToString().Substring(2);
        }

        public static string GetIdNumber(string id)
        {
            if (id == null || id.Length != 9)
            {
                return "307497339";
            }
            return id;
        }

        public static string GetTime(DateTime date)
        {
            return GetIntString(date.Hour, 2) + GetIntString(date.Minute, 2);
        }

        //return the 15 number randome code
        public static string GetRandomeCode()
        {
            Random rnd = new Random();
            var builder = new StringBuilder();
            for (var i = 0; i < 15; i++)
            {
                int num = rnd.Next(1, 10);
                builder.Append(num);
            }
            return builder.ToString();
        }

        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public static string GetAlphaAndReverse(string s, int length)
        {
            string temp = "";

            if (!string.IsNullOrEmpty(s))
            {
                temp = Reverse(s);// Reverse(s);
            }

            return GetAlphaString(temp, length);
        }

        public static string GetQuantinty(decimal value)
        {
            if (value > 0)
            {
                return GetDecimalString(value, 15) + "00";//"+" + GetDecimalString(value, 12) + "0000";
            }
            else
            {
                return (GetDecimalString(value, 15) + "00").Replace("+", "-");
            }
        }
    }



}
