﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RapidVet.Enums.Finances;
using System.Data;
using System.ComponentModel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text.html;

namespace RapidVet.Helpers
{
    public static class PDFHelper
    {
        private static readonly Object obj = new Object();
        public static bool CreateDoc(string FileUrl, string LogoUrl, DataTable dt, string docCaption, string fontFile)
        {
            lock (obj)
            {
                try
                {
                    BaseFont bf = BaseFont.CreateFont(fontFile, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                    iTextSharp.text.Font fontHead = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.BOLD, WebColors.GetRGBColor("#5A3928"));
                    iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 11, iTextSharp.text.Font.BOLD, WebColors.GetRGBColor("#5A3928"));
                    iTextSharp.text.Font fontSmall = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.BOLD, WebColors.GetRGBColor("#5A3928"));

                    Document doc = new Document(PageSize.A4);
                    var output = new FileStream(FileUrl, FileMode.Create);
                    var writer = PdfWriter.GetInstance(doc, output);

                    doc.Open();

                    var logo = iTextSharp.text.Image.GetInstance(LogoUrl);
                    logo.SetAbsolutePosition(90, 785);
                    logo.ScaleAbsoluteHeight(20);
                    logo.ScaleAbsoluteWidth(20);
                    doc.Add(logo);


                    PdfPTable table2 = new PdfPTable(dt.Columns.Count);
                    table2.DefaultCell.Border = 0;
                    table2.WidthPercentage = 80;
                    table2.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                    PdfPCell cellHeadTiltle = new PdfPCell();

                    cellHeadTiltle.AddElement(new Paragraph(docCaption, fontHead));
                    cellHeadTiltle.AddElement(new Paragraph(DateTime.Now.ToString("HH:mm:ss dd/MM/yyyy"), fontSmall));

                    cellHeadTiltle.FixedHeight = 50.0f;
                    cellHeadTiltle.Colspan = dt.Columns.Count;

                    table2.AddCell(cellHeadTiltle);

                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        PdfPCell cellHead = new PdfPCell();

                        Paragraph p = new Paragraph(dt.Columns[i].Caption, fontHead);

                        p.Alignment = Element.ALIGN_CENTER; 

                        cellHead.AddElement(p);

                        cellHead.FixedHeight = 30.0f;

                        cellHead.BackgroundColor = WebColors.GetRGBColor("#8AC119");

                        table2.AddCell(cellHead);
                    }


                    //New Row Added
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            PdfPCell cellData = new PdfPCell();

                            cellData.AddElement(new Paragraph(dt.Rows[i][j].ToString(), font));

                            cellData.FixedHeight = 30.0f;

                            table2.AddCell(cellData);
                        }
                    }

                    doc.Add(table2);

                    doc.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
           
        }
    }
}
