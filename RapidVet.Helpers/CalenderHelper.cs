﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace RapidVet.Helpers
{
   public static class CalenderHelper
    {
       public static string GetHebrewJewishDayOfWeekString(DateTime anyDate)
       {
           var hebrewFormatedString = new System.Text.StringBuilder();

           // Create the hebrew culture to use hebrew (Jewish) calendar 
           var jewishCulture = CultureInfo.CreateSpecificCulture("he-IL");
           jewishCulture.DateTimeFormat.Calendar = new HebrewCalendar();

           #region Format the date into a Jewish format

           
           // Day of the week in the format "יום א' " 
           hebrewFormatedString.Append(Resources.Calendar.Day + " " + anyDate.ToString("ddd", jewishCulture) + "' ");
           

           return hebrewFormatedString.ToString();
       }

       public static string GetHebrewJewishDateString(DateTime anyDate)
       {
           var hebrewFormatedString = new System.Text.StringBuilder();

           // Create the hebrew culture to use hebrew (Jewish) calendar 
           var jewishCulture = CultureInfo.CreateSpecificCulture("he-IL");
           jewishCulture.DateTimeFormat.Calendar = new HebrewCalendar();

          

           // Day of the month in the format "'" 
           hebrewFormatedString.Append(anyDate.ToString("dd", jewishCulture) + " ");

           // Month and year in the format " " 
           hebrewFormatedString.Append("" + anyDate.ToString("y", jewishCulture));

           #endregion

           return hebrewFormatedString.ToString();
       }
    }
}
