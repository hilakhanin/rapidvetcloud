﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Helpers
{
   public enum DrugAdministration
    {
        PO, PR, PV, TOP, SC, SL, TD, IV, IM, IT, ED
    }
}
