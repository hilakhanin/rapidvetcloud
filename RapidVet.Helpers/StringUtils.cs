﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace RapidVet.Helpers
{
    public static class StringUtils
    {

        /// <summary>
        /// get checkbox value from string
        /// </summary>
        /// <param name="checkbox"> string containing checkbox values</param>
        /// <returns>bool</returns>
        public static bool GetCheckBoxValue(string checkbox)
        {
            var result = false;
            if (!string.IsNullOrWhiteSpace(checkbox))
            {
                var arr = checkbox.Split(',');
                if (arr.Length > 0)
                {
                    result = arr[0] == "true";
                }
            }
            return result;
        }

        /// <summary>
        /// gets list of int from input
        /// </summary>
        /// <param name="selected">string</param>
        /// <returns>List<int></returns>
        public static List<int> GetIntList(string selected)
        {
            var list = new List<int>();
            if (!string.IsNullOrWhiteSpace(selected))
            {
                var split = selected.Split(',');
                foreach (var t in split)
                {
                    var temp = 0;
                    int.TryParse(t, out temp);
                    if (temp > 0)
                    {
                        list.Add(temp);
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// gets indicator string for lab tests
        /// </summary>
        /// <param name="result"> result</param>
        /// <param name="min"> min value</param>
        /// <param name="max"> max value</param>
        /// <returns></returns>
        public static string GetIndicator(string value, decimal min, decimal max)
        {
            var indicator = string.Empty;
            var marker = "<span class=\"indicatorMarker {0}\">*</span>";
            var warningCssClass = "red";
            var goodCssClass = "green";
            decimal result;

            if (Decimal.TryParse(value, out result))
            {
                if (result <= min || result >= max)
                {
                    marker = string.Format(marker, warningCssClass);
                    if (result < min)
                    {
                        indicator = string.Format("[ -------------------- ]<span>&nbsp;&nbsp;&nbsp;</span>{0}", marker);
                    }
                    else if (result == min)
                    {
                        indicator = string.Format("[ --------------------{0}]", marker);
                    }

                    else if (result > max)
                    {
                        indicator = string.Format("{0}<span>&nbsp;&nbsp;&nbsp;</span>[ -------------------- ]", marker);
                    }
                    else if (result == max)
                    {
                        indicator = string.Format("[{0}-------------------- ]", marker);
                    }
                }

                else if (max - min > 0)
                {
                    marker = string.Format(marker, goodCssClass);

                    var r = Math.Abs(result - min);
                    var precentage = 10 - ((r / (max - min)) * 10);
                    var tempIndicator = string.Empty;
                    bool indicatorUsed = false;

                    for (int i = 1; i < 11; i++)
                    {
                        if (precentage < i && !indicatorUsed)
                        {
                            tempIndicator += string.Format("{0}{1}", marker, "-");
                            indicatorUsed = true;
                        }
                        else
                        {
                            tempIndicator += "--";
                        }

                    }
                    indicator = string.Format("[ {0} ]", tempIndicator);
                }
            }
            else
            {
                indicator = "";
            }
            return indicator;
        }

        //this function removes any - or other none digits from string
        public static string ClearNonDigitChars(string data)
        {
            var result = string.Empty;
            for (int i = 0; i < data.Length; i++)
            {
                if (Char.IsDigit(data[i]))
                    result += data[i];
            };
            return result;
        }

        /// <summary>
        /// parses dd/MM/yyyy string to datetime
        /// </summary>
        /// <param name="date">streing containing date</param>
        /// <returns>dateTime, wil return datetime.Min if parse is unsuccessfull</returns>
        public static DateTime ParseStringToDateTime(string date)
        {
            var result = DateTime.MinValue;
            if (!string.IsNullOrWhiteSpace(date))
            {
                DateTime.TryParse(date, out result);
                //DateTime.TryParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out result);
            }
            return result;
        }

        /// <summary>
        /// returns datetime from combined parameters
        /// </summary>
        /// <param name="startDate"> datetime start date</param>
        /// <param name="toString"> string __:__ representing time</param>
        /// <returns>DateTime</returns>
        public static DateTime ParseTimeString(DateTime date, string timeStr)
        {
            if (!string.IsNullOrWhiteSpace(timeStr))
            {
                timeStr = timeStr.Trim();
                var spaceSplit = timeStr.Split(' ');
                if (spaceSplit.Length == 2)
                {
                    timeStr = spaceSplit[1];
                }

                var split = timeStr.Split(':');
                if (split.Length >= 2)
                {
                    var hours = 0; //GetTime(split[0]);
                    int.TryParse(split[0], out hours);

                    var minutes = 0; //GetTime(split[1]);
                    int.TryParse(split[1], out minutes);
                    date = date.AddHours(hours).AddMinutes(minutes);
                }
            }
            return date;
        }
        public static int GetTime(string time)
        {
            var str = string.Empty;
            var result = 0;
            char[] charArray = time.ToCharArray();
            for (int i = 0; i < charArray.Length; i++)
            {
                if (charArray[i] != '_')
                {
                    str = str + charArray[i];
                }
            }
            if (!string.IsNullOrWhiteSpace(str))
            {
                int.TryParse(str, out result);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date">datetime </param>
        /// <param name="time">string HH:MM:SS</param>
        /// <returns>datetime</returns>
        public static DateTime GetDateTime(DateTime date, string time)
        {
            var split = time.Split(':');

            var hours = 0;
            int.TryParse(split[0], out hours);

            var minutes = 0;
            int.TryParse(split[1], out minutes);

            var seconds = 0;
            int.TryParse(split[2], out seconds);

            return date.AddHours(hours).AddMinutes(minutes).AddSeconds(seconds);
        }

        public static string GetExtension(string oldLocation)
        {
            var result = string.Empty;
            if (!string.IsNullOrWhiteSpace(oldLocation))
            {
                var split = oldLocation.Split('.');
                if (split.Length > 0)
                {
                    result = split[split.Length - 1];
                }
            }
            return result;
        }

        public static string GetRootFolder(string oldLocation)
        {
            var result = string.Empty;

            if (!string.IsNullOrWhiteSpace(oldLocation))
            {
                var split = oldLocation.Split('\\');
                if (split.Length >= 3)
                {
                    result = string.Format("{0}\\{1}", split[0], split[1]);
                }
            }

            return result;
        }

        public static string GetConetntType(string fileExtension)
        {
            //images
            if (fileExtension == "jpeg" || fileExtension == "jpg") return "image/jpeg";
            if (fileExtension == "tiff") return "image/tiff";
            if (fileExtension == "bmp") return "image/bmp";
            if (fileExtension == "png") return "image/png";

            //videos
            if (fileExtension == "avi") return "video/x-msvideo";
            if (fileExtension == "MPEG4" || fileExtension == "mpeg4") return "video/mp4";
            if (fileExtension == "wmv") return "video/x-ms-wmv";

            //documents
            if (fileExtension == "pdf") return "application/pdf";
            if (fileExtension == "doc") return "application/msword";
            if (fileExtension == "docx") return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            if (fileExtension == "ppt") return "application/vnd.ms-powerpoint";
            if (fileExtension == "pptx") return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
            if (fileExtension == "xls") return "application/vnd.ms-excel";
            if (fileExtension == "xlsx") return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            return "text/plain";
        }

        public static string GetDirectoryName(string path)
        {
            var result = string.Empty;
            if (!string.IsNullOrWhiteSpace(path))
            {
                var split = path.Split('\\').ToArray();
                if (split.Length > 0)
                {
                    result = split[split.Length - 1];
                }
            }
            return result;
        }

        public static string GetTitle(string oldLocation)
        {
            var result = string.Empty;

            if (!string.IsNullOrWhiteSpace(oldLocation))
            {
                var split = oldLocation.Split('\\');
                if (split.Length > 0)
                {
                    result = split[split.Length - 1];
                }
            }
            return result;
        }
    }
}
