﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Helpers
{
   public class SerializationHelperObject
    {
       public int Id { get; set; }
       public string Value { get; set; }
    }
}
