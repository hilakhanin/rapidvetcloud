﻿using RapidVet.WebModels.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RapidVet.Helpers
{
    public static class GoogleHelper
    {
        public static IEnumerable<SelectListItem> GetSyncPeriods(int currentSyncPeriod)
        {
            string vals = "2,5,10,15,20,30,45,60";
            var splitted = vals.Split(',');
            foreach (var i in splitted)
                yield return new SelectListItem() { Text = i, Value = i, Selected = currentSyncPeriod.ToString().Equals(i) };
        }

        public static IEnumerable<SelectListItem> GetBackForwardDays(int currentDay)
        {
            string vals = "5,10,15,20,30,60";
            var splitted = vals.Split(',');
            foreach (var i in splitted)
                yield return new SelectListItem() { Text = i, Value = i, Selected = currentDay.ToString().Equals(i) };
        }

        public static IEnumerable<SelectListItem> GetUsersList(List<UsersSettingsShowUserModel> usersList, int selectedDoctorID)
        {
            if (usersList != null && usersList.Count > 0)
                foreach (var u in usersList)
                    yield return new SelectListItem() { Text = u.User, Value = u.DoctorId.ToString(), Selected = u.DoctorId == selectedDoctorID };
        }
    }
}
