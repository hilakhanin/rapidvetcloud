﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.WebModels.MinistryOfAgricultureReport;

namespace RapidVet.Helpers
{
    public static class MinistryOfAgricultureReportHelpers
    {

        public static String ChipImplantReport(ChipImplant data, Patient patient, Client owner, Clinic clinic, string regionalCouncilName)
        {
            var sb = new StringBuilder();
            sb.AddGeneralReportFields(patient, owner, clinic, regionalCouncilName);
            sb.AddReportCode(MinistryOfAgricultureReportType.ChipImplant);
            //event date(8) ddMmyyyy
            var date = data.Date.Replace("/", "");
            sb.AddField(date, 8);
            //vet license(50)
            sb.AddField(data.VetLicense, 50);
            //reimplant(2) yes=1, no=2
            switch (data.Reimplant)
            {
                case true:
                    {
                        sb.AddField("1", 1);
                        break;
                    }
                case false:
                    {
                        sb.AddField("2", 1);
                        break;
                    }
            }
            return sb.ToString();
        }

        public static String RabiesVaccineReport(RabiesVaccine data, Patient patient, Client owner, Clinic clinic, string regionalCouncilName)
        {
            var sb = new StringBuilder();
            sb.AddGeneralReportFields(patient, owner, clinic, regionalCouncilName);
            sb.AddReportCode(MinistryOfAgricultureReportType.RabiesVaccination);
            //event date(8) ddMmyyyy
            var date = data.Date.Replace("/", "");
            sb.AddField(date, 8);
            //vet license(50)
            sb.AddField(data.VaccinatingVetLicense, 50);
            //batch(8)
            sb.AddField(data.Batch, 8);
            return sb.ToString();
        }

        public static String AttackReport(Attack data, Patient patient, Client owner, Clinic clinic, string regionalCouncilName)
        {
            var sb = new StringBuilder();
            sb.AddGeneralReportFields(patient, owner, clinic, regionalCouncilName);
            sb.AddReportCode(MinistryOfAgricultureReportType.Attack);
            //event date(8) ddMmyyyy
            var date = data.Date.Replace("/", "");
            sb.AddField(date, 8);
            //description(50)
            sb.AddField(data.Description, 50);
            //cancelation date(8)
            var cancellationDate = data.CancellationDate.Replace("/", "");
            sb.AddField(cancellationDate, 8);
            return sb.ToString();
        }

        public static String QuarantineAdmissionReport(QuarantineAdmission data, Patient patient, Client owner,
                                                       Clinic clinic, string regionalCouncilName)
        {
            var sb = new StringBuilder();
            sb.AddGeneralReportFields(patient, owner, clinic, regionalCouncilName);
            sb.AddReportCode(MinistryOfAgricultureReportType.QuarantineAdmission);
            //event date(8) ddMmyyyy
            var date = data.Date.Replace("/", "");
            sb.AddField(date, 8);
            //facility name(50)
            sb.AddField(data.FacilityName, 50);
            //quarantine reason(8)
            sb.AddField(data.AdmissionReason, 8);
            //contact(20)
            sb.AddField(data.ContactName, 20);
            //phone(11)
            sb.AddField(data.Phone, 11);
            return sb.ToString();
        }

        public static String DogLicenseReport(DogLicense data, Patient patient, Client owner, Clinic clinic, string regionalCouncilName)
        {
            var sb = new StringBuilder();
            sb.AddGeneralReportFields(patient, owner, clinic, regionalCouncilName);
            sb.AddReportCode(MinistryOfAgricultureReportType.DogLicenseGranting);
            //event date(8) ddMmyyyy
            var date = data.Date.Replace("/", "");
            sb.AddField(date, 8);
            //license number(50)
            sb.AddField(data.DogLicenseNumber, 50);
            //verification number(8)
            sb.AddField(data.DogLicenseNumber, 8);
            return sb.ToString();
        }

        public static String RevokingLicenseReport(RevokingLicense data, Patient patient, Client owner, Clinic clinic, string regionalCouncilName)
        {
            var sb = new StringBuilder();
            sb.AddGeneralReportFields(patient, owner, clinic, regionalCouncilName);
            sb.AddReportCode(MinistryOfAgricultureReportType.RevokingLicense);
            //event date(8) ddMmyyyy
            var date = data.Date.Replace("/", "");
            sb.AddField(date, 8);
            //reason(50)
            sb.AddField(data.RevokingReason, 50);
            return sb.ToString();
        }

        public static String QuarantineDischargeReport(QuarantineDischarge data, Patient patient, Client owner,
                                                       Clinic clinic, string regionalCouncilName)
        {
            var sb = new StringBuilder();
            sb.AddGeneralReportFields(patient, owner, clinic, regionalCouncilName);
            sb.AddReportCode(MinistryOfAgricultureReportType.QuarantineDischarge);
            //event date(8) ddMmyyyy
            var date = data.Date.Replace("/", "");
            sb.AddField(date, 8);
            //facility(50)
            sb.AddField(data.DischargingFacilityName, 50);
            //discharge type(1)
            sb.AddField(data.DischargeTypeId.ToString(), 1);
            return sb.ToString();
        }

        public static String DangerousDogReport(DangerousDog data, Patient patient, Client owner, Clinic clinic, string regionalCouncilName)
        {
            var sb = new StringBuilder();
            sb.AddGeneralReportFields(patient, owner, clinic, regionalCouncilName);
            sb.AddReportCode(MinistryOfAgricultureReportType.DangerousDog);
            //event date(8) ddMmyyyy
            var date = data.Date.Replace("/", "");
            sb.AddField(date, 8);
            //reason(50)
            sb.AddField(data.DangerousDogReason, 50);
            //insurance finish date(8)
            var cancellationDate = data.InsuranceFinishDate.Replace("/", "");
            sb.AddField(cancellationDate, 8);
            //insurance company(20)
            sb.AddField(data.InsuranceCompany, 20);
            //policy number(10)
            sb.AddField(data.InsuranceNumber, 10);
            return sb.ToString();
        }

        public static String ComplaintReport(ComplaintReport data, Patient patient, Client owner, Clinic clinic, string regionalCouncilName)
        {
            var sb = new StringBuilder();
            sb.AddGeneralReportFields(patient, owner, clinic, regionalCouncilName);
            sb.AddReportCode(MinistryOfAgricultureReportType.ComplaintReport);
            //event date(8) ddMmyyyy
            var date = data.Date.Replace("/", "");
            sb.AddField(date, 8);
            //details(50)
            sb.AddField(data.ComplaintDetails, 50);
            //type(1)
            sb.AddField(data.ComplaintTypeId.ToString(), 1);
            //legal proceedings(20 1=yes,2=no, default:2
            switch (data.LegalProceeding)
            {
                case true:
                    {
                        sb.AddField("1", 1);
                        break;
                    }
                case false:
                    {
                        sb.AddField("2", 1);
                        break;
                    }

            }
            //offence type(1)
            sb.AddField(data.OffenceTypeId.ToString(), 1);
            return sb.ToString();
        }

        public static String DogFindingReport(DogFinding data, Patient patient, Client owner, Clinic clinic, string regionalCouncilName)
        {
            var sb = new StringBuilder();
            sb.AddGeneralReportFields(patient, owner, clinic, regionalCouncilName);
            sb.AddReportCode(MinistryOfAgricultureReportType.FindingADog);
            //event date(8) ddMmyyyy
            var date = data.Date.Replace("/", "");
            sb.AddField(date, 8);
            //details(50)
            sb.AddField(data.Details, 50);
            //reported to owner (1) yes=1, no=2, default:2
            switch (data.ReportedToOwner)
            {
                case true:
                    {
                        sb.AddField("1", 1);
                        break;
                    }
                case false:
                    {
                        sb.AddField("2", 1);
                        break;
                    }
            }
            return sb.ToString();
        }

        private static void AddReportCode(this StringBuilder sb, MinistryOfAgricultureReportType type)
        {
            sb.AddField(((int)type).ToString(), 1);
        }

        private static void AddGeneralReportFields(this StringBuilder sb, Patient patient, Client owner, Clinic clinic, string regionalCouncilName)
        {
            //reporting body name(30) 
            sb.AddField(regionalCouncilName, 30);
            //index(5) - leave empty, fill later: index 30 to 34 in string
            sb.AddEmptyField(5);
            //electronic number(16) 
            sb.AddField(patient.ElectronicNumber, 16);
            //license number(10)
            sb.AddField(patient.LicenseNumber, 10);
            //animal kind(1): dog=1,default=dog 
            sb.AddField("1", 1);
            //animal name(10)
            sb.AddField(patient.Name, 10);
            //animal gender (1) (male=1, female =2)
            sb.AddField(patient.GenderId == 2 ? "2" : "1", 1);
            //animal race(15)
            sb.AddField(patient.AnimalRace.Value, 15);
            //seeing eye dog(1) default 2=no
            sb.AddField(patient.SeeingEyeDog ? "1" : "2", 1);
            //animal security system(1) - no reference in MOG doc file
            sb.AddEmptyField(1);
            //color(10)
            sb.AddField(patient.AnimalColor != null ? patient.AnimalColor.Value : "", 10);
            //marked abroad(1) default 2=no
            sb.AddField("2", 1);
            //year of birth(4)
            sb.AddField(patient.BirthDate.HasValue ? patient.BirthDate.Value.Year.ToString() : string.Empty, 4);
            //month of birth(2)
            sb.AddField(patient.BirthDate.HasValue ? patient.BirthDate.Value.Month.ToString() : string.Empty, 2);

            //sterilization(1) yes=1,no=2, default=2
            sb.AddField(patient.Sterilization ? "1" : "2", 1);

            //owner id(9)
            sb.AddField(owner.IdCard, 9);
            //owner name(20)
            sb.AddField(owner.Name, 20);
            //owner address(50)
            var address = owner.Addresses.FirstOrDefault();
            if (address != null)
            {
                sb.AddField(address.Street, 50);
            }
            else
            {
                sb.AddEmptyField(50);
            }
            //owner city(4) - city code in ministry of inerior tables - CityId in our DB?
            if (address != null)
            {
                var city = address.City;
                if (city != null)
                {
                    sb.AddField(city.CityId.ToString(), 4);
                }
                else
                {
                    sb.AddEmptyField(4);
                }
            }
            else
            {
                sb.AddEmptyField(4);
            }
            //mailing address(20) - different from owner address - not required
            sb.AddEmptyField(20);
            //owner zipcode(5)
            if (address != null)
            {
                sb.AddField(address.ZipCode, 5);
            }
            else
            {
                sb.AddEmptyField(5);
            }
            //owner phone(11)
            var homephoneId = (int)PhoneTypeEnum.Home;
            var homephone = owner.Phones.FirstOrDefault(p => p.PhoneTypeId == homephoneId);
            if (homephone != null)
            {
                sb.AddField(homephone.PhoneNumber, 11);
            }
            else
            {
                sb.AddEmptyField(11);
            }
            //requests hiding phone = !display phone(1) yes=1,no=2
            var hide = owner.ApproveDisplayPhone ? 2 : 1;
            sb.AddField(hide.ToString(), 1);
            //owner fax(10)
            var faxId = (int)PhoneTypeEnum.Fax;
            var fax = owner.Phones.FirstOrDefault(p => p.PhoneTypeId == faxId);
            if (fax != null)
            {
                sb.AddField(fax.PhoneNumber, 10);
            }
            else
            {
                sb.AddEmptyField(10);
            }
            //owner cell(11)
            var cellId = (int)PhoneTypeEnum.Mobile;
            var cell = owner.Phones.FirstOrDefault(p => p.PhoneTypeId == cellId);
            if (cell != null)
            {
                sb.AddField(cell.PhoneNumber, 11);
            }
            else
            {
                sb.AddEmptyField(11);
            }
            //owner email(25)
            var email = owner.Emails.FirstOrDefault();
            if (email != null)
            {
                sb.AddField(email.Name, 25);
            }
            else
            {
                sb.AddEmptyField(25);
            }
            //STATUS_BH (1) default: 0
            sb.AddField("0", 1);
            //TAR_STATUS(8) 
            sb.AddField("00000000", 8);
        }

        private static void AddField(this StringBuilder sb, String str, int length)
        {
            sb.Append(OpenFormatUtils.GetAlphaString(str, length));
        }
        private static void AddEmptyField(this StringBuilder sb, int length)
        {
            sb.Append(OpenFormatUtils.GetEmptyString(length, false));
        }
    }
}
