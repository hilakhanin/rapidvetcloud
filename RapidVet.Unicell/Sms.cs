﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RapidVet.Helpers;

namespace RapidVet.Unicell
{
    public class Sms
    {
        // 0  userID
        // 1 password
        // 2 phone
        // 3 message
        // 4 sender phone
        static string _BaseSmsString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><sms command=\"submit\" version=\"1.0\"><account><id>{0}</id><password>{1}</password></account><attributes><replyPath>{4}</replyPath></attributes><targets><cellphone>{2}</cellphone></targets><data type=\"text\">{3}</data></sms>";

        static string _Url = @"http://soprano.co.il/prodsms/corpsms";


        string _ServiceId, _Password, _To, _Message, _SenderPhone;

        /// <summary>
        /// represents sms message
        /// </summary>
        /// <param name="serviceId"> service id</param>
        /// <param name="Password"> passrord</param>
        /// <param name="to"> recipient</param>
        /// <param name="message"> message</param>
        /// <param name="senderPhone"> sender phone number</param>
        public Sms(string serviceId, string Password, string to, string message, string senderPhone)
        {
            _ServiceId = serviceId;
            _Password = Password;
            _Message = message.Replace('<', ' ').Replace('>', ' ');
            _SenderPhone = senderPhone;
            _To = StringUtils.ClearNonDigitChars(to);
        }

        /// <summary>
        /// sends sms message
        /// </summary>
        /// <returns> true</returns>
        public bool Send()
        {
            string xml = getXml();

            string s = HttpPost.Send(_Url, xml);

            return s.Contains("Success");

        }

        public string SendStr()
        {
            string xml = getXml();

            string s = HttpPost.Send(_Url, xml);

            return s;
        }

        /// <summary>
        /// creates xml string to be sent
        /// </summary>
        /// <returns>string</returns>
        private string getXml()
        {
            return string.Format(_BaseSmsString, _ServiceId, _Password, _To, _Message, _SenderPhone);
        }

    }
}
