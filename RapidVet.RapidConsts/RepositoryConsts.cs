﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.RapidConsts
{
    public static class RepositoryConsts
    {
        public static string FOOD_ITEM_TYPE_NAME = "מזון";

        public static string VACCINE_ITEM_TYPE_NAME = "חיסונים";

        public static string PREVENTIVE_TREATMENT_ITEM_TYPE_NAME = "טיפולים מונעים";
        //public static string RABIES_VACCINE_NAME = "חיסון כלבת";

        public static string ANIMAL_KIND_DOG_NAME = "כלב";
        public static string ANIMAL_KIND_CAT_NAME = "חתול";

        public static string ANIMAL_RACE_NAME_UNKNOWN = "-";
    }
}
