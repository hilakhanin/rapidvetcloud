﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.RapidConsts.Controllers
{
   public static class UserConsts
   {
       public static string GRID_ID = "usersGrid";
       public static string MODAL_ID = "usersModal";
       public static string CREATE_ID = "createUser";
       public static string EDIT_ID = "editUser";
       public static string EDIT_ROLES_ID = "editUserRoles";
       public static string DELETE_ID = "deleteUser";
   }
}
