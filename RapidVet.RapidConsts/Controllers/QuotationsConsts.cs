﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.RapidConsts.Controllers
{
    public static class QuotationsConsts
   {
        public static string GRID_ID = "quotationsGrid";
       public static string MODAL_ID = "quotationsModal";
       public static string CREATE_ID = "createUser";
       public static string EDIT_ID = "editQuotation";
       public static string EDIT_ROLES_ID = "editUserRoles";
       public static string DELETE_ID = "deleteUser";
   }
}
