﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.RapidConsts.Controllers
{
    public static class TariffConsts
    {
        public static string TARIFF_GRID_ID = "tariffGrid";
        public static string TARIFF_MODAL_ID = "tariffModal";
        public static string TARIFF_CREATE_FORM = "tariffCreate";
        public static string TARIFF_EDIT_FORM = "tariffEdit";
        public static string TARIFF_DELETE_FORM = "tariffDelete";
        public static string TARIFF_DUPLICATE_TARIFF_FORM = "tariffDuplicate";
        public static string TARIFF_DUPLICATE_CLINIC_FORM = "tariffClinicDuplicate";
        public static string TARIFF_PRICE_SHIFT_FORM = "tariffPriceShift";
    }
}
