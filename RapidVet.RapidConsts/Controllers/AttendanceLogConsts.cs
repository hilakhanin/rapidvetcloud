﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.RapidConsts.Controllers
{
    public static class AttendanceLogConsts
    {
        public static string TABLE_ID = "attendanceLogTable";
        public static string MODAL_ID = "attendanceLogModal";
    }
}
