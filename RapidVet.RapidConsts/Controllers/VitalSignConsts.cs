﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.RapidConsts.Controllers
{
    public static class VitalSignConsts
    {
        public static string TABLE_ID = "vitalSignTable";
        public static string MODAL_ID = "vitalSignModal";
    }
}
