﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.RapidConsts.Controllers
{
    public static class CategoryConsts
    {
        public static string CATEGORY_GRID_ID = "categoryGrid";
        public static string CATEGORY_MODAL_ID = "categoryModal";
        public static string MENU_BAR = "categoryMenuBar";
    }
}
