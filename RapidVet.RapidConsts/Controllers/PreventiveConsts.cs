﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.RapidConsts.Controllers
{
    public static class PreventiveConsts
   {
       public static string GRID_ID = "clientStatusGrid";
       public static string CREATE_FORM = "clientStatusCreate";
       public static string EDIT_FORM = "preventiveHistoryEdit";
       public static string DELETE_FORM = "clientStatusDelete";
       public static string MODAL_ID = "preventiveHistoryModal";
   }
}
