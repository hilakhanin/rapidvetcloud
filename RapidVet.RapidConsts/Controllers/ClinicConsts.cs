﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace RapidVet.RapidConsts.Controllers
{
    public static class ClinicConsts
    {
        public static string EDIT_MAIN_CONTENT_ID = "editContent";
        public static string GRID_ID = "clinicsGrid";
        public static string MODAL_ID = "clinicModal";
        private static decimal defaultClinicTaxRate = 18;
        public static decimal TAX_RATE
        {
            get
            {
                var result = defaultClinicTaxRate;
                decimal.TryParse(ConfigurationManager.AppSettings["NewClinicTaxRatePercent"], out result);
                return result;
            }
        }

    }
}
