﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.RapidConsts.Controllers
{
    public static class PriceListItemsConsts
    {
        public static string PRICE_LIST_ITEMS_GRID_ID = "itemsGrid";
        public static string PRICE_LIST_ITEMS_CONTAINER_ID = "priceListContainer";
        public static string PRICE_LIST_ITEMS_MODAL_ID = "itemsModal";
        public static string PRICE_LIST_ITEMS_EDIT = "itemEdit";
        public static string PRICE_LIST_ITEMS_CREATE = "itemCreate";
        public static string TPRICE_LIST_ITEMS_TABLE_ID = "itemsTable";
    }
}
