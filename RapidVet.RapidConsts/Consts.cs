﻿namespace RapidVet.RapidConsts
{
    public static class Consts
    {
        //Tariff
        public static string TARIFF_GRID_ID = "tariffGrid";
        public static string TARIFF_MODAL_ID = "tariffModal";
        public static string TARIFF_CREATE_FORM = "tariffCreate";
        public static string TARIFF_EDIT_FORM = "tariffEdit";
        public static string TARIFF_DELETE_FORM = "tariffDelete";
        public static string TARIFF_DUPLICATE_FORM = "tariffDuplicate";
        public static string TARIFF_PRICE_SHIFT_FORM = "tariffPriceShift";

        //PriceListItems
        public static string PRICE_LIST_ITEMS_GRID_ID = "PriceListContainer";
        public static string PRICE_LIST_ITEMS_MODAL_ID = "itemsModal";

        //Categories
        public static string CATEGORY_GRID_ID = "";
        public static string CATEGORY_MODAL_ID = "CategoryModal";

    }
}
