﻿namespace RapidVet.RapidConsts
{
    public class RolesConsts
    {
        public static string ADMINISTRATOR { get { return "Administrator"; } }
        public static string CLINICGROUPMANAGER { get { return "ClinicGroupManager"; } }
        public static string CLINICMANAGER { get { return "ClinicManager"; } }
        public static string DOCTOR { get { return "Doctor"; } }
        public static string SECRATERY { get { return "Secratery"; } }
        public static string VIEW_REPORTS { get { return "ViewReports"; } }
        public static string VIEW_FINANCIAL_REPORTS { get { return "ViewFinancialReports"; } }
        public static string EXCEL_EXPORT { get { return "ExcelExport"; } }
        public static string NO_DIARY { get { return "NoDiary"; } }

        public static string INVENTORY_ENTITY_MANAGEMENT { get { return "InventoryEntityManagement"; } }
        public static string INVENTORY_CREATE_ORDER { get { return "InventoryCreateOrder"; } }
        public static string INVENTORY_RECIEVE_ORDER { get { return "InventoryReciveOrder"; } }
        public static string INVENTORY_UPDATE { get { return "InventoryUpdate"; } }
        public static string INVENTORY_REPORTS { get { return "InventoryReports"; } }



    }
}
