﻿using System;
using System.Data.Entity;
using System.Linq;
using RapidVet.Model.Clients;
using RapidVet.Repository.Clients;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class ClientPatientsUpdater : MigrationBase
    {
        public ClientPatientsUpdater(int clinicGroupId, int clinicId)
        {

        }

        public bool RunUpdator(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            int counter = 0;
            var clientIds = Context.Clients.Where(c => c.ClinicId == clinicId).Select(c => c.Id).ToList();

            //update patient names string
            using (var clientRepository = new ClientRepository())
            {
                foreach (var clientId in clientIds)
                {
                    var client = Context.Clients.Include(c => c.Patients).Single(c => c.Id == clientId);
                    client.PatientNames = clientRepository.GetClientPatientName(client);
                    counter++;
                    migrationStatus.Status = counter.ToString();
                    SaveWithContextCleaning();
                }
            }

            SaveWithContextCleaning(true);



            //Update balance
            foreach (var clientId in clientIds)
            {
                using (var clientRepository = new ClientRepository())
                {
                    var client = Context.Clients.Single(c => c.Id == clientId);
                    client.Balance = clientRepository.GetClientCurrentBalance(client);
                    counter++;
                    migrationStatus.Status = counter.ToString();
                    SaveWithContextCleaning();
                }
            }
            SaveWithContextCleaning(true);
            return true;
        }
    }
}