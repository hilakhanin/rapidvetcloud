﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Clinics;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration.Labs
{
    public class LabsTemplateMigration : MigrationBase
    {
        public LabsTemplateMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            int counter = 0;
            var success = CreateLabTestTypes(clinicGroupId, clinicId);
            if (!success)
            {
                return false;
            }


            var connection = GetConnection(DataBases.rapidmed);
            var labTestTypes = Context.LabTestTypes.Where(l => l.ClinicId == clinicId).ToList();
            var animalKinds = Context.AnimalKinds.Where(a => a.ClinicGroupId == clinicGroupId).ToList();

            var templateNames = new HashSet<string>();
            var templateReader = GetReader(connection, "LabDef");
            while (templateReader.Read())
            {
                var templateName = templateReader["DefName"].ToString();
                if (!string.IsNullOrWhiteSpace(templateName))
                {
                    templateNames.Add(templateName);
                }
            }

            foreach (var templateName in templateNames)
            {
                var labTestType = labTestTypes.FirstOrDefault(l => templateName.Contains(l.Name)) ??
                                  labTestTypes.First();


                //foreach (var animalKind in animalKinds)
                //{
                //    var reader = GetReader(connection, "LabDef",
                //                           string.Format("DefName = '{0}' AND AnimalType = '{1}'", templateName,
                //                                         animalKind.Value));


                var query = string.Format("DefName = '{0}'", templateName);
                var reader = GetReader(connection, "LabDef", query);
                // if (reader.HasRows)
                //   {
                //var labTestTemplate = new LabTestTemplate()
                //    {
                //        AnimalKind = animalKind,
                //        LabTestType = labTestType,
                //        Name = templateName
                //    };
                while (reader.Read())
                {
                    var kindName = reader["AnimalType"].ToString();
                    var animalKind = animalKinds.FirstOrDefault(k => k.Value == kindName);

                    if (animalKind == null)
                    {
                        break;
                    }

                    var labTestTemplate =
                        Context.LabTestsTemplates.Local.FirstOrDefault(
                            l =>
                            l.LabTestTypeId == labTestType.Id &&
                            l.Name == templateName &&
                            l.AnimalKindId == animalKind.Id);

                    if (labTestTemplate == null)
                    {
                        labTestTemplate = Context.LabTestsTemplates.FirstOrDefault(l =>
                                                                                   l.LabTestTypeId == labTestType.Id &&
                                                                                   l.Name == templateName &&
                                                                                   l.AnimalKindId == animalKind.Id);
                    }
                    if (labTestTemplate == null)
                    {
                        labTestTemplate = new LabTestTemplate()
                            {
                                AnimalKindId = animalKind.Id,
                                LabTestTypeId = labTestType.Id,
                                Name = templateName,
                            };
                        Context.LabTestsTemplates.Add(labTestTemplate);
                    }


                    labTestTemplate.LabTestResults.Add(new LabTest()
                        {
                            Name = reader["TestName"].ToString(),
                            MinValue = GetDecimal(reader["MinValue"].ToString()),
                            MaxValue = GetDecimal(reader["MaxValue"].ToString()),
                            OldId = int.Parse(reader["RecordId"].ToString()),
                            OldTestId = int.Parse(reader["TestId"].ToString())
                        });
                    counter++;
                    migrationStatus.Status = counter.ToString();
                    SaveWithContextCleaning();
                }
                reader.Close();
            }
            //}
            //}

            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }

        private bool CreateLabTestTypes(int clinicGroupId, int clinicId)
        {
            var reader = new IniReader(IniPath);
            for (int i = 1; i < 11; i++)
            {
                var name = reader.GetValue(string.Format("Lab{0}Name", i), "Program Settings").ToString();

                if (!string.IsNullOrWhiteSpace(name))
                {
                    var labTestType = new LabTestType()
                        {
                            ClinicId = clinicId,
                            Name = name
                        };
                    Context.LabTestTypes.Add(labTestType);
                }
            }
            SaveWithContextCleaning(true);
            return true;
        }
    }
}
