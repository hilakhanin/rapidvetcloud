﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.PatientLabTests;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration.Labs
{
    public class LabResultsMigration : MigrationBase
    {
        public LabResultsMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var patientsIds =
                Context.Patients.Where(p => p.Client.ClinicId == clinicId).Select(p => p.Id).ToList();

            var connection = GetConnection(DataBases.rapidmed);
            int counter = 0;
            foreach (var patientId in patientsIds)
            {
                var patient = Context.Patients.Include(p => p.AnimalRace).SingleOrDefault(p => p.Id == patientId);
                if (patient == null) continue;

                var labWorksQuery = string.Format("AnimalID = {0}", patient.OldId);
                var testReader = GetReader(connection, "LabWorks", labWorksQuery);

                while (testReader.Read())
                {
                    var labWorkId = GetInt(testReader["LabWorkID"].ToString());

                    var labTestDate = GetDateTimeOrMin(testReader["LabDate"].ToString());

                    var labTemplateName = testReader["TemplateName"].ToString();

                    var template =
                        Context.LabTestsTemplates.FirstOrDefault(
                            t =>
                            t.AnimalKindId == patient.AnimalRace.AnimalKindId &&
                            t.Name == labTemplateName);

                    if (template != null && labTestDate > DateTime.MinValue && labTestDate.Year > 1990)
                    {
                        var labResultsQuery = string.Format("LabWorkID = {0}", labWorkId);
                        var resultsReader = GetReader(connection, "LabResults", labResultsQuery);

                        if (resultsReader.HasRows)
                        {
                            var patientLabTest = new PatientLabTest()
                            {
                                LabTestTemplateId = template.Id,
                                PatientId = patient.Id,
                                Created = labTestDate,
                            };

                            while (resultsReader.Read())
                            {
                                var oldLabTestId = GetInt(resultsReader["RowNum"].ToString());
                                var labTest = Context.LabTests.Local.FirstOrDefault(l => l.OldTestId == oldLabTestId);
                                if (labTest == null)
                                {
                                    labTest = Context.LabTests.FirstOrDefault(l => l.OldTestId == oldLabTestId);
                                }

                                if (labTest == null) continue;

                                patientLabTest.Results.Add(
                                    new PatientLabTestResult()
                                        {
                                            LabTestId = labTest.Id,
                                            Result = resultsReader["Value"].ToString()
                                        });
                            }

                            Context.PatientLabTests.Add(patientLabTest);
                            counter++;
                            migrationStatus.Status = counter.ToString();
                            SaveWithContextCleaning();
                        }

                        resultsReader.Close();
                    }
                }
                testReader.Close();
            }
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }
    }
}
