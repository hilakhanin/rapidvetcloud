﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Clinics;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class TelephoneDirectoryMigration : MigrationBase
    {
        public TelephoneDirectoryMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clincId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.phnbook);
            var reader = GetReader(connection, "PhoneBook");
            int counter = 0;
            while (reader.Read())
            {
                var td = new TelephoneDirectory()
                    {
                        ClinicId = clincId,
                        Name = reader["Name"].ToString(),
                        Address = reader["Address"].ToString(),
                        PrimaryPhone = reader["Phone"].ToString(),
                        SecondaryPhone = reader["Phone2"].ToString(),
                        Fax = reader["Fax"].ToString(),
                        Email = reader["Email"].ToString(),
                        CellPhone = reader["CellPhone"].ToString(),
                        ContactPerson = reader["Contact"].ToString(),
                        Comments =
                            string.Format("{0} {1} {2} {3}", reader["Note"].ToString(),
                                          reader["CellPhoneNote"].ToString(),
                                          reader["Phone1Note"].ToString(),
                                          reader["Phone2Note"].ToString())
                    };

                Context.TelephoneDirectories.Add(td);
                SaveWithContextCleaning();
                counter++;
                migrationStatus.Status = counter.ToString();
            }
            reader.Close();
            connection.Close();
         SaveWithContextCleaning(true);
            return true;
        }
    }
}
