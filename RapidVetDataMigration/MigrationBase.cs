﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Patients;
using RapidVet.Repository;

namespace RapidVetDataMigration
{
    public abstract class MigrationBase : IDisposable
    {
        protected RapidVet.Repository.RapidVetDataContext Context = new RapidVetDataContext();

        protected string DbPath;


        protected string IniPath
        {
            get { return Path.Combine(DbPath, "Rapidmed.ini"); }
        }



        public void Save()
        {
            Context.SaveChanges();
        }

        private int counter = 0;

        // Call Save whene loop finish to save all data
        public void SaveWithContextCleaning(bool forceClean = false)
        {
            if (counter > 50 || forceClean)
            {

                Context.SaveChanges();
                Context.Dispose();
                Context = new RapidVetDataContext();
                counter = 0;
            }
            else
            {
                counter++;
            }
        }



        protected int GetInt(string intStr)
        {
            int r;
            if (int.TryParse(intStr, out r))
            {
                return r;
            }
            return 0;
        }

        protected bool GetBoolean(string boolStr)
        {
            boolStr = boolStr.Trim().ToLower();
            var intResult = GetInt(boolStr);
            var result = intResult == 1;
            if (!result && !string.IsNullOrWhiteSpace(boolStr))
            {
                bool.TryParse(boolStr, out result);
            }
            if (!result && !string.IsNullOrWhiteSpace(boolStr))
            {
                result = boolStr == "כן" || boolStr == "yes";
            }
            return result;
        }

        protected decimal GetDecimal(string decStr)
        {
            decimal r;
            if (Decimal.TryParse(decStr, out r))
            {
                return r;
            }
            return 0;
        }

        protected long GetLong(string longStr)
        {
            long result = 0;
            if (!string.IsNullOrWhiteSpace(longStr))
            {
                long.TryParse(longStr, out result);
            }
            return result;
        }

        protected DateTime? GetNullableDateTime(object dateTime)
        {
            if (dateTime == System.DBNull.Value)
            {
                return null;
            }
            return (DateTime)dateTime;
        }

        protected DateTime GetDateTimeOrMin(string dateTimeStr)
        {
            var result = DateTime.MinValue;
            if (!string.IsNullOrWhiteSpace(dateTimeStr))
            {
                DateTime.TryParse(dateTimeStr, out result);
            }
            return result;
        }

        protected DateTime GetDateTime(string dateTimeStr)
        {
            DateTime result;
            DateTime.TryParse(dateTimeStr, out result);
            return result == DateTime.MinValue
                       ? DateTime.Now
                       : result;
        }

        protected string GetColor(string color)
        {
            var result = string.Empty;

            var colorInt = GetInt(color);
            if (colorInt > 0)
            {
                result = string.Format("#{0}", colorInt.ToString("X"));
            }

            return result;
        }

        protected OleDbConnection GetConnection(DataBases db)
        {
            var filePath = Path.Combine(DbPath, db.ToString() + ".mdb");
            var myConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Jet OLEDB:Database password=rpd8088;";

            var thisConnection = new OleDbConnection(myConnectionString);

            thisConnection.Open();
            return thisConnection;
        }

        protected OleDbDataReader GetReader(OleDbConnection connection, string tableName, string query = "")
        {
            var thisCommand = connection.CreateCommand();
            if (string.IsNullOrEmpty(query))
            {
                thisCommand.CommandText = string.Format("SELECT * FROM [{0}]", tableName);
            }
            else
            {

                thisCommand.CommandText = string.Format("SELECT * FROM [{0}] WHERE {1}", tableName, query);
            }
            var thisReader = thisCommand.ExecuteReader();
            return thisReader;
        }

        public static decimal ToNullableDecimal(string s)
        {
            decimal i;
            if (decimal.TryParse(s, out i)) return i;
            return 0;
        }

        public static bool HasColumn(IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
