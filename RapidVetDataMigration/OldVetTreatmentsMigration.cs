﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Enums.DataMigration;
using RapidVet.Helpers;
using RapidVet.Model.Visits;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class OldVetTreatmentsMigration : MigrationBase
    {
        public OldVetTreatmentsMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.treatmnt);
            var reader = GetReader(connection, "Treatments");
            int counter = 0;

            while (reader.Read())
            {
                var patientOldId = GetInt(reader["AnimalID"].ToString());
                var patient =
                    Context.Patients.Include(p => p.Client)
                           .FirstOrDefault(p => p.Client.ClinicId == clinicId && p.OldId == patientOldId); ;

                if (patient == null) continue;

                var visitDate = GetDateTimeOrMin(reader["Date"].ToString());
                var dateWithTime = StringUtils.ParseTimeString(visitDate, reader["Time"].ToString());

                var priceListItemCode = reader["TreatCode"].ToString();
                var priceListItem =
                    Context.PriceListItems.Include(i => i.Category)
                           .FirstOrDefault(i => i.Category.ClinicId == clinicId && i.OldCode == priceListItemCode);
                int? priceListItemId = null;
                if (priceListItem != null)
                {
                    priceListItemId = priceListItem.Id;
                }

                var doctorName = reader["Doctor"].ToString();
                var doctor =
                    Context.Users.FirstOrDefault(d => d.ClinicGroupId == clinicGroupId && d.FirstName == doctorName);
                if (doctor == null) continue;


                var visit = new Visit()
                    {
                        PatientId = patient.Id,
                        DoctorId = doctor.Id,
                        VisitDate = dateWithTime,
                        Close = true,
                        MainComplaint = string.Format("{0} {1}", reader["Description"], reader["DLogNote"]),
                        OldRapidvetTreatmentId = GetInt(reader["RecordID"].ToString()),
                        Active = !GetBoolean(reader["Cancelled"].ToString()),
                        ClientIdAtTimeOfVisit = patient.ClientId,
                        Treatments = new List<VisitTreatment>()
                           {
                               new VisitTreatment()
                                   {
                                       CategoryName = reader["TreatCat"].ToString(),
                                       Name = reader["Treatment"].ToString(),
                                       Price = GetDecimal(reader["Price"].ToString()),
                                       Quantity = (int) Math.Round(GetDecimal(reader["TreatUnits"].ToString())),
                                       Invoiced = GetBoolean(reader["Invoiced"].ToString()),
                                       Discount = GetDecimal(reader["Discount"].ToString()),
                                       PriceListItemId = priceListItemId,
                                   }
                           }
                    };

                Context.Visits.Add(visit);
                counter++;
                migrationStatus.Status = counter.ToString();
                SaveWithContextCleaning();
            }
            reader.Close();
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }
    }
}
