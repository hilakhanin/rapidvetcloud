﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.FullCalender;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class HolidaysMigration : MigrationBase
    {
        public HolidaysMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinigGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.Holidays);
            var reader = GetReader(connection, "Holidays");
            int counter = 0;
            while (reader.Read())
            {
                var holiday = new Holiday()
                    {
                        ClinicId = clinicId,
                        Name = reader["HolidayName"].ToString(),
                        StartDay = DateTime.Parse(reader["StartDate"].ToString()),
                        EndDay = DateTime.Parse(reader["EndDate"].ToString()),
                        HasHolidayEve = bool.Parse(reader["ErevHag"].ToString())
                    };
                Context.Holidays.Add(holiday);
                SaveWithContextCleaning();
                counter++;
                migrationStatus.Status = counter.ToString();
            }
            reader.Close();
            connection.Close();
           SaveWithContextCleaning(true);
            return true;
        }
    }
}
