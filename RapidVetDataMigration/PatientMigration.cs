﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RapidVet.Enums;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Clients;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Patients;
using RapidVet.Model.Users;
using System.Data.Entity;
using RapidVet.Resources;
using Patient = RapidVet.Model.Patients.Patient;
using PatientComment = RapidVet.Model.Patients.PatientComment;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class PatientMigration : MigrationBase
    {
        public PatientMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        // migrate all animals
        public bool RunMigration(int clinicGroupId, int clincId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.rapidmed);
            var reader = GetReader(connection, "Animals");
            int counter = 0;
            //var localKind = false;
            while (reader.Read())
            {
                var ownerId = GetInt(reader["OwnerID"].ToString());
                var client = Context.Clients.Local.SingleOrDefault(c => c.ClinicId == clincId && c.HsId == ownerId);
                if (client == null)
                {
                    client = Context.Clients.SingleOrDefault(c => c.ClinicId == clincId && c.HsId == ownerId);
                }
                if (client != null)
                {
                    var oldId = GetInt(reader["AnimalID"].ToString());
                    var patient = client.Patients.SingleOrDefault(p => p.OldId == oldId) ?? new Patient();

                    patient.Active = GetBoolean(reader["AnimalActive"].ToString());
                    patient.OldId = oldId;
                    patient.Name = reader["AnimalName"].ToString();
                    patient.GenderId = GetBoolean(reader["AnimalSex"].ToString())
                                           ? (int)GenderEnum.Female
                                           : (int)GenderEnum.Male;
                    patient.PureBred = GetBoolean(reader["AnimalRacePure"].ToString());
                    patient.BirthDate = GetNullableDateTime(reader["AnimalBirthDate"]);
                    patient.ElectronicNumber = reader["AnimalElecNo"].ToString();
                    patient.SAGIRNumber = reader["AnimalSagirNo"].ToString();
                    patient.Sterilization = GetBoolean(reader["AnimalCastration"].ToString());
                    patient.SterilizationDate = GetNullableDateTime(reader["AnimalCastrationDate"]);
                    patient.Fertilization = GetBoolean(reader["AnimalFertilization"].ToString());
                    patient.BloodType = reader["AnimalBloodType"].ToString();
                    patient.HumanDangerous = GetBoolean(reader["AnimalDangerous"].ToString());
                    patient.AnimalDangerous = GetBoolean(reader["DangerousToAnimals"].ToString());
                    patient.FoodAmount = reader["AnimalFoodAmount"].ToString();
                    patient.Owner_Farm = reader["AnimalFarm"].ToString();


                    // kind & race
                    var kindName = reader["AnimalType"].ToString();
                    kindName = kindName.Trim();
                    if (string.IsNullOrEmpty(kindName))
                    {
                        kindName = "-";
                    }
                    var kind = Context.AnimalKinds.Local
                        .SingleOrDefault(k => k.ClinicGroupId == clinicGroupId && k.Value == kindName);
                    if (kind == null)
                    {
                        kind = Context.AnimalKinds.Include(a => a.AnimalRaces)
                                      .SingleOrDefault(k => k.ClinicGroupId == clinicGroupId && k.Value == kindName);
                    }
                    if (kind == null)
                    {
                        kind = new AnimalKind()
                            {
                                Active = true,
                                ClinicGroupId = clinicGroupId,
                                Value = kindName,
                                AnimalIconId = 1
                            };
                        if (kind.Value == Migration.Cat)
                        {
                            kind.AnimalIconId = 2;
                        }
                        Context.AnimalKinds.Add(kind);

                    }

                    var raceName = reader["AnimalRace"].ToString().Trim();
                    if (string.IsNullOrEmpty(raceName))
                    {
                        raceName = "-";
                    }
                    var race = kind.AnimalRaces.FirstOrDefault(r => r.Value == raceName);
                    if (race == null)
                    {
                        race = new AnimalRace()
                            {
                                Value = raceName,
                                Active = true,
                                AnimalKind = kind
                            };

                        kind.AnimalRaces.Add(race);
                        Context.AnimalRaces.Add(race);
                    }

                    patient.AnimalRace = race;

                    // color
                    var colorName = reader["AnimalColor"].ToString();
                    colorName = colorName.Trim();
                    if (string.IsNullOrEmpty(colorName))
                    {
                        colorName = "-";
                    }
                    var color =
                        Context.AnimalColors.Local.SingleOrDefault(
                            c => c.ClinicGroupId == clinicGroupId && c.Value == colorName);
                    if (color == null)
                    {
                        color =
                            Context.AnimalColors.SingleOrDefault(
                                c => c.ClinicGroupId == clinicGroupId && c.Value == colorName);
                    }
                    if (color == null)
                    {
                        color = new AnimalColor()
                            {
                                Value = colorName,
                                Active = true,
                                ClinicGroupId = clinicGroupId
                            };
                        Context.AnimalColors.Add(color);
                    }
                    patient.AnimalColor = color;

                    // notes 
                    if (!string.IsNullOrEmpty(reader["AnimalNotes"].ToString()))
                    {
                        patient.Comments.Add(new PatientComment()
                            {
                                Active = true,
                                CommentBody = reader["AnimalNotes"].ToString(),
                                Popup = false,
                            });
                    }


                    if (patient.Id == 0)
                    {
                        client.Patients.Add(patient);
                    }
                    SaveWithContextCleaning();
                    counter++;
                    migrationStatus.Status = counter.ToString();
                }
            }
            reader.Close();
            connection.Close();
           SaveWithContextCleaning(true);
            return true;
        }
    }
}
