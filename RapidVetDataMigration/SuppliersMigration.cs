﻿using System;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Expenses;
using RapidVet.Repository;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class SuppliersMigration : MigrationBase
    {
        public SuppliersMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunSuppliersMigration(int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.rapidmed);
            var reader = GetReader(connection, "Suppliers");
            //int counter = 0;
            while (reader.Read())
            {
                var supplier = new Supplier()
                    {
                        ClinicId = clinicId,
                        Name = reader["Name"].ToString(),
                        OldId = GetInt(reader["RecordID"].ToString())
                    };

                Context.Suppliers.Add(supplier);
                SaveWithContextCleaning();
            }
            reader.Close();
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }

        public bool RunExpenseGroupMigration(int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.rapidmed);
            var reader = GetReader(connection, "Expense Types");
            //int counter = 0;
            while (reader.Read())
            {
                var expenseGroup = new ExpenseGroup()
                    {
                        ClinicId = clinicId,
                        Name = reader["Name"].ToString(),
                        IsTextDeductible = GetBoolean(reader["Taxed"].ToString()),
                        RecognitionPercent = GetDecimal(reader["TaxAmount"].ToString())
                    };

                Context.ExpenseGroups.Add(expenseGroup);
                SaveWithContextCleaning();
            }
            reader.Close();
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }
    }
}