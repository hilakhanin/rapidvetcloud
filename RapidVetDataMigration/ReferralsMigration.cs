﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Patients;
using RapidVet.RapidConsts;
using RapidVet.Repository;
using RapidVet.Repository.Clients;
using RapidVet.Repository.Patients;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class ReferralsMigration : MigrationBase
    {
        public ReferralsMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.rapidmed);
            var reader = GetReader(connection, "DiagnosisSentences");
            int counter = 0;
            var clinicDoctors =
               Context.Users.Where(
                   u => u.UsersClinicsRoleses.Any(r => r.ClinicId == clinicId && r.Role.RoleName == RolesConsts.DOCTOR)).ToList();
            using (var repo = new ClientRepository())
            {
                while (reader.Read())
                {
                    var fromDoctorName = reader["FromDoctor"].ToString();
                    var fromDoctor = clinicDoctors.SingleOrDefault(d => d.FirstName == fromDoctorName);
                    //var client = repo.GetClientFromHsId(clinicId, int.Parse("PatID"));
                    var client = repo.GetClientFromHsId(clinicId, GetInt(reader["PatID"].ToString()));
                    if (fromDoctor != null && client != null && client.Patients.Any())
                    {
                        var toDoctorName = reader["ToDoctor"].ToString();
                        var toDoctor = clinicDoctors.SingleOrDefault(d => d.FirstName == toDoctorName);
                        var referral = new Referral()
                            {
                                PatientId = client.Patients.First().Id,
                                Reason = reader["Reason"].ToString(),
                                RefferedToExternal = toDoctor != null ? string.Empty : toDoctorName,
                                RefferingUserId = fromDoctor.Id,
                                OldId = GetInt(reader["RecordID"].ToString())
                            };

                        if (toDoctor != null)
                        {
                            referral.RefferedToUserId = toDoctor.Id;
                        }

                        Context.Referrals.Add(referral);
                        SaveWithContextCleaning();
                        counter++;
                        migrationStatus.Status = counter.ToString();
                    }
                }

            }
            reader.Close();
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }
    }
}
