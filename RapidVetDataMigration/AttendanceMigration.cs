﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Helpers;
using RapidVet.Model.Clinics;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class AttendanceMigration : MigrationBase
    {
        public AttendanceMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.Presence);
            var reader = GetReader(connection, "Presence");
            int counter = 0;
            while (reader.Read())
            {
                var employeeName = reader["workername"].ToString();
                var employe =
                    Context.Users.FirstOrDefault(u => u.ClinicGroupId == clinicGroupId && u.FirstName == employeeName);
                if (employe != null)
                {
                    var date = DateTime.MinValue;
                    DateTime.TryParse(reader["date"].ToString(), out date);

                    if (date > DateTime.MinValue)
                    {
                        var inHour = DateTime.Parse(reader["inhour"].ToString());

                        var attendance = new AttendanceLog()
                            {
                                ClinicId = clinicId,
                                UserId = employe.Id,
                                StartTime = date.AddHours(inHour.Hour).AddMinutes(inHour.Minute),
                            };

                        var outHourStr = reader["outhour"].ToString();
                        if (!string.IsNullOrWhiteSpace(outHourStr))
                        {
                            var outHour = DateTime.Parse(outHourStr);
                            attendance.EndTime = date.AddHours(outHour.Hour).AddMinutes(outHour.Minute);
                        }
                        Context.AttendanceLogs.Add(attendance);
                        SaveWithContextCleaning();
                        counter++;
                        migrationStatus.Status = counter.ToString();
                    }
                }
            }
            reader.Close();
            connection.Close();
        SaveWithContextCleaning(true);
            return true;
        }
    }
}
