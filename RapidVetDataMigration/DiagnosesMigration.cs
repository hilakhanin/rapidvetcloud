﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Visits;
using RapidVet.Repository;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class DiagnosesMigration : MigrationBase
    {
        public DiagnosesMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.rapidmed);
            var reader = GetReader(connection, "DiagnosisSentences");
            int counter = 0;
            while (reader.Read())
            {
                var diagnosis = new Diagnosis()
                    {
                        Active = true,
                        ClinicId = clinicId,
                        Name = reader["Sentence"].ToString(),
                        OldId = int.Parse(reader["RecordID"].ToString())
                    };
                Context.Diagnoses.Add(diagnosis);
                counter++;
                migrationStatus.Status = counter.ToString();
               SaveWithContextCleaning();
            }
            reader.Close();
            connection.Close();
         SaveWithContextCleaning(true);
            return true;
        }
    }
}
