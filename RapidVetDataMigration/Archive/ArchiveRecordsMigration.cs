﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Enums;
using RapidVet.Enums.DataMigration;
using RapidVet.Helpers;
using RapidVet.Model.Archives;
using RapidVet.Model.DataMigration;
using RapidVet.Repository.Archives;
using RapidVet.Repository.DataMigrationRepository;

namespace RapidVetDataMigration.Archive
{
    public class ArchiveRecordsMigration : MigrationBase
    {
        public ArchiveRecordsMigration(string dbPath)
        {
            DbPath = dbPath;
        }


        public HashSet<string> GetFilePaths(int clinicId)
        {
            var result = new HashSet<string>();

            var connection = GetConnection(DataBases.archive);
            var clinicClients =
                Context.Clients.Include(c => c.Patients).Where(c => c.ClinicId == clinicId && c.Patients.Any() && c.HsId != null).ToList();

            foreach (var client in clinicClients)
            {
                var fileReader = GetReader(connection, "Files", string.Format("PatID = {0}", client.HsId));
                while (fileReader.Read())
                {
                    var fileName = fileReader["FileName"].ToString();
                    result.Add(StringUtils.GetRootFolder(fileName));
                }
                fileReader.Close();

                foreach (var patient in client.Patients.Where(p => p.OldId.HasValue))
                {
                    var imageReader = GetReader(connection, "Images", string.Format("AnimalID = {0}", patient.OldId));
                    while (imageReader.Read())
                    {
                        var fileName = imageReader["FullName"].ToString();
                        result.Add(StringUtils.GetRootFolder(fileName));
                    }
                    imageReader.Close();
                }
            }
            connection.Close();
            return result;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, Dictionary<string, string> filePaths)
        {
            var connection = GetConnection(DataBases.archive);
            var clinicClients =
                Context.Clients.Include(c => c.Patients).Where(c => c.ClinicId == clinicId && c.Patients.Any() && c.HsId != null).ToList();

            foreach (var client in clinicClients)
            {
                var fileReader = GetReader(connection, "Files", string.Format("PatID = {0}", client.HsId));

                while (fileReader.Read())
                {
                    var patientName = fileReader["Description"].ToString();
                    var currentPatient = client.Patients.SingleOrDefault(p => p.Name == patientName);
                    var created = DateTime.Now;
                    DateTime.TryParse(fileReader["ArchiveDate"].ToString(), out created);
                    if (created == DateTime.MinValue || created.Year < 1990)
                    {
                        created = DateTime.Now;
                    }

                    var oldLocation = fileReader["FileName"].ToString();
                    var fileExtension = StringUtils.GetExtension(oldLocation);

                    var fileMigrationLocation = GetNewFileLocation(oldLocation, filePaths);

                    if (string.IsNullOrWhiteSpace(fileMigrationLocation))
                    {
                        LogFailedFile(clinicGroupId, clinicId, oldLocation);
                    }
                    var title = StringUtils.GetTitle(oldLocation);

                    if (currentPatient != null)
                    {
                        var archiveDocumant = new ArchiveDocument()
                            {
                                ClinicId = clinicId,
                                Patient = currentPatient,
                                Created = created,
                                FileType = ArchivesFileType.Document,
                                ShowInPatientHistory = true,
                                FileExtension = fileExtension,
                                ContentType = StringUtils.GetConetntType(fileExtension),
                                FileMigrationLocation = fileMigrationLocation,
                                Title = title
                            };

                        Context.ArchiveDocuments.Add(archiveDocumant);
                    }
                }
                fileReader.Close();


                foreach (var patient in client.Patients.Where(p => p.OldId.HasValue))
                {
                    var imageReader = GetReader(connection, "Images", string.Format("AnimalID = {0}", patient.OldId));
                    while (imageReader.Read())
                    {
                        var created = DateTime.Now;
                        DateTime.TryParse(imageReader["ImageDate"].ToString(), out created);
                        if (created == DateTime.MinValue || created.Year < 1990)
                        {
                            created = DateTime.Now;
                        }

                        var showInHistory = imageReader["ShowInRecord"].ToString() == "0";

                        var oldLocation = imageReader["FullName"].ToString();
                        var fileExtension = StringUtils.GetExtension(oldLocation);

                        var fileMigrationLocation = GetNewFileLocation(oldLocation, filePaths);

                        if (string.IsNullOrWhiteSpace(fileMigrationLocation))
                        {
                            LogFailedFile(clinicGroupId, clinicId, oldLocation);
                        }

                        var archiveDocumant = new ArchiveDocument()
                            {
                                ClinicId = clinicId,
                                Patient = patient,
                                Created = created,
                                FileType = ArchivesFileType.Document,
                                ShowInPatientHistory = showInHistory,
                                Title = imageReader["ShortName"].ToString(),
                                KeyWord1 = imageReader["Keyword1"].ToString(),
                                KeyWord2 = imageReader["Keyword2"].ToString(),
                                KeyWord3 = imageReader["Keyword3"].ToString(),
                                KeyWord4 = imageReader["Keyword4"].ToString(),
                                FileExtension = fileExtension,
                                ContentType = StringUtils.GetConetntType(fileExtension),
                                FileMigrationLocation = fileMigrationLocation
                            };
                      
                            Context.ArchiveDocuments.Add(archiveDocumant);
                        SaveWithContextCleaning();
                    }
                    imageReader.Close();


                }
            }
            Save();
            connection.Close();
            return true;
        }


        private string GetNewFileLocation(string oldLocation, Dictionary<string, string> filePaths)
        {
            var result = string.Empty;
            foreach (var path in filePaths.Where(path => oldLocation.StartsWith(path.Key)))
            {
                result = oldLocation.Replace(path.Key, path.Value);
            }
            return result;

        }

        private void LogFailedFile(int clinicGroupId, int clinicId, string oldLocation)
        {
            using (var repo = new DataMigrationRepository())
            {
                repo.LogFailedFile(clinicId, null, oldLocation);
            }
        }


    }
}
