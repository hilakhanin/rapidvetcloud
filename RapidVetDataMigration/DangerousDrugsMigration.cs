﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Visits;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class DangerousDrugsMigration : MigrationBase
    {
        public DangerousDrugsMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.rapidmed);
            var reader = GetReader(connection, "Toxines");
            int counter = 0;
            while (reader.Read())
            {
                var oldVisitId = long.Parse(reader["VisitID"].ToString());
                var visit = Context.Visits.SingleOrDefault(v => v.OldId == oldVisitId);
                if (visit != null)
                {
                    var medicationName = reader["ToxinName"].ToString();
                    var medications = Context.Medications.Where(m => m.Name == medicationName);
                    if (medications.Any())
                    {
                        var medication = medications.FirstOrDefault();
                        if (medication != null)
                        {
                            var destroyed = 0;
                            int.TryParse(reader["WrackQty"].ToString(), out destroyed);

                            var injected = 0;
                            int.TryParse(reader["InjectQty"].ToString(), out injected);

                            var dangerousDrug = new DangerousDrug()
                                {
                                    VisitId = visit.Id,
                                    MedicationId = medication.Id,
                                    AmountDestroyed = destroyed,
                                    AmountInjected = injected
                                };

                            Context.DangerousDrugs.Add(dangerousDrug);
                            SaveWithContextCleaning();
                            counter++;
                            migrationStatus.Status = counter.ToString();
                        }
                    }
                }
            }
            reader.Close();
            connection.Close();
            Save();
            return true;
        }
    }
}
