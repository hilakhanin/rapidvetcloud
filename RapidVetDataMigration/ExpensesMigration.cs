﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Expenses;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class ExpensesMigration : MigrationBase
    {

        public ExpensesMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            //int counter = 0;
            var connection = GetConnection(DataBases.rapidmed);
            var expenseGroups = Context.ExpenseGroups.Where(e => e.ClinicId == clinicId).ToList();
            var suppliers = Context.Suppliers.Where(s => s.ClinicId == clinicId).ToList();

            var reader = GetReader(connection, "Expenses");
            while (reader.Read())
            {
                var supplierName = reader["Supplier"].ToString();
                var supplier = suppliers.FirstOrDefault(s => s.Name == supplierName);

                var expenseGroupName = reader["Group"].ToString();
                var expenceGroup = expenseGroups.FirstOrDefault(g => g.Name == expenseGroupName);

                if (supplier != null && expenceGroup != null)
                {
                    var paymenyDate =GetDateTimeOrMin(reader["Date"].ToString());

                    if (paymenyDate > DateTime.MinValue && paymenyDate.Year > 1990)
                    {
                        var sumBeforeVat = GetDecimal(reader["SumBefore"].ToString());

                        var vat = GetDecimal(reader["SumPercent"].ToString());


                        Context.Expenses.Add(new Expense()
                            {
                                ExpenseGroupId = expenceGroup.Id,
                                SupplierId = supplier.Id,
                                Description = string.Format("{0} {1}", reader["Description"].ToString(), reader["Note"].ToString()),
                                PaymentDate = paymenyDate,
                                Vat = vat,
                                TotalAmount = sumBeforeVat * (1 + (vat / 100)),
                             //   Refference = reader["PaymentNumber"].ToString(),
                                InvoiceDate = paymenyDate,
                               // RecordNumber = int.Parse(reader["RecordId"].ToString())
                            });
                        SaveWithContextCleaning();
                    }
                }

            }

            reader.Close();
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }
    }
}
