﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RapidVet.Enums.DataMigration;

namespace RapidVetDataMigration
{
   public class MigrationFilesTest : MigrationBase
    {
       public MigrationFilesTest(string dbPath)
        {
            DbPath = dbPath;
        }

       public List<string> RunTest()
       {
           var errorList = new List<string>();
           if (!File.Exists(IniPath))
           {
               errorList.Add("Could not find file '" + IniPath + "'.");
           }
           foreach (DataBases db in (DataBases[])Enum.GetValues(typeof(DataBases)))
           {
               try
               {
                   var connection = GetConnection(db);
                   connection.Close();
               }
               catch (Exception ex)
               {
                   errorList.Add(ex.Message);
               }
           }
           return errorList;
       }

    }
}
