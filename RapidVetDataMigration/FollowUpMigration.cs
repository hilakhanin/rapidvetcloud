﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RapidVet.Enums;
using RapidVet.Enums.DataMigration;
using RapidVet.Helpers;
using RapidVet.Model.Clients;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Finance;
using RapidVet.Model.PatientFollowUp;
using RapidVet.Model.Users;
using RapidVet.Model;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class FollowUpMigration : MigrationBase
    {
        public FollowUpMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clincId, MigrationStageStatus migrationStatus)
        {
            int counter = 0;
            var connection = GetConnection(DataBases.recalls);
            var reader = GetReader(connection, "Recalls");
            while (reader.Read())
            {
                int animalId = GetInt(reader["AnimalID"].ToString());
                var patient =
                    Context.Patients.SingleOrDefault(p => p.OldId == animalId && p.Client.ClinicId == clincId);
                string doctorName = reader["Doctor1"].ToString();
                var doctor =
                    Context.Users.SingleOrDefault(u => u.ClinicGroupId == clinicGroupId && u.FirstName == doctorName);
                if (patient != null && doctor != null)
                {
                    var recallDate = GetDateTime(reader["Date"].ToString());

                    var folow = new FollowUp
                        {
                            PatientId = patient.Id,
                            UserId = doctor.Id,
                            Comment = reader["Note"].ToString(),
                            DateTime = recallDate,
                            Updated = recallDate,
                            Created = recallDate,
                        };
                    Context.FollowUps.Add(folow);
                    counter++;
                    migrationStatus.Status = counter.ToString();
                    SaveWithContextCleaning();
                }
            }
            SaveWithContextCleaning(true);
            reader.Close();
            connection.Close();
            return true;
        }
    }
}
