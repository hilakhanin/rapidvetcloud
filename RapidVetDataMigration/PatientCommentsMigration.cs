﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Helpers;
using RapidVet.Model.Patients;
using RapidVet.Model.Visits;
using RapidVet.Model;

namespace RapidVetDataMigration
{
    public class PatientCommentsMigration : MigrationBase
    {
        public PatientCommentsMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId)
        {
            var connection = GetConnection(DataBases.trtnotes);
            var reader = GetReader(connection, "Notes");
            while (reader.Read())
            {
                var patId = GetInt(reader["PatID"].ToString());
                var patient = Context.Patients.SingleOrDefault(p => p.Client.ClinicId == clinicId && p.OldId == patId);
                if (patient != null)
                {
                    var date = GetDateTime(reader["Date"].ToString());
                    var comment = new PatientComment()
                        {
                            CommentBody = reader["Note"].ToString(),
                            ValidThrough = date,
                            CreatedDate = date,
                            UpdatedDate = date,
                            Popup = GetBoolean(reader["Popup"].ToString()),
                            Active = true,
                            Patient = patient
                        };
                    Context.PatientComments.Add(comment);
                    SaveWithContextCleaning();
                }


            }
            reader.Close();
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }

    }
}
