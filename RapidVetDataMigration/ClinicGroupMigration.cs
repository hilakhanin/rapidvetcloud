﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Clinics;
using RapidVet.Model.FullCalender;
using RapidVet.Repository;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class ClinicGroupMigration : MigrationBase
    {
        public ClinicGroupMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public int RunMigration(int creatorId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.rapidmed);
            var reader = GetReader(connection, "Doctor Details");
            var result = 0;
            ClinicGroup clinicGroup = null;
            var now = DateTime.Now;
            var counter = 0;
            while (reader.Read() && counter == 0)
            {
                clinicGroup = new ClinicGroup()
                {
                    Active = true,
                    CreatedById = creatorId,
                    CreatedDate = now,
                    Name = reader["Name"].ToString(),
                    EmailAddress = reader["Email"].ToString(),
                    MigrationFolder = DbPath
                };
                migrationStatus.Status = clinicGroup.Name;
                Context.ClinicGroups.Add(clinicGroup);

                var clinic = new Clinic()
                    {
                        Active = true,
                        ClinicGroup = clinicGroup,
                        CreatedDate = now,
                        CreatedById = creatorId,
                        Name = reader["Name"].ToString(),
                        Fax = reader["Fax"].ToString(),
                        Email = reader["Email"].ToString(),
                        Phone = reader["Phone"].ToString(),
                        Address = reader["Address"].ToString(),
                        Rooms = new List<Room>()
                            {
                                new Room() {Active = true, Name = "1"},
                                new Room() {Active = true, Name = "2"},
                                new Room() {Active = true, Name = "3"},
                                new Room() {Active = true, Name = "4"},
                                new Room() {Active = true, Name = "5"},
                                new Room() {Active = true, Name = "6"},
                            }
                    };
                Context.Clinics.Add(clinic);
                counter++;
            }

            reader.Close();
            connection.Close();
            SaveWithContextCleaning(true);
            if ( clinicGroup != null && clinicGroup.Clinics.Any())
            {
                result = clinicGroup.Clinics.First().Id;
            }

            return result;
        }

    }
}
