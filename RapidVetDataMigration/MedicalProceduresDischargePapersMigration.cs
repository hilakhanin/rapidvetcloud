﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Data.Entity;
using RapidVet.Enums;
using RapidVet.Enums.DataMigration;
using RapidVet.Helpers;
using RapidVet.Model.Clients;
using RapidVet.Model.Patients;
using RapidVet.Model.Visits;
using RapidVet.Repository.Clinics;
using RapidVet.Repository.Patients;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class MedicalProceduresDischargePapersMigration : MigrationBase
    {
        public enum FormType
        {
            MedicalProcedure = 201,
            DischargePaper = 202,
            Sterilization = 5001
        };

        public MedicalProceduresDischargePapersMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.Forms);
            var reader = GetReader(connection, "Forms");
            int counter = 0;
            using (var letterRepo = new LetterTemplatesRepository())
            {
                using (var patientRepo = new PatientRepository())
                {
                    while (reader.Read())
                    {
                        var formTypeId = GetInt(reader["FormId"].ToString());

                        var oldClientId = GetInt(reader["PatId"].ToString());

                        var oldPatientId = 0;
                        Patient patient;

                        switch (formTypeId)
                        {
                            case (int)FormType.MedicalProcedure:
                                oldPatientId = GetInt(reader["Field13"].ToString());
                                patient = Context.Patients.SingleOrDefault(p => p.OldId == oldPatientId && p.Client.ClinicId == clinicId);
                                if (patient != null)
                                {
                                    GenerateMedicalProcedure(patient, reader, clinicId);
                                }
                                break;

                            case (int)FormType.DischargePaper:
                                oldPatientId = GetInt(reader["Field6"].ToString());
                                patient = Context.Patients.SingleOrDefault(p => p.OldId == oldPatientId && p.Client.ClinicId == clinicId);
                                if (patient != null)
                                {
                                    GenerateDiscargePaper(patient, reader);
                                }
                                break;

                            case (int)FormType.Sterilization:
                                oldPatientId = GetInt(reader["Field1"].ToString());
                                var patientId = patientRepo.GetPatientIdFromOldId(clinicId, oldPatientId);
                                if (patientId.HasValue && patientId.Value > 0)
                                {
                                    patient = patientRepo.GetPatientForLetter(patientId.Value);
                                    GenerateSterilizationForm(patient, reader, letterRepo);
                                }
                                break;
                        }
                        SaveWithContextCleaning();
                        counter++;
                        migrationStatus.Status = counter.ToString();
                    }
                }
                reader.Close();
            }
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }

        private void GenerateSterilizationForm(Patient patient, OleDbDataReader reader, LetterTemplatesRepository repo)
        {
            var date = reader["Field2"].ToString();
            var actionId = int.Parse(reader["Field3"].ToString());
            var action = actionId == 1
                             ? "בוצע עיקור/סירוס"
                             : "אושר ביצוע עיקור/סירוס";
            var patientFeatures = reader["Field6"].ToString();
            var copiesTo = reader["Field5"].ToString();
            var comments = reader["Summary"].ToString();


            var content = repo.GetFormattedSterilizationDocument(patient, date, action, patientFeatures, copiesTo,
                                                                 comments, patient.Client.Vet.Name,
                                                                 patient.Client.Vet.LicenceNumber);
            var letter = new Letter()
                {
                    Content = content,
                    PatientId = patient.Id,
                    ShowInPatientHistory = true,
                    LetterTemplateTypeId = (int)LetterTemplateType.Sterilization,
                    CreatedDate = GetDateTime(reader["Field7"].ToString())
                };
            Context.Letters.Add(letter);
        }


        private void GenerateDiscargePaper(Patient patient, OleDbDataReader reader)
        {
            var datetime = GetDateTimeOrMin(reader["Field3"].ToString());
            var checkUp = GetDateTimeOrMin(reader["Field4"].ToString());

            if (datetime > DateTime.MinValue && datetime.Year > 1990 && checkUp > DateTime.MinValue && checkUp.Year > 1990)
            {
                var dischargePaper = new DischargePaper()
                    {
                        PatientId = patient.Id,
                        From = reader["Field1"].ToString(),
                        To = reader["Field2"].ToString(),
                        Background = reader["Summary2"].ToString(),
                        Summery = reader["Summary"].ToString(),
                        Recommendations = reader["Summary3"].ToString(),
                        DateTime = datetime,
                        CheckUp = checkUp,
                    };

                Context.DischargePapers.Add(dischargePaper);
            }
        }

        private void GenerateMedicalProcedure(Patient patient, OleDbDataReader reader, int clinicId)
        {
            var diagnosisText = reader["Field1"].ToString();
            var diagnosis =
                Context.Diagnoses.FirstOrDefault(d => d.ClinicId == clinicId && d.Name == diagnosisText) ??
                new Diagnosis()
                    {
                        Active = true,
                        ClinicId = clinicId,
                        Name = diagnosisText
                    };



            var procedureType = int.Parse(reader["Field11"].ToString());

            var procedureTypeId = procedureType == 1
                                      ? (int)MedicalProcedureType.Surgery
                                      : (int)MedicalProcedureType.Anesthesiology;

            var startDate = GetDateTime(reader["Field3"].ToString());

            var start = StringUtils.ParseTimeString(startDate, reader["Field4"].ToString());
            var finish = StringUtils.ParseTimeString(startDate, reader["Field5"].ToString());

            if (startDate > DateTime.MinValue && startDate.Year > 1990)
            {
                var medicalProcedure = new MedicalProcedure()
                    {
                        Diagnosis = diagnosis,
                        SurgeonName = reader["Field6"].ToString(),
                        AnesthesiologistName = reader["Field7"].ToString(),
                        NurseName = reader["Field8"].ToString(),
                        AdditionalPersonnel = reader["Field9"].ToString(),
                        ProcedureLocation = reader["Field10"].ToString(),
                        ProcedureTypeId = procedureTypeId,
                        PatientId = patient.Id,
                        Start = start,
                        Finish = finish,
                        Summery = reader["Summary"].ToString(),
                        ProcedureName = reader["Field2"].ToString()
                    };

                Context.MedicalProcedures.Add(medicalProcedure);
            }
        }


    }
}
