﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;
using RapidVet.RapidConsts;
using RapidVetMembership;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class DoctorsMigration : MigrationBase
    {
        public DoctorsMigration(string dbPath)
        {
            DbPath = dbPath;
        }



        public bool RunMigration(int clinicGroupId, int clincId, MigrationStageStatus migrationStatus)
        {

            int counter = 1;
            var connection = GetConnection(DataBases.doctors);
            var reader = GetReader(connection, "Doctors");
            var roles = Context.Roles.ToList();
            var clinicGroupManagerRoleId =
                Context.Roles.Single(r => r.RoleName == RolesConsts.CLINICGROUPMANAGER).RoleId;
            var clinicManagerRoleId = Context.Roles.Single(r => r.RoleName == RolesConsts.CLINICMANAGER).RoleId;
            var doctorRoleId = Context.Roles.Single(r => r.RoleName == RolesConsts.DOCTOR).RoleId;
            var updateInventoryRoleId = Context.Roles.Single(r => r.RoleName == RolesConsts.INVENTORY_UPDATE).RoleId;
            while (reader.Read())
            {
                Issuer issuer = null;
                var issuerCompanyId = reader["Osek"].ToString();
                if (!string.IsNullOrWhiteSpace(issuerCompanyId))
                {
                    issuerCompanyId = issuerCompanyId.Trim(' ');
                    var length = issuerCompanyId.Length;
                    if (length < 9)
                    {
                        issuerCompanyId = issuerCompanyId.PadLeft(9, '0');
                    }
                    else if (length > 9)
                    {
                        var charsToRemove = length - 9;
                        var startIndex = charsToRemove - 1;

                        issuerCompanyId = issuerCompanyId.Remove(startIndex, charsToRemove);
                    }

                    issuer = new Issuer()
                   {
                       ClinicId = clincId,
                       CompanyId = issuerCompanyId,
                       Name = reader["DoctorName"].ToString(),
                       Active = !bool.Parse(reader["UnActive"].ToString()),
                   };

                    Context.Issuers.Add(issuer);
                    Context.SaveChanges();
                }

                var user = new User()
                    {
                        Username = "ru_" + clinicGroupId + "_" + counter,
                        FirstName = reader["DoctorName"].ToString(),
                        LastName = "",
                        Password = MigrationConsts.DEFAULT_USER_PASSWORD,
                        ClinicGroupId = clinicGroupId,
                        LicenceNumber = reader["License"].ToString(),
                        TitleId = 3,
                        IsApproved = true,
                        IsLockedOut = false,
                        CreateDate = DateTime.Now,
                        LastActivityDate = DateTime.Now,
                        PasswordFailuresSinceLastSuccess = 0,
                        LastPasswordFailureDate = DateTime.Now,
                        LastPasswordChangedDate = DateTime.Now,
                        LastLockoutDate = DateTime.Now,
                        LastLoginDate = DateTime.Now,
                        OldDoctorId = reader["RecordID"].ToString(),
                        UsersClinicsRoleses = new List<UsersClinicsRoles>()
                            {
                                new UsersClinicsRoles() {ClinicId = clincId, RoleId = clinicManagerRoleId},
                                new UsersClinicsRoles() {ClinicId = clincId, RoleId = doctorRoleId},
                                new UsersClinicsRoles() {ClinicId = clincId, RoleId = updateInventoryRoleId}
                            },
                        DefaultClinicId = clincId
                    };

                if (issuer != null)
                {
                    user.DefaultIssuerEmployerId = issuer.Id;
                }

                Context.Users.Add(user);
                counter++;
                migrationStatus.Status = counter.ToString();
            }
            reader.Close();
            connection.Close();
            Save();
            return true;
        }

        public void AddClinicGroupRoles(int clinicGroupId, int clinicId)
        {
            var users = Context.Users.Where(u => u.ClinicGroupId == clinicGroupId);
            var userNames = users.Select(u => u.Username).ToArray();
            var clinicGroupManagerRole = Context.Roles.Single(r => r.RoleName == RolesConsts.CLINICGROUPMANAGER);
            var roleArr = new string[] { clinicGroupManagerRole.RoleName };
            var roleProvider = new CodeFirstRoleProvider();
            roleProvider.AddUsersToRoles(userNames, roleArr);
        }

    }
}
