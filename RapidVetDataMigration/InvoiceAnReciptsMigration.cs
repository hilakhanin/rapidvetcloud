﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using RapidVet.Enums.DataMigration;
using RapidVet.Enums.Finances;
using RapidVet.Helpers;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.Users;
using RapidVet.RapidConsts;
using RapidVetMembership;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class InvoiceAnReciptsMigration : MigrationBase
    {
        public InvoiceAnReciptsMigration(string dbPath)
        {
            DbPath = dbPath;
        }



        public bool RunMigration(int clinicGroupId, int clincId, MigrationStageStatus migrationStatus)
        {
            var queryStr = string.Empty;
            var medConnection = GetConnection(DataBases.rapidmed);
            var taxReader = GetReader(medConnection, "Tax");
            int counter = 0;
            var taxList = new Dictionary<DateTime, decimal>();
            var isMultiIssuer = Context.Issuers.Count(i => i.ClinicId == clincId) > 1;

            while (taxReader.Read())
            {
                var dateString = taxReader["Date"].ToString();
                var splitDate = dateString.Split('/');
                var date = new DateTime(GetInt(splitDate[2]), GetInt(splitDate[1]), GetInt(splitDate[0]));
                var tax = GetDecimal(taxReader["Percentage"].ToString());
                tax = tax / 100;
                tax = tax + 1;
                taxList.Add(date, tax);
            }
            taxReader.Close();
            medConnection.Close();

            var connection = GetConnection(DataBases.Income);

            //invoice recipts & recipts
            var paymentReade = GetReader(connection, "Payments", "Cancelled = False and HMasNum is NULL");
            while (paymentReade.Read())
            {
                queryStr = string.Empty;

                var doctorName = paymentReade["DoctorName"].ToString();
                var issuer =
                    Context.Issuers.SingleOrDefault(
                        i => i.ClinicId == clincId && i.Name == doctorName);
                if (issuer == null)
                {
                    // if no manpik found use the first one
                    issuer = Context.Issuers.First(i => i.ClinicId == clincId);
                }
                if (paymentReade["For"].ToString() == "ריק")
                {
                    continue;
                }
                var doc = new FinanceDocument();
                if (paymentReade["HNum"].ToString() != "-" && paymentReade["HNum"].ToString() != "")
                {
                    doc.SerialNumber = GetInt(paymentReade["HNum"].ToString());
                    doc.FinanceDocumentType = FinanceDocumentType.InvoiceReceipt;
                }
                else if (paymentReade["KNum"].ToString() != "-" && paymentReade["KNum"].ToString() != "")
                {
                    doc.SerialNumber = GetInt(paymentReade["KNum"].ToString());
                    doc.FinanceDocumentType = FinanceDocumentType.Receipt;
                }
                else
                {
                    doc.SerialNumber = GetInt(paymentReade["RNum"].ToString());
                    doc.FinanceDocumentType = FinanceDocumentType.Refound;
                }
                doc.Created = GetDateTime(paymentReade["Date"].ToString());

                //tax
                doc.VAT = taxList.OrderBy(o => o.Key).First(o => o.Key <= doc.Created).Value;

                //find client
                var patId = GetInt(paymentReade["PatID"].ToString());
                if (patId > 0)
                {
                    var client = Context.Clients.SingleOrDefault(c => c.ClinicId == clincId && c.HsId == patId);
                    doc.Client = client;
                    doc.ClientName = client.Name;

                }
                else
                {
                    doc.ClientName = RapidVet.Resources.Migration.TempClient;
                }
                if (!string.IsNullOrEmpty(paymentReade["AlterName"].ToString()))
                {
                    doc.ClientName = paymentReade["AlterName"].ToString();
                }
                //finde issuer       

                doc.Issuer = issuer;

                //finde doctor

                var doctor =
                    Context.Users.SingleOrDefault(
                        i => i.ClinicGroupId == clinicGroupId && i.FirstName == doctorName);
                if (doctor == null)
                {
                    doctor =
                        Context.Users.First(
                            i => i.ClinicGroupId == clinicGroupId);
                }
                doc.User = doctor;
                doc.ClinicId = clincId;
                //date
                var visitDate = GetDateTime(paymentReade["Date"].ToString());
                var dateWithTime = StringUtils.ParseTimeString(visitDate, paymentReade["Time"].ToString());
                doc.Created = dateWithTime;
                //cheque
                if (!string.IsNullOrEmpty(paymentReade["Check"].ToString()))
                {
                    doc.TotalChequesPayment = GetDecimal(paymentReade["Check"].ToString());
                }
                //cash
                if (!string.IsNullOrEmpty(paymentReade["Cash"].ToString()))
                {
                    doc.CashPaymentSum = GetDecimal(paymentReade["Cash"].ToString());
                }
                //credit
                if (!string.IsNullOrEmpty(paymentReade["Credit"].ToString()))
                {
                    doc.TotalCreditPaymentSum = GetDecimal(paymentReade["Credit"].ToString());
                }
                //transfer
                if (!string.IsNullOrEmpty(paymentReade["BTransfer"].ToString()))
                {
                    doc.BankTransferSum = GetDecimal(paymentReade["BTransfer"].ToString());
                }
                if (doc.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt || doc.FinanceDocumentType == FinanceDocumentType.Receipt || doc.FinanceDocumentType == FinanceDocumentType.Refound)
                {
                    OleDbDataReader reader;
                    var issuarName = issuer.Name.Replace("'", "''");
                    if (doc.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt)
                    {
                         queryStr =
                            string.Format(
                                "[No]='{0}' AND Cancelled = False and DoctorName='{1}'AND HeshbonitMas = False AND Refund=False",
                                doc.SerialNumber, issuarName);
                         reader = GetReader(connection, "Heshbonit", queryStr);
                    }
                    else if (doc.FinanceDocumentType == FinanceDocumentType.Receipt)
                    {
                        queryStr = string.Format("[No] = '{0}' AND Cancelled = False and DoctorName='{1}'", doc.SerialNumber, issuarName);
                        reader = GetReader(connection, "Kabala", queryStr);
                    }
                    else //refund
                    {
                        queryStr =
                            string.Format(
                                "[No]='{0}'AND Cancelled = False AND DoctorName = '{1}' AND HeshbonitMas = False AND Refund = True",
                                doc.SerialNumber, issuarName);
                        reader = GetReader(connection, "Heshbonit", queryStr);
                    }
                    //payments
                    while (reader.Read())
                    {
                        //cheque
                        if (!string.IsNullOrEmpty(reader["Sum"].ToString()))
                        {
                            var payment = new FinanceDocumentPayment();
                            if (doc.FinanceDocumentType == FinanceDocumentType.Refound)
                            {
                                payment.Refound = doc;
                            }
                            else
                            {
                                payment.Recipt = doc;
                                if (doc.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt)
                                {
                                    payment.Invoice = doc;
                                }
                            }
                            payment.Sum = GetDecimal(reader["Sum"].ToString());
                            payment.PaymentType = PaymentType.Cheque;
                            payment.DueDate = GetDateTime(reader["Date"].ToString());
                            payment.BankBranch = reader["Snif"].ToString();
                            payment.BankAccount = reader["Acc"].ToString();
                            payment.ChequeNumber = reader["CheckNo"].ToString();
                            var bankName = reader["Bank"].ToString();

                            payment.BankCode = GetBankCode(bankName); ;

                            Context.FinanceDocumentPayments.Add(payment);
                        }
                        //cash
                        if (!string.IsNullOrEmpty(reader["Cash"].ToString()))
                        {
                            var payment = new FinanceDocumentPayment();
                            if (doc.FinanceDocumentType == FinanceDocumentType.Refound)
                            {
                                payment.Refound = doc;
                            }
                            else
                            {
                                payment.Recipt = doc;
                                if (doc.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt)
                                {
                                    payment.Invoice = doc;
                                }
                            }
                            payment.Sum = GetDecimal(reader["Cash"].ToString());
                            payment.PaymentType = PaymentType.Cash;
                            payment.DueDate = GetDateTime(reader["Date"].ToString()); ;
                            Context.FinanceDocumentPayments.Add(payment);

                        }
                        //credit
                        if (!string.IsNullOrEmpty(reader["TotalSum"].ToString()))
                        {
                            var payments = GetInt(reader["CreditPayments"].ToString());
                            //if (!int.TryParse(reader["CreditPayments"].ToString(), out payments))
                            //{
                            //    payments = 1;
                            //}
                            //else if (payments == 0)
                            //{
                            //    payments = 1;
                            //}
                            if (payments == 0)
                            {
                                payments = 1;
                            }

                            string creditCardNumber = reader["CreditNum"].ToString();
                            if (creditCardNumber.Length > 4)
                            {
                                // gets only the last 4 digidts since we can't store on the web the full number
                                creditCardNumber = creditCardNumber.Substring(creditCardNumber.Length - 4);
                            }

                            //credit type
                            var creditTypeName = reader["CreditType"].ToString();
                            var creditType = Context.IssuerCreditTypes.Local.SingleOrDefault(
                                iss => iss.IssuerId == issuer.Id && iss.Name == creditTypeName);
                            if (creditType == null)
                            {
                                creditType =
                                   Context.IssuerCreditTypes.SingleOrDefault(
                                       iss => iss.IssuerId == issuer.Id && iss.Name == creditTypeName);
                            }
                            if (creditType == null)
                            {
                                creditType = new IssuerCreditType()
                                    {
                                        Active = true,
                                        PostponedPayment = false,
                                        Name = creditTypeName,
                                        Issuer = issuer,
                                        Updated = DateTime.Now
                                    };
                                Context.IssuerCreditTypes.Add(creditType);
                            }

                            //credit company
                            var creditCodeName = reader["CreditComp"].ToString();
                            var creditCode =
                                Context.CreditCardCodes.Local.SingleOrDefault(
                                    c => c.ClinicId == clincId && c.Name == creditCodeName);
                            if (creditCode == null)
                            {
                                creditCode =
                                   Context.CreditCardCodes.SingleOrDefault(
                                       c => c.ClinicId == clincId && c.Name == creditCodeName);
                            }
                            if (creditCode == null)
                            {
                                creditCode = new CreditCardCode()
                                    {
                                        Active = true,
                                        ClinicId = clincId,
                                        Name = creditCodeName,
                                        Updated = DateTime.Now
                                    };
                                Context.CreditCardCodes.Add(creditCode);
                            }


                            for (int i = 0; i < payments; i++)
                            {
                                var payment = new FinanceDocumentPayment();
                                if (doc.FinanceDocumentType == FinanceDocumentType.Refound)
                                {
                                    payment.Refound = doc;
                                }
                                else
                                {
                                    payment.Recipt = doc;
                                    if (doc.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt)
                                    {
                                        payment.Invoice = doc;
                                    }
                                }
                                payment.PaymentType = PaymentType.CreditCard;
                                payment.Sum = GetDecimal(reader["TotalSum"].ToString());
                                payment.CreditOkNumber = reader["CreditClearance"].ToString();
                                payment.CreditDealNumber = reader["CreditShovar"].ToString();
                                payment.CardNumberLastFour = creditCardNumber;
                                if (i == 0)
                                {
                                    payment.Sum = GetDecimal(reader["CreditPayment"].ToString());
                                }
                                else
                                {
                                    payment.Sum = GetDecimal(reader["CreditEachPayment"].ToString());
                                }
                                var dueDate = dateWithTime.AddMonths(i);
                                payment.IssuerCreditType = creditType;
                                payment.CreditCardCode = creditCode;
                                Context.FinanceDocumentPayments.Add(payment);
                            }
                        }
                        //transfer
                        if (doc.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt && !string.IsNullOrEmpty(reader["BankTransferSum"].ToString()))
                        {
                            var payment = new FinanceDocumentPayment();
                            if (doc.FinanceDocumentType == FinanceDocumentType.Refound)
                            {
                                payment.Refound = doc;
                            }
                            else
                            {
                                payment.Recipt = doc;
                                if (doc.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt)
                                {
                                    payment.Invoice = doc;
                                }
                            }


                            payment.Sum = GetDecimal(reader["BankTransferSum"].ToString());
                            payment.PaymentType = PaymentType.Cheque;
                            payment.DueDate = GetDateTime(reader["Date"].ToString());
                            payment.BankBranch = reader["BankTransferSnif"].ToString();
                            payment.BankAccount = reader["BankTransferAcc"].ToString();
                            payment.BankTransferNumber = reader["BankTransferNum"].ToString();
                            payment.IsNoMham = reader["NoMham"].ToString() == "true" ? true : false;
                            var bankName = reader["BankTransferBank"].ToString();
                            payment.BankCode = GetBankCode(bankName);
                            Context.FinanceDocumentPayments.Add(payment);
                        }
                    }
                    reader.Close();
                }

                var itemsReader = GetReader(connection, "InvoiceItems", "PaymentID = " + paymentReade["RecordID"].ToString());
                while (itemsReader.Read())
                {
                    var quantity = int.Parse(itemsReader["ItemCount"].ToString());
                    decimal unitprice;
                    if (quantity != 0)
                    {
                        unitprice = ((GetDecimal(itemsReader["ItemPrice"].ToString()) / doc.VAT) / quantity);
                    }
                    else
                    {
                        unitprice = (GetDecimal(itemsReader["ItemPrice"].ToString()) / doc.VAT);
                    }
                    var item = new FinanceDocumentItem()
                        {
                            CatalogNumber = itemsReader["ItemCode"].ToString(),
                            UnitPrice = unitprice,
                            TotalBeforeVAT = GetDecimal(itemsReader["ItemTotalPrice"].ToString()) / doc.VAT,
                            Discount = GetDecimal(itemsReader["ItemDiscount"].ToString()) / doc.VAT,
                            Quantity = quantity,
                            Description = itemsReader["ItemDesc"].ToString(),
                            FinanceDocument = doc
                        };

                    int itemId = GetInt(itemsReader["ItemID"].ToString());
                    if (itemId > 0)
                    {
                        var DbItem =
                            Context.PriceListItems.FirstOrDefault(
                                p => p.Category.ClinicId == clincId && p.OldId == itemId);
                        if (DbItem != null)
                        {
                            item.PriceListItemId = DbItem.Id;
                        }
                    }

                    Context.FinanceDocumentItems.Add(item);

                }
                itemsReader.Close();
                Context.FinanceDocuments.Add(doc);
                SaveWithContextCleaning();
                counter++;
                migrationStatus.Status = counter.ToString();
            }

            paymentReade.Close();

            connection.Close();

            //need to save all unSaved recordes
           SaveWithContextCleaning(true);

            return true;
        }

        BankCode GetBankCode(string bankName)
        {
            var bankSplit = bankName.Split(' ');
            bankSplit = bankSplit.Where(s => !string.IsNullOrEmpty(s)).ToArray();
            BankCode bank = null;
            string bName = string.Empty;
            int bankNumber = 0;
            if (bankSplit.Length == 2)
            {
                bankNumber = GetInt(bankSplit[0]);
                bName = bankSplit[1];
                if (int.TryParse(bankSplit[0], out bankNumber))
                {
                    bank = Context.BankCodes.Local.FirstOrDefault(b => b.Name == bName);
                    if (bank == null)
                    {
                        bank = Context.BankCodes.FirstOrDefault(b => b.Name == bName);
                    }
                }
                else
                {
                    bName = string.Format("{0} {1}",bankSplit[0],bankSplit[0]);
                    bank = Context.BankCodes.Local.FirstOrDefault(b => b.Name == bName);
                    if (bank == null)
                    {
                        bank = Context.BankCodes.FirstOrDefault(b => b.Name == bName);
                    }
                }
            }
            else if (bankSplit.Length == 1)
            {
                if (int.TryParse(bankSplit[0], out bankNumber))
                {
                    bank = Context.BankCodes.Local.FirstOrDefault(b => b.Code == bankNumber);
                    if (bank == null)
                    {
                        bank = Context.BankCodes.FirstOrDefault(b => b.Code == bankNumber);
                    }
                }
                else
                {
                    bName = bankSplit[0];
                    if (bName == "הפועלים")
                    {
                        bName = "פועלים";
                    }
                    bank = Context.BankCodes.Local.FirstOrDefault(b => b.Name == bName);
                    if (bank == null)
                    {
                        bank = Context.BankCodes.FirstOrDefault(b => b.Name == bName);
                    }
                }
            }
            else if (bankSplit.Length > 2)
            {
                if (int.TryParse(bankSplit[0],out bankNumber))
                {
                    for (int i = 1; i < bankSplit.Length; i++)
                    {
                        bName += bankSplit[i] + " ";
                    }
                    bName = bName.Trim();
                    bank = Context.BankCodes.Local.FirstOrDefault(b => b.Code == bankNumber);
                    if (bank == null)
                    {
                        bank = Context.BankCodes.FirstOrDefault(b => b.Code == bankNumber);
                    }
                }
                else
                {
                    for (int i = 0; i < bankSplit.Length; i++)
                    {
                        bName += bankSplit[i] + " ";
                    }
                    bName = bName.Trim();


                    bank = Context.BankCodes.Local.FirstOrDefault(b => b.Name == bName);
                    if (bank == null)
                    {
                        bank = Context.BankCodes.FirstOrDefault(b => b.Name == bName);
                    }
                }

            }
            else
            {
                throw new Exception();
            }

            if (bank == null)
            {
                bank = new BankCode();
                bank.Name = bName;
                bank.Code = bankNumber;
                Context.BankCodes.Add(bank);
            }
            return bank;
        }
    }
}
