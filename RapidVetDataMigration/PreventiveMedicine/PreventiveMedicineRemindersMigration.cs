﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration.PreventiveMedicine
{
    public class PreventiveMedicineRemindersMigration : MigrationBase
    {
        public PreventiveMedicineRemindersMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            int counter = 0;
            var connection = GetConnection(DataBases.rapidmed);
            var clinicPatients =
                Context.Patients.Include(p => p.AnimalRace).Where(p => p.Client.ClinicId == clinicId).ToList();
        
            foreach (var patient in clinicPatients)
            {
                var query = string.Format("AnimalID = '{0}'",patient.OldId);
                var reader = GetReader(connection, "InnoculationsRecalls", query);
                while (reader.Read())
                {
                    var oldInnoculationId = string.Format("{0}-{1}", patient.AnimalRace.AnimalKindId,
                                                          reader["InocID"].ToString());
                    var lastPreformance =
                        Context.PreventiveMedicineItems.Where(
                            p =>
                            p.PatientId == patient.Id &&
                            p.PriceListItem.Category.ClinicId == clinicId &&
                            p.PriceListItem.OldInnoculationId == oldInnoculationId &&
                            p.Preformed.HasValue && p.Preformed.Value > DateTime.MinValue)
                               .OrderByDescending(p => p.Id)
                               .FirstOrDefault();


                    if (lastPreformance != null)
                    {
                        var date = GetDateTimeOrMin(reader["LastRecallDate"].ToString());

                        if (date > DateTime.MinValue && date.Year > 1990)
                        {
                            lastPreformance.Scheduled = date;
                            lastPreformance.ReminderCount = GetInt(reader["RecallCount"].ToString());
                            lastPreformance.ReminderComments = reader["Note"].ToString();

                            SaveWithContextCleaning();
                            counter++;
                            migrationStatus.Status = counter.ToString();
                        }
                    }


                }
                reader.Close();
            }
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }
    }
}
