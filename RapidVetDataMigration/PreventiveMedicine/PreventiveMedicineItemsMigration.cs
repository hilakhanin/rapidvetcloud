﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Finance;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class PreventiveMedicinePriceListItemsMigration : MigrationBase
    {
        public PreventiveMedicinePriceListItemsMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.rapidmed);
            int counter = 0;
            var categoryId = 0;
            var clinicTariffs = Context.Tariffs.Where(t => t.ClinicId == clinicId);
            var animalKinds = Context.AnimalKinds.Where(a => a.ClinicGroupId == clinicGroupId);
          
            foreach (var animalKind in animalKinds)
            {
                var query = string.Format("AnimalType = '{0}'",animalKind.Value);
                var reader = GetReader(connection, "InnoculationsDef", query);

                while (reader.Read())
                {
                    if (categoryId == 0)
                    {
                        var category =
                            Context.PriceListCategories.FirstOrDefault(
                                c => c.ClinicId == clinicId && (c.Name.Contains("חיסו") || c.Name.Contains("מונע")));

                        if (category == null)
                        {
                            category = new PriceListCategory()
                            {
                                Available = true,
                                ClinicId = clinicId,
                                Name = "חיסונים וטיפולים מונעים",
                            };

                            Context.PriceListCategories.Add(category);
                            Save();
                            categoryId = category.Id;

                        }
                        else
                        {
                            categoryId = category.Id;
                        }
                    }

                    var itemName = reader["InocName"].ToString();

                    if (string.IsNullOrWhiteSpace(itemName))
                    {
                        continue;
                    }

                    var price = GetDecimal(reader["InocPrice"].ToString());

                        var priceListItem =
                            Context.PriceListItems.FirstOrDefault(
                                i =>
                                i.Category.ClinicId == clinicId && i.Name == itemName)
                            ?? new PriceListItem()
                                {
                                    Available = true,
                                    CategoryId = categoryId,
                                    Name = itemName,
                                    ItemTypeId = itemName.Contains("חיסו")
                                                     ? (int)PriceListItemTypeEnum.Vaccines
                                                     : (int)PriceListItemTypeEnum.PreventiveTreatments,

                                };

                        priceListItem.OldInnoculationId = string.Format("{0}-{1}", animalKind.Id,
                                                                        reader["InocID"].ToString());

                        if (priceListItem.Id == 0)
                        {
                            foreach (var tariff in clinicTariffs)
                            {
                                priceListItem.ItemsTariffs.Add(new PriceListItemTariff()
                                    {
                                        TariffId = tariff.Id,
                                        TariffName = tariff.Name,
                                        Price = price,
                                        ItemId = priceListItem.Id
                                    });
                            }

                            Context.PriceListItems.Add(priceListItem);
                        }
                    
                    SaveWithContextCleaning();
                    counter++;
                    migrationStatus.Status = counter.ToString();
                }
                reader.Close();
            }

            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }
    }
}
