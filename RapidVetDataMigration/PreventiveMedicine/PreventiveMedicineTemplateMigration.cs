﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Clinics;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration.PreventiveMedicine
{
    public class PreventiveMedicineTemplateMigration : MigrationBase
    {
        public PreventiveMedicineTemplateMigration(String dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            int counter = 0;
            var connection = GetConnection(DataBases.rapidmed);
            var animalKinds = Context.AnimalKinds.Where(a => a.ClinicGroupId == clinicGroupId).ToList();

            foreach (var animalKind in animalKinds)
            {
                var query = string.Format("AnimalType = '{0}'",animalKind.Value);

                var reader = GetReader(connection, "InnoculationsMatrix", query);
                while (reader.Read())
                {
                    var parentOldInoculationId = string.Format("{0}-{1}", animalKind.Id, reader["ParentID"]);
                    var parentItem =
                        Context.PriceListItems.FirstOrDefault(
                            i => i.Category.ClinicId == clinicId && i.OldInnoculationId == parentOldInoculationId);

                    var parentVot = new VaccineOrTreatment()
                        {
                            AnimalKind = animalKind,
                            ClinicId = clinicId,
                            PriceListItem = parentItem,
                            Order = 0,
                        };


                    var childOldInnoculationId = string.Format("{0}-{1}", animalKind.Id, reader["ChildID"]);
                    var childItem =
                      Context.PriceListItems.FirstOrDefault(
                          i => i.Category.ClinicId == clinicId && i.OldInnoculationId == childOldInnoculationId);

                    var childVot = new VaccineOrTreatment()
                    {
                        AnimalKind = animalKind,
                        ClinicId = clinicId,
                        PriceListItem = childItem,
                        Order = 0,
                    };


                    var nvt = new NextVaccineOrTreatment()
                        {
                            ParentVaccineOrTreatment = parentVot,
                            VaccineOrTreatment = childVot,
                            Created = DateTime.Now,
                            DaysToNext = int.Parse(reader["DaysLapse"].ToString())
                        };

                    Context.NextVaccineAndTreatments.Add(nvt);

                    SaveWithContextCleaning();
                    counter++;
                    migrationStatus.Status = counter.ToString();
                }
            }
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }
    }
}
