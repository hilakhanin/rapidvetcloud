﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;
using RapidVet.RapidConsts;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class PreventiveMedicineRecordsMigration : MigrationBase
    {
        public PreventiveMedicineRecordsMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            int counter = 0;
            var connection = GetConnection(DataBases.rapidmed);
            var defaultDoctorId =
                    Context.Users.First(
                    u =>
                    u.ClinicGroupId == clinicGroupId &&
                    u.UsersClinicsRoleses.Any(r => r.ClinicId == clinicId && r.Role.RoleName == RolesConsts.DOCTOR)).Id;

            User PreformingDoctor = null;

            var clinicPatients =
                Context.Patients.Include("AnimalRace.AnimalKind").Where(p => p.Client.ClinicId == clinicId).ToList();

            var tested = false;
            var hasDoctorLisenceColumn = false;


            foreach (var patient in clinicPatients)
            {
                var query = string.Format("AnimalID = {0}", patient.OldId);
                var reader = GetReader(connection, "Innoculations", query);
                while (reader.Read())
                {

                    if (!tested)
                    {
                        hasDoctorLisenceColumn = HasColumn(reader, "DocLicense");
                        tested = true;
                    }

                    var oldInnoculationId = string.Format("{0}-{1}", patient.AnimalRace.AnimalKindId,
                                                          reader["InocID"].ToString());

                    var priceListItem =
                        Context.PriceListItems.FirstOrDefault(
                            i => i.Category.ClinicId == clinicId && i.OldInnoculationId == oldInnoculationId);

                    if (priceListItem == null)
                    {
                        continue;
                    }

                    var price = GetDecimal(reader["ShovarSum"].ToString());

                    var date = GetDateTimeOrMin(reader["InocDate"].ToString());
                    var externalyPreformed = GetBoolean(reader["Import"].ToString());

                    if (date > DateTime.MinValue && date.Year > 1900)
                    {
                        var item = new PreventiveMedicineItem()
                            {
                                PatientId = patient.Id,
                                Preformed = date,
                                PriceListItemId = priceListItem.Id,
                                PriceListItemTypeId = priceListItem.ItemTypeId,
                                ExternalyPreformed = externalyPreformed,
                                Price = price,
                                OldId = GetInt(reader["RecordID"].ToString())
                            };

                        if (hasDoctorLisenceColumn)
                        {
                            var doctorLicense = reader["DocLicense"].ToString();
                            if (!string.IsNullOrWhiteSpace(doctorLicense))
                            {
                                PreformingDoctor =
                                    Context.Users.FirstOrDefault(
                                        u => u.ClinicGroupId == clinicGroupId && u.LicenceNumber == doctorLicense);
                                if (PreformingDoctor != null)
                                {
                                    item.PreformingUserId = PreformingDoctor.Id;
                                }
                            }
                        }
                        else if (!externalyPreformed)
                        {

                            item.PreformingUserId = defaultDoctorId;
                        }

                        var visit = new Visit()
                            {
                                DoctorId =
                                    item.PreformingUserId == null ? defaultDoctorId : item.PreformingUserId.Value,
                                PatientId = patient.Id,
                                Price = item.Price,
                                VisitDate = date,
                                CreatedById = defaultDoctorId,
                                CreatedDate = date,
                                MainComplaint = RapidVet.Resources.Migration.PerformingPreventiveMedicineItem,
                                PreventiveMedicineItems = new List<PreventiveMedicineItem>() { item },
                                Close = true
                            };

                        Context.Visits.Add(visit);
                        SaveWithContextCleaning();
                        counter++;
                        migrationStatus.Status = counter.ToString();
                    }

                }
                reader.Close();
            }
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }
    }
}
