﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Users;
using RapidVet.RapidConsts;
using RapidVet.Repository;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
   public class EmployeesMigration : MigrationBase
    {
       public EmployeesMigration(string dbPath)
       {
           DbPath = dbPath;
       }

           public bool RunMigration(int clinicGroupId, int clincId, MigrationStageStatus migrationStatus)
           {
               var connection = GetConnection(DataBases.Presence);
               int counter = Context.Users.Count(u => u.ClinicGroupId == clinicGroupId) + 1;
               var reader = GetReader(connection, "workers");
               var secrateryRoleId = Context.Roles.Single(r => r.RoleName == RolesConsts.SECRATERY).RoleId;
               
            while (reader.Read())
            {
                var password = reader["password"].ToString();
                var user = new User()
                {
                    Username = "ru_" + clinicGroupId + "_" + counter,
                    FirstName = reader["workername"].ToString(),
                    LastName = "",
                    Password = !string.IsNullOrWhiteSpace(password) ? password : MigrationConsts.DEFAULT_USER_PASSWORD,
                    ClinicGroupId = clinicGroupId,
                    TitleId = 1,
                    IsApproved = true,
                    IsLockedOut = false,
                    CreateDate = DateTime.Now,
                    LastActivityDate = DateTime.Now,
                    PasswordFailuresSinceLastSuccess = 0,
                    LastPasswordFailureDate = DateTime.Now,
                    LastPasswordChangedDate = DateTime.Now,
                    LastLockoutDate = DateTime.Now,
                    LastLoginDate = DateTime.Now,
                    UsersClinicsRoleses = new List<UsersClinicsRoles>()
                            {
                                new UsersClinicsRoles() {ClinicId = clincId, RoleId = secrateryRoleId},
                            }
                };

                Context.Users.Add(user);
                counter++;
                migrationStatus.Status = counter.ToString();
                SaveWithContextCleaning();
            }
            reader.Close();
            connection.Close();
               Save();
            return true;
           }
    }
}
