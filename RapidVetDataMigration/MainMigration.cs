﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc.Async;
using RapidVet.Enums.DataMigration;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Repository;
using System.Web;
using RapidVetDataMigration.Labs;
using RapidVetDataMigration.PreventiveMedicine;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{

    //public class MigrationStageStatus
    //{
    //    public int MigrationStage { get; set; }

    //    public string Status { get; set; }
    //}

    public class MainMigration : MigrationBase
    {
        private RapidVetUnitOfWork _rapidVetUnitOfWork;
        protected RapidVetUnitOfWork RapidVetUnitOfWork
        {
            get { return _rapidVetUnitOfWork ?? (_rapidVetUnitOfWork = new RapidVetUnitOfWork()); }
        }


        public List<MigrationStageStatus> MigrationStageStatuses { get; private set; }

        public MigrationOrder CurrentStage { get; set; }

        public string _originPath = HttpContext.Current.Server.MapPath("/MigrationData/");
        public string _dbPath = string.Empty;
        public int _clinicGroupId = 0;
        public int _clinicId = 0;
        private int _userId;
        private Thread workingThread;

        public bool IsBusy
        {
            get
            {
                if (workingThread != null && workingThread.IsAlive && CurrentStage != MigrationOrder.Finish)
                {
                    return true;
                }
                return false;
            }
        }

        public MainMigration()
        {
            workingThread = new Thread(DoWork);
            MigrationStageStatuses = new List<MigrationStageStatus>();
            foreach (MigrationOrder val in Enum.GetValues(typeof(MigrationOrder)))
            {
                if (val != MigrationOrder.Finish)
                {
                    MigrationStageStatuses.Add(new MigrationStageStatus
                        {
                            MigrationStage = (int)val,
                            Status = "-"
                        });
                }
            }
        }

        public bool StartMigration(string dbPath, int userId)
        {
            if (!IsBusy)
            {
                _dbPath = dbPath;
                _userId = userId;
                foreach (var item in MigrationStageStatuses)
                {
                    item.Status = "-";
                }
                workingThread.Start();
                return true;
            }
            return false;
        }



        private void DoWork()
        {
            //Clinic
            CurrentStage = MigrationOrder.Clinic;
            var clinicMigrator = new ClinicGroupMigration(_dbPath);
            _clinicId = clinicMigrator.RunMigration(_userId, GetStatus(MigrationOrder.Clinic));
            if (_clinicId > 0)
            {
                _clinicGroupId = RapidVetUnitOfWork.ClinicGroupRepository.GetId(_clinicId);
                UpdateLog(MigrationOrder.Clinic);
                clinicMigrator.Dispose();
            }

            //Doctors
            CurrentStage = MigrationOrder.Doctors;
            var doctorMigrator = new DoctorsMigration(_dbPath);
            if (doctorMigrator.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.Doctors)))
            {
                UpdateLog(MigrationOrder.Doctors);
                doctorMigrator.Dispose();
            }

            //Employees
            CurrentStage = MigrationOrder.Employees;
            var employeesMigrator = new EmployeesMigration(_dbPath);
            if (employeesMigrator.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.Employees)))
            {
                UpdateLog(MigrationOrder.Employees);
                employeesMigrator.Dispose();
            }

            //PriceList
            CurrentStage = MigrationOrder.PriceList;
            var priceMigrator = new PriceListMigration(_dbPath);
            if (priceMigrator.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.PriceList)))
            {
                UpdateLog(MigrationOrder.PriceList);
                priceMigrator.Dispose();
            }

            //PreventiveMedicinePriceListItems
            CurrentStage = MigrationOrder.PreventiveMedicinePriceListItems;
            var preventiveMedicinePriceListItemsMigrator = new PreventiveMedicinePriceListItemsMigration(_dbPath);
            if (preventiveMedicinePriceListItemsMigrator.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.PreventiveMedicinePriceListItems)))
            {
                UpdateLog(MigrationOrder.PreventiveMedicinePriceListItems);
                preventiveMedicinePriceListItemsMigrator.Dispose();
            }

            //Clients
            CurrentStage = MigrationOrder.Clients;
            var clientsMigrator = new ClientMigration(_dbPath);
            if (clientsMigrator.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.Clients)))
            {
                UpdateLog(MigrationOrder.Clients);
                clientsMigrator.Dispose();
            }

            //Patients
            CurrentStage = MigrationOrder.Patients;
            var patientsMigrator = new PatientMigration(_dbPath);
            if (patientsMigrator.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.Patients)))
            {
                UpdateLog(MigrationOrder.Patients);
                //    RapidVetUnitOfWork.ClinicRepository.UpdateClientsSearchString(_clinicId);
                patientsMigrator.Dispose();
            }

            //Medications
            CurrentStage = MigrationOrder.Medications;
            var medicationMigration = new MedicationMigration(_dbPath);
            if (medicationMigration.RunMigration(_clinicGroupId, GetStatus(MigrationOrder.Medications)))
            {
                UpdateLog(MigrationOrder.Medications);
                medicationMigration.Dispose();
            }

            //Diagnoses
            CurrentStage = MigrationOrder.Diagnoses;
            var diagnosesMigration = new DiagnosesMigration(_dbPath);
            if (diagnosesMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.Diagnoses)))
            {
                UpdateLog(MigrationOrder.Diagnoses);
                diagnosesMigration.Dispose();
            }

            //Visits (ביקורים)
            CurrentStage = MigrationOrder.Visits;
            var visitMigration = new VisitMigration(_dbPath);
            var success = visitMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.Visits));
            if (success)
            {
                UpdateLog(MigrationOrder.Visits);
                visitMigration.Dispose();
            }

            //OldRapidVetTreatments (טיפולים)
            CurrentStage = MigrationOrder.OldRapidVetTreatments;
            var oldRapidVetTreatments = new OldVetTreatmentsMigration(_dbPath);
            success = oldRapidVetTreatments.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.OldRapidVetTreatments));
            if (success)
            {
                UpdateLog(MigrationOrder.OldRapidVetTreatments);
                oldRapidVetTreatments.Dispose();
            }

            //DangerousDrugs
            CurrentStage = MigrationOrder.DangerousDrugs;
            var dangerousDrugsMigration = new DangerousDrugsMigration(_dbPath);
            if (dangerousDrugsMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.DangerousDrugs)))
            {
                UpdateLog(MigrationOrder.DangerousDrugs);
                dangerousDrugsMigration.Dispose();
            }

            //MedicalProcedures
            CurrentStage = MigrationOrder.MedicalProcedures;
            var medicalProceduresDischargePapersMigration = new MedicalProceduresDischargePapersMigration(_dbPath);
            if (medicalProceduresDischargePapersMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.MedicalProcedures)))
            {
                UpdateLog(MigrationOrder.MedicalProcedures);
                medicalProceduresDischargePapersMigration.Dispose();
            }

            //Referrals
            CurrentStage = MigrationOrder.Referrals;
            var referralsMigration = new ReferralsMigration(_dbPath);
            if (referralsMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.Referrals)))
            {
                UpdateLog(MigrationOrder.Referrals);
                referralsMigration.Dispose();
            }

            //Suppliers
            CurrentStage = MigrationOrder.Suppliers;
            var suppliersMigration = new SuppliersMigration(_dbPath);
            if (suppliersMigration.RunSuppliersMigration(_clinicId, GetStatus(MigrationOrder.Suppliers)))
            {
                UpdateLog(MigrationOrder.Suppliers);
                suppliersMigration.Dispose();
            }

            //ExpenseGroups
            CurrentStage = MigrationOrder.ExpenseGroups;
            var expenseGroupsMigrator = new SuppliersMigration(_dbPath);
            if (expenseGroupsMigrator.RunExpenseGroupMigration(_clinicId, GetStatus(MigrationOrder.ExpenseGroups)))
            {
                UpdateLog(MigrationOrder.ExpenseGroups);
                expenseGroupsMigrator.Dispose();
            }

            //Quotations
            CurrentStage = MigrationOrder.Quotations;
            var quotationMigration = new QuotationMigration(_dbPath);
            if (quotationMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.Quotations)))
            {
                UpdateLog(MigrationOrder.Quotations);
                quotationMigration.Dispose();
            }

            //TelephoneDirectory
            CurrentStage = MigrationOrder.TelephoneDirectory;
            var telephoneDirectoryMigration = new TelephoneDirectoryMigration(_dbPath);
            if (telephoneDirectoryMigration.RunMigration(_clinicId, GetStatus(MigrationOrder.TelephoneDirectory)))
            {
                UpdateLog(MigrationOrder.TelephoneDirectory);
                telephoneDirectoryMigration.Dispose();

            }

            //Calendars
            CurrentStage = MigrationOrder.Calendars;
            var calendarMigration = new CalendarMigration(_dbPath);
            if (calendarMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.Calendars)))
            {
                UpdateLog(MigrationOrder.Calendars);
                calendarMigration.Dispose();
            }

            //Holidays
            CurrentStage = MigrationOrder.Holidays;
            var holidaysMigration = new HolidaysMigration(_dbPath);
            if (holidaysMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.Holidays)))
            {
                UpdateLog(MigrationOrder.Holidays);
                holidaysMigration.Dispose();
            }

            //Attendance
            CurrentStage = MigrationOrder.Attendance;
            var attendanceMigration = new AttendanceMigration(_dbPath);
            if (attendanceMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.Attendance)))
            {
                UpdateLog(MigrationOrder.Attendance);
                attendanceMigration.Dispose();
            }

            //PreventiveMedicineRecords
            CurrentStage = MigrationOrder.PreventiveMedicineRecords;
            var preventiveMedicineRecordsMigration = new PreventiveMedicineRecordsMigration(_dbPath);
            if (preventiveMedicineRecordsMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.PreventiveMedicineRecords)))
            {
                UpdateLog(MigrationOrder.PreventiveMedicineRecords);
                preventiveMedicineRecordsMigration.Dispose();
            }

            //PreventiveMedicineTemplates
            CurrentStage = MigrationOrder.PreventiveMedicineTemplates;
            var preventiveMedicineTemplateMigration = new PreventiveMedicineTemplateMigration(_dbPath);
            if (preventiveMedicineTemplateMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.PreventiveMedicineTemplates)))
            {
                UpdateLog(MigrationOrder.PreventiveMedicineTemplates);
                preventiveMedicineTemplateMigration.Dispose();
            }

            //PreventiveMedicineReminders
            CurrentStage = MigrationOrder.PreventiveMedicineReminders;
            var preventiveMedicineRemindersMigration = new PreventiveMedicineRemindersMigration(_dbPath);
            if (preventiveMedicineRemindersMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.PreventiveMedicineReminders)))
            {
                UpdateLog(MigrationOrder.PreventiveMedicineReminders);
                preventiveMedicineRemindersMigration.Dispose();
            }

            //LabsTemplates
            CurrentStage = MigrationOrder.LabsTemplates;
            var labsTemplateMigration = new LabsTemplateMigration(_dbPath);
            if (labsTemplateMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.LabsTemplates)))
            {
                UpdateLog(MigrationOrder.LabsTemplates);
                labsTemplateMigration.Dispose();
            }

            //LabResults
            CurrentStage = MigrationOrder.LabResults;
            var labResultsMigration = new LabResultsMigration(_dbPath);
            if (labResultsMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.LabResults)))
            {
                UpdateLog(MigrationOrder.LabResults);
                labResultsMigration.Dispose();
            }

            //InvoiceAndRecipts
            CurrentStage = MigrationOrder.InvoiceAndRecipts;
            var invoiceAnReciptsMigration = new InvoiceAnReciptsMigration(_dbPath);
            if (invoiceAnReciptsMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.InvoiceAndRecipts)))
            {
                UpdateLog(MigrationOrder.InvoiceAndRecipts);
                invoiceAnReciptsMigration.Dispose();
            }

            //FollowUp
            CurrentStage = MigrationOrder.FollowUp;
            var followUpMigration = new FollowUpMigration(_dbPath);
            if (followUpMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.FollowUp)))
            {
                UpdateLog(MigrationOrder.FollowUp);
                followUpMigration.Dispose();
            }

            //Expenses
            CurrentStage = MigrationOrder.Expenses;
            var expensesMigration = new ExpensesMigration(_dbPath);
            if (expensesMigration.RunMigration(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.Expenses)))
            {
                UpdateLog(MigrationOrder.Expenses);
                expensesMigration.Dispose();
            }

            CurrentStage = MigrationOrder.UpdateClients;
            var clientPatientsUpdater = new ClientPatientsUpdater(_clinicGroupId, _clinicId);
            if (clientPatientsUpdater.RunUpdator(_clinicGroupId, _clinicId, GetStatus(MigrationOrder.UpdateClients)))
            {
                UpdateLog(MigrationOrder.UpdateClients);
                expensesMigration.Dispose();
            }

            CurrentStage = MigrationOrder.Finish;
        }

        private MigrationStageStatus GetStatus(MigrationOrder order)
        {
            return MigrationStageStatuses.Single(s => s.MigrationStage == (int)order);
        }

        private void UpdateLog(MigrationOrder order)
        {

            var entry = new DataMigrationLog()
            {
                ClinicGroupId = _clinicGroupId,
                ClinicId = _clinicId,
                LastSuccessfullStageId = (int)order,
                LastSuccessfullStageName = order.ToString(),
                DateTime = DateTime.Now
            };
            var status = RapidVetUnitOfWork.DataMigrationRepository.UpdateLog(entry);


        }

    }
}
