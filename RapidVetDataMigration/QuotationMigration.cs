﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Finance;
using RapidVet.Model.Quotations;
using RapidVet.Repository.Finances;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class QuotationMigration : MigrationBase
    {
        public QuotationMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.trtplans);
            var reader = GetReader(connection, "TreatmentPlans ");
            int counter = 0;
            while (reader.Read())
            {
                var patientOldId = GetInt(reader["AnimalID"].ToString());
                var patient =
                    Context.Patients.SingleOrDefault(p => p.Client.ClinicId == clinicId && p.OldId == patientOldId);
                var doctorName = reader["Doctor"].ToString();
                var user =
                    Context.Users.SingleOrDefault(
                        u => u.FirstName == doctorName && u.UsersClinicsRoleses.Any(c => c.ClinicId == clinicId));
                if (patient != null && user != null)
                {
                    var created = GetDateTime(reader["Date"].ToString());

                    var quotation = new Quotation()
                        {
                            ClinicId = clinicId,
                            PatientId = patient.Id,
                            Created = created,
                            Comments = string.Format("{0} {1}", reader["DoctorNotes"], reader["Notes"]),
                          //  Discount = GetDecimal(reader["GlobalDiscount"].ToString()),
                            Name = reader["TreatmentPlanName"].ToString(),
                            OldId = GetInt(reader["TreatmentPlanID"].ToString()),
                            UserId = user.Id
                        };

                    var query = string.Format("TreatmentPlanID={0}", quotation.OldId);
                    var quotationItemsReader = GetReader(connection, "TreatmentInfo", query);
                    using (var repo = new PriceListRepository())
                    {
                        while (quotationItemsReader.Read())
                        {
                            var treatmentCode = quotationItemsReader["TreatCode"].ToString();
                            var priceListItem = repo.GetPriceListItemByCode(clinicId, treatmentCode);
                            if (priceListItem != null)
                            {
                                var quotationTreatment = new QuotationTreatment()
                                    {
                                        PriceListItemId = priceListItem.Id,
                                        Name = quotationItemsReader["TreatName"].ToString(),
                                        Discount = GetDecimal(quotationItemsReader["Discount"].ToString()),
                                        Comments = quotationItemsReader["Note"].ToString(),
                                        Price = GetDecimal(quotationItemsReader["Price"].ToString()),
                                        Amount = 1
                                    };
                                quotation.Treatments.Add(quotationTreatment);
                            }
                        }
                    }
                    Context.Quotations.Add(quotation);
                    SaveWithContextCleaning();
                    counter++;
                    migrationStatus.Status = counter.ToString();
                }
            }

            reader.Close();
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }
    }
}
