﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Data.Entity;
using System.Text;
using RapidVet.Enums;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Finance;
using RapidVetDataMigration;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class PriceListMigration : MigrationBase
    {
        public PriceListMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        private int counter = 0;
        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.pricelst);
            var result = false;
            var tariffsCreated = CreateTariffs(clinicId, connection);
            result = tariffsCreated;
            if (tariffsCreated)
            {
                var clinic = Context.Clinics.Include(c => c.Tariffs).Single(c => c.Id == clinicId);
                if (clinic.Tariffs.Any())
                {
                    clinic.DefualtTariffId = clinic.Tariffs.First().Id;
                }

                var categoriesCreated = CreateCategories(clinicId, connection,migrationStatus);
                result = categoriesCreated;
                if (categoriesCreated)
                {
                    var categories = Context.PriceListCategories.Where(c => c.ClinicId == clinicId).ToList();
                    var itemsCreated = CreatePriceListItems(connection, clinicId, categories ,  migrationStatus);
                    result = itemsCreated;
                    if (itemsCreated)
                    {
                        var priceListItems = Context.PriceListItems.Include(p => p.Category).Where(p => p.Category.ClinicId == clinicId).ToList();
                        var tariffs = Context.Tariffs.Where(t => t.ClinicId == clinicId).ToList();
                        result = UpdatePrices(connection, clinicId, priceListItems, tariffs);
                    }
                }
            }
            connection.Close();
            SaveWithContextCleaning(true);
            return result;
        }




        private bool CreateTariffs(int clinicId, OleDbConnection connection)
        {
            var reader = GetReader(connection, "PriceLists");
            while (reader.Read())
            {
                var tariff = new Tariff()
                    {
                        Active = true,
                        ClinicId = clinicId,
                        CreatedDate = DateTime.Now,
                        Name = reader["Name"].ToString(),
                        OldId = GetInt(reader["Number"].ToString())
                    };
                Context.Tariffs.Add(tariff);
                SaveWithContextCleaning();
            }
            reader.Close();
            SaveWithContextCleaning(true);
            return true;
        }

        private bool CreateCategories(int clinicId, OleDbConnection connection, MigrationStageStatus migrationStatus)
        {
            var reader = GetReader(connection, "CategoriesMap");
            var counter = 0;
            while (reader.Read())
            {
                var category = new PriceListCategory()
                    {
                        ClinicId = clinicId,
                        Available = true,
                        Name = reader["ChosenName"].ToString(),
                        ShortName = reader["ShortName"].ToString(),
                        OldId = GetInt(reader["קוד זיהוי"].ToString()),
                        OriginalName = reader["OriginalName"].ToString()
                    };

                Context.PriceListCategories.Add(category);
                SaveWithContextCleaning();
                counter++;
                migrationStatus.Status = counter.ToString();
            }
            SaveWithContextCleaning();
            reader.Close();
            return true;
        }

        private bool CreatePriceListItems(OleDbConnection connection, int clinicId, IEnumerable<PriceListCategory> categories, MigrationStageStatus status)
        {
            foreach (var category in categories)
            {

                var itemTypeId = (int)PriceListItemTypeEnum.Treatments;

                switch (category.Name)
                {
                    case "חיסונים":
                        itemTypeId = (int)PriceListItemTypeEnum.Vaccines;
                        break;
                    case "טיפולים מונעים":
                        itemTypeId = (int)PriceListItemTypeEnum.PreventiveTreatments;
                        break;
                }


                var reader = GetReader(connection, category.OriginalName);
                while (reader.Read())
                {
                    var priceListItem = new PriceListItem()
                        {
                            CategoryId = category.Id,
                            Available = true,
                            Name = reader["Name"].ToString(),
                            OldId = GetInt(reader["קוד זיהוי"].ToString()),
                            Barcode = reader["Barcode"].ToString(),
                            ItemTypeId = itemTypeId,
                            CreatedDate = DateTime.Now,
                            OldCode = reader["Code"].ToString()
                        };

                    Context.PriceListItems.Add(priceListItem);
                    counter++;
                    status.Status = counter.ToString();
                    SaveWithContextCleaning();
                }
                reader.Close();
            }
            SaveWithContextCleaning(true);
            return true;
        }

        private bool UpdatePrices(OleDbConnection connection, int clinicId, List<PriceListItem> priceListItems, List<Tariff> tariffs)
        {
            var reader = GetReader(connection, "Prices");
            while (reader.Read())
            {
                var oldTariffId = GetInt(reader["PriceList"].ToString());
                var oldPriceListItemsId = reader["TreatCode"].ToString();
                var tariff = tariffs.SingleOrDefault(t => t.OldId == oldTariffId);
                if (tariff != null)
                {
                    var priceListItem = priceListItems.FirstOrDefault(p => p.OldCode == oldPriceListItemsId);
                    if (priceListItem != null)
                    {
                        var itemTariff = new PriceListItemTariff()
                            {
                                ItemId = priceListItem.Id,
                                TariffId = tariff.Id,
                                TariffName = tariff.Name,
                                Price = GetDecimal(reader["Price"].ToString())
                            };
                        Context.PriceListItemsTariff.Add(itemTariff);
                    }
                }
                SaveWithContextCleaning();
            }
            reader.Close();
            SaveWithContextCleaning(true);
            return true;
        }
    }
}
