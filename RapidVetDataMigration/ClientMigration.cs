﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RapidVet.Enums;
using RapidVet.Enums.DataMigration;
using RapidVet.Model.Clients;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Users;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class ClientMigration : MigrationBase
    {
        public ClientMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clincId, MigrationStageStatus migrationStatus)
        {
            var counter = 0;
            var connection = GetConnection(DataBases.rapidmed);
            var reader = GetReader(connection, "Patients");


            //Status
            var iniReader = new IniReader(IniPath);
            var status0 = new ClientStatus()
                {
                    Active = true,
                    ClinicGroupId = clinicGroupId,
                    Name = RapidVet.Resources.Migration.Active
                };
            Context.ClientStatuses.Add(status0);
            var status1 = new ClientStatus()
            {
                Active = true,
                ClinicGroupId = clinicGroupId,
                Name = RapidVet.Resources.Migration.Temporary
            };
            Context.ClientStatuses.Add(status1);
            var status2 = new ClientStatus()
            {
                Active = true,
                ClinicGroupId = clinicGroupId,
                Name = RapidVet.Resources.Migration.NotActive
            };
            Context.ClientStatuses.Add(status2);
            var status3 = new ClientStatus()
            {
                Active = true,
                ClinicGroupId = clinicGroupId,
                Name = iniReader.GetValue("Special1", "Program Settings")
            };
            Context.ClientStatuses.Add(status3);
            var status4 = new ClientStatus()
            {
                Active = true,
                ClinicGroupId = clinicGroupId,
                Name = iniReader.GetValue("Special2", "Program Settings")
            };
            Context.ClientStatuses.Add(status4);
            var status5 = new ClientStatus()
            {
                Active = true,
                ClinicGroupId = clinicGroupId,
                Name = iniReader.GetValue("Special3", "Program Settings")
            };
            Context.ClientStatuses.Add(status5);
            var status6 = new ClientStatus()
            {
                Active = true,
                ClinicGroupId = clinicGroupId,
                Name = iniReader.GetValue("Special4", "Program Settings")
            };
            Context.ClientStatuses.Add(status6);
            var status7 = new ClientStatus()
            {
                Active = true,
                ClinicGroupId = clinicGroupId,
                Name = iniReader.GetValue("Special5", "Program Settings")
            };
            Context.ClientStatuses.Add(status7);
            Save();
            int statusId0 = status0.Id;
            int statusId1 = status1.Id;
            int statusId2 = status2.Id;
            int statusId3 = status3.Id;
            int statusId4 = status4.Id;
            int statusId5 = status5.Id;
            int statusId6 = status6.Id;
            int statusId7 = status7.Id;

            var showEmailExists = true;
            var showPhoneExists = true;
            var tested = false;

            while (reader.Read())
            {
                if (!tested)
                {
                    showEmailExists = HasColumn(reader, "ShowEMail");
                    showPhoneExists = HasColumn(reader, "ShowPhone");
                    tested = true;
                }

                var approveEmailTranfer = showEmailExists && GetBoolean(reader["ShowEMail"].ToString());
                var approveDisplayPhone = showPhoneExists && GetBoolean(reader["ShowPhone"].ToString());
                //string firstName, lastName;
                //try
                //{
                //    firstName = reader["Name"].ToString();
                //}
                //catch (Exception)
                //{

                //    firstName = " ";
                //}
                //try
                //{
                //    lastName = reader["Family Name"].ToString();
                //}
                //catch (Exception)
                //{

                //    lastName = " ";
                //}

                var client = new Client()
                    {
                        FirstName = reader["Name"].ToString(),
                        LastName = reader["Family Name"].ToString(),
                        IdCard = reader["ID"].ToString(),
                        VisibleComment = reader["Comments"].ToString(),
                        HiddenComment = reader["HiddenComments"].ToString(),
                        CreatedDate = reader["CreationDate"] as DateTime?,
                        UpdatedDate = reader["LastUpdate"] as DateTime?,
                        //Active = true,
                        HsId = reader["PatID"] as int?,
                        ApproveEmailTranfer = approveEmailTranfer,
                        ApproveDisplayPhone = approveDisplayPhone,
                        ClinicId = clincId,
                        ReferredBy = reader["Reference"].ToString(),
                        ExternalVet = reader["TVetName"].ToString()
                    };
                Context.Clients.Add(client);
                // add address 
                var cityName = reader["City"].ToString().Trim();
                var city = Context.Cities.Local.FirstOrDefault(c => c.Name == cityName);

                if (city == null)
                {
                    city = Context.Cities.FirstOrDefault(c => c.Name == cityName);
                    if (city == null)
                    {
                        var defaultRegionalCouncil =
                            Context.RegionalCouncils.Single(r => r.Name == RapidVet.Resources.Migration.WithOut);
                        city = new City()
                            {
                                Name = cityName,
                                Active = true,
                                RegionalCouncil = defaultRegionalCouncil
                            };
                    }

                }
                client.Addresses.Add(new Address()
                    {
                        City = city,
                        Street = reader["Address"].ToString(),
                        ZipCode = reader["ZIP Code"].ToString()
                    });
                // add alt address
                string altCityName = reader["AltCity"].ToString();
                var altCity = Context.Cities.FirstOrDefault(c => c.Name == altCityName);
                if (altCity == null)
                {
                    var defaultRegionalCouncil = Context.RegionalCouncils.Single(r => r.Name == RapidVet.Resources.Migration.WithOut);
                    altCity = new City()
                    {
                        Name = altCityName,
                        Active = true,
                        RegionalCouncil = defaultRegionalCouncil
                    };
                }
                if (!string.IsNullOrEmpty(reader["ALTADDR"].ToString()))
                    client.Addresses.Add(new Address()
                    {
                        City = altCity,
                        Street = reader["ALTADDR"].ToString(),
                        ZipCode = reader["AltZip"].ToString()
                    });
                //add Email
                var email = reader["EMAIL"].ToString();
                if (!string.IsNullOrEmpty(email))
                {
                    client.Emails.Add(new Email()
                        {
                            Name = email
                        });
                }
                // add phone
                var phone = reader["Phone"].ToString();
                if (!string.IsNullOrEmpty(phone))
                {
                    client.Phones.Add(new Phone()
                    {
                        PhoneNumber = phone,
                        PhoneTypeId = (int)PhoneTypeEnum.Home
                    });
                }
                // add phone
                var altPhone = reader["ALTPHONE"].ToString();
                if (!string.IsNullOrEmpty(altPhone))
                {
                    client.Phones.Add(new Phone()
                    {
                        PhoneNumber = altPhone,
                        PhoneTypeId = (int)PhoneTypeEnum.Home
                    });
                }
                //add work phone
                var workPhone = reader["Work Phone"].ToString();
                if (!string.IsNullOrEmpty(workPhone))
                {
                    client.Phones.Add(new Phone()
                        {
                            PhoneNumber = workPhone,
                            PhoneTypeId = (int)PhoneTypeEnum.Work,
                        });
                }
                // add fax number
                var fax = reader["Fax"].ToString();
                if (!string.IsNullOrEmpty(fax))
                {
                    client.Phones.Add(new Phone()
                    {
                        PhoneNumber = fax,
                        PhoneTypeId = (int)PhoneTypeEnum.Fax
                    });
                }
                // add cell phone
                var mobile = reader["Cell Phone"].ToString();
                if (!string.IsNullOrEmpty(mobile))
                {
                    client.Phones.Add(new Phone()
                    {
                        PhoneNumber = mobile,
                        PhoneTypeId = (int)PhoneTypeEnum.Mobile
                    });
                }
                // add cell phone 2
                var mobile2 = reader["CellPhone2"].ToString();
                if (!string.IsNullOrEmpty(mobile2))
                {
                    client.Phones.Add(new Phone()
                    {
                        PhoneNumber = mobile2,
                        PhoneTypeId = (int)PhoneTypeEnum.Mobile
                    });
                }
                // Client bithdate date is not conssetive waiting to dan's solution
                var birthdate = reader["Birthdate"].ToString();
                if (birthdate.Length == 8)
                {
                    var day = int.Parse(birthdate.Substring(0, 2));
                    var month = int.Parse(birthdate.Substring(2, 2));
                    var year = int.Parse(birthdate.Substring(4, 4));
                    if (month <= 12 && month > 0 && day > 0 && year > 0)
                    {
                        client.BirthDate = new DateTime(year, month, day);
                    }
                }
                // Status
                switch ((int)reader["Status"])
                {
                    case 0:
                        client.StatusId = statusId0;
                        break;
                    case 1:
                        client.StatusId = statusId1;
                        break;
                    case 2:
                        client.StatusId = statusId2;
                       // client.Active = false;
                        break;
                    case 3:
                        client.StatusId = statusId3;
                        break;
                    case 4:
                        client.StatusId = statusId4;
                        break;
                    case 5:
                        client.StatusId = statusId5;
                        break;
                    case 6:
                        client.StatusId = statusId6;
                        break;
                    case 7:
                        client.StatusId = statusId7;
                        break;
                }

                //Price List
                var priceList = (int)reader["PriceList"];
                if (priceList > 0)
                {
                    var tarrif = Context.Tariffs.SingleOrDefault(t => t.ClinicId == clincId && t.OldId == priceList);
                    if (tarrif != null)
                    {
                        //client.Tariff = tarrif;
                        client.TariffId = tarrif.Id;
                    }
                }


                // doctor
                var doctorName = reader["TreatingDoctor"].ToString();
                var docUser =
                    Context.Users.FirstOrDefault(u => u.ClinicGroupId == clinicGroupId && u.FirstName == doctorName);
                client.Vet = docUser;

                // prefix
                var prefix = reader["Prefix"].ToString();
                var title = Context.Titles.FirstOrDefault(t => t.Name == prefix);
                client.Title = title;
                client.Vet = docUser;

                SaveWithContextCleaning();
                counter++;
                migrationStatus.Status = counter.ToString();
            }
            reader.Close();
            connection.Close();
            Save();
            return true;
        }
    }
}
