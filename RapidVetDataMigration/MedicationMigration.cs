﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Helpers;
using RapidVet.Model.Visits;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class MedicationMigration : MigrationBase
    {
        public MedicationMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.medprob);
            var reader = GetReader(connection, "Drugs");
            int counter = 0;
            while (reader.Read())
            {
                DrugAdministration adminType;
                int drugAdministrationId = 0;

                if (DrugAdministration.TryParse(reader["DrugConfig"].ToString(), true, out adminType))
                {
                    drugAdministrationId = (int)adminType;
                }

                var medication = new Medication()
                    {
                        ClinicGroupId = clinicGroupId,
                        Active = true,
                        Name = reader["DrugName"].ToString(),
                        OldId = GetInt(reader["RecordId"].ToString()),
                        DangerousDrug = GetBoolean(reader["IsToxine"].ToString()),
                        UnitPrice = ToNullableDecimal(reader["Price"].ToString()),
                        Insturctions = reader["MethodOfUse"].ToString(),
                        MedicationAdministrationTypeId = drugAdministrationId,
                        TimesPerDay = GetInt(reader["PerDayQuantity"].ToString()),
                        NumberOfDays = 1,
                        Dosage = reader["DefaultQuantity"].ToString(),
                        DoctorInfo = reader["Notes"].ToString(),
                    };
                Context.Medications.Add(medication);
                counter++;
                migrationStatus.Status = counter.ToString();
                SaveWithContextCleaning();
            }
            reader.Close();
            connection.Close();
            SaveWithContextCleaning(true);
            return true;
        }

    }
}
