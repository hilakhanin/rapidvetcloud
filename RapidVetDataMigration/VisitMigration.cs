﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RapidVet.Enums;
using RapidVet.Enums.DataMigration;
using RapidVet.Helpers;
using RapidVet.Model.Clients;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Finance;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class VisitMigration : MigrationBase
    {
        public VisitMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.rapidmed);
            var reader = GetReader(connection, "VisitsNew");
            int counter = 0;
            while (reader.Read())
            {
                int animalId = GetInt(reader["AnimalID"].ToString());
                var patient =
                    Context.Patients.SingleOrDefault(p => p.OldId == animalId && p.Client.ClinicId == clinicId);
                string doctorName = reader["Doctor"].ToString();
                var doctor =
                    Context.Users.SingleOrDefault(u => u.ClinicGroupId == clinicGroupId && u.FirstName == doctorName);
                if (patient != null && doctor != null)
                {
                    var visitDate = GetDateTime(reader["VisitDate"].ToString());
                    var dateWithTime = StringUtils.ParseTimeString(visitDate, reader["Time"].ToString());

                    var visit = new Visit()
                        {
                            DoctorId = doctor.Id,
                            Patient = patient,
                            CreatedById = doctor.Id,
                            MainComplaint = reader["Complaint"].ToString(),
                            CreatedDate = dateWithTime,
                            VisitDate = dateWithTime,
                            OldId = GetInt(reader["RecordID"].ToString()),
                            Weight = GetDecimal(reader["Weight"].ToString()),
                            Temp = GetDecimal(reader["Temp"].ToString()),
                            Pulse = GetInt(reader["Dofek"].ToString()),
                            BPM = GetInt(reader["Breath"].ToString()),
                            BCS = GetInt(reader["BCS"].ToString()),
                            Mucosa = reader["Mucous"].ToString(),
                            Price = GetDecimal(reader["Price"].ToString()),
                            Close = true,
                            ClientIdAtTimeOfVisit = patient.ClientId,
                            Active = !GetBoolean(reader["Cancelled"].ToString())
                        };

                    patient.Visits.Add(visit);
                    var color = GetColor(reader["Color"].ToString());

                    //diagnosis
                    var diagnosisReader = GetReader(connection, "VisitDiagnosis", "AnimalID=" + patient.OldId);
                    while (diagnosisReader.Read())
                    {
                        var diagnosis = new VisitDiagnosis()
                            {
                                CategoryName = diagnosisReader["CatDiagnosis"].ToString(),
                                Name = diagnosisReader["Diagnosis"].ToString(),
                            };
                        visit.Diagnoses.Add(diagnosis);
                    }
                    diagnosisReader.Close();

                    //Exeminations
                    var testReader = GetReader(connection, "VisitTest", "Cancelled=0 and AnimalID=" + patient.OldId);
                    while (testReader.Read())
                    {
                        var exemination = new VisitExamination()
                        {
                            CategoryName = testReader["CatTest"].ToString(),
                            Name = testReader["Test"].ToString(),
                            Price = GetDecimal(testReader["TestPrice"].ToString()),
                            Quantity = 1,
                            Invoiced = GetBoolean(testReader["TestInvoiced"].ToString()),
                            Discount = 0
                        };
                        visit.Examinations.Add(exemination);
                    }
                    testReader.Close();

                    //Treatments
                    var treatmentReader = GetReader(connection, "VisitTreatments", "Cancelled=0 and AnimalID=" + patient.OldId);
                    while (treatmentReader.Read())
                    {
                        PriceListItem priceListItem = null;
                        var oldCode = treatmentReader["TreatCode"].ToString();
                        if (!string.IsNullOrWhiteSpace(oldCode))
                        {
                            priceListItem =
                                Context.PriceListItems.FirstOrDefault(
                                    p => p.Category.ClinicId == clinicId && p.OldCode == oldCode);
                        }
                        var treatment = new VisitTreatment()
                        {
                            CategoryName = treatmentReader["CatTreatment"].ToString(),
                            Name = treatmentReader["Treatment"].ToString(),
                            Price = GetDecimal(treatmentReader["TreatPrice"].ToString()),
                            UnitPrice = GetDecimal(treatmentReader["PriceTreatPU"].ToString()),
                            Quantity = (int)Math.Round(GetDecimal(treatmentReader["TreatAmt"].ToString())),
                            Invoiced = GetBoolean(treatmentReader["TreatInvoiced"].ToString()),
                            Discount = GetDecimal(treatmentReader["TreatDiscount"].ToString()),


                        };
                        if (priceListItem != null)
                        {
                            treatment.PriceListItem = priceListItem;
                        }
                        visit.Treatments.Add(treatment);
                    }
                    treatmentReader.Close();

                    //Medications
                    var drugsReader = GetReader(connection, "VisitDrugs", "Cancelled=0 and AnimalID=" + patient.OldId);
                    while (drugsReader.Read())
                    {
                        var drugType = drugsReader["DrugMethod"].ToString();
                        DrugAdministration adminType;
                        int drugAdministrationId = 0;

                        if (DrugAdministration.TryParse(drugType, true, out adminType))
                        {
                            drugAdministrationId = (int)adminType;
                        }

                        var medication = new VisitMedication()
                            {
                                Dosage = drugsReader["DrugDosage"].ToString(),
                                Instructions = drugsReader["DrugMethod"].ToString(),
                                Name = drugsReader["Drug"].ToString(),
                                Invoiced = GetBoolean(drugsReader["DrugInvoiced"].ToString()),
                                TimesPerDay = GetInt(drugsReader["DrugDailyQty"].ToString()),
                                NumberOfDays = GetInt(drugsReader["DrugPeriod"].ToString()),
                                DrugAdministrationId = drugAdministrationId,
                                OverallAmount = 1,
                                UnitPrice = GetDecimal(drugsReader["DrugPrice"].ToString()),
                                Total = GetDecimal(drugsReader["DrugPrice"].ToString())
                            };

                        visit.Medications.Add(medication);
                    }
                    drugsReader.Close();
                }
                counter++;
                migrationStatus.Status = counter.ToString();
                SaveWithContextCleaning();

            }
            SaveWithContextCleaning(true);
            reader.Close();
            connection.Close();
            return true;
        }
    }
}
