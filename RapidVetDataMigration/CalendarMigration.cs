﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;
using RapidVet.Helpers;
using RapidVet.Model.FullCalender;
using RapidVet.WebModels.DataMigration;

namespace RapidVetDataMigration
{
    public class CalendarMigration : MigrationBase
    {
        public CalendarMigration(string dbPath)
        {
            DbPath = dbPath;
        }

        public bool RunMigration(int clinicGroupId, int clinicId, MigrationStageStatus migrationStatus)
        {
            var connection = GetConnection(DataBases.applog);
            var reader = GetReader(connection, "פגישות");
            var hasGoogleId = false;
            var hasCancelled = false;
            var tested = false;
            int counter = 0;
            while (reader.Read())
            {
                if (!tested)
                {
                    hasCancelled = HasColumn(reader, "Cancelled");
                    hasGoogleId = HasColumn(reader, "GoogleID");
                    tested = true;
                }
                var googleId = string.Empty;
                if (hasGoogleId)
                {
                    googleId = reader["GoogleID"].ToString();
                }

                var cancelled = false;
                if (hasCancelled)
                {
                    cancelled = GetBoolean(reader["Cancelled"].ToString());
                }


                var doctorName = reader["DoctorName"].ToString();
                var doctor =
                    Context.Users.SingleOrDefault(u => u.ClinicGroupId == clinicGroupId && u.FirstName == doctorName);

                var createdByName = reader["EnteredBy"].ToString();
                var createdBy =
                   Context.Users.SingleOrDefault(u => u.ClinicGroupId == clinicGroupId && u.FirstName == createdByName);

                if (doctor != null)
                {
                    var clientOldId = GetInt(reader["PatID"].ToString());
                    var client = Context.Clients.SingleOrDefault(c => c.ClinicId == clinicId && c.HsId == clientOldId);
                    int? clientId = null;
                    if (client != null)
                    {
                        clientId = client.Id;
                    }

                    var patientOldId = GetInt(reader["AnimalID"].ToString());
                    var patient =
                        Context.Patients.SingleOrDefault(p => p.OldId == patientOldId && p.Client.ClinicId == clinicId);
                    int? patientId = null;
                    if (patient != null)
                    {
                        patientId = patient.Id;
                    }

              
                    var date = GetDateTimeOrMin(reader["Date"].ToString());

                    if (date > DateTime.MinValue)
                    {
                        var dateTime = StringUtils.ParseTimeString(date, reader["time"].ToString());

                        var entry = new CalenderEntry()
                            {
                                ClinicId = clinicId,
                                DoctorId = doctor.Id,
                                CreatedByUserId = createdBy != null ? createdBy.Id : doctor.Id,
                                ClientId = clientId,
                                PatientId = patientId,
                                OldId = GetInt(reader["RecordID"].ToString()),
                                Date = dateTime,
                                Duration = (int)(GetDecimal(reader["Length"].ToString()) * 60),
                                IsNoShow = GetBoolean(reader["NoShow"].ToString()),
                                IsNewClient = GetBoolean(reader["NewPatient"].ToString()),
                                IsCancelled = cancelled,
                                GoogleId = googleId,
                                Comments = reader["Note"].ToString(),
                                Title = reader["Note"].ToString()
                            };

                        var updatedStr = reader["LastUpdated"].ToString();

                        if (!string.IsNullOrEmpty(updatedStr))
                        {
                            entry.Updated = StringUtils.ParseStringToDateTime(updatedStr);
                        }

                        var color = GetColor(reader["Symbol"].ToString());

                        var roomName = reader["Chair"].ToString();
                        var room = Context.Rooms.SingleOrDefault(r => r.ClinicId == clinicId && r.Name == roomName);
                        if (room != null)
                        {
                            entry.RoomId = room.Id;
                        }

                        Context.CalenderEntries.Add(entry);
                        SaveWithContextCleaning();
                        counter++;
                        migrationStatus.Status = counter.ToString();
                    }
                }
            }
            reader.Close();
            connection.Close();
       SaveWithContextCleaning(true);
            return true;
        }
    }
}
