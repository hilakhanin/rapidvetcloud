﻿using System;
using System.Collections.Generic;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class Visit 
    {        
        public int Id { get; set; }
        public Nullable<int> PatientId { get; set; }
        public DateTime VisitDate { get; set; }
        public string MainComplaint { get; set; }
        public string Doctor { get; set; }
        public decimal Weight { get; set; }
        public decimal Temp { get; set; }
        public int BPM { get; set; }
        public int BCS { get; set; }
        public int Pulse { get; set; }
        public string Mucosa { get; set; }
        public string Color { get; set; }
        public decimal? Price { get; set; }
        private List<VisitExamination> _examinations;
        public List<VisitExamination> Examinations
        {
            get { return _examinations ?? (_examinations = new List<VisitExamination>()); }
            set { _examinations = value; }
        }
        private List<VisitDiagnosis> _diagnoses;
        public List<VisitDiagnosis> Diagnoses
        {
            get { return _diagnoses ?? (_diagnoses = new List<VisitDiagnosis>()); }
            set { _diagnoses = value; }
        }
        private List<VisitTreatment> _treatments;
        public List<VisitTreatment> Treatments
        {
            get { return _treatments ?? (_treatments = new List<VisitTreatment>()); }
            set { _treatments = value; }
        }
        private List<VisitMedication> _medications;
        public List<VisitMedication> Medications
        {
            get { return _medications ?? (_medications = new List<VisitMedication>()); }
            set { _medications = value; }
        }
        public bool Close { get; set; }
        private List<DangerousDrug> _dangerousDrugs;
        public List<DangerousDrug> DangerousDrugs
        {
            get { return _dangerousDrugs ?? (_dangerousDrugs = new List<DangerousDrug>()); }
            set { _dangerousDrugs = value; }
        }
        public int? ReciptId { get; set; }
        public bool Active { get; set; }
        public bool? DebtVisit { get; set; }
        private List<VitalSign> _vitalSigns;
        public List<VitalSign> VitalSigns
        {
            get { return _vitalSigns ?? (_vitalSigns = new List<VitalSign>()); }
            set { _vitalSigns = value; }
        }
    }
}
