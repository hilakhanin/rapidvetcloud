﻿using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class DangerousDrug
    {
        public int Id { get; set; }
        public string Medication { get; set; }
        public int VisitId { get; set; }
        public decimal AmountInjected { get; set; }
        public decimal AmountDestroyed { get; set; }
    }
}
