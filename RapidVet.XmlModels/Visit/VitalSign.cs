﻿using System;

namespace RapidVet.XmlModels
{
   [Serializable]   
    public class VitalSign
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public string VitalSignType { get; set; }
        public DateTime Time { get; set; }
        public decimal Value { get; set; }
    }
}
