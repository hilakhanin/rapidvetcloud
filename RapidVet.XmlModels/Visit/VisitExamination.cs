﻿using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class VisitExamination
    {
        public int Id { get; set; }
        public int VisitId { get; set; }
        public string PriceListItem { get; set; }
        public string Name { get; set; }
        public string CategoryName { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public decimal Price { get; set; }
        public bool Invoiced { get; set; }
        public string AddedBy { get; set; }
    }
}
