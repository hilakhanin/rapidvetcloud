﻿using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class VisitDiagnosis
    {
        public int Id { get; set; }
        public int VisitId { get; set; }
        public string Diagnosis { get; set; }
        public string Name { get; set; }
        public string CategoryName { get; set; }
        public string AddedBy { get; set; }        
    }
}
