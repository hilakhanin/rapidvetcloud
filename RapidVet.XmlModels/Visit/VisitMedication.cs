﻿
using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class VisitMedication
    {
        public int Id { get; set; }
        public int VisitId { get; set; }
        public string Medication { get; set; }
        public string Name { get; set; }
        public string Dosage { get; set; }
        public int? TimesPerDay { get; set; }
        public int? NumberOfDays { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Total { get; set; }
        public string Instructions { get; set; }
        public decimal OverallAmount { get; set; }
        public decimal Discount { get; set; }
        public bool Invoiced { get; set; }
        public string AddedBy { get; set; }
    }
}
