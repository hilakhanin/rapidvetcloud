﻿using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class FinanceDocumentPayment
    {
        public int Id { get; set; }
        public int? InvoiceId { get; set; }
        public int? ReciptId { get; set; }
        public int? RefoundId { get; set; }
        public string Issuer { get; set; }
        public int ClinicId { get; set; }
        public string PaymentType { get; set; }
        public decimal Sum { get; set; }
        public bool? IsEasyCard { get; set; }
        public String CreditDealNumber { get; set; }
        public String CreditOkNumber { get; set; }
        public String CardNumberLastFour { get; set; }
        public String CreditValidDate { get; set; }
        public string IssuerCreditType { get; set; }
        public string CreditCardCode { get; set; }
        public DateTime? DueDate { get; set; }
        public string BankCode { get; set; }
        public String BankBranch { get; set; }
        public String BankAccount { get; set; }
        public String ChequeNumber { get; set; }
        public String BankTransferNumber { get; set; }
        public decimal? WitholdingTax { get; set; } //this is NIKUI BAMAKOR
        public DateTime? Created { get; set; }
    }
}
