﻿using System;
using System.Collections.Generic;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class FinanceDocument
    {
        public int Id { get; set; }
        public int SerialNumber { get; set; }
        public int ClinicId { get; set; }
        public int? ClientId { get; set; }
        public string User { get; set; }
        public string Issuer { get; set; }
        public string FinanceDocumentType { get; set; }
        public String ClientName { get; set; }
        public String ClientAddress { get; set; }
        public String ClientPhone { get; set; }
        public List<FinanceDocumentPayment> Payments { get; set; }
        public decimal TotalCreditPaymentSum { get; set; }
        public decimal TotalChequesPayment { get; set; }
        public decimal CashPaymentSum { get; set; }
        public decimal BankTransferSum { get; set; }
        public List<FinanceDocumentItem> Items { get; set; }
        public decimal VAT { get; set; }
        public decimal VatPrcent{ get; set; }
        public Decimal Discount { get; set; }
        public Decimal RoundingFactor { get; set; }
        public decimal SumToPayForInvoice { get; set; }
        public Decimal TotalSum{ get; set; }
        public Decimal VatPayed { get; set; }
        public Decimal TotalBeforeVAT { get; set; }
        public Decimal TotalBeforeVATAfterDisscount{ get; set; }
        public String Comments { get; set; }
        public DateTime Created { get; set; }
        public DateTime? OrginalPrintedDate { get; set; }
        public bool ConvertedFromRecipt { get; set; }
        public int ConvertedFromReciptSerialNumber { get; set; }
        public decimal AmountPayed { get; set; }
        public string DocumentCloseNote { get; set; }
        public bool? ReciptPriningOnly { get; set; }
        public string SignatureKey { get; set; }
    }
}
