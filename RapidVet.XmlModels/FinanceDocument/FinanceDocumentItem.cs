﻿using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class FinanceDocumentItem
    {
        public int Id { get; set; }
        public int? VisitId { get; set; }
        public String Description { get; set; }
        public String CatalogNumber { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? Discount { get; set; }
        public decimal? DiscountPercentage { get; set; }
        public decimal TotalBeforeVAT { get; set; }
        public int? PriceListItemId { get; set; }
        public decimal TotalDiscount { get; set; }
        public string AddedBy { get; set; }
    }
}
