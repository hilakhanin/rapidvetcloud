﻿using System.Collections.Generic;
using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class LabTestTemplate
    {
        public int Id { get; set; }
        public int LabTestTypeId { get; set; }
        public string Name { get; set; }
        public string AnimalKind { get; set; }
        private List<LabTest> _labTestResults;
        public List<LabTest> LabTestResults
        {
            get { return _labTestResults ?? (_labTestResults = new List<LabTest>()); }
            set { _labTestResults = value; }
        }
    }
}
