﻿using System;
using System.Collections.Generic;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class LabTestType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        private List<LabTestTemplate> _animalKinds;
        public List<LabTestTemplate> AnimalKinds
        {
            get { return _animalKinds ?? (_animalKinds = new List<LabTestTemplate>()); }
            set { _animalKinds = value; }
        }
    }
}
