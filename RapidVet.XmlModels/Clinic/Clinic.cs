﻿using System;
using System.Collections.Generic;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class Clinic 
    {
        public int Id { get; set; }
        public string Name { get; set; }

        private List<LabTestType> _labTests;
        public List<LabTestType> LabTests
        {
            get { return _labTests ?? (_labTests = new List<LabTestType>()); }
            set { _labTests = value; }
        }

        private List<Client> _clients;
        public List<Client> Clients
        {
            get { return _clients ?? (_clients = new List<Client>()); }
            set { _clients = value; }
        }      
    }
}
