﻿using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class LabTest
    {
        public int Id { get; set; }
        public int LabTestTemplateId { get; set; }
        public string Name { get; set; }
        public decimal MinValue { get; set; }
        public decimal MaxValue { get; set; }
    }
}
