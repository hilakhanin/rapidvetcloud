﻿
using System;
namespace RapidVet.XmlModels
{
    [Serializable]
    public class Phone 
    {    
        public string PhoneNumber { get; set; }
        public int PhoneTypeId { get; set; }
    }
}
