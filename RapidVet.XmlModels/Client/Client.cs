﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;


namespace RapidVet.XmlModels
{
    [Serializable]
    public class Client 
    {        
        public int Id { get; set; }
        public int LocalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdCard { get; set; }       
        public int? TitleId { get; set; }
        public DateTime? BirthDate { get; set; }
        public string WorkePlace { get; set; }
        public string Practice { get; set; }
        public string ReferredBy { get; set; }
        public string ExternalVet { get; set; }
        public int? VetId { get; set; }

        private List<Address> _addresses;
        public List<Address> Addresses
        {
            get { return _addresses ?? (_addresses = new List<Address>()); }
            set { _addresses = value; }
        }
       
        private List<Phone> _phones;
        public List<Phone> Phones
        {
            get { return _phones ?? (_phones = new List<Phone>()); }
            set { _phones = value; }
        }

        private List<Email> _emails;
        public List<Email> Emails
        {
            get { return _emails ?? (_emails = new List<Email>()); }
            set { _emails = value; }
        }


        private List<Patient> _patients;
        public List<Patient> Patients
        {
            get { return _patients ?? (_patients = new List<Patient>()); }
            set { _patients = value; }
        }

        public bool ApproveEmailTranfer { get; set; }
        public bool ApproveDisplayPhone { get; set; }
        public int ClinicId { get; set; }
        public bool Active { get; set; }
        public int? StatusId { get; set; }
        public string VisibleComment { get; set; }
        public string HiddenComment { get; set; }
        public string RegionalCounil { get; set; }
        private List<FinanceDocument> _financeDocuments;
        public List<FinanceDocument> FinanceDocuments
        {
            get { return _financeDocuments ?? (_financeDocuments = new List<FinanceDocument>()); }
            set { _financeDocuments = value; }
        }

        public decimal? Balance { get; set; }
        public int? HsId { get; set; }

        //private List<InternalSettlement> _internalSettlements;
        //public List<InternalSettlement> InternalSettlements
        //{
        //    get { return _internalSettlements ?? (_internalSettlements = new List<InternalSettlement>()); }
        //    set { _internalSettlements = value; }
        //}

        private List<Visit> _visits;
        public List<Visit> Visits
        {
            get { return _visits ?? (_visits = new List<Visit>()); }
            set { _visits = value; }
        }

    }
}
