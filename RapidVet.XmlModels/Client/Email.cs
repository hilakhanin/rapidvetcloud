﻿using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class Email 
    {
        public string Name { get; set; }
    }
}
