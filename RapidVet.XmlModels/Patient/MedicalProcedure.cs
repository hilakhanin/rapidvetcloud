﻿using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class MedicalProcedure
    {
        public int Id { get; set; }
        public string Diagnosis { get; set; }
        public DateTime Start { get; set; }
        public DateTime? Finish { get; set; }
        public string ProcedureName { get; set; }
        public string ProcedureType { get; set; }
        public string ProcedureLocation { get; set; }
        public string SurgeonName { get; set; }
        public string AnesthesiologistName { get; set; }
        public string NurseName { get; set; }
        public string AdditionalPersonnel { get; set; }
        public string Summery { get; set; }
        public DateTime? Updated { get; set; }
    }
}
