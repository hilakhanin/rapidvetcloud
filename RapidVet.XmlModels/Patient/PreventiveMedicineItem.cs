﻿using System;

namespace RapidVet.XmlModels
{  
    [Serializable]
    public class PreventiveMedicineItem
    {        
        public int Id { get; set; }
        public int DocumentId { get; set; }
        public string PriceListItemType { get; set; }
        public string PriceListItem { get; set; }
        public int? VisitId { get; set; }
        public DateTime? Preformed { get; set; }       
        public string Comments { get; set; }
        public bool ExternalyPreformed { get; set; }
        public decimal Discount { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }        
        public DateTime? Scheduled { get; set; }
        public DateTime? LastReminderDate { get; set; }
        public int ReminderCount { get; set; }
        public string ReminderComments { get; set; }
        public string ExternalDrName { get; set; }
        public string ExternalDrLicenseNumber { get; set; }
        public string BatchNumber { get; set; }
        public bool ClientPaidOrReturnedTollVaucher { get; set; }
        public decimal TollPrice { get; set; }
        public DateTime? TollPaymentDate { get; set; }
        public string TollPaymentMethod { get; set; }       
    }
}
