﻿using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class PatientComment
    {
        public string CommentBody { get; set; }
        public DateTime? ValidThrough { get; set; }
        public bool Popup { get; set; }
        public bool Active { get; set; }
    }
}
