﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace RapidVet.XmlModels
{
    [Serializable]
    public class Patient 
    {        
        public int Id { get; set; }
        public int LocalId { get; set; }
        public string Name { get; set; }
        public AnimalRace AnimalRace { get; set; }
        public string AnimalColor { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }
        public bool PureBred { get; set; }
        public bool Fertilization { get; set; }
        public bool Sterilization { get; set; }
        public DateTime? SterilizationDate { get; set; }
        public string ElectronicNumber { get; set; }
        public string LicenseNumber { get; set; }
        public string Owner_Farm { get; set; }
        public string SAGIRNumber { get; set; }
        public bool HumanDangerous { get; set; }
        public bool AnimalDangerous { get; set; }
        public string BloodType { get; set; }
        public string Allergies { get; set; }
        public string FoodAmount { get; set; }

        private List<PatientComment> _comments;
        public List<PatientComment> Comments
        {
            get { return _comments ?? (_comments = new List<PatientComment>()); }
            set { _comments = value; }
        }
        public bool Active { get; set; }       
        private List<PatientLabTest> _labTests;
        public List<PatientLabTest> LabTests
        {
            get { return _labTests ?? (_labTests = new List<PatientLabTest>()); }
            set { _labTests = value; }
        }

        private List<PreventiveMedicineItem> _preventiveMedicine;
        public List<PreventiveMedicineItem> PreventiveMedicine
        {
            get { return _preventiveMedicine ?? (_preventiveMedicine = new List<PreventiveMedicineItem>()); }
            set { _preventiveMedicine = value; }
        }

        private List<MedicalProcedure> _medicalProcedures;
        public List<MedicalProcedure> MedicalProcedures
        {
            get { return _medicalProcedures ?? (_medicalProcedures = new List<MedicalProcedure>()); }
            set { _medicalProcedures = value; }
        }

        
        public bool SeeingEyeDog { get; set; }
        public bool Exemption { get; set; }
        public string ExemptionCause { get; set; }
    }
}
