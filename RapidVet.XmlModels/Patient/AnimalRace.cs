﻿using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class AnimalRace
    {
        public string AnimalKind { get; set; }
        public string Value { get; set; }        
    }
}
