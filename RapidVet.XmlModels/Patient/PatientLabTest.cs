﻿using System;
using System.Collections.Generic;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class PatientLabTest
    {        
        public int Id { get; set; }
        public int LabTestTemplateId { get; set; }
        public DateTime Created { get; set; }
        private List<PatientLabTestResult> _results;
        public List<PatientLabTestResult> Results
        {
            get { return _results ?? (_results = new List<PatientLabTestResult>()); }
            set { _results = value; }
        }
    }
}
