using System;

namespace RapidVet.XmlModels
{
    [Serializable]
    public class PatientLabTestResult
    {        
        public int Id { get; set; }
        public int LabTestId { get; set; }
        public string Result { get; set; }
    }
}