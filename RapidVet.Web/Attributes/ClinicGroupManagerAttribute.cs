﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVetMembership;
using RapidVet.RapidConsts;

namespace RapidVet.Web
{
    public class ClinicGroupManagerAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");

            var user = ConcurrentUserInfo.GetConcurrentUserInfoByGuid(HttpContext.Current.User.Identity.Name);

            return user.IsClinicGroupManager;
        }
    }
}