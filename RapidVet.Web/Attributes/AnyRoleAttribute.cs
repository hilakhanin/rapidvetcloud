﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVetMembership;
using RapidVet.RapidConsts;

namespace RapidVet.Web
{
    public class AnyRoleAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");

            var user = ConcurrentUserInfo.GetConcurrentUserInfoByGuid(HttpContext.Current.User.Identity.Name);

            //if the user is in any role return true
            if (user.IsUserInRole(RolesConsts.CLINICGROUPMANAGER) ||
                user.IsUserInRole(RolesConsts.ADMINISTRATOR) ||
                user.IsUserInRole(RolesConsts.CLINICMANAGER) ||
                user.IsUserInRole(RolesConsts.DOCTOR) ||
                user.IsUserInRole(RolesConsts.EXCEL_EXPORT) ||
                user.IsUserInRole(RolesConsts.INVENTORY_CREATE_ORDER) ||
                user.IsUserInRole(RolesConsts.INVENTORY_ENTITY_MANAGEMENT) ||
                user.IsUserInRole(RolesConsts.INVENTORY_RECIEVE_ORDER) ||
                user.IsUserInRole(RolesConsts.INVENTORY_REPORTS) ||
                user.IsUserInRole(RolesConsts.INVENTORY_UPDATE) ||
                user.IsUserInRole(RolesConsts.SECRATERY) ||
                user.IsUserInRole(RolesConsts.VIEW_FINANCIAL_REPORTS) ||
                user.IsUserInRole(RolesConsts.VIEW_REPORTS))
            {
                return true;
            }

            
            return user.IsSecratery;


        }
    }
}