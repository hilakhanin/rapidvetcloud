﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVetMembership;

namespace RapidVet.Web
{
    public class DoctorAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");

            var user = ConcurrentUserInfo.GetConcurrentUserInfoByGuid(HttpContext.Current.User.Identity.Name);

            return user.IsDoctor;

        }
    }
}