﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using RapidVetMembership;

namespace RapidVet.Web.Attributes
{
    public class NoDiaryAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");

            var user = ConcurrentUserInfo.GetConcurrentUserInfoByGuid(HttpContext.Current.User.Identity.Name);

            return user.IsNoDiary;

        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary(
                    new
                    {
                        controller = "Home",
                        action = "Unauthorised"
                    })
                );
        }
    }
}