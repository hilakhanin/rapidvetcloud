﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model.Visits;
using RapidVet.Repository.Visits;
using RapidVet.WebModels.Visits;

namespace RapidVet.Web.Controllers
{
    public class TreatmentsController : BaseController
    {

        //id is clinicId
        public PartialViewResult Index()
        {
            var tretments = RapidVetUnitOfWork.TreatmentRepository.GetList(CurrentUser.ActiveClinicId);
            var model = tretments.Select(AutoMapper.Mapper.Map<Treatment, TreatmentAdminModel>);
            return PartialView("_Index", model);
        }

        //id is clinicId
        [HttpGet]
        public PartialViewResult Create()
        {
            var model = new TreatmentAdminModel();
            return PartialView("_Create", model);
        }

        [HttpPost]
        public JsonResult Create(TreatmentAdminModel model)
        {
            var result = new JsonResult();

            if (ModelState.IsValid)
            {
                var treatment = AutoMapper.Mapper.Map<TreatmentAdminModel, Treatment>(model);
                treatment.ClinicId = CurrentUser.ActiveClinicId;
                treatment.Active = true;
                var status = RapidVetUnitOfWork.TreatmentRepository.Create(treatment);
                result.Data = new
                    {
                        success = true,
                        target = Request.Form["target"],
                        indexId = Request.Form["indexGridId"],
                        modalId = Request.Form["modalId"]

                    };
            }
            else
            {
                result.Data = new
                    {
                        success = false,
                        data = RenderPartialViewToString("_Create", model)
                    };
            }
            return result;
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var treatment = RapidVetUnitOfWork.TreatmentRepository.GetItem(id);
            if (treatment.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<Treatment, TreatmentAdminModel>(treatment);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public JsonResult Edit(TreatmentAdminModel model)
        {
            var result = new JsonResult();

            if (ModelState.IsValid)
            {
                var treatment = RapidVetUnitOfWork.TreatmentRepository.GetItem(model.Id);
                if (treatment.ClinicId != CurrentUser.ActiveClinicId)
                {
                    throw new SecurityException(Resources.Exceptions.AccessDenied);
                }
                AutoMapper.Mapper.Map(model, treatment);
                var status = RapidVetUnitOfWork.TreatmentRepository.Update(treatment);
                result.Data = new
                {
                    success = true,
                    target = Request.Form["target"],
                    indexId = Request.Form["indexGridId"],
                    modalId = Request.Form["modalId"]

                };

            }
            else
            {
                result.Data = new
                    {
                        success = false,
                        data = RenderPartialViewToString("_Edit", model)
                    };
            }
            return result;
        }

        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            var treatment = RapidVetUnitOfWork.TreatmentRepository.GetItem(id);
            if (treatment.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<Treatment, TreatmentAdminModel>(treatment);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public JsonResult Delete(TreatmentAdminModel model)
        {
            var treatment = RapidVetUnitOfWork.TreatmentRepository.GetItem(model.Id);
            if (treatment.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var status = RapidVetUnitOfWork.TreatmentRepository.Delete(treatment);
            return new JsonResult()
                {
                    Data = new
                        {
                            success = true,
                            target = Request.Form["target"],
                            indexId = Request.Form["indexGridId"],
                            modalId = Request.Form["modalId"]
                        }
                };
        }

    }
}
