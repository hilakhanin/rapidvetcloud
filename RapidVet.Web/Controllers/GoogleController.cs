﻿using RapidVet.Enums.Google.Results;
using RapidVet.Model.Google;
using RapidVet.Web.Infra.Google;
using RapidVet.WebModels.Google;
using RapidVet.WebModels.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace RapidVet.Web.Controllers
{
    public class GoogleController : BaseController
    {
        #region Settings
        public ActionResult Settings()
        {
            if (Session["SyncCalErrorMessage"] != null)
            {
                SetErrorMessage(Session["SyncCalErrorMessage"].ToString());
                Session["SyncCalErrorMessage"] = null;
            }

            var m = new GoogleSyncModel() { MaxSyncAllowedForClinigGroup = CurrentUser.ClinicGroup == null ? (CurrentUser.ActiveClinicGroup == null ? 1 : CurrentUser.ActiveClinicGroup.MaxGoogleSyncAllowed) : CurrentUser.ClinicGroup.MaxGoogleSyncAllowed };
            m.GSettings = RapidVetUnitOfWork.GoogleRepository.GetGoogleSettings(CurrentUser.ClinicGroup == null ? CurrentUser.ActiveClinicGroupId : CurrentUser.ClinicGroup.Id);
            ViewBag.CanAddCalendarsForSync = m.GSettings.FindAll(x => x.AllowSync).Count < m.MaxSyncAllowedForClinigGroup;
            var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctorsForSchedule(CurrentUser.ActiveClinicId, CurrentUser.Id);
            ViewBag.UsersList = doctors.Select(x => new UsersSettingsShowUserModel()
            {
                User = x.Name,
                DoctorId = x.Id
            }).ToList();
            return View(m);
        }

        [HttpPost]
        public ActionResult Settings(GoogleSyncSet model)
        {
            return addUpdateSyncCal(new GoogleSettings()
            {
                Id = model.Id,
                AllowSync = model.AllowSync,
                AllowSyncCalendar = model.AllowSyncCalendar,
                AllowSyncContacts = model.AllowSyncContacts,
                DoctorID = model.DoctorID,
                GoogleCalName = model.GoogleCalName,
                GoogleTokenId = model.GoogleTokenId,
                SyncCalBackDays = model.SyncCalBackDays,
                SyncCalFromGoogle = model.SyncCalFromGoogle,
                SyncCalToGoogle = model.SyncCalToGoogle,
                SyncPeriod = model.SyncPeriod,
                SyncCalForwardDays = model.SyncCalForwardDays
            });
        }
        #endregion

        #region Auth
        private string GetAccountGoogleEmailSessionKey()
        {
            return "AG_Email" + User.Identity.Name;
        }

        public ActionResult Authorization(string code)
        {
            // Retrieve the authenticator and save it in session for future use
            var authenticator = GoogleAuthorizationHelper.GetAuthenticator(code);
            authenticator.UserID = Session[GetAccountGoogleEmailSessionKey()] as String;
            RapidVetUnitOfWork.GoogleRepository.AddUpdateGoogleToken(authenticator.GetGoogleTokenRepository, CurrentUser.Id, DateTime.Now);
            return RedirectToAction("Settings", "Google");
        }

        [HttpPost]
        public string ValidateOfflineAuth(string token)
        {
            var guf = getGoogleUserProfile(token);
            if (guf == null)
                return null;

            try
            {
                var r = RapidVetUnitOfWork.GoogleRepository.GetGoogleToken(guf.email);
                if (r != null)
                    return "ok";

                Session[GetAccountGoogleEmailSessionKey()] = guf.email;
                return GoogleAuthorizationHelper.GetAuthorizationUrl(guf.email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CurrentUser.CurrentGoogleSyncEmail = guf.email;
            }
        }

        private GoogleUserProfile getGoogleUserProfile(string token)
        {
            try
            {
                WebRequest request = WebRequest.Create("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + token);
                request.ContentType = "application/json";
                WebResponse response = request.GetResponse();
                GoogleUserProfile uf = null;
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    uf = new JavaScriptSerializer().Deserialize<GoogleUserProfile>(result);
                }
                return uf;
            }
            catch (Exception ex)
            {
                BaseLogger.LogWriter.WriteEx(ex);
            }
            return null;
        }

        [HttpPost]
        public string ResetGoogleCalendarID(int settingID)
        {
            return RapidVetUnitOfWork.GoogleRepository.ResetGoogleCalendarID(settingID);
        }
        #endregion

        #region AddUpdateSyncCal
        public ActionResult AddNewSyncCal()
        {
            int maxAllowed = CurrentUser.ClinicGroup.MaxGoogleSyncAllowed;

            return View();
        }

        [HttpPost]
        public ActionResult AddNewSyncCal(GoogleSettings model)
        {
            return addUpdateSyncCal(model);
        }

        [HttpPost]
        public ActionResult UpdateSyncCal(GoogleSyncSet model)
        {
            return addUpdateSyncCal(new GoogleSettings()
            {
                Id = model.Id,
                AllowSync = model.AllowSync,
                AllowSyncCalendar = model.AllowSyncCalendar,
                AllowSyncContacts = model.AllowSyncContacts,
                DoctorID = model.DoctorID,
                GoogleCalName = model.GoogleCalName,
                GoogleTokenId = model.GoogleTokenId,
                SyncCalBackDays = model.SyncCalBackDays,
                SyncCalForwardDays = model.SyncCalForwardDays,
                SyncCalFromGoogle = model.SyncCalFromGoogle,
                SyncCalToGoogle = model.SyncCalToGoogle,
                SyncPeriod = model.SyncPeriod
            });
        }

        private ActionResult addUpdateSyncCal(GoogleSettings model)
        {
            try
            {
                var r = RapidVetUnitOfWork.GoogleRepository.AddUpdateGoogleSetting(model, CurrentUser.Id, CurrentUser.ActiveClinicId, CurrentUser.ClinicGroup == null ? CurrentUser.ActiveClinicGroupId : CurrentUser.ClinicGroup.Id, CurrentUser.CurrentGoogleSyncEmail);
                if (r != SyncCalResult.OK)
                    switch (r)
                    {
                        case SyncCalResult.CalendarNameAlreadyExist:
                            Session["SyncCalErrorMessage"] = "שם יומן בגוגל כבר קיים";
                            break;
                        case SyncCalResult.MaxSyncAcoountsExeeded:
                            Session["SyncCalErrorMessage"] = "חריגה במספר החשבונות לסנכרון";
                            break;
                        case SyncCalResult.GoogleSyncEmailAccountNotProvided:
                            Session["SyncCalErrorMessage"] = "לא ניתן לאמת חשבון גוגל";
                            break;
                        case SyncCalResult.GoogleSyncEmailAccountNotValid:
                            Session["SyncCalErrorMessage"] = "לא בוצע אימות לעבודה לא מכוונת של הסנכרון";
                            break;
                    }
            }
            catch (Exception ex)
            {
                SetErrorMessage(ex.Message);
                BaseLogger.LogWriter.WriteEx(ex);
                ViewBag.ErrorMsg = "שגיאת מערכת";
                return View(model);
            }
            return RedirectToAction("Settings");
        }
        #endregion

        #region History
        public ActionResult History(string Id, string doctoName)
        {
            var m = new GoogleSyncHistoryModel(){
                GoogleSettingId = Id,
                From = DateTime.Now.AddDays(-1).Date,
                To = DateTime.Now.Date,
                HasChangesOnly = false,
                WithErrorsOnly = false,
                DoctorName = doctoName
            };
            m.Reports = RapidVetUnitOfWork.GoogleRepository.GetHistory(Id, m.From, m.To, m.WithErrorsOnly, m.HasChangesOnly);
            return View(m);
        }

        [HttpPost]
        public ActionResult History(GoogleSyncHistoryModel m)
        {
            m.Reports = RapidVetUnitOfWork.GoogleRepository.GetHistory(m.GoogleSettingId, m.From, m.To, m.WithErrorsOnly, m.HasChangesOnly);
            return View(m);
        }
        #endregion
    }
}