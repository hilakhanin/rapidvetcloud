﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using RapidVet.Enums;
using RapidVet.Enums.MinistryOfAgricultureReports;
using RapidVet.Model.Clients;
using RapidVet.Model.Patients;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.Patients;
using System.Collections.Specialized;
using RapidVet.Helpers;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class LettersController : BaseController
    {

        public ActionResult Index(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = GetClientDetailsModelFromPatient(id);
                return View(model);
            }
            throw new SecurityException();
        }

        public ActionResult Sterilization(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = GetClientDetailsModelFromPatient(id);
                return View(model);
            }
            throw new SecurityException();
        }

        public ActionResult BillOfHealthHe(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = new GenericLetterModel()
                    {
                        Client = GetClientDetailsModelFromPatient(id),
                        ExaminationDate = DateTime.Now.ToShortDateString()
                    };
                return View(model);
            }
            throw new SecurityException();
        }

        public ActionResult BillOfHealthEn(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);

                var model = new GenericLetterModel()
                {
                    Client = GetClientDetailsModelFromPatient(id),
                    ExaminationDate = DateTime.Now.ToShortDateString()
                };
                return View(model);
            }
            throw new SecurityException();
        }

        public ActionResult DogOwnerLicense(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
                var model = GetClientDetailsModelFromPatient(id);//AutoMapper.Mapper.Map<Client, ClientDetailsModel>(patient.Client);
                return View(model);
            }
            throw new SecurityException();
        }

        public ActionResult RabiesVaccineCertificate(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                //var model = GetClientDetailsModelFromPatient(id);
                //return View(model);
             return PrePrintHub(id, (int)LetterTemplateType.RabiesVaccineCertificate);
             
            }
            throw new SecurityException();
        }

        public ActionResult MedicalProcedureSummery(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = GetClientDetailsModelFromPatient(id);
                return View(model);
            }
            throw new SecurityException();
        }

        public ActionResult PatientDischargePaper(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = GetClientDetailsModelFromPatient(id);
                return View(model);
            }
            throw new SecurityException();
        }

        public ActionResult MinistryOfAgricultureReport(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                //report type options
                var typeOptions = new List<SelectListItem>();
                var rm = Resources.Enums.MinistryOfAgricultureReportType.ResourceManager;
                foreach (var rtype in Enum.GetValues(typeof(MinistryOfAgricultureReportType)))
                {
                    typeOptions.Add(new SelectListItem()
                        {
                            Text = rm.GetString(rtype.ToString()),
                            Value = ((int)rtype).ToString()
                        });
                }
                //vet license options
                //var docOptions = new List<SelectListItem>();
                //var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId);
                //foreach (var doctor in doctors)
                //{
                //    docOptions.Add(new SelectListItem()
                //        {
                //            Text = doctor.Name,
                //            Value = doctor.LicenceNumber
                //        });
                //}
                //complaint type options
                var complaintOptions = new List<SelectListItem>();
                rm = Resources.Enums.MinistryOfAgricultureReports.ComplaintType.ResourceManager;
                foreach (var ctype in Enum.GetValues(typeof(ComplaintType)))
                {
                    complaintOptions.Add(new SelectListItem()
                        {
                            Text = rm.GetString(ctype.ToString()),
                            Value = ((int)ctype).ToString()
                        });
                }
                //offence type options
                var offenceOptions = new List<SelectListItem>();
                rm = Resources.Enums.MinistryOfAgricultureReports.OffenceType.ResourceManager;
                foreach (var otype in Enum.GetValues(typeof(OffenceType)))
                {
                    offenceOptions.Add(new SelectListItem()
                        {
                            Text = rm.GetString(otype.ToString()),
                            Value = ((int)otype).ToString()
                        });
                }
                //discharge type options
                var dischargeOptions = new List<SelectListItem>();
                rm = Resources.Enums.MinistryOfAgricultureReports.DischargeType.ResourceManager;
                foreach (var dtype in Enum.GetValues(typeof(QuarantineDischargeType)))
                {
                    dischargeOptions.Add(new SelectListItem()
                    {
                        Text = rm.GetString(dtype.ToString()),
                        Value = ((int)dtype).ToString()
                    });
                }
                var model = new MinistryOfAgricultureReportWebModel()
                    {
                        Client = GetClientDetailsModelFromPatient(id),
                        DateTime = DateTime.Now,
                        PatientId = id,
                        ReportTypeOptions = typeOptions,
                        ComplaintTypeOptions = complaintOptions,
                        OffenceTypeOptions = offenceOptions,
                        DischargeTypeOptions = dischargeOptions
                    };
                return View(model);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult MinistryOfAgricultureReport(MinistryOfAgricultureReportWebModel model)
        {
            if (ModelState.IsValid)
            {
                if (IsPatientInCurrentClinic(model.PatientId))
                {
                    var report =
                        AutoMapper.Mapper.Map<MinistryOfAgricultureReportWebModel, MinistryOfAgricultureReport>(model);
                    var status = RapidVetUnitOfWork.PatientLetterRepository.CreateMinistryOfAgricultureReport(report);
                    if (status.Success)
                    {
                        SetSuccessMessage("הנתונים נשמרו בהצלחה");
                    }
                    else
                    {
                        SetErrorMessage();
                    }
                    return RedirectToAction("Index", "Letters", new { id = model.PatientId });
                }
                throw new SecurityException();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult PrePrintHub(int id, int letterTemplateTypeId)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var letterTemplateType = (LetterTemplateType)letterTemplateTypeId;
                var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(id);
                var showInPatientHistory = false;

                //common
                var date = Request.Form["date"] ?? string.Empty;
                var patientFeatures = Request.Form["patientFeatures"] ?? string.Empty;
                var doctorName = Request.Form["doctorName"] ?? string.Empty;
                var examinationDate = Request.Form["examinationDate"] ?? string.Empty;
                var doctorLisence = string.Empty;

                var result = string.Empty;

                switch (letterTemplateType)
                {
                    case LetterTemplateType.Sterilization:
                        var action = Request.Form["action"] ?? string.Empty;

                        var comments = Request.Form["comments"] ?? string.Empty;

                        var copiesTo = Request.Form["copiesTo"] ?? string.Empty;
                        result = RapidVetUnitOfWork.LetterTemplatesRepository.GetFormattedSterilizationDocument(
                            patient, date, action, patientFeatures, copiesTo, comments, CurrentUser.Name, CurrentUser.LicenceNumber);
                        break;
                    case LetterTemplateType.BillOfHealthHe:

                        doctorLisence = RapidVetUnitOfWork.UserRepository.GetDoctorLisence(doctorName,
                                                                                              CurrentUser.ActiveClinicGroupId);
                        result = RapidVetUnitOfWork.LetterTemplatesRepository
                                                   .GetFormattedBillOfHealthDocument(patient,
                                                                                     LanguageEnum
                                                                                         .Hebrew,
                                                                                     doctorName,
                                                                                     examinationDate, doctorLisence);
                        break;
                    case LetterTemplateType.BillOfHealthEn:
                        doctorLisence = RapidVetUnitOfWork.UserRepository.GetDoctorLisence(doctorName,
                                                                                             CurrentUser
                                                                                                 .ActiveClinicId);
                        result = RapidVetUnitOfWork.LetterTemplatesRepository
                                                   .GetFormattedBillOfHealthDocument(patient,
                                                                                     LanguageEnum
                                                                                         .English,
                                                                                     doctorName,
                                                                                     examinationDate, doctorLisence);
                        break;
                    case LetterTemplateType.DogOwnerLicense:
                        var licenseNumber = Request.Form["licenseNumber"] ?? string.Empty;
                        var year = Request.Form["year"] ?? string.Empty;
                      //  var seeingEyeDogStr = Request.Form["seeingEyeDog"] ?? string.Empty;
                        var seeingEyeDog = patient.SeeingEyeDog;//!string.IsNullOrWhiteSpace(seeingEyeDogStr) && seeingEyeDogStr == "on";
                        var toll = Request.Form["toll"] ?? string.Empty;
                        var payThrough = Request.Form["payThrough"] ?? string.Empty;
                        result =
                            RapidVetUnitOfWork.LetterTemplatesRepository
                                              .GetFormattedDogOwnerLicenseDocument(patient,
                                                                                   date,
                                                                                   licenseNumber,
                                                                                   patientFeatures,
                                                                                   payThrough,
                                                                                   toll,
                                                                                   seeingEyeDog,
                                                                                   year);
                        break;
                    case LetterTemplateType.RabiesVaccineCertificate:
                        result =
                           RapidVetUnitOfWork.LetterTemplatesRepository
                                             .GetFormattedRabiesVaccineCertificateDocument(patient);
                        break;
                    case LetterTemplateType.MedicalProcedureSummery:
                        break;
                    case LetterTemplateType.PatientDischargePaper:
                        break;
                    case LetterTemplateType.MinistryOfAgricultureReport:
                        break;
                    case LetterTemplateType.PersonalLetter:
                        break;
                }

                var letter = new Letter()
                {
                    CreatedDate = DateTime.Now,
                    LetterTemplateTypeId = letterTemplateTypeId,
                    PatientId = id,
                    Content = result,
                    ShowInPatientHistory = showInPatientHistory
                };

                var status = RapidVetUnitOfWork.PatientLetterRepository.Create(letter);
                return RedirectToAction("ShowLetter", "Letters", new { id = id, letterId = letter.Id });
            }
            throw new SecurityException();
        }

        public ActionResult ShowLetter(int id, int letterid)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var letter = RapidVetUnitOfWork.PatientLetterRepository.GetLetter(letterid);
                if (letter.PatientId == id)
                {
                    var model = AutoMapper.Mapper.Map<Letter, LetterWebModel>(letter);
                    model.Client = GetClientDetailsModelFromPatient(id);
                   
                    return View(model);
                }
            }
            throw new SecurityException();
        }

        public ActionResult ShowPersonalLetter(int id, int personalLetterId,bool print=false)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var template = RapidVetUnitOfWork.LetterTemplatesRepository.GetPersonalTemplate(personalLetterId);
                ViewBag.TemplateKeywords = RapidVetUnitOfWork.TemplateKeyWordsRepository.GetLetterTemplateKeyWord(template.LetterTemplateType);
                var result = GetFormattedTemplate(id, template.Content);
                var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(id);
                var showInPatientHistory = false;
                var letter = new Letter()
                {
                    CreatedDate = DateTime.Now,
                    LetterTemplateTypeId = (int)LetterTemplateType.PersonalLetter,
                    PatientId = id,
                    Content = result,
                    ShowInPatientHistory = showInPatientHistory
                };

                var status = RapidVetUnitOfWork.PatientLetterRepository.Create(letter);
                var model = new PersonalLetterWebModel()
                {
                    Id = letter.Id,
                    LetterTemplateType = LetterTemplateType.PersonalLetter,
                    PatientId = id,
                    Print = false,
                    ShowInPatientHistory = showInPatientHistory,
                    TemplateName = template.TemplateName,
                    Content = result
                };

                model.Client = GetClientDetailsModelFromPatient(id);
                   

                return View(model);
            }
            throw new SecurityException();
        }

        [ValidateInput(false)]
        [HttpPost]
        public JsonResult ShowPersonalLetter(PersonalLetterWebModel model)
        {
            if (IsPatientInCurrentClinic(model.PatientId))
            {
                var result = new JsonResult() { Data = false };
                var letter = RapidVetUnitOfWork.PatientLetterRepository.GetLetter(model.Id);
                if (letter.PatientId == model.PatientId)
                {
                    letter.Content = model.Content;
                    letter.ShowInPatientHistory = model.ShowInPatientHistory;
                    var status = RapidVetUnitOfWork.Save();
                    if (status.Success)
                    {
                        SetSuccessMessage("הנתונים נשמרו בהצלחה");
                        result.Data = new
                        {
                            success = true,
                            url = Url.Action("Index", "Letters", new { id = model.PatientId }),
                            printUrl = Url.Action("PrintLetter", "Letters", new { id = letter.Id })
                        };
                    }
                    else
                    {
                        SetErrorMessage();
                    }

                    return result;
                }
            }
            throw new SecurityException();
        }

        private String GetFormattedTemplate(int patientId, string template)
        {

            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(patientId);
             var date = DateTime.Now.ToShortDateString();
            var nvc = GetNVC();
                       
            return GetMessage(nvc, template, patient);
            
        }

        private NameValueCollection GetNVC()
        {
            var nvc = new NameValueCollection();
            var doctor = CurrentUser.IsDoctor ? CurrentUser : CurrentUser.DefaultDrId.HasValue ? RapidVetUnitOfWork.UserRepository.GetItem(CurrentUser.DefaultDrId.Value) : null;

            nvc.Add(getTemplateKeyWord("ActiveDoctorName"), doctor != null ? doctor.Name : "");
            nvc.Add(getTemplateKeyWord("Date"), DateTime.Now.ToShortDateString());
            nvc.Add(getTemplateKeyWord("ClinicAddress"), CurrentUser.ActiveClinic.Address);
            nvc.Add(getTemplateKeyWord("ClinicPhone"), CurrentUser.ActiveClinic.Phone);
            nvc.Add(getTemplateKeyWord("ClinicFax"), CurrentUser.ActiveClinic.Fax);
            nvc.Add(getTemplateKeyWord("ActiveDoctorLicence"), doctor != null ? doctor.LicenceNumber : "");
            nvc.Add(getTemplateKeyWord("ClinicName"), CurrentUser.ActiveClinic.Name);
            nvc.Add(getTemplateKeyWord("ClinicEmail"), CurrentUser.ActiveClinic.Email);
            nvc.Add(getTemplateKeyWord("ClinicLogo"), "<img src=\"/ClinicGroups/LogoImage\" style=\"max-height: 150px;max-width: 150px;\">");
            nvc.Add(getTemplateKeyWord("DoctorName"), doctor != null ? doctor.Name : "");
            nvc.Add(getTemplateKeyWord("ClientName"), "");
            nvc.Add(getTemplateKeyWord("ClientLastName"), "");
            nvc.Add(getTemplateKeyWord("ClientFirstName"), "");
            nvc.Add(getTemplateKeyWord("ClientReferer"), "");
            nvc.Add(getTemplateKeyWord("ClientBalance"), "");
            nvc.Add(getTemplateKeyWord("ClientAge"), "");
            nvc.Add(getTemplateKeyWord("ClientTitle"), "");
            nvc.Add(getTemplateKeyWord("ClientIdCardNumber"), "");
            nvc.Add(getTemplateKeyWord("ClientAddress"), "");
            nvc.Add(getTemplateKeyWord("ClientZip"), "");
            nvc.Add(getTemplateKeyWord("ClientHomePhone"), "");
            nvc.Add(getTemplateKeyWord("ClientMobilePhone"), "");
            nvc.Add(getTemplateKeyWord("ClientBirthdate"), "");
            nvc.Add(getTemplateKeyWord("ClientStreetAndNumber"), "");
            nvc.Add(getTemplateKeyWord("ClientRecordNumber"), "");
            nvc.Add(getTemplateKeyWord("ClientIdCard"), "");
            nvc.Add(getTemplateKeyWord("ClientCity"), "");
            nvc.Add(getTemplateKeyWord("PatientName"), "");
            nvc.Add(getTemplateKeyWord("PatientAnimalKind"), "");
            nvc.Add(getTemplateKeyWord("PatientRace"), "");
            nvc.Add(getTemplateKeyWord("PatientColor"), "");
            nvc.Add(getTemplateKeyWord("PatientAge"), "");
            nvc.Add(getTemplateKeyWord("PatientSex"), "");
            nvc.Add(getTemplateKeyWord("Sterilization"), "");
            nvc.Add(getTemplateKeyWord("PatientChip"), "");
            nvc.Add(getTemplateKeyWord("PatientBirthDate"), "");
            return nvc;
        }

        private String GetMessage(NameValueCollection nvc, String template, Patient patient)
        {

            nvc.Set(getTemplateKeyWord("ClientName"), patient.Client.Name);
            nvc.Set(getTemplateKeyWord("ClientFirstName"), patient.Client.FirstName);
            nvc.Set(getTemplateKeyWord("ClientLastName"), patient.Client.LastName);
            nvc.Set(getTemplateKeyWord("ClientReferer"), patient.Client.ReferredBy);
            nvc.Set(getTemplateKeyWord("ClientBalance"), patient.Client.Balance.HasValue ? patient.Client.Balance.Value.ToString() : "");
          
            if (patient.Client.BirthDate.HasValue)
            {
                AgeHelper age = new AgeHelper(patient.Client.BirthDate.Value, DateTime.Today);
                nvc.Set(getTemplateKeyWord("ClientAge"), string.Format("{0}.{1}",age.Years, age.Months));
            }
            else
            {
                nvc.Set(getTemplateKeyWord("ClientAge"), "");
            }
            
            nvc.Set(getTemplateKeyWord("ClientTitle"), patient.Client.Title != null ? patient.Client.Title.Name : "");
            nvc.Set(getTemplateKeyWord("ClientIdCardNumber"), patient.Client.IdCard);

            var address = patient.Client.Addresses.FirstOrDefault();
            if (address != null)
            {
                nvc.Set(getTemplateKeyWord("ClientAddress"), address.Street + " " + address.City != null ? address.City.Name : "");
                nvc.Set(getTemplateKeyWord("ClientZip"), address.ZipCode); 
                nvc.Set(getTemplateKeyWord("ClientStreetAndNumber"), address.Street);
                nvc.Set(getTemplateKeyWord("ClientCity"), address.City != null ? address.City.Name : ""); 
            }
            var phone = patient.Client.Phones.FirstOrDefault(p => p.PhoneTypeId == HomePhone());
            nvc.Set(getTemplateKeyWord("ClientHomePhone"), phone == null ? "" : phone.PhoneNumber); 

            var cellPhone = patient.Client.Phones.FirstOrDefault(p => p.PhoneTypeId == CellPhone());
            nvc.Set(getTemplateKeyWord("ClientMobilePhone"), cellPhone == null ? "" : cellPhone.PhoneNumber); 
            nvc.Set(getTemplateKeyWord("ClientBirthdate"), patient.Client.BirthDate.HasValue ? patient.Client.BirthDate.Value.ToShortDateString() : "");
            nvc.Set(getTemplateKeyWord("ClientRecordNumber"), patient.Client.LocalId.ToString());
            nvc.Set(getTemplateKeyWord("ClientIdCard"), patient.Client.IdCard);
            nvc.Set(getTemplateKeyWord("PatientName"), patient.Name);
            nvc.Set(getTemplateKeyWord("PatientAnimalKind"),(patient.AnimalRace != null && patient.AnimalRace.AnimalKind != null) ? patient.AnimalRace.AnimalKind.Value : "");
            nvc.Set(getTemplateKeyWord("PatientRace"),patient.AnimalRace != null ? patient.AnimalRace.Value : "");
            nvc.Set(getTemplateKeyWord("PatientColor"), patient.AnimalColor != null ? patient.AnimalColor.Value : "");

            if (patient.BirthDate.HasValue)
            {
                AgeHelper age = new AgeHelper(patient.BirthDate.Value, DateTime.Today);
                nvc.Set(getTemplateKeyWord("PatientAge"), string.Format("{0}.{1}", age.Years.ToString(), age.Months.ToString()));
            }
            else
            {
                nvc.Set(getTemplateKeyWord("PatientAge"), "");
            }
            
            nvc.Set(getTemplateKeyWord("PatientSex"), patient.GenderId == 1 ? Resources.Gender.Male : (patient.GenderId == 2 ? Resources.Gender.Female : Resources.Gender.UndefinedSex));
            nvc.Set(getTemplateKeyWord("Sterilization"), patient.Sterilization ? Resources.Global.Yes : Resources.Global.No);
            nvc.Set(getTemplateKeyWord("PatientChip"), patient.ElectronicNumber);
            nvc.Set(getTemplateKeyWord("PatientBirthDate"), patient.BirthDate.HasValue ? patient.BirthDate.Value.ToShortDateString() : "");

            var result = RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template);
            return result;
        }


        [ValidateInput(false)]
        [HttpPost]
        public JsonResult ShowLetter(LetterWebModel model)
        {
            if (IsPatientInCurrentClinic(model.PatientId))
            {
                var result = new JsonResult() { Data = false };
                var letter = RapidVetUnitOfWork.PatientLetterRepository.GetLetter(model.Id);
                if (letter.PatientId == model.PatientId)
                {
                    letter.Content = model.Content;
                    letter.ShowInPatientHistory = model.ShowInPatientHistory;
                    var status = RapidVetUnitOfWork.Save();
                    if (status.Success)
                    {
                        SetSuccessMessage("הנתונים נשמרו בהצלחה");
                        result.Data = new
                            {
                                success = true,
                                url = Url.Action("Index", "Letters", new { id = model.PatientId }),
                                printUrl = Url.Action("PrintLetter", "Letters", new { id = letter.Id })
                            };
                    }
                    else
                    {
                        SetErrorMessage();
                    }

                    return result;
                }
            }
            throw new SecurityException();
        }

        public ActionResult PrintLetter(int id)
        {
            var letter = RapidVetUnitOfWork.PatientLetterRepository.GetLetter(id);
            if (IsPatientInCurrentClinic(letter.PatientId))
            {
                return View(letter);
            }
            throw new SecurityException();
        }

        public ActionResult Delete(int id)
        {
            var letter = RapidVetUnitOfWork.PatientLetterRepository.GetLetter(id);
            var patientId = letter.PatientId;
            var letterId = letter.Id;
            if (IsPatientInCurrentClinic(patientId))
            {
                var status = RapidVetUnitOfWork.PatientLetterRepository.Delete(letter);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "Letters", new { id = patientId });
            }
            throw new SecurityException();
        }
    }
}
