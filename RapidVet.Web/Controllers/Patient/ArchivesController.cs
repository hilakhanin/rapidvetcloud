﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using RapidVet.Enums;
using RapidVet.FilesAndStorage;
using RapidVet.Helpers;
using RapidVet.Model.Archives;
using RapidVet.Model.Clients;
using RapidVet.WebModels.ArchiveDocuments;
using RapidVet.WebModels.Clients;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Text;
//using ImageResizer;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class ArchivesController : BaseController
    {
        public static int MAX_UPLOAD_PERCENTAGE = 95;
        /// <summary>
        /// the main archive view
        /// </summary>
        /// <param name="id">patient id</param>
        /// <returns></returns>
        public ActionResult Index(int id)
        {
            if (!IsPatientInCurrentClinic(id)) { throw new SecurityException(); };
            var model = GetClientDetailsModelFromPatient(id);
            return View(model);
        }

        /// <summary>
        /// return the json data for the ko view model
        /// </summary>
        /// <param name="id">patient id</param>
        /// <returns></returns>
        public JsonResult GetIndexData(int id)
        {
            if (!IsPatientInCurrentClinic(id)) { throw new SecurityException(); };

            var patientClinicId = RapidVetUnitOfWork.PatientRepository.GetPatientClinicId(id);

            if (CurrentUser.IsDoctor && CurrentUser.ActiveClinicId == patientClinicId)
            {
                var documentsDb = RapidVetUnitOfWork.ArchivesRepository.GetPatientDocuments(id);
                var documents =
                    documentsDb.Select(AutoMapper.Mapper.Map<ArchiveDocument, ArchiveDocumentIndexModel>).ToList();

                foreach (var doc in documents)
                {
                    doc.EditUrl = Url.Action("Edit", "Archives", new { id = id, documentId = doc.Id });
                    doc.ViewUrl = Url.Action("View", "Archives", new { id = id, documentId = doc.Id });
                }

                var images = documents.Where(d => d.FileType == ArchivesFileType.Image).ToList();
                var videos = documents.Where(d => d.FileType == ArchivesFileType.Video).ToList();
                var other = documents.Where(d => d.FileType == ArchivesFileType.Document).ToList();

                var model = new ArchiveDocumantCollectionsModel()
                    {
                        Images = images,
                        Videos = videos,
                        Other = other
                    };
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            throw new SecurityException();
        }

        /// <summary>
        /// return enum json of file type for drop down
        /// </summary>
        /// <returns></returns>
        public JsonResult GetFileTypesData()
        {

            var list = from ArchivesFileType a in Enum.GetValues(typeof(ArchivesFileType))
                       where a != ArchivesFileType.Audio
                       select new SelectListItem()
                       {
                           Text = RapidVet.Resources.Enums.ArchivesFileType.ResourceManager.GetString(a.ToString()),
                           Value = ((int)a).ToString(),
                       };

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// return enum json of file type for drop down
        /// </summary>
        /// <returns></returns>
        public string GetMaxFileSize()
        {
            var fsl = System.Configuration.ConfigurationManager.AppSettings["FILE_SIZE_LIMIT"];
            double maxSize;
            if (!double.TryParse(fsl, out maxSize))
                maxSize = 10;

            return ((int)Math.Floor(maxSize / 1000000)).ToString();
        }

        /// <summary>
        /// display the video/image in the html file
        /// </summary>
        /// <param name="id">patient id</param>
        /// <param name="documentId"></param>
        /// <param name="print"></param>
        /// <returns></returns>
        public ActionResult View(int id, int documentId, bool print = false)
        {
            if (!IsPatientInCurrentClinic(id)) { throw new SecurityException(); };
            var patientClinicId = RapidVetUnitOfWork.PatientRepository.GetPatientClinicId(id);
            if (CurrentUser.IsDoctor && CurrentUser.ActiveClinicId == patientClinicId)
            {
                var document = RapidVetUnitOfWork.ArchivesRepository.GetItem(documentId);
                ViewBag.Print = print;
                return View(document);
            }
            throw new SecurityException();
        }

        /// <summary>
        /// Create new archive item
        /// </summary>
        /// <param name="id">patient id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create(int id)
        {
            if (!IsPatientInCurrentClinic(id)) { throw new SecurityException(); };
            var patientClinicId = RapidVetUnitOfWork.PatientRepository.GetPatientClinicId(id);
            if (CurrentUser.IsDoctor && CurrentUser.ActiveClinicId == patientClinicId)
            {
                var model = new ArchiveDocument() { ShowInPatientHistory = true };
                return View(model);
            }
            throw new SecurityException();
        }


        /// <summary>
        /// Post response 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ArchiveDocument model)
        {
            if (Request.Files.Count > 0)
            {
                double size;
                double usage = GetArchiveUsagePercent(CurrentUser.ActiveClinicId, out size);
                if ((int)usage < MAX_UPLOAD_PERCENTAGE)
                {
                    model.ClinicId = CurrentUser.ActiveClinicId;
                    model.Created = DateTime.Now;
                    var fileCounter = 0;
                    var uploadedCounter = 0;

                    HttpPostedFileBase file = null;

                    for (var i = 0; i < Request.Files.Count; i++)
                    {
                        var fileBase = Request.Files[i] as HttpPostedFileBase;

                        if (fileBase.ContentLength > 0)
                        {
                            if (fileBase.ContentLength >= int.Parse(System.Configuration.ConfigurationManager.AppSettings["FILE_SIZE_LIMIT"]))
                            {
                                SetErrorMessage("הקובץ חורג מהגודל המותר");
                                return View(model);
                            }
                            file = fileBase;
                            fileCounter++;
                        }


                        var document = model;

                        if (file != null && fileBase.ContentLength > 0)
                        {
                            document.FileExtension = file != null ? Path.GetExtension(file.FileName) : "txt";
                            document.ContentType = file.ContentType ?? string.Empty;
                            document.UploadedByUserId = CurrentUser.Id;
                            var status = RapidVetUnitOfWork.ArchivesRepository.Create(document);

                            if (ArchivesUploadHelper.UploadDocument(CurrentUser.ActiveClinicGroupId, CurrentUser.ActiveClinicId,
                                (RapidVetUnitOfWork.ClientRepository.GetClientFromPatientId(document.PatientId)).Id,
                                document.PatientId, document.Id + document.FileExtension, file))
                            {
                                document.FileSizeInBytes = file.ContentLength;
                                uploadedCounter++;
                            }
                            else
                            {
                                document.FileSizeInBytes = 0;
                            }
                            file = null;
                            RapidVetUnitOfWork.Save();
                        }
                    }

                    if (fileCounter > 0)
                    {
                        if (fileCounter == uploadedCounter)
                        {
                            SetSuccessMessage("הנתונים נשמרו בהצלחה");
                        }
                        else if (uploadedCounter < fileCounter)
                        {
                            SetErrorMessage("אירעו שגיאות בחלק מהנתונים");
                        }
                    }
                    else
                    {
                        SetErrorMessage("שמירת הנתונים נכשלה");
                    }

                    return RedirectToAction("Index", "Archives", new { id = model.PatientId });

                }
                else
                {
                    SetErrorMessage(Resources.ArchiveDocumant.BlockAdditionalContentMessage);
                }
            }
           // ModelState.AddModelError(string.Empty, "לא נבחר קובץ");
            return View(model);
        }

        /// <summary>
        /// Get the edit view of document
        /// </summary>
        /// <param name="id">patient id</param>
        /// <param name="documentId">Document id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(int id, int documentId)
        {
            if (!IsPatientInCurrentClinic(id)) { throw new SecurityException(); };
            var patientClinicId = RapidVetUnitOfWork.PatientRepository.GetPatientClinicId(id);
            if (CurrentUser.ActiveClinicId == patientClinicId)
            {
                var document = RapidVetUnitOfWork.ArchivesRepository.GetItem(documentId);
                return View(document);
            }
            throw new SecurityException();
        }

        /// <summary>
        /// the Post method of edit dcoument
        /// </summary>
        /// <param name="model">doc edit model with form data</param>
        /// <param name="file">replacment file</param>
        /// <param name="audioFile">audio file for image</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(ArchiveDocumentEditModel model, HttpPostedFileBase file, HttpPostedFileBase audioFile)
        {
            var document = RapidVetUnitOfWork.ArchivesRepository.GetItem(model.Id);
            if (CurrentUser.IsDoctor && CurrentUser.ActiveClinicId == document.ClinicId)
            {
                if (document.FileType == ArchivesFileType.Image)
                {
                    ProccessImage(model.CroopX, model.CroopY, model.CroopX2, model.CroopY2, model.CropWidth, model.CropHeight, model.Contrast,
                                  model.Brithness, document, CurrentUser.ActiveClinicGroupId);
                }
                AutoMapper.Mapper.Map(model, document);
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        double size;
                        double usage = GetArchiveUsagePercent(CurrentUser.ActiveClinicId, out size);
                        if ((int)usage < MAX_UPLOAD_PERCENTAGE)
                        {
                            if (file.ContentLength >= int.Parse(System.Configuration.ConfigurationManager.AppSettings["FILE_SIZE_LIMIT"]))
                            {
                                SetErrorMessage("הקובץ חורג מהגודל המותר");
                                return View(model);
                            }
                            document.FileExtension = Path.GetExtension(file.FileName) ?? "txt";
                            document.ContentType = file.ContentType;
                            var uploaded = ArchivesUploadHelper.UploadDocument(CurrentUser.ActiveClinicGroupId, document.ClinicId, document.Patient.ClientId, document.PatientId, document.Id + document.FileExtension,
                                 file, false);

                            document.FileSizeInBytes = uploaded ? file.ContentLength : 0;

                        }
                        else
                        {
                            SetErrorMessage(Resources.ArchiveDocumant.BlockAdditionalContentMessage);
                            return RedirectToAction("Index", new { Id = document.PatientId });
                        }
                    }
                }
                //if (audioFile != null && audioFile.ContentLength > 0 && document.FileType == ArchivesFileType.Image)
                //{
                //    document.HasAudio = true;

                //    ArchivesUploadHelper.UploadDocument(CurrentUser.ActiveClinicGroupId, document.ClinicId, 
                //        document.Patient.ClientId, document.PatientId, document.Id + ".wav",
                //            archivePath, audioFile, true);
                //}



                var status = RapidVetUnitOfWork.Save();

                if (string.IsNullOrEmpty(status.Message))
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage("שמירת הנתונים נכשלה");
                }

                return RedirectToAction("Index", "Archives", new { id = document.PatientId });
            }
            throw new SecurityException();
        }

        private static void ProccessImage(int? x, int? y, int? x2, int? y2, int? cropWidth, int? cropHeight, decimal contrast, decimal brightness, ArchiveDocument document, int clinicGroupId)
        {
            if ((x.HasValue && y.HasValue && x2.HasValue && y2.HasValue) || contrast != 0 || brightness != 0)
            {
                DateTime? lastModified;
                using (Stream stream = ArchivesUploadHelper.GetDocumentStream(clinicGroupId, document.ClinicId, document.Patient.ClientId, document.PatientId, document.Id + document.FileExtension, out lastModified))
                {
                    using (Stream newImageStream = new MemoryStream())
                    {
                        //String.Format("crop=({0},{1},{2},{3})&s.brightness={4}&&s.contrast={5};", x, y, x2, y2, brightness, contrast)
                        var settings = new ImageResizer.ResizeSettings();
                        if (contrast != 0)
                        {
                            settings.Add("s.contrast", contrast.ToString());
                        }
                        if (brightness != 0)
                        {
                            settings.Add("s.brightness", brightness.ToString());
                        }
                        if (x.HasValue && y.HasValue && x2.HasValue && y2.HasValue)
                        {
                            settings.Add("crop", string.Format("({0} , {1} , {2} , {3} )", x, y, x2, y2));
                            settings.Add("cropyunits", cropHeight.ToString());
                            settings.Add("cropxunits", cropWidth.ToString());
                        }


                        var i = new ImageResizer.ImageJob(stream, newImageStream, settings);

                        i.Build();
                        ArchivesUploadHelper.UploadProccessImage(clinicGroupId, document.ClinicId, document.Patient.ClientId, document.PatientId, document.Id + document.FileExtension, newImageStream);
                    }
                }
            }
        }

        /// <summary>
        /// get the images as pdf file
        /// </summary>
        /// <param name="id">the paitent id</param>
        /// <returns></returns>
        public ActionResult GetImagesAsPdf(int id)
        {
            var doc = new Document();
            using (var stream = new MemoryStream())
            {
                var writer = PdfWriter.GetInstance(doc, stream);
                doc.Open();
                var docu2 = RapidVetUnitOfWork.ArchivesRepository.GetPatientDocuments(id);
                var docu = RapidVetUnitOfWork.ArchivesRepository.GetPatientDocuments(id).Where(d => d.FileType == ArchivesFileType.Image);
                if (docu.Count() > 0)
                {
                    foreach (var item in docu)
                    {
                        if (CurrentUser.ActiveClinicId != item.ClinicId)
                        {
                            throw new SecurityException(Resources.Exceptions.AccessDenied);
                        }

                        doc.Add(new Paragraph(""));
                        DateTime? lastModified;
                        Stream imgStream = ArchivesUploadHelper.GetDocumentStream(CurrentUser.ActiveClinicGroupId, item.ClinicId, item.Patient.ClientId, item.PatientId, item.Id + item.FileExtension, out lastModified);

                        Image img = Image.GetInstance(imgStream);
                        if (img.Height > img.Width)
                        {
                            if (img.Height > 700)
                            {
                                //Maximum height is 800 pixels.
                                float percentage = 0.0f;
                                percentage = 700 / img.Height;
                                img.ScalePercent(percentage * 100);
                            }
                        }
                        else
                        {
                            if (img.Width > 540)
                            {
                                //Maximum width is 600 pixels.
                                float percentage = 0.0f;
                                percentage = 540 / img.Width;
                                img.ScalePercent(percentage * 100);
                            }
                        }
                        doc.Add(img);

                    }
                    doc.Close();
                }

                return File(stream.ToArray(), "application/pdf", "Archive.pdf");
            }
        }

        /// <summary>
        /// Delete archive item
        /// </summary>
        /// <param name="id">archive document id</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int id)
        {
            var document = RapidVetUnitOfWork.ArchivesRepository.GetItem(id);
            if (IsPatientInCurrentClinic(document.PatientId))
            {
                ArchivesUploadHelper.DeleteDocument(CurrentUser.ActiveClinicGroupId, document.ClinicId, document.Patient.ClientId, document.PatientId, document.Id + document.FileExtension);
                //if (document.HasAudio)
                //{
                //    ArchivesUploadHelper.DeleteDocument(document.ClinicId, document.Id, document.FileExtension, true);
                //}

                RapidVetUnitOfWork.ArchivesRepository.Delete(document);
                var status = RapidVetUnitOfWork.Save();

                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return Json(status.Success);
            }
            throw new SecurityException();
        }

        /// <summary>
        /// show  images side by side
        /// </summary>
        /// <param name="id">patient id</param>
        /// <param name="imageIds"></param>
        /// <returns></returns>
        public ActionResult SideBySideImages(int id, string imageIds, bool print = false)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var imagesIds = RapidVet.Helpers.StringUtils.GetIntList(imageIds);
                var images = RapidVetUnitOfWork.ArchivesRepository.GetImages(CurrentUser.ActiveClinicId, imagesIds);
                ViewBag.Print = print;
                return View(images);
            }
            throw new SecurityException();
        }


        /// <summary>
        /// serch function for archive
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public JsonResult Serach(string query)
        {
            var emptySerach = getemptySerach(query);
            if (!emptySerach)
            {

                var documentsDb = RapidVetUnitOfWork.ArchivesRepository.Search(CurrentUser.ActiveClinicId, query);
                var model = documentsDb.Select(AutoMapper.Mapper.Map<ArchiveDocument, ArchiveDocumentIndexModel>).ToList();
                if (!model.Any())
                {
                    SetErrorMessage("לא נמצאו תוצאות מתאימות");
                }
                else
                {
                    foreach (var m in model)
                    {
                        var doc = documentsDb.Single(d => d.Id == m.Id);
                        m.EditUrl = Url.Action("Edit", "Archives", new { id = doc.PatientId, documentId = m.Id });
                        m.ViewUrl = Url.Action("View", "Archives", new { id = doc.PatientId, documentId = m.Id });
                    }
                }
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            SetErrorMessage(" נא להזין ערך לחיפוש ");
            RapidVet.WebModels.ArchiveDocuments.ArchiveDocumentIndexModel model2 = new RapidVet.WebModels.ArchiveDocuments.ArchiveDocumentIndexModel();
            return Json(model2, JsonRequestBehavior.AllowGet);

        }

        private static bool getemptySerach(string query)
        {

            if (query == "null" || string.IsNullOrEmpty(query) || query == "undefined")
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Get the object from the blob
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isAudio"></param>
        /// <returns></returns>
        public ActionResult GetDocument(int id, bool isAudio = false)
        {
            if (id != 0)
            {
                HttpContext.Response.Cache.SetCacheability(HttpCacheability.Public);
                HttpContext.Response.Cache.SetMaxAge(new TimeSpan(0, 0, 1));
               
                var document = RapidVetUnitOfWork.ArchivesRepository.GetItem(id);
                if (CurrentUser.ActiveClinicId != document.ClinicId)
                {
                    throw new SecurityException(Resources.Exceptions.AccessDenied);
                }
              
                DateTime? lastModified;
                MemoryStream stream = ArchivesUploadHelper.GetDocumentStream(CurrentUser.ActiveClinicGroupId, CurrentUser.ActiveClinicId,
                            (RapidVetUnitOfWork.ClientRepository.GetClientFromPatientId(document.PatientId)).Id,
                            document.PatientId, document.Id + document.FileExtension,
                            out lastModified);


                string contentType = document.ContentType;

                if (stream != null)
                {
                    string rawIfModifiedSince = HttpContext.Request.Headers.Get("If-Modified-Since");
                    if (string.IsNullOrEmpty(rawIfModifiedSince))
                    {
                        // Set Last Modified time
                        HttpContext.Response.Cache.SetLastModified(lastModified.Value);
                    }
                    else
                    {
                        DateTime ifModifiedSince = DateTime.Parse(rawIfModifiedSince);
                        double diff = lastModified.Value.Subtract(ifModifiedSince).TotalMilliseconds;
                        if (diff < 1000)
                        {
                            // The requested file has not changed
                            HttpContext.Response.StatusCode = 304;
                            return Content(string.Empty);
                        }
                    }
                }
              
                return File(stream, contentType, document.Id + document.FileExtension);
            }
            return null;
        }

        /// <summary>
        /// get the recorded audio from wami
        /// </summary>
        /// <param name="id">id is document id</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RecordAudion(int id)
        {
            var stream = Request.InputStream;
            //ArchivesUploadHelper.UploadAudioStream(CurrentUser.ActiveClinicId, id, stream);
            var document = RapidVetUnitOfWork.ArchivesRepository.GetItem(id);
            document.HasAudio = true;
            var status = RapidVetUnitOfWork.Save();

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }


    }
}
