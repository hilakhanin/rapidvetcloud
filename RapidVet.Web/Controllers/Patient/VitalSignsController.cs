﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model.Clients;
using RapidVet.Model.Visits;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.Visits;
using RapidVet.WebModels.VitalSigns;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class VitalSignsController : BaseController
    {
        /// <summary>
        /// Get the vital singes graph
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Index(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var visits = GetChartModel(id);                
                var model = new VitalSignsIndexModel()
                    {
                        ClientDetailsModel = GetClientDetailsModelFromPatient(id),
                        Visits = visits.Select(AutoMapper.Mapper.Map<Visit, VisitModel>).ToList()
                    };
                return View(model);
            }
            throw new SecurityException();
        }

        public string SaveManualVitalSign(int id, int vitalSignID, string dateTime, string val)
        {
            try
            {
                DateTime date;
                DateTime.TryParse(dateTime, out date);
                date = date.AddHours(DateTime.Now.Hour);
                date = date.AddMinutes(DateTime.Now.Minute);
                date = date.AddSeconds(DateTime.Now.Second);
                RapidVetUnitOfWork.VisitRepository.SaveManualVitalSign(id, vitalSignID, date, decimal.Parse(val), null);
                return "1";
            }
            catch (Exception ex)
            {
                SetErrorMessage(ex.Message);
            }
            return "0";
        }

        public void SaveVitalSign(int id, int manualID, int vitalSignID, string val, string vitalDate)
        {
            try
            {
                DateTime vDate;
                if (!DateTime.TryParse(vitalDate, out vDate))
                    vDate = DateTime.MinValue;

                VitalSignType vst = (VitalSignType)vitalSignID;
                RapidVetUnitOfWork.VisitRepository.SaveVitalSign(id, manualID, vst, Decimal.Parse(val), vDate);
            }
            catch (Exception ex)
            {
                SetErrorMessage(ex.Message);
            }
        }

        public ActionResult WeightChart(int id)
        {
            return View(GetChartModel(id));
        }

        public ActionResult BPMChart(int id)
        {
            return View(GetChartModel(id));
        }

        public ActionResult TempChart(int id)
        {
            return View(GetChartModel(id));
        }

        public ActionResult PulseChart(int id)
        {
            return View(GetChartModel(id));
        }

        public ActionResult BCSChart(int id)
        {
            return View(GetChartModel(id));
        }

        private List<Visit> GetChartModel(int id)
        {
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
            if (patient.Client.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException();
            }
           // var visits = RapidVetUnitOfWork.VisitRepository.GetPatientVisits(id);
            var manualVisits = RapidVetUnitOfWork.VisitRepository.GetPatientManualVisits(id);
            var mappedManualVisits = manualVisits == null ? null : manualVisits.Select(AutoMapper.Mapper.Map<VitalSign, Visit>).ToList();
            //if (mappedManualVisits != null && mappedManualVisits.Count > 0)
            //    visits.AddRange(mappedManualVisits);

            return mappedManualVisits;
        }

        [HttpGet]
        public ActionResult Delete(string id)
        {
            var splttedIds = id.Split('_');
            return PartialView("_Delete", new VisitModel()
            {
                Id = Convert.ToInt32(splttedIds[0]),
                ManualId = Convert.ToInt32(splttedIds[1]),
                VitalSignID = Convert.ToInt32(splttedIds[2])
            });
        }

        public JsonResult Delete(string Id, int ManualId, int VitalSignID)
        {
            int visitID = Convert.ToInt32((Id.Split('_')[0]));
            VitalSignType vst = (VitalSignType)VitalSignID;
            var result = new JsonResult();
            var r = RapidVetUnitOfWork.VisitRepository.DeleteVitalSign(visitID, ManualId, vst);
            result.Data = new
            {
                success = r,
                redirectUrl = Request.UrlReferrer,
                isRedirect = true,
                indexId = RapidVet.RapidConsts.Controllers.VitalSignConsts.TABLE_ID,
                modalId = RapidVet.RapidConsts.Controllers.VitalSignConsts.MODAL_ID
            };

            return result;
        }
    }
}
