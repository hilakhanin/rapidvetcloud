﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.PatientFollowUp;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class HistoryController : BaseController
    {
        //
        // GET: /History/

        public ActionResult Index(int id)
        {
            var model = GetClientDetailsModelFromPatient(id);
            ViewBag.NextEntryID = GetPatientNextEntryId(id);
            ViewBag.NextWatchID = GetPatientNextWatchId(id);
            model.NextClientWatch = RapidVetUnitOfWork.CalenderRepository.GetNextClientWatch(id);
            Session["EntryId"] = 0;
            return View(model);
        }

        private List<HistoryDate> getHistoryDates(int id, bool isForPrint = false, string dtFrom = "01/01/1990", string dtTo = "31/12/2045", string isToShowMoney = "true")
        {
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForHistory(id);
            var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(CurrentUser.ActiveClinicId);

            var historyItems = new List<HistoryItem>();
            var historyDates = new List<HistoryDate>();
            DateTime dFrom = Convert.ToDateTime(dtFrom);
            DateTime dTo = Convert.ToDateTime(dtTo);

            historyItems.AddRange(AutoMapper.Mapper.Map<List<HistoryItem>>(patient.Quotations.Where(l => l.Created >= dFrom && l.Created <= dTo)));
            historyItems.AddRange(AutoMapper.Mapper.Map<List<HistoryItem>>(patient.Archives.Where(l => l.Created >= dFrom && l.Created <= dTo)));
            historyItems.AddRange(AutoMapper.Mapper.Map<List<HistoryItem>>(patient.Letters.Where(l => l.CreatedDate >= dFrom && l.CreatedDate <= dTo)));
            historyItems.AddRange(AutoMapper.Mapper.Map<List<HistoryItem>>(patient.MedicalProcedures.Where(l => l.Start >= dFrom && l.Finish <= dTo)));
            historyItems.AddRange(AutoMapper.Mapper.Map<List<HistoryItem>>(patient.DischargePapers.Where(l => l.DateTime >= dFrom && l.DateTime <= dTo)));
            historyItems.AddRange(AutoMapper.Mapper.Map<List<HistoryItem>>(patient.MinistryOfAgricultureReports.Where(l => l.DateTime >= dFrom && l.DateTime <= dTo)));
            historyItems.AddRange(AutoMapper.Mapper.Map<List<HistoryItem>>(patient.LabTests.Where(l => l.Created >= dFrom && l.Created <= dTo)));
            historyItems.AddRange(AutoMapper.Mapper.Map<List<HistoryItem>>(patient.Client.CalenderEntries.Where(l => l.Date >= dFrom && l.Date <= dTo)));
            if (CurrentUser.ActiveClinic.ShowFollowUpsInPatientHistory)
            {
                historyItems.AddRange(patient.FollowUps
                    .Select(AutoMapper.Mapper.Map<FollowUp, HistoryItem>)
                    .Where(l => l.Date >= dFrom && l.Date <= dTo)
                    .ToList());
            }
            foreach (var historyItem in historyItems.Where(h => h.ItemType == "archive"))
            {
                if (string.IsNullOrEmpty(historyItem.Name))
                {
                    historyItem.Name = "פריט ארכיון";
                }
            }
            foreach (var visit in patient.Visits.Where(l => l.VisitDate >= dFrom && l.VisitDate <= dTo))
            {
                if ((!visit.Active && !CurrentUser.ActiveClinic.ShowOldVisits) || (!visit.Active && isForPrint))
                    continue;

                var visitString = new StringBuilder();
                if (clinic.ShowComplaint)
                {
                    if (visit.MainComplaint != null && visit.MainComplaint.Length > 0)
                    {
                        visitString.Append("<b>תלונה עיקרית: </b>");
                        visitString.Append(visit.MainComplaint);
                        visitString.Append("<br/>");
                    }
                }
                if (clinic.ShowExaminations)
                {
                    if (visit.Examinations.Count > 0)
                    {
                        visitString.Append("<b>בדיקות:</b> ");

                        foreach (var examination in visit.Examinations)
                        {

                            visitString.Append(examination.PriceListItem != null
                                                   ? examination.PriceListItem.Name
                                                   : examination.Name);

                            if (examination != visit.Examinations.Last())
                            {
                                visitString.Append(", ");
                            }
                        }
                        visitString.Append("<br/>");
                    }
                }
                if (clinic.ShowDiagnosis)
                {
                    if (visit.Diagnoses.Count > 0)
                    {
                        visitString.Append("<b>אבחנות:</b> ");
                        foreach (var diagnosis in visit.Diagnoses)
                        {
                            if (String.IsNullOrEmpty(diagnosis.Name))
                                continue;

                            visitString.Append(diagnosis.Name);
                            if (diagnosis != visit.Diagnoses.Last())
                            {
                                visitString.Append(", ");
                            }
                        }
                        visitString.Append("<br/>");
                    }
                }
                if (clinic.ShowTreatments)
                {
                    if (visit.Treatments.Count > 0)
                    {
                        visitString.Append("<b>טיפולים:</b> ");
                        //visitString.Append("<br/>");
                        foreach (var treatment in visit.Treatments)
                        {
                            visitString.Append(treatment.Name);
                            if (treatment != visit.Treatments.Last())
                            {
                                visitString.Append(", ");
                            }

                        }

                        if (visit.PreventiveMedicineItems.Count > 0)
                        {
                            visitString.Append(", ");
                        }
                        else
                        {
                            visitString.Append("<br/>");
                        }
                    }
                    if (visit.PreventiveMedicineItems.Count > 0)
                    {
                        if (visit.Treatments.Count == 0)
                        {
                            visitString.Append("<b>טיפולים:</b> ");
                            //visitString.Append("<br/>");
                        }
                        foreach (var preventiveMedicine in visit.PreventiveMedicineItems)
                        {
                            visitString.Append(preventiveMedicine.PriceListItem.Name);
                            if (preventiveMedicine != visit.PreventiveMedicineItems.Last())
                            {
                                visitString.Append(", ");
                            }
                        }
                        visitString.Append("<br/>");
                    }
                }
                if (clinic.ShowMedicins)
                {
                    if (visit.Medications.Count > 0)
                    {
                        visitString.Append("<b>תרופות:</b> ");
                        foreach (var medication in visit.Medications)
                        {
                            visitString.Append(medication.Name);
                            if (medication != visit.Medications.Last())
                            {
                                visitString.Append(", ");
                            }
                        }
                        visitString.Append("<br/>");
                    }
                }
                if (clinic.ShowVisitNote)
                {
                    if (!string.IsNullOrWhiteSpace(visit.VisitNote))
                    {
                        visitString.Append("<b>הערת ביקור:</b> ");

                        visitString.Append(visit.VisitNote);

                        visitString.Append("<br/>");
                    }
                }
                if (clinic.ShowTemperature)
                {
                    visitString.Append("<b>טמפרטורה:</b> ");
                    visitString.Append(visit.Temp);
                    visitString.Append("<br/>");
                }
                if (clinic.ShowWeight)
                {
                    visitString.Append("<b>משקל:</b> ");
                    visitString.Append(visit.Weight);
                    visitString.Append("<br/>");
                }

                if (visitString.Length == 0) // || (visitString.Length > 0 && visitString.ToString().Equals("<b>תלונה עיקרית:</b><br/>")))
                {
                    //visitString.Clear();
                    visitString.Append("ביקור במרפאה");
                }

                string externalDrName = "";
                bool isExternallyPerformed = false;

                foreach (var preventiveMedicine in visit.PreventiveMedicineItems)
                {
                    if (preventiveMedicine.ExternalyPreformed)
                    {
                        isExternallyPerformed = true;
                        externalDrName = preventiveMedicine.ExternalDrName;
                    }
                }

                var treatedBy = isExternallyPerformed ? externalDrName : visit.Doctor != null
                                  ? visit.Doctor.Name
                                  : string.Empty;

                historyItems.Add(new HistoryItem()
                {
                    Id = visit.Id,
                    Name = visitString.Replace("&quot;","\"").ToString(),
                    Date = visit.VisitDate,
                    ItemType = "visit",
                    Sum = visit.Price,
                    Color = visit.Color,
                    DaysFromTreatment = (DateTime.Now - visit.VisitDate).Days.ToString(),
                    HandlingDoctor = treatedBy,
                    Url = Url.Action("Index", "Visit", new { id = patient.Id, visitId = visit.Id }),
                    Active = visit.Active
                });
            }

            historyItems = historyItems.OrderBy(i => i.Date).ToList();

            foreach (var historyItem in historyItems)
            {
                var date = new DateTime(historyItem.Date.Year, historyItem.Date.Month, historyItem.Date.Day);
                var historyDate = historyDates.SingleOrDefault(hd => hd.Date == date);
                if (historyDate != null)
                {
                    historyDates.Single(h => h.Date == date).Items.Add(historyItem);
                }
                else
                {
                    historyDates.Add(new HistoryDate() { Date = date });
                    historyDates.Single(h => h.Date == date).Items.Add(historyItem);
                }
            }

            historyDates = historyDates.OrderByDescending(hd => hd.Date).ToList();

            return historyDates;
        }

        public JsonResult GetHistory(int id)
        {
            var historyDates = getHistoryDates(id);

            return Json(historyDates, JsonRequestBehavior.AllowGet); //.ThenBy(h=>h.Items.OrderBy(i=>i.Date))
        }

        public ActionResult PrintHistory(int id, string dtFrom = "01/01/2017", string dtTo = "31/12/2017", string isToShowMoney = "true")
        {
            PatientHistoryIndexModel model = new PatientHistoryIndexModel(){
                HistoryDates = getHistoryDates(id, true, dtFrom, dtTo, isToShowMoney),
                Client = GetClientDetailsModelFromPatient(id, dtFrom, dtTo, isToShowMoney)
            };
            ViewBag.isToShowMoney = isToShowMoney;
            return View(model);
        }
        
    }
}
