﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using RapidVet.FilesAndStorage;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Finance;
using RapidVet.Model.Patients;
using RapidVet.Model.Visits;
using RapidVet.Repository.Clients;
using RapidVet.Repository.Clinics;
using RapidVet.Repository.Patients;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.Patients;
using RapidVet.WebModels.Visits;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class PatientsController : BaseController
    {
        //id is patientId
        public ActionResult Index(int id)
        {
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
            if (patient.Client.ClinicId != CurrentUser.ActiveClinicId)
                throw new SecurityException();

            var comments = new StringBuilder();
            var dt = DateTime.Now;
            foreach (var comment in patient.Comments.Where(c => c.Active && c.Popup && (!c.ValidThrough.HasValue || c.ValidThrough.Value > dt)))
                comments.Append(string.Format("{0}|", comment.CommentBody));

            if (!String.IsNullOrEmpty(patient.Allergies))
                if (comments.Length > 0)
                    comments.Insert(0, string.Format("{0}:^ {1}|", Resources.Patient.Allergies , patient.Allergies));
                else
                    Response.Cookies.Add(new HttpCookie("allergies", patient.Allergies));

            if (comments.Length > 0)
                Response.Cookies.Add(new HttpCookie("comments", comments.ToString().Trim('|')));

            return RedirectToAction("Index", "History", new { id = id });
        }


        //id is clientId
        [HttpGet]
        public ActionResult Create(int id)
        {
            if (RapidVetUnitOfWork.ClientRepository.IsClientInClinic(CurrentUser.ActiveClinicId, id))
            {
                var client = RapidVetUnitOfWork.ClientRepository.GetClient(id);
                int animal_kind_id = 0;

                //shira - fix for bug when there's no default animal kind
                if (CurrentUser.ActiveClinic.AnimalKindId == 0)
                {
                    var animal_kind = RapidVetUnitOfWork.AnimalKindRepository.GetList(client.Clinic.ClinicGroupID).OrderBy(k => k.Id).First();
                    animal_kind_id = animal_kind.Id;
                }
                //end fix

                var model = new PatientCreateModel()
                    {
                        Client = GetClientDetailsModel(id),
                        ClientId = id,
                        HumanDangerous = false,
                        AnimalDangerous = false,
                        PureBred = false,
                        Fertilization = false,
                        Sterilization = false,
                        AnimalKindId = (CurrentUser.ActiveClinic.AnimalKindId == 0) ? animal_kind_id : CurrentUser.ActiveClinic.AnimalKindId,
                        AnimalRaceId = 0,
                        Active = true
                    };

                return View(model);
            }
            return RedirectToAction("Index", "Clients");
        }

        [HttpPost]
        public ActionResult Create(PatientCreateModel model, HttpPostedFileBase profilePic)
        {
            model.Id = 0;
            model.Comments = GetCommentModelList();
            var tempSterilizationDate = Request.Form["SterilizationDate"];
            if (!string.IsNullOrWhiteSpace(tempSterilizationDate) && tempSterilizationDate.Contains('\\'))
            {
                tempSterilizationDate = tempSterilizationDate.Replace('\\', '/');
                DateTime sterDate;
                DateTime.TryParse(tempSterilizationDate, out sterDate);
                model.SterilizationDate = sterDate;
            }
            //model.PureBred = StringUtils.GetCheckBoxValue(Request.Form["PureBread"]);
            //model.Fertilization = StringUtils.GetCheckBoxValue(Request.Form["Fertilization"]);


            //if (ModelState.IsValid)
            //{

            var patient = AutoMapper.Mapper.Map<PatientCreateModel, Patient>(model);
            //map birthdate (this was causing problems in automapper so moved it here)
            if (model.BirthDate.HasValue && model.BirthDate.Value > DateTime.MinValue)
            {
                patient.BirthDate = model.BirthDate.Value;
            }
            else
            {
                patient.BirthDate = null;
            }
            patient.Id = 0;
            //patient.Active = true;
            patient.LocalId = ClinicLocalIdManager.GetNextLocalPatientId(CurrentUser.ActiveClinicId);
            patient.CreatedById = CurrentUser.Id;
            patient.CreatedDate = DateTime.Now;
            var dt = DateTime.Now;
            foreach (var comment in model.Comments)
            {
                patient.Comments.Add(new PatientComment()
                    {
                        Active = true,
                        CreatedDate = dt,
                        CreatedById = CurrentUser.Id,
                        Popup = comment.Popup,
                        ValidThrough = null,//comment.ValidThrough,
                        CommentBody = comment.CommentBody,
                    });
            }
            var status = RapidVetUnitOfWork.PatientRepository.Create(patient);
            RapidVetUnitOfWork.ClientRepository.SetClientNameWithPatients(model.ClientId, true);
            if (status.Success)
            {
                //save the image
                if (profilePic != null)
                {
                    if (profilePic.ContentType.StartsWith("image") && profilePic.ContentLength > 0)
                    {
                        //Image image = new Bitmap(profilePic.InputStream);
                        var uploaded = FilesAndStorage.PatientUploadHelper.UploadPatientProfileImage(CurrentUser.ActiveClinicGroupId, CurrentUser.ActiveClinicId, patient.ClientId, patient.Id, profilePic);
                        patient.ProfilePicSizeInBytes = uploaded ? profilePic.ContentLength : 0;
                        RapidVetUnitOfWork.Save();
                    }
                }
                RapidVetUnitOfWork.ClientRepository.SetClientNameWithPatients(model.ClientId);
                SetSuccessMessage();
                return RedirectToAction("Patients", "Clients", new { id = patient.ClientId });
            }
            else
            {
                SetErrorMessage();
            }
            //}
            var client = RapidVetUnitOfWork.ClientRepository.GetClient(model.ClientId);
            model.Client = GetClientDetailsModel(model.ClientId);
            return View(model);
        }


        //id is patientId
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
            var model = AutoMapper.Mapper.Map<Patient, PatientCreateModel>(patient);
            model.Client = GetClientDetailsModel(model.ClientId);
            if (patient.AnimalRace != null)
            {
                model.AnimalKind = AutoMapper.Mapper.Map<AnimalKind, AnimalKindModel>(patient.AnimalRace.AnimalKind);
                model.AnimalKindId = patient.AnimalRace.AnimalKindId;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(PatientCreateModel model, HttpPostedFileBase profilePic)
        {
            model.Comments = GetCommentModelList();
            //model.PureBred = StringUtils.GetCheckBoxValue(Request.Form["PureBread"]);
            //model.Fertilization = StringUtils.GetCheckBoxValue(Request.Form["Fertilization"]);

            //if (Request.Form["SterilizationDate"] != null)
            //{
            //    model.SterilizationDate = DateTime.ParseExact(Request.Form["SterilizationDate"], "dd/MM/yyyy", null);
            //}

            //if (ModelState.IsValid)
            //{
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(model.Id);
            patient.UpdatedById = CurrentUser.Id;
            patient.UpdatedDate = DateTime.Now;

            if (model.AnimalRaceId != 0)
            {
                patient.AnimalRaceId = model.AnimalRaceId;
            }
            else
            {
                patient.AnimalRaceId = RapidVetUnitOfWork.AnimalRaceRepository.GetEmptyRaceID(model.AnimalKindId); //empty row
            }

            if (model.AnimalColorId != 0)
            {
                patient.AnimalColorId = model.AnimalColorId;
            }

            if (!string.IsNullOrEmpty(model.Name))
            {
                patient.Name = model.Name;
            }

            patient.GenderId = model.GenderId;
            patient.PureBred = model.PureBred;
            patient.Fertilization = model.Fertilization;
            patient.Sterilization = model.Sterilization;
            patient.SterilizationDate = model.SterilizationDate;
            patient.ElectronicNumber = model.ElectronicNumber;
            patient.LicenseNumber = model.LicenseNumber;
            patient.Owner_Farm = model.Owner_Farm;
            patient.SAGIRNumber = model.SAGIRNumber;
            patient.BloodType = model.BloodType;
            patient.Allergies = model.Allergies;
            patient.FoodTypeId = model.FoodTypeId;
            patient.AnimalDangerous = model.AnimalDangerous;
            patient.HumanDangerous = model.HumanDangerous;
            patient.FoodAmount = model.FoodAmount;
            patient.SeeingEyeDog = model.SeeingEyeDog;
            patient.Exemption = model.Exemption;
            patient.ExemptionCause = model.ExemptionCause;
            patient.Active = model.Active;
            if (model.BirthDate.HasValue && model.BirthDate > DateTime.MinValue)
            {
                patient.BirthDate = model.BirthDate.Value;
            }


            //handle comments
            var currentComments = patient.Comments.ToList();

            //existing comments to be removed 
            foreach (var comment in currentComments)
            {
                var removeFromClient = model.Comments.SingleOrDefault(c => c.Id == comment.Id) == null;
                if (removeFromClient)
                {
                    comment.Active = false;
                }
            }

            foreach (var comment in model.Comments)
            {
                comment.PatientId = patient.Id;

                //new comments to be added
                if (comment.Id == 0)
                {
                    patient.Comments.Add(new PatientComment()
                        {
                            CreatedById = CurrentUser.Id,
                            CreatedDate = DateTime.Now,
                            PatientId = patient.Id,
                            Popup = comment.Popup,
                            Active = true,
                            ValidThrough = (comment.ValidThrough != null && comment.ValidThrough > DateTime.MinValue) ? comment.ValidThrough : (DateTime?)null,
                            CommentBody = comment.CommentBody
                        });
                }
                else
                {
                    //existing comments to be updated
                    foreach (var currentComment in patient.Comments)
                    {
                        if (currentComment.Id == comment.Id)
                        {
                            currentComment.UpdatedById = CurrentUser.Id;
                            currentComment.UpdatedDate = DateTime.Now;
                            currentComment.CommentBody = comment.CommentBody;
                            currentComment.ValidThrough = (comment.ValidThrough != null && comment.ValidThrough > DateTime.MinValue) ? comment.ValidThrough : (DateTime?)null;
                            currentComment.Popup = comment.Popup;
                        }
                    }
                }
            }


            var status = RapidVetUnitOfWork.PatientRepository.Update(patient);
            if (status.Success)
            {
                if (profilePic != null)
                {
                    //save the image
                    if (profilePic.ContentLength > 0)
                    {
                        if (profilePic.ContentType.StartsWith("image"))
                        {
                            //Image image = new Bitmap(profilePic.InputStream);
                           var uploaded = PatientUploadHelper.UploadPatientProfileImage(CurrentUser.ActiveClinicGroupId,
                                CurrentUser.ActiveClinicId, patient.ClientId, patient.Id, profilePic);
                           patient.ProfilePicSizeInBytes = uploaded ? profilePic.ContentLength : 0;
                           RapidVetUnitOfWork.Save();
                        }
                    }
                }

                RapidVetUnitOfWork.ClientRepository.SetClientNameWithPatients(model.ClientId);
                SetSuccessMessage();
                return RedirectToAction("Patients", "Clients", new { id = patient.ClientId });
            }
            else
            {
                SetErrorMessage();
            }
            //}
            model.Client = GetClientDetailsModel(model.ClientId);
            return View(model);
        }

        //id is patient Id
        //[OutputCache(Duration = 360000 )]
        public ActionResult ProfileImage(int id)
        {
            if (id != 0)
            {
                HttpContext.Response.Cache.SetCacheability(HttpCacheability.Public);
                HttpContext.Response.Cache.SetMaxAge(new TimeSpan(0, 0, 1));

                var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
                if (patient.Client.ClinicId != CurrentUser.ActiveClinicId)
                {
                    throw new SecurityException(Resources.Exceptions.AccessDenied);
                }
                DateTime? lastModified;
                Stream stream = PatientUploadHelper.GetProfileImageStream(CurrentUser.ActiveClinicGroupId, CurrentUser.ActiveClinicId, patient.ClientId, patient.Id, out lastModified);
                if (stream == null)
                {

                    var dir = Server.MapPath("/Content/img/RapidVet/animale-icons");
                    if (patient.AnimalRace != null)
                    {
                        var path = Path.Combine(dir, patient.AnimalRace.AnimalKind.AnimalIcon.FileName);

                        return File(path, "image/png");
                    }
                    return null;
                }
                else
                {
                    string rawIfModifiedSince = HttpContext.Request.Headers.Get("If-Modified-Since");
                    if (string.IsNullOrEmpty(rawIfModifiedSince))
                    {
                        // Set Last Modified time
                        HttpContext.Response.Cache.SetLastModified(lastModified.Value);
                    }
                    else
                    {
                        DateTime ifModifiedSince = DateTime.Parse(rawIfModifiedSince);
                        double diff = lastModified.Value.Subtract(ifModifiedSince).TotalMilliseconds;
                        if (diff < 1000)
                        {
                            // The requested file has not changed
                            HttpContext.Response.StatusCode = 304;
                            return Content(string.Empty);
                        }
                    }
                }
                return new FileStreamResult(stream, "image/png");
            }
            else
            {
                var dir = Server.MapPath("/Content/img/RapidVet/animale-icons");

                var path = Path.Combine(dir, "dog.png");

                var stream = new FileStream(path, FileMode.Open);

                return new FileStreamResult(stream, "image/png");
            }
        }



        /// <summary>
        /// gets values from Request.Foem and converts them to PatientCommentModel
        /// </summary>
        /// <returns>List(PatientCommentModel)</returns>
        private List<PatientCommentModel> GetCommentModelList()
        {
            var comments = new List<PatientCommentModel>();
            foreach (var key in Request.Form.AllKeys)
            {
                if (key.StartsWith("CommentBody_"))
                {
                    var commentKey = key.Split('_');
                    var commentBody = Request.Form[key];
                    //var validThrough = Request.Form["validThrough_" + commentKey[1]];
                    var popUp = Request.Form["popup_" + commentKey[1]];
                    var commentIdStr = Request.Form["idComment_" + commentKey[1]];

                    var commentId = 0;
                    int.TryParse(commentIdStr, out commentId);

                    var pop = StringUtils.GetCheckBoxValue(popUp);

                    if (!string.IsNullOrWhiteSpace(commentBody))// && !string.IsNullOrWhiteSpace(validThrough))
                    {
                        comments.Add(new PatientCommentModel()
                            {
                                Id = commentId,
                                CommentBody = commentBody,
                                Popup = pop
                                //ValidThrough = DateTime.Parse(validThrough)
                            });
                    }
                }
            }
            return comments;
        }


        [HttpGet]
        public JsonResult GetAnimalKindOptions()
        {
            List<AnimalKind> animals;
            var repository = RapidVetUnitOfWork.AnimalKindRepository;
            animals = repository.GetList(CurrentUser.ActiveClinicGroupId).ToList();

            List<SelectListItem> list = animals.Where(x => x.Active).Select(d => new SelectListItem()
            {
                Value = d.Id.ToString(),
                Text = d.Value,
                Selected = false
            }).ToList();

            list.Insert(0, new SelectListItem()
            {
                Value = "",
                Text = Resources.Global.Select,
                Selected = true
            });

            return Json(list, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetAnimalRaceOptions(int? kindId)
        {
            var list = new List<SelectListItem>();
            if (kindId.HasValue && kindId.Value != 0)
            {
                var races = RapidVetUnitOfWork.AnimalRaceRepository.GetListByAnimalKind(kindId.Value);
                list = races.Where(x => x.Active).Select(r => new SelectListItem()
                    {
                        Value = r.Id.ToString(),
                        Text = r.Value,
                        Selected = false
                    }).ToList();
            }
            return Json(list, JsonRequestBehavior.AllowGet);

        }
    }
}
