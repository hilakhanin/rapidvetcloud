﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using Newtonsoft.Json.Linq;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.PatientLabTests;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.PatientsLabTests;
using RapidVet.WebModels.LabTests;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using RapidVet.Repository;
using System.Collections.ObjectModel;
using System.Data.Entity.SqlServer;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class LabsController : BaseController
    {
        public ActionResult Index(int id, string Data = "", string cc = "0", string Error = "")
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = GetClientDetailsModelFromPatient(id);
                int? labTestType = null;
                int? labTestId = null;
                int? labTestTemplateId = null;
                string lastLabTestTypeName = null;
                StringBuilder sb = new StringBuilder();
                StringBuilder sbPatients = new StringBuilder();

                var lastLabTest = RapidVetUnitOfWork.LabTestsRepository.GetLastLabTest(id);
                if (lastLabTest != null)
                {
                    labTestId = lastLabTest.Id;
                    labTestType = lastLabTest.LabTestTemplate.LabTestTypeId;
                    labTestTemplateId = lastLabTest.LabTestTemplate.Id;
                    lastLabTestTypeName = lastLabTest.LabTestTemplate.LabTestType.Name;
                }

                ViewBag.lastLabTestTypeId = labTestType;
                ViewBag.lastLabTestId = labTestId;
                ViewBag.lastLabTestTypeName = lastLabTestTypeName;
                ViewBag.labTestTemplateId = labTestTemplateId;
                ViewBag.HeaderData = Server.UrlDecode(Data);

                if (Data.Length > 0)
                {
                    var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
                    var Templates = RapidVetUnitOfWork.LabTestsRepository.GetLabTestTemplatesByAnimalKindId(patient.AnimalRace.AnimalKindId).ToList();
                    sb.Append("<option value='0'>בחר..</option>");
                    foreach (var template in Templates)
                    {
                        sb.Append("<option value='" + template.Id.ToString() + "'>" + template.Name + "</option>");
                    }

                    var Patients = RapidVetUnitOfWork.PatientRepository.GetClientPatients(Convert.ToInt32(cc)).ToList();
                    sbPatients.Append("<option value='-1'>בחר.. </option>");
                    foreach (var patientt in Patients)
                    {
                        sbPatients.Append("<option value='" + patientt.Id.ToString() + "'>" + patientt.Name + "</option>");
                    }
                }

                ViewBag.LabTestTemplates = sb.ToString();
                ViewBag.Patients = sbPatients.ToString();
                ViewBag.ClientID = cc;
                ViewBag.ID = id.ToString();
                ViewBag.Error = Error;

                return View(model);
            }
            throw new SecurityException();
        }


        public JsonResult GetPatientData(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
                var testTypes = RapidVetUnitOfWork.LabTestsRepository.GetLabTestTypesForPatient(id, CurrentUser.ActiveClinicId);
                var allData = testTypes.Select(AutoMapper.Mapper.Map<LabTestType, PatientLabTestIndexModel>).ToList();
                var model = new List<PatientLabTestIndexModel>();
                foreach (var type in testTypes)
                {
                    var testTemplates = RapidVetUnitOfWork.LabTestsRepository.GetLabTestTemplates(type.Id, patient.AnimalRace.AnimalKindId);
                    var patientIndex = allData.SingleOrDefault(l => l.LabTestTypeId == type.Id);
                    if (patientIndex != null)
                    {
                        patientIndex.LabTestTemplates = testTemplates.Select(AutoMapper.Mapper.Map<LabTestTemplate, LabTestTemplatesWebModel>).ToList();

                        foreach (var test in testTemplates)
                        {
                            var preformances = RapidVetUnitOfWork.PatientLabTestRepository.GetTestPreformances(id, test.Id, CurrentUser.ActiveClinicId);
                            var labTestTemplateDates = patientIndex.LabTestTemplates.SingleOrDefault(l => l.Id == test.Id);
                            if (labTestTemplateDates != null)
                            {
                                labTestTemplateDates.Dates = preformances.Select(AutoMapper.Mapper.Map<PatientLabTest, PatientLabTestDate>).ToList();
                            }
                        }
                    }

                }

                for (int i = 0; i < allData.Count; i++)
                {
                    for (int j = allData[i].LabTestTemplates.Count - 1; j >= 0; j--)
                    {
                        var labTestTemplates = allData[i].LabTestTemplates.ToList();
                        if (labTestTemplates[j].Dates.Count == 0)
                        {
                            allData[i].LabTestTemplates.Remove(labTestTemplates[j]);
                        }
                    }

                    if (allData[i].LabTestTemplates.Count > 0)
                    {
                        model.Add(allData[i]);
                    }

                }
                //     allData = allData.Where(t => t.LabTestTemplates.Where(l => l.Dates.Count() > 0) != null).ToList();
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            throw new SecurityException();
        }

        private PatientLabTestData GetPatientLabTestData(int id, int patientLabTestId)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var patientLabTest = RapidVetUnitOfWork.PatientLabTestRepository.GetPatientLabTest(patientLabTestId);
                var results =
                    patientLabTest.Results.Select(AutoMapper.Mapper.Map<PatientLabTestResult, PatientLabTestResultModel>)
                                  .ToList();
                foreach (var r in results)
                {
                    r.Indicator = StringUtils.GetIndicator(r.Result, r.Min, r.Max);
                }

                if (patientLabTest.PatientId == id)
                {
                    var model = new PatientLabTestData()
                    {
                        Name = patientLabTest.LabTestTemplate.Name,
                        Results = results,
                        Created = patientLabTest.Created.ToShortDateString()
                    };
                    return model;
                }
            }
            throw new SecurityException();
        }

        public JsonResult GetTestData(int id, int patientLabTestId)
        {
            if (IsPatientInCurrentClinic(id))
            {
                return Json(GetPatientLabTestData(id, patientLabTestId), JsonRequestBehavior.AllowGet);
            }
            throw new SecurityException();
        }

        public ActionResult Print(int id, int labTestTemplateId, int? labId)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = new PatientLabTestPrintIndexModel();
                model.ClientDetailsModel = GetClientDetailsModelFromPatient(id);
                model.PatientId = id;
                if (labId.HasValue)
                {
                    model.PatientLabTestData = GetPatientLabTestData(id, labId.Value);
                    model.isPrintingComparativeTestData = false;
                }
                else
                {
                    model.PatientLabTestGroupResult = GetPatientLabTestGroupResultData(id, labTestTemplateId);
                    model.isPrintingComparativeTestData = true;
                }

                return View(model);
            }
            throw new SecurityException();
        }

        private List<PatientLabTestGroupResult> GetPatientLabTestGroupResultData(int id, int labTestTemplateId)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var patientLabTest = RapidVetUnitOfWork.PatientLabTestRepository.GetPatientGroupLabTest(id, labTestTemplateId);
                var model = new List<PatientLabTestGroupResult>();

                for (int i = 0; i < patientLabTest.Count; i++)
                {
                    var results =
                        patientLabTest[i].Results.Select(AutoMapper.Mapper.Map<PatientLabTestResult, PatientLabTestResultModel>)
                                      .ToList();

                    foreach (var r in results)
                    {
                        PatientLabTestGroupResult modelResult = model.SingleOrDefault(l => l.LabTestId == r.LabTestId);

                        if (modelResult == null)
                        {
                            modelResult = new PatientLabTestGroupResult()
                            {
                                Id = r.Id,
                                LabTestId = r.LabTestId,
                                LabTestName = r.Name,
                                Min = r.Min,
                                Max = r.Max
                            };

                            model.Add(modelResult);

                            //add missing tests to previous ones
                            if (i != 0)
                            {
                                for (int j = 0; j < i; j++)
                                {
                                    modelResult.Results.Add(new GroupTestResults() { Result = "", Indicator = "<span></span>", LabTestId = r.LabTestId, Created = patientLabTest[j].Created.ToShortDateString() });
                                }
                            }
                        }

                        if (r.Result == null)
                        {
                            r.Indicator = "<span></span>";
                        }
                        else
                        {
                            r.Indicator = StringUtils.GetIndicator(r.Result, r.Min, r.Max);
                        }

                        modelResult.Results.Add(new GroupTestResults() { Result = r.Result, Indicator = r.Indicator, LabTestId = r.LabTestId, Created = patientLabTest[i].Created.ToShortDateString() });
                    }

                }

                return model;
            }
            throw new SecurityException();
        }

        public JsonResult GetLastGroupTestData(int id, int labTestTemplateId)
        {
            if (IsPatientInCurrentClinic(id))
            {
                return Json(GetPatientLabTestGroupResultData(id, labTestTemplateId), JsonRequestBehavior.AllowGet);
            }
            throw new SecurityException();
        }

        public ActionResult Create(int id)//, int labTestTypeId)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = GetClientDetailsModelFromPatient(id);
                
                return View(model);
            }
            throw new SecurityException();
        }
        public void importXML(int id)//, int labTestTypeId)
        {
            StringBuilder sb = new StringBuilder();
            string ClientId = "0";
            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    MemoryStream target = new MemoryStream();
                    file.InputStream.CopyTo(target);
                    byte[] data = target.ToArray();
                    string xml = Encoding.UTF8.GetString(data);
                    Session["IDEXX_XML"] = xml;
                    XDocument xmlDoc = XDocument.Parse(xml);
                    sb.Append(xmlDoc.Descendants("first_name").FirstOrDefault().Value + " " + xmlDoc.Descendants("last_name").FirstOrDefault().Value);
                    sb.Append("*");
                    sb.Append(xmlDoc.Descendants("patient_name").FirstOrDefault().Value + " " + xmlDoc.Descendants("patient").Attributes("patient_species").FirstOrDefault().Value);

                    string TemplateName = xmlDoc.Descendants("result").Attributes("instrument").FirstOrDefault().Value;

                    RapidVet.Repository.RapidVetDataContext DataContext = new RapidVetDataContext();
                    var LabTestType = DataContext.LabTestTypes.Where(l => l.Name == TemplateName).ToList();

                    bool isFound = false;
                    //if (LabTestType.Count > 0)
                        //isFound = true;

                    sb.Append("*" + isFound.ToString() + "*" + TemplateName);

                    var ClientIdObj = DataContext.Patients.Where(l => l.Id == id).FirstOrDefault();
                    if (ClientIdObj != null)
                        ClientId = ClientIdObj.ClientId.ToString();
                }
            }
            catch (Exception ex)
            {
                SetErrorMessage(ex.Message);
            }
            Response.Redirect("../Index/" + id.ToString() + "?Data=" + Server.UrlEncode(sb.ToString()).Replace("+", "%2b") + "&cc=" + ClientId);
        }
        public void UploadXML(int id, string cc = "0", string PatientID = "0", string AnimalKindID = "0", string LabTestTemplateID = "0")//, int labTestTypeId)
        {
            StringBuilder sb = new StringBuilder();
            RapidVet.Repository.RapidVetDataContext DataContext = new RapidVetDataContext();
            try
            {
                var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
                AnimalKindID = patient.AnimalRace.AnimalKindId.ToString();
                PatientID = patient.Id.ToString();

                //if (Request.Files.Count > 0)
                {
                    if (Session["IDEXX_XML"] != null)
                    {
                        string xml = Session["IDEXX_XML"].ToString();
                        XDocument xmlDoc = XDocument.Parse(xml);
                        string TypeName = xmlDoc.Descendants("result").Attributes("instrument").FirstOrDefault().Value;
                        //JsonResult json = GetPatientTemplates(id);
                        //string strJson = new JavaScriptSerializer().Serialize(json.Data);
                        //var DeserializeJson = JsonConvert.DeserializeObject(strJson);
                        //var results = (JObject)JsonConvert.DeserializeObject<dynamic>(strJson);

                        int LabTestTemplateId = Convert.ToInt32(LabTestTemplateID);
                        if (AnimalKindID != null && AnimalKindID != "0")//if new lab type
                        {
                            var o = DataContext.LabTestTypes.Where(l => l.ClinicId == CurrentUser.ActiveClinicId && l.Name == TypeName).FirstOrDefault();
                            if (o == null)
                            {
                                var newLabTestType = new LabTestType()
                                {
                                    Name = TypeName,
                                    ClinicId = CurrentUser.ActiveClinicId
                                };

                                if (newLabTestType != null)
                                {
                                    DataContext.LabTestTypes.Add(newLabTestType);
                                    DataContext.SaveChanges();
                                }
                            }
                        }
                        
                        try 
                        {
                            string patientName = xmlDoc.Descendants("patient_name").FirstOrDefault().Value;

                            string dt ="01/01/2017";

                            if (xmlDoc.Descendants("patient_birth_dt").FirstOrDefault() != null)
                            {
                                string[] arrDt = xmlDoc.Descendants("patient_birth_dt").FirstOrDefault().Value.Split('/');
                                dt = arrDt[2] + "-" + arrDt[1] + "-" + arrDt[0];
                            }
                            //l.Name == patientName && SqlFunctions.DateDiff("day", l.BirthDate, dt) == 0
                            int p = Convert.ToInt32(PatientID);
                            var Patient = DataContext.Patients.Where(l => l.Id == p).FirstOrDefault();
                            DateTime now = DateTime.Now;

                            var LabTestsTemplate = DataContext.LabTestsTemplates.Where(l => l.Id == LabTestTemplateId).FirstOrDefault();
                            if (LabTestsTemplate != null)
                            {
                                //LabTestTemplateId = LabTestsTemplate.Id;

                                var AssayResults = xmlDoc.Descendants("assay_result").ToList();

                                if (Patient == null)//new Patient
                                {
                                    //Response.Redirect("../Index/" + id.ToString() + "?Error=" + Server.UrlEncode("Patient Not Found").Replace("+", "%2b"));
                                    throw new Exception("Patient Not Found");
                                    return;// "Patient Not Found";
                                }

                                var newPatientLabTest = new PatientLabTest()
                                {
                                    PatientId = Patient.Id,
                                    LabTestTemplateId = LabTestTemplateId,
                                    Created = now,
                                };

                                DataContext.PatientLabTests.Add(newPatientLabTest);
                                DataContext.SaveChanges();

                                foreach (var assayResult in AssayResults)
                                {
                                    string assayName = assayResult.Attributes("assay_name").FirstOrDefault().Value;
                                    

                                    var LabTest = DataContext.LabTests.Where(l => l.Name == assayName && l.LabTestTemplateId == LabTestTemplateId).ToList();
                                    string MinValue = assayResult.Descendants("assay_reference_range").Descendants("low").FirstOrDefault().Value;
                                    string MaxValue = assayResult.Descendants("assay_reference_range").Descendants("high").FirstOrDefault().Value;
                                    string rassayName = assayName;
                                    assayName = assayName.Replace(" / ", "-");
                                    assayName = assayName.Replace("/ ", "-");
                                    assayName = assayName.Replace(" /", "-");
                                    assayName = assayName.Replace("/", "-");

                                    if (LabTest.Count == 0)//not found
                                    {
                                       // if (MinValue.Trim().Length > 0 && MaxValue.Trim().Length > 0)
                                        {
                                            var newLabTest = new LabTest()
                                            {
                                                Name = assayName,
                                                LabTestTemplateId = LabTestTemplateId,
                                                MinValue = MinValue == "" ? 1 : Convert.ToDecimal(MinValue),
                                                MaxValue = MaxValue == "" ? 100 : Convert.ToDecimal(MaxValue),
                                            };
                                            DataContext.LabTests.Add(newLabTest);//insert
                                            DataContext.SaveChanges();
                                        }
                                    }
                                    else if (LabTest.Count == 1)//found do update
                                    {
                                        var LabTestObj = LabTest.FirstOrDefault();
                                        DataContext.LabTests.Attach(LabTestObj);//update
                                        if (MinValue != "")
                                            LabTestObj.MinValue = MinValue == "" ? 1 : Convert.ToDecimal(MinValue);
                                        if (MaxValue != "")
                                            LabTestObj.MaxValue = MaxValue == "" ? 100 : Convert.ToDecimal(MaxValue);

                                        DataContext.SaveChanges();
                                    }

                                    
                                    

                                    var PatientLabTest = DataContext.PatientLabTests.Where(l => l.PatientId == Patient.Id && l.LabTestTemplateId == LabTestTemplateId && SqlFunctions.DateDiff("day", l.Created, now) == 0).OrderByDescending(l => l.Created).FirstOrDefault();
                                    if (PatientLabTest != null)//exists
                                    {
                                        rassayName = rassayName.Replace(" / ", "-");
                                        rassayName = rassayName.Replace("/ ", "-");
                                        rassayName = rassayName.Replace(" /", "-");
                                        rassayName = rassayName.Replace("/", "-");
                                        var LabTestRes = DataContext.LabTests.Where(l => l.Name == rassayName && l.LabTestTemplateId == LabTestTemplateId).FirstOrDefault();
                                        if (LabTestRes != null)
                                        {
                                            var newPatientLabTestResults = new PatientLabTestResult()
                                            {
                                                PatientLabTestId = PatientLabTest.Id,
                                                LabTestId = LabTestRes.Id,
                                                Result = assayResult.Descendants("result_value").FirstOrDefault().Value,
                                            };

                                            DataContext.PatientLabTestResults.Add(newPatientLabTestResults);
                                            DataContext.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            SetErrorMessage(ex.Message);
                            throw ex;
                        }
                        finally
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SetErrorMessage(ex.Message);
                throw ex;
                return;// "Error 2";
            }
            finally
            {
                DataContext.Dispose();
            }
            //Response.Redirect("../Index/" + id.ToString() + "?cc=" + clientId);
            //return;// "OK";
        }
        public ActionResult EditTestDate(int id, int labId)
        {
            if (IsPatientInCurrentClinic(id))
            {
                return View();
            }
            throw new SecurityException();
        }

        public JsonResult GetPatientTemplates(int id)//, int labTestTypeId)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var animalKindId = RapidVetUnitOfWork.LabTestsRepository.GetAnimalKindIdFromPatientId(id);
                var testTypes = RapidVetUnitOfWork.LabTestsRepository.GetLabTestTypesForPatient(id, CurrentUser.ActiveClinicId);
                var testTemplates = RapidVetUnitOfWork.LabTestsRepository.GetLabTestTemplatesForKind(animalKindId);
                var model = new
                {
                    testTypes = AutoMapper.Mapper.Map<List<LabTestType>, List<LabTestTypeEdit>>(testTypes),
                    testTemplates = AutoMapper.Mapper.Map<List<LabTestTemplate>, List<LabTestTemplatesWebModel>>(testTemplates)
                };
                return Json(model, JsonRequestBehavior.AllowGet);


                //var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
                //var testTemplates = RapidVetUnitOfWork.LabTestsRepository.GetLabTestTemplatesByAnimalKindId(patient.AnimalRace.AnimalKindId).ToList();
                //var model = new
                //{
                //    testTypes = AutoMapper.Mapper.Map<List<LabTestType>, List<LabTestTypeEdit>>(testTypes),
                //    testTemplates = AutoMapper.Mapper.Map<List<LabTestTemplate>, List<LabTestTemplatesWebModel>>(testTemplates)
                //};
                //return Json(model, JsonRequestBehavior.AllowGet);
            }
            throw new SecurityException();
        }

        public JsonResult GetTestTemplateItems(int id)
        {
            var templateItems = RapidVetUnitOfWork.LabTestsRepository.GetLabTestData(id);
            var model = templateItems.Select(AutoMapper.Mapper.Map<LabTest, PatientLabTestResultModel>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdatePatientLabTestDate(int id, DateTime created, int labId)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var patientLabTest = RapidVetUnitOfWork.PatientLabTestRepository.GetPatientLabTest(labId);
                patientLabTest.Created = created;
                patientLabTest.Created = patientLabTest.Created.AddHours(DateTime.Now.Hour);
                patientLabTest.Created = patientLabTest.Created.AddMinutes(DateTime.Now.Minute);
                patientLabTest.Created = patientLabTest.Created.AddSeconds(DateTime.Now.Second);
                patientLabTest.Created = patientLabTest.Created.AddMilliseconds(DateTime.Now.Millisecond);

                var status = RapidVetUnitOfWork.PatientLabTestRepository.UpdateDate(patientLabTest);

                if (status.Success)
                {
                    SetSuccessMessage("הנתונים נשמרו בהצלחה");
                }
                else
                {
                    SetErrorMessage();
                }

                return Json(status, JsonRequestBehavior.AllowGet);
            }
            throw new SecurityException();
        }


        [HttpPost]
        public JsonResult CreatePatientLabTest(int id, DateTime created, int labTestTemplateId, string results)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var status = new OperationStatus() { Success = false };

                if (!string.IsNullOrWhiteSpace(results))
                {
                    var jArr = JArray.Parse(results);
                    var patientResults = jArr.ToObject<List<PatientLabTestResultModel>>();
                    if (patientResults.Any())
                    {
                        var patientLabTest = new PatientLabTest()
                            {
                                Created = created,//DateTime.Now,
                                PatientId = id,
                                LabTestTemplateId = labTestTemplateId,
                                Results = patientResults.Select(AutoMapper.Mapper.Map<PatientLabTestResultModel, PatientLabTestResult>).ToList()
                            };

                        patientLabTest.Created = patientLabTest.Created.AddHours(DateTime.Now.Hour);
                        patientLabTest.Created = patientLabTest.Created.AddMinutes(DateTime.Now.Minute);
                        patientLabTest.Created = patientLabTest.Created.AddSeconds(DateTime.Now.Second);
                        patientLabTest.Created = patientLabTest.Created.AddMilliseconds(DateTime.Now.Millisecond);

                        status = RapidVetUnitOfWork.PatientLabTestRepository.Create(patientLabTest);

                        if (status.Success)
                        {
                            SetSuccessMessage("הנתונים נשמרו בהצלחה");
                        }
                        else
                        {
                            SetErrorMessage();
                        }
                    }
                    else
                    {
                        SetErrorMessage();
                    }
                }
                else
                {
                    SetErrorMessage();
                }
                return Json(status);
            }
            throw new SecurityException();
        }

        public JsonResult GetIndicator(int id, decimal min, decimal max, string result)
        {
            var indicator = StringUtils.GetIndicator(result, min, max);
            return Json(indicator, JsonRequestBehavior.AllowGet);
        }

        public FileResult TestResultGraph(int patientLabTestId, int patientId)
        {
            var patientLabTestsResults = RapidVetUnitOfWork.PatientLabTestRepository.GetAllSimilarLabTestResultsForPatient(patientId, patientLabTestId);

            return Graph(patientLabTestsResults);
        }

        public FileResult Graph(List<PatientLabTestResult> model)
        {
            var name = String.Empty;
            var tmpFirst = model.FirstOrDefault();
            if (tmpFirst != null)
            {
                if (tmpFirst != null)
                {
                    name = tmpFirst.LabTest.Name;
                }
            }

            var chartImage = new Chart();

            chartImage.Width = 800;
            chartImage.Height = 600;
            chartImage.ChartAreas.Add("chart");
            chartImage.ChartAreas["chart"].AxisX.MajorGrid.LineColor = Color.LightGray;
            chartImage.ChartAreas["chart"].AxisY.MajorGrid.LineColor = Color.LightGray;
            chartImage.ChartAreas[0].AxisX.Interval = 1;

            chartImage.Titles.Add("תוצאות מעבדה\n" + model.First().LabTest.Name);
            chartImage.Series.Add("report");
            chartImage.Series["report"].ChartType = SeriesChartType.Line;

            foreach (var item in model.OrderBy(i => i.PatientLabTest.Created))
            {
                decimal parsedResult;
                if (Decimal.TryParse(item.Result, out parsedResult))
                {
                    var nameString = (item.LabTest != null) ? item.PatientLabTest.Created.ToString("dd/MM/yyyy") : "";
                    var roundedVal = decimal.Round(parsedResult, 4, MidpointRounding.AwayFromZero);

                    chartImage.Series["report"].Points.Add(new DataPoint()
                    {
                        MarkerStyle = MarkerStyle.Circle,
                        MarkerSize = 10,
                        AxisLabel = nameString,
                        YValues = new double[] { (double)roundedVal },
                        LabelToolTip = item.PatientLabTest.Created.ToString("dd/MM/yyyy")
                    });
                }
            }

            // Find point with maximum Y value 
            var maxValuePoint = new DataPoint()
                {
                    XValue = chartImage.Series[0].Points.FindMaxByValue().XValue,
                    YValues = new double[] { ((double)tmpFirst.LabTest.MaxValue) }
                };
            chartImage.ChartAreas[0].AxisY.StripLines.Add(
                new StripLine()
                {
                    BorderColor = Color.Red,
                    IntervalOffset = maxValuePoint.YValues[0],
                    Text = "Max Value"
                });

            // Find point with minimum Y value 
            var minValuePoint = new DataPoint()
            {
                XValue = chartImage.Series[0].Points.FindMinByValue().XValue,
                YValues = new double[] { ((double)tmpFirst.LabTest.MinValue) }
            };
            chartImage.ChartAreas[0].AxisY.StripLines.Add(
                new StripLine()
                {
                    BorderColor = Color.Red,
                    IntervalOffset = minValuePoint.YValues[0],
                    Text = "Min Value",
                    TextLineAlignment = StringAlignment.Far
                });

            using (var strm = new MemoryStream())
            {

                chartImage.SaveImage(strm, ChartImageFormat.Png);
                strm.Seek(0, SeekOrigin.Begin);
                return File(strm.ToArray(), "image/png", "labTestResults.png");
            }
        }

        public bool UpdateResult(int Id, string Result)
        {
            var r = RapidVetUnitOfWork.LabTestsRepository.UpdateResult(Id, Result);
            return r.Success;
        }
    }
}
