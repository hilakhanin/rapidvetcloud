﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model.Clients;
using RapidVet.Model.Patients;
using RapidVet.WebModels.Clients;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class CommentsController : BaseController
    {
        public ActionResult Index(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = GetClientDetailsModelFromPatient(id);
                return View(model);
            }
            throw new SecurityException();
        }

        public JsonResult Get(int id)
        {
            var model = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
            if (model.Client.ClinicId == CurrentUser.ActiveClinicId)
            {
                return new JsonResult()
                {
                    Data = model.Comments.Where(c => c.Active).Select(c => new
                        {
                            Id = c.Id,
                            CreatedDate = c.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy"),
                            CreatedBy = c.CreatedBy.Name,
                            c.CommentBody,
                            ValidThrough = c.ValidThrough != null ? c.ValidThrough.GetValueOrDefault().ToString("dd/MM/yyyy") : "",
                            c.Popup
                        }),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            throw new SecurityException();
        }


        public JsonResult Update(int id, string CommentBody, DateTime? ValidThrough, bool Popup)
        {
            var comment = RapidVetUnitOfWork.PatientRepository.GetComment(id);

            if (IsPatientInCurrentClinic(comment.PatientId))
            {

                comment.CommentBody = CommentBody;
                comment.ValidThrough = ValidThrough;
                comment.Popup = Popup;
                comment.UpdatedById = CurrentUser.Id;
                comment.UpdatedDate = DateTime.Now;

                var status = RapidVetUnitOfWork.Save();

                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }

                return new JsonResult()
                {
                    Data = status.Success
                };
            }
            throw new SecurityException();
        }

        public JsonResult Create(int PatientId, string CommentBody, DateTime? ValidThrough, bool Popup)
        {
            if (IsPatientInCurrentClinic(PatientId))
            {
                var comment = new PatientComment
                    {
                        Active = true,
                        CommentBody = CommentBody,
                        CreatedById = CurrentUser.Id,
                        CreatedDate = DateTime.Now,
                        Popup = Popup,
                        PatientId = PatientId,
                        ValidThrough = ValidThrough,
                    };

                RapidVetUnitOfWork.PatientRepository.AddComment(comment);

                var status = RapidVetUnitOfWork.Save();

                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }

                return new JsonResult()
                    {
                        Data = new { status.Success, comment.Id, CreatedBy = CurrentUser.Name }
                    };
            }
            throw new SecurityException();
        }

        /// <summary>
        /// Id is the PatientComment to delete 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            var comment = RapidVetUnitOfWork.PatientRepository.GetComment(id);

            if (IsPatientInCurrentClinic(comment.PatientId))
            {
                var status = RapidVetUnitOfWork.PatientRepository.DeleteComment(comment);

                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }

                return new JsonResult()
                    {
                        Data = status.Success
                    };
            }
            throw new SecurityException();
        }

    }
}
