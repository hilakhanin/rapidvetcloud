﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Newtonsoft.Json.Linq;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.Patients;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Visits;
using RapidVet.RapidConsts;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.PreventiveMedicine;
using RapidVet.WebModels.Patients;
using RapidVet.WebModels.MinistryOfAgricultureReport;
using RapidVet.Model.InternalSettlements;
using System.Text;
using System.Web.Routing;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class PreventiveController : BaseController
    {
        //id is patientId
        public ActionResult Index(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = GetClientDetailsModelFromPatient(id);
                ViewBag.IsDog = RapidVetUnitOfWork.PatientRepository.IsPatientDog(id);
                return View(model);
            }
            throw new SecurityException();
        }

        public JsonResult GetPatientData(int id)
        {
            var model = GetVaccinesAndTreatmentsData(id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //id is patient id
        public ActionResult Reminders(int id, int? PriceListItemId)
        {
            var model = GetVaccinesAndTreatmentHistoryData(id, PriceListItemId, true);

            ViewBag.PatientId = id;
            return View(model);
        }

        public ActionResult Print(int id, string selected)
        {

            if (IsPatientInCurrentClinic(id))
            {
                if (!string.IsNullOrWhiteSpace(selected))
                {
                    var selectedIds = StringUtils.GetIntList(selected);
                    var model = GetVaccinesAndTreatmentsData(id);

                    var treatments = model.Treatments.Where(t => selectedIds.Contains(t.PriceListItemId)).ToList();
                    var vaccines = model.Vaccines.Where(v => selectedIds.Contains(v.PriceListItemId)).ToList();

                    model = new PreventiveMedicineIndexModel()
                        {
                            Treatments = treatments,
                            Vaccines = vaccines
                        };
                    var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
                    ViewBag.ClientName = string.Format("{0} {1}", patient.Client.FirstName, patient.Client.LastName);
                    ViewBag.PatientName = patient.Name;

                    return View(model);
                }
                return null;
            }
            throw new SecurityException();
        }





        public JsonResult GetPrePreformanceData(int id, int priceListItemId)
        {
            var isDog = RapidVetUnitOfWork.PatientRepository.IsPatientDog(id);
            var isRabiesVaccine = RapidVetUnitOfWork.PreventiveMedicineRepository.IsRabiesVaccine(priceListItemId);
            var result = new JsonResult()
                {
                    Data = new
                        {
                            IsDog = isDog,
                            IsRabiesVaccine = isRabiesVaccine,
                            DoctorId = CurrentUser.DefaultDrId != null ? CurrentUser.DefaultDrId.Value : 0
                        },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

            return result;
        }

        [HttpPost]
        public JsonResult PreformMultiplePreventiveMedicineItems(PreventiveMedicinePreformModel model, string selectedPriceListItemIds)
        {
            var result = new JsonResult() { Data = new { success = false } };

            if (!string.IsNullOrWhiteSpace(selectedPriceListItemIds))
            {
                var priceListItemIds = StringUtils.GetIntList(selectedPriceListItemIds);
                var tariffId = RapidVetUnitOfWork.TariffRepository.GetTariffIdByPatientId(model.PatientId);
                var performedOn = model.PerformedOn;
                for (var i = 0; i < priceListItemIds.Count; i++)
                {
                    var priceListItem = RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(priceListItemIds[i]);
                    model.PriceListItemId = priceListItem.Id;
                    var tariff = priceListItem.ItemsTariffs.SingleOrDefault(it => it.TariffId == tariffId);
                    model.Price = tariff != null ? tariff.Price : 0;
                    model.PerformedOn = performedOn;
                    PreformPreventiveMedicineItem(model);
                }
            }

            return result;
        }

        [HttpPost]
        public JsonResult PreformPreventiveMedicineItem(PreventiveMedicinePreformModel model, bool internallyDirected = false)
        {
            if (IsPatientInCurrentClinic(model.PatientId))
            {
                var result = new JsonResult() { Data = new { success = false } };

                var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(model.PatientId);
                //var template = RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetItemByPriceListItemId(patient.AnimalRace.AnimalKindId, model.PriceListItemId);

                var preventiveMedicineItem =
                    Mapper.Map<PreventiveMedicinePreformModel, PreventiveMedicineItem>(model);
                preventiveMedicineItem.PreformingUserId = model.ExternallyPreformed ? CurrentUser.Id : model.PreformingDoctor;
                preventiveMedicineItem.PriceListItemTypeId = RapidVetUnitOfWork.PriceListRepository.GetItemTypeId(model.PriceListItemId);
                preventiveMedicineItem.Price = model.ExternallyPreformed ? 0 : model.Price;
                preventiveMedicineItem.Quantity = 1;
                preventiveMedicineItem.Discount = 0;
                model.PerformedOn = model.PerformedOn.AddHours(DateTime.Now.Hour);
                model.PerformedOn = model.PerformedOn.AddMinutes(DateTime.Now.Minute);
                model.PerformedOn = model.PerformedOn.AddSeconds(DateTime.Now.Second);
                preventiveMedicineItem.Preformed = model.PerformedOn;
                preventiveMedicineItem.Scheduled = null;

                patient.LicenseNumber = string.IsNullOrWhiteSpace(model.LicenseNumber) ? patient.LicenseNumber : model.LicenseNumber;
                patient.ElectronicNumber = string.IsNullOrWhiteSpace(model.ElectronicNumber) ? patient.LicenseNumber : model.ElectronicNumber;

                if (model.ExternallyPreformed && string.IsNullOrWhiteSpace(model.ExternalDrName))
                {
                    preventiveMedicineItem.ExternalDrName = Resources.Client.NotMentioned;
                }

                if (model.AddToVisit)
                {
                    var visit = RapidVetUnitOfWork.VisitRepository.GetLastVisitIfOpen(model.PatientId);
                    var visitExists = visit.Id > 0;
                    if (!visitExists)
                    {
                        visit = new Visit()
                            {
                                PatientId = model.PatientId,
                                CreatedDate = DateTime.Now,
                                UpdatedById = UserId,
                                VisitDate = DateTime.Now,
                                Close = true,
                                Price = preventiveMedicineItem.Price,
                                DoctorId =
                                    model.PreformingDoctor.HasValue && model.PreformingDoctor.Value > 0
                                        ? model.PreformingDoctor.Value
                                        : CurrentUser.Id,
                                PreventiveMedicineItems = new List<PreventiveMedicineItem>() { preventiveMedicineItem },
                                ClientIdAtTimeOfVisit = patient.ClientId,
                                Active = true
                            };
                    }
                    else
                    {
                        visit.Price += preventiveMedicineItem.Price;
                        visit.PreventiveMedicineItems.Add(preventiveMedicineItem);
                    }


                    var status = visitExists
                                  ? RapidVetUnitOfWork.Save()
                                  : RapidVetUnitOfWork.VisitRepository.AddNewVisit(visit, model.UpdateScheduledPreventiveItems);

                    if (status.Success)
                    {
                        if (visitExists && model.UpdateScheduledPreventiveItems)
                        {
                            RapidVetUnitOfWork.PreventiveMedicineRepository.ItemPreformed(preventiveMedicineItem.Id, CurrentUser.ActiveClinicId);
                        }

                        if (!internallyDirected)
                        {
                            string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                   Resources.Auditing.Module, Resources.Auditing.VaccinationRegistry,
                                                   Resources.Auditing.Action, Resources.Auditing.PerformPreventiveTreatment,
                                                   Resources.Auditing.ClientName, RapidVetUnitOfWork.ClientRepository.GetClient(patient.ClientId).Name,
                                                   Resources.Auditing.PatientName, patient.Name,
                                                   Resources.Auditing.ItemName, RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(preventiveMedicineItem.PriceListItemId).Name);
                            RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.VaccineAndTreatments, logMessage, "");
                        }
                        RapidVetUnitOfWork.ClientRepository.UpdateClientBalance(patient.Client);
                        RapidVetUnitOfWork.Save();

                        var isRabiesVaccine = RapidVetUnitOfWork.PreventiveMedicineRepository.IsRabiesVaccine(model.PriceListItemId);
                        bool registerRabiesToAgricultureOffice = true;
                        if (isRabiesVaccine && !internallyDirected && !model.ExternallyPreformed)
                        {
                            preventiveMedicineItem.VisitId = visit.Id;
                            registerRabiesToAgricultureOffice = CreateRabiesVaccineReport(preventiveMedicineItem);
                        }


                        var url = Url.Action("Index", "Preventive", new { id = model.PatientId });
                        if (model.PrintDogOwnerLicense)
                        {
                            url = Url.Action("DogOwnerLicense", "Letters", new { id = model.PatientId });
                        }
                        else if (model.PrintRabiesVaccineSlip)
                        {
                            url = Url.Action("RabiesVaccineCertificate", "Letters", new { id = model.PatientId });
                        }

                        if (registerRabiesToAgricultureOffice)
                            SetSuccessMessage();
                        else
                            SetErrorMessage();

                        result.Data = new { success = status.Success, url = url };
                    }
                    else
                    {
                        SetErrorMessage();
                    }
                }
                else
                {
                    result = new JsonResult() { Data = new { success = true } };
                }

                return result;
            }
            throw new SecurityException();
        }


        public JsonResult Delete(int id, int preventiveId)
        {
            var preventive = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(preventiveId);
            var model = RenderPartialViewToString("_Delete", preventive);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteReminderConfirmMessage(int id, int preventiveId)
        {
            var preventive = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(preventiveId, true);
            var model = RenderPartialViewToString("_DeleteReminder", preventive);
            return PartialView("_DeleteReminder", preventive);
        }

        [HttpPost]
        public JsonResult EditReminderComment(int id, int preventiveMedicineItemId, string comment)
        {
            var preventiveMedicineItem = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(preventiveMedicineItemId, true);
            preventiveMedicineItem.ReminderComments = comment;
            var status = RapidVetUnitOfWork.PreventiveMedicineRepository.EditItem(preventiveMedicineItem);
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return Json(status, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult DeleteReminder(int preventiveId)
        {
            var preventive = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(preventiveId, true);
            var patientId = preventive.PatientId;
            var logMessage = string.Format("{0}: {1}<br/>{2}: {3}<br/>{4}: {5}<br/>{6}: {7}", Resources.Auditing.ClientName,
                             preventive.Patient.Client.Name, Resources.PreventiveReminders.AnimalName, preventive.Patient.Name, Resources.Auditing.ReminderType,
                             preventive.PriceListItem.Name, Resources.PreventiveReminders.ScheduledDate, preventive.Scheduled.Value.ToShortDateString());

            if (preventive.Preformed.HasValue)
            {
                preventive.Scheduled = null;
            }
            else
            {
                RapidVetUnitOfWork.PreventiveMedicineRepository.Delete(preventive);
            }

            var status = RapidVetUnitOfWork.Save();

            if (status.Success)
            {
                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, "מחיקת תזכור", logMessage, "");
                SetSuccessMessage("הפעולה בוצעה בהצלחה");
            }
            else
            {
                SetErrorMessage("הפעולה נכשלה");
            }
            return RedirectToAction("Reminders", new { id = patientId });
        }


        private bool CreateRabiesVaccineReport(PreventiveMedicineItem preventiveMedicineItem)
        {
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(preventiveMedicineItem.PatientId);
            var clinicDoctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId);
            var doctor = clinicDoctors.SingleOrDefault(d => d.Id == preventiveMedicineItem.PreformingUserId);
            var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(patient.Client.Clinic.RegionalCouncilId);

            RabiesVaccine data = new RabiesVaccine()
            {
                Batch = preventiveMedicineItem.BatchNumber,
                Date = preventiveMedicineItem.Preformed.Value.ToString("dd'/'MM'/'yyyy"),
                VaccinatingVetLicense = doctor == null ? String.Empty : doctor.LicenceNumber

            };

            if (data.Date == null)
            {
                data.Date = string.Empty;
            }

            var reportString = MinistryOfAgricultureReportHelpers.RabiesVaccineReport(data, patient, patient.Client,
                                                                              patient.Client.Clinic, regionalCouncil.Name);
            var report = new MinistryOfAgricultureReport()
            {
                Comments = preventiveMedicineItem.Comments,
                DateTime = DateTime.Now,
                PatientId = preventiveMedicineItem.PatientId,
                ReportType = MinistryOfAgricultureReportType.RabiesVaccination,
                Report = reportString,
                VisitId = preventiveMedicineItem.VisitId.Value
            };

            var status = RapidVetUnitOfWork.PatientLetterRepository.CreateMinistryOfAgricultureReport(report);
            if (status.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [HttpPost]
        public ActionResult DeleteHistory(int id, bool DeleteFromTreatment)
        {
            var preventive = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(id, false);
            int patID = preventive.PatientId;
            bool result = false;
            if(DeleteFromTreatment && preventive.VisitId.HasValue)
                deletePreventiveVisit(preventive);

            int clientId = preventive.Patient.ClientId;
            if (IsPatientInCurrentClinic(preventive.PatientId))
            {
                var patientId = preventive.PatientId;
                var status = RapidVetUnitOfWork.PreventiveMedicineRepository.Delete(preventive);
                result = status.Success;
                if (result)
                    RapidVetUnitOfWork.ClientRepository.UpdateClientBalance(RapidVetUnitOfWork.ClientRepository.GetClient(clientId));
            }

            if (result)
                SetSuccessMessage("הפעולה בוצעה בהצלחה");
            else
                SetErrorMessage("הפעולה נכשלה");

            return RedirectToAction("History", new RouteValueDictionary(new { Id = patID }));
        }

        //id is preventiveId
        [HttpPost]
        public JsonResult Delete(int id, string name)
        {
            var preventive = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(id, true);
            if (preventive.VisitId.HasValue)
                deletePreventiveVisit(preventive);
            else if (preventive.Price > 0)
            {
                RapidVetUnitOfWork.InternalSettlementsRepository.AddSettlement(new InternalSettlement()
                {
                    ClientId = preventive.Patient.ClientId,
                    Date = DateTime.Now,
                    Description = string.Format(Resources.Visit.RefundOn, preventive.PriceListItem.Name),
                    Sum = preventive.Price
                });

                RapidVetUnitOfWork.Save();
            }
            int clientId = preventive.Patient.ClientId;
            if (IsPatientInCurrentClinic(preventive.PatientId))
            {
                var patientId = preventive.PatientId;
                var status = RapidVetUnitOfWork.PreventiveMedicineRepository.Delete(preventive);
                if (status.Success)
                {
                    RapidVetUnitOfWork.ClientRepository.UpdateClientBalance(RapidVetUnitOfWork.ClientRepository.GetClient(clientId));
                    SetSuccessMessage("הפעולה בוצעה בהצלחה");
                }
                else
                    SetErrorMessage("הפעולה נכשלה");

                return Json(status);
            }
            throw new SecurityException();
        }

        private void deletePreventiveVisit(PreventiveMedicineItem preventive)
        {
            var visit = RapidVetUnitOfWork.VisitRepository.GetVisit(preventive.VisitId.Value);
            var items = visit.PreventiveMedicineItems.Where(p => p.PriceListItemId == preventive.PriceListItemId && p.PriceListItemTypeId == preventive.PriceListItemTypeId);
            if (items.Count() > 0)
            {
                string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                              Resources.Auditing.Module, Resources.Auditing.VaccinationRegistry,
                                              Resources.Auditing.Action, Resources.Auditing.DeletePreventiveTreatment,
                                              Resources.Auditing.ClientName, preventive.Patient.Client.Name,
                                              Resources.Auditing.PatientName, preventive.Patient.Name,
                                              Resources.Auditing.ItemName, preventive.PriceListItem.Name);
                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.VaccineAndTreatments, logMessage, "");

                var itemToRemove = items.Take(1).Single();
                visit.Price = visit.Price - itemToRemove.Price - itemToRemove.Discount;
                visit.PreventiveMedicineItems.Remove(itemToRemove);
                var isRabiesVaccine = RapidVetUnitOfWork.PreventiveMedicineRepository.IsRabiesVaccine(preventive.PriceListItemId);
                if (isRabiesVaccine)
                    RapidVetUnitOfWork.MinistryOfAgricultureReportsRepository.DeleteRabiesReportByVisitId(visit.Id);
            }
        }

        [HttpPost]
        public JsonResult UpdatePreventiveTreatment(PreventiveMedicineUpdateWebModel model)
        {
            OperationStatus status = new OperationStatus() { Success = true };

            var client = RapidVetUnitOfWork.ClientRepository.GetClientFromPatientId(model.PatientId);
            if (model.Id != 0) // item exists
            {
                var preventive = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(model.Id, true);
                var patientId = preventive.PatientId;

                StringBuilder logMessage = new StringBuilder();
                string basicInfo = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}<br />",
                                                Resources.Auditing.Module, Resources.Auditing.VaccinationRegistry,
                                                Resources.Auditing.Action, Resources.Auditing.ChangeVaccine,
                                                Resources.Auditing.ClientName, RapidVetUnitOfWork.ClientRepository.GetClientFromPatientId(patientId).Name,
                                                Resources.Auditing.PatientName, RapidVetUnitOfWork.PatientRepository.GetName(patientId),
                                                Resources.Auditing.ItemName, RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(preventive.PriceListItemId).Name);

                logMessage.Append(basicInfo);
                string oldInfo = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}",
                    Resources.Preventive.VaccineDate, preventive.Preformed.HasValue ? preventive.Preformed.Value.ToShortDateString() : "",
                    Resources.Auditing.PerformingDoctor, preventive.PreformingUserId.HasValue && preventive.PreformingUserId.Value > 0 ? RapidVetUnitOfWork.UserRepository.GetItem(preventive.PreformingUserId.Value).Name : "",
                    Resources.PreventiveReminders.ScheduledDate, preventive.Scheduled.HasValue ? preventive.Scheduled.Value.ToShortDateString() : "");

                if (model.UpdateDate)
                {
                    model.Date = model.Date.Value.AddHours(DateTime.Now.Hour);
                    model.Date = model.Date.Value.AddMinutes(DateTime.Now.Minute);
                    model.Date = model.Date.Value.AddSeconds(DateTime.Now.Second);
                    //  preventive.Preformed = model.Date;
                    logMessage.Append(string.Format("{0}: {1}<br />", Resources.Preventive.VaccineDate, preventive.Preformed == null ? "לא בוצע" : preventive.Preformed.Value.ToShortDateString()));
                    if (preventive.PreformingUserId != model.DoctorId && model.DoctorId.HasValue && model.DoctorId.Value > 0)
                    {
                        logMessage.Append(string.Format("{0}: {1}<br />", Resources.Auditing.PerformingDoctor, RapidVetUnitOfWork.UserRepository.GetItem(model.DoctorId.Value).Name));
                    }
                    //  preventive.PreformingUserId = model.DoctorId;

                    var newPreventiveItem = new PreventiveMedicineItem()
                    {
                        BatchNumber = model.BatchNumber,
                        Discount = 0,
                        DocumentId = 0,
                        Preformed = model.Date,
                        PatientId = model.PatientId,
                        PriceListItemId = preventive.PriceListItemId,
                        PriceListItemTypeId = preventive.PriceListItemTypeId,
                        Price = model.BillAccordingToTariff ? model.Price : 0,
                        ExternalyPreformed = model.ExternallyPerformed,
                        ExternalDrName = model.ExternalDoctorName,
                        ExternalDrLicenseNumber = model.ExternalDoctorLicense,
                        PreformingUserId = model.ExternallyPerformed ? CurrentUser.Id : model.DoctorId
                    };

                    // status = RapidVetUnitOfWork.Save();

                    //if (model.BillAccordingToTariff)
                    //{
                    var visit = RapidVetUnitOfWork.VisitRepository.GetLastVisitIfOpen(preventive.PatientId);
                    var visitExists = visit.Id > 0;
                    if (!visitExists)
                    {
                        visit = new Visit()
                        {
                            PatientId = preventive.PatientId,
                            CreatedDate = DateTime.Now,
                            UpdatedById = UserId,
                            VisitDate = model.Date.Value,//DateTime.Now,
                            Close = true,
                            Price = model.BillAccordingToTariff ? model.Price : 0,
                            DoctorId = model.ExternallyPerformed ? CurrentUser.Id : model.DoctorId.Value,
                            PreventiveMedicineItems = new List<PreventiveMedicineItem>() { newPreventiveItem },
                            ClientIdAtTimeOfVisit = preventive.Patient.ClientId,
                            Active = true
                        };
                    }
                    else
                    {
                        visit.Price += model.Price;
                        visit.PreventiveMedicineItems.Add(preventive);
                    }

                    status = visitExists
                                  ? RapidVetUnitOfWork.Save()
                                  : RapidVetUnitOfWork.VisitRepository.AddNewVisit(visit, false);

                    status = RapidVetUnitOfWork.PreventiveMedicineRepository.ChangeParentPreventiveItem(preventive.Id, newPreventiveItem.Id);
                    if (model.UpdateScheduledPreventiveItems)
                    {
                        var ret = RapidVetUnitOfWork.PreventiveMedicineRepository.ItemPreformedUpdate(newPreventiveItem.Id, CurrentUser.ActiveClinicId);
                    }
                    // }                 
                }

                if (model.UpdateNextDate)
                {
                    preventive.Scheduled = model.NextDate.Value;
                    logMessage.Append(string.Format("{0}: {1}<br />", Resources.PreventiveReminders.ScheduledDate, preventive.Scheduled.Value.ToShortDateString()));
                    status = RapidVetUnitOfWork.Save();
                }

                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.VaccineAndTreatments, logMessage.ToString(), oldInfo);

            }
            else //item doesnt exist
            {
                var priceListItem = RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(model.PriceListItemId);
                var patientId = model.PatientId;

                StringBuilder logMessage = new StringBuilder();
                string basicInfo = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}<br />",
                                                Resources.Auditing.Module, Resources.Auditing.VaccinationRegistry,
                                                Resources.Auditing.Action, Resources.Auditing.ChangeVaccine,
                                                Resources.Auditing.ClientName, RapidVetUnitOfWork.ClientRepository.GetClientFromPatientId(patientId).Name,
                                                Resources.Auditing.PatientName, RapidVetUnitOfWork.PatientRepository.GetName(patientId),
                                                Resources.Auditing.ItemName, priceListItem.Name);

                logMessage.Append(basicInfo);

                if (model.UpdateDate)
                {
                    model.Date = model.Date.Value.AddHours(DateTime.Now.Hour);
                    model.Date = model.Date.Value.AddMinutes(DateTime.Now.Minute);
                    model.Date = model.Date.Value.AddSeconds(DateTime.Now.Second);

                    logMessage.Append(string.Format("{0}: {1}<br />", Resources.Preventive.VaccineDate, model.Date.Value.ToShortDateString()));
                    if (model.DoctorId.HasValue && model.DoctorId > 0)
                    {
                        logMessage.Append(string.Format("{0}: {1}<br />", Resources.Auditing.PerformingDoctor, RapidVetUnitOfWork.UserRepository.GetItem(model.DoctorId.Value).Name));
                    }
                    if (model.UpdateScheduledPreventiveItems && !model.BillAccordingToTariff)
                    {
                        var newPreventiveItem = new PreventiveMedicineItem()
                        {
                            Discount = 0,
                            DocumentId = 0,
                            Preformed = model.Date,
                            PatientId = model.PatientId,
                            PriceListItemId = priceListItem.Id,
                            PriceListItemTypeId = priceListItem.ItemTypeId,
                            Price = model.BillAccordingToTariff ? model.Price : 0,
                            ExternalyPreformed = model.ExternallyPerformed,
                            ExternalDrName = model.ExternalDoctorName,
                            ExternalDrLicenseNumber = model.ExternalDoctorLicense,
                            PreformingUserId = model.ExternallyPerformed ? CurrentUser.Id : model.DoctorId
                        };

                        if (model.ExternallyPerformed && string.IsNullOrWhiteSpace(model.ExternalDoctorName))
                        {
                            newPreventiveItem.ExternalDrName = Resources.Client.NotMentioned;
                        }
                        status = RapidVetUnitOfWork.PreventiveMedicineRepository.Create(newPreventiveItem);
                        RapidVetUnitOfWork.PreventiveMedicineRepository.ItemPreformedUpdate(newPreventiveItem.Id, CurrentUser.ActiveClinicId);

                        var visit = RapidVetUnitOfWork.VisitRepository.GetLastVisitIfOpen(patientId);
                        var visitExists = visit.Id > 0;
                        if (!visitExists)
                        {
                            visit = new Visit()
                            {
                                PatientId = patientId,
                                CreatedDate = DateTime.Now,
                                UpdatedById = UserId,
                                VisitDate = model.Date.Value,
                                Close = true,
                                Price = model.BillAccordingToTariff ? model.Price : 0,
                                DoctorId = model.ExternallyPerformed ? CurrentUser.Id : model.DoctorId.Value,
                                PreventiveMedicineItems = new List<PreventiveMedicineItem>() { newPreventiveItem },
                                ClientIdAtTimeOfVisit = client.Id,
                                Active = true
                            };
                        }
                        else
                        {
                            visit.Price += model.Price;
                            visit.PreventiveMedicineItems.Add(newPreventiveItem);
                        }

                        status = visitExists
                                      ? RapidVetUnitOfWork.Save()
                                      : RapidVetUnitOfWork.VisitRepository.AddNewVisit(visit, false);

                    }
                    else if (model.UpdateScheduledPreventiveItems)
                    {
                        var preventiveMedicinePerformModel = new PreventiveMedicinePreformModel()
                        {
                            PatientId = model.PatientId,
                            PriceListItemId = priceListItem.Id,
                            PriceListItemTypeId = priceListItem.ItemTypeId,
                            Price = model.Price,
                            ExternallyPreformed = model.ExternallyPerformed,
                            ExternalDrLicenseNumber = model.ExternalDoctorLicense,
                            ExternalDrName = model.ExternalDoctorName,
                            PrintDogOwnerLicense = false,
                            PrintRabiesVaccineSlip = false,
                            PerformedOn = model.Date.Value,
                            PreformingDoctor = model.ExternallyPerformed ? CurrentUser.Id : model.DoctorId,
                            UpdateScheduledPreventiveItems = model.UpdateScheduledPreventiveItems,
                            AddToVisit = model.BillAccordingToTariff
                        };
                        PreformPreventiveMedicineItem(preventiveMedicinePerformModel, true);
                    }
                    else //only update date
                    {
                        var preventiveMedicineItem = new PreventiveMedicineItem()
                        {
                            DocumentId = 0,
                            PatientId = model.PatientId,
                            Price = model.BillAccordingToTariff ? model.Price : 0,
                            PriceListItemId = priceListItem.Id,
                            PriceListItemTypeId = priceListItem.ItemTypeId,
                            Quantity = 1,
                            Preformed = model.Date,
                            ExternalyPreformed = model.ExternallyPerformed,
                            ExternalDrName = model.ExternalDoctorName,
                            ExternalDrLicenseNumber = model.ExternalDoctorLicense,
                            PreformingUserId = model.ExternallyPerformed ? CurrentUser.Id : model.DoctorId
                        };

                        if (model.ExternallyPerformed && string.IsNullOrWhiteSpace(model.ExternalDoctorName))
                        {
                            preventiveMedicineItem.ExternalDrName = Resources.Client.NotMentioned;
                        }

                        status = RapidVetUnitOfWork.PreventiveMedicineRepository.Create(preventiveMedicineItem);

                        //  if (model.BillAccordingToTariff)
                        //  {
                        var visit = RapidVetUnitOfWork.VisitRepository.GetLastVisitIfOpen(preventiveMedicineItem.PatientId);
                        var visitExists = visit.Id > 0;
                        if (!visitExists)
                        {
                            visit = new Visit()
                            {
                                PatientId = preventiveMedicineItem.PatientId,
                                CreatedDate = DateTime.Now,
                                UpdatedById = UserId,
                                VisitDate = model.Date.Value,
                                Close = true,
                                Price = model.BillAccordingToTariff ? model.Price : 0,
                                DoctorId = model.ExternallyPerformed ? CurrentUser.Id : model.DoctorId.Value,
                                PreventiveMedicineItems = new List<PreventiveMedicineItem>() { preventiveMedicineItem },
                                ClientIdAtTimeOfVisit = client.Id,
                                Active = true
                            };
                        }
                        else
                        {
                            visit.Price += model.Price;
                            visit.PreventiveMedicineItems.Add(preventiveMedicineItem);
                        }

                        status = visitExists
                                      ? RapidVetUnitOfWork.Save()
                                      : RapidVetUnitOfWork.VisitRepository.AddNewVisit(visit, false);
                        //  }
                    }
                }

                if (model.UpdateNextDate)
                {
                    logMessage.Append(string.Format("{0}: {1}<br />", Resources.PreventiveReminders.ScheduledDate, model.NextDate.Value.ToShortDateString()));
                    var preventiveMedicineItem = new PreventiveMedicineItem()
                    {
                        DocumentId = 0,
                        PatientId = model.PatientId,
                        Price = 0,
                        PriceListItemId = priceListItem.Id,
                        PriceListItemTypeId = priceListItem.ItemTypeId,
                        Quantity = 1,
                        Scheduled = model.NextDate,
                        ExternalyPreformed = model.ExternallyPerformed,
                        ExternalDrName = model.ExternalDoctorName,
                        ExternalDrLicenseNumber = model.ExternalDoctorLicense,
                        PreformingUserId = model.ExternallyPerformed ? CurrentUser.Id : model.DoctorId
                    };

                    if (model.ExternallyPerformed && string.IsNullOrWhiteSpace(model.ExternalDoctorName))
                    {
                        preventiveMedicineItem.ExternalDrName = Resources.Client.NotMentioned;
                    }

                    status = RapidVetUnitOfWork.PreventiveMedicineRepository.Create(preventiveMedicineItem);
                }

                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.VaccineAndTreatments, logMessage.ToString(), "");
            }

            if (status.Success)
            {
                RapidVetUnitOfWork.ClientRepository.UpdateClientBalance(client);
                SetSuccessMessage("הפעולה בוצעה בהצלחה");
            }
            else
            {
                SetErrorMessage("הפעולה נכשלה");
            }

            return Json(status);

        }

        public ActionResult History(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = GetVaccinesAndTreatmentHistoryData(id, null, false);
                return View(model);
            }
            throw new SecurityException();
        }

        public ActionResult HistoryPrint(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = GetVaccinesAndTreatmentHistoryData(id, null, false);
                return View(model);
            }
            throw new SecurityException();
        }

        public ActionResult PrintCertificate(int id)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var items = GetVaccinesAndTreatmentsData(id);
                var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
                var model = AutoMapper.Mapper.Map<Patient, CertificateWebModel>(patient);
                model.Items = items;
                var clientAddresses = RapidVetUnitOfWork.ClientRepository.GetAllClientAddresses(patient.ClientId);
                if (clientAddresses.Any())
                {
                    var address = clientAddresses.First();
                    model.ClientAddress = string.Format("{0} {1}", address.City.Name, address.Street);
                }

                return View(model);
            }
            throw new SecurityException();
        }


        private PreventiveMedicineIndexModel GetVaccinesAndTreatmentsData(int patientId)
        {
            var tariffId = RapidVetUnitOfWork.TariffRepository.GetTariffIdByPatientId(patientId);

            var priceListItems =
                RapidVetUnitOfWork.PreventiveMedicineRepository.GetPriceListItems(CurrentUser.ActiveClinicId).ToList();
            var templates = RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetVaccineAndTreatmentByPatientId(patientId);
            var now = DateTime.Now;

            var preventiveItemsDbList = RapidVetUnitOfWork.PreventiveMedicineRepository.GetCurrentState(patientId);

            var preveniveItemsList = GetList(preventiveItemsDbList, templates, priceListItems, now, tariffId, patientId);

            var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(patientId);

            var result = new PreventiveMedicineIndexModel()
            {
                Treatments = preveniveItemsList.Where(pil => pil.PriceListItemTypeId == (int)PriceListItemTypeEnum.PreventiveTreatments).ToList(),
                Vaccines = preveniveItemsList.Where(pil => pil.PriceListItemTypeId == (int)PriceListItemTypeEnum.Vaccines).ToList(),
                ElectronicNumber = patient.ElectronicNumber,
                LicenseNumber = patient.LicenseNumber
            };

            return result;

        }

        private PreventiveMedicineIndexModel GetVaccinesAndTreatmentHistoryData(int patientId, int? PriceListItemId,bool NewItems = false)
        {
            var tariffId = RapidVetUnitOfWork.TariffRepository.GetTariffIdByPatientId(patientId);

            var priceListItems =
                RapidVetUnitOfWork.PreventiveMedicineRepository.GetPriceListItems(CurrentUser.ActiveClinicId).ToList();

            var templates = RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetVaccineAndTreatmentByPatientId(patientId);

            var now = DateTime.Now;

            var preventiveItemsDbList = RapidVetUnitOfWork.PreventiveMedicineRepository.GetAllForPatient(patientId, PriceListItemId);

            var list = new List<PreventiveMedicineItemWebModel>();
            foreach (var s in preventiveItemsDbList)
            {
                ///var f = list.Find(x => x.PriceListItemId == s.PriceListItemId);
                ///if (f != null)
                //{
                    //if (s.Id > f.Id && NewItems)//s is newer preventive items with parent, else skip
                    //    list.Remove(f);
                    //else if (NewItems)
                    //    continue;

                    ////if (NewItems && s.Id > f.Id && s.Preformed < DateTime.Now)
                    ////    continue;
                    ////else if (NewItems && s.Preformed < DateTime.Now)
                    ////    continue;

                   
                ////}
                if (NewItems && ((s.Scheduled.HasValue && s.Scheduled < DateTime.Now) || !s.Scheduled.HasValue))
                    continue;
                else if (!NewItems && s.Preformed.HasValue && s.Preformed > DateTime.Now)
                    continue;

                var newItem = new PreventiveMedicineItemWebModel()
                {
                    Id = s.Id,
                    IsNextFuture = s.Scheduled.HasValue && s.Scheduled.Value > DateTime.Now,
                    IsToday = s.Preformed.HasValue && s.Preformed.Value.Date == DateTime.Now.Date,
                    Name = s.PriceListItem.Name,
                    Next = s.Scheduled.HasValue ? s.Scheduled.Value : (DateTime?)null,
                    NextStr = s.Scheduled.HasValue ? s.Scheduled.Value.ToShortDateString() : null,
                    Preformed = s.Preformed,
                    PreformedStr = s.Preformed.ToString(),
                    Price = s.Price,
                    PriceListItemId = s.PriceListItemId,
                    PriceListItemTypeId = s.PriceListItemTypeId,
                    IsRabiesVaccine = s.PriceListItem.IsRabiesVaccine,
                    Comment = s.ReminderComments
                };
                list.Add(newItem);
            }

            var result = new PreventiveMedicineIndexModel()
            {
                Treatments = list.Where(pil => pil.PriceListItemTypeId == (int)PriceListItemTypeEnum.PreventiveTreatments).ToList(),
                Vaccines = list.Where(pil => pil.PriceListItemTypeId == (int)PriceListItemTypeEnum.Vaccines).ToList()
            };

            return result;
        }

        private List<PreventiveMedicineItemWebModel> GetList(List<PreventiveMedicineItem> source, List<VaccineOrTreatment> templates,
                                                              List<PriceListItem> priceListItems,
                                                             DateTime now, int tariffId, int patientId)
        {
            foreach (var t in templates)
            {
                var preventiveItem = source.SingleOrDefault(s => s.PriceListItemId == t.PriceListItemId);
                if (preventiveItem == null)
                {
                    preventiveItem = new PreventiveMedicineItem()
                    {
                        PriceListItemId = t.PriceListItemId,
                        PriceListItemTypeId = t.PriceListItem.ItemTypeId,
                    };

                    source.Add(preventiveItem);
                }
            }

            //   var result = source.Select(AutoMapper.Mapper.Map<PreventiveMedicineItem, PreventiveMedicineItemWebModel>).ToList(); // Old try

            // New try
            var result = new List<PreventiveMedicineItemWebModel>();
            foreach (var s in source)
            {
                var newItem = new PreventiveMedicineItemWebModel()
                    {
                        Id = s.Id,
                        IsNextFuture = s.Scheduled.HasValue && s.Scheduled.Value > DateTime.Now,
                        IsToday = s.Preformed.HasValue && s.Preformed.Value.Date == DateTime.Now.Date,
                        Name = s.PriceListItem.Name,
                        Next = s.Scheduled.HasValue ? s.Scheduled.Value : (DateTime?)null,
                        NextStr = s.Scheduled.HasValue ? s.Scheduled.Value.ToString() : null,
                        Preformed = s.Preformed,
                        PreformedStr = s.Preformed.ToString(),
                        Price = s.Price,
                        PriceListItemId = s.PriceListItemId,
                        PriceListItemTypeId = s.PriceListItemTypeId,
                        IsRabiesVaccine = s.PriceListItem.IsRabiesVaccine
                    };
                result.Add(newItem);
            }


            foreach (var r in result)
            {
                var pl = priceListItems.SingleOrDefault(pli => pli.Id == r.PriceListItemId);
                if (pl != null)
                {
                    var tariff = pl.ItemsTariffs.SingleOrDefault(it => it.TariffId == tariffId);
                    r.Price = tariff != null ? tariff.Price : 0;
                }
                else
                {
                    r.Price = 0;
                }


                if (r.Next.HasValue && r.Next.Value > DateTime.MinValue)
                {
                    r.NextStr = r.Next.Value.ToShortDateString();
                    r.IsNextFuture = now < r.Next.Value;
                }

                if (r.Preformed.HasValue && r.Preformed.Value > DateTime.MinValue)
                {
                    r.PreformedStr = r.Preformed.Value.ToShortDateString();
                    r.IsToday = r.Preformed.Value.Year == now.Year &&
                                r.Preformed.Value.Month == now.Month && r.Preformed.Value.Day == now.Day;
                }
            }

            return result;
        }

        [HttpGet]
        public PartialViewResult UpdateTollData(int id)
        {
            var preventiveMedicineItem = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(id, true);
            var model = AutoMapper.Mapper.Map<PreventiveMedicineItem, PreventiveMedicineItemWebModel>(preventiveMedicineItem);
            return PartialView("_UpdateTollData", model);
        }

        [HttpPost]
        public ActionResult EditTollData(PreventiveMedicineItemWebModel model)
        {
            var preventiveMedicineItem = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(model.Id);

            preventiveMedicineItem.ClientPaidOrReturnedTollVaucher = model.ClientPaidOrReturnedTollVaucher;
            preventiveMedicineItem.TollPrice = model.TollPrice;
            preventiveMedicineItem.TollPaymentMethodId = model.TollPaymentMethodId;
            preventiveMedicineItem.TollPaymentDate = model.TollPaymentDate;

            var status = RapidVetUnitOfWork.Save();

            if (status.Success)
            {
                //    RapidVetUnitOfWork.ClientRepository.UpdateClientBalance(RapidVetUnitOfWork.ClientRepository.GetClientFromPatientId(preventiveMedicineItem.PatientId));
                SetSuccessMessage("הפעולה בוצעה בהצלחה");
            }
            else
            {
                SetErrorMessage("הפעולה נכשלה");
            }

            return RedirectToAction("History", new { id = preventiveMedicineItem.PatientId });
        }

        [HttpGet]
        public PartialViewResult DeleteItem(int id)
        {
            var model = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(id, false);
            ViewBag.ShowDeleteFromTreatmentCheckBox = true;
            return PartialView("_Delete", model);
        }
    }
}

