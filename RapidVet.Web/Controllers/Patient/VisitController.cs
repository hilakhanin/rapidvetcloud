﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json.Linq;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.PatientFollowUp;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Medications;
using RapidVet.WebModels.Visits;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class VisitController : BaseController
    {

        public ActionResult Index(int id, int visitId = 0)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var model = GetClientDetailsModelFromPatient(id);
                return View(model);
            }
            throw new SecurityException();

        }

        public JsonResult GetData(int id, int visitId)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var tariffId = RapidVetUnitOfWork.ClientRepository.GetClientTariffIdFromPatientId(id);

                var categoriesDb =
                    RapidVetUnitOfWork.CategoryRepository.GetClinicActiveCategories(CurrentUser.ActiveClinicId,true).ToList();
                var categories =
                    categoriesDb.Select(AutoMapper.Mapper.Map<PriceListCategory, PriceListCategoryModel>).ToList();

                var doctorsDb = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId);
                var doctors = doctorsDb.Select(AutoMapper.Mapper.Map<User, UserBasicModel>).ToList();

                Visit visit;
                visit = visitId > 0
                            ? RapidVetUnitOfWork.VisitRepository.GetVisit(visitId)
                            : RapidVetUnitOfWork.VisitRepository.GetLastVisitIfOpen(id);

                var model = AutoMapper.Mapper.Map<Visit, VisitModel>(visit);
                model.Doctors = doctors;
                model.Categories = categories;

                var preventiveTreatments =
                    visit.PreventiveMedicineItems.Select(AutoMapper.Mapper.Map<PreventiveMedicineItem, VisitChildItem>)
                         .ToList();
                foreach (var pvt in preventiveTreatments)
                {
                    model.VisitTreatments.Add(pvt);
                }

                if (visitId == 0)
                {
                    var exist = model.Doctors.SingleOrDefault(d => d.Id == CurrentUser.Id);
                    //i'm a different dr than the current user
                    if (exist != null)
                    {
                        model.SelectedDoctorId = CurrentUser.Id;
                    }//current user's default dr
                    else if (CurrentUser.DefaultDrId != null)
                    {
                        model.SelectedDoctorId = CurrentUser.DefaultDrId.Value;
                    }
                    else //client's default dr
                    {
                        var DefaultDr = RapidVetUnitOfWork.PatientRepository.GetPatient(id).Client.VetId;
                        model.SelectedDoctorId = DefaultDr.HasValue ? DefaultDr.Value : 0;
                    }
                }

                model.ActiveUserName = CurrentUser.Name;
                model.ActiveUserId = CurrentUser.Id;
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            throw new SecurityException();
        }

        public JsonResult GetExaminationsData(int id)
        {
            var tariffId = RapidVetUnitOfWork.ClientRepository.GetClientTariffIdFromPatientId(id);

            var examinationsDb = RapidVetUnitOfWork.PriceListRepository.GetExaminations(CurrentUser.ActiveClinicId);
            var examinations = examinationsDb.Select(AutoMapper.Mapper.Map<PriceListItem, PriceListItemSingleTariffFlatModel>).ToList();
            examinations = UpdatePrices(examinations, tariffId, examinationsDb);
            return Json(examinations, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetTreatmentsData(int id)
        {
            var tariffId = RapidVetUnitOfWork.ClientRepository.GetClientTariffIdFromPatientId(id);

            var treatmentsDb = RapidVetUnitOfWork.PriceListRepository.GetTreatments(CurrentUser.ActiveClinicId);
            var treatments = treatmentsDb.Select(AutoMapper.Mapper.Map<PriceListItem, PriceListItemSingleTariffFlatModel>).ToList();
            treatments = UpdatePrices(treatments, tariffId, treatmentsDb);
            return Json(treatments, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMedicationData()
        {
            var medicationsDb =
                       RapidVetUnitOfWork.MedicationRepository.GetClinicGroupMedications(CurrentUser.ActiveClinicGroupId);
            var medications =
                medicationsDb.Select(AutoMapper.Mapper.Map<Medication, MedicationEditWebModel>).ToList();
            return Json(medications, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDiagnosisData()
        {
            var diagnosesDb = RapidVetUnitOfWork.DiagnosisRepository.GetList(CurrentUser.ActiveClinicId);
            var diagnoses = diagnosesDb.Select(AutoMapper.Mapper.Map<Diagnosis, DiagnosisAdminModel>).ToList();
            return Json(diagnoses, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMedicationAdministrationTypes()
        {
            var model = from MedicationAdministrationType p in Enum.GetValues(typeof(MedicationAdministrationType))
                        select new SelectListItem()
                        {
                            Text = p.ToString(),
                            Value = ((int)p).ToString(),
                        };

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="examinations">json string that represents visitExaminations</param>
        /// <param name="diagnoses">json string that represents visitDiagnoses</param>
        /// <param name="treatments">json string that represents visitTreatments</param>
        /// <param name="medications">json string that represents visitMedications</param>
        /// <param name="dangerousDrugs">json string that represents visitDangerouDrugs</param>
        /// <param name="close">if true - close visit</param>
        /// <param name="print">if true - return link to print</param>
        /// <returns>json: success[true/false], 
        ///                                 url[if print -> return url to print view ; 
        ///                                 if close & not print -> return url to home/index; 
        ///                                 if not close & not print: url to edit visit ]
        /// </returns>
        public JsonResult SaveVisit(VisitModel model, string examinations, string diagnoses,
                                    string treatments, string medications, string dangerousDrugs, bool close, bool print, bool printPrescription)
        {
            var status = new OperationStatus() { Success = false };

            if (!string.IsNullOrWhiteSpace(examinations))
            {
                var jArr = JArray.Parse(examinations);
                model.VisitExaminations = jArr.ToObject<Collection<VisitChildItem>>();
            }
            if (!string.IsNullOrWhiteSpace(diagnoses))
            {
                var jArr = JArray.Parse(diagnoses);
                model.VisitDiagnoses = jArr.ToObject<Collection<VisitChildItem>>();
            }
            if (!string.IsNullOrWhiteSpace(treatments))
            {
                var jArr = JArray.Parse(treatments);
                model.VisitTreatments = jArr.ToObject<Collection<VisitChildItem>>();
            }
            if (!string.IsNullOrWhiteSpace(medications))
            {
                var jArr = JArray.Parse(medications);
                model.VisitMedications = jArr.ToObject<Collection<VisitMedicationModel>>();
            }
            if (!string.IsNullOrWhiteSpace(dangerousDrugs))
            {
                var jArr = JArray.Parse(dangerousDrugs);
                model.DangerousDrugs = jArr.ToObject<Collection<DangerousDrugWebModel>>();
            }

            var visit = AutoMapper.Mapper.Map<VisitModel, Visit>(model);
            visit.Active = true;
            visit.Close = close;

            var preventiveMedicineItems = new Collection<PreventiveMedicineItem>();

            // Seprate preventive items from treatments
            var visitTreatmentsList = visit.Treatments.ToList();
            foreach (var treatmentOrPreventive in visitTreatmentsList)
            {
                if (treatmentOrPreventive.PriceListItemId != null)
                {
                    var priceListItem =
                        RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(
                            treatmentOrPreventive.PriceListItemId.Value);

                    if (priceListItem.ItemTypeId == (int)PriceListItemTypeEnum.PreventiveTreatments ||
                        priceListItem.ItemTypeId == (int)PriceListItemTypeEnum.Vaccines)
                    {
                        var preventiveItem =
                            RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(treatmentOrPreventive.Id);
                        // New preventive Item
                        if (preventiveItem == null)
                        {
                            preventiveItem = new PreventiveMedicineItem()
                                {
                                    PatientId = visit.PatientId.Value,
                                    PriceListItemTypeId = priceListItem.ItemTypeId,
                                    PriceListItemId = priceListItem.Id,
                                    VisitId = treatmentOrPreventive.VisitId,
                                    Preformed = DateTime.Now,
                                    Price = treatmentOrPreventive.UnitPrice,
                                    Quantity = treatmentOrPreventive.Quantity,
                                    Discount = treatmentOrPreventive.Discount,
                                    DiscountPercent = treatmentOrPreventive.DiscountPercent,
                                    ExternalyPreformed = false,
                                    PreformingUserId = model.SelectedDoctorId,
                                };
                        }
                        // Update Preventive Item
                        else
                        {
                            preventiveItem.Price = treatmentOrPreventive.UnitPrice;
                            preventiveItem.Quantity = treatmentOrPreventive.Quantity;
                            preventiveItem.Discount = treatmentOrPreventive.Discount;
                            preventiveItem.DiscountPercent = treatmentOrPreventive.DiscountPercent;
                            preventiveItem.Preformed = (preventiveItem.Preformed == DateTime.MinValue ||
                                                        preventiveItem.Preformed == null)
                                                           ? DateTime.Now
                                                           : preventiveItem.Preformed;
                            preventiveItem.ExternalyPreformed = false;
                            preventiveItem.PreformingUserId = preventiveItem.PreformingUserId == null &&
                                                              !preventiveItem.ExternalyPreformed
                                                                  ? model.SelectedDoctorId
                                                                  : preventiveItem.PreformingUserId;
                        }
                        preventiveMedicineItems.Add(preventiveItem);
                        visit.Treatments.Remove(treatmentOrPreventive);
                    }
                }
            }
            visit.PreventiveMedicineItems = preventiveMedicineItems;

            if (!string.IsNullOrWhiteSpace(model.Date) && !string.IsNullOrWhiteSpace(model.Time))
            {
                var dt = DateTime.MinValue;
                DateTime.TryParse(string.Format("{0} {1}", model.Date, model.Time), out dt);

                if (dt > DateTime.MinValue)
                {
                    visit.VisitDate = dt;
                    visit.VisitDate = visit.VisitDate.AddSeconds(DateTime.Now.Second);
                    visit.VisitDate = visit.VisitDate.AddMilliseconds(DateTime.Now.Millisecond);
                }

            }

            if (visit.Id == 0)
            {
                visit.CreatedById = CurrentUser.Id;
            }
            else
            {
                visit.UpdatedById = CurrentUser.Id;
            }
            var newVisit = visit.Id == 0;

            //--- New -----------
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(visit.PatientId.Value);
            visit.ClientIdAtTimeOfVisit = patient.ClientId;
            //-------------------
            status = newVisit
                         ? RapidVetUnitOfWork.VisitRepository.AddNewVisit(visit)
                         : RapidVetUnitOfWork.VisitRepository.UpdateVisit(visit);

            if (status.Success)
            {
                RapidVetUnitOfWork.VisitRepository.UpdateVitalSignsForVisit(visit);
                var client = RapidVetUnitOfWork.VisitRepository.GetVisitClient(visit.Id);
                RapidVetUnitOfWork.ClientRepository.UpdateClientBalance(client);
                RapidVetUnitOfWork.Save();
                SetSuccessMessage();

                if (newVisit && CurrentUser.ActiveClinic.AutomaticallySetFollowUpsAfterVisit)
                {
                    var followUp = new FollowUp()
                        {
                            PatientId = visit.PatientId.Value,
                            UserId = visit.DoctorId,
                            Created = DateTime.Now,
                            DateTime =
                                RapidVetUnitOfWork.FollowUpRepository.FollowUpDefaultDate(CurrentUser.ActiveClinicId)
                        };
                    RapidVetUnitOfWork.FollowUpRepository.Create(followUp);
                }
            }
            else
            {
                SetErrorMessage();
            }
            // var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(visit.PatientId); //Old removed
            var printUri = string.Empty;
            if (print)
            {
                printUri = Url.Action("PrintVisit", "Visit", new { id = visit.PatientId, visitId = visit.Id });
            }
            else if (printPrescription)
            {
                printUri = Url.Action("PrintPrescription", "Visit", new { id = visit.Id });
            }

            var returnUrl = close
                                ? Url.Action("Index", "History", new { id = patient.Id })
                                : Url.Action("Index", new { id = visit.PatientId, visitId = visit.Id });

            var result = new JsonResult()
                {
                    Data =
                        new
                            {
                                success = status.Success,
                                url = returnUrl,
                                printUrl = printUri,
                                visitId = visit.Id
                            }
                };
            return result;
        }

        public ActionResult PrintVisit(int id, int visitId)
        {
            if (IsPatientInCurrentClinic(id))
            {
                var visit = RapidVetUnitOfWork.VisitRepository.GetVisit(visitId);
                return View(visit);
            }
            throw new SecurityException();

        }

        /// <summary>
        /// Print Prescription of client
        /// </summary>
        /// <param name="id">id is visit id</param>
        /// <returns></returns>
        public ActionResult PrintPrescription(int id, bool printValidityComment = false)
        {
            var visit = RapidVetUnitOfWork.VisitRepository.GetVisit(id);

            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(visit.PatientId.Value);

            var model = AutoMapper.Mapper.Map<Clinic, PrescriptionModel>(CurrentUser.ActiveClinic);
            model.VisitMedications =
                visit.Medications.Select(AutoMapper.Mapper.Map<VisitMedication, VisitMedicationModel>).ToList();
            model.ClientAddress = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientAddress(patient.Client.Addresses);
            model.ClientName = patient.Client.Name;
            model.ClientIdCard = patient.Client.IdCard;
            model.PatientName = patient.Name;
            model.DoctorName = visit.Doctor.Name;
            model.DoctorLicenseNumber = visit.Doctor.LicenceNumber;
            model.PrintComments = printValidityComment;

            return View(model);
        }

        public JsonResult DeactivateVisit(int? id, int visitId)
        {
            if (!id.HasValue || (id.HasValue && IsPatientInCurrentClinic(id.Value)))
            {
                var visit = RapidVetUnitOfWork.VisitRepository.GetVisit(visitId);
                var client = RapidVetUnitOfWork.ClientRepository.GetClient(visit.ClientIdAtTimeOfVisit);
                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, "תיק לקוח - ביטול ביקור", String.Format("לקוח - {0}, חיה - {1}", client.Name, visit.Patient != null ? visit.Patient.Client.Name : "כללי"), String.Empty);
                visit.Active = false;
                visit.Close = true;
                var status = RapidVetUnitOfWork.VisitRepository.UpdateVisit(visit);
                RapidVetUnitOfWork.ClientRepository.UpdateClientBalance(visit.ClientAtTimeOfVisit);
                RapidVetUnitOfWork.Save();
                return new JsonResult() { Data = status.Success, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            throw new SecurityException();
        }
    }

}
