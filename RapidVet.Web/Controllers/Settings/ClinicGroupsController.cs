using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MvcContrib.Filters;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.Model.Users;
using RapidVet.RapidConsts.Controllers;
using RapidVet.Repository.Clinics;
using System.Data.Linq;
using RapidVet.WebModels.ClinicGroups;
using System.Text.RegularExpressions;
using System.Resources;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Globalization;
using RestSharp;
using RestSharp.Authenticators;
using System.Net;


namespace RapidVet.Web.Controllers
{
    public enum TemplateKeyWordsGroups
    {
        Calendar, PreventiveMedicineReminders, Birthday, VisitsReport, ClientsReport, FollowUps
    }

    public class ClinicGroupsController : BaseController
    {

        private IFileUploader _fileUploader;

        public ClinicGroupsController(IFileUploader fileUploader)
        {
            _fileUploader = fileUploader;
        }

        [Administrator]
        [ClinicGroupManager]
        public ActionResult Index()
        {
            return View();
        }

        [Administrator]
        [ClinicGroupManager]
        public PartialViewResult IndexGrid()
        {
            var model = RapidVetUnitOfWork.ClinicGroupRepository.GetAllActiveIncludeClinics();
            return PartialView("_Index", model);
        }

        [Administrator]
        [ClinicGroupManager]
        public ActionResult Create()
        {
            var model = new ClinicGroupCreate();
            return View(model);
        }


        [HttpPost]
        [Administrator]
        [ClinicGroupManager]
        public ActionResult Create(ClinicGroupCreate model, HttpPostedFileBase LogoImage)
        {

            if (ModelState.IsValid)
            {
                var clinicGroup = AutoMapper.Mapper.Map<ClinicGroupCreate, ClinicGroup>(model);
                var haveLogo = LogoImage != null && LogoImage.ContentLength > 0 &&
                               LogoImage.ContentType.StartsWith("image");

                clinicGroup.CreatedDate = DateTime.Now;
                clinicGroup.CreatedById = UserId;
                clinicGroup.Active = true;
                clinicGroup.HasLogo = haveLogo;

                //create default animal kind
                var defaultKind = DefaultAnimalKind();
                clinicGroup.AnimalKinds.Add(defaultKind);
                clinicGroup.ClientStatuses.Add(new ClientStatus()
                    {
                        Active = true,
                        Name = Resources.ClinicGroup.DefaultClientStatus,
                        MeetingColor = "#33cccc"
                    });
                clinicGroup.AnimalColors.Add(new AnimalColor()
                    {
                        Active = true,
                        Value = Resources.ClinicGroup.DefaultColor
                    });


                var status = RapidVetUnitOfWork.ClinicGroupRepository.Create(clinicGroup);


                if (status.Success)
                {
                    var users = RapidVetUnitOfWork.ClinicGroupRepository.GetClinicGroupUsers(clinicGroup.Id).ToList();

                    if (users.Any())
                    {
                        var user = users.First();
                        Roles.AddUserToRole(user.Username, RapidConsts.RolesConsts.CLINICGROUPMANAGER);
                        RapidVetUnitOfWork.UserRepository.AddToClinic(user.Id,
                                                                      RapidConsts.RolesConsts.CLINICGROUPMANAGER,
                                                                      clinicGroup.Clinics.First().Id);
                        RapidVetUnitOfWork.UserRepository.AddToClinic(user.Id, RapidConsts.RolesConsts.DOCTOR,
                                                                      clinicGroup.Clinics.First().Id);
                    }
                    //save the image
                    if (haveLogo)
                    {
                        //Image image = new Bitmap(LogoImage.InputStream);
                        var uploaded = RapidVet.FilesAndStorage.ClinicGroupLogoUploadHelper.UploadClinicGroupLogo(CurrentUser.ActiveClinicGroupId, LogoImage);
                        clinicGroup.LogoSizeInBytes = uploaded ? LogoImage.ContentLength : 0;
                        RapidVetUnitOfWork.Save();
                    }

                    SetSuccessMessage();
                    return RedirectToAction("Index", "ClinicGroups");
                }
                SetErrorMessage();
            }
            return View(model);
        }

        //id is clinicGropId
        [AnyRoleAttribute]
        public ActionResult LogoImage(int? id)
        {
            var savePath = System.Configuration.ConfigurationManager.AppSettings["LOG_PATH"];
            var clinicGroupId = CurrentUser.ActiveClinicGroupId;
            if (savePath != null)
                BaseLogger.LogWriter.WriteMsgPath(String.Format("LogoImage, clinicGroupId={0}, id={1}", clinicGroupId, id), null, savePath);

            if (id > 0)
            {
                clinicGroupId = id.Value;
            }

            HttpContext.Response.Cache.SetCacheability(HttpCacheability.Public);
            HttpContext.Response.Cache.SetMaxAge(new TimeSpan(0, 0, 1));

            DateTime? lastModified;
            using (var stream = FilesAndStorage.ClinicGroupLogoUploadHelper.GetClinicGroupLogoStream(clinicGroupId, out lastModified))
            {
                if (stream == null)
                {
                    var dir = Server.MapPath("/Content/img/rapidvet");
                    var path = Path.Combine(dir, "logo.png");
                    return base.File(path, "image/png");
                }
                else
                {
                    string rawIfModifiedSince = HttpContext.Request.Headers.Get("If-Modified-Since");
                    if (string.IsNullOrEmpty(rawIfModifiedSince))
                    {
                        // Set Last Modified time
                        HttpContext.Response.Cache.SetLastModified(lastModified.Value);
                    }
                    else
                    {
                        DateTime ifModifiedSince = DateTime.Parse(rawIfModifiedSince);
                        double diff = lastModified.Value.Subtract(ifModifiedSince).TotalMilliseconds;
                        if (diff < 1000)
                        {
                            // The requested file has not changed
                            HttpContext.Response.StatusCode = 304;
                            return Content(string.Empty);
                        }
                    }
                }


                byte[] arrB = stream.ToArray();
                MemoryStream ms = new MemoryStream(arrB);
                return File(ms, "image/png", "myimage.png");
                //return File(stream, "image/bmp", "myimage.bmp"); //FileStreamResult(stream, "image/bmp");
            }
            return null;
        }


        //id is clinicGroup id
        [ClinicGroupManager]
        public ActionResult EditMain() //int id)
        {
            return View();
        }

        //[ChildActionOnly]
        //public PartialViewResult EditNavigation()
        //{
        //    return PartialView("_EditNavigation");
        //}

        [ClinicGroupManager]
        public ActionResult Edit()
        {
            return View(getEditModel());
        }

        [ClinicGroupManager]
        public ActionResult EditCalendarTemplates()
        {
            return View(getEditModel());
        }

        [HttpGet]
        [ClinicGroupManager]
        public JsonResult GetTemplateKeywordsForGroup(string groupName)
        {
            List<string> keywords = null;
            TemplateKeyWordsGroups parsedGroupName;

            if (Enum.TryParse(groupName, out parsedGroupName))
            {
                switch (parsedGroupName)
                {
                    case TemplateKeyWordsGroups.Birthday:
                        keywords = new List<string>(){ "ClientFirstName", "ClientLastName", "ClientName", "ClientAddress", 
                                                       "ClientZip", "PatientName", "PatientBirthDate", "DoctorName",
                                                       "ClinicAddress", "ClinicName", "ClinicPhone", "ClinicFax", "ClinicEmail",
                                                       "ActiveDoctorName", "ActiveDoctorLicence", "Date", "ClientReferer", 
                                                       "PatientAnimalKind", "PatientRace", "PatientColor", "PatientAge",
                                                       "PatientSex", "ClientBalance", "ClientAge", "ClientTitle", "ClientIdCardNumber",
                                                       "ClientCity", "ClientHomePhone", "ClientMobilePhone", "ClientBirthdate", 
                                                       "ClientStreetAndNumber", "ClientRecordNumber" };
                        break;
                    case TemplateKeyWordsGroups.Calendar:
                        keywords = new List<string>(){ "ClientFirstName", "ClientLastName", "ClientName", "AppointmentTime", "ClientHomePhone", 
                                                       "ClientWorkPhone", "ClientMobilePhone", "ClinicAddress", "ClinicName", "ClinicPhone", 
                                                       "ClinicFax", "ClinicEmail", "PatientName", "Comment", "DoctorName"
                                                    };
                        break;
                    case TemplateKeyWordsGroups.ClientsReport:
                        keywords = new List<string>(){ "DoctorName", "ClientFirstName", "ClientLastName", "ClientName", "ClientAddress",
                                                       "ClientZip", "PatientName", "Sterilization", "PatientSex", "PatientRace", "PatientAnimalKind",
                                                       "ClientWorkPhone", "ClientHomePhone", "ClientMobilePhone", "PatientColor", "ClientAge",
                                                       "ClientTitle", "PatientChip", "ClientIdCardNumber", "ClientIdCard", "ClientReferer",
                                                       "ClientStatus", "Comment", "ClientRecordNumber", "ClientBalance","ClinicAddress", 
                                                       "ClinicName", "ClinicPhone", "ClinicFax", "ClinicEmail", "ClinicLogo", "Date",
                                                       "ActiveDoctorName", "ActiveDoctorLicence", "ClientCity", "ClientBirthdate", "ClientStreetAndNumber"
                                                     };
                        break;
                    case TemplateKeyWordsGroups.FollowUps:
                        keywords = new List<string>() { "ClientName", "ClientMobilePhone", "ClientHomePhone", "ClientAddress", "PatientName",
                                                        "PatientAnimalKind", "DoctorName", "ClientTitle", "Comment", "ClinicAddress", "ClinicName",
                                                        "ClinicPhone", "ClinicFax", "ClinicEmail", "ClinicLogo", "ClientFirstName", "ClientLastName",
                                                        "ActiveDoctorName", "ActiveDoctorLicence", "Date", "ClientReferer", "PatientRace",
                                                        "PatientColor", "PatientAge", "PatientSex", "ClientBalance", "ClientAge", "ClientBirthdate",
                                                        "ClientIdCardNumber", "ClientCity", "ClientZip", "ClientStreetAndNumber", "ClientRecordNumber"
                                                      };
                        break;
                    case TemplateKeyWordsGroups.PreventiveMedicineReminders:
                        keywords = new List<string>() { "ClientFirstName", "PatientName", "ClinicName", "ClinicPhone", "Date", "ClientName",
                                                        "ClientLastName", "DoctorName", "ClientAddress", "ClientZip", "ClinicAddress",
                                                        "PreventiveMedicineName", "TreatmentDetails"
                                                        //"PriceListItemName", "TreatmentDate",
                                                      };
                        break;
                    case TemplateKeyWordsGroups.VisitsReport:
                        keywords = new List<string>() { "DoctorName", "ClientFirstName", "ClientLastName", "ClientName", "ClientAddress",
                                                        "PatientName", "PriceListItemName", "Sterilization", "PatientSex", "PatientRace",
                                                        "PatientAnimalKind", "ClientHomePhone", "ClientMobilePhone", "PatientColor",
                                                        "PatientBirthDate", "Summery", "PatientChip", "ClientIdCardNumber", "ClientIdCard",
                                                        "ClinicAddress", "ClinicName", "ClinicPhone", "ClinicFax", "ClinicEmail", 
                                                        "ClinicLogo", "Date", "ActiveDoctorName", "ActiveDoctorLicence", "ClientReferer", 
                                                        "PatientAge", "ClientBalance", "ClientAge", "ClientTitle", "ClientCity", 
                                                        "ClientZip", "ClientBirthdate", "ClientStreetAndNumber","ClientRecordNumber",
                                                      };
                        break;
                }



                var model = RapidVetUnitOfWork.TemplateKeyWordsRepository.GetAllTemplateKeyWords(CultureInfo.CurrentCulture.Name)
                          .Select(t => new TemplateKeyWordsWebModel { TemplateKeyWord = t.TemplateKeyWord, KeyWord = t.KeyWord, KeyWordComment = t.Comment }).ToList();

                model = model.Where(w => keywords.Contains(w.TemplateKeyWord)).OrderBy(t => t.KeyWord).ToList();

                return Json(model, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        [HttpPost]
        [ClinicGroupManager]
        [ValidateInput(false)]
        public ActionResult EditCalendarTemplates(ClinicGroupEdit model)
        {
            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);

            clinicGroup.CalenderEntrySmsTemplate = model.CalenderEntrySmsTemplate;
            clinicGroup.CalenderEntryEmailSubject = model.CalenderEntryEmailSubject;
            clinicGroup.CalenderEntryEmailTemplate = model.CalenderEntryEmailTemplate;

            clinicGroup.UpdatedById = UserId;
            clinicGroup.UpdatedDate = DateTime.Now;
            clinicGroup.Id = CurrentUser.ActiveClinicGroupId;


            var status = RapidVetUnitOfWork.ClinicGroupRepository.Update(clinicGroup);
            //   var data = new RedirectData();
            if (status.Success)
            {
                CurrentUser.UpdateClinicGroup(clinicGroup);
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }


            return RedirectToAction("EditCalendarTemplates", "ClinicGroups");
        }

        [ClinicGroupManager]
        public ActionResult EditPreventiveRemindersTemplates()
        {
            return View(getEditModel());
        }

        [HttpPost]
        [ClinicGroupManager]
        [ValidateInput(false)]
        public ActionResult EditPreventiveRemindersTemplates(ClinicGroupEdit model)
        {
            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);

            clinicGroup.PreventiveMedicineSmsTemplate = model.PreventiveMedicineSmsTemplate;
            clinicGroup.PreventiveMedicineSmsMultiAnimalsTemplate = model.PreventiveMedicineSmsMultiAnimalsTemplate;
            clinicGroup.PreventiveMedicineLetterTemplate = model.PreventiveMedicineLetterTemplate;
            clinicGroup.PreventiveMedicineLetterMultiAnimalsTemplate = model.PreventiveMedicineLetterMultiAnimalsTemplate;
            clinicGroup.PreventiveMedicinePostCardTemplate = model.PreventiveMedicinePostCardTemplate;
            clinicGroup.PreventiveMedicinePostCardMultiAnimalsTemplate = model.PreventiveMedicinePostCardMultiAnimalsTemplate;
            clinicGroup.PreventiveMedicineStikerTemplate = model.PreventiveMedicineStikerTemplate;
            clinicGroup.PreventiveMedicineEmailSubject = model.PreventiveMedicineEmailSubject;
            clinicGroup.PreventiveMedicineEmailTemplate = model.PreventiveMedicineEmailTemplate;
            clinicGroup.PreventiveMedicineEmailMultiAnimalsSubject = model.PreventiveMedicineEmailMultiAnimalsSubject;
            clinicGroup.PreventiveMedicineEmailMultiAnimalsTemplate = model.PreventiveMedicineEmailMultiAnimalsTemplate;
            clinicGroup.UnitePreventiveReminders = model.UnitePreventiveReminders;

            clinicGroup.UpdatedById = UserId;
            clinicGroup.UpdatedDate = DateTime.Now;
            clinicGroup.Id = CurrentUser.ActiveClinicGroupId;


            var status = RapidVetUnitOfWork.ClinicGroupRepository.Update(clinicGroup);
            //   var data = new RedirectData();
            if (status.Success)
            {
                CurrentUser.UpdateClinicGroup(clinicGroup);
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }


            return RedirectToAction("EditPreventiveRemindersTemplates", "ClinicGroups");
        }

        [ClinicGroupManager]
        public ActionResult EditBirthdayTemplates()
        {
            return View(getEditModel());
        }

        [HttpPost]
        [ClinicGroupManager]
        [ValidateInput(false)]
        public ActionResult EditBirthdayTemplates(ClinicGroupEdit model)
        {
            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);

            clinicGroup.BirthdayStickerTemplate = model.BirthdayStickerTemplate;
            clinicGroup.BirthdayEmailSubject = model.BirthdayEmailSubject;
            clinicGroup.BirthdayEmailTemplate = model.BirthdayEmailTemplate;
            clinicGroup.BirthdaySmsTemplate = model.BirthdaySmsTemplate;

            clinicGroup.UpdatedById = UserId;
            clinicGroup.UpdatedDate = DateTime.Now;
            clinicGroup.Id = CurrentUser.ActiveClinicGroupId;


            var status = RapidVetUnitOfWork.ClinicGroupRepository.Update(clinicGroup);
            //   var data = new RedirectData();
            if (status.Success)
            {
                CurrentUser.UpdateClinicGroup(clinicGroup);
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }


            return RedirectToAction("EditBirthdayTemplates", "ClinicGroups");
        }

        [ClinicGroupManager]
        public ActionResult EditVisitReportTemplates()
        {
            return View(getEditModel());
        }

        [HttpPost]
        [ClinicGroupManager]
        [ValidateInput(false)]
        public ActionResult EditVisitReportTemplates(ClinicGroupEdit model)
        {
            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);

            clinicGroup.TreatmentStikerTemplate = model.TreatmentStikerTemplate;
            clinicGroup.TreatmentEmailSubject = model.TreatmentEmailSubject;
            clinicGroup.TreatmentEmailTemplate = model.TreatmentEmailTemplate;
            clinicGroup.TreatmentSmsTemplate = model.TreatmentSmsTemplate;

            clinicGroup.UpdatedById = UserId;
            clinicGroup.UpdatedDate = DateTime.Now;
            clinicGroup.Id = CurrentUser.ActiveClinicGroupId;


            var status = RapidVetUnitOfWork.ClinicGroupRepository.Update(clinicGroup);
            //   var data = new RedirectData();
            if (status.Success)
            {
                CurrentUser.UpdateClinicGroup(clinicGroup);
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }


            return RedirectToAction("EditVisitReportTemplates", "ClinicGroups");
        }

        [ClinicGroupManager]
        public ActionResult EditClientsReportTemplates()
        {
            return View(getEditModel());
        }

        [HttpPost]
        [ClinicGroupManager]
        [ValidateInput(false)]
        public ActionResult EditClientsReportTemplates(ClinicGroupEdit model)
        {
            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);

            clinicGroup.ClientsStikerTemplate = model.ClientsStikerTemplate;
            clinicGroup.ClientsEmailSubject = model.ClientsEmailSubject;
            clinicGroup.ClientsEmailTemplate = model.ClientsEmailTemplate;
            clinicGroup.ClientsSmsTemplate = model.ClientsSmsTemplate;

            clinicGroup.UpdatedById = UserId;
            clinicGroup.UpdatedDate = DateTime.Now;
            clinicGroup.Id = CurrentUser.ActiveClinicGroupId;


            var status = RapidVetUnitOfWork.ClinicGroupRepository.Update(clinicGroup);
            //   var data = new RedirectData();
            if (status.Success)
            {
                CurrentUser.UpdateClinicGroup(clinicGroup);
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }


            return RedirectToAction("EditClientsReportTemplates", "ClinicGroups");
        }


        [ClinicGroupManager]
        public ActionResult EditFollowUpsTemplates()
        {
            return View(getEditModel());
        }

        [HttpPost]
        [ClinicGroupManager]
        [ValidateInput(false)]
        public ActionResult EditFollowUpsTemplates(ClinicGroupEdit model)
        {
            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);

            clinicGroup.FollowUpsEmailSubject = model.FollowUpsEmailSubject;
            clinicGroup.FollowUpsEmailTemplate = model.FollowUpsEmailTemplate;
            clinicGroup.FollowUpsSmsTemplate = model.FollowUpsSmsTemplate;

            clinicGroup.UpdatedById = UserId;
            clinicGroup.UpdatedDate = DateTime.Now;
            clinicGroup.Id = CurrentUser.ActiveClinicGroupId;


            var status = RapidVetUnitOfWork.ClinicGroupRepository.Update(clinicGroup);
            //   var data = new RedirectData();
            if (status.Success)
            {
                CurrentUser.UpdateClinicGroup(clinicGroup);
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }


            return RedirectToAction("EditFollowUpsTemplates", "ClinicGroups");
        }

        private ClinicGroupEdit getEditModel()
        {
            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);
            var model = AutoMapper.Mapper.Map<ClinicGroup, ClinicGroupEdit>(clinicGroup);

            return model;

        }

        [HttpPost]
        [ClinicGroupManager]
        public ActionResult Edit(ClinicGroupEdit model, HttpPostedFileBase LogoImage)
        {
            if (ModelState.IsValid)
            {
                var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);

                //defending against permissions hack
                if (CurrentUser.IsAdministrator)
                {
                    clinicGroup = AutoMapper.Mapper.Map(model, clinicGroup);

                    if (!model.TrialPeriod)
                    {
                        clinicGroup.ExpirationDate = null;
                    }
                }
                else
                {
                    model.MaxGoogleSyncAllowed = clinicGroup.MaxGoogleSyncAllowed;
                    model.MaxConcurrentUsers = clinicGroup.MaxConcurrentUsers;
                    model.TimeToDisconnectInactiveUsersInMinutes = clinicGroup.TimeToDisconnectInactiveUsersInMinutes;
                    model.HSModule = clinicGroup.HSModule;
                    model.StockModule = clinicGroup.StockModule;
                    model.TrialPeriod = clinicGroup.TrialPeriod;
                    model.ExpirationDate = clinicGroup.ExpirationDate.HasValue ? clinicGroup.ExpirationDate.Value.ToShortDateString() : null;
                    clinicGroup = AutoMapper.Mapper.Map(model, clinicGroup);
                }

                clinicGroup.EmailBusinessName = RemoveInvalidXmlChars(clinicGroup.EmailBusinessName);

                clinicGroup.UpdatedById = UserId;
                clinicGroup.UpdatedDate = DateTime.Now;
                clinicGroup.Id = CurrentUser.ActiveClinicGroupId;

                //save the image
                if (LogoImage != null && LogoImage.ContentLength > 0 && LogoImage.ContentType.StartsWith("image"))
                {
                    // Image image = new Bitmap(LogoImage.InputStream);
                    var uploaded = RapidVet.FilesAndStorage.ClinicGroupLogoUploadHelper.UploadClinicGroupLogo(clinicGroup.Id, LogoImage);
                    if (uploaded)
                    {
                        clinicGroup.HasLogo = true;
                        clinicGroup.LogoSizeInBytes = LogoImage.ContentLength;
                        RapidVetUnitOfWork.Save();
                    }
                }

                var status = RapidVetUnitOfWork.ClinicGroupRepository.Update(clinicGroup);
                //   var data = new RedirectData();
                if (status.Success)
                {
                    CurrentUser.UpdateClinicGroup(clinicGroup);
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
            }

            return RedirectToAction("Edit", "ClinicGroups");

        }


        [HttpGet]
        [Administrator]
        [ClinicGroupManager]
        public ActionResult Delete()
        {
            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);
            var model = AutoMapper.Mapper.Map<ClinicGroup, ClinicGroupEdit>(clinicGroup);

            return PartialView("_Delete", model);
        }

        [HttpPost]
        [Administrator]
        [ClinicGroupManager]
        public ActionResult Remove()
        {

            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);
            clinicGroup.Deactivate();

            var status = RapidVetUnitOfWork.ClinicGroupRepository.Update(clinicGroup);

            if (status.Success)
            {
                SetSuccessMessage(UiMessages.CreateMessage(EntityType.ClinicGroup, clinicGroup.Name,
                                                           OperationType.Updated));
            }
            else
            {
                SetErrorMessage(UiMessages.CreateMessage(EntityType.ClinicGroup, clinicGroup.Name, OperationType.Updated,
                                                         true));
            }
            return IndexGrid();
        }

        [HttpGet]
        [ClinicGroupManager]
        public JsonResult GetAdministrativeData()
        {
            var users = RapidVetUnitOfWork.ClinicGroupRepository.GetClinicGroupUsers(CurrentUser.ActiveClinicId);

            var model = new AdministrativeDataJsonModel()
            {
                IsAdministrator = CurrentUser.IsAdministrator,
                ActiveUsers = users.Where(u => u.Active).Count(),
                NonActiveUsers = users.Where(u => !u.Active).Count()
            };


            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ClinicGroupManager]
        [ValidateInput(false)]
        public JsonResult SendTestMail(string EmailAddress, string EmailBusinessName)
        {

            //save latest changes to db
            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);
            clinicGroup.EmailAddress = EmailAddress;
            clinicGroup.EmailBusinessName = EmailBusinessName;
            clinicGroup.EmailBusinessName = RemoveInvalidXmlChars(clinicGroup.EmailBusinessName);

            clinicGroup.UpdatedById = CurrentUser.Id;
            clinicGroup.UpdatedDate = DateTime.Now;
            clinicGroup.Id = CurrentUser.ActiveClinicGroupId;

            RapidVetUnitOfWork.ClinicGroupRepository.Update(clinicGroup);
            CurrentUser.UpdateClinicGroup(clinicGroup);

            string status = "���� ������ ���� ������ " + EmailAddress;
            string from = string.Format("{0}<{1}>", !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.EmailBusinessName) ? CurrentUser.ActiveClinicGroup.EmailBusinessName : CurrentUser.ActiveClinic.Name,
                                                     CurrentUser.ActiveClinicGroup.EmailAddress);

            var client = getMailClient();
            RestRequest request = new RestRequest();
            request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_RAPID_DOMAIN"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", from);
            request.AddParameter("to", EmailAddress);
            request.AddParameter("subject", "���� ����� �����");
            request.AddParameter("html", "<p style=\"position:relative;display:block;margin:auto;direction:rtl; text-align:right;\">��� ���� ����� ������ RapidVet.<br> ���� ����� ����� ����� " + DateTime.Now + "</p>");
            request.Method = Method.POST;
            var execute = client.Execute(request);
            if ((int)execute.StatusCode != 200)
            {
                status = "����� �����";
            }
            else
            {
                RapidVetUnitOfWork.MessageDistributionRepository.Increment(RapidVetUnitOfWork.ClinicGroupRepository.GetFirstClinic(CurrentUser.ActiveClinicGroupId).Id, 1, Model.Clinics.MessageType.Email);

                string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                Resources.Auditing.SendingModule, Resources.Auditing.EmailTestCheck,
                                                                Resources.Auditing.ClientName, CurrentUser.ActiveClinicGroup.Name,
                                                                Resources.Auditing.Address, EmailAddress,
                                                                Resources.Auditing.MailSubject, "���� ����� �����",
                                                                Resources.Auditing.ReturnCode, (int)execute.StatusCode + " - " + execute.StatusCode);

                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.MailSent, logMessage, "");

            }

            RapidVetUnitOfWork.Save();
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        private string RemoveInvalidXmlChars(string text)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                text = text.Replace("<", "");
                text = text.Replace(">", "");
                text = text.Replace("&", "");
                text = text.Replace("\"", "''");
            }
            return text;
        }

        private bool ValidateTestMailFields(int? EmailPort, string EmailSmtp, string EmailAddress, string EmailUserName, string EmailPassword)
        {
            bool isValid = true;

            if (!EmailPort.HasValue)
            {
                isValid = false;
                ViewBag.EmailPort = "��� ����";
            }

            return isValid;

        }

        [Administrator]
        [ClinicGroupManager]
        public PartialViewResult MoveClinic(int clinicId)
        {
            var model = RapidVetUnitOfWork.ClinicGroupRepository.GetAllActiveIncludeClinics();
            ViewBag.ClinicId = clinicId;
            return PartialView("_MoveClinic", model);
        }

        [Administrator]
        [HttpPost]
        [ClinicGroupManager]
        public ActionResult MoveClinic(int clinicId, int targetClinicGroupId)
        {
            var status = RapidVetUnitOfWork.ClinicTransferRepository.MoveClinicFromClinicGroup(clinicId, targetClinicGroupId);
            if (status.Success)
            {
                CurrentUser.ActiveClinicGroup = null;
                SetSuccessMessage(RapidVet.Resources.Global.SuccessMessage);
            }
            else
            {
                SetErrorMessage(RapidVet.Resources.Global.ErrorMessage);
            }
            return RedirectToAction("Index", "ClinicGroups");
        }

        [Administrator]
        [ClinicGroupManager]
        public PartialViewResult DuplicateClinicGroup(int clinicGroupId)
        {
            var model = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(clinicGroupId);
            return PartialView("_DuplicateClinicGroup", model);
        }

        [Administrator]
        [HttpPost]
        [ClinicGroupManager]
        public ActionResult DuplicateClinicGroup(int clinicGroupId, string newClinicGroupName)
        {
            var status = RapidVetUnitOfWork.ClinicGroupDuplicateRepository.DuplicateClinicGroup(clinicGroupId, newClinicGroupName);
            if (status.Success)
            {
                SetSuccessMessage(RapidVet.Resources.Global.SuccessMessage);
            }
            else
            {
                SetErrorMessage(RapidVet.Resources.Global.ErrorMessage);
            }
            return RedirectToAction("Index", "ClinicGroups");
        }

        /*---------------------private functions-------------------------*/
        private AnimalKind DefaultAnimalKind()
        {



            var defaultKind = new AnimalKind()
                {
                    Active = true,
                    Value = Resources.ClinicGroup.DefaultAnimalKind,
                    AnimalIconId = 1,
                    AnimalRaces = new List<AnimalRace>()
                        {
                            new AnimalRace()
                                {
                                    Active = true,
                                    Value = Resources.ClinicGroup.DefaultAnimalRace,
                                }
                        }
                };

            return defaultKind;
        }


        //testing SMS send, cell is the desired number
        [ClinicGroupManager]
        public string SendTestSms(string cell)
        {
            var xml = string.Format(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?><sms command=\"submit\" version=\"1.0\"><account><id>rapid1000</id><password>hagit1</password></account><attributes><replyPath></replyPath></attributes><targets><cellphone>{0}</cellphone></targets><data type=\"text\">sent from vetcloud.cloudapp.net</data></sms>",
                cell);
            var sms = new Unicell.Sms(CurrentUser.ActiveClinicGroup.UnicellUserName,
                                      CurrentUser.ActiveClinicGroup.UnicellPassword, cell,
                                      " sent from vetcloud.cloudapp.net", string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.SignatureSmsTemplate) ? "" : CurrentUser.ActiveClinicGroup.SignatureSmsTemplate);
            var response = sms.SendStr();

            var sb = new StringBuilder();
            sb.Append("<textarea>");
            sb.Append(xml);
            sb.AppendLine();
            sb.Append(response);
            sb.Append("</textarea>");

            return sb.ToString();
        }
    }
}