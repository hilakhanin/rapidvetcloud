﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model.Clinics;
using System.Globalization;
using RapidVet.WebModels.ClinicGroups;

namespace RapidVet.Web.Controllers.Settings
{
    [Authorize]
    public class LetterTemplatesController : BaseController
    {

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult PersonalLettersJson()
        {
            var personalLetters =
                RapidVetUnitOfWork.LetterTemplatesRepository.GetClinicPersonalLetters(CurrentUser.ActiveClinicId).ToList();
            return Json(personalLetters, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddPersonalTemplate(LetterTemplate model)
        {
            var template = RapidVetUnitOfWork.LetterTemplatesRepository.GetOrCreatePersonalTemplate(0, CurrentUser.ActiveClinicId);
            template.TemplateName = model.TemplateName;
            var status = RapidVetUnitOfWork.Save();
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("Index", "LetterTemplates");
        }


        [HttpPost]
        public ActionResult EditPersonalTemplateName(LetterTemplate model)
        {
            var template = RapidVetUnitOfWork.LetterTemplatesRepository.GetOrCreatePersonalTemplate(model.Id, CurrentUser.ActiveClinicId);
            template.TemplateName = model.TemplateName;
            var status = RapidVetUnitOfWork.Save();
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("Index", "LetterTemplates");
        }

      

        public ActionResult EditPersonalTemplate(int id)
        {
            var template = RapidVetUnitOfWork.LetterTemplatesRepository.GetPersonalTemplate(id);
            ViewBag.TemplateKeywords = RapidVetUnitOfWork.TemplateKeyWordsRepository.GetLetterTemplateKeyWord(template.LetterTemplateType);
            return View(template);
        }

        public ActionResult Edit(int id)
        {
            var template = RapidVetUnitOfWork.LetterTemplatesRepository.GetOrCreateTemplate(id, CurrentUser.ActiveClinicId);
            ViewBag.TemplateKeywords = RapidVetUnitOfWork.TemplateKeyWordsRepository.GetLetterTemplateKeyWord(template.LetterTemplateType);
            return View(template);
        }

     

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult EditPersonalTemplate(LetterTemplate model)
        {
            var template = RapidVetUnitOfWork.LetterTemplatesRepository.GetOrCreatePersonalTemplate(model.Id, CurrentUser.ActiveClinicId);
            template.Content = model.Content;
            var status = RapidVetUnitOfWork.Save();
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("Index", "LetterTemplates");
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(LetterTemplate model)
        {
          
            var template = RapidVetUnitOfWork.LetterTemplatesRepository.GetOrCreateTemplate(model.LetterTemplateTypeId, CurrentUser.ActiveClinicId);
            template.Content = model.Content;
            var status = RapidVetUnitOfWork.Save();
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("Index", "LetterTemplates");
        }

        [HttpPost]
        public ActionResult DeletePersonalLetter(int id, string name)
        {
            var result = new JsonResult();
            var status = RapidVetUnitOfWork.LetterTemplatesRepository.DeletePersonalTemplate(id);
                      
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("Index", "LetterTemplates");
        }

        
        public JsonResult GetClinicsPersonalLetters()
        {
            if (IsPersonalLetterExist())
            {
                var letters = RapidVetUnitOfWork.LetterTemplatesRepository.GetClinicPersonalLetters(CurrentUser.ActiveClinicId);
               
                return Json(letters, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}
