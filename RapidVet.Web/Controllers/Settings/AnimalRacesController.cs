﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class AnimalRacesController : BaseController
    {

        //id is animalKindId
        [ClinicGroupManager]
        public ActionResult Index(int id)
        {
            var model = RapidVetUnitOfWork.AnimalRaceRepository.GetList(id);
            if (model.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            return View(model);
        }

        //id is animalKindId
        [HttpGet]
        [ClinicGroupManager]
        public ActionResult Create(int id)
        {
            var kinde = RapidVetUnitOfWork.AnimalRaceRepository.GetList(id);
            if (kinde.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = new AnimalRaceModel()
                {
                    AnimalKindId = id
                };
            return PartialView("_Create", model);
        }

        [HttpPost]
        [ClinicGroupManager]
        public ActionResult Create(AnimalRaceModel model)
        {

            var kinde = RapidVetUnitOfWork.AnimalRaceRepository.GetList(model.AnimalKindId);
            if (kinde.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var animalRace = AutoMapper.Mapper.Map<AnimalRaceModel, AnimalRace>(model);
            animalRace.Active = true;
            animalRace.Id = 0;
            var status = RapidVetUnitOfWork.AnimalRaceRepository.Create(animalRace);

            return RedirectToAction("Index", "AnimalKinds");
        }

        //id is animalRaceId
        [ClinicGroupManager]
        public PartialViewResult Edit(int id)
        {
            var animalRace = RapidVetUnitOfWork.AnimalRaceRepository.GetItem(id);
            if (animalRace.AnimalKind.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<AnimalRace, AnimalRaceModel>(animalRace);
            return PartialView("_Edit", model);
        }


        [HttpPost]
        [ClinicGroupManager]
        public ActionResult Edit(AnimalRaceModel model)
        {

            var animalRace = RapidVetUnitOfWork.AnimalRaceRepository.GetItem(model.Id);
            if (animalRace.AnimalKind.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            AutoMapper.Mapper.Map(model, animalRace);
            var status = RapidVetUnitOfWork.AnimalRaceRepository.Update(animalRace);
            return RedirectToAction("Index", "AnimalKinds");
        }

        [ClinicGroupManager]
        public PartialViewResult Delete(int id)
        {
            var animalRace = RapidVetUnitOfWork.AnimalRaceRepository.GetItem(id);
            if (animalRace.AnimalKind.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<AnimalRace, AnimalRaceModel>(animalRace);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        [ClinicGroupManager]
        public ActionResult Delete(AnimalRaceModel model)
        {
            var animalRace = RapidVetUnitOfWork.AnimalRaceRepository.GetItem(model.Id);
            if (animalRace.AnimalKind.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            animalRace.Active = false;
            var status = RapidVetUnitOfWork.AnimalRaceRepository.Save(animalRace);
            return RedirectToAction("Index", "AnimalKinds");
        }

        //id is animalKindId
        [HttpGet]
        public JsonResult RaceByAnimalKind(int id)
        {
            var kinde = RapidVetUnitOfWork.AnimalRaceRepository.GetList(id);
            if (kinde.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var animalRaces = RapidVetUnitOfWork.AnimalRaceRepository.GetListByAnimalKind(id);
            var races = animalRaces.Select(r => new SerializationHelperObject()
                {
                    Id = r.Id,
                    Value = r.Value
                }).ToList();

            return Json(races, JsonRequestBehavior.AllowGet);
        }
    }
}
