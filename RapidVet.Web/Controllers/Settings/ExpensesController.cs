﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using RapidVet.Enums;
using RapidVet.Model.Expenses;
using RapidVet.Web.Attributes;
using RapidVet.WebModels.Expenses;
using RapidVet.Repository.Clinics;
using System.IO;
using System.Text;
using CsvHelper.Configuration;
using CsvHelper;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class ExpensesController : BaseController
    {

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetClinicExpenseGroups()
        {
            // var list = new List<SelectListItem>();
            var data = RapidVetUnitOfWork.ExpenseGroupRepository.GetClinicExpenseGroups(CurrentUser.ActiveClinicId);

            //foreach (var d in data)
            //{
            //    list.Add(new SelectListItem()
            //    {
            //        Selected = false,
            //        Text = d.Name,
            //        Value = d.Id.ToString()
            //    });
            //}

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClinicSuppliers()
        {
            var list = new List<SelectListItem>();
            var data = RapidVetUnitOfWork.SupplierRepository.GetClinicSuppliers(CurrentUser.ActiveClinicId);

            foreach (var d in data)
            {
                list.Add(new SelectListItem()
                {
                    Selected = false,
                    Text = d.Name,
                    Value = d.Id.ToString()
                });
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDateTypes()
        {
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem()
                {
                    Selected = true,
                    Text = Resources.Expences.EnteredDate,
                    Value = Resources.Expences.EnteredDate
                });

            list.Add(new SelectListItem()
            {
                Selected = true,
                Text = Resources.Expences.InvoiceDate,
                Value = Resources.Expences.InvoiceDate
            });
            

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPaymentTypes()
        {
            var data = Helpers.FinanceHelper.PaymentTypeOptions();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExpense(int id)
        {
            var expense = RapidVetUnitOfWork.ExpenseRepository.GetExpense(id);
            var data = AutoMapper.Mapper.Map<Expense, ExpenseCreateEditWebModel>(expense);

            data.InvoiceDate = expense.InvoiceDate.ToShortDateString();
            data.EnteredDate = expense.EnteredDate.ToShortDateString();
            data.PaymentDate = expense.PaymentDate.HasValue ? expense.PaymentDate.Value.ToShortDateString() : "";

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClinicExpenses(string fromDate, string toDate, int issuerId, string dateType)
        {
            var model = getClinicExpenses(fromDate, toDate, issuerId, dateType);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintClinicExpenses(string fromDate, string toDate, int issuerId, string dateType)
        {
            var model = getClinicExpenses(fromDate, toDate, issuerId, dateType);
            ViewBag.DateType = dateType;
            ViewBag.From = fromDate;
            ViewBag.To = toDate;
            return View(model);
        }

        private List<ExpenseIndexWebModel> getClinicExpenses(string fromDate, string toDate, int issuerId, string dateType)
        {
            var from = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var to = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1);
            if (!string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
            {
                from = DateTime.ParseExact(fromDate, "dd/MM/yyyy", null);
                to = DateTime.ParseExact(toDate, "dd/MM/yyyy", null);
            }
            var expences =
                    RapidVetUnitOfWork.ExpenseRepository.GetClinicExpenses(CurrentUser.ActiveClinicId, from, to, issuerId, dateType)
                                      .ToList();
            var model = expences.Select(AutoMapper.Mapper.Map<Expense, ExpenseIndexWebModel>).ToList();
            return model;
        }

        [ExcelExport]
        public ActionResult ExportToCSV(string fromDate, string toDate, int issuerId, string dateType)
        {
            var model = getClinicExpenses(fromDate, toDate, issuerId, dateType);

            return Excel(model);
        }

        private FileResult Excel(List<ExpenseIndexWebModel> data)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);

            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8, HasHeaderRecord = false };
            var csv = new CsvWriter(writer, csvConfig);

            csv.WriteField("תאריך הקלדה");
            csv.WriteField("תאריך חשבונית");
            csv.WriteField("ספק");
            csv.WriteField("קבוצה");
            csv.WriteField("תיאור");
            csv.WriteField("מספר חשבונית");
            csv.WriteField("סכום כולל מע\"מ");
            csv.WriteField("אחוז הכרה");
            csv.WriteField("מע\"מ");
            csv.WriteField("תאריך תשלום");
            csv.NextRecord();

            foreach (var expense in data)
            {
                csv.WriteRecord(new
                {
                    c1 = expense.EnteredDate,
                    c2 = expense.InvoiceDate,
                    c3 = expense.Supplier,
                    c4 = expense.ExpenseGroup,
                    c5 = expense.Description,
                    c6 = expense.InvoiceNumber,
                    c7 = expense.TotalAmount,
                    c8 = expense.RecognitionPercent,
                    c9 = expense.Vat,
                    c10 = expense.PaymentDate
                });
            }
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", "ExpenseReport.csv");
        }


        //private decimal getTaxRate()
        //{
        //    decimal TaxRate = CurrentUser.ActiveClinic.TaxRate;
        //    if (TaxRate == 0)
        //    {
        //        int id = Convert.ToInt32(CurrentUser.ActiveClinic.Id);
        //        ClinicRepository clcr = new ClinicRepository();
        //        var rate = clcr.GetAllTaxRateChanges(id).FirstOrDefault();
        //        TaxRate = rate != null ? rate.Rate : 0;
        //    }

        //    return TaxRate;
        //}

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.ExpenseId = 0;
            ViewBag.TaxRate = 1 + GetTaxRate() * (decimal)0.01;
            //var model = new ExpenseCreateEditWebModel()
            //    {
            //        InvoiceDate = DateTime.Now,
            //        PaymentDate = DateTime.Now
            //    };

            //ViewBag.Issuers = RapidVetUnitOfWork.IssuerRepository.GetDropdownList(CurrentUser.ActiveClinicId);
            return View();
        }

        [HttpPost]
        public JsonResult Create(ExpenseCreateEditWebModel model)
        {
            if (RapidVetUnitOfWork.ExpenseRepository.ValidateGroupAndSupplier(model.ExpenseGroupId, model.SupplierId, CurrentUser.ActiveClinicId))
            {
                var expense = AutoMapper.Mapper.Map<ExpenseCreateEditWebModel, Expense>(model);
                var status = RapidVetUnitOfWork.ExpenseRepository.Create(expense);
                if (status.Success)
                {
                    SetSuccessMessage("הנתונים נשמרו בהצלחה");
                }
                else
                {
                    SetErrorMessage();
                }
                return Json(status, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var expense = RapidVetUnitOfWork.ExpenseRepository.GetExpense(id);
            if (RapidVetUnitOfWork.ExpenseRepository.ValidateGroupAndSupplier(expense.ExpenseGroupId, expense.SupplierId, CurrentUser.ActiveClinicId))
            {
                ViewBag.ExpenseId = id;
                ViewBag.TaxRate = 1 + GetTaxRate() * (decimal)0.01;
                //var model = AutoMapper.Mapper.Map<Expense, ExpenseCreateEditWebModel>(expense);
                //ViewBag.Issuers = RapidVetUnitOfWork.IssuerRepository.GetDropdownList(CurrentUser.ActiveClinicId);

                return View();
            }
            throw new SecurityException();
        }

        [HttpPost]
        public JsonResult Edit(ExpenseCreateEditWebModel model)
        {
            if (RapidVetUnitOfWork.ExpenseRepository.ValidateGroupAndSupplier(model.ExpenseGroupId, model.SupplierId,
                                                                              CurrentUser.ActiveClinicId))
            {
                var expense = RapidVetUnitOfWork.ExpenseRepository.GetExpense(model.Id);
                AutoMapper.Mapper.Map(model, expense);
                
                if(model.PaymentTypeId == -1)
                {
                    expense.PaymentTypeId = null;
                    expense.PaymentDate = null;
                }

                expense.Updated = DateTime.Now;

                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    SetSuccessMessage("הנתונים נשמרו בהצלחה");
                }
                else
                {
                    SetErrorMessage();
                }
                return Json(status, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        [HttpGet]
        public PartialViewResult ConfirmDelete(int id)
        {
            var expense = RapidVetUnitOfWork.ExpenseRepository.GetExpense(id);
            if (RapidVetUnitOfWork.ExpenseRepository.ValidateGroupAndSupplier(expense.ExpenseGroupId, expense.SupplierId, CurrentUser.ActiveClinicId))
            {
                var model = AutoMapper.Mapper.Map<Expense, ExpenseCreateEditWebModel>(expense);
                return PartialView("_Delete", model);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var expense = RapidVetUnitOfWork.ExpenseRepository.GetExpense(id);
            if (RapidVetUnitOfWork.ExpenseRepository.ValidateGroupAndSupplier(expense.ExpenseGroupId, expense.SupplierId, CurrentUser.ActiveClinicId))
            {
                var status = RapidVetUnitOfWork.ExpenseRepository.Delete(expense);
                if (status.Success)
                {
                    SetSuccessMessage("הנתונים נשמרו בהצלחה");
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "Expenses");
            }
            throw new SecurityException();
        }

        [ViewFinancialReports]
        [HttpGet]
        public ActionResult ExpensesReports()
        {

            //  ViewBag.Issuers = RapidVetUnitOfWork.IssuerRepository.GetDropdownList(CurrentUser.ActiveClinicId);
            return View();
        }

        //id is supplierId
        [ViewFinancialReports]
        public ActionResult ReportBySupplier(int id, string from, string to, int issuerId)
        {
            if (!string.IsNullOrWhiteSpace(from) || !string.IsNullOrWhiteSpace(to))
            {
                if (RapidVetUnitOfWork.SupplierRepository.IsSupplierInClinic(CurrentUser.ActiveClinicId, id))
                {
                    var fromDate = DateTime.Parse(from);
                    var toDate = DateTime.Parse(to);
                    var expenses = RapidVetUnitOfWork.ExpenseRepository.GetReportBySupplier(id, fromDate, toDate, issuerId);
                    var model = expenses.Select(AutoMapper.Mapper.Map<Expense, ExpenseIndexWebModel>).ToList();
                    var supplierName = RapidVetUnitOfWork.SupplierRepository.GetName(id);
                    ViewBag.Title = string.Format("חיתוך הוצאות עבור {0} בתאריכים {1}-{2}", supplierName,
                                                  fromDate.ToShortDateString(), toDate.ToShortDateString());
                    ViewBag.Name = string.Empty;
                    return View(model);
                }
            }
            return View("Error");
        }

        [ViewFinancialReports]
        public ActionResult ReportByGroups(string from, string to, int issuerId)
        {
            if (!string.IsNullOrWhiteSpace(from) || !string.IsNullOrWhiteSpace(to))
            {
                var fromDate = DateTime.Parse(from);
                var toDate = DateTime.Parse(to);
                var expenses = RapidVetUnitOfWork.ExpenseRepository.GetExpenses(CurrentUser.ActiveClinicId, fromDate,
                                                                                toDate, issuerId);

                var groupIds = new HashSet<int>();
                foreach (var e in expenses)
                {
                    groupIds.Add(e.ExpenseGroupId);
                }

                var model = new List<ExpenseReportModel>();
                foreach (var expenseGroupId in groupIds)
                {
                    var items = expenses.Where(e => e.ExpenseGroupId == expenseGroupId);
                    model.Add(new ExpenseReportModel()
                        {
                            Title = items.First().ExpenseGroup.Name,
                            Expenses = items.Select(AutoMapper.Mapper.Map<Expense, ExpenseIndexWebModel>).ToList()
                        });
                }

                ViewBag.Title = string.Format("חיתוך על פי קבוצת הוצאות לתאריכים {0}-{1}", fromDate.ToShortDateString(),
                                              toDate.ToShortDateString());
                return View(model);

            }
            return View("Error");
        }

        [ViewFinancialReports]
        public ActionResult MonthlyReport(string from, string to, int issuerId)
        {
            if (!string.IsNullOrWhiteSpace(from) || !string.IsNullOrWhiteSpace(to))
            {
                var fromDate = DateTime.Parse(from);
                var toDate = DateTime.Parse(to);
                var expenses = RapidVetUnitOfWork.ExpenseRepository.GetExpenses(CurrentUser.ActiveClinicId, fromDate,
                                                                                toDate, issuerId).ToList();

                var monthsCount = GetMonthsCount(fromDate, toDate);
                var loopDate = new DateTime(fromDate.Year, fromDate.Month, 1);
                var model = new List<ExpenseReportModel>();
                for (int i = 0; i < monthsCount; i++)
                {
                    var nextMonth = loopDate.AddMonths(1);
                    var items = expenses.Where(e => e.InvoiceDate >= loopDate && e.InvoiceDate < nextMonth);
                    model.Add(new ExpenseReportModel()
                    {
                        Title = string.Format("{0}/{1}", loopDate.Month, loopDate.Year),
                        Expenses = items.Select(AutoMapper.Mapper.Map<Expense, ExpenseIndexWebModel>).ToList()
                    });
                    loopDate = loopDate.AddMonths(1);
                }
                ViewBag.Name = string.Format("דוח הוצאות חודשי לחודשים {0}/{1}-{2}/{3}", fromDate.Month, fromDate.Year,
                                             toDate.Month, toDate.Year);

                return View(model);

            }
            return View("Error");
        }

        [ViewFinancialReports]
        public ActionResult QuarterlyReport(string from, string to, int issuerId)
        {
            if (!string.IsNullOrWhiteSpace(from) || !string.IsNullOrWhiteSpace(to))
            {
                var fromDate = DateTime.Parse(from);
                var toDate = DateTime.Parse(to);

                var model = new List<ExpenseReportModel>();

                var monthsCount = GetMonthsCount(fromDate, toDate);
                var quartersCount = monthsCount / 3;
                if (monthsCount % 3 > 0)
                {
                    quartersCount++;
                }


                var quarter = GetQuarter(fromDate.Month);
                var quarterStartDate = GetQuarterStartDate(quarter, fromDate.Year, false);
                var quarterEndDate = GetQuarterEndDate(quarter, fromDate.Year);
                for (int i = 0; i < quartersCount; i++)
                {
                    var expenses = RapidVetUnitOfWork.ExpenseRepository.GetExpenses(CurrentUser.ActiveClinicId,
                                                                                    quarterStartDate, quarterEndDate, issuerId);
                    model.Add(new ExpenseReportModel()
                        {
                            Title = string.Format("{0} {1}", quarter.ToString(), quarterStartDate.Year),
                            Expenses = expenses.Select(AutoMapper.Mapper.Map<Expense, ExpenseIndexWebModel>).ToList()
                        });

                    var nextYear = quarter == YearQuarters.Q4;
                    quarter = GetNextQuarter(quarter);
                    quarterStartDate = GetQuarterStartDate(quarter, quarterStartDate.Year, nextYear);
                    quarterEndDate = GetQuarterEndDate(quarter, quarterStartDate.Year);

                }

                return View(model);
            }
            return View("Error");
        }


        private DateTime GetQuarterStartDate(YearQuarters quarter, int year, bool nextYear)
        {
            var result = new DateTime();
            switch (quarter)
            {
                case YearQuarters.Q1:
                    if (nextYear)
                    {
                        year = year + 1;
                    }
                    result = new DateTime(year, 1, 1);
                    break;
                case YearQuarters.Q2:
                    result = new DateTime(year, 4, 1);
                    break;
                case YearQuarters.Q3:
                    result = new DateTime(year, 7, 1);
                    break;
                case YearQuarters.Q4:
                    result = new DateTime(year, 10, 1);
                    break;
            }
            return result;
        }

        private DateTime GetQuarterEndDate(YearQuarters quarter, int year)
        {

            var result = new DateTime();
            switch (quarter)
            {
                case YearQuarters.Q1:
                    result = new DateTime(year, 4, 1);
                    break;
                case YearQuarters.Q2:
                    result = new DateTime(year, 7, 1);
                    break;
                case YearQuarters.Q3:
                    result = new DateTime(year, 10, 1);
                    break;
                case YearQuarters.Q4:
                    result = new DateTime(year + 1, 1, 1);
                    break;
            }
            return result;
        }




        private YearQuarters GetNextQuarter(YearQuarters quarter)
        {
            var result = YearQuarters.Q1;

            switch (quarter)
            {
                case YearQuarters.Q1:
                    result = YearQuarters.Q2;
                    break;
                case YearQuarters.Q2:
                    result = YearQuarters.Q3;
                    break;
                case YearQuarters.Q3:
                    result = YearQuarters.Q4;
                    break;
            }

            return result;
        }

        private YearQuarters GetQuarter(int month)
        {
            var q = YearQuarters.Q4;

            if (month > 0 && month < 4)
            {
                q = YearQuarters.Q1;
            }
            else if (month > 3 && month < 7)
            {
                q = YearQuarters.Q2;
            }
            else if (month > 7 && month < 10)
            {
                q = YearQuarters.Q3;
            }
            return q;
        }

        private int GetMonthsCount(DateTime from, DateTime to)
        {
            var result = ((to.Year - from.Year) * 12) + to.Month - from.Month;
            if (result < 0)
            {
                result = result * -1;
            }
            else if (result == 0)
            {
                result = 1;
            }
            return from.Year == to.Year && from.Month == to.Month ? result : result + 1;
        }
    }
}
