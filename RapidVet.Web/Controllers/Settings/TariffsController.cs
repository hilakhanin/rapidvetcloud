﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.RapidConsts.Controllers;
using RapidVet.Repository.Finances;
using RapidVet.WebModels.Finance;
using RapidVet.RapidConsts;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class TariffsController : BaseController
    {

        public PartialViewResult Index()
        {
            var tariffs = RapidVetUnitOfWork.TariffRepository.GetActiveTariffList(CurrentUser.ActiveClinicId);
            var model = tariffs.Select(AutoMapper.Mapper.Map<Tariff, TariffModel>).ToList();
            return PartialView("_Index", model);
        }

        [HttpGet]
        public PartialViewResult Create()
        {
            var model = new TariffModel()
                {
                    ClinicId = CurrentUser.ActiveClinicId,
                };
            return PartialView("_Create", model);
        }

        [HttpPost]
        public ActionResult Create(TariffModel model)
        {

            var tariff = AutoMapper.Mapper.Map<TariffModel, Tariff>(model);
            tariff.Id = 0;
            tariff.Active = true;
            tariff.CreatedById = UserId;
            tariff.CreatedDate = DateTime.Now;
            tariff.ClinicId = CurrentUser.ActiveClinicId;
            var status = RapidVetUnitOfWork.TariffRepository.Create(tariff);
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("Index");
        }


        // id is tariff id
        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            var tariff = RapidVetUnitOfWork.TariffRepository.GetTariff(id);
            var model = AutoMapper.Mapper.Map<Tariff, TariffModel>(tariff);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(TariffModel model)
        {
            var tariff = RapidVetUnitOfWork.TariffRepository.GetTariff(model.Id);
            AutoMapper.Mapper.Map(model, tariff);
            tariff.UpdatedById = UserId;
            tariff.UpdatedDate = DateTime.Now;
            var status = RapidVetUnitOfWork.TariffRepository.Update(tariff);
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("Index");

        }

        // id is tariff id
        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            var tariff = RapidVetUnitOfWork.TariffRepository.GetTariff(id);
            var model = AutoMapper.Mapper.Map<Tariff, TariffModel>(tariff);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public ActionResult Delete(int id, string name)
        {

            var tariff = RapidVetUnitOfWork.TariffRepository.GetTariff(id);
            tariff.Active = false;
            tariff.UpdatedById = UserId;
            tariff.UpdatedDate = DateTime.Now;
            var status = RapidVetUnitOfWork.Save();
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("Index");
        }


        public ActionResult TariffDDL(string elementId = "")
        {
            var tariffs = RapidVetUnitOfWork.TariffRepository.GetActiveTariffList(CurrentUser.ActiveClinicId);
            var model = tariffs.Select(AutoMapper.Mapper.Map<Tariff, TariffModel>).ToList();
            return PartialView("_TariffDDL", model);
        }
    }
}
