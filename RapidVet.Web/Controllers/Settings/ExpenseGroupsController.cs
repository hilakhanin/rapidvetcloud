﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using RapidVet.Model.Expenses;
using RapidVet.WebModels.Expenses;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class ExpenseGroupsController : BaseController
    {
        public ActionResult Index()
        {
            var groups =
                RapidVetUnitOfWork.ExpenseGroupRepository.GetClinicExpenseGroups(CurrentUser.ActiveClinicId).ToList();
            return View(groups);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new ExpenseGroupWebModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ExpenseGroupWebModel model)
        {
            if (ModelState.IsValid)
            {
                var expenseGroup = AutoMapper.Mapper.Map<ExpenseGroupWebModel, ExpenseGroup>(model);
                expenseGroup.ClinicId = CurrentUser.ActiveClinicId;
                var status = RapidVetUnitOfWork.ExpenseGroupRepository.Create(expenseGroup);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "ExpenseGroups");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var expenseGroup = RapidVetUnitOfWork.ExpenseGroupRepository.GetGroup(id);
            if (CurrentUser.ActiveClinicId == expenseGroup.ClinicId)
            {
                var model = AutoMapper.Mapper.Map<ExpenseGroup, ExpenseGroupWebModel>(expenseGroup);
                return View(model);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult Edit(ExpenseGroupWebModel model)
        {
            if (ModelState.IsValid)
            {
                var expenseGroup = RapidVetUnitOfWork.ExpenseGroupRepository.GetGroup(model.Id);
                if (expenseGroup.ClinicId == CurrentUser.ActiveClinicId)
                {
                    if (ModelState.IsValid)
                    {
                        Mapper.Map(model, expenseGroup);
                        expenseGroup.Updated = DateTime.Now;
                        var status = RapidVetUnitOfWork.Save();
                        if (status.Success)
                        {
                            SetSuccessMessage();
                        }
                        else
                        {
                            SetErrorMessage();
                        }
                        return RedirectToAction("Index", "ExpenseGroups");
                    }
                    return View(model);
                }
                throw new SecurityException();
            }
            return View(model);

        }

        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            var expenseGroup = RapidVetUnitOfWork.ExpenseGroupRepository.GetGroup(id);
            if (CurrentUser.ActiveClinicId == expenseGroup.ClinicId)
            {
                var model = AutoMapper.Mapper.Map<ExpenseGroup, ExpenseGroupWebModel>(expenseGroup);
                return PartialView("_Delete", model);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult Delete(int id, string name)
        {
            var expenseGroup = RapidVetUnitOfWork.ExpenseGroupRepository.GetGroup(id);
            if (expenseGroup.ClinicId == CurrentUser.ActiveClinicId)
            {

                var status = RapidVetUnitOfWork.ExpenseGroupRepository.Delete(expenseGroup);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "ExpenseGroups");

            }
            throw new SecurityException();
        }
    }
}
