﻿using System.Linq;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Clients.Details;
using System.Security;
using RapidVet.Model.Tools;
using System;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;
using RapidVet.XmlModels;
using System.Collections.ObjectModel;
using RapidVet.FilesAndStorage;
using Ionic.Zip;
using System.Net.Mime;
using System.Threading;
using System.Net.Mail;
using Hangfire;
using RestSharp;
using RapidVet.WebModels.Clinics;

namespace RapidVet.Web.Controllers.Settings
{
    public class AdminController : BaseController
    {
        public ActionResult GeneralMessage()
        {
            if (CurrentUser.IsAdministrator)
            {
                var generalMessage = RapidVetUnitOfWork.AdminRepository.GetGeneralSettings();
                if (generalMessage == null)
                {
                    generalMessage = new GeneralSettings();
                }

                return View(generalMessage);
            }

            throw new SecurityException();
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(GeneralSettings model)
        {
            OperationStatus status;
            //Create 
            if (model.Id == 0)
            {
                status = RapidVetUnitOfWork.AdminRepository.Create(model);
            }
            //Update
            else
            {
                status = RapidVetUnitOfWork.AdminRepository.Update(model);
            }

            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }


        public ActionResult MoveRecieptsToAnotherIssuer(int? issuerId, string fromTime)
        {
            DateTime from;

            if (string.IsNullOrWhiteSpace(fromTime))
            {
                var now = DateTime.Now;
                from = new DateTime(now.Year, now.Month, 1);
            }
            else
            {
                from = DateTime.MinValue;
                var format = "dd/MM/yyyy";
                var unparsedFrom = fromTime;
                DateTime.TryParseExact(unparsedFrom, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out from);
                if (from == DateTime.MinValue)
                {
                    from = DateTime.Now;
                }
            }

            if (!issuerId.HasValue)
            {
                issuerId = (RapidVetUnitOfWork.IssuerRepository.GetList(CurrentUser.ActiveClinicId, CurrentUser.Id).OrderBy(i => i.Id).First()).Id;
            }

            var model = RapidVetUnitOfWork.AdminRepository.GetPaymentsWithoutInvoice(CurrentUser.ActiveClinicId, from, issuerId.Value);

            foreach (var item in model)
            {
                if (string.IsNullOrWhiteSpace(item.Recipt.ClientName))
                {
                    item.Recipt.ClientName = Resources.Migration.TempClient;
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult MoveRecieptsToAnotherIssuer(int[] paymentsIds, int newIssuerId)
        {
            int completed = 0;
            bool vaild = true;
            string message = "";

            if (paymentsIds == null)
            {
                message = Resources.Admin.MoveReciepstNoItems;
            }
            else
            {
                foreach (var itemId in paymentsIds)
                {
                    var payment = RapidVetUnitOfWork.FinanceDocumentReposetory.GetFinancePayment(itemId);
                    if (RapidVetUnitOfWork.FinanceDocumentReposetory.IsDocumentSerialIssuedToIssuer(newIssuerId, payment.Recipt.SerialNumber))
                    {
                        vaild = false;
                        break;
                    }
                }

                if (!vaild)
                {
                    message = Resources.Admin.MoveReciepstFailure;
                }
                else
                {
                    completed = RapidVetUnitOfWork.AdminRepository.PaymentsToNewIssuer(paymentsIds, newIssuerId);
                    message = Resources.Admin.MoveReciepstSuccess;
                }
            }
            return View("ActionResults", model: message);
        }

        public ActionResult UnitePreventiveItems()
        {
            var model = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveMedicineItemsByClinic(CurrentUser.ActiveClinicId);

            return View(model);
        }

        [HttpPost]
        public ActionResult UnitePreventiveItems(int[] priceListItemIds, int priceListItemMasterId)
        {
            string message = Resources.Admin.UnitePreventiveItemsSuccess;

            if (priceListItemIds == null)
            {
                message = Resources.Admin.UnitePreventiveItemsNoItems;
            }
            else
            {
                if (priceListItemIds.Any(p => p.Equals(priceListItemMasterId)))
                {
                    priceListItemIds = priceListItemIds.Where(p => !p.Equals(priceListItemMasterId)).ToArray();
                }

                foreach (var itemId in priceListItemIds)
                {
                    RapidVetUnitOfWork.PreventiveMedicineRepository.ChangePriceListItemIdInAllItems(itemId, priceListItemMasterId);
                    RapidVetUnitOfWork.VaccineAndTreatmentRepository.UpdateVaccineByPriceListItem(itemId, priceListItemMasterId);
                    RapidVetUnitOfWork.PriceListRepository.ChangePriceListItemInAllTablesAndRemoveIt(itemId, priceListItemMasterId);
                }
            }

            return View("ActionResults", model: message);
        }

        public ActionResult ExportClinic()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAllClinicGroups()
        {

            var clinicGroups = RapidVetUnitOfWork.ClinicGroupRepository.GetAllActive().ToList();

            return Json(clinicGroups, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StorageUsageReport()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetStorageUsage(bool showAll = true)
        {
            var clinicGroups = RapidVetUnitOfWork.ClinicGroupRepository.GetAllActive().ToList();
            List<StorageUsage> clinicsStorageUsage = new List<StorageUsage>();

            foreach (var group in clinicGroups)
            {
                var clinics = RapidVetUnitOfWork.ClinicGroupRepository.GetClinicList(group.Id).ToList();

                foreach (var clinic in clinics)
                {
                    double usageSize;
                    double usagePercent = GetArchiveUsagePercent(clinic.Id, out usageSize);
                    if (showAll || usagePercent >= ArchivesController.MAX_UPLOAD_PERCENTAGE)
                    {
                        clinicsStorageUsage.Add(new StorageUsage()
                        {
                            Allocation = clinic.ArchiveAllocation + "GB",
                            ClinicGroup = group.Name,
                            ClinicName = clinic.Name,
                            UsagePercent = usagePercent,
                            UsageSize = usageSize.ToString("F") + "GB",
                        });
                    }
                }
            }

            return Json(clinicsStorageUsage.OrderByDescending(p => p.UsagePercent), JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmailsUsageReport()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetEmailsUsage(int month, int year, bool showAll = true)
        {
            var fromDate = new DateTime(year, month, 1);
            var clinicGroups = RapidVetUnitOfWork.ClinicGroupRepository.GetAllActive().ToList();
            List<EmailsUsage> clinicsEmailsUsage = new List<EmailsUsage>();

            foreach (var group in clinicGroups)
            {
                var clinics = RapidVetUnitOfWork.ClinicGroupRepository.GetClinicList(group.Id).ToList();

                foreach (var clinic in clinics)
                {
                    var messagesCount = RapidVetUnitOfWork.MessageDistributionRepository.MessagesSentInMonth(clinic.Id, fromDate);
                    if (showAll || (messagesCount != null && messagesCount.EmailsCount > ClinicsController.EMAILS_NO_CHARGE_NUMBER))
                    {

                        clinicsEmailsUsage.Add(new EmailsUsage()
                        {
                            ClinicGroup = group.Name,
                            ClinicName = clinic.Name,
                            EmailsCount = messagesCount != null ? messagesCount.EmailsCount : 0,
                            SmsCount = messagesCount != null ? messagesCount.SmsCount : 0
                        });

                    }
                }
            }

            return Json(clinicsEmailsUsage.OrderByDescending(p => p.EmailsCount), JsonRequestBehavior.AllowGet);
        }

        public ActionResult RecalculateBalance()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RecalculateBalance([Bind(Prefix = "id")] string taskId)
        {
            ProgressHub.NotifyStart(taskId);
            var successMessage = Resources.Admin.RecalculateBalanceSuccess;
            var ClinicId = CurrentUser.ActiveClinicId;
            var ClinicClients = RapidVetUnitOfWork.ClinicRepository.GetClinicClients(ClinicId);
            var ClientsIndex = ClinicClients.Count();
            decimal progress = 0;

            for (var i = 0; i < ClientsIndex; i++)
            {
                var Client = ClinicClients[i];
                RapidVetUnitOfWork.ClientRepository.UpdateClientBalance(Client);
                progress = (decimal)i / ClientsIndex;

                ProgressHub.NotifyProgress(taskId, (int)Math.Ceiling(progress * 100));

                if (i % 100 == 0)
                {
                    RapidVetUnitOfWork.Save();
                }
            }
            RapidVetUnitOfWork.Save();
            ProgressHub.NotifyProgress(taskId, 100);

            return View("ActionResults", model: successMessage);
        }

        public ActionResult RecalculatePatientNames()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RecalculatePatientNames([Bind(Prefix = "id")] string taskId)
        {
            ProgressHub.NotifyStart(taskId);
            var successMessage = Resources.Admin.RecalculatePatientNamesSuccess;
            var ClinicId = CurrentUser.ActiveClinicId;
            var ClinicClients = RapidVetUnitOfWork.ClinicRepository.GetClinicClients(ClinicId);
            var ClientsIndex = ClinicClients.Count();
            decimal progress = 0;

            for (var i = 0; i < ClientsIndex; i++)
            {
                var Client = ClinicClients[i];
                RapidVetUnitOfWork.ClientRepository.SetClientNameWithPatients(Client.Id, true);
                progress = (decimal)i / ClientsIndex;

                ProgressHub.NotifyProgress(taskId, (int)Math.Ceiling(progress * 100));

                if (i % 100 == 0)
                {
                    RapidVetUnitOfWork.Save();
                }

            }
            RapidVetUnitOfWork.Save();
            ProgressHub.NotifyProgress(taskId, 100);

            return View("ActionResults", model: successMessage);
        }

        [HttpGet]
        public JsonResult GetClinicsOfClinicGroup(string clinicGroup)
        {
            int clinicGroupId;
            if (!int.TryParse(clinicGroup, out clinicGroupId))
            {
                clinicGroupId = -1;
            }

            var clinics = RapidVetUnitOfWork.ClinicRepository.GetClearList(clinicGroupId).ToList();

            return Json(clinics, JsonRequestBehavior.AllowGet);
        }

        static string archivePath = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_PATH"];

        [HttpPost]
        public ActionResult ExportClinic(int clinicId, string notifyEmail)
        {
            string message = "";

            string hostUrl = HttpContext.Request.Url.ToString().Substring(0, HttpContext.Request.Url.ToString().IndexOf('/', 8));

            var generalSettings = RapidVetUnitOfWork.AdminRepository.GetGeneralSettings();
            if (generalSettings == null)
            {
                generalSettings = new GeneralSettings();
                RapidVetUnitOfWork.AdminRepository.Create(generalSettings);
            }

            if (!generalSettings.IsExportingClinic)
            {
                generalSettings.IsExportingClinic = true;
                generalSettings.ClinicId = clinicId;
                generalSettings.ExportStartTime = DateTime.Now;
                RapidVetUnitOfWork.AdminRepository.Update(generalSettings);
                message = Resources.Admin.ExportClinicStart;
                int pageSize = int.Parse(System.Configuration.ConfigurationManager.AppSettings["CLIENTS_TO_EXPORT"]);
                var clinicGroupBusinesEmail = CurrentUser.ActiveClinicGroup.EmailBusinessName;
                var clinicGroupEmail = CurrentUser.ActiveClinicGroup.EmailAddress;
                var userId = CurrentUser.Id;
                BackgroundJob.Enqueue(() => exportClinicFiles(clinicId, pageSize, hostUrl, notifyEmail, clinicGroupBusinesEmail, clinicGroupEmail, Server.MapPath("~/"), userId));
            }
            else
            {
                message = Resources.Admin.ExportClinicInProgress;
            }

            return View("ExportClinicMessage", model: message);
        }

        public void exportClinicFiles(int clinicId, int pageSize, string hostUrl, string notifyEmail, string clinicGroupBusnEmail, string clinicGroupEmail, string savePath, int userId)
        {
            string emailMessage = "";
            Model.Clinics.Clinic clinic = null;
            var CasheFiles = Path.Combine(archivePath, "CasheFileStreamFiles");
            if (!Directory.Exists(CasheFiles))
                Directory.CreateDirectory(CasheFiles);

            try
            {
                clinic = RapidVetUnitOfWork.ClinicRepository.GetClinicWithLabsData(clinicId);
                int clientsCount = RapidVetUnitOfWork.ClinicRepository.GetClinicClientsCount(clinicId);

                // for (var i = 0; i < 1; i++)
                for (var i = 0; i < clientsCount / pageSize + 1; i++)
                {
                    var clients = new List<Model.Clients.Client>();
                    // clinic.Clients.Clear();
                    clients.AddRange(RapidVetUnitOfWork.ClinicRepository.GetClinicClientsForExport(clinicId, i, pageSize));

                    if (clients.Any())
                    {
                        foreach (var c in clients)
                        {
                            foreach (var doc in c.FinanceDocuments)
                            {
                                var payments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetPaymentsForDocument(doc);
                                doc.Payments = payments != null ? payments.ToList() : null;
                            }
                        }
                    }
                    clinic.Clients = clients;
                    var xmlClients = AutoMapper.Mapper.Map<RapidVet.Model.Clinics.Clinic, RapidVet.XmlModels.Clinic>(clinic);
                    var xs = new System.Xml.Serialization.XmlSerializer(typeof(RapidVet.XmlModels.Clinic));
                    using (FileStream fs = System.IO.File.Create(Path.Combine(CasheFiles, Guid.NewGuid().ToString())))
                    {
                        xs.Serialize(fs, xmlClients);
                        ArchivesUploadHelper.SaveExportFileInClinicGroupFolder(clinic.ClinicGroupID, clinic.Id, string.Format("clients{0}.xml", (i + 1).ToString()), archivePath, fs, false);
                    }
                }

                using (var strm = System.IO.File.Create(Path.Combine(CasheFiles, Guid.NewGuid().ToString())))
                {
                    using (var zip = new ZipFile())
                    {
                        var files = ArchivesUploadHelper.GetXmlFileList(clinic.ClinicGroupID, clinic.Id, archivePath);
                        zip.AddFiles(files, "Clinic");
                        zip.Save(strm);
                        strm.Seek(0, SeekOrigin.Begin);
                        ArchivesUploadHelper.DeleteXmlFiles(clinic.ClinicGroupID, clinic.Id, archivePath);
                        ArchivesUploadHelper.SaveExportFileInClinicGroupFolder(clinic.ClinicGroupID, clinic.Id, "clinic.zip", savePath, strm, true);

                        StringBuilder downloadPath = new StringBuilder();
                        downloadPath.Append(hostUrl);
                        downloadPath.Append(@"/ClinicExportFiles/");
                        downloadPath.Append(clinic.ClinicGroupID);
                        downloadPath.Append(@"/");
                        downloadPath.Append(clinic.Id);
                        downloadPath.Append(@"/clinic.zip");

                        emailMessage = string.Format(Resources.Admin.ExportEmailSuccessBody, downloadPath);
                        sendNotification(notifyEmail, emailMessage, clinicGroupBusnEmail, clinicGroupEmail, userId, clinic);
                    }
                }
            }
            catch (Exception ex)
            {
                emailMessage = Resources.Admin.ExportEmailFailureBody;
                sendNotification(notifyEmail, emailMessage, clinicGroupBusnEmail, clinicGroupEmail, userId, clinic);
                var logPath = System.Configuration.ConfigurationManager.AppSettings["LOG_PATH"];
                if (logPath != null)
                    BaseLogger.LogWriter.WriteExPath(ex, logPath);
            }
            finally
            {
                Thread th = new Thread(new ParameterizedThreadStart(deleteFileStreamFiles));
                th.SetApartmentState(ApartmentState.MTA);
                th.Start(CasheFiles);
            }
        }

        private void deleteFileStreamFiles(object obj)
        {
            try
            {
                if (obj == null || !Directory.Exists(obj.ToString()))
                    return;

                foreach (var f in Directory.GetFiles(obj.ToString()))
                {
                    var fi = new FileInfo(f);
                    if (fi == null || fi.CreationTime > DateTime.Now.Date)
                        continue;

                    try
                    {
                        System.IO.File.SetAttributes(f, FileAttributes.Normal);
                        System.IO.File.Delete(f);
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }
        }

        string emailTemplatePrefix = "<div style=\"position:relative;display:block;margin:auto;direction:rtl;\">";
        string emailTemplatePostfix = "</div>";

        private void sendNotification(string notifyEmail, string emailMessage, string clinicGroupBusnEmail, string clinicGroupEmail, int userId, Model.Clinics.Clinic selectedClinic)
        {
            var generalSettings = RapidVetUnitOfWork.AdminRepository.GetGeneralSettings();
            var clinic = RapidVetUnitOfWork.ClinicRepository.GetClinic(generalSettings.ClinicId);
            generalSettings.IsExportingClinic = false;
            //generalSettings.ExportStartTime = null;
            generalSettings.ClinicId = 0;
            // generalSettings.ExportNotifyEmail = "";

            RapidVetUnitOfWork.AdminRepository.Update(generalSettings);

            var mailClient = getMailClient();
            string from = string.Format("{0}<{1}>", !string.IsNullOrWhiteSpace(clinicGroupBusnEmail) ? clinicGroupBusnEmail : selectedClinic.Name,
                                                clinicGroupEmail);

            RestRequest request = new RestRequest();
            request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_RAPID_DOMAIN"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", from);
            request.AddParameter("subject", Resources.Admin.ExportEmailSubject);
            request.AddParameter("html", emailTemplatePrefix + emailMessage + emailTemplatePostfix);
            request.AddParameter("to", notifyEmail);
            request.Method = Method.POST;
            var message = mailClient.Execute(request);

            if ((int)message.StatusCode == 200)
            {
                string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                     Resources.Auditing.SendingModule, Resources.Auditing.ExportClinic,
                                                     Resources.Auditing.ClientName, clinic.Name,
                                                     Resources.Auditing.Address, notifyEmail,
                                                     Resources.Auditing.MailSubject, Resources.Admin.ExportEmailSubject,
                                                     Resources.Auditing.ReturnCode, (int)message.StatusCode + " - " + message.StatusCode);

                RapidVetUnitOfWork.AuditingRepository.Audit(selectedClinic.Id, userId, Resources.Auditing.MailSent, logMessage, "");
            }
            //RapidVetUnitOfWork.ClinicGroupRepository.SendEmailMessages(1, messageList);
        }
    }
}