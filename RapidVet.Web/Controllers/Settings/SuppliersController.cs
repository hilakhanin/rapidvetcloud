﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model.Expenses;
using RapidVet.WebModels.Expenses;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class SuppliersController : BaseController
    {

        public ActionResult Index()
        {
            var suppliers =
                RapidVetUnitOfWork.SupplierRepository.GetClinicSuppliers(CurrentUser.ActiveClinicId).ToList();
            return View(suppliers);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new SupplierWebModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(SupplierWebModel model)
        {
            if (ModelState.IsValid)
            {
                var supplier = AutoMapper.Mapper.Map<SupplierWebModel, Supplier>(model);
                supplier.ClinicId = CurrentUser.ActiveClinicId;
                var status = RapidVetUnitOfWork.SupplierRepository.Create(supplier);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "Suppliers");
            }
            return View(model);
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            var supplier = RapidVetUnitOfWork.SupplierRepository.GetSupplier(id);
            if (supplier.ClinicId == CurrentUser.ActiveClinicId)
            {
                var model = AutoMapper.Mapper.Map<Supplier, SupplierWebModel>(supplier);
                return View(model);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult Edit(SupplierWebModel model)
        {
            var supplier = RapidVetUnitOfWork.SupplierRepository.GetSupplier(model.Id);
            if (supplier.ClinicId == CurrentUser.ActiveClinicId)
            {
                if (ModelState.IsValid)
                {
                    AutoMapper.Mapper.Map(model, supplier);
                    supplier.Updated = DateTime.Now;
                    var status = RapidVetUnitOfWork.Save();
                    if (status.Success)
                    {
                        SetSuccessMessage();
                    }
                    else
                    {
                        SetErrorMessage();
                    }
                    return RedirectToAction("Index", "Suppliers");
                }

                return View(model);
            }
            throw new SecurityException();
        }

        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            var supplier = RapidVetUnitOfWork.SupplierRepository.GetSupplier(id);
            if (supplier.ClinicId == CurrentUser.ActiveClinicId)
            {
                var model = AutoMapper.Mapper.Map<Supplier, SupplierWebModel>(supplier);
                return PartialView("_Delete", model);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult Delete(int id, string name)
        {
            var supplier = RapidVetUnitOfWork.SupplierRepository.GetSupplier(id);
            if (supplier.ClinicId == CurrentUser.ActiveClinicId)
            {

                var status = RapidVetUnitOfWork.SupplierRepository.Delete(supplier);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "Suppliers");

            }
            throw new SecurityException();
        }

        public JsonResult GetClinicSuppliers()
        {
            var suppliers = RapidVetUnitOfWork.SupplierRepository.GetClinicSuppliers(CurrentUser.ActiveClinicId);
            var model = suppliers.Select(AutoMapper.Mapper.Map<Supplier, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
