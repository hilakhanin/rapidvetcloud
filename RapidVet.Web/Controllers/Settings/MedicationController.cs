﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model.Visits;
using RapidVet.Repository;
using RapidVet.WebModels.Medications;

namespace RapidVet.Web.Controllers.Settings
{
    [Authorize]
    [ClinicGroupManager]
    public class MedicationController : BaseController
    {

        //
        // GET: /Medication/

        public ViewResult Index(bool print = false)
        {
            var medications =
                RapidVetUnitOfWork.MedicationRepository.GetClinicGroupMedications(CurrentUser.ActiveClinicGroupId);
            ViewBag.Print = print;
            return View(medications.ToList());
        }

        /// <summary>
        /// Get Action of Creat new Medication in the active clinic Group
        /// </summary>
        /// <returns>Eampty Creat Medication Modal</returns>
        public ActionResult Create()
        {
            return View();
        }


        /// <summary>
        /// Post Action of Creat new Medication in the active clinic Group
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Redirect to medication Index</returns>
        [HttpPost]
        public ActionResult Create(MedicationEditWebModel model)
        {
            if (ModelState.IsValid)
            {
                var medication = AutoMapper.Mapper.Map<MedicationEditWebModel, Medication>(model);
                medication.ClinicGroupId = CurrentUser.ActiveClinicGroupId;
                var status = RapidVetUnitOfWork.MedicationRepository.AddMedication(medication);
                if (status.Success)
                {
                    SetSuccessMessage();
                    return RedirectToAction("Index", "Medication");
                }
                else
                {
                    SetErrorMessage();
                    return View("Error");
                }
            }
            return View(model);
        }

        [HttpPost]
        public JsonResult CreateFromVisit(MedicationEditWebModel model)
        {
            var medication = AutoMapper.Mapper.Map<MedicationEditWebModel, Medication>(model);
            medication.ClinicGroupId = CurrentUser.ActiveClinicGroupId;
            var status = RapidVetUnitOfWork.MedicationRepository.AddMedication(medication);
            if (status.Success)
            {
                status.ItemId = medication.Id;
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }

            return Json(status);
        }

        /// <summary>
        /// returns edit view of medication
        /// </summary>
        /// <param name="id">medicationId</param>
        /// <returns>ActionResult</returns>
        public ActionResult Edit(int id)
        {
            var medication = RapidVetUnitOfWork.MedicationRepository.GetMedication(id);
            if (medication.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException();
            }
            var model = AutoMapper.Mapper.Map<Medication, MedicationEditWebModel>(medication);
            return View(model);
        }


        /// <summary>
        /// handles post for previous actionResult
        /// </summary>
        /// <param name="model">MedicationEditWebModel</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult Edit(MedicationEditWebModel model)
        {
            if (ModelState.IsValid)
            {
                var medication = RapidVetUnitOfWork.MedicationRepository.GetMedication(model.Id);
                if (medication.ClinicGroupId == CurrentUser.ActiveClinicGroupId)
                {
                    AutoMapper.Mapper.Map(model, medication);
                    medication.Updated = DateTime.Now;
                    var status = RapidVetUnitOfWork.MedicationRepository.Save(medication);
                    if (status.Success)
                    {
                        SetSuccessMessage();
                    }
                    else
                    {
                        SetErrorMessage();
                    }
                    if (status.Success)
                    {
                        return RedirectToAction("Index", "Medication");
                    }
                    return View("Error");
                }
                throw new SecurityException();
            }
            return View(model);
        }


        /// <summary>
        /// gets delete view for medication
        /// </summary>
        /// <param name="id">medicationId</param>
        /// <returns>PartialViewResult</returns>
        public PartialViewResult Delete(int id)
        {
            var medication = RapidVetUnitOfWork.MedicationRepository.GetMedication(id);
            if (medication.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException();
            }
            var model = AutoMapper.Mapper.Map<Medication, MedicationEditWebModel>(medication);
            return PartialView("_Delete", model);
        }

        /// <summary>
        /// handles post event for previous function
        /// </summary>
        /// <param name="id">medicationId</param>
        /// <param name="name">medication name</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult Delete(int id, string name)
        {
            var medication = RapidVetUnitOfWork.MedicationRepository.GetMedication(id);
            if (medication.ClinicGroupId == CurrentUser.ActiveClinicGroupId)
            {
                medication.Active = false;
                var status = RapidVetUnitOfWork.MedicationRepository.Save(medication);
                if (status.Success)
                {
                    return RedirectToAction("Index", "Medication");
                }
                return View("Error");
            }
            throw new SecurityException();
        }


    }
}