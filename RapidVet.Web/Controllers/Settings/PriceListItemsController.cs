﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.RapidConsts.Controllers;
using RapidVet.Repository.Clinics;
using RapidVet.Repository.Finances;
using RapidVet.WebModels.Finance;
using RapidVet.Enums;

namespace RapidVet.Web.Controllers
{
    public class PriceListItemsController : BaseController
    {


        public ActionResult Main()
        {
            return View("MainPriceLists");
        }

        public PartialViewResult Index()
        {
            var categories = RapidVetUnitOfWork.CategoryRepository.GeIdList(CurrentUser.ActiveClinicId);
            return PartialView("_Index", categories);
        }


        public PartialViewResult CategoryColumn(int id)
        {
            var category = RapidVetUnitOfWork.CategoryRepository.GetSimpleItem(id);
            var model = AutoMapper.Mapper.Map<PriceListCategory, SimpleCategoryModel>(category);
            return PartialView("_CategoryColumn", model);
        }

        // id is category id
        public ActionResult CategoryItemsView(int id, bool displayInactiveItems = false)
        {
            var items = RapidVetUnitOfWork.ItemRepository.GetList(id, displayInactiveItems);
            var tariffs = RapidVetUnitOfWork.TariffRepository.GetActiveTariffList(CurrentUser.ActiveClinicId);
            var model = new PriceListModel
            {
                Items = items.Select(AutoMapper.Mapper.Map<PriceListItem, PriceListItemModel>).ToList(),
                Tariffs = tariffs.Select(AutoMapper.Mapper.Map<Tariff, GridTariffModel>).ToList()
            };

            foreach (var item in model.Items)
            {
                if (item.ItemsTariffs == null)
                {
                    item.ItemsTariffs = new List<ItemTariffGridModel>();
                }

                foreach (var tariff in model.Tariffs)
                {
                    //var price = item.ItemsTariffs.SingleOrDefault(p => p.TariffId == tariff.Id);
                    var price = item.ItemsTariffs.FirstOrDefault(p => p.TariffId == tariff.Id);
                    if (price == null)
                    {
                        price = new ItemTariffGridModel()
                            {
                                ItemId = item.Id,
                                TariffId = tariff.Id,
                                TariffName = tariff.Name
                            };
                        item.ItemsTariffs.Add(price);
                    }
                }

                item.ItemsTariffs.OrderBy(p => p.TariffName);
            }
            ViewBag.CategoryName = RapidVetUnitOfWork.CategoryRepository.GetName(id);
            ViewBag.DisplayInactiveItems = displayInactiveItems;
            return View(model);
        }

        //id is categoryId
        [HttpGet]
        public PartialViewResult Create(int id)
        {
            var model = new PriceListItemModel()
                {
                    ClinicId = CurrentUser.ActiveClinicId,
                    CategoryId = id,
                    Available = true
                };
            return PartialView("_Create", model);
        }

        [HttpPost]
        public ActionResult Create(PriceListItemModel model)
        {

            if (ModelState.IsValid)
            {
                model.Id = 0;
                var item = AutoMapper.Mapper.Map<PriceListItemModel, PriceListItem>(model);
                item.Available = true;
                item.CreatedById = CurrentUser.Id;
                item.CreatedDate = DateTime.Now;

                var status = RapidVetUnitOfWork.ItemRepository.Create(item);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("CategoryItemsView", "PriceListItems", new { id = model.CategoryId });
        }

        //id is itemId
        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            var item = RapidVetUnitOfWork.ItemRepository.GetItem(id);
            var model = AutoMapper.Mapper.Map<PriceListItem, PriceListItemModel>(item);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(PriceListItemModel model)
        {
            var result = new JsonResult();
            model.ClinicId = CurrentUser.ActiveClinicId;
            var item = RapidVetUnitOfWork.ItemRepository.GetItem(model.Id);
            var originalCategoryId = item.CategoryId;

            if (ModelState.IsValid)
            {

                if (model.ItemTypeId != item.ItemTypeId && (item.ItemTypeId == (int)PriceListItemTypeEnum.Vaccines || item.ItemTypeId == (int)PriceListItemTypeEnum.PreventiveTreatments)) 
                {
                    if (item.VaccineOrTreatment.Any())
                    {
                        RapidVetUnitOfWork.VaccineAndTreatmentRepository.SetInactiveByPriceListItemId(item.Id);
                    }
                }

                AutoMapper.Mapper.Map(model, item);
                item.UpdatedById = CurrentUser.Id;
                item.UpdatedDate = DateTime.Now;

                if(!item.Available)
                {
                    if (item.VaccineOrTreatment.Any())
                    {
                        RapidVetUnitOfWork.VaccineAndTreatmentRepository.SetInactiveByPriceListItemId(item.Id);
                    }
                }

                var status = RapidVetUnitOfWork.ItemRepository.Update(item);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("CategoryItemsView", "PriceListItems", new { id = originalCategoryId });
        }

        //id is itemId
        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            var item = RapidVetUnitOfWork.ItemRepository.GetItem(id);
            var model = AutoMapper.Mapper.Map<PriceListItem, PriceListItemModel>(item);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public ActionResult Delete(int id, string name)
        {
            var result = new JsonResult();
            var item = RapidVetUnitOfWork.ItemRepository.GetItem(id);
            item.Available = false;
            item.UpdatedById = CurrentUser.Id;
            item.UpdatedDate = DateTime.Now;

            if(item.VaccineOrTreatment.Any())
            {
                RapidVetUnitOfWork.VaccineAndTreatmentRepository.SetInactiveByPriceListItemId(item.Id);
            }

            var status = RapidVetUnitOfWork.ItemRepository.Update(item);
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("CategoryItemsView", "PriceListItems", new { id = item.CategoryId });
        }

        [HttpPost]
        public JsonResult PriceUpdate(ItemTariffGridModel model)
        {
            var result = new JsonResult();
            if (model.ItemId > 0 && model.TariffId > 0)
            {
                var itemTariff = AutoMapper.Mapper.Map<ItemTariffGridModel, PriceListItemTariff>(model);

                itemTariff.Price = 0;
                if (model.Price.HasValue)
                {
                    itemTariff.Price = model.Price.Value;
                }

                var status = RapidVetUnitOfWork.ItemRepository.UpdatePrice(itemTariff);
            }
            else
            {
                result.Data = new { success = false };
            }
            return result;
        }



        [HttpGet]
        public PartialViewResult PriceShift()
        {
            var model = new PriceShiftModel();
            return PartialView("_PriceShift", model);
        }

        [HttpPost]
        public JsonResult PriceShift(PriceShiftModel model)
        {
            var result = new JsonResult();

            if (ModelState.IsValid)
            {
                var status = RapidVetUnitOfWork.PriceListRepository.ShiftPrices(model.TariffId, model.PriceShiftMethod,
                                                              model.PriceShiftAction, model.Amount);
                result.Data = new
                    {
                        success = string.IsNullOrWhiteSpace(status.Message),
                        target = Url.Action("Index", "PriceListItems"),//Url.Action("Index", new { id = CurrentUser.ActiveClinicId }),
                        indexId = PriceListItemsConsts.PRICE_LIST_ITEMS_CONTAINER_ID,//PRICE_LIST_ITEMS_GRID_ID,
                        modalId = TariffConsts.TARIFF_MODAL_ID
                    };

            }
            else
            {
                result.Data = new
                    {
                        success = false,
                        data = RenderPartialViewToString("_PriceShift", model)
                    };
            }
            return result;
        }

        [HttpGet]
        public PartialViewResult DuplicatePricesTariff()
        {
            var model = new DuplicatePricesTariffModel();
            return PartialView("_DuplicatePricesTariff", model);
        }

        [HttpPost]
        public JsonResult DuplicatePricesTariff(DuplicatePricesTariffModel tariffModel, int? selectedCategoryId)
        {
            var result = new JsonResult();
            if (ModelState.IsValid)
            {
                var status = RapidVetUnitOfWork.PriceListRepository.DuplicatePricesBetweenTariffs(tariffModel.SourceTariffId,
                                                                                tariffModel.DestinationTariffId);
                result.Data = new
                    {
                        success = string.IsNullOrWhiteSpace(status.Message),
                        target = Url.Action("Index", "PriceListItems"),//Url.Action("Index", new { id = CurrentUser.ActiveClinicId }),
                        indexId = PriceListItemsConsts.PRICE_LIST_ITEMS_CONTAINER_ID,//PRICE_LIST_ITEMS_GRID_ID,
                        modalId = TariffConsts.TARIFF_MODAL_ID
                    };
                SetSuccessMessage();
            }
            else
            {
                result.Data = new
                    {
                        success = false,
                        data = RenderPartialViewToString("_DuplicatePrices", tariffModel)
                    };
                SetErrorMessage();
            }
            return result;
        }

        [HttpGet]
        public PartialViewResult DuplicatePricesClinic()
        {
            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);
            var model = (from clinic in clinicGroup.Clinics
                         where clinic.Id != CurrentUser.ActiveClinicId
                         select AutoMapper.Mapper.Map<Clinic, DuplicatePricesClinicModel>(clinic)).ToList();
            return PartialView("_DuplicatePricesClinic", model);
        }

        [HttpPost]
        public JsonResult DuplicatePricesClinic(string optional = "")
        {
            var targetClinicIds = GetTargetClinics();
            var status = RapidVetUnitOfWork.PriceListRepository.DuplicatePricesBetweenClinics(CurrentUser.ActiveClinicId, targetClinicIds,
                                                                            CurrentUser.Id);
            var result = new JsonResult();
            if (status.Success)
            {
                result.Data = new
                    {
                        success = true,
                        modalId = TariffConsts.TARIFF_MODAL_ID
                    };
            }
            return result;
        }

        private List<int> GetTargetClinics()
        {
            return (from key in Request.Form.AllKeys
                    where key.StartsWith("clinic")
                    let id = key.Split('_')[1]
                    where Request.Form[key].Split(',')[0] == "true"
                    select int.Parse(id)).ToList();
        }

        public ActionResult Categories()
        {
            return View();
        }

        public JsonResult CategoriesJson()
        {
            var categories =
                RapidVetUnitOfWork.CategoryRepository.GetClinicActiveCategories(CurrentUser.ActiveClinicId).ToList();
            return Json(categories, JsonRequestBehavior.AllowGet);
        }

    }
}