﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model.Visits;
using RapidVet.Repository.Visits;
using RapidVet.WebModels.Visits;

namespace RapidVet.Web.Controllers
{
    public class DiagnosesController : BaseController
    {


        public ActionResult Index()
        {
            var diagnoses = RapidVetUnitOfWork.DiagnosisRepository.GetList(CurrentUser.ActiveClinicId);
            var model = diagnoses.Select(AutoMapper.Mapper.Map<Diagnosis, DiagnosisAdminModel>);
            return View(model);
        }

        //id is clinicId
        [HttpGet]
        public PartialViewResult Create()
        {
            var model = new DiagnosisAdminModel();
            return PartialView("_Create", model);
        }

        [HttpPost]
        public ActionResult Create(DiagnosisAdminModel model)
        {

            var diagnosis = AutoMapper.Mapper.Map<DiagnosisAdminModel, Diagnosis>(model);
            diagnosis.ClinicId = CurrentUser.ActiveClinicId;
            diagnosis.Active = true;
            diagnosis.Id = 0;
            var status = RapidVetUnitOfWork.DiagnosisRepository.Create(diagnosis);
            return RedirectToAction("Index", "Diagnoses");
        }

        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            var diagnosis = RapidVetUnitOfWork.DiagnosisRepository.GetItem(id);
            if (diagnosis.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<Diagnosis, DiagnosisAdminModel>(diagnosis);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(DiagnosisAdminModel model)
        {
            var diagnosis = RapidVetUnitOfWork.DiagnosisRepository.GetItem(model.Id);
            if (diagnosis.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            AutoMapper.Mapper.Map(model, diagnosis);
            var status = RapidVetUnitOfWork.DiagnosisRepository.Update(diagnosis);
            return RedirectToAction("Index", "Diagnoses");
        }

        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            var diagnosis = RapidVetUnitOfWork.DiagnosisRepository.GetItem(id);
            if (diagnosis.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<Diagnosis, DiagnosisAdminModel>(diagnosis);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public ActionResult Delete(DiagnosisAdminModel model)
        {
            var diagnosis = RapidVetUnitOfWork.DiagnosisRepository.GetItem(model.Id);
            if (diagnosis.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            AutoMapper.Mapper.Map(model, diagnosis);
            var status = RapidVetUnitOfWork.DiagnosisRepository.Delete(diagnosis);
            return RedirectToAction("Index", "Diagnoses");
        }


    }
}
