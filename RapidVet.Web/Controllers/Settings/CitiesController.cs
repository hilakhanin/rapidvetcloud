﻿using System.Linq;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Clients.Details;

namespace RapidVet.Web.Controllers.Settings
{
    public class CitiesController : BaseController
    {
        public ActionResult Index()
        {
            var cities = RapidVetUnitOfWork.ClientRepository.GetCities();
            return View(cities.ToList());
        }

        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return View(new City { Active = true });
            }
            else
            {
                var city = RapidVetUnitOfWork.ClientRepository.GetCity(id.Value);
                return city == null ? View("Error") : View(city);
            }
        }

        [HttpPost]
        public ActionResult Edit(City model)
        {
            OperationStatus status;
            //Create 
            if (model.Id == 0)
            {
                status = RapidVetUnitOfWork.ClientRepository.CreateCity(model);
            }
            //Update
            else
            {
                var cityDb = RapidVetUnitOfWork.ClientRepository.GetCity(model.Id);
                AutoMapper.Mapper.Map(model, cityDb);
                status = RapidVetUnitOfWork.ClientRepository.UpdateCity(cityDb);
            }
            return RedirectToAction("Index");
        }

        // Delete lab test template directory
        public PartialViewResult Remove(int id)
        {
            var city = RapidVetUnitOfWork.ClientRepository.GetCity(id);
            return PartialView("_Delete", city);
        }

        [HttpPost]
        public ActionResult Remove(City model)
        {
            var status = RapidVetUnitOfWork.ClientRepository.RemoveCity(model.Id);
            return RedirectToAction("Index", "Cities");
        }
    }
}