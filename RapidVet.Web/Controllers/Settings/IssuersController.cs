﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using RapidVet.Exeptions;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Repository.Clinics;
using RapidVet.WebModels.Clinics;
using RapidVet.Model.Finance;
using RapidVet.WebModels.Finance.Invoice;


namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class IssuersController : BaseController
    {

        public ActionResult Index()
        {
            var issuers = RapidVetUnitOfWork.IssuerRepository.GetList(CurrentUser.ActiveClinicId, CurrentUser.Id);
            var model = AutoMapper.Mapper.Map<List<Issuer>, List<IssuerCreate>>(issuers);
            //  var model = (from item in clinic.Issuers where item.Active select AutoMapper.Mapper.Map<Issuer, IssuerCreate>(item)).ToList();
            return View(model);
        }

        //id is clinicId
        [HttpGet]
        public PartialViewResult Create()
        {
            if (CurrentUser.IsAdministrator)
            {
                var model = new IssuerCreate();
                return PartialView("_Create", model);
            }
            throw new SecurityException(Resources.Exceptions.AccessDenied);
        }

        [HttpPost]
        public ActionResult Create(IssuerCreate model)
        {
            if (CurrentUser.IsAdministrator)
            {
                var issuer = AutoMapper.Mapper.Map<IssuerCreate, Issuer>(model);
                issuer.CreatedById = CurrentUser.Id;
                issuer.ClinicId = CurrentUser.ActiveClinicId;
                issuer.InvoiceInitialNum = 1;
                issuer.InvoiceReceiptInitialNum = 1;
                issuer.ReceiptInitialNum = 1;
                issuer.ProformaInitialNum = 1;
                issuer.RefoundInitialNum = 1;
                issuer.Active = true;
                var status = RapidVetUnitOfWork.IssuerRepository.Create(issuer);

                if (status.Success)
                {
                    CreateDedaultCreditTypesForIssuer(issuer.Id);
                    CreateDedaultCardCompaniesForIssuer(issuer.Id);
                }

                return RedirectToAction("Index", "Issuers");
            }
            throw new SecurityException(Resources.Exceptions.AccessDenied);
        }

        private void CreateDedaultCreditTypesForIssuer(int issuerId)
        {
            var creditTypes = new List<IssuerCreditType>();

            creditTypes.Add(new IssuerCreditType()
                {
                    IssuerId = issuerId,
                    Name = Resources.Credit.RegularPayment,
                    PostponedPayment = false,
                    IgnorePayments = true,
                    Updated = DateTime.Now,
                    Active = true
                });

            creditTypes.Add(new IssuerCreditType()
            {
                IssuerId = issuerId,
                Name = Resources.Credit.Payments,
                PostponedPayment = true,
                IgnorePayments = false,
                Updated = DateTime.Now,
                Active = true
            });

            creditTypes.Add(new IssuerCreditType()
            {
                IssuerId = issuerId,
                Name = Resources.Credit.CreditPayment,
                PostponedPayment = false,
                IgnorePayments = true,
                Updated = DateTime.Now,
                Active = true
            });

            foreach (IssuerCreditType ict in creditTypes)
            {
                RapidVetUnitOfWork.IssuerRepository.CreateCreditType(ict);
            }

        }

        private void CreateDedaultCardCompaniesForIssuer(int issuerId)
        {
            var defaultCardCompanies = RapidVetUnitOfWork.DefaultCardCompaniesRepository.GetAllDefaultCardCompanies();
           
            foreach (DefaultCardCompanies dc in defaultCardCompanies)
            {
                var creditCardCompany = new CreditCardCode()
                {
                    IssuerId = issuerId,
                    Name = dc.Name,
                    ClinicId = CurrentUser.ActiveClinicId,
                    Active = true,
                    Updated = DateTime.Now
                };
                
                RapidVetUnitOfWork.CreditCardCodeRepository.Create(creditCardCompany);
            }           

        }


        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(id);
            if (issuer.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<Issuer, IssuerCreate>(issuer);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(IssuerCreate model)
        {

            var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(model.Id);
            if (issuer.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            AutoMapper.Mapper.Map(model, issuer);
            issuer.UpdatedById = UserId;
            issuer.UpdatedDate = DateTime.Now;
            var status = RapidVetUnitOfWork.IssuerRepository.Update(issuer);
            return RedirectToAction("Index", "Issuers");

        }

        public PartialViewResult Delete(int id)
        {
            var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(id);
            var model = AutoMapper.Mapper.Map<Issuer, IssuerCreate>(issuer);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public ActionResult Delete(IssuerCreate model)
        {
            var status = RapidVetUnitOfWork.IssuerRepository.RemoveIssuer(model.Id);
            return RedirectToAction("Index", "Issuers");
        }

        // --------------------------------EasyCard---------------------------------------------------

        public ActionResult EasyCardSettings(int id)
        {
            if (IsIssuerInClinic(id))
            {
                return View();
            }
            throw new SecurityException();
        }
      
        //id is issuer id
        public JsonResult GetEasyCardData(int id)
        {
            if (IsIssuerInClinic(id))
            {
                var easycardSettings = RapidVetUnitOfWork.IssuerRepository.GetEasyCardSettings(id).ToList();
                var model =
                    easycardSettings.Select(AutoMapper.Mapper.Map<EasyCardSettings, EasyCardSettingsJsonModel>).ToList();
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        [HttpPost]
        public JsonResult UpdateEasyCardSettings(EasyCardSettings model)
        {
            OperationStatus status;
            var result = new JsonResult();

            if (model.Id == 0)
            {
                model.Updated = DateTime.Now;
                model.Active = true;
                status = RapidVetUnitOfWork.IssuerRepository.CreateEasyCardTerminal(model);
                result.Data = new { success = status.Success, ItemId = status.Success ? model.Id : 0 };
            }
            else
            {
                var easyCardTerminal = RapidVetUnitOfWork.IssuerRepository.GetEasyCardTerminal(model.Id);
                if (easyCardTerminal.IssuerId == model.IssuerId)
                {
                    if(model.IsDefault) //default terminal changed, remove previous default
                    {
                        var previuosDefaultTerminal = RapidVetUnitOfWork.IssuerRepository.GetIssuersDefaultEasyCardTerminal(model.IssuerId);
                        if(previuosDefaultTerminal != null)
                        {
                            previuosDefaultTerminal.IsDefault = false;
                            RapidVetUnitOfWork.Save();
                        }
                    }
                    easyCardTerminal.Name = model.Name;
                    easyCardTerminal.TerminalId = model.TerminalId;
                    easyCardTerminal.Password = model.Password;
                    easyCardTerminal.IsDefault = model.IsDefault;                    
                    easyCardTerminal.Updated = DateTime.Now;
                    status = RapidVetUnitOfWork.Save();
                    result.Data = new { success = status.Success };
                }
                else
                {
                    throw new SecurityException();
                }
            }

            if (status.Success)
            {
                SetSuccessMessage("הנתונים נשמרו בהצלחה");
            }
            else
            {
                SetErrorMessage();
            }
            return result;

        }

        [HttpPost]
        public ActionResult DeleteEasyCardTerminal(EasyCardSettings terminal)
        {
            var easyCardTerminal = RapidVetUnitOfWork.IssuerRepository.GetEasyCardTerminal(terminal.Id);
              if (easyCardTerminal.IssuerId == terminal.IssuerId)
              {
                easyCardTerminal.Active = false;
                easyCardTerminal.Updated = DateTime.Now;
                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    SetSuccessMessage("הנתונים נשמרו בהצלחה");
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index");
            }
            throw new SecurityException();
        }
    }
}
