﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.WebModels.Finance.Invoice;

namespace RapidVet.Web.Controllers
{
    public class CreditCardCodesController : BaseController
    {
        public ActionResult Index(int id)
        {
            if (IsIssuerInClinic(id))
            {               
                return View();
            }
            throw new SecurityException();
        }

        //private bool IsIssuerInClinic(int issuerId)
        //{
        //    var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(issuerId);
        //    return issuer.ClinicId == CurrentUser.ActiveClinicId;
        //}

        public JsonResult GetAllCodes()
        {
            var codes = RapidVetUnitOfWork.CreditCardCodeRepository.GetCodes(CurrentUser.ActiveClinicId).ToList();
            return Json(codes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetIssuerData(int id)
        {
            if (IsIssuerInClinic(id))
            {
                var creditCardCodes = RapidVetUnitOfWork.CreditCardCodeRepository.GetCodesByIssuer(CurrentUser.ActiveClinicId,id).ToList();
                var model =
                    creditCardCodes.Select(AutoMapper.Mapper.Map<CreditCardCode, CreditCardCodesJsonModel>).ToList();
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return null;
        }


        [HttpPost]
        public JsonResult Update(CreditCardCode model)
        {
            OperationStatus status;
            var result = new JsonResult();

            if (model.Id == 0)
            {
                model.ClinicId = CurrentUser.ActiveClinicId;
                model.Active = true;
                status = RapidVetUnitOfWork.CreditCardCodeRepository.Create(model);
                result.Data = new { success = status.Success, ItemId = status.Success ? model.Id : 0 };
            }
            else
            {
                var creditCardCode = RapidVetUnitOfWork.CreditCardCodeRepository.GetCreditCardCode(model.Id);
                if (creditCardCode.ClinicId == CurrentUser.ActiveClinicId)
                {
                    creditCardCode.Name = model.Name;
                    creditCardCode.Updated = DateTime.Now;
                    creditCardCode.ExternalAccountId = model.ExternalAccountId;
                    creditCardCode.IssuerId = model.IssuerId;
                    status = RapidVetUnitOfWork.Save();
                    result.Data = new { success = status.Success };
                }
                else
                {
                    throw new SecurityException();
                }
            }

            if (status.Success)
            {
                SetSuccessMessage("הנתונים נשמרו בהצלחה");
            }
            else
            {
                SetErrorMessage();
            }
            return result;

        }


        [HttpPost]
        public ActionResult Delete(CreditCardCode code)
        {
            var creditCardCode = RapidVetUnitOfWork.CreditCardCodeRepository.GetCreditCardCode(code.Id);
            if (creditCardCode.ClinicId == CurrentUser.ActiveClinicId)
            {
                creditCardCode.Active = false;
                creditCardCode.Updated = DateTime.Now;
                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    SetSuccessMessage("הנתונים נשמרו בהצלחה");
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index");
            }
            throw new SecurityException();
        }


    }
}