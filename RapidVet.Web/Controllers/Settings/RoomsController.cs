﻿using System.Linq;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.FullCalender;

namespace RapidVet.Web.Controllers
{
    public class RoomsController : BaseController
    {
        public ActionResult Index()
        {
            var rooms = RapidVetUnitOfWork.RoomRepository.GetRoomsForClinic(CurrentUser.ActiveClinicId);
            return View(rooms.OrderBy(r => r.Name).ToList());
        }

        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return View(new Room {ClinicId = CurrentUser.ActiveClinicId, Active = true});
            }
            else
            {
                var room = RapidVetUnitOfWork.RoomRepository.GetRoom(id.Value);
                return room == null ? View("Error") : View(room);
            }
        }

        [HttpPost]
        public ActionResult Edit(Room model)
        {
            OperationStatus status;
            //Create 
            if (model.Id == 0)
            {
                status = RapidVetUnitOfWork.RoomRepository.Create(model);
            }
            //Update
            else
            {
                var roomDb = RapidVetUnitOfWork.RoomRepository.GetRoom(model.Id);
                AutoMapper.Mapper.Map(model, roomDb);
                status = RapidVetUnitOfWork.RoomRepository.Update(roomDb);
            }
            return RedirectToAction("Index");
        }

        // Delete lab test template directory
        public PartialViewResult Remove(int id)
        {
            var room = RapidVetUnitOfWork.RoomRepository.GetRoom(id);
            return PartialView("_Delete", room);
        }

        [HttpPost]
        public ActionResult Remove(Room model)
        {
            var status = RapidVetUnitOfWork.RoomRepository.Remove(model.Id);
            return RedirectToAction("Index", "Rooms");
        }
    }
}