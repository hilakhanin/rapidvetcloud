﻿using System;
using System.Linq;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.FullCalender;

namespace RapidVet.Web.Controllers
{
    [ClinicGroupManager]
    public class RecessesController : BaseController
    {
        public ActionResult Index(int? id)
        {
            var userId = CurrentUser.Id;
            if (id.HasValue)
            {
                userId = id.Value;
            }
            var recesses = RapidVetUnitOfWork.RecessRepository.GetRecessesForUser(userId, CurrentUser.ActiveClinicId);
            ViewBag.UserId = userId;
            return View(recesses.OrderBy(r => r.DayId).ToList());
        }

        public ActionResult Edit(int? id, int userId)
        {
            if (!id.HasValue)
            {
                return View(new Recess { UserId = userId, Start = DateTime.Now, End = DateTime.Now, ClinicId = CurrentUser.ActiveClinicId });
            }
            else
            {
                var recess = RapidVetUnitOfWork.RecessRepository.GetRecess(id.Value);
                return recess == null ? View("Error") : View(recess);
            }
        }

        [HttpPost]
        public ActionResult Edit(Recess model)
        {
            if (model.Start >= model.End)
            {
                SetErrorMessage(RapidVet.Resources.Calendar.StartCantBeAfterEnd);
                return View(model);
            }
            if (IsRecessInWorkHours(model))
            {
                OperationStatus status;
                //Create 
                if (model.Id == 0)
                {
                    model.ClinicId = CurrentUser.ActiveClinicId;
                    status = RapidVetUnitOfWork.RecessRepository.Create(model);
                }
                //Update
                else
                {
                    var recessDb = RapidVetUnitOfWork.RecessRepository.GetRecess(model.Id);
                    AutoMapper.Mapper.Map(model, recessDb);
                    status = RapidVetUnitOfWork.RecessRepository.Update(recessDb);
                    SetSuccessMessage();
                }
                return RedirectToAction("Index", new { id = model.UserId});
            }
            SetErrorMessage(RapidVet.Resources.Calendar.CantAddRecessOutOfWork);
            return View(model);
        }

        // Delete lab test template directory
        public PartialViewResult Remove(int id)
        {
            var recess = RapidVetUnitOfWork.RecessRepository.GetRecess(id);
            return PartialView("_Delete", recess);
        }

        [HttpPost]
        public ActionResult Remove(Recess model)
        {
            var status = RapidVetUnitOfWork.RecessRepository.Remove(model.Id);
            return RedirectToAction("Index", "Recesses", new { id = model.UserId });
        }

        private bool IsRecessInWorkHours(Recess recess)
        {
            var dayOfRecessSchedule =
                RapidVetUnitOfWork.UserRepository.GetDailySchedules(recess.UserId, CurrentUser.ActiveClinicId)
                                  .SingleOrDefault(d => d.DayId == recess.DayId);

            return dayOfRecessSchedule != null &&
                   dayOfRecessSchedule.Start.TimeOfDay <= recess.Start.TimeOfDay && recess.Start.TimeOfDay <= dayOfRecessSchedule.End.TimeOfDay &&
                   dayOfRecessSchedule.Start.TimeOfDay <= recess.End.TimeOfDay && recess.End.TimeOfDay <= dayOfRecessSchedule.End.TimeOfDay;
        }
    }
}