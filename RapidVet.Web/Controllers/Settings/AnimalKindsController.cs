﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Web.Mvc;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Controllers
{
    [ClinicGroupManager]
    public class AnimalKindsController : BaseController
    {

        public ActionResult Index()
        {
            var kinds = RapidVetUnitOfWork.AnimalKindRepository.GetList(CurrentUser.ActiveClinicGroupId);
            var model = kinds.Select(AutoMapper.Mapper.Map<AnimalKind, AnimalKindModel>).ToList();
            return View(model);
        }

        [HttpGet]
        public PartialViewResult Create()
        {
            var model = new AnimalKindModel()
                {
                    ClinicGroupId = CurrentUser.ActiveClinicGroupId
                };
            ViewBag.icons = RapidVetUnitOfWork.AnimalKindRepository.GetIcons();
            return PartialView("_Create", model);
        }

        [HttpPost]
        public ActionResult Create(AnimalKindModel model)
        {

            model.ClinicGroupId = CurrentUser.ActiveClinicGroupId;
            var kind = AutoMapper.Mapper.Map<AnimalKindModel, AnimalKind>(model);
            kind.Id = 0;
            kind.Active = true;
            var status = RapidVetUnitOfWork.AnimalKindRepository.Create(kind);
            return RedirectToAction("Index", "AnimalKinds");
        }

        //id is animalKindId
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var kind = RapidVetUnitOfWork.AnimalKindRepository.GetItem(id);
            if (kind.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            ViewBag.icons = RapidVetUnitOfWork.AnimalKindRepository.GetIcons();
            var model = AutoMapper.Mapper.Map<AnimalKind, AnimalKindModel>(kind);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(AnimalKindModel model)
        {

            var kind = RapidVetUnitOfWork.AnimalKindRepository.GetItem(model.Id);
            if (kind.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            AutoMapper.Mapper.Map(model, kind);
            var status = RapidVetUnitOfWork.AnimalKindRepository.Update(kind);
            return RedirectToAction("Index", "AnimalKinds");

        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var kind = RapidVetUnitOfWork.AnimalKindRepository.GetItem(id);
            if (kind.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<AnimalKind, AnimalKindModel>(kind);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public ActionResult Delete(AnimalKindModel model)
        {
            var kind = RapidVetUnitOfWork.AnimalKindRepository.GetItem(model.Id);
            if (kind.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var status = RapidVetUnitOfWork.AnimalKindRepository.Delete(model.Id);
            return RedirectToAction("Index", "AnimalKinds");
        }
    }
}
