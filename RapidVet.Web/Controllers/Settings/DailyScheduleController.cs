﻿using System;
using System.Linq;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Users;
using RapidVet.WebModels.Users;

namespace RapidVet.Web.Controllers
{
    [ClinicGroupManager]
    public class DailyScheduleController : BaseController
    {
        public ActionResult Index(int? id)
        {
            var userId = CurrentUser.Id;
            if (id.HasValue)
            {
                userId = id.Value;
            }

            var dailySchedules = RapidVetUnitOfWork.UserRepository.GetDailySchedules(userId, CurrentUser.ActiveClinicId);
            //var date = new DateTime();
            var model = new WeeklyScheduleWebModel() { UserId = userId };

            // Add to webmodel all the daily schedules for each day of the week
            for (var i = 0; i < 7; i++)
            {
                var dailySchedule = dailySchedules.SingleOrDefault(ds => ds.DayId == i);
                if (dailySchedule == null) continue;
                switch (i)
                {
                    case 0:
                        model.SundayId = dailySchedule.Id;
                        model.SundayStart = dailySchedule.Start;
                        model.SundayEnd = dailySchedule.End;
                        break;
                    case 1:
                        model.MondayId = dailySchedule.Id;
                        model.MondayStart = dailySchedule.Start;
                        model.MondayEnd = dailySchedule.End;
                        break;
                    case 2:
                        model.TuesdayId = dailySchedule.Id;
                        model.TuesdayStart = dailySchedule.Start;
                        model.TuesdayEnd = dailySchedule.End;
                        break;
                    case 3:
                        model.WednesdayId = dailySchedule.Id;
                        model.WednesdayStart = dailySchedule.Start;
                        model.WednesdayEnd = dailySchedule.End;
                        break;
                    case 4:
                        model.ThursdayId = dailySchedule.Id;
                        model.ThursdayStart = dailySchedule.Start;
                        model.ThursdayEnd = dailySchedule.End;
                        break;
                    case 5:
                        model.FridayId = dailySchedule.Id;
                        model.FridayStart = dailySchedule.Start;
                        model.FridayEnd = dailySchedule.End;
                        break;
                    case 6:
                        model.SaturdayId = dailySchedule.Id;
                        model.SaturdayStart = dailySchedule.Start;
                        model.SaturdayEnd = dailySchedule.End;
                        break;
                }
            }
            ViewBag.UserId = userId;
            return View(model);
        }
        [HttpPost]
        public ActionResult Index(WeeklyScheduleWebModel model)
        {
            var status = new OperationStatus() { Success = true };
            // return to database all information from webmodel

            //Sunday
            if (model.SundayId == 0)
            {
                if (model.SundayStart.HasValue && model.SundayEnd.HasValue)
                {
                    status =
                        RapidVetUnitOfWork.UserRepository.CreateDailySchedule(new DailySchedule()
                        {
                            DayId = 0,
                            Start = model.SundayStart.Value,
                            End = model.SundayEnd.Value,
                            UserId = model.UserId,
                            ClinicId = CurrentUser.ActiveClinicId
                        });
                }
            }
            else
            {
                var dailyScheduleDb = RapidVetUnitOfWork.UserRepository.GetDailySchedule(model.SundayId);
                if (dailyScheduleDb != null && model.SundayStart.HasValue && model.SundayEnd.HasValue)
                {
                    if (dailyScheduleDb.Start != model.SundayStart.Value || dailyScheduleDb.End != model.SundayEnd.Value)
                    {
                        dailyScheduleDb.Start = model.SundayStart.Value;
                        dailyScheduleDb.End = model.SundayEnd.Value;
                        status = RapidVetUnitOfWork.UserRepository.UpdateDailySchedule(dailyScheduleDb);
                    }
                }
                else
                {
                    status = RapidVetUnitOfWork.UserRepository.RemoveDailySchedule(dailyScheduleDb.Id);
                }
            }

            //Monday
            if (model.MondayId == 0)
            {
                if (model.MondayStart.HasValue && model.MondayEnd.HasValue)
                {
                    status =
                        RapidVetUnitOfWork.UserRepository.CreateDailySchedule(new DailySchedule()
                        {
                            DayId = 1,
                            Start = model.MondayStart.Value,
                            End = model.MondayEnd.Value,
                            UserId = model.UserId,
                            ClinicId = CurrentUser.ActiveClinicId
                        });
                }
            }
            else
            {
                var dailyScheduleDb = RapidVetUnitOfWork.UserRepository.GetDailySchedule(model.MondayId);
                if (dailyScheduleDb != null && model.MondayStart.HasValue && model.MondayEnd.HasValue)
                {
                    if (dailyScheduleDb.Start != model.MondayStart.Value || dailyScheduleDb.End != model.MondayEnd.Value)
                    {
                        dailyScheduleDb.Start = model.MondayStart.Value;
                        dailyScheduleDb.End = model.MondayEnd.Value;
                        status = RapidVetUnitOfWork.UserRepository.UpdateDailySchedule(dailyScheduleDb);
                    }
                }
                else
                {
                    status = RapidVetUnitOfWork.UserRepository.RemoveDailySchedule(dailyScheduleDb.Id);
                }
            }

            //Tuesday
            if (model.TuesdayId == 0)
            {
                if (model.TuesdayStart.HasValue && model.TuesdayEnd.HasValue)
                {
                    status =
                        RapidVetUnitOfWork.UserRepository.CreateDailySchedule(new DailySchedule()
                        {
                            DayId = 2,
                            Start = model.TuesdayStart.Value,
                            End = model.TuesdayEnd.Value,
                            UserId = model.UserId,
                            ClinicId = CurrentUser.ActiveClinicId
                        });
                }
            }
            else
            {
                var dailyScheduleDb = RapidVetUnitOfWork.UserRepository.GetDailySchedule(model.TuesdayId);
                if (dailyScheduleDb != null && model.TuesdayStart.HasValue && model.TuesdayEnd.HasValue)
                {
                    if (dailyScheduleDb.Start != model.TuesdayStart.Value || dailyScheduleDb.End != model.TuesdayEnd.Value)
                    {
                        dailyScheduleDb.Start = model.TuesdayStart.Value;
                        dailyScheduleDb.End = model.TuesdayEnd.Value;
                        status = RapidVetUnitOfWork.UserRepository.UpdateDailySchedule(dailyScheduleDb);
                    }
                }
                else
                {
                    status = RapidVetUnitOfWork.UserRepository.RemoveDailySchedule(dailyScheduleDb.Id);
                }
            }

            //Wednesday
            if (model.WednesdayId == 0)
            {
                if (model.WednesdayStart.HasValue && model.WednesdayEnd.HasValue)
                {
                    status =
                        RapidVetUnitOfWork.UserRepository.CreateDailySchedule(new DailySchedule()
                        {
                            DayId = 3,
                            Start = model.WednesdayStart.Value,
                            End = model.WednesdayEnd.Value,
                            UserId = model.UserId,
                            ClinicId = CurrentUser.ActiveClinicId
                        });
                }
            }
            else
            {
                var dailyScheduleDb = RapidVetUnitOfWork.UserRepository.GetDailySchedule(model.WednesdayId);
                if (dailyScheduleDb != null && model.WednesdayStart.HasValue && model.WednesdayEnd.HasValue)
                {
                    if (dailyScheduleDb.Start != model.WednesdayStart.Value || dailyScheduleDb.End != model.WednesdayEnd.Value)
                    {
                        dailyScheduleDb.Start = model.WednesdayStart.Value;
                        dailyScheduleDb.End = model.WednesdayEnd.Value;
                        status = RapidVetUnitOfWork.UserRepository.UpdateDailySchedule(dailyScheduleDb);
                    }
                }
                else
                {
                    status = RapidVetUnitOfWork.UserRepository.RemoveDailySchedule(dailyScheduleDb.Id);
                }
            }

            //Thursday
            if (model.ThursdayId == 0)
            {
                if (model.ThursdayStart.HasValue && model.ThursdayEnd.HasValue)
                {
                    status =
                        RapidVetUnitOfWork.UserRepository.CreateDailySchedule(new DailySchedule()
                        {
                            DayId = 4,
                            Start = model.ThursdayStart.Value,
                            End = model.ThursdayEnd.Value,
                            UserId = model.UserId,
                            ClinicId = CurrentUser.ActiveClinicId
                        });
                }
            }
            else
            {
                var dailyScheduleDb = RapidVetUnitOfWork.UserRepository.GetDailySchedule(model.ThursdayId);
                if (dailyScheduleDb != null && model.ThursdayStart.HasValue && model.ThursdayEnd.HasValue)
                {
                    if (dailyScheduleDb.Start != model.ThursdayStart.Value || dailyScheduleDb.End != model.ThursdayEnd.Value)
                    {
                        dailyScheduleDb.Start = model.ThursdayStart.Value;
                        dailyScheduleDb.End = model.ThursdayEnd.Value;
                        status = RapidVetUnitOfWork.UserRepository.UpdateDailySchedule(dailyScheduleDb);
                    }
                }
                else
                {
                    status = RapidVetUnitOfWork.UserRepository.RemoveDailySchedule(dailyScheduleDb.Id);
                }
            }

            //Friday
            if (model.FridayId == 0)
            {
                if (model.FridayStart.HasValue && model.FridayEnd.HasValue)
                {
                    status =
                        RapidVetUnitOfWork.UserRepository.CreateDailySchedule(new DailySchedule()
                        {
                            DayId = 5,
                            Start = model.FridayStart.Value,
                            End = model.FridayEnd.Value,
                            UserId = model.UserId,
                            ClinicId = CurrentUser.ActiveClinicId
                        });
                }
            }
            else
            {
                var dailyScheduleDb = RapidVetUnitOfWork.UserRepository.GetDailySchedule(model.FridayId);
                if (dailyScheduleDb != null && model.FridayStart.HasValue && model.FridayEnd.HasValue)
                {
                    if (dailyScheduleDb.Start != model.FridayStart.Value || dailyScheduleDb.End != model.FridayEnd.Value)
                    {
                        dailyScheduleDb.Start = model.FridayStart.Value;
                        dailyScheduleDb.End = model.FridayEnd.Value;
                        status = RapidVetUnitOfWork.UserRepository.UpdateDailySchedule(dailyScheduleDb);
                    }
                }
                else
                {
                    status = RapidVetUnitOfWork.UserRepository.RemoveDailySchedule(dailyScheduleDb.Id);
                }
            }

            //Saturday
            if (model.SaturdayId == 0)
            {
                if (model.SaturdayStart.HasValue && model.SaturdayEnd.HasValue)
                {
                    status =
                        RapidVetUnitOfWork.UserRepository.CreateDailySchedule(new DailySchedule()
                        {
                            DayId = 6,
                            Start = model.SaturdayStart.Value,
                            End = model.SaturdayEnd.Value,
                            UserId = model.UserId,
                            ClinicId = CurrentUser.ActiveClinicId
                        });
                }
            }
            else
            {
                var dailyScheduleDb = RapidVetUnitOfWork.UserRepository.GetDailySchedule(model.SaturdayId);
                if (dailyScheduleDb != null && model.SaturdayStart.HasValue && model.SaturdayEnd.HasValue)
                {
                    if (dailyScheduleDb.Start != model.SaturdayStart.Value || dailyScheduleDb.End != model.SaturdayEnd.Value)
                    {
                        dailyScheduleDb.Start = model.SaturdayStart.Value;
                        dailyScheduleDb.End = model.SaturdayEnd.Value;
                        status = RapidVetUnitOfWork.UserRepository.UpdateDailySchedule(dailyScheduleDb);
                    }
                }
                else
                {
                    status = RapidVetUnitOfWork.UserRepository.RemoveDailySchedule(dailyScheduleDb.Id);
                }
            }
            if (status.Success)
            {
                SetSuccessMessage("הנתונים נשמרו בהצלחה");
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("Index");
        }
    }
}