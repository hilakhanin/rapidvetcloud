﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.WebModels.LabTests;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class LabTestsController : BaseController
    {
        //
        public ActionResult ChooseAnimalKindForLabTest()
        {
            var model = RapidVetUnitOfWork.AnimalKindRepository.GetList(CurrentUser.ActiveClinicGroupId).ToList();
            return View(model);
        }

        //id is animal kind
        public ActionResult TemplatesIndex(int id)
        {
            var model = new LabTestTemplateIndexModel();
            model.AnimalKindId = id;

            var templates = RapidVetUnitOfWork.LabTestsRepository.GetLabTestTemplatesForKind(id);
            model.Templates = AutoMapper.Mapper.Map<List<LabTestTemplate>, List<LabTestTemplateEdit>>(templates);

            var types = RapidVetUnitOfWork.LabTestsRepository.GetLabTestTypes(CurrentUser.ActiveClinicId).ToList();
            model.LabTestTypes = AutoMapper.Mapper.Map<List<LabTestType>, List<SelectListItem>>(types);

            model.LabTestTypes.Add(new SelectListItem() { Text = Resources.Labs.OtherAddNew, Value = "0" });

            var serializer = new JavaScriptSerializer();
            //pass stuff in viewbags
            ViewBag.JSON = serializer.Serialize(model.Templates);
            ViewBag.TemplateOptions = serializer.Serialize(model.LabTestTypes);
            return View(model);
        }

        // GET: /LabTests/
        //id is animalkind id
        public ActionResult Index(int id)
        {
            ViewBag.Id = id;

            var labTestTypes = RapidVetUnitOfWork.LabTestsRepository.GetLabTestTypes(CurrentUser.ActiveClinicId).Where(l => l.AnimalKinds.Any(t => t.AnimalKindId == id));

            return View(labTestTypes.OrderBy(l => l.Name).ToList());
        }

        public ActionResult EditLabTestType(int id, int kind)
        {
            ViewBag.Kind = kind;
            if (id == 0)
            {
                return View(new LabTestTypeEdit() { ClinicId = CurrentUser.ActiveClinicId });
            }
            var labTestType = RapidVetUnitOfWork.LabTestsRepository.GetItem(id);
            if (labTestType == null)
            {
                return View("Error");
            }
            var model = AutoMapper.Mapper.Map<LabTestType, LabTestTypeEdit>(labTestType);
            return View(model);
        }

        [HttpPost]
        public ActionResult EditLabTestType(LabTestTypeEdit model, int kind)
        {
            OperationStatus status;
            //Create 
            if (model.Id == 0)
            {
                var labTestType = AutoMapper.Mapper.Map<LabTestTypeEdit, LabTestType>(model);
                status = RapidVetUnitOfWork.LabTestsRepository.AddItem(labTestType);
            }
            //Update
            else
            {
                var labTestTypeDb = RapidVetUnitOfWork.LabTestsRepository.GetItem(model.Id);
                AutoMapper.Mapper.Map(model, labTestTypeDb);
                status = RapidVetUnitOfWork.LabTestsRepository.UpdateItem(labTestTypeDb);
            }
            return RedirectToAction("Index", new { id = kind });
        }

        [HttpPost]
        public JsonResult AddType(String name)
        {
            var type = new LabTestType()
                {
                    ClinicId = CurrentUser.ActiveClinicId,
                    Name = name
                };

            var status = RapidVetUnitOfWork.LabTestsRepository.AddItem(type);

            if (status.Success)
            {
                return Json(type.Id);
            }
            else
            {
                return Json(false);
            }
        }

        // Delete lab test type directory
        public PartialViewResult Remove(int id)
        {
            var labTestType = RapidVetUnitOfWork.LabTestsRepository.GetItem(id);
            return PartialView("_Delete", labTestType);
        }

        [HttpPost]
        public ActionResult Remove(LabTestType model)
        {
            var status = RapidVetUnitOfWork.LabTestsRepository.RemoveItem(model.Id);
            return RedirectToAction("Index", "LabTests");
        }

        //old code
        //public ActionResult TemplateIndex(int id)
        //{
        //    var labTestType = RapidVetUnitOfWork.LabTestsRepository.GetItem(id);
        //    if (labTestType == null)
        //    {
        //        return View("Error");
        //    }

        //    ViewBag.LabTestTypeName = labTestType.Name;
        //    ViewBag.LabTestTypeId = id;

        //    var model = new LabTestTemplateIndexModel()
        //    {
        //        AnimalKinds = RapidVetUnitOfWork.AnimalKindRepository.GetList(CurrentUser.ActiveClinicGroupId).Select(AutoMapper.Mapper.Map<AnimalKind, AnimalKindModel>).ToList()
        //    };
        //    return View(model);
        //}

        public ActionResult EditLabTestTemplate(int id, int labTestTypeId = 0, int animalKindId = 0)
        {
            var animalKinds = RapidVetUnitOfWork.AnimalKindRepository.GetList(CurrentUser.ActiveClinicGroupId).Select(AutoMapper.Mapper.Map<AnimalKind, AnimalKindModel>).ToList();

            if (id == 0)
            {
                return View(new LabTestTemplateEdit() { LabTestTypeId = labTestTypeId, AnimalKindId = animalKindId });
            }
            var labTestTemplateEdit = RapidVetUnitOfWork.LabTestsRepository.GetTemplateItem(id);
            if (labTestTemplateEdit == null)
            {
                return View("Error");
            }
            var model = AutoMapper.Mapper.Map<LabTestTemplate, LabTestTemplateEdit>(labTestTemplateEdit);
            model.AnimalKinds = animalKinds;
            return View(model);
        }

        [HttpPost]
        public ActionResult EditLabTestTemplate(LabTestTemplateEdit model)
        {
            OperationStatus status;
            //Create 
            if (model.Id == 0)
            {
                var labTestTemplateEdit = AutoMapper.Mapper.Map<LabTestTemplateEdit, LabTestTemplate>(model);
                status = RapidVetUnitOfWork.LabTestsRepository.AddTemplateItem(labTestTemplateEdit);
            }
            //Update
            else
            {
                var labTestTemplateDb = RapidVetUnitOfWork.LabTestsRepository.GetTemplateItem(model.Id);
                AutoMapper.Mapper.Map(model, labTestTemplateDb);
                status = RapidVetUnitOfWork.LabTestsRepository.UpdateTemplateItem(labTestTemplateDb);
            }
            return RedirectToAction("TemplatesIndex", new { id = model.LabTestTypeId });
        }

        [HttpPost]
        public JsonResult SaveLabTestTemplate(LabTestTemplateEdit model)
        {
            int id;
            OperationStatus status;
            if (model.Id == 0)
            {
                var labTestTemplateEdit = AutoMapper.Mapper.Map<LabTestTemplateEdit, LabTestTemplate>(model);

                status = RapidVetUnitOfWork.LabTestsRepository.AddTemplateItem(labTestTemplateEdit);
                id = labTestTemplateEdit.Id;
            }
            //Update
            else
            {
                var labTestTemplateDb = RapidVetUnitOfWork.LabTestsRepository.GetTemplateItem(model.Id);
                AutoMapper.Mapper.Map(model, labTestTemplateDb);
                id = labTestTemplateDb.Id;
                status = RapidVetUnitOfWork.LabTestsRepository.UpdateTemplateItem(labTestTemplateDb);
            }
            if (status.Success)
            {
                SetSuccessMessage("תבנית עודכנה בהצלחה");
            }
            else
            {
                //SetErrorMessage("ארעה שגיאה בעדכון תבנית");
            }
            return Json(id);
        }

        [HttpPost]
        public JsonResult RemoveLabTestTemplate(int id)
        {
            var res = RapidVetUnitOfWork.LabTestsRepository.RemoveTemplateItem(id);
            if (res.Success)
            {
                SetSuccessMessage("תבנית הוסרה בהצלחה");
            }
            else
            {
                SetErrorMessage("ארעה שגיאה");
            }
            return Json(res.Success);
        }

        // Delete lab test template directory
        public PartialViewResult RemoveTemplate(int id)
        {
            var labTestType = RapidVetUnitOfWork.LabTestsRepository.GetTemplateItem(id);
            return PartialView("_DeleteTemplate", labTestType);
        }

        [HttpPost]
        public ActionResult RemoveTemplate(LabTestTemplate model)
        {
            var status = RapidVetUnitOfWork.LabTestsRepository.RemoveTemplateItem(model.Id);
            return RedirectToAction("TemplatesIndex", "LabTests", new { id = model.LabTestTypeId });
        }

        public JsonResult GetTemplateOfAnimal(int id, int animalKindId)
        {
            var labTestTemplates = RapidVetUnitOfWork.LabTestsRepository.GetLabTestTemplates(id, animalKindId);

            return Json(labTestTemplates, JsonRequestBehavior.AllowGet);
        }


        //id is LabTestTemplateId
        public ActionResult EditLabTests(int id)
        {
            ViewBag.testName = RapidVetUnitOfWork.LabTestsRepository.GetTemplateItemName(id);
            return View();
        }

        //id is LabTestTemplateId
        public JsonResult GetLabTestData(int id)
        {
            var data = RapidVetUnitOfWork.LabTestsRepository.GetLabTestData(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveLabTestData(int id, string data)
        {
            var status = new OperationStatus() { Success = false };
            if (!string.IsNullOrWhiteSpace(data))
            {
                var testData = JsonConvert.DeserializeObject<List<LabTest>>(data);

                foreach (var td in testData)
                {
                    td.LabTestTemplateId = id;
                }
                status = RapidVetUnitOfWork.LabTestsRepository.SaveLabTestData(id, testData);
                if (status.Success)
                {
                    SetSuccessMessage("המידע נשמר בהצלחה");
                }
                else
                {
                    SetErrorMessage();
                }
            }
            return Json(status);
        }
    }
}
