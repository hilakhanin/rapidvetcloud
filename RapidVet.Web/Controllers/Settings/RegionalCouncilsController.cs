﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model;

namespace RapidVet.Web.Controllers.Settings
{
    [Authorize]
    [Administrator]
    public class RegionalCouncilsController : BaseController
    {

        public ActionResult Index()
        {
            var model = RapidVetUnitOfWork.RegionalCouncilsRepository.GetAll().ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new RegionalCouncil();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(RegionalCouncil model)
        {
            if (ModelState.IsValid)
            {
                var status = RapidVetUnitOfWork.RegionalCouncilsRepository.Create(model);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "RegionalCouncils");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(id);
            return View(regionalCouncil);
        }

        [HttpPost]
        public ActionResult Edit(RegionalCouncil model)
        {
            if (ModelState.IsValid)
            {
                var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(model.Id);
                AutoMapper.Mapper.Map(model, regionalCouncil);
                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "RegionalCouncils");
            }
            return View(model);
        }
    }
}
