﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model.Finance;
using RapidVet.RapidConsts.Controllers;
using RapidVet.Repository.Finances;
using RapidVet.WebModels.Finance;

namespace RapidVet.Web.Controllers
{
    public class PriceListCategoriesController : BaseController
    {

        [HttpPost]
        public ActionResult Create(string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                var category = new PriceListCategory()
                    {
                        ClinicId = CurrentUser.ActiveClinicId,
                        Available = true,
                        Name = name,
                        ShortName = name
                    };
                var status = RapidVetUnitOfWork.CategoryRepository.Create(category);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
            }
            return RedirectToAction("Categories", "PriceListItems");
        }


        [HttpPost]
        public ActionResult Edit(int id, string name)
        {
            if (id > 0 && !string.IsNullOrWhiteSpace(name))
            {
                var category = RapidVetUnitOfWork.CategoryRepository.GetItem(id);
                if (category != null && category.ClinicId == CurrentUser.ActiveClinicId)
                {
                    category.Name = name;
                    var status = RapidVetUnitOfWork.Save();
                    if (status.Success)
                    {
                        SetSuccessMessage();
                    }
                    else
                    {
                        SetErrorMessage();
                    }
                }
            }
            return RedirectToAction("Categories", "PriceListItems");
        }

        //id is categoryId
        [HttpPost]
        public ActionResult Delete(int id)
        {
            var status = RapidVetUnitOfWork.CategoryRepository.RemoveCategoryFromClinic(id);
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("Categories", "PriceListItems");
        }

        public PartialViewResult CategoriesDdl(int? id)
        {
            var categories = RapidVetUnitOfWork.CategoryRepository.GetClinicActiveCategories(CurrentUser.ActiveClinicId);
            return PartialView("_CategoriesDdl", categories);
        }

        public PartialViewResult MenuBar(int? selected)
        {
            return PartialView("_CategoryMenuBar");
        }
    }
}
