﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.WebModels.Finance.Invoice;

namespace RapidVet.Web.Controllers
{
    public class IssuerCreditTypesController : BaseController
    {
        //is is issuerId
        public ActionResult Index(int id)
        {
            if (IsIssuerInClinic(id))
            {
                return View();
            }
            throw new SecurityException();
            //return View();
        }

        public JsonResult GetIssuerData(int id)
        {
            //if (IsIssuerInClinic(id))
            //{
                var creditTypes = RapidVetUnitOfWork.IssuerRepository.GetCreditTypes(id).ToList();
                var model =
                    creditTypes.Select(AutoMapper.Mapper.Map<IssuerCreditType, IssuerCreditTypeJsonModel>).ToList();
                return Json(model, JsonRequestBehavior.AllowGet);
            //}
            //return null;
        }

        [HttpPost]
        public JsonResult Save(IssuerCreditType model)
        {
            OperationStatus status;
            var result = new JsonResult();

            if (model.Id == 0)
            {
                model.Active = true;
                status = RapidVetUnitOfWork.IssuerRepository.CreateCreditType(model);
                result.Data = new { success = status.Success, itemId = model.Id };
            }
            else
            {
                var creditType = RapidVetUnitOfWork.IssuerRepository.GetIssuerCreditType(model.Id);
                if (creditType.IssuerId == model.IssuerId && IsIssuerInClinic(creditType.IssuerId))
                {
                    AutoMapper.Mapper.Map(model, creditType);
                    status = RapidVetUnitOfWork.Save();
                    result.Data = new { success = status.Success };
                }
                else return null;
            }
            if (status.Success)
            {
                SetSuccessMessage("הנתונים נשמרו בהצלחה");
            }
            else
            {
                SetErrorMessage();
            }
            return result;
        }

        [HttpPost]
        public JsonResult Delete(IssuerCreditType model)
        {
            var creditType = RapidVetUnitOfWork.IssuerRepository.GetIssuerCreditType(model.Id);
            if (creditType.IssuerId == model.IssuerId && IsIssuerInClinic(creditType.IssuerId))
            {
                creditType.Active = false;
                creditType.Updated = DateTime.Now;
                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    SetSuccessMessage("הנתונים נשמרו בהצלחה");
                }
                else
                {
                    SetErrorMessage();
                }
                return Json(status.Success);
            }
            return null;
        }


        //private bool IsIssuerInClinic(int issuerId)
        //{
        //    var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(issuerId);
        //    return issuer.ClinicId == CurrentUser.ActiveClinicId;
        //}
    }
}