﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Finance;
using RapidVet.Repository.Finances;
using RapidVet.WebModels.Finance;

namespace RapidVet.Web.Controllers
{
    public class PriceListsController : BaseController
    {


        //id is ClinicId
        public PartialViewResult Index(int id)
        {
       
            return PartialView("_Index");
        }

        /// <summary>
        /// view containing priceListCategory details & items
        /// </summary>
        /// <param name="id">priceListCategoryId</param>
        /// <returns>actionResult view containing priceListCategory details & items</returns>
        public ActionResult Items(int id)
        {
            throw new NotImplementedException();
        }


        public ActionResult CreateItem(int id)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public ActionResult CreateItem(PriceListItem model)
        {
            throw new NotImplementedException();
        }

        public ActionResult EditItem(int id)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public ActionResult EditItem(FormCollection formCollection, int id)
        {
            throw new NotImplementedException();
        }

        public ActionResult ItemDetails(int id)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public ActionResult RemoveItem(int id, int CategoryId)
        {
            throw new NotImplementedException();
        }

        public ActionResult DuplicatePrices(int clinicId, int targetClinic)
        {
            throw new NotImplementedException();
        }

        public ActionResult Tariffs(int id)
        {
            throw new NotImplementedException();
        }

        public ActionResult CreateTariff(int id)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public ActionResult CreateTariff(Tariff model)
        {
            throw new NotImplementedException();
        }
    }
}
