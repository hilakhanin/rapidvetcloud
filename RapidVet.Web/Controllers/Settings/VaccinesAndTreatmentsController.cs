﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.Patients;
using RapidVet.WebModels.Clinics;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Patients;
using RapidVet.WebModels.VaccinesAndTreatments;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class VaccinesAndTreatmentsController : BaseController
    {
        //
        // GET: /VaccinesAndTreatments/

        public ActionResult Index()
        {
            var animals = RapidVetUnitOfWork.AnimalKindRepository.GetList(CurrentUser.ActiveClinicGroupId);
            var model = animals.Select(AutoMapper.Mapper.Map<AnimalKind, AnimalKindModel>).ToList();
            return View(model);
        }

        public JsonResult GetVaccinesAndTreatmentsForAnimal(int id)
        {
            var model = AutoMapper.Mapper.Map<List<VaccineOrTreatmentModel>>(RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetVaccineAndTreatmentOfAnimalKind(
                    CurrentUser.ActiveClinicId, id).OrderBy(vt => vt.Order).ToList());

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditVAndTPatternForAnimal(int id)
        {
            ViewBag.AnimalKindId = id;
            return View();
        }


        public JsonResult PatternForAnimal(int id)
        {
            var priceListItems =
                RapidVetUnitOfWork.ItemRepository.GetAllItemsOfTypeVaccineOrTreatment(CurrentUser.ActiveClinicId);

            var model = new VaccineAndTreatmentPatternSetting()
                {
                    AnimalKindId = id,
                    AnimalKind =
                        AutoMapper.Mapper.Map<AnimalKindModel>(RapidVetUnitOfWork.AnimalKindRepository.GetItem(id)),
                    AllItems = priceListItems.Select(AutoMapper.Mapper.Map<PriceListItem, PriceListItemModel>).ToList(),
                    SelectedItems =
                        AutoMapper.Mapper.Map<List<VaccineOrTreatmentModel>>(
                            RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetVaccineAndTreatmentOfAnimalKind(
                                CurrentUser.ActiveClinicId, id).ToList())
                };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveVaccinesAndTreatments(int id, string itemsJson)
        {
            var clientItems = JsonConvert.DeserializeObject<List<VaccineOrTreatmentJsonModel>>(itemsJson);

            var serverItems = RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetVaccineAndTreatmentOfAnimalKind(CurrentUser.ActiveClinicId, id);
            var counter = 0;
            //Remove unused vaccine and treatments
            foreach (var vaccineOrTreatment in serverItems)
            {
                if (clientItems.All(c => c.id != vaccineOrTreatment.PriceListItemId))
                {
                    string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}",
                                                                    Resources.Auditing.Module, Resources.Auditing.VaccineAndTreatments,
                                                                    Resources.Auditing.Action, Resources.Auditing.Remove,
                                                                    Resources.Auditing.AnimalKind, RapidVetUnitOfWork.AnimalKindRepository.GetKindById(id),
                                                                    Resources.Auditing.ItemName, RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(vaccineOrTreatment.PriceListItemId).Name);
                    RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.VaccineAndTreatments, logMessage, "");
          

                    RapidVetUnitOfWork.PreventiveMedicineRepository.ReomveNextVaccineAndTreatmentsByID(vaccineOrTreatment.Id);

                    RapidVetUnitOfWork.VaccineAndTreatmentRepository.RemoveItem(vaccineOrTreatment.Id);
                    counter++;
                }
            }

            //AddOrUpdate the New vaccine and treatments
            foreach (var item in clientItems)
            {
                var dbItem = RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetItemByPriceListItemId(id, item.id);
                if (dbItem == null)
                {
                    RapidVetUnitOfWork.VaccineAndTreatmentRepository.AddItem(new VaccineOrTreatment()
                        {
                            AnimalKindId = id,
                            ClinicId = CurrentUser.ActiveClinicId,
                            Order = item.order,
                            PriceListItemId = item.id,
                            Active = true
                        });
                    counter++;

                    string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}",
                                                    Resources.Auditing.Module, Resources.Auditing.VaccineAndTreatments,
                                                    Resources.Auditing.Action, Resources.Auditing.Add,
                                                    Resources.Auditing.AnimalKind, RapidVetUnitOfWork.AnimalKindRepository.GetKindById(id),
                                                    Resources.Auditing.ItemName, RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(item.id).Name);
                    RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.VaccineAndTreatments, logMessage, "");                 
                }
                else
                {
                    if (dbItem.Order != item.order)
                    {
                        dbItem.Order = item.order;
                        counter++;
                    }
                }
            }

            var result = RapidVetUnitOfWork.Save();

            if (result.Success || counter == 0)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }

            return new JsonResult() { Data = result.Success };
        }

        public ActionResult NextVaccinesAndTreatments(int id)
        {
            ViewBag.VaccineOrTreatmentId = id;
            return View();
        }

        public JsonResult NextVaccinesAndTreatmentsForItem(int id)
        {
            var vaccineOrTreatment = AutoMapper.Mapper.Map<VaccineOrTreatmentModel>(RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetItem(id));

            var allVaccinesAndTreatments = AutoMapper.Mapper.Map<List<VaccineOrTreatmentModel>>(
                RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetVaccineAndTreatmentOfAnimalKind(
                    CurrentUser.ActiveClinicId, vaccineOrTreatment.AnimalKindId).ToList());

            var nextVaccinesAndTreatments = AutoMapper.Mapper.Map<List<NextVaccineOrTreatmentModel>>(
                RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetNextVaccineAndTreatmentOfItem(id).ToList());

            var model = new NextVaccineAndTreatmentSetting()
                {
                    AllItems = allVaccinesAndTreatments,
                    SelectedItems = nextVaccinesAndTreatments,
                    VaccineOrTreatmentId = id,
                    VaccineOrTreatment = vaccineOrTreatment,
                    AnimalKindId = vaccineOrTreatment.AnimalKindId,
                    AnimalKind = AutoMapper.Mapper.Map<AnimalKindModel>(RapidVetUnitOfWork.AnimalKindRepository.GetItem(vaccineOrTreatment.AnimalKindId))
                };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveNextVaccinesAndTreatments(int id, string itemsJson)
        {
            var clientItems = JsonConvert.DeserializeObject<List<NextVaccineOrTreatmentJsonModel>>(itemsJson);

            var serverItems = RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetNextVaccineAndTreatmentOfItem(id);

            var status = new OperationStatus() { Success = true };
            var counter = 0;

            //Remove unused vaccine and treatments
            foreach (var vaccineOrTreatment in serverItems)
            {
                if (clientItems.All(c => c.id != vaccineOrTreatment.VaccineOrTreatmentId))
                {
                    string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                    Resources.Auditing.Module, Resources.Auditing.NextVaccineAndTreatments,
                                                    Resources.Auditing.Action, Resources.Auditing.Remove,
                                                    Resources.Auditing.ParentItemName, RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetPriceListItemNameByVaccineId(vaccineOrTreatment.ParentVaccineOrTreatmentId),
                                                    Resources.Auditing.ItemName, RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetPriceListItemNameByVaccineId(vaccineOrTreatment.VaccineOrTreatmentId),
                                                    Resources.Auditing.DaysToNext, vaccineOrTreatment.DaysToNext);
                    RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.VaccineAndTreatments, logMessage, "");
                    RapidVetUnitOfWork.VaccineAndTreatmentRepository.RemoveNextVoTItem(vaccineOrTreatment.Id);
                }
            }

            //AddOrUpdate the New vaccine and treatments
            foreach (var item in clientItems)
            {
                var dbItem = RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetNextVoTItem(id, item.id);

                if (dbItem == null)
                {
                    RapidVetUnitOfWork.VaccineAndTreatmentRepository.AddNextVoTItem(new NextVaccineOrTreatment()
                    {
                        ParentVaccineOrTreatmentId = id,
                        VaccineOrTreatmentId = item.id,
                        DaysToNext = item.days,
                        Created = DateTime.Now
                    });
                    counter++;
                    string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                  Resources.Auditing.Module, Resources.Auditing.NextVaccineAndTreatments,
                                                  Resources.Auditing.Action, Resources.Auditing.Add,
                                                  Resources.Auditing.ParentItemName, RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetPriceListItemNameByVaccineId(id),
                                                  Resources.Auditing.ItemName, RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetPriceListItemNameByVaccineId(item.id),
                                                  Resources.Auditing.DaysToNext, item.days);
                    RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.VaccineAndTreatments, logMessage, "");
                   
                }
                else
                {
                    if (dbItem.DaysToNext != item.days)
                    {
                        counter++;
                        string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                    Resources.Auditing.Module, Resources.Auditing.NextVaccineAndTreatments,
                                                    Resources.Auditing.Action, Resources.Auditing.Update,
                                                    Resources.Auditing.ParentItemName, RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetPriceListItemNameByVaccineId(id),
                                                    Resources.Auditing.ItemName, RapidVetUnitOfWork.VaccineAndTreatmentRepository.GetPriceListItemNameByVaccineId(item.id),
                                                    Resources.Auditing.DaysToNext, item.days);
                        RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.VaccineAndTreatments, logMessage, Resources.Auditing.DaysToNext + ": " + dbItem.DaysToNext);
                 
                        dbItem.DaysToNext = item.days;
                        dbItem.Updated = DateTime.Now;
                    }

                }
            }

            status = RapidVetUnitOfWork.Save();

            //this changes all performed vaccines by new definition.
            //if (counter > 0)
            //{
            //    RapidVetUnitOfWork.VaccineAndTreatmentRepository.TemplateCahnged(CurrentUser.ActiveClinicId);
            //}

            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage("הפעולה נכשלה");
            }
            return new JsonResult() { Data = status.Success };
        }
    }
}
