﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Patients;
using RapidVet.Repository.Patients;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Controllers
{
    [ClinicGroupManager]
    public class AnimalColorsController : BaseController
    {
        public ActionResult Index()
        {
            var colors = RapidVetUnitOfWork.AnimalColorRepository.GetList(CurrentUser.ActiveClinicGroupId);
            var model = colors.AnimalColors.Where(c => c.Active).Select(AutoMapper.Mapper.Map<AnimalColor, AnimalColorModel>).ToList();
            return View(model);
        }

        //id is clinicGroupId
        public PartialViewResult Create()
        {
            var model = new AnimalColorModel()
                {
                    ClinicGroupId = CurrentUser.ActiveClinicGroupId
                };
            return PartialView("_Create", model);

        }

        [HttpPost]
        public ActionResult Create(AnimalColorModel model)
        {


            var color = AutoMapper.Mapper.Map<AnimalColorModel, AnimalColor>(model);
            color.Active = true;
            color.Id = 0;
            color.ClinicGroupId = CurrentUser.ActiveClinicGroupId;
            var status = RapidVetUnitOfWork.AnimalColorRepository.Create(color);
            return RedirectToAction("Index", "AnimalColors");

        }

        //id is animalColorId
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var color = RapidVetUnitOfWork.AnimalColorRepository.GetItem(id);
            if (color.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<AnimalColor, AnimalColorModel>(color);
            return PartialView("_Edit", model);
        }

        //id is animalColorId
        [HttpPost]
        public ActionResult Edit(AnimalColorModel model)
        {

            var color = RapidVetUnitOfWork.AnimalColorRepository.GetItem(model.Id);
            if (color.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            AutoMapper.Mapper.Map(model, color);

            var status = RapidVetUnitOfWork.AnimalColorRepository.Update(color);
            return RedirectToAction("Index", "AnimalColors");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var color = RapidVetUnitOfWork.AnimalColorRepository.GetItem(id);
            if (color.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<AnimalColor, AnimalColorModel>(color);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public ActionResult Delete(AnimalColorModel model)
        {
            var color = RapidVetUnitOfWork.AnimalColorRepository.GetItem(model.Id);
            if (color.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            color.Active = false;
            var status = RapidVetUnitOfWork.AnimalColorRepository.Update(color);
            return RedirectToAction("Index", "AnimalColors");

        }
    }
}
