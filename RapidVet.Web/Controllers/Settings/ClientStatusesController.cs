﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.RapidConsts.Controllers;
using RapidVet.Repository.Clients;
using RapidVet.WebModels.Clients;

namespace RapidVet.Web.Controllers
{
    public class ClientStatusesController : BaseController
    {


        public ActionResult Index()
        {
            var statuses = RapidVetUnitOfWork.ClientStatusRepository.GetAll(CurrentUser.ActiveClinicGroupId).ToList();
            var model = statuses.Select(AutoMapper.Mapper.Map<ClientStatus, ClientStatusModel>).ToList();
            return View(model);
        }

        [HttpGet]
        public PartialViewResult Create()
        {
            var model = new ClientStatusModel()
                {
                    ClinicGroupId = CurrentUser.ActiveClinicGroupId
                };
            return PartialView("_Create", model);
        }

        [HttpPost]
        public ActionResult Create(ClientStatusModel model)
        {
            var clientStatus = AutoMapper.Mapper.Map<ClientStatusModel, ClientStatus>(model);
            clientStatus.Active = true;
            clientStatus.Id = 0;
            clientStatus.ClinicGroupId = CurrentUser.ActiveClinicGroupId;
            var status = RapidVetUnitOfWork.ClientStatusRepository.Create(clientStatus);
            return RedirectToAction("Index", "ClientStatuses");
        }

        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            var clientStatus = RapidVetUnitOfWork.ClientStatusRepository.GetItem(id);
            var model = AutoMapper.Mapper.Map<ClientStatus, ClientStatusModel>(clientStatus);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(ClientStatusModel model)
        {
            var clientStatus = RapidVetUnitOfWork.ClientStatusRepository.GetItem(model.Id);
            AutoMapper.Mapper.Map(model, clientStatus);
            var status = RapidVetUnitOfWork.ClientStatusRepository.Update(clientStatus);
            return RedirectToAction("Index", "ClientStatuses");
        }

        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            var clientStatus = RapidVetUnitOfWork.ClientStatusRepository.GetItem(id);
            var model = AutoMapper.Mapper.Map<ClientStatus, ClientStatusModel>(clientStatus);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public ActionResult Delete(ClientStatusModel model)
        {
            var clientStatus = RapidVetUnitOfWork.ClientStatusRepository.GetItem(model.Id);
            clientStatus.Active = false;
            var status = RapidVetUnitOfWork.ClientStatusRepository.Update(clientStatus);
            return RedirectToAction("Index", "ClientStatuses");
        }


    }
}
