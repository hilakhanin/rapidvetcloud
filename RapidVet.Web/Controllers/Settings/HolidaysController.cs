﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.FullCalender;

namespace RapidVet.Web.Controllers
{
    public class HolidaysController : BaseController
    {
        public ActionResult Index()
        {
            var holidays = RapidVetUnitOfWork.HolidayRepository.GetHolidaysForClinic(CurrentUser.ActiveClinicId);
            return View(holidays.OrderBy(r => r.StartDay).ToList());
        }

        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return View(new Holiday
                        {
                            ClinicId = CurrentUser.ActiveClinicId,
                            StartDay = DateTime.Now,
                            EndDay = DateTime.Now,
                            StartDayString = DateTime.Now.ToString("dd/MM/yyyy"),
                            EndDayString = DateTime.Now.ToString("dd/MM/yyyy")
                        });
            }
            else
            {
                var holiday = RapidVetUnitOfWork.HolidayRepository.GetHoliday(id.Value);
                if (holiday == null)
                {
                    return View("Error");
                }

                holiday.StartDayString = holiday.StartDay.ToString("dd/MM/yyyy");
                holiday.EndDayString = holiday.EndDay.ToString("dd/MM/yyyy");
                return View(holiday);
            }
        }

        [HttpPost]
        public ActionResult Edit(Holiday model)
        {
            DateTime from;
            DateTime to;
            if (string.IsNullOrWhiteSpace(model.StartDayString) || string.IsNullOrWhiteSpace(model.EndDayString))
            {
                to = DateTime.Now;
                from = to.AddDays(-7);
            }
            else
            {
                from = DateTime.MinValue;
                to = DateTime.MinValue;
                var format = "dd/MM/yyyy";
                var unparsedFrom = model.StartDayString;
                DateTime.TryParseExact(unparsedFrom, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out from);
                if (from == DateTime.MinValue)
                {
                    from = DateTime.Now;
                }
                var unparsedTo = model.EndDayString;
                DateTime.TryParseExact(unparsedTo, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out to);
                if (to == DateTime.MinValue)
                {
                    to = DateTime.Now;
                }
            }
            model.StartDay = from;
            model.EndDay = to;

            OperationStatus status;
            //Create 
            if (model.Id == 0)
            {
                status = RapidVetUnitOfWork.HolidayRepository.Create(model);
            }
            //Update
            else
            {
                var holidayDb = RapidVetUnitOfWork.HolidayRepository.GetHoliday(model.Id);
                AutoMapper.Mapper.Map(model, holidayDb);
                status = RapidVetUnitOfWork.HolidayRepository.Update(holidayDb);
            }
            return RedirectToAction("Index");
        }

        // Delete lab test template directory
        public PartialViewResult Remove(int id)
        {
            var holiday = RapidVetUnitOfWork.HolidayRepository.GetHoliday(id);
            return PartialView("_Delete", holiday);
        }

        [HttpPost]
        public ActionResult Remove(Holiday model)
        {
            var status = RapidVetUnitOfWork.HolidayRepository.Remove(model.Id);
            return RedirectToAction("Index", "Holidays");
        }
    }
}