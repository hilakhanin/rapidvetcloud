﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.Data.OData.Query;
using RapidVet.Exeptions;
using RapidVet.Model.Clinics;
using RapidVet.Web.Attributes;
using RapidVet.WebModels.Clinics;
using RapidVetMembership;
using System.Security;

namespace RapidVet.Web.Controllers
{
    public class AttendanceLogController : BaseController
    {
        private const string FAILSTR = "0";
        public ActionResult Index()
        {
            var model = new AttendanceLoginWebModel();
            var selectList = new List<SelectListItem>();

            //populate dropdown by permission
            if (CurrentUser.IsAdministrator || CurrentUser.IsClinicManager || CurrentUser.IsClinicGroupManager)
            {
                var users = RapidVetUnitOfWork.UserRepository.GetListByClinicGroup(CurrentUser.ActiveClinicGroupId);

                foreach (var user in users)
                {
                    if (user.Active)
                    {
                        var selectListItem = new SelectListItem() { Text = user.Name, Value = user.Id.ToString() };
                        selectList.Add(selectListItem);
                    }
                }
            }
            else
            {
                selectList.Add(new SelectListItem() { Text = CurrentUser.Name, Value = CurrentUser.Id.ToString() });
            }

            model.Users = selectList;

            //mark login / logout
            model.LogIn = RapidVetUnitOfWork.AttendanceLogRepository.GetTodayLastLogIn(CurrentUser.ActiveClinicId, CurrentUser.Id) == null ? true : false;
            model.LastLogIn = RapidVetUnitOfWork.AttendanceLogRepository.GetLastRecordedLogIn(CurrentUser.ActiveClinicId, CurrentUser.Id);
            model.LastLogOut = RapidVetUnitOfWork.AttendanceLogRepository.GetLastRecordedLogOut(CurrentUser.ActiveClinicId, CurrentUser.Id);

            return View(model);
        }


        [HttpPost]
        public ActionResult Log(AttendanceLoginWebModel model)
        {
            if (ModelState.IsValid)
            {
                //UserValidationStatus status;
                var user = RapidVetUnitOfWork.UserRepository.GetItem(model.UserId);
                if (user.IsPasswordValid(model.Password))
                {
                    //var lastAttendanceLogIn = RapidVetUnitOfWork.AttendanceLogRepository.GetLastLogIn(CurrentUser.ActiveClinicId, user.Id);
                    if (model.LogIn)//lastAttendanceLogIn == null
                    {
                        RapidVetUnitOfWork.AttendanceLogRepository.CreateNewLog(CurrentUser.ActiveClinicId, user.Id, DateTime.Now, null);
                        return RedirectToAction("LogIn");
                    }
                    var lastAttendanceLogIn = RapidVetUnitOfWork.AttendanceLogRepository.GetTodayLastLogIn(CurrentUser.ActiveClinicId, user.Id);

                    if (lastAttendanceLogIn == null)
                    {
                        RapidVetUnitOfWork.AttendanceLogRepository.CreateNewLog(CurrentUser.ActiveClinicId, user.Id, null, DateTime.Now);
                    }
                    else
                    {
                        lastAttendanceLogIn.EndTime = DateTime.Now;
                        RapidVetUnitOfWork.Save();
                    }

                    return RedirectToAction("LogOut");
                }
                else
                {
                    ModelState.AddModelError("", RapidVet.Resources.GlobalValidationErrors.InvalidPassword);
                }
            }
            var users = RapidVetUnitOfWork.UserRepository.GetListByClinicGroup(CurrentUser.ActiveClinicGroupId);
            var selectList = new List<SelectListItem>();
            foreach (var u in users)
            {
                var selectListItem = new SelectListItem() { Text = u.Name, Value = u.Id.ToString() };
                selectList.Add(selectListItem);
            }
            model.Users = selectList;
            return View("Index", model);
        }

        public ActionResult LogIn()
        {
            return View();
        }

        public ActionResult LogOut()
        {
            return View();
        }


        public ActionResult LogReport(int? userId, string fromTime, string toTime, bool toPrint = false)
        {
            ViewBag.ToPrint = toPrint;
            DateTime from;
            DateTime to;
            if (string.IsNullOrWhiteSpace(fromTime) || string.IsNullOrWhiteSpace(toTime))
            {
                to = DateTime.Now;
                from = to.AddDays(-7);
            }
            else
            {
                from = DateTime.MinValue;
                to = DateTime.MinValue;
                var format = "dd/MM/yyyy";
                var unparsedFrom = fromTime;
                DateTime.TryParseExact(unparsedFrom, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out from);
                if (from == DateTime.MinValue)
                {
                    from = DateTime.Now;
                }
                var unparsedTo = toTime;
                DateTime.TryParseExact(unparsedTo, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out to);
                if (to == DateTime.MinValue)
                {
                    to = DateTime.Now;
                }
            }
            var model = new AttendanceLogsReportModel();
            var selectList = new List<SelectListItem>();

            if (CurrentUser.IsAdministrator || CurrentUser.IsClinicManager || CurrentUser.IsClinicGroupManager)
            {
                var users = RapidVetUnitOfWork.UserRepository.GetListByClinicGroup(CurrentUser.ActiveClinicGroupId);

                foreach (var u in users)
                {
                    if (u.Active)
                    {
                        var selectListItem = new SelectListItem() { Text = u.Name, Value = u.Id.ToString() };
                        selectList.Add(selectListItem);
                    }
                }
                selectList.Insert(0, new SelectListItem() { Text = Resources.AttendanceLog.AllEmployees, Value = "" });
            }
            else
            {
                selectList.Add(new SelectListItem() { Text = CurrentUser.Name, Value = CurrentUser.Id.ToString() });
            }

            model.Users = selectList;

            if (userId.HasValue)
            {
                model.AttendanceLogs = AutoMapper.Mapper.Map<List<AttendanceLog>, List<AttendanceLogWebModel>>
                    (RapidVetUnitOfWork.AttendanceLogRepository.GetAttendanceLogsOfUser(CurrentUser.ActiveClinicId, userId.Value, from, to));
                model.HourSums = new Dictionary<string, decimal>();
            }
            else
            {
                var attendanceLogs = RapidVetUnitOfWork.AttendanceLogRepository.GetAllUsersAttendanceLogsOfClinic(CurrentUser.ActiveClinicId, from, to);
                var sums = new Dictionary<string, decimal>();
                foreach (var attendanceLog in attendanceLogs)
                {
                    if (attendanceLog.EndTime.HasValue && attendanceLog.StartTime.HasValue)
                    {
                        var totalSecs = (decimal)(attendanceLog.EndTime.Value - attendanceLog.StartTime.Value).TotalSeconds;
                        if (sums.ContainsKey(attendanceLog.User.Name))
                        {
                            sums[attendanceLog.User.Name] += totalSecs;
                        }
                        else
                        {
                            sums.Add(attendanceLog.User.Name, totalSecs);
                        }
                    }
                }
                model.HourSums = sums;
                model.AttendanceLogs = new List<AttendanceLogWebModel>();
            }
            return View(model);
        }

        
        public ActionResult WeeklyLogReport(string dayOfWeek)
        {
            DateTime day;
            if (string.IsNullOrWhiteSpace(dayOfWeek))
            {
                day = DateTime.Now;
            }
            else
            {
                day = DateTime.MinValue;
                var format = "dd/MM/yyyy";
                var unparsed = dayOfWeek;
                DateTime.TryParseExact(unparsed, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out day);
                if (day == DateTime.MinValue)
                {
                    day = DateTime.Now;
                }
            }
            var weekStart = day.StartOfWeek(DayOfWeek.Sunday);
            List<AttendanceLog> attendanceLogs;
            
            if (CurrentUser.IsAdministrator || CurrentUser.IsClinicManager || CurrentUser.IsClinicGroupManager)
            {
                attendanceLogs = RapidVetUnitOfWork.AttendanceLogRepository.GetAllUsersAttendanceLogsOfClinic(CurrentUser.ActiveClinicId, weekStart, weekStart.AddDays(7));
            }
            else
            {
                attendanceLogs = RapidVetUnitOfWork.AttendanceLogRepository.GetAttendanceLogsOfUser(CurrentUser.ActiveClinicId, CurrentUser.Id, weekStart, weekStart.AddDays(7));
            }
            return View(attendanceLogs);
        }

        [ExcelExport]
        public ActionResult DownloadCsv(int? userId, string fromTime, string toTime)
        {
            DateTime from;
            DateTime to;
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8 };
            var csv = new CsvWriter(writer, csvConfig);

            csv.Configuration.HasHeaderRecord = false;

            if (string.IsNullOrWhiteSpace(fromTime) || string.IsNullOrWhiteSpace(toTime))
            {
                to = DateTime.Now;
                from = to.AddDays(-7);
            }
            else
            {
                from = DateTime.MinValue;
                to = DateTime.MinValue;
                var format = "dd/MM/yyyy";
                var unparsedFrom = fromTime;
                DateTime.TryParseExact(unparsedFrom, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out from);
                if (from == DateTime.MinValue)
                {
                    from = DateTime.Now;
                }
                var unparsedTo = toTime;
                DateTime.TryParseExact(unparsedTo, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out to);
                if (to == DateTime.MinValue)
                {
                    to = DateTime.Now;
                }
            }
            var model = new AttendanceLogsReportModel();
            if (userId.HasValue)
            {
                model.AttendanceLogs = AutoMapper.Mapper.Map<List<AttendanceLog>, List<AttendanceLogWebModel>>
                    (RapidVetUnitOfWork.AttendanceLogRepository.GetAttendanceLogsOfUser(CurrentUser.ActiveClinicId, userId.Value, from, to));
                model.HourSums = new Dictionary<string, decimal>();
                csv.WriteRecord(new
                {
                    c1 = Resources.AttendanceLog.Date,
                    c2 = Resources.AttendanceLog.EmployeeName,
                    c3 = Resources.AttendanceLog.HourIn,
                    c4 = Resources.AttendanceLog.HourOut,
                    c5 = Resources.AttendanceLog.TotalHours
                });
            }
            else
            {
                var attendanceLogs = RapidVetUnitOfWork.AttendanceLogRepository.GetAllUsersAttendanceLogsOfClinic(CurrentUser.ActiveClinicId, from, to);
                var sums = new Dictionary<string, decimal>();
                foreach (var attendanceLog in attendanceLogs)
                {
                    if (attendanceLog.EndTime.HasValue && attendanceLog.StartTime.HasValue)
                    {
                        var totalSecs = (decimal)(attendanceLog.EndTime.Value - attendanceLog.StartTime.Value).TotalSeconds;
                        if (sums.ContainsKey(attendanceLog.User.Name))
                        {
                            sums[attendanceLog.User.Name] += totalSecs;
                        }
                        else
                        {
                            sums.Add(attendanceLog.User.Name, totalSecs);
                        }
                    }
                }
                model.HourSums = sums;
                model.AttendanceLogs = new List<AttendanceLogWebModel>();

                csv.WriteRecord(new
                {
                    c1 = Resources.AttendanceLog.EmployeeName,
                    c2 = Resources.AttendanceLog.TotalHours
                });
            }

            if (model.HourSums.Any())
            {
                var fixToHourSums = model.HourSums.ToDictionary(sum => sum.Key, sum => new TimeSpan(0, 0, (int)sum.Value).ToString().Substring(0, 8));
                csv.WriteRecords(fixToHourSums);
            }
            if (model.AttendanceLogs.Any())
            {
                double totalSecs = 0;// = (decimal)(attendanceLog.EndTime.Value - attendanceLog.StartTime).TotalSeconds;
                foreach (AttendanceLogWebModel atw in model.AttendanceLogs)
                {
                    csv.WriteRecord(new
                    {
                        c1 = atw.StartTime.HasValue ? atw.StartTime.Value.Date.ToString("dd/MM/yyyy") : atw.EndTime.Value.Date.ToString("dd/MM/yyyy"),
                        c2 = atw.UserName,
                        c3 = atw.StartTime.HasValue ? atw.StartTime.Value.ToString("HH:mm:ss") : "",
                        c4 = atw.EndTime.HasValue ? atw.EndTime.Value.ToString("HH:mm:ss") : "",
                        c5 = atw.EndTime.HasValue && atw.StartTime.HasValue ? atw.EndTime.Value.Subtract(atw.StartTime.Value).ToString().Substring(0, 8) : ""
                    });

                    if (atw.EndTime.HasValue && atw.StartTime.HasValue)
                        totalSecs += atw.EndTime.Value.Subtract(atw.StartTime.Value).TotalSeconds;
                }

                csv.WriteRecord(new
                {
                    c1 = Resources.AttendanceLog.Total,
                    c2 = "",
                    c3 = "",
                    c4 = "",
                    c5 = new TimeSpan(0, 0, (int)totalSecs).ToString().Substring(0, 8)
                });
            }
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", "AttendantLogs.csv");
        }

        public string SaveLogReport(int id, int? userID, string startTime, string endTime, string startDate)
        {
            try
            {
                TimeSpan tS;
                TimeSpan tE;
                DateTime dS;
                if (!TimeSpan.TryParse(startTime, out tS) || !TimeSpan.TryParse(endTime, out tE) ||
                    !DateTime.TryParse(startDate, out dS) || !userID.HasValue || tS == TimeSpan.Zero || tE == TimeSpan.Zero || Convert.ToInt32(this.Request.Form["userID"]) != userID.Value)
                {
                    throw new Exception("שגיאה בהמרת הזמנים");
                }

                if (tS > tE)
                    throw new Exception("זמן ההתחלה גדול מזמן הסיום");

                if (id == -1)
                {
                    var r = RapidVetUnitOfWork.AttendanceLogRepository.CreateNewLog(CurrentUser.ActiveClinicId, userID.Value, dS.Add(tS), dS.Add(tE));
                    return r.Success ? "1" : FAILSTR;
                }

                var b = RapidVetUnitOfWork.AttendanceLogRepository.SaveLogReport(CurrentUser.ActiveClinicId, id, userID.Value, dS, tS, tE);
                return b ? "1" : FAILSTR;
            }
            catch (Exception ex)
            {
                SetErrorMessage(ex.Message);
                return FAILSTR;
            }
        }  

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var logReport = RapidVetUnitOfWork.AttendanceLogRepository.GetItem(CurrentUser.ActiveClinicId, id);
            if (logReport == null)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<AttendanceLog, AttendanceLogWebModel>(logReport);
            return PartialView("_Delete", model);
        }

        public JsonResult Delete(int id, int userId)
        {
            var result = new JsonResult();
            var r = RapidVetUnitOfWork.AttendanceLogRepository.DeleteLogReport(CurrentUser.ActiveClinicId, id, userId);
            result.Data = new
            {
                success = r,
                redirectUrl = Request.UrlReferrer,
                isRedirect = true,
                indexId = RapidVet.RapidConsts.Controllers.AttendanceLogConsts.TABLE_ID,
                modalId = RapidVet.RapidConsts.Controllers.AttendanceLogConsts.MODAL_ID
            };          

            return result;
        }
    }
}