﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using Newtonsoft.Json;
using RapidVet.Exeptions;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.RapidConsts;
using RapidVet.RapidConsts.Controllers;
using RapidVet.Repository;
using RapidVet.Repository.Clinics;
using RapidVet.WebModels;
using RapidVet.Model.Users;
using RapidVet.WebModels.Users;
using RapidVetMembership;
using System.Text;
using AutoMapper;

namespace RapidVet.Web.Controllers
{

    [Authorize]
    public class UsersController : BaseController
    {
        [ClinicManager]
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult IndexGrid()
        {
            string ActiveStatus = "All";
            if (Request.QueryString["ActiveStatus"] != null)
            {
                ActiveStatus = Request.QueryString["ActiveStatus"].ToString();
                ViewBag.ActiveStatus = ActiveStatus;
            }
            else
            {
                ActiveStatus = "OnlyActive";
                ViewBag.ActiveStatus = ActiveStatus;
            }


            var model = RapidVetUnitOfWork.UserRepository.GetListByClinicGroup(CurrentUser.ActiveClinicGroupId, ActiveStatus);
            updateClinicUsers();
            return PartialView("_IndexGrid", model);
        }

        [HttpGet]
        [ClinicManager]
        public PartialViewResult Create()
        {
            return PartialView("_Create", new UserModel());
        }

        [HttpPost]
        [ClinicManager]
        public JsonResult Create(UserModel model)
        {

            var result = new JsonResult();
            //if (isUserQuotaFree())
            //{
            if (RapidVetUnitOfWork.UserRepository.GetItemByUserName(model.Username) != null)
            {
                result.Data = new
                    {
                        success = false,
                        data = RenderPartialViewToString("_Create", model)
                    };
                return result;
            }

            if (ModelState.IsValid && !string.IsNullOrWhiteSpace(model.Password))
            {
                //var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);
                //if (clinicGroup.Users.Count(u => u.Active) >= CurrentUser.ActiveClinicGroup.MaxUsers)
                //{
                //    SetErrorMessage(Resources.GlobalValidationErrors.MaxUsersReached);
                //    result.Data = new
                //    {
                //        success = false,
                //        data = RenderPartialViewToString("_Create", model)
                //    };
                //    return result;
                //}
                var user = RapidVetUnitOfWork.UserRepository.CreateUser(model.Username, model.Password, model.Email);
                if (user != null)
                {
                    user.Active = true;
                    user.TitleId = model.TitleId;
                    user.FirstName = model.FirstName ?? "";
                    user.LastName = model.LastName;
                    user.MobilePhone = model.MobilePhone;
                    user.LicenceNumber = model.LicenceNumber;
                    user.ClinicGroupId = CurrentUser.ActiveClinicGroupId;


                    var status = RapidVetUnitOfWork.UserRepository.Save(user);

                    result.Data = new
                        {
                            success = user != null,
                            target = Url.Action("IndexGrid"),
                            indexId = UserConsts.GRID_ID,
                            modalId = UserConsts.MODAL_ID
                        };
                }
                updateClinicUsers();
            }
            else
            {

                if (string.IsNullOrWhiteSpace(model.Password))
                {

                    ModelState.AddModelError("Password", Resources.GlobalValidationErrors.Required);
                    ModelState.AddModelError("PasswordReType", Resources.GlobalValidationErrors.Required);
                }
                else if (model.Password.Length < 6)
                {
                    ModelState.AddModelError("Password", string.Format(Resources.GlobalValidationErrors.MinLength, 0, 0, 6));
                }
                result.Data = new
                    {
                        success = false,
                        data = RenderPartialViewToString("_Create", model)
                    };
            }
            //}

            result.Data = new
            {
                success = true,
                target = Url.Action("IndexGrid"),
                indexId = UserConsts.GRID_ID,
                modalId = UserConsts.MODAL_ID
            };

            return result;

        }

        private void updateClinicUsers()
        {
            CurrentUser.ActiveClinicGroup.Users.Clear();
            var clinicUsers = RapidVetUnitOfWork.UserRepository.GetListByClinicGroup(CurrentUser.ActiveClinicGroupId);

            foreach (var u in clinicUsers)
                CurrentUser.ActiveClinicGroup.Users.Add(u);
        }

        [HttpGet]
        [ClinicManager]
        public PartialViewResult Edit(int id)
        {
            var user = RapidVetUnitOfWork.UserRepository.GetItem(id);
            if (user == null)
            {
                throw new UserNotFoundException();
            }
            if (user.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<User, UserModel>(user);

            return PartialView("_Edit", model);
        }



        [HttpPost]
        [ClinicManager]
        public JsonResult Edit(UserModel model)
        {
            var result = new JsonResult();

            if (ModelState.IsValid)
            {
                var user = RapidVetUnitOfWork.UserRepository.GetItem(model.Id);
                if (user.ClinicGroupId != CurrentUser.ActiveClinicGroupId && !CurrentUser.IsAdministrator)
                {
                    throw new SecurityException(Resources.Exceptions.AccessDenied);
                }

                if (model.Password.Equals(Resources.User.GENERIC_PASSWORD))
                {
                    model.Password = null;
                    model.PasswordReType = null;
                }

                AutoMapper.Mapper.Map(model, user);

                if (!string.IsNullOrWhiteSpace(model.Password))
                {
                    user.Password = model.Password;
                    user.LastPasswordChangedDate = DateTime.Now;
                }

                if (!model.DefaultDrId.HasValue)
                {
                    user.DefaultDr = null;
                    user.DefaultDrId = null;
                }

                if (model != null && !model.DefaultIssuerEmployerId.HasValue)
                    user.DefaultIssuerEmployerId = null;

                var status = RapidVetUnitOfWork.UserRepository.Save(user);

                result.Data = new
                    {
                        success = status.Message == null,
                        target = Url.Action("IndexGrid"),
                        indexId = UserConsts.GRID_ID,
                        modalId = UserConsts.MODAL_ID
                    };

            }
            else
            {
                result.Data = new
                    {
                        success = false,
                        data = RenderPartialViewToString("_Edit", model)
                    };
            }

            return result;
        }

        //id is user Id
        // this action displays a confirmation message
        [HttpGet]
        [ClinicManager]
        public ActionResult Delete(int id)
        {
            var user = RapidVetUnitOfWork.UserRepository.GetItem(id);
            if (user == null)
            {
                throw new UserNotFoundException();
            }
            if (user.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<User, UserModel>(user);
            return PartialView("_Delete", model);
        }

        [HttpGet]
        [ClinicManager]
        public ActionResult DeleteAll()
        {
            var model = RapidVetUnitOfWork.UserRepository.GetListByClinicGroup(CurrentUser.ActiveClinicGroupId).ToList();
            return PartialView("_Delete", new UserModel());
        }

        [HttpGet]
        [ClinicManager]
        public ActionResult ActivateAll()
        {
            var model = RapidVetUnitOfWork.UserRepository.GetListByClinicGroup(CurrentUser.ActiveClinicGroupId);
            return PartialView("_ReactivateUser", new UserModel());
        }

        //followed by the confirmation message, this method deletes the user
        [ClinicManager]
        public JsonResult Delete(int id, string name)
        {
            var result = new JsonResult();
            OperationStatus status = null;
            if (id == 0)
                status = RapidVetUnitOfWork.UserRepository.SwitchUsersIdActive(CurrentUser.ActiveClinicGroupId, false);
            else
            {
                var user = RapidVetUnitOfWork.UserRepository.GetItem(id);
                user.IsApproved = false;
                user.IsLockedOut = true;
                user.Active = false;
                status = RapidVetUnitOfWork.UserRepository.Save(user);

                //CurrentUser = null;
                var updateCurrentUser = CurrentUser.ActiveClinicGroup.Users.First(u => u.Id == id);
                updateCurrentUser.IsApproved = false;
                updateCurrentUser.IsLockedOut = true;
                updateCurrentUser.Active = false;
            }

            result.Data = new
                {
                    success = status.Message == null,
                    target = Url.Action("IndexGrid"),
                    indexId = UserConsts.GRID_ID,
                    modalId = UserConsts.MODAL_ID
                };

            //RapidVetUnitOfWork.UserRepository.RefreshRepository();

            return result;
        }

        public ActionResult EditProfile()
        {
            var model = AutoMapper.Mapper.Map<Model.Users.User, UsersSettingsWebModel>(CurrentUser);
            var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctorsForSchedule(CurrentUser.ActiveClinicId, CurrentUser.Id);
            List<User> doctorsToReturn = doctors.Select(Mapper.Map<Model.Users.User, User>).ToList();
            foreach (var doctor in doctors)
            {
                if (RapidVetUnitOfWork.ClinicRepository.GetUserByUN(doctor.Username) != null)
                    doctorsToReturn.Remove(doctor);
            }

            model.ShowUsersList = doctorsToReturn.Select(x => new UsersSettingsShowUserModel()
            {
                Id = x.UsersDoctorsViewId,
                User = x.Name,
                Show = x.ShowOnMainWin,
                DoctorId = x.Id
            }).ToList();
            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditProfile(UsersSettingsWebModel model)
        {
            if (ModelState.IsValid)
            {
                var _currentUser = CurrentUser;
                var user = RapidVetUnitOfWork.UserRepository.GetItem(CurrentUser.Id, true);
                AutoMapper.Mapper.Map(model, user);
                if (!model.DefaultIssuerId.HasValue)
                {
                    user.DefaultIssuerEmployerId = null;
                }
                if (_currentUser != null && _currentUser.IsAdministrator)
                {
                    user.ActiveClinicGroup = _currentUser.ActiveClinicGroup;
                    user.ActiveClinicId = _currentUser.ActiveClinicId;
                }
                UpdatCurrentUser(user);
                RapidVetUnitOfWork.ClinicRepository.Save(CurrentUser.Id, CurrentUser.ActiveClinicId, model.ShowUsersList);
                RapidVetUnitOfWork.UserRepository.Save(user);
            }
            return RedirectToAction("Index", "Home");
        }

        //id is userId
        [ClinicManager]
        public PartialViewResult UserRolesTable(int id)
        {
            var model = new UserRolesModel()
                {
                    UserId = id
                };
            var roles = RapidVetUnitOfWork.UserRepository.GetRoles().ToList();

            var clinicManagerRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.CLINICMANAGER).RoleId;
            var doctorRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.DOCTOR).RoleId;
            var secretaryRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.SECRATERY).RoleId;
            var viewReportsRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.VIEW_REPORTS).RoleId;
            var viewFinancialReportsRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.VIEW_FINANCIAL_REPORTS).RoleId;
            var excelExportRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.EXCEL_EXPORT).RoleId;
            //var noDiary = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.NO_DIARY).RoleId;

            var clinics =
                RapidVetUnitOfWork.ClinicGroupRepository.GetClinicList(CurrentUser.ActiveClinicGroupId).ToList();

            foreach (var clinic in clinics)
            {
                var userClinicRoles = RapidVetUnitOfWork.UserRepository.GetUserClinicRoles(id, clinic.Id).ToList();

                model.ClinicRoles.Add(
                    new UserClinicRoles()
                        {
                            ClinicId = clinic.Id,
                            ClinicName = clinic.Name,
                            isClinicManager =
                                userClinicRoles.SingleOrDefault(r => r.RoleId == clinicManagerRoleId) != null,
                            isDoctor = userClinicRoles.SingleOrDefault(r => r.RoleId == doctorRoleId) != null,
                            isSecretary = userClinicRoles.SingleOrDefault(r => r.RoleId == secretaryRoleId) != null,
                            isViewReports = userClinicRoles.SingleOrDefault(r => r.RoleId == viewReportsRoleId) != null,
                            isViewFinancialReports = userClinicRoles.SingleOrDefault(r => r.RoleId == viewFinancialReportsRoleId) != null,
                            //isNoDiary = userClinicRoles.SingleOrDefault(r => r.RoleId == noDiary) != null,
                        });



            }


            return PartialView("_UserClinicRoles", model);
        }

        //id is userId
        [ClinicManager]
        public PartialViewResult IsClinicGroupManager(string userName)
        {
            if (Roles.IsUserInRole(userName, RapidConsts.RolesConsts.CLINICGROUPMANAGER))
            {
                return PartialView("_ClinicGroupManager");
            }
            return null;
        }

        //id is userId
        [HttpGet]
        [ClinicManager]
        public PartialViewResult EditRoles(int id)
        {

            var roles = RapidVetUnitOfWork.UserRepository.GetRoles().ToList();
            var clinicManagerRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.CLINICMANAGER).RoleId;
            var doctorRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.DOCTOR).RoleId;
            var secretaryRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.SECRATERY).RoleId;
            var viewReportsRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.VIEW_REPORTS).RoleId;
            var viewFinancialReportsRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.VIEW_FINANCIAL_REPORTS).RoleId;
            var excelExportRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.EXCEL_EXPORT).RoleId;
            var noDiaryRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.NO_DIARY).RoleId;

            var user = RapidVetUnitOfWork.UserRepository.GetItem(id);

            var model = new UserRolesEditModel()
                {
                    UserId = user.Id,
                    UserFullName = user.Name,
                    isClinicGroupManager =
                        Roles.IsUserInRole(user.Username, RapidConsts.RolesConsts.CLINICGROUPMANAGER) == true
                };


            var clinics =
                RapidVetUnitOfWork.ClinicGroupRepository.GetClinicList(CurrentUser.ActiveClinicGroupId).ToList();

            foreach (var clinic in clinics)
            {
                var userClinicRoles = RapidVetUnitOfWork.UserRepository.GetUserClinicRoles(id, clinic.Id).ToList();

                model.Roles.Add(
                    new UserClinicRoles()
                        {
                            ClinicId = clinic.Id,
                            ClinicName = clinic.Name,
                            isClinicManager = userClinicRoles.SingleOrDefault(r => r.RoleId == clinicManagerRoleId) != null,
                            isDoctor = userClinicRoles.SingleOrDefault(r => r.RoleId == doctorRoleId) != null,
                            isSecretary = userClinicRoles.SingleOrDefault(r => r.RoleId == secretaryRoleId) != null,
                            isExcelExport = userClinicRoles.SingleOrDefault(r => r.RoleId == excelExportRoleId) != null,
                            isViewFinancialReports = userClinicRoles.SingleOrDefault(r => r.RoleId == viewFinancialReportsRoleId) != null,
                            isViewReports = userClinicRoles.SingleOrDefault(r => r.RoleId == viewReportsRoleId) != null,
                            isNoDiary = userClinicRoles.SingleOrDefault(r => r.RoleId == noDiaryRoleId) != null
                        });
            }



            return PartialView("_EditRoles", model);
        }

        [HttpPost]
        [ClinicManager]
        public ActionResult EditRoles(UserRolesEditModel model)
        {
            model.Roles = GetUserClinicRoles();
            var roles = RapidVetUnitOfWork.UserRepository.GetRoles().ToList();
            var clinicManagerRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.CLINICMANAGER).RoleId;
            var doctorRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.DOCTOR).RoleId;
            var secretaryRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.SECRATERY).RoleId;
            var viewReportsRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.VIEW_REPORTS).RoleId;
            var viewFinancialReportsRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.VIEW_FINANCIAL_REPORTS).RoleId;
            var excelExportRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.EXCEL_EXPORT).RoleId;
            var noDiaryRoleId = roles.Single(r => r.RoleName == RapidConsts.RolesConsts.NO_DIARY).RoleId;

            var inventoryRoles = new List<String>()
                {
                    RolesConsts.INVENTORY_CREATE_ORDER,
                    RolesConsts.INVENTORY_ENTITY_MANAGEMENT,
                    RolesConsts.INVENTORY_RECIEVE_ORDER,
                    RolesConsts.INVENTORY_REPORTS,
                    RolesConsts.INVENTORY_UPDATE
                };

            var result = new JsonResult();
            if (ModelState.IsValid)
            {
                var userName = RapidVetUnitOfWork.UserRepository.GetUserName(model.UserId);
                var userClinicRoles = new List<UsersClinicsRoles>();


                //don't override inventory roles
                var userInventoryRoles =
                    RapidVetUnitOfWork.UserRepository.GetUserRoles(model.UserId)
                                      .Where(r => inventoryRoles.Contains(r.Role.RoleName));
                userClinicRoles.AddRange(userInventoryRoles);

                foreach (var userRole in model.Roles)
                {
                    foreach (var role in roles)
                    {

                        if (role.RoleName == RapidConsts.RolesConsts.SECRATERY)
                        {
                            if (userRole.isSecretary)
                            {
                                userClinicRoles.Add(new UsersClinicsRoles()
                                    {
                                        ClinicId = userRole.ClinicId,
                                        Role = role,
                                        RoleId = role.RoleId,
                                        UserId = model.UserId
                                    });
                            }
                        }
                        else if (role.RoleName == RapidConsts.RolesConsts.DOCTOR)
                        {
                            if (userRole.isDoctor)
                            {
                                userClinicRoles.Add(new UsersClinicsRoles()
                                    {
                                        ClinicId = userRole.ClinicId,
                                        Role = role,
                                        RoleId = role.RoleId,
                                        UserId = model.UserId
                                    });
                            }

                        }
                        else if (role.RoleName == RapidConsts.RolesConsts.CLINICMANAGER)
                        {
                            if (userRole.isClinicManager)
                            {
                                userClinicRoles.Add(new UsersClinicsRoles()
                                    {
                                        ClinicId = userRole.ClinicId,
                                        Role = role,
                                        RoleId = role.RoleId,
                                        UserId = model.UserId
                                    });
                            }

                        }
                        else if (role.RoleName == RapidConsts.RolesConsts.VIEW_REPORTS)
                        {
                            if (userRole.isViewReports)
                            {
                                userClinicRoles.Add(new UsersClinicsRoles()
                                {
                                    ClinicId = userRole.ClinicId,
                                    Role = role,
                                    RoleId = role.RoleId,
                                    UserId = model.UserId
                                });
                            }

                        }
                        else if (role.RoleName == RapidConsts.RolesConsts.VIEW_FINANCIAL_REPORTS)
                        {
                            if (userRole.isViewFinancialReports)
                            {
                                userClinicRoles.Add(new UsersClinicsRoles()
                                {
                                    ClinicId = userRole.ClinicId,
                                    Role = role,
                                    RoleId = role.RoleId,
                                    UserId = model.UserId
                                });
                            }

                        }

                        else if (role.RoleName == RapidConsts.RolesConsts.EXCEL_EXPORT)
                        {
                            if (userRole.isExcelExport)
                            {
                                userClinicRoles.Add(new UsersClinicsRoles()
                                {
                                    ClinicId = userRole.ClinicId,
                                    Role = role,
                                    RoleId = role.RoleId,
                                    UserId = model.UserId
                                });
                            }

                        }
                        else if (role.RoleName == RapidConsts.RolesConsts.NO_DIARY)
                        {
                            if (userRole.isNoDiary)
                            {
                                userClinicRoles.Add(new UsersClinicsRoles()
                                {
                                    ClinicId = userRole.ClinicId,
                                    Role = role,
                                    RoleId = role.RoleId,
                                    UserId = model.UserId
                                });
                            }

                        }
                    }
                }

                var isCurrentManager = Roles.IsUserInRole(userName, RapidConsts.RolesConsts.CLINICGROUPMANAGER);
                if (isCurrentManager != model.isClinicGroupManager)
                {
                    if (model.isClinicGroupManager)
                    {
                        Roles.AddUserToRole(userName, RapidConsts.RolesConsts.CLINICGROUPMANAGER);
                    }
                    else
                    {
                        Roles.RemoveUserFromRole(userName, RapidConsts.RolesConsts.CLINICGROUPMANAGER);
                    }
                }

                if (userClinicRoles.Any())
                {
                    RapidVetUnitOfWork.UserRepository.UpdateUserClinicsRoles(userClinicRoles);
                }

                //result.Data = new
                //    {
                //        success = true,
                //        target = Url.Action("IndexGrid"),
                //        indexId = UserConsts.TABLE_ID,
                //        modalId = UserConsts.MODAL_ID
                //    };
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            //else
            //{
            //    result.Data = new
            //        {
            //            success = false,
            //            data = RenderPartialViewToString("_EditRoles", model)
            //        };
            //}
            return RedirectToAction("Index");

        }

        //id is user id
        [HttpGet]
        [ClinicManager]
        public PartialViewResult ActivateUser(int id)
        {
            var user = RapidVetUnitOfWork.UserRepository.GetItem(id);
            if (user == null)
            {
                throw new UserNotFoundException();
            }
            if (user.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException(Resources.Exceptions.AccessDenied);
            }
            var model = AutoMapper.Mapper.Map<User, UserModel>(user);
            return PartialView("_ReactivateUser", model);
        }

        //id is user id
        [HttpPost]
        [ClinicManager]
        public JsonResult ActivateUser(int id, string name = "")
        {
            var result = new JsonResult();
            OperationStatus status = null;
            if (id == 0)
                status = RapidVetUnitOfWork.UserRepository.SwitchUsersIdActive(CurrentUser.ActiveClinicGroupId, true);
            else
            {
                var user = RapidVetUnitOfWork.UserRepository.GetItem(id);
                user.IsApproved = true;
                user.IsLockedOut = false;
                user.Active = true;

                status = RapidVetUnitOfWork.UserRepository.Save(user);
                //CurrentUser = null;
                var updateCurrentUser = CurrentUser.ActiveClinicGroup.Users.First(u => u.Id == id);
                updateCurrentUser.IsApproved = true;
                updateCurrentUser.IsLockedOut = false;
                updateCurrentUser.Active = true;
            }

            // RapidVetUnitOfWork.UserRepository.RefreshRepository();
            // }
            return new JsonResult()
                {
                    Data = new
                        {
                            success = true,//status.Message),
                            target = Url.Action("IndexGrid"),
                            indexId = UserConsts.GRID_ID,
                            modalId = UserConsts.MODAL_ID

                        }
                };
        }

        //private bool isUserQuotaFree()
        //{
        //    var users = RapidVetUnitOfWork.ClinicGroupRepository.GetClinicGroupUsers(CurrentUser.ActiveClinicId);
        //    return CurrentUser.ActiveClinicGroup.MaxUsers > users.Where(u => u.Active).Count();
        //}

        private List<UserClinicRoles> GetUserClinicRoles()
        {
            var userClinicRoles = new List<UserClinicRoles>();

            var keys = Request.Form.AllKeys;

            foreach (var key in keys)
            {
                var clinicId = 0;
                if (key.StartsWith("secretary") || key.StartsWith("doctor") || key.StartsWith("manager") || key.StartsWith("reports") || key.StartsWith("financial") || key.StartsWith("excel") || key.StartsWith("nodiary"))
                {
                    var tempId = key.Split('_');
                    if (tempId.Length > 1)
                    {
                        int.TryParse(tempId[1], out clinicId);

                        if (clinicId > 0)
                        {
                            if (userClinicRoles.SingleOrDefault(r => r.ClinicId == clinicId) == null)
                            {
                                userClinicRoles.Add(new UserClinicRoles()
                                    {
                                        ClinicId = clinicId,
                                        isSecretary = bool.Parse(Request.Form["secretary_" + clinicId].Split(',')[0]),
                                        isDoctor = bool.Parse(Request.Form["doctor_" + clinicId].Split(',')[0]),
                                        isClinicManager = bool.Parse(Request.Form["manager_" + clinicId].Split(',')[0]),
                                        isViewReports = bool.Parse(Request.Form["reports_" + clinicId].Split(',')[0]),
                                        isViewFinancialReports =
                                            bool.Parse(Request.Form["financial_" + clinicId].Split(',')[0]),
                                        isNoDiary = bool.Parse(Request.Form["nodiary_" + clinicId].Split(',')[0])
                                    });
                            }
                        }
                    }
                }
            }

            return userClinicRoles;
        }

        public JsonResult GetInventoryRoles(int id)
        {
            var userClinicRoles = RapidVetUnitOfWork.UserRepository.GetUserClinicRoles(id,
                                                                                       CurrentUser.ActiveClinicId);
            var roles = RapidVetUnitOfWork.UserRepository.GetRoles();
            var inventoryRoles = new List<Role>()
                {
                    roles.Single(r => r.RoleName == RolesConsts.INVENTORY_ENTITY_MANAGEMENT),
                    roles.Single(r => r.RoleName == RolesConsts.INVENTORY_CREATE_ORDER),
                    roles.Single(r => r.RoleName == RolesConsts.INVENTORY_RECIEVE_ORDER),
                    roles.Single(r => r.RoleName == RolesConsts.INVENTORY_UPDATE),
                    roles.Single(r => r.RoleName == RolesConsts.INVENTORY_REPORTS)
                };
            var model = inventoryRoles.Select(ir => new SelectListItem()
                {
                    Text = Resources.Roles.ResourceManager.GetString(ir.RoleName),
                    Value = ir.RoleId.ToString(),
                    Selected = userClinicRoles.SingleOrDefault(u => u.RoleId == ir.RoleId) != null
                }).ToList();


            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //id is user id
        [HttpPost]
        public JsonResult SaveInventoryRoles(int id, string roles)
        {
            if (!string.IsNullOrWhiteSpace(roles))
            {
                var inventoryRolesList = JsonConvert.DeserializeObject<List<SelectListItem>>(roles);
                var userClinicRoles = RapidVetUnitOfWork.UserRepository.GetUserClinicRoles(id,
                                                                                      CurrentUser.ActiveClinicId);
                foreach (var role in inventoryRolesList)
                {
                    var roleId = new Guid(role.Value);
                    var existingRole = userClinicRoles.SingleOrDefault(u => u.RoleId == roleId);
                    if (existingRole != null)
                    {
                        if (!role.Selected)
                        {
                            RapidVetUnitOfWork.UserRepository.RemoveUserRole(existingRole);
                        }
                    }
                    else if (role.Selected)
                    {
                        RapidVetUnitOfWork.UserRepository.AddClinicRole(new UsersClinicsRoles()
                            {
                                ClinicId = CurrentUser.ActiveClinicId,
                                UserId = id,
                                RoleId = roleId,
                            });
                    }
                }

                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetSuccessMessage();
                }
                return null;

            }
            SetErrorMessage();
            return null;

        }

        [ClinicManager]
        public ActionResult Online()
        {
            ConcurrentUserInfo.ClearInactiveUsers(CurrentUser);
            var model = ConcurrentUserInfo.ConcurrentUsers.Where(u => u.User.ActiveClinicGroupId == CurrentUser.ActiveClinicGroupId && u.User.Roles.All(r => r.RoleName != RolesConsts.ADMINISTRATOR));
            return View(model);
        }

        [ClinicManager]
        public ActionResult DisconnectUser(Guid id)
        {
            var userToRemove = ConcurrentUserInfo.ConcurrentUsers.SingleOrDefault(u => u.UserName == id);
            if (userToRemove != null)
            {
                ConcurrentUserInfo.ConcurrentUsers.Remove(userToRemove);
                SetSuccessMessage(Resources.Global.DisconnectSuccess);
            }
            else
            {
                SetErrorMessage(Resources.Global.ErrorMessage);
            }
            return RedirectToAction("Online");
        }

        [HttpGet]
        [ClinicGroupManager]
        public PartialViewResult EditIssuerPermissions()
        {
            var users = RapidVetUnitOfWork.UserRepository.GetListByClinicGroup(CurrentUser.ActiveClinicGroupId).ToList();
            var issuers = RapidVetUnitOfWork.IssuerRepository.GetListByClinicGroup(CurrentUser.ActiveClinicGroupId);

            var permissions = RapidVetUnitOfWork.UserExtendedPermissionsRepository.GetUsersByClinicGroupAndPermission(CurrentUser.ActiveClinicGroupId, ExtendedPermissions.ViewFinancialReports);

            var model = new UsersExtendedPermissionsIndexModel()
            {
                Users = users,
                Issuers = issuers,
                Permissions = permissions
            };

            return PartialView("_EditIssuerPermissions", model);
        }

        public void SetUserIssuerPermission(bool val, string Ids)
        {
            var splittedIds = Ids.Split('_');
            try
            {
                int issuerID = Convert.ToInt32(splittedIds[0]);
                int userID = Convert.ToInt32(splittedIds[1]);
                RapidVetUnitOfWork.UserExtendedPermissionsRepository.SetUserIssuerPermissionByClinicGroup(CurrentUser.ActiveClinicGroupId, userID, issuerID, val);
            }
            catch (Exception ex)
            {
                BaseLogger.LogWriter.WriteExPath(ex, System.Configuration.ConfigurationManager.AppSettings["LOG_PATH"]);
            }
        }
    }
}
