using System;
using RapidVet.Helpers;

namespace RapidVet.Web.Controllers
{
    public class RedirectData
    {
        public String Message { get; set; }

        public AlertType AlertType { get; set; }
    }
}