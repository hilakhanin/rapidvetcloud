﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVet.Enums;
using RapidVet.Model.Clients;
using RapidVet.WebModels.Clients;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class InactiveClientsController : BaseController
    {
        //
        // GET: /InActive/

        public ActionResult Index(int id = 12, bool toPrint = false, bool toDeactivate = false)
        {
            var clients = RapidVetUnitOfWork.ClientRepository.GetAllClientsIncludingForInactive(CurrentUser.ActiveClinicId);

            var calculatedDate = DateTime.Now.AddMonths(-id);

            var inactiveClients = new List<InactiveClient>();

            foreach (var client in clients)
            {
                DateTime? lastClientVisit = null;
                foreach (var patient in client.Patients)
                {
                    int? visitId;
                    var lastPatientVisit = RapidVetUnitOfWork.VisitRepository.GetLastPatientVisitDate(patient.Id, out visitId);
                    if (lastPatientVisit != null)
                    {
                        if (lastClientVisit == null)
                        {
                            lastClientVisit = lastPatientVisit;
                        }
                        else
                        {
                            if (lastPatientVisit.Value > lastClientVisit.Value)
                            {
                                lastClientVisit = lastPatientVisit;
                            }
                        }
                    }
                }
                inactiveClients.Add(new InactiveClient()
                    {
                        ClientId = client.Id,
                        FullName = string.Format("{0} {1}", client.FirstName, client.LastName),
                        Address = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientAddress(client.Addresses),
                        HomeNumber = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(client.Phones, PhoneTypeEnum.Home),
                        CellularNumer = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(client.Phones, PhoneTypeEnum.Mobile),
                        LastVisitDate = lastClientVisit
                    });
            }
            var model = inactiveClients.Where(c => c.LastVisitDate == null || c.LastVisitDate <= calculatedDate).OrderBy(c => c.FullName).ToList();
            ViewBag.ToPrint = toPrint;
            
            if (toDeactivate)
            {
                foreach (var inactiveClient in model)
                {
                    RapidVetUnitOfWork.ClientRepository.DeactivateClient(inactiveClient.ClientId);
                }
                model.Clear();
            }
            return View(model);
        }

        public PartialViewResult Deactivate(int id)
        {
            var client = RapidVetUnitOfWork.ClientRepository.GetClient(id);
            return PartialView("_Deactivate", client);
        }

        [HttpPost]
        public ActionResult Deactivate(Client model)
        {
            var status = RapidVetUnitOfWork.ClientRepository.DeactivateClient(model.Id);
            return RedirectToAction("Index", "InactiveClients");
        }

        public PartialViewResult DeactivateAllInactiveClients(int id)
        {
            var monthsBack = id;
            return PartialView("_DeactivateAllInactiveClients", monthsBack);
        }
    }
}
