﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using RapidVet.Enums;
using RapidVet.Model.Patients;
using RapidVet.WebModels.Referrals;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class ReferralsController : BaseController
    {

        public ActionResult Index(string fromDate, string toDate, int? months, int? method)
        {
            var now = DateTime.Now;
            var lastDateCurrentMonth = DateTime.DaysInMonth(now.Year, now.Month);
            var firstDateNextMonth = new DateTime(now.Year, now.Month, lastDateCurrentMonth).AddDays(1);

            var from = new DateTime(now.Year, now.Month, 1);
            if (!string.IsNullOrWhiteSpace(fromDate))
            {
                DateTime.TryParse(fromDate, out from);
            }
            ViewBag.From = from.ToShortDateString();

            var to = firstDateNextMonth;
            if (!string.IsNullOrWhiteSpace(toDate))
            {
                DateTime.TryParse(toDate, out to);
            }

            ViewBag.To = to.ToShortDateString();
            ViewBag.Months = months.HasValue ? months.Value.ToString() : string.Empty;

            var searchMethod = method.HasValue && method.Value >= 0 && method.Value < 1
                                   ? (ReferralsSearchMethod)method.Value
                                   : ReferralsSearchMethod.DateRange;

            var referrals =
                RapidVetUnitOfWork.ReferralRepository.GetRefferals(CurrentUser.ActiveClinicId, @from, to, months, searchMethod).ToList();
            return View(referrals);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new ReferralEditWebModel() { DateTime = DateTime.Now };

            if (CurrentUser.DefaultDrId.HasValue)
            {
                model.RefferingUserId = CurrentUser.DefaultDrId.Value;
            }

            ViewBag.ClientId = 0;
            ViewBag.PatientId = 0;
            ViewBag.ReferredTo = 0;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ReferralEditWebModel model)
        {
            if (ModelState.IsValid)
            {
                var referral = AutoMapper.Mapper.Map<ReferralEditWebModel, Referral>(model);
                var status = RapidVetUnitOfWork.ReferralRepository.Create(referral);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "Referrals");
            }
            ViewBag.ClientId = Request.Form["clientId"];
            ViewBag.PatientId = model.PatientId;
            ViewBag.ReferredTo = model.RefferedToUserId;
            return View(model);
        }

        [HttpGet]
        public ActionResult Print(int id)
        {
            var refferal = RapidVetUnitOfWork.ReferralRepository.GetReferral(id);
            var model = AutoMapper.Mapper.Map<Referral, ReferralEditWebModel>(refferal);
            var p = refferal.Patient;
            var c = p == null ? null : p.Client;
            ViewBag.ClientIdCard = c == null ? String.Empty : c.IdCard;
            ViewBag.ClientName = c == null ? String.Empty : c.Name;
            ViewBag.AnimalName = p == null ? String.Empty : p.Name;
            ViewBag.ReferedToDoctor = refferal.RefferedToUser == null ? refferal.RefferedToExternal : refferal.RefferedToUser.Name;
            ViewBag.RefferingUser = refferal.RefferingUser == null ? String.Empty : refferal.RefferingUser.Name;
            ViewBag.ReferralDate = refferal.DateTime.ToString("dd/MM/yyyy");
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var refferal = RapidVetUnitOfWork.ReferralRepository.GetReferral(id);
            if (IsPatientInCurrentClinic(refferal.PatientId))
            {
                var model = AutoMapper.Mapper.Map<Referral, ReferralEditWebModel>(refferal);
                ViewBag.ClientId = refferal.Patient.ClientId;
                ViewBag.PatientId = refferal.PatientId;
                ViewBag.ReferredTo = refferal.RefferedToUserId == null &&
                                     !string.IsNullOrWhiteSpace(refferal.RefferedToExternal)
                                         ? -9
                                         : refferal.RefferedToUserId;
                return View(model);
            }
            throw new SecurityException();
        }

        public ActionResult Edit(ReferralEditWebModel model)
        {
            if (ModelState.IsValid)
            {
                var refferal = RapidVetUnitOfWork.ReferralRepository.GetReferral(model.Id);

                if (IsPatientInCurrentClinic(model.PatientId))
                {
                    AutoMapper.Mapper.Map(model, refferal);
                    if (model.RefferedToUserId == -9)
                    {
                        refferal.RefferedToUser = null;
                    }
                    refferal.Updated = DateTime.Now;
                    var status = RapidVetUnitOfWork.Save();
                    if (status.Success)
                    {
                        if (model.IsForPrint)                            
                            return RedirectToAction("Print/" + model.Id, "Referrals", new { target = "_blank" });

                        SetSuccessMessage();
                    }
                    else
                    {
                        SetErrorMessage();
                    }
                    ViewBag.ClientId = Request.Form["clientId"];
                    ViewBag.PatientId = model.PatientId;
                    ViewBag.ReferredTo = model.RefferedToUserId;
                    return RedirectToAction("Index", "Referrals");
                }
                throw new SecurityException();
            }
            return View(model);
        }


        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            var refferal = RapidVetUnitOfWork.ReferralRepository.GetReferral(id);
            if (IsPatientInCurrentClinic(refferal.PatientId))
            {
                return PartialView("_Delete", refferal);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult Delete(int id, int patientId)
        {
            var refferal = RapidVetUnitOfWork.ReferralRepository.GetReferral(id);
            if (IsPatientInCurrentClinic(refferal.PatientId))
            {
                var ststus = RapidVetUnitOfWork.ReferralRepository.Delete(refferal);

                if (ststus.Success)
                {
                    SetSuccessMessage("הנתונים נשמרו בהצלחה");
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "Referrals");

            }
            throw new SecurityException();
        }

        public JsonResult GetReferralDoctors()
        {
            var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId);

            var selectList = doctors.Select(d => new SelectListItem()
                {
                    Text = d.Name,
                    Value = d.Id.ToString()
                }).ToList();

            selectList.Add(new SelectListItem() { Value = "-9", Text = "רופא חיצוני" });

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }
    }
}
