﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CsvHelper;
using CsvHelper.Configuration;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model.Clients;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Resources;
using RapidVet.Web.Attributes;
using RapidVet.WebModels.PreventiveMedicine;
using RapidVet.WebModels.Users;
using PriceListItem = RapidVet.Model.Finance.PriceListItem;
using RestSharp;
using RapidVet.Model.Clinics;
using RapidVet.Repository;
using RapidVet.Model.Tools;
using System.Threading;
using System.ComponentModel;

namespace RapidVet.Web.Controllers.Office
{

    [Authorize]
    public class PreventiveMedicineRemindersController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.Stikers = IsStikerActive();
            ViewBag.PostCards = CurrentUser.ActiveClinicGroup.PostCardWidth > 0 &&
                                CurrentUser.ActiveClinicGroup.PostCardHight > 0 && !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.PreventiveMedicinePostCardTemplate);
            ViewBag.Sms = IsSmsActive() &&
                          !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.PreventiveMedicineSmsTemplate);
            ViewBag.Email = IsEmailActive() && !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.PreventiveMedicineEmailSubject) && !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.PreventiveMedicineEmailTemplate);
            ViewBag.Letter = !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.PreventiveMedicineLetterTemplate);

            ViewBag.MultiAnimalsSms = IsSmsActive() &&
                         !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.PreventiveMedicineSmsMultiAnimalsTemplate);
            ViewBag.MultiAnimalsEmail = IsEmailActive() && !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.PreventiveMedicineEmailMultiAnimalsSubject) && !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.PreventiveMedicineEmailMultiAnimalsTemplate);
            ViewBag.MultiAnimalsLetter = !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.PreventiveMedicineLetterMultiAnimalsTemplate);
            ViewBag.MultiAnimalsPostCards = CurrentUser.ActiveClinicGroup.PostCardWidth > 0 &&
                               CurrentUser.ActiveClinicGroup.PostCardHight > 0 && !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.PreventiveMedicinePostCardMultiAnimalsTemplate);
            ViewBag.DistinctClients = CurrentUser.ActiveClinicGroup.UnitePreventiveReminders;

            return View();
        }

        public ActionResult Print(RemindersFilter filter)
        {
            try
            {
                int CurrentUserID = 1;
                if (filter == null)
                    filter = new RemindersFilter();

                if (CurrentUser == null || CurrentUser.ActiveClinicId == null)
                    CurrentUserID = 1;
                else
                    CurrentUserID = CurrentUser.ActiveClinicId;


                var data = GetReportResultsList(filter, CurrentUserID);
                ViewBag.From = filter.FromDate.Equals("undefined") ? "" : StringUtils.ParseStringToDateTime(filter.FromDate).ToShortDateString();
                ViewBag.To = filter.ToDate.Equals("undefined") ? "" : StringUtils.ParseStringToDateTime(filter.ToDate).ToShortDateString();
                try
                {
                    if (Request.Cookies["currentSortProperty"] != null && Request.Cookies["currentSortState"]!=null)
                        sortPrintData(ref data, Request.Cookies["currentSortProperty"].Value, Request.Cookies["currentSortState"].Value);
                }
                catch (Exception ex)
                {
                    var savePath = System.Configuration.ConfigurationManager.AppSettings["LOG_PATH"];
                    BaseLogger.LogWriter.WriteMsgPath("Print sortPrintData", savePath, "");
                }
                return View(data);
            }
            catch (Exception ex)
            {
                var savePath = System.Configuration.ConfigurationManager.AppSettings["LOG_PATH"];
                BaseLogger.LogWriter.WriteMsgPath("Print", savePath, "");
            }
            return null;
        }

        private void sortPrintData(ref List<PreventiveReminderIndexModel> data, string property, string direction)
        {
            if (data == null || String.IsNullOrEmpty(property) || String.IsNullOrEmpty(direction))
                return;

            //true = up , false = down
            bool dirUD = direction == null || direction.Equals("up");
            PropertyDescriptor prop = TypeDescriptor.GetProperties(typeof(PreventiveReminderIndexModel)).Find(property, true);
            if (prop == null)
                return;

            data = (dirUD ? data.OrderBy(x => prop.GetValue(x)) : data.OrderByDescending(x => prop.GetValue(x))).ToList();
        }

        public void MarkAsReminded(RemindersFilter filter)
        {
            var data = GetReportResultsListByPrint(filter, CurrentUser.ActiveClinicId, true);
        }

        public ActionResult Letters(RemindersFilter filter, bool distinctClients = false, bool print = true)
        {
            var template = distinctClients ? CurrentUser.ActiveClinicGroup.PreventiveMedicineLetterMultiAnimalsTemplate : CurrentUser.ActiveClinicGroup.PreventiveMedicineLetterTemplate;
            var model = GetViewResults(filter, distinctClients, template, print, false);
            ViewBag.Print = print;

            ViewBag.PrintUrl = string.Format("{0}?{1}&distinctClients={2}&print={3}",
                                             Url.Action("Letters", "PreventiveMedicineReminders"),
                                             filter.ToString(), distinctClients, true);
            return View(model);
        }

        public ActionResult PostCards(RemindersFilter filter, bool distinctClients = false, bool print = true)
        {
            var template = distinctClients ? CurrentUser.ActiveClinicGroup.PreventiveMedicinePostCardMultiAnimalsTemplate : CurrentUser.ActiveClinicGroup.PreventiveMedicinePostCardTemplate;
            //  template = "<div>" + template + "</div>";
            var model = GetViewResults(filter, distinctClients, template, print, false);
            ViewBag.Print = print;
            ViewBag.Width = CurrentUser.ActiveClinicGroup.PostCardWidth;
            ViewBag.Height = CurrentUser.ActiveClinicGroup.PostCardHight;

            ViewBag.PrintUrl = string.Format("{0}?{1}&distinctClients={2}&print={3}",
                                            Url.Action("PostCards", "PreventiveMedicineReminders"),
                                            filter.ToString(), distinctClients, true);
            return View(model);
        }

        public ActionResult Stickers(RemindersFilter filter, bool distinctClients = false, bool print = true)
        {


            var model = GetViewResults(filter, distinctClients, CurrentUser.ActiveClinicGroup.PreventiveMedicineStikerTemplate/*PreventiveReminderPrintOutput.Stiker*/, print, false);

            ViewBag.ColumnCount = CurrentUser.ActiveClinicGroup.StikerColumns;
            ViewBag.RowCount = CurrentUser.ActiveClinicGroup.StikerRows;
            ViewBag.RowHeight = CurrentUser.ActiveClinicGroup.StikerRowHight;
            ViewBag.Margin = CurrentUser.ActiveClinicGroup.StickerSideMargin;
            ViewBag.MarginTop = CurrentUser.ActiveClinicGroup.StikerTopMargin;
            ViewBag.Print = true;

            return View(model);
        }


        [ExcelExport]
        public FileResult Excel(RemindersFilter filter, bool distinctClients)
        {
            var data = GetReportResultsList(filter, CurrentUser.ActiveClinicId, false);

            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8 };
            var csv = new CsvWriter(writer, csvConfig);
            csv.Configuration.HasHeaderRecord = false;

            csv.WriteRecord(new
                {
                    c1 = "תאריך",
                    c2 = "שם הבעלים",
                    c3 = "שם החיה",
                    c4 = "סוג החיה",
                    c5 = "מין חיה",
                    c6 = "צבע",
                    c7 = "גיל",
                    c8 = "סטטוס",
                    c9 = "שם החיסון",
                    c10 = "כתובת",
                    c11 = "טל' בית",
                    c12 = "טל' סלולרי",
                    c13 = "אימייל",
                    c14 = "מס תזכורות שנשלחו",
                    c15 = "תזכור אחרון",
                    c16 = "תור הבא",
                    c17 = "הערה"
                });

            var csvData = AutoMapper.Mapper.Map<List<PreventiveReminderIndexModel>, List<PreventiveReminderCsvModel>>(data);
            csv.WriteRecords(csvData);
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", "reminders.csv");
        }

        public JsonResult CountSmsMessages(RemindersFilter filter, bool distinctClients)
        {
            int messagesCount = 0;
            var data = GetReportResultsList(filter, CurrentUser.ActiveClinicId, false);
            var clientIds = new HashSet<int>();
            var patientIds = new HashSet<int>();
            foreach (var d in data)
            {
                clientIds.Add(d.ClientId);
                patientIds.Add(d.PatientIdR);
            }

            foreach (var clientId in clientIds)
            {
                var client = RapidVetUnitOfWork.ClientRepository.GetClient(clientId);
                var clientPhone = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(client.Phones,
                                                                                       PhoneTypeEnum.Mobile);
                if (!string.IsNullOrWhiteSpace(clientPhone))
                {

                    if (distinctClients)
                    {
                        messagesCount++;
                    }
                    else
                    {
                        foreach (var patient in client.Patients.Where(p => patientIds.Contains(p.Id)))
                        {
                            messagesCount++;
                        }
                    }
                }
            }

            return Json(messagesCount, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Sms(RemindersFilter filter, bool distinctClients)
        {
            var template = distinctClients ? CurrentUser.ActiveClinicGroup.PreventiveMedicineSmsMultiAnimalsTemplate : CurrentUser.ActiveClinicGroup.PreventiveMedicineSmsTemplate;

            string jobGuid = Guid.NewGuid().ToString();
            var jobId = Hangfire.BackgroundJob.Schedule(() => SendSmsMessages(filter, distinctClients, template, CurrentUser.Id, CurrentUser.ActiveClinicGroupId,
                CurrentUser.ActiveClinicId, Hangfire.JobCancellationToken.Null, jobGuid), TimeSpan.FromSeconds(15));

            ClinicBackgroundJob job = new ClinicBackgroundJob()
            {
                BackgroundJobType = BackgroundJobType.SMS,
                ClinicId = CurrentUser.ActiveClinicId,
                InitiatingUserId = CurrentUser.Id,
                JobGuid = jobGuid,
                JobId = jobId,
                JobModule = Resources.Auditing.PreventiveMedicineReminders,
                StartTime = DateTime.Now,
                WasCancelled = false
            };

            RapidVetUnitOfWork.BackgroundJobsRepository.Create(job);
            SetSuccessMessage(Resources.LetterTemplate.SmsGeneralSentAction);

            return null;
        }


        public void SendSmsMessages(RemindersFilter filter, bool distinctClients, string template, int userId, int clinicGroupId, int clinicId, Hangfire.IJobCancellationToken cancellationToken, string jobGuid)
        {
            try
            {
                Model.Users.User CurrentUser = RapidVetUnitOfWork.UserRepository.GetItem(userId);
                var activeClinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(clinicGroupId);
                var activeClinic = RapidVetUnitOfWork.ClinicRepository.GetItem(clinicId);
                CurrentUser.UpdateClinicGroup(activeClinicGroup);
                CurrentUser.UpdateClinic(activeClinic);
                var data = GetReportResultsList(filter, clinicId, true);
                var clientIds = new HashSet<int>();
                var patientIds = new HashSet<int>();
                foreach (var d in data)
                {
                    clientIds.Add(d.ClientId);
                    patientIds.Add(d.PatientId);
                }
                var listLength = clientIds.Count;
                var successCounter = 0;
                var patientNames = string.Empty;


                template = template.Replace("&lt;", "<");
                template = template.Replace("&gt;", ">");
                template = template.Replace("<br />", "");
                template = template.Replace("&nbsp;", " ");

                var nvc = new NameValueCollection();
                nvc.Add(getTemplateKeyWord("ClientName"), "");
                nvc.Add(getTemplateKeyWord("ClientFirstName"), "");
                nvc.Add(getTemplateKeyWord("ClientLastName"), "");
                nvc.Add(getTemplateKeyWord("PatientName"), "");
                nvc.Add(getTemplateKeyWord("PreventiveMedicineName"), "");
                nvc.Add(getTemplateKeyWord("TreatmentDetails"), "");
                nvc.Add(getTemplateKeyWord("DoctorName"), "");
                nvc.Add(getTemplateKeyWord("Date"), DateTime.Now.ToShortDateString());
                nvc.Add(getTemplateKeyWord("ClinicName"), CurrentUser.ActiveClinic.Name);
                nvc.Add(getTemplateKeyWord("ClinicPhone"), CurrentUser.ActiveClinic.Phone);
                nvc.Add(getTemplateKeyWord("ClinicAddress"), CurrentUser.ActiveClinic.Address);
                nvc.Add(getTemplateKeyWord("ClientAddress"), "");
                nvc.Add(getTemplateKeyWord("ClientZip"), "");

                var clinicJob = RapidVetUnitOfWork.BackgroundJobsRepository.GetBackgroundJobByGuid(jobGuid);

                if (clinicJob.Processed == 0 && clinicJob.Total == 0)
                {
                    foreach (var clientId in clientIds)
                    {
                        try
                        {
                            cancellationToken.ThrowIfCancellationRequested();
                            Thread.Sleep(1000);
                            patientNames = string.Empty;
                            var client = RapidVetUnitOfWork.ClientRepository.GetClient(clientId);
                            if (client.VetId.HasValue)
                            {
                                var clientDoctor = RapidVetUnitOfWork.UserRepository.GetItem(client.VetId.Value);
                                nvc.Set(getTemplateKeyWord("DoctorName"), clientDoctor.Name);
                            }
                            var clientAddress = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientAddressForSticker(client.Addresses);
                            var clientZipcode = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientZipCode(client.Addresses);

                            nvc.Set(getTemplateKeyWord("ClientName"), client.Name);
                            nvc.Set(getTemplateKeyWord("ClientFirstName"), client.FirstName);
                            nvc.Set(getTemplateKeyWord("ClientLastName"), client.LastName);
                            nvc.Set(getTemplateKeyWord("ClientAddress"), clientAddress);
                            nvc.Set(getTemplateKeyWord("ClientZip"), clientZipcode);

                            var clientPhone = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(client.Phones,
                                                                                                   PhoneTypeEnum.Mobile);
                            if (!string.IsNullOrWhiteSpace(clientPhone))
                            {

                                if (distinctClients)
                                {
                                    patientNames = client.Patients.Where(p => patientIds.Contains(p.Id))
                                                                          .Aggregate(string.Empty, (current, patient) => current + ", " + patient.Name);
                                    patientNames = patientNames.Substring(2, patientNames.Length - 2);
                                    nvc.Set(getTemplateKeyWord("PatientName"), patientNames);
                                    var treatmentDetails = string.Empty;

                                    foreach (var patientId in client.Patients.Where(p => patientIds.Contains(p.Id)).Select(p => p.Id))
                                    {
                                        var preventiveItems = data.Where(d => d.PatientId == patientId);

                                        //ג'וליה כלבת 2 26/02/2016
                                        treatmentDetails = preventiveItems.Aggregate(treatmentDetails,
                                                                                       (current, treatment) =>
                                                                                       current + "\r\n" + treatment.PatientName + " - " + treatment.PriceListItemName + ", בתאריך " + (treatment.ScheduledDateStr.HasValue ? treatment.ScheduledDateStr.Value.ToShortDateString() : ""));

                                    }
                                    nvc.Set(getTemplateKeyWord("TreatmentDetails"), treatmentDetails);

                                    var message = RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template);

                                    var sms = new Unicell.Sms(CurrentUser.ActiveClinicGroup.UnicellUserName,
                                                              CurrentUser.ActiveClinicGroup.UnicellPassword, clientPhone, message, string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.SignatureSmsTemplate) ? "" : CurrentUser.ActiveClinicGroup.SignatureSmsTemplate);
                                    var sendResult = sms.Send();
                                    if (sendResult)
                                    {
                                        RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, 1, MessageType.Sms);

                                        string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                             Resources.Auditing.SendingModule, Resources.Auditing.PreventiveMedicineReminders,
                                                                             Resources.Auditing.ClientName, client.Name,
                                                                             Resources.Auditing.PhoneNumber, clientPhone,
                                                                             Resources.Auditing.SmsContent, message,
                                                                             Resources.Auditing.ReturnCode, sendResult);

                                        RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.SmsSent, logMessage, "");

                                        successCounter++;

                                        clinicJob.Processed = successCounter;
                                        clinicJob.Total = listLength;
                                        RapidVetUnitOfWork.BackgroundJobsRepository.Update(clinicJob);
                                    }
                                }
                                else
                                {
                                    var miniCounter = client.Patients.Count;
                                    foreach (var patient in client.Patients.Where(p => patientIds.Contains(p.Id)))
                                    {
                                        nvc.Add(getTemplateKeyWord("PatientName"), patient.Name);
                                        nvc.Remove(getTemplateKeyWord("PatientName"));

                                        var preventiveItems = data.Where(d => d.PatientId == patient.Id);

                                        var priceListItemNames = string.Empty;

                                        priceListItemNames = preventiveItems.Where(p => p.PatientId == patient.Id).Aggregate(priceListItemNames,
                                                                                       (current, item) =>
                                                                                       current + ", " + item.PriceListItemName);
                                        nvc.Set(getTemplateKeyWord("PatientName"), patient.Name);
                                        var treatmentsString = priceListItemNames.Substring(2, priceListItemNames.Length - 2); //to lose the additional ", "
                                        nvc.Set(getTemplateKeyWord("PreventiveMedicineName"), treatmentsString);
                                        var message = RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template);

                                        var sms = new Unicell.Sms(CurrentUser.ActiveClinicGroup.UnicellUserName,
                                                              CurrentUser.ActiveClinicGroup.UnicellPassword, clientPhone, message, string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.SignatureSmsTemplate) ? "" : CurrentUser.ActiveClinicGroup.SignatureSmsTemplate);
                                        var sendResult = sms.Send();
                                        if (sendResult)
                                        {
                                            // miniCounter--;
                                            RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, 1, MessageType.Sms);

                                            string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                             Resources.Auditing.SendingModule, Resources.Auditing.PreventiveMedicineReminders,
                                                                             Resources.Auditing.ClientName, client.Name,
                                                                             Resources.Auditing.PhoneNumber, clientPhone,
                                                                             Resources.Auditing.SmsContent, message,
                                                                             Resources.Auditing.ReturnCode, sendResult);

                                            RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.SmsSent, logMessage, "");

                                            successCounter++;

                                            clinicJob.Processed = successCounter;
                                            clinicJob.Total = listLength;
                                            RapidVetUnitOfWork.BackgroundJobsRepository.Update(clinicJob);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                listLength--;
                            }
                        }
                        catch (OperationCanceledException)
                        {
                            var savePath = System.Configuration.ConfigurationManager.AppSettings["LOG_PATH"];
                            BaseLogger.LogWriter.WriteMsgPath("SMS send ABORTED", savePath, "ClinicId: " + clinicId);
                            throw;
                        }

                    }

                    clinicJob.Processed = successCounter;
                    clinicJob.Total = listLength;
                    clinicJob.EndTime = DateTime.Now;
                    RapidVetUnitOfWork.BackgroundJobsRepository.Update(clinicJob);
                }
                //if (successCounter >= listLength)
                //{
                //    SetSuccessMessage(string.Format(Resources.LetterTemplate.SmsSuccessMessage, successCounter, listLength - successCounter < 0 ? 0 : listLength - successCounter, listLength));
                //}
                //else
                //{
                //    SetErrorMessage(Resources.Global.MessagesSendingFailed);
                //}
            }
            catch (Exception ex)
            {
                var savePath = System.Configuration.ConfigurationManager.AppSettings["LOG_PATH"];
                BaseLogger.LogWriter.WriteExPath(ex, savePath, "ClinicId: " + clinicId);
            }
        }


        [HttpPost]
        public JsonResult Email(RemindersFilter filter, bool distinctClients)
        {
            var data = GetReportResultsList(filter, CurrentUser.ActiveClinicId, true);
            var clientIds = new HashSet<int>();
            var patientIds = new HashSet<int>();
            int messagesCounter = 0;

            foreach (var d in data)
            {
                clientIds.Add(d.ClientId);
                patientIds.Add(d.PatientId);
            }

            var template = distinctClients ? CurrentUser.ActiveClinicGroup.PreventiveMedicineEmailMultiAnimalsTemplate : CurrentUser.ActiveClinicGroup.PreventiveMedicineEmailTemplate;
            var subject = distinctClients ? CurrentUser.ActiveClinicGroup.PreventiveMedicineEmailMultiAnimalsSubject : CurrentUser.ActiveClinicGroup.PreventiveMedicineEmailSubject;

            if (String.IsNullOrEmpty(subject))
            {
                subject = "   ";
            }
            var clientAddress = string.Empty;
            var clientZipcode = string.Empty;

            var nvc = new NameValueCollection();
            nvc.Add(getTemplateKeyWord("Date"), DateTime.Now.ToShortDateString());
            nvc.Add(getTemplateKeyWord("ClientName"), "");
            nvc.Add(getTemplateKeyWord("ClientFirstName"), "");
            nvc.Add(getTemplateKeyWord("ClientLastName"), "");
            nvc.Add(getTemplateKeyWord("ClientAddress"), "");
            nvc.Add(getTemplateKeyWord("ClientZip"), "");
            nvc.Add(getTemplateKeyWord("ClinicName"), CurrentUser.ActiveClinic.Name);
            nvc.Add(getTemplateKeyWord("ClinicAddress"), CurrentUser.ActiveClinic.Address);
            nvc.Add(getTemplateKeyWord("ClinicPhone"), CurrentUser.ActiveClinic.Phone);
            nvc.Set(getTemplateKeyWord("PatientName"), "");
            nvc.Add(getTemplateKeyWord("PreventiveMedicineName"), "");
            nvc.Add(getTemplateKeyWord("TreatmentDetails"), "");
            nvc.Add(getTemplateKeyWord("DoctorName"), "");

            var mailClient = getMailClient();
            string from = string.Format("{0}<{1}>", !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.EmailBusinessName) ? CurrentUser.ActiveClinicGroup.EmailBusinessName : CurrentUser.ActiveClinic.Name,
                                                CurrentUser.ActiveClinicGroup.EmailAddress);

            foreach (var clientId in clientIds)
            {

                var client = RapidVetUnitOfWork.ClientRepository.GetClient(clientId);
                if (client.VetId.HasValue)
                {
                    var clientDoctor = RapidVetUnitOfWork.UserRepository.GetItem(client.VetId.Value);
                    nvc.Set(getTemplateKeyWord("DoctorName"), clientDoctor.Name);
                }
                nvc.Set(getTemplateKeyWord("ClientName"), client.Name);
                nvc.Set(getTemplateKeyWord("ClientFirstName"), client.FirstName);
                nvc.Set(getTemplateKeyWord("ClientLastName"), client.LastName);

                if (client.Emails.Any())
                {
                    clientAddress = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientAddress(client.Addresses);
                    clientZipcode = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientZipCode(client.Addresses);
                    nvc.Add(getTemplateKeyWord("ClientAddress"), clientAddress);
                    nvc.Add(getTemplateKeyWord("ClientZip"), clientZipcode);

                    if (distinctClients) //create one email per client - aggregate patients
                    {
                        var treatmentDetails = string.Empty;
                        var patientNames = string.Empty;
                        var treatmentDueDate = string.Empty;

                        foreach (var patientId in client.Patients.Where(p => patientIds.Contains(p.Id)).Select(p => p.Id))
                        {
                            var preventiveItems = data.Where(d => d.PatientId == patientId);

                            //ג'וליה כלבת 2 26/02/2016
                            treatmentDetails = preventiveItems.Aggregate(treatmentDetails,
                                                                           (current, treatment) =>
                                                                           current + "<br />" + treatment.PatientName + " - " + treatment.PriceListItemName + ", בתאריך " + (treatment.ScheduledDateStr.HasValue ? treatment.ScheduledDateStr.Value.ToShortDateString() : ""));

                        }
                        nvc.Set(getTemplateKeyWord("TreatmentDetails"), treatmentDetails);
                        //  nvc.Set(TemplateKeyWord.PatientName, patientNames.Trim());

                        int counter = 0;
                        RestRequest request = new RestRequest();
                        request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_RAPID_DOMAIN"], ParameterType.UrlSegment);
                        request.Resource = "{domain}/messages";
                        request.AddParameter("from", from);
                        request.AddParameter("subject", RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, subject));
                        request.AddParameter("html", RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template));
                        request.Method = Method.POST;

                        StringBuilder emailAdress = new StringBuilder();

                        foreach (var email in client.Emails)
                        {
                            request.AddParameter("to", email.Name);
                            emailAdress.Append(email.Name);
                            emailAdress.Append("; ");
                            counter++;
                        }

                        if (counter > 0)
                        {
                            var message = mailClient.Execute(request);
                            if ((int)message.StatusCode == 200)
                            {
                                RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, counter, MessageType.Email);
                                string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                  Resources.Auditing.SendingModule, Resources.Auditing.PreventiveMedicineReminders,
                                                                  Resources.Auditing.ClientName, client.Name,
                                                                  Resources.Auditing.Address, emailAdress.ToString(),
                                                                  Resources.Auditing.MailSubject, RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, subject),
                                                                  Resources.Auditing.ReturnCode, (int)message.StatusCode + " - " + message.StatusCode);

                                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.MailSent, logMessage, "");
                                messagesCounter += counter;
                            }
                        }
                    }
                    else
                    {
                        foreach (var patient in client.Patients.Where(p => patientIds.Contains(p.Id)))
                        {
                            var preventiveItems = data.Where(d => d.PatientId == patient.Id);

                            var priceListItemNames = string.Empty;

                            priceListItemNames = preventiveItems.Where(p => p.PatientId == patient.Id).Aggregate(priceListItemNames,
                                                                           (current, item) =>
                                                                           current + ", " + item.PriceListItemName);
                            nvc.Set(getTemplateKeyWord("PatientName"), patient.Name);
                            var treatmentsString = priceListItemNames.Substring(2, priceListItemNames.Length - 2); //to lose the additional ", "
                            nvc.Set(getTemplateKeyWord("PreventiveMedicineName"), treatmentsString);

                            int counter = 0;
                            RestRequest request = new RestRequest();
                            request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_RAPID_DOMAIN"], ParameterType.UrlSegment);
                            request.Resource = "{domain}/messages";
                            request.AddParameter("from", from);
                            request.AddParameter("subject", RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, subject));
                            request.AddParameter("html", RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template));
                            request.Method = Method.POST;

                            nvc.Remove(getTemplateKeyWord("PreventiveMedicineName"));

                            StringBuilder emailAdress = new StringBuilder();

                            foreach (var email in client.Emails)
                            {
                                request.AddParameter("to", email.Name);
                                emailAdress.Append(email.Name);
                                emailAdress.Append("; ");
                                counter++;
                            }

                            if (counter > 0)
                            {
                                var message = mailClient.Execute(request);
                                if ((int)message.StatusCode == 200)
                                {
                                    RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, counter, MessageType.Email);
                                    string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                 Resources.Auditing.SendingModule, Resources.Auditing.PreventiveMedicineReminders,
                                                                 Resources.Auditing.ClientName, client.Name,
                                                                 Resources.Auditing.Address, emailAdress.ToString(),
                                                                 Resources.Auditing.MailSubject, RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, subject),
                                                                 Resources.Auditing.ReturnCode, (int)message.StatusCode + " - " + message.StatusCode);

                                    RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.MailSent, logMessage, "");
                                    messagesCounter += counter;
                                }
                            }
                        }
                    }
                }
            }

            SetSuccessMessage(string.Format(Resources.LetterTemplate.EmailSuccessMessage, messagesCounter, clientIds.Count() - messagesCounter < 0 ? 0 : clientIds.Count() - messagesCounter, clientIds.Count()));

            //  SetSuccessMessage(Resources.Global.MessagesSentSuccessfully);
            return null;
        }


        public JsonResult GetPreventiveItemsList()
        {
            var items = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPriceListItems(CurrentUser.ActiveClinicId);
            var model = items.Select(AutoMapper.Mapper.Map<PriceListItem, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFiltersList()
        {
            var filters = RapidVetUnitOfWork.UserRepository.GetPreventiveItemReminderFilters(CurrentUser.Id);
            var model =
                filters.Select(AutoMapper.Mapper.Map<UserPreventiveMedicineReminderFilter, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetReportData(RemindersFilter filter)
        {
            //var data = GetReportResults(filter, CurrentUser.ActiveClinicId);
            var data = GetReportResultsList(filter, CurrentUser.ActiveClinicId);
            //var rsR = Json(data, JsonRequestBehavior.AllowGet);
            return Content(SimpleJson.SerializeObject(data));
        }

        [HttpGet]
        public JsonResult GetFilterData(int id)
        {
            var filter = RapidVetUnitOfWork.UserRepository.GetPreventiveReminderFilter(id);
            var model =
                AutoMapper.Mapper
                          .Map<UserPreventiveMedicineReminderFilter, UserPreventiveMedicineReminderFilterWebModel>(
                              filter);
            model.PriceListItemIds = filter.PreventiveMedicineItems.Select(f => f.PriceListItemId).ToList();
            model.CityIds = filter.PreventiveMedicineFilterCity.Select(f => f.CityId).ToList();

            return filter.UserId == CurrentUser.Id ? Json(model, JsonRequestBehavior.AllowGet) : null;
        }


        [HttpPost]
        public JsonResult SaveFilter(UserPreventiveMedicineReminderFilter model, string preventiveItemIdsStr, string cityIdsStr, string name)
        {
            UserPreventiveMedicineReminderFilter filter;
            var priceListItemsIds = StringUtils.GetIntList(preventiveItemIdsStr);
            var cityIds = StringUtils.GetIntList(cityIdsStr);
            var finalName = name;
            if (model.Id > 0)
            {
                filter = RapidVetUnitOfWork.UserRepository.GetPreventiveReminderFilter(model.Id);
                finalName = filter.Name;
            }
            else
            {
                filter = new UserPreventiveMedicineReminderFilter
                    {
                        UserId = CurrentUser.Id,
                        Name = name,
                        ClinicId = CurrentUser.ActiveClinicId
                    };
                RapidVetUnitOfWork.UserRepository.AddPreventiveReminderFilter(filter);
            }

            AutoMapper.Mapper.Map(model, filter);
            filter.Name = finalName;

            var status = RapidVetUnitOfWork.Save();

            if (status.Success)
            {
                RapidVetUnitOfWork.UserRepository.UpdatePreventiveReminderFilterWithItems(filter.Id, priceListItemsIds, cityIds);
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }

            return Json(status.Success);
        }


        [HttpGet]
        public ActionResult Create()
        {
            var model = new PreventiveMedicineItemCreateModel()
                {
                    SchedualedDate = DateTime.Now,
                    PatientId = 0
                };
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(PreventiveMedicineItemCreateModel model)
        {
            if (ModelState.IsValid && IsPatientInCurrentClinic(model.PatientId))
            {
                var priceListItem = RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(model.PriceListItemId);

                var preventiveItem =
                    AutoMapper.Mapper.Map<PreventiveMedicineItemCreateModel, PreventiveMedicineItem>(model);
                preventiveItem.Quantity = 1;
                preventiveItem.ReminderCount = 0;
                //preventiveItem.LastReminderDate = DateTime.Now;

                preventiveItem.PriceListItemTypeId = priceListItem.ItemTypeId;
                var status = RapidVetUnitOfWork.PreventiveMedicineRepository.Create(preventiveItem);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else { SetErrorMessage(); }
                return RedirectToAction("Index", "PreventiveMedicineReminders");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var preventiveItem = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(id);
            if (IsPatientInCurrentClinic(preventiveItem.PatientId))
            {
                var model =
                    AutoMapper.Mapper.Map<PreventiveMedicineItem, PreventiveMedicineItemCreateModel>(preventiveItem);
                return View(model);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult Edit(PreventiveMedicineItemCreateModel model)
        {
            var preventiveItem = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(model.PreventiveMedicineItemId);
            if (ModelState.IsValid && IsPatientInCurrentClinic(model.PatientId))
            {
                var priceListItem = RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(model.PriceListItemId);

                StringBuilder logMessage = new StringBuilder();
                string basicInfo = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}<br />",
                                                Resources.Auditing.Module, Resources.Auditing.PreventiveMedicineReminders,
                                                Resources.Auditing.Action, Resources.Auditing.ChangeVaccine,
                                                Resources.Auditing.ClientName, RapidVetUnitOfWork.ClientRepository.GetClientFromPatientId(model.PatientId).Name,
                                                Resources.Auditing.PatientName, RapidVetUnitOfWork.PatientRepository.GetName(model.PatientId),
                                                Resources.Auditing.ItemName, priceListItem.Name);

                logMessage.Append(basicInfo);
                string oldInfo = string.Format("{0}: {1}<br />{2}: {3}<br />",
                    Resources.PreventiveReminders.ScheduledDate, preventiveItem.Scheduled.HasValue ? preventiveItem.Scheduled.Value.ToShortDateString() : "",
                    Resources.PreventiveReminders.Comment, preventiveItem.ReminderComments);



                AutoMapper.Mapper.Map(model, preventiveItem);

                preventiveItem.PriceListItemTypeId = priceListItem.ItemTypeId;
                // preventiveItem.LastReminderDate = DateTime.Now;
                // preventiveItem.ReminderCount = preventiveItem.ReminderCount + 1;
                var status = RapidVetUnitOfWork.Save();

                logMessage.Append(string.Format("{0}: {1}<br />{2}: {3}<br />",
                      Resources.PreventiveReminders.ScheduledDate, preventiveItem.Scheduled.HasValue ? preventiveItem.Scheduled.Value.ToShortDateString() : "",
                      Resources.PreventiveReminders.Comment, preventiveItem.ReminderComments));

                if (status.Success)
                {
                    RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.VaccineAndTreatments, logMessage.ToString(), oldInfo);
                    SetSuccessMessage();
                }
                else { SetErrorMessage(); }
                return RedirectToAction("Index", "PreventiveMedicineReminders");
            }
            return View(model);
        }

        [HttpPost]
        public JsonResult DeleteReminder(int ReminderId)
        {
            var preventiveMedicineItem = RapidVetUnitOfWork.PreventiveMedicineRepository.GetPreventiveItem(ReminderId, true);

            RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.PreventiveReminders.DeleteReminder, string.Format("{0}: {1}<br/>{2}: {3}<br/>{4}: {5}<br/>{6}: {7}", Resources.Auditing.ClientName,
            preventiveMedicineItem.Patient.Client.Name, Resources.PreventiveReminders.AnimalName, preventiveMedicineItem.Patient.Name, Resources.Auditing.ReminderType,
            preventiveMedicineItem.PriceListItem.Name, Resources.PreventiveReminders.ScheduledDate, preventiveMedicineItem.Scheduled.Value.ToShortDateString()), "");

            preventiveMedicineItem.Parent = null;
            preventiveMedicineItem.ParentPreventiveItemId = null;

            RapidVetUnitOfWork.PreventiveMedicineRepository.Delete(preventiveMedicineItem);

            var status = RapidVetUnitOfWork.Save();
            if (status.Success)
            {
                SetSuccessMessage(Resources.PreventiveReminders.ConfirmDeleteMessage);
            }
            else { SetErrorMessage(); }
            return Json(status, JsonRequestBehavior.AllowGet);

        }

        private List<PreventiveReminderIndexModel> GetReportResultsList(RemindersFilter filter, int activeClinicId, bool update = false)
        {
            var itemsList = StringUtils.GetIntList(filter.ItemIds);
            var citiesList = StringUtils.GetIntList(filter.CityIds);
            var selectedRemindersFilter = StringUtils.GetIntList(filter.SelectedRemindersIds);
            var from = StringUtils.ParseStringToDateTime(filter.FromDate);
            var to = StringUtils.ParseStringToDateTime(filter.ToDate);
            var fromReminder = StringUtils.ParseStringToDateTime(filter.FromReminderDate);
            var toReminder = StringUtils.ParseStringToDateTime(filter.ToReminderDate);
            var data = RapidVetUnitOfWork
              .PreventiveMedicineRemindersRepository
              .GetDataList(activeClinicId, from, to, filter.RegionalCouncilId,
                       filter.AnimalKindId, filter.InactiveClients, filter.InactivePatients,
                       filter.FutureOnly, itemsList, fromReminder, toReminder, selectedRemindersFilter, filter.RemoveClientsWithFutureCalendarEntries, citiesList, filter.DoctorId, update).ToList();

            if (update)
                RapidVetUnitOfWork.Save(); //UpdateReminders(data);

            return data;
        }
        private List<PreventiveReminderIndexModel> GetReportResultsListByPrint(RemindersFilter filter, int activeClinicId, bool update = false)
        {
            var itemsList = StringUtils.GetIntList(filter.ItemIds);
            var citiesList = StringUtils.GetIntList(filter.CityIds);
            var selectedRemindersFilter = StringUtils.GetIntList(filter.SelectedRemindersIds);
            var from = StringUtils.ParseStringToDateTime(filter.FromDate);
            var to = StringUtils.ParseStringToDateTime(filter.ToDate);
            var fromReminder = StringUtils.ParseStringToDateTime(filter.FromReminderDate);
            var toReminder = StringUtils.ParseStringToDateTime(filter.ToReminderDate);
           
            var data = RapidVetUnitOfWork
              .PreventiveMedicineRemindersRepository
              .GetDataListPrint(activeClinicId, from, to, filter.RegionalCouncilId,
                       filter.AnimalKindId, filter.InactiveClients, filter.InactivePatients,
                       filter.FutureOnly, itemsList, fromReminder, toReminder, selectedRemindersFilter, filter.RemoveClientsWithFutureCalendarEntries, citiesList, filter.DoctorId, update).ToList();

            if (data != null && update)
            {
               RapidVetUnitOfWork.Save(); //UpdateReminders(data);
            }

            

            return data;
        }
        private List<PreventiveReminderIndexModel> GetReportResultsListPrint(RemindersFilter filter, int activeClinicId, bool update = false)
        {
            var itemsList = StringUtils.GetIntList(filter.ItemIds);
            var citiesList = StringUtils.GetIntList(filter.CityIds);
            var selectedRemindersFilter = StringUtils.GetIntList(filter.SelectedRemindersIds);
            var from = StringUtils.ParseStringToDateTime(filter.FromDate);
            var to = StringUtils.ParseStringToDateTime(filter.ToDate);
            var fromReminder = StringUtils.ParseStringToDateTime(filter.FromReminderDate);
            var toReminder = StringUtils.ParseStringToDateTime(filter.ToReminderDate);
            var data = RapidVetUnitOfWork
              .PreventiveMedicineRemindersRepository
              .GetData(activeClinicId, from, to, filter.RegionalCouncilId,
                       filter.AnimalKindId, filter.InactiveClients, filter.InactivePatients,
                       filter.FutureOnly, itemsList, fromReminder, toReminder, selectedRemindersFilter, filter.RemoveClientsWithFutureCalendarEntries, citiesList, filter.DoctorId).ToList();

            //if (update)
            //{
            //    UpdateReminders(data);
            //}

            var result = data.Select(AutoMapper.Mapper.Map<PreventiveMedicineItem, PreventiveReminderIndexModel>).ToList();

            var day = DateTime.Now.Day;
            var month = DateTime.Now.Month;
            var year = DateTime.Now.Year;

            foreach (var item in result)
            {
                var patient = data.Where(d => d.PatientId == item.PatientId).Take(1);
                var client = patient.First().Patient.Client;
                item.ClientAddress = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientAddress(client.Addresses);
                item.ClientCellPhone = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(client.Phones,
                                                                                                   PhoneTypeEnum.Mobile);
                item.ClientHomePhone = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(client.Phones,
                                                                                                   PhoneTypeEnum.Home);
                item.ClientEmail = client.Emails.Any() ? client.Emails.First().Name : string.Empty;
                item.IsPast = item.Scheduled <= DateTime.UtcNow;
                //item.Scheduled.HasValue && item.Scheduled.Value.Year <= year &&
                //item.Scheduled.Value.Month <= month && item.Scheduled.Value.Day <= day;
            }

            return result;
        }
        //private List<PreventiveReminderIndexModel> GetReportResults(RemindersFilter filter, int activeClinicId, bool update = false)
        //{
        //    var itemsList = StringUtils.GetIntList(filter.ItemIds);
        //    var citiesList = StringUtils.GetIntList(filter.CityIds);
        //    var selectedRemindersFilter = StringUtils.GetIntList(filter.SelectedRemindersIds);
        //    var from = StringUtils.ParseStringToDateTime(filter.FromDate);
        //    var to = StringUtils.ParseStringToDateTime(filter.ToDate);
        //    var fromReminder = StringUtils.ParseStringToDateTime(filter.FromReminderDate);
        //    var toReminder = StringUtils.ParseStringToDateTime(filter.ToReminderDate);
        //    var data = RapidVetUnitOfWork
        //      .PreventiveMedicineRemindersRepository
        //      .GetData(activeClinicId, from, to, filter.RegionalCouncilId,
        //               filter.AnimalKindId, filter.InactiveClients, filter.InactivePatients,
        //               filter.FutureOnly, itemsList, fromReminder, toReminder, selectedRemindersFilter, filter.RemoveClientsWithFutureCalendarEntries, citiesList, filter.DoctorId).ToList();

        //    if (update)
        //    {
        //        UpdateReminders(data);
        //    }

        //    var result = data.Select(AutoMapper.Mapper.Map<PreventiveMedicineItem, PreventiveReminderIndexModel>).ToList();

        //    var day = DateTime.Now.Day;
        //    var month = DateTime.Now.Month;
        //    var year = DateTime.Now.Year;

        //    foreach (var item in result)
        //    {
        //        var patient = data.Where(d => d.PatientId == item.PatientId).Take(1);
        //        var client = patient.First().Patient.Client;
        //        item.ClientAddress = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientAddress(client.Addresses);
        //        item.ClientCellPhone = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(client.Phones,
        //                                                                                           PhoneTypeEnum.Mobile);
        //        item.ClientHomePhone = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(client.Phones,
        //                                                                                           PhoneTypeEnum.Home);
        //        item.ClientEmail = client.Emails.Any() ? client.Emails.First().Name : string.Empty;
        //        item.IsPast = item.Scheduled <= DateTime.UtcNow;
        //        //item.Scheduled.HasValue && item.Scheduled.Value.Year <= year &&
        //        //item.Scheduled.Value.Month <= month && item.Scheduled.Value.Day <= day;
        //    }

        //    return result;
        //}

        //private void UpdateReminders(List<PreventiveMedicineItem> data)
        //{
        //    var now = DateTime.Now;
        //    foreach (var d in data)
        //    {
        //        d.LastReminderDate = now;
        //        d.ReminderCount = d.ReminderCount + 1;
        //    }

        //    RapidVetUnitOfWork.Save();
        //}

        private List<string> GetViewResults(RemindersFilter filter, bool distinctClients,
                                             string template, bool print, bool addLineBreaks)
        {
            var itemsList = StringUtils.GetIntList(filter.ItemIds);
            var citiesList = StringUtils.GetIntList(filter.CityIds);
            var selectedRemindersFilter = StringUtils.GetIntList(filter.SelectedRemindersIds);
            var from = StringUtils.ParseStringToDateTime(filter.FromDate);
            var fromReminder = StringUtils.ParseStringToDateTime(filter.FromReminderDate);
            var toReminder = StringUtils.ParseStringToDateTime(filter.ToReminderDate);
            var to = StringUtils.ParseStringToDateTime(filter.ToDate);
            var data = RapidVetUnitOfWork
                .PreventiveMedicineRemindersRepository
                .GetData(CurrentUser.ActiveClinicId, from, to, filter.RegionalCouncilId,
                         filter.AnimalKindId, filter.InactiveClients, filter.InactivePatients,
                         filter.FutureOnly, itemsList, fromReminder, toReminder, selectedRemindersFilter, filter.RemoveClientsWithFutureCalendarEntries, citiesList, filter.DoctorId).ToList();

            if (print)
            {
                #region UpdateReminders Function (Need to remove after fitting GetFormattedTemplates to work with enumerable)
                var now = DateTime.Now;
                foreach (var d in data)
                {
                    d.LastReminderDate = now;
                    d.ReminderCount = d.ReminderCount + 1;
                }
                #endregion
                RapidVetUnitOfWork.Save(); //UpdateReminders(data);
            }

            var result = GetFormattedTemplates(data, template, distinctClients, addLineBreaks);

            return result;
        }

        private List<string> GetFormattedTemplates(List<PreventiveMedicineItem> data, string template, bool distinctClients, bool addLineBreaks)
        {
            var result = new List<string>();

            var date = DateTime.Now.ToShortDateString();
            var nvc = new NameValueCollection();

            if (addLineBreaks)
            {
                nvc.Add("\r\n", "<br />");
            }
            nvc.Add(getTemplateKeyWord("Date"), date);
            nvc.Add(getTemplateKeyWord("DoctorName"), "");
            nvc.Add(getTemplateKeyWord("ClinicAddress"), CurrentUser.ActiveClinic.Address);
            nvc.Add(getTemplateKeyWord("ClientAddress"), "");
            nvc.Add(getTemplateKeyWord("ClinicName"), CurrentUser.ActiveClinic.Name);
            nvc.Add(getTemplateKeyWord("ClinicPhone"), CurrentUser.ActiveClinic.Phone);
            nvc.Add(getTemplateKeyWord("ClientName"), "");
            nvc.Add(getTemplateKeyWord("ClientFirstName"), "");
            nvc.Add(getTemplateKeyWord("ClientLastName"), "");
            nvc.Add(getTemplateKeyWord("ClientZip"), "");
            nvc.Add(getTemplateKeyWord("PatientName"), "");
            nvc.Add(getTemplateKeyWord("PreventiveMedicineName"), "");
            nvc.Add(getTemplateKeyWord("TreatmentDetails"), "");

            var clientIds = new HashSet<int>();
            foreach (var d in data)
            {
                clientIds.Add(d.Patient.ClientId);
            }

            var patientItems = new List<PreventiveMedicineItem>();
            var patientName = string.Empty;
            var priceListItemsName = string.Empty;


            foreach (var clientId in clientIds)
            {
                patientItems = new List<PreventiveMedicineItem>();
                patientName = string.Empty;
                priceListItemsName = string.Empty;

                var clientItems = data.Where(d => d.Patient.ClientId == clientId);

                //client details
                var client = clientItems.ToList().First().Patient.Client;
                var clientAddress = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientAddressForSticker(client.Addresses);
                var clientZipCode = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientZipCode(client.Addresses);

                if (client.VetId.HasValue)
                {
                    var clientDoctor = RapidVetUnitOfWork.UserRepository.GetItem(client.VetId.Value);
                    nvc.Set(getTemplateKeyWord("DoctorName"), clientDoctor.Name);
                }

                nvc.Set(getTemplateKeyWord("ClientName"), client.Name);
                nvc.Set(getTemplateKeyWord("ClientFirstName"), client.FirstName);
                nvc.Set(getTemplateKeyWord("ClientLastName"), client.LastName);
                nvc.Set(getTemplateKeyWord("ClientAddress"), clientAddress);
                nvc.Set(getTemplateKeyWord("ClientZip"), clientZipCode);

                //patientDetails
                var patientIds = new HashSet<int>();
                foreach (var i in clientItems.ToList())
                {
                    patientIds.Add(i.PatientId);
                }

                if (distinctClients)
                //create one item for each client, containing all patients and priceListItem names [aggregate patient & priceListItem names only]
                {
                    var treatmentDetails = string.Empty;
                    var patientNames = string.Empty;
                    var treatmentDueDate = string.Empty;

                    foreach (var patientId in client.Patients.Where(p => patientIds.Contains(p.Id)).Select(p => p.Id))
                    {
                        var preventiveItems = data.Where(d => d.PatientId == patientId);

                        //ג'וליה כלבת 2 26/02/2016
                        treatmentDetails = preventiveItems.Aggregate(treatmentDetails,
                                                                       (current, treatment) =>
                                                                       current + "<br />" + treatment.Patient.Name + " - " + treatment.PriceListItem.Name + ", בתאריך " + (treatment.Scheduled.HasValue ? treatment.Scheduled.Value.ToShortDateString() : ""));
                    }
                    nvc.Set(getTemplateKeyWord("TreatmentDetails"), treatmentDetails);

                    result.Add(RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template));
                }
                else //create one item per patient [aggregate priceListItem names only]
                {
                    foreach (var patientId in patientIds)
                    {
                        patientItems = clientItems.ToList().Where(c => c.PatientId == patientId).ToList();

                        patientName = patientItems.First().Patient.Name;

                        var patientItemNames = new HashSet<string>();
                        foreach (var preventiveItem in patientItems)
                        {
                            patientItemNames.Add(preventiveItem.PriceListItem.Name);
                        }
                        priceListItemsName = patientItemNames.Aggregate(priceListItemsName,
                                                                        (current, name) => current + ",  " + name);
                        priceListItemsName = priceListItemsName.Substring(2, priceListItemsName.Length - 2);
                        UpdateNvc(nvc, patientName, priceListItemsName);
                        result.Add(RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template));
                        priceListItemsName = string.Empty;
                    }
                }
            }
            return result;
        }


        private NameValueCollection UpdateNvc(NameValueCollection nvc, string patientsNames, string preventiveMedicineNames)
        {
            nvc.Set(getTemplateKeyWord("PatientName"), patientsNames);
            nvc.Set(getTemplateKeyWord("PreventiveMedicineName"), preventiveMedicineNames);

            return nvc;
        }


        private string GetTemplate(PreventiveReminderPrintOutput output)
        {
            var template = string.Empty;
            switch (output)
            {
                case PreventiveReminderPrintOutput.Stiker:
                    template = CurrentUser.ActiveClinicGroup.PreventiveMedicineStikerTemplate;
                    break;
                case PreventiveReminderPrintOutput.Letter:
                    template = CurrentUser.ActiveClinicGroup.PreventiveMedicineLetterTemplate;
                    break;
                case PreventiveReminderPrintOutput.PostCard:
                    template = CurrentUser.ActiveClinicGroup.PreventiveMedicinePostCardTemplate;
                    break;
                case PreventiveReminderPrintOutput.Sms:
                    template = CurrentUser.ActiveClinicGroup.PreventiveMedicineSmsTemplate;
                    break;
                case PreventiveReminderPrintOutput.Email:
                    template = CurrentUser.ActiveClinicGroup.PreventiveMedicineEmailTemplate;
                    break;
            }
            return template;
        }

    }


}
