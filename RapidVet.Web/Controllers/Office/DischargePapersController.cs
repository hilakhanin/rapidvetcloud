﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Patients;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.DischargePapers;
using RapidVet.WebModels.MedicalProcedures;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class DischargePapersController : BaseController
    {
        public ActionResult Index(string fromDate, string toDate)
        {
            var now = DateTime.Now;
            var lastDateCurrentMonth = DateTime.DaysInMonth(now.Year, now.Month);
            var firstDateNextMonth = new DateTime(now.Year, now.Month, lastDateCurrentMonth).AddDays(1);

            var from = new DateTime(now.Year, now.Month, 1);
            if (!string.IsNullOrWhiteSpace(fromDate))
            {
                DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out from);
            }
            ViewBag.From = from.ToString("dd/MM/yyyy");

            var to = firstDateNextMonth;
            if (!string.IsNullOrWhiteSpace(toDate))
            {
                DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out to);
            }
            ViewBag.To = to.ToString("dd/MM/yyyy");

            var dischargePapers =
                RapidVetUnitOfWork.DischargePaperRepository.GetDischargePapers(CurrentUser.ActiveClinicId, from, to.AddDays(1));
            var model = dischargePapers.ToList();
            return View(model);

        }
        //id is patient id
        [HttpGet]
        public ActionResult Create(int? id)
        {
            var model = new DischargePaperWebModel()
                {
                    DateTime = DateTime.Now,
                    From = CurrentUser.DefaultDrId.HasValue ? RapidVetUnitOfWork.UserRepository.GetItem(CurrentUser.DefaultDrId.Value).Name : CurrentUser.Name
                };
            if (id != null)
            {
                var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id.Value);
                ViewBag.ClientId = patient.ClientId;
                ViewBag.PatientId = id;
            }
            else
            {
                ViewBag.ClientId = 0;
                ViewBag.PatientId = 0;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(DischargePaperWebModel model, bool print = false)
        {
            if (IsPatientInCurrentClinic(model.PatientId))
            {
                if (ModelState.IsValid)
                {
                    var dischargePaper = AutoMapper.Mapper.Map<DischargePaperWebModel, DischargePaper>(model);
                    var status = RapidVetUnitOfWork.DischargePaperRepository.Create(dischargePaper);
                    if (status.Success)
                    {
                        SetSuccessMessage();
                        return print
                                   ? RedirectToAction("PrePrint", "DischargePapers", new { id = dischargePaper.Id })
                                   : RedirectToAction("Index", dischargePaper);
                    }

                    SetErrorMessage();
                }
                return View(model);
            }
            throw new SecurityException();
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            var dischargePaper = RapidVetUnitOfWork.DischargePaperRepository.GetDischargePaper(id);

            if (IsPatientInCurrentClinic(dischargePaper.PatientId))
            {
                var model = AutoMapper.Mapper.Map<DischargePaper, DischargePaperWebModel>(dischargePaper);
                ViewBag.ClientId = dischargePaper.Patient.ClientId;
                ViewBag.PatientId = dischargePaper.PatientId;
                return View(model);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult Edit(DischargePaperWebModel model, bool print = false)
        {

            if (IsPatientInCurrentClinic(model.PatientId))
            {
                if (ModelState.IsValid)
                {
                    var dischargePaper = RapidVetUnitOfWork.DischargePaperRepository.GetDischargePaper(model.Id);

                    if (dischargePaper.PatientId == model.PatientId)
                    {
                        AutoMapper.Mapper.Map(model, dischargePaper);
                        dischargePaper.Updated = DateTime.Now;
                        var status = RapidVetUnitOfWork.Save();
                        if (status.Success)
                        {
                            SetSuccessMessage();
                            return print
                                   ? RedirectToAction("PrePrint", "DischargePapers", new { id = dischargePaper.Id })
                                   : RedirectToAction("Index", "DischargePapers");
                        }
                        SetErrorMessage();
                    }
                }
                ViewBag.ClientId = Request.Form["clientId"];
                ViewBag.PatientId = model.PatientId;
                return View(model);
            }
            throw new SecurityException();
        }


        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            var dischargPaper = RapidVetUnitOfWork.DischargePaperRepository.GetDischargePaper(id);
            if (IsPatientInCurrentClinic(dischargPaper.PatientId))
            {
                return PartialView("_Delete", dischargPaper);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult Delete(int id, int patientId)
        {
            if (IsPatientInCurrentClinic(patientId))
            {
                var status = new OperationStatus() { Success = false };

                var dischargePaper = RapidVetUnitOfWork.DischargePaperRepository.GetDischargePaper(id);
                if (patientId == dischargePaper.PatientId)
                {
                    status = RapidVetUnitOfWork.DischargePaperRepository.Delete(dischargePaper);
                }
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "DischargePapers");

            }
            throw new SecurityException();
        }

        public ActionResult PrePrint(int? id, string from, string to)
        {
            return View();
        }

        public JsonResult GetPrintingData(int? id, string from, string to)
        {
            //var fromDate = StringUtils.ParseStringToDateTime(from);
            //var toDate = StringUtils.ParseStringToDateTime(to);

            var dischargePapers = GetDischargePapers(id, from, to);

            var model =
                dischargePapers.Select(AutoMapper.Mapper.Map<DischargePaper, MedicalProcedurePrintModel>)
                                 .ToList();

            foreach (var p in model.Where(dp => dp.Letter == null))
            {
                var paper = dischargePapers.Single(mp => mp.Id == p.Id);
                var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(paper.PatientId);
                p.Letter =
                    RapidVetUnitOfWork.LetterTemplatesRepository.GetFormattedPatientDischargePaperDocument(patient, paper);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        [HttpPost]
        public JsonResult SaveBeforePrint(string letters)
        {
            if (!string.IsNullOrWhiteSpace(letters))
            {
                var printmodelLettes = JsonConvert.DeserializeObject<List<MedicalProcedurePrintModel>>(letters);
                foreach (var pml in printmodelLettes)
                {
                    var dischargePaper = RapidVetUnitOfWork.DischargePaperRepository.GetDischargePaper(pml.Id);
                    AutoMapper.Mapper.Map(pml, dischargePaper);
                    dischargePaper.Updated = DateTime.Now;
                }
                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return Json(status.Success);

            }
            return Json(false);
        }

        public ActionResult Print(int? id, string from, string to)
        {
            var model = GetDischargePapers(id, from, to);
            return View(model);
        }

        private List<DischargePaper> GetDischargePapers(int? id, string from, string to)
        {
            var result = new List<DischargePaper>();
            var fromDate = StringUtils.ParseStringToDateTime(from);
            var toDate = StringUtils.ParseStringToDateTime(to);

            var singleItem = id.HasValue && id.Value > 0;

            var multipleItems = fromDate > DateTime.MinValue && toDate > DateTime.MinValue;

            if (singleItem || multipleItems)
            {
                if (singleItem)
                {
                    result.Add(RapidVetUnitOfWork.DischargePaperRepository.GetDischargePaper(id.Value));
                }
                else if (multipleItems)
                {
                    result = RapidVetUnitOfWork
                        .DischargePaperRepository
                        .GetDischargePapers(CurrentUser.ActiveClinicId,
                                            fromDate, toDate)
                        .ToList();
                }

            }

            return result;
        }
    }
}