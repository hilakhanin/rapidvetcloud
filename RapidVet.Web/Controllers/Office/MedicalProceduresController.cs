﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Patients;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.MedicalProcedures;
using RapidVet.WebModels.Patients;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class MedicalProceduresController : BaseController
    {
        public ActionResult Index(string fromDate, string toDate)
        {
            var now = DateTime.Now;
            var lastDateCurrentMonth = DateTime.DaysInMonth(now.Year, now.Month);
            var firstDateNextMonth = new DateTime(now.Year, now.Month, lastDateCurrentMonth).AddDays(1);

            var from = new DateTime(now.Year, now.Month, 1);
            if (!string.IsNullOrWhiteSpace(fromDate))
            {
                DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out from);
            }
            ViewBag.From = from.ToString("dd/MM/yyyy");

            var to = firstDateNextMonth;
            if (!string.IsNullOrWhiteSpace(toDate))
            {
                DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out to);
            }
            ViewBag.To = to.ToString("dd/MM/yyyy");


            var medicalProcedures =
                RapidVetUnitOfWork.MedicalProcedureRepository.GetMedicalProcedures(CurrentUser.ActiveClinicId, from,
                                                                                   to.AddDays(1));
            var model = medicalProcedures.ToList();
            return View(model);

        }

        //id is patient id
        [HttpGet]
        public ActionResult Create(int? id)
        {
            var model = new MedicalProcedureWebModel()
                {
                    ProcedureDate = DateTime.Now,
                    Start = DateTime.Now,
                    Finish = DateTime.Now.AddHours(1)
                };
            if (id != null)
            {
                var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id.Value);
                ViewBag.ClientId = patient.ClientId;
                ViewBag.PatientId = id;
            }
            else
            {
                ViewBag.ClientId = 0;
                ViewBag.PatientId = 0;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(MedicalProcedureWebModel model, bool print = false)
        {
            if (IsPatientInCurrentClinic(model.PatientId))
            {
                if (ModelState.IsValid)
                {
                    var medicalProcedure = AutoMapper.Mapper.Map<MedicalProcedureWebModel, MedicalProcedure>(model);
                    var status = RapidVetUnitOfWork.MedicalProcedureRepository.Create(medicalProcedure);
                    if (status.Success)
                    {
                        SetSuccessMessage();
                        return print
                               ? RedirectToAction("PrePrint", "MedicalProcedures", new { id = medicalProcedure.Id })
                               : RedirectToAction("Index", "MedicalProcedures");
                    }
                    SetErrorMessage();
                }

                ViewBag.ClientId = Request.Form["clientId"];
                ViewBag.PatientId = model.PatientId;
                return View(model);
            }
            throw new SecurityException();
        }

        //id is medicalProcedureId
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var medicalProcedure =
                  RapidVetUnitOfWork.MedicalProcedureRepository.GetMedicalProcedure(id);
            if (IsPatientInCurrentClinic(medicalProcedure.PatientId))
            {
                var model = AutoMapper.Mapper.Map<MedicalProcedure, MedicalProcedureWebModel>(medicalProcedure);

                ViewBag.ClientId = medicalProcedure.Patient.ClientId;
                ViewBag.PatientId = medicalProcedure.PatientId;

                return View(model);
            }
            throw new SecurityException();
        }


        [HttpPost]
        public ActionResult Edit(MedicalProcedureWebModel model, bool print = false)
        {
            if (IsPatientInCurrentClinic(model.PatientId))
            {
                if (ModelState.IsValid)
                {
                    var medicalProcedure =
                   RapidVetUnitOfWork.MedicalProcedureRepository.GetMedicalProcedure(model.Id);
                    if (model.PatientId == medicalProcedure.PatientId)
                    {
                        AutoMapper.Mapper.Map(model, medicalProcedure);
                        medicalProcedure.Updated = DateTime.Now;

                        medicalProcedure.Start =
                            model.ProcedureDate.AddHours(medicalProcedure.Start.Hour)
                                 .AddMinutes(medicalProcedure.Start.Minute);

                        if (medicalProcedure.Finish.HasValue && medicalProcedure.Finish.Value > DateTime.MinValue)
                        {
                            medicalProcedure.Finish = model.ProcedureDate.AddHours(medicalProcedure.Finish.Value.Hour)
                                                           .AddMinutes(medicalProcedure.Finish.Value.Minute);
                        }
                        else
                        {
                            medicalProcedure.Finish = model.ProcedureDate;
                        }

                        var status = RapidVetUnitOfWork.Save();
                        if (status.Success)
                        {
                            SetSuccessMessage();
                            return print
                                       ? RedirectToAction("PrePrint", "MedicalProcedures", new { id = medicalProcedure.Id })
                                       : RedirectToAction("Index", "MedicalProcedures");
                        }
                        SetErrorMessage();
                    }
                }
                ViewBag.ClientId = Request.Form["clientId"];
                ViewBag.PatientId = model.PatientId;
                return View(model);
            }
            throw new SecurityException();
        }

        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            var medicalProcedure = RapidVetUnitOfWork.MedicalProcedureRepository.GetMedicalProcedure(id);
            if (IsPatientInCurrentClinic(medicalProcedure.PatientId))
            {
                return PartialView("_Delete", medicalProcedure);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult Delete(int id, int patientId)
        {
            if (IsPatientInCurrentClinic(patientId))
            {
                var status = new OperationStatus() { Success = false };

                var medicalProcedure = RapidVetUnitOfWork.MedicalProcedureRepository.GetMedicalProcedure(id);
                if (patientId == medicalProcedure.PatientId)
                {
                    status = RapidVetUnitOfWork.MedicalProcedureRepository.Delete(medicalProcedure);
                }
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "MedicalProcedures");

            }
            throw new SecurityException();
        }

        public ActionResult PrePrint(int? id)
        {
            return View();
        }

        public JsonResult GetPrintingData(int? id, string from, string to)
        {
            var medicalProcedures = GetMedicalProcedures(id, from, to);

            var model =
                medicalProcedures.Select(AutoMapper.Mapper.Map<MedicalProcedure, MedicalProcedurePrintModel>)
                                 .ToList();

            foreach (var p in model)//.Where(mp => mp.Letter == null)) fix for issue 595, slower but fixed
            {
                var procedure = medicalProcedures.Single(mp => mp.Id == p.Id);
                var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(procedure.PatientId);
                p.Letter =
                    RapidVetUnitOfWork.LetterTemplatesRepository.GetFormattedMedicalProcedureSummeryDocument(
                        patient,
                        procedure);

            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        [HttpPost]
        public JsonResult SaveBeforePrint(string letters)
        {
            if (!string.IsNullOrWhiteSpace(letters))
            {
                var counter = 0;
                var printmodelLettes = JsonConvert.DeserializeObject<List<MedicalProcedurePrintModel>>(letters);
                foreach (var pml in printmodelLettes)
                {
                    var medicalProcedure = RapidVetUnitOfWork.MedicalProcedureRepository.GetMedicalProcedure(pml.Id);
                    if (medicalProcedure.Letter != pml.Letter)
                    {
                        counter++;
                    }
                    AutoMapper.Mapper.Map(pml, medicalProcedure);

                }
                var status = RapidVetUnitOfWork.Save();
                if (status.Success || counter == 0)
                {
                    status.Success = true;
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return Json(status.Success);

            }
            return Json(false);
        }

        public ActionResult Print(int? id, string from, string to)
        {
            var medicalProcedures = GetMedicalProcedures(id, from, to);
            return View(medicalProcedures);
        }

        private List<MedicalProcedure> GetMedicalProcedures(int? id, string from, string to)
        {
            var result = new List<MedicalProcedure>();
            var fromDate = StringUtils.ParseStringToDateTime(from);
            var toDate = StringUtils.ParseStringToDateTime(to);

            var singleItem = id.HasValue && id.Value > 0;

            var multipleItems = fromDate > DateTime.MinValue && toDate > DateTime.MinValue;

            if (singleItem || multipleItems)
            {
                if (singleItem)
                {
                    result.Add(RapidVetUnitOfWork.MedicalProcedureRepository.GetMedicalProcedure(id.Value));
                }
                else if (multipleItems)
                {
                    result =
                        RapidVetUnitOfWork.MedicalProcedureRepository
                                          .GetMedicalProcedures(CurrentUser.ActiveClinicId, fromDate, toDate)
                                          .ToList();

                }

            }

            //foreach (var item in result)
            //{

            //    item.Letter =
            //        RapidVetUnitOfWork.LetterTemplatesRepository.GetFormattedMedicalProcedureSummeryDocument(
            //            item.Patient, item);
            //}

            return result;
        }

    }
}