﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model.Clinics;
using RapidVet.Model.PatientFollowUp;
using RapidVet.Model.Patients;
using RapidVet.WebModels.PatientFollowUps;
using apidVet.WebModels.FollowUp;
using RestSharp;

namespace RapidVet.Web.Controllers
{
    public class FollowUpsController : BaseController
    {
        enum FollowUpsPrintOutput
        {
            Sms, Email
        }
        public ActionResult Index()
        {
            var docs = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId);
            if (docs == null)
                docs = new List<Model.Users.User>();

            docs.Insert(0, new Model.Users.User()
            {
                Id = -1,
                FirstName = "הכל"
            });
            ViewBag.Doctors = docs;
            ViewBag.Email = IsEmailActive() && IsFollowUpEmailExist();
            ViewBag.Sms = IsSmsActive() && IsFollowUpSmsExist();
            ViewBag.Letter = IsPersonalLetterExist();

            var from = DateTime.Now;
            var to = DateTime.Now;

            switch (CurrentUser.ActiveClinic.FollowUpTimeRange)
            {
                case FollowUpTimeRange.Days:
                    from = from.AddDays(CurrentUser.ActiveClinic.FollowUpPast * -1);
                    to = to.AddDays(CurrentUser.ActiveClinic.FollowUpFuture);
                    break;
                case FollowUpTimeRange.Months:
                    from = from.AddMonths(CurrentUser.ActiveClinic.FollowUpPast * -1);
                    to = to.AddMonths(CurrentUser.ActiveClinic.FollowUpFuture);
                    break;
            }

            ViewBag.From = from.ToShortDateString();
            ViewBag.To = to.ToShortDateString();
            return View();
        }


        public JsonResult GetFollowUps(string from, string to, bool includeInactivePatients, string docID)
        {
            var model = GetFollowUpsData(from, to, includeInactivePatients, docID);
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Create()
        {
            var defaultDate = DateTime.Now;
            switch (CurrentUser.ActiveClinic.DefaultFollowUpType)
            {
                case FollowUpTimeRange.Days:
                    defaultDate = defaultDate.AddDays(CurrentUser.ActiveClinic.DefaultFollowUpTime);
                    break;
                case FollowUpTimeRange.Months:
                    defaultDate = defaultDate.AddMonths(CurrentUser.ActiveClinic.DefaultFollowUpTime);
                    break;
            }
            var model = new FollowUp()
                {
                    DateTime = defaultDate,
                    UserId = CurrentUser.Id,
                };
            return View(model);
        }

        [HttpPost]
        public JsonResult Create(FollowUpWebModel model)
        {
            var date = StringUtils.ParseStringToDateTime(model.DateTime);
            if (date > DateTime.MinValue)
            {
                FollowUp followUp = new FollowUp()
                {
                    Created = DateTime.Now,
                    PatientId = model.PatientId,
                    DateTime = date,
                    Comment = model.Comment,
                    UserId = model.UserId
                };

                var status = RapidVetUnitOfWork.FollowUpRepository.Create(followUp);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }

                return Json(status.Success);

            }

            return Json(false);
        }

        [HttpPost]
        public void CreateFromPatientFile(FollowUpWebModel model)
        {
            var date = StringUtils.ParseStringToDateTime(model.DateTime);
            if (date == DateTime.MinValue)
            {
                date = DateTime.Now;
            }
            if (date > DateTime.MinValue)
            {
                FollowUp followUp = new FollowUp()
                {
                    Created = DateTime.Now,
                    PatientId = model.PatientId,
                    DateTime = date,
                    Comment = model.Comment,
                    UserId = model.UserId
                };

                var status = RapidVetUnitOfWork.FollowUpRepository.Create(followUp);
                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }



            }

        }

        [HttpPost]
        public ActionResult Update(string json)
        {
            if (!string.IsNullOrWhiteSpace(json))
            {
                var viewfollowUp = JsonConvert.DeserializeObject<FollowUpIndexModel>(json);
                var followUp = RapidVetUnitOfWork.FollowUpRepository.GetFollowUp(viewfollowUp.FollowUpId);
                followUp.Comment = viewfollowUp.Comment;
                if (!string.IsNullOrWhiteSpace(viewfollowUp.DateTime))
                {
                    var followupDate = StringUtils.ParseStringToDateTime(viewfollowUp.DateTime);
                    if (followupDate > DateTime.MinValue)
                    {
                        followUp.DateTime = followupDate;
                    }
                }

                if (viewfollowUp.DoctorId > 0)
                {
                    followUp.UserId = viewfollowUp.DoctorId;
                }
                followUp.Updated = DateTime.Now;
                var status = RapidVetUnitOfWork.Save();

                if (status.Success) { SetSuccessMessage(); }
                else { SetErrorMessage(); }

                return Json(status.Success);
            }
            SetErrorMessage("לא נשלח מעקב");
            return Json(false);
        }

        //id is followUp id
        [HttpPost]
        public JsonResult Delete(int id)
        {
            var status = RapidVetUnitOfWork.FollowUpRepository.Delete(id);
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return Json(status.Success);
        }

        [HttpPost]
        public JsonResult RenewAll(int minDaysFromFollowUpWithoutAppointment, int remindersAddMonths)
        {
            var status = RapidVetUnitOfWork.FollowUpRepository.RenewAllFollowUps(CurrentUser.ActiveClinicId,
                                                                                 minDaysFromFollowUpWithoutAppointment,
                                                                                 remindersAddMonths);
            return Json(status);
        }

        [HttpPost]
        public JsonResult SendSMS(string clients, bool single = false)
        {
            var template = CurrentUser.ActiveClinicGroup.FollowUpsSmsTemplate;
            int messagesCounter = 0;
            int totalMessages = 0;
            if (!string.IsNullOrWhiteSpace(clients) && !string.IsNullOrWhiteSpace(template))
            {
                var data = GetClients(clients, single);

                foreach (var followUpIndexModel in data.Where(d => !string.IsNullOrWhiteSpace(d.CellPhone)))
                {
                    totalMessages++;
                    var followUp = RapidVetUnitOfWork.FollowUpRepository.GetFollowUp(followUpIndexModel.FollowUpId);
                    var nvc = GetNvcResults(followUp);
                    var message = GetTempalteWithData(template, followUpIndexModel);//RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template);
                    var sms = new Unicell.Sms(CurrentUser.ActiveClinicGroup.UnicellUserName,
                                              CurrentUser.ActiveClinicGroup.UnicellPassword,
                                              followUpIndexModel.CellPhone, message, string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.SignatureSmsTemplate) ? "" : CurrentUser.ActiveClinicGroup.SignatureSmsTemplate);
                    var sendResult = sms.Send();
                    if (sendResult)
                    {
                        RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, 1, MessageType.Sms);
                        string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                Resources.Auditing.SendingModule, Resources.Auditing.FollowUps,
                                                                Resources.Auditing.ClientName, followUpIndexModel.ClientName,
                                                                Resources.Auditing.PhoneNumber, followUpIndexModel.CellPhone,
                                                                Resources.Auditing.SmsContent, message,
                                                                Resources.Auditing.ReturnCode, sendResult);

                        RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.SmsSent, logMessage, "");
                        messagesCounter += 1;
                    }
                    else
                    {
                        SetErrorMessage("אירעה שגיאה בשליחת ההודעות");
                        return null;
                    }
                }
                //SetSuccessMessage("ההודעות נשלחו בהצלחה");

                SetSuccessMessage(string.Format(Resources.LetterTemplate.SmsSuccessMessage, messagesCounter, totalMessages - messagesCounter < 0 ? 0 : totalMessages - messagesCounter, totalMessages));
                return null;
            }
            SetErrorMessage("אין כתובות למשלוח");
            return null;
        }

        string emailTemplatePrefix = "<div style=\"position:relative;display:block;margin:auto;direction:rtl;\">";
        string emailTemplatePostfix = "</div>";

        [HttpPost]
        public JsonResult SendEmail(string clients, bool single = false)
        {
            var bodyTemplate = CurrentUser.ActiveClinicGroup.FollowUpsEmailTemplate;
            int messagesCounter = 0;
            int totalMessages = 0;
            if (!string.IsNullOrWhiteSpace(clients) && !string.IsNullOrWhiteSpace(bodyTemplate))
            {
                var data = GetClients(clients, single);
                var mailClient = getMailClient();
                string from = string.Format("{0}<{1}>", !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.EmailBusinessName) ? CurrentUser.ActiveClinicGroup.EmailBusinessName : CurrentUser.ActiveClinic.Name,
                                                    CurrentUser.ActiveClinicGroup.EmailAddress);

                foreach (var item in data.Where(d => !string.IsNullOrWhiteSpace(d.Email)))
                {
                    totalMessages++;
                    var followUp = RapidVetUnitOfWork.FollowUpRepository.GetFollowUp(item.FollowUpId);
                    var nvc = GetNvcResults(followUp);
                    nvc.Add("\r\n", "<br />");

                    RestRequest request = new RestRequest();
                    request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_RAPID_DOMAIN"], ParameterType.UrlSegment);
                    request.Resource = "{domain}/messages";
                    request.AddParameter("from", from);
                    request.AddParameter("subject", GetTempalteWithData(CurrentUser.ActiveClinicGroup.FollowUpsEmailSubject, item));
                    request.AddParameter("html", emailTemplatePrefix + GetTempalteWithData(bodyTemplate, item) + emailTemplatePostfix);
                    request.Method = Method.POST;

                    request.AddParameter("to", item.Email);
                    var message = mailClient.Execute(request);
                    if ((int)message.StatusCode == 200)
                    {
                        RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, 1, MessageType.Email);
                        string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                Resources.Auditing.SendingModule, Resources.Auditing.FollowUps,
                                                                Resources.Auditing.ClientName, item.ClientName,
                                                                Resources.Auditing.Address, item.Email,
                                                                Resources.Auditing.MailSubject, GetTempalteWithData(CurrentUser.ActiveClinicGroup.FollowUpsEmailSubject, item),
                                                                Resources.Auditing.ReturnCode, (int)message.StatusCode + " - " + message.StatusCode);

                        RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.MailSent, logMessage, "");
                        messagesCounter += 1;
                    }
                }
                var status = string.Empty;
                if (string.IsNullOrWhiteSpace(status))
                {
                    SetSuccessMessage(string.Format(Resources.LetterTemplate.EmailSuccessMessage, messagesCounter, totalMessages - messagesCounter < 0 ? 0 : totalMessages - messagesCounter, totalMessages));
                    //SetSuccessMessage("ההודעות נשלחו בהצלחה");
                }
                else
                {
                    SetErrorMessage("אירעה שגיאה במשלוח ההודעות");
                }
                return null;
            }
            SetErrorMessage("לא ניתן לשלוח את ההודעות");
            return null;
        }

        private NameValueCollection GetNvcResults(FollowUp followUp)
        {
            var nvc = new NameValueCollection();

            nvc.Add(getTemplateKeyWord("ClinicAddress"), CurrentUser.ActiveClinic.Address);
            nvc.Add(getTemplateKeyWord("ClinicName"), CurrentUser.ActiveClinic.Name);
            nvc.Add(getTemplateKeyWord("ClinicPhone"), CurrentUser.ActiveClinic.Phone);
            nvc.Add(getTemplateKeyWord("ClinicFax"), CurrentUser.ActiveClinic.Fax);
            nvc.Add(getTemplateKeyWord("ClinicEmail"), CurrentUser.ActiveClinic.Email);
            nvc.Add(getTemplateKeyWord("ClinicLogo"), "<img src=\"/ClinicGroups/LogoImage\" style=\"max-height: 150px;max-width: 150px;\">");
            nvc.Add(getTemplateKeyWord("ClientTitle"), followUp.Patient.Client.Title != null ? followUp.Patient.Client.Title.Name : string.Empty);
            nvc.Add(getTemplateKeyWord("ClientName"), followUp.Patient.Client.Name);
            nvc.Add(getTemplateKeyWord("ClientFirstName"), followUp.Patient.Client.FirstName);
            nvc.Add(getTemplateKeyWord("ClientLastName"), followUp.Patient.Client.LastName);
            nvc.Add(getTemplateKeyWord("DoctorName"), followUp.User.Name);
            nvc.Add(getTemplateKeyWord("Comment"), followUp.Comment);
            nvc.Add(getTemplateKeyWord("PatientName"), followUp.Patient.Name);


            return nvc;
        }

        //id is followUp id
        public ActionResult Letter(int id, int LetterId, string clients, bool print = false)
        {
            var followUp = RapidVetUnitOfWork.FollowUpRepository.GetFollowUp(id);
            var followUpIndexModel = GetClients(clients, true);
            var template = (RapidVetUnitOfWork.LetterTemplatesRepository.GetPersonalTemplate(LetterId)).Content;// CurrentUser.ActiveClinicGroup.PersonalLetterTemplate;
            if (!string.IsNullOrWhiteSpace(template))
            {
                var nvc = GetNvcResults(followUp);

                ViewBag.Letter = GetTempalteWithData(template, followUpIndexModel.Single(f => f.FollowUpId == id));//RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template);
                ViewBag.Print = print;
                ViewBag.Id = id;

                var filterString = string.Format("LetterId={0}&clients={1}", LetterId, clients);

                ViewBag.PrintUrl = string.Format("{0}?{1}&print={2}", Url.Action("Letter", "FollowUps"), filterString, true);

                return View();
            }
            return View("Error");
        }
        //id is followUp id
        [HttpPost]
        public JsonResult RenewSingle(int id, string date)
        {
            var followUp = RapidVetUnitOfWork.FollowUpRepository.GetFollowUp(id);

            followUp.DateTime = StringUtils.ParseStringToDateTime(date);
            followUp.Updated = DateTime.Now;

            var status = RapidVetUnitOfWork.Save();

            if (status.Success)
            { SetSuccessMessage("הנתונים נשמרו בהצלחה"); }
            else { SetErrorMessage(); }
            return Json(status.Success);
        }

        public ActionResult Settings()
        {
            var model = AutoMapper.Mapper.Map<Clinic, FollowUpSettingsModel>(CurrentUser.ActiveClinic);
            return View(model);
        }

        [HttpPost]
        public ActionResult Settings(FollowUpSettingsModel model)
        {
            if (ModelState.IsValid)
            {
                var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(CurrentUser.ActiveClinicId);
                AutoMapper.Mapper.Map(model, clinic);
                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    SetSuccessMessage();
                    CurrentUser.UpdateClinic(clinic);
                }
                else
                {
                    SetErrorMessage();
                }
                return View();
            }
            return View(model);
        }

        private List<FollowUpIndexModel> GetFollowUpsData(string from, string to, bool includeInactivePatients, string docID)
        {
            var result = new List<FollowUpIndexModel>();
            DateTime? fromDate = null;
            DateTime? toDate = null;
            int docIDInt;
            if (!Int32.TryParse(docID, out docIDInt))
                docIDInt = -1;

            if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
            {
                fromDate = StringUtils.ParseStringToDateTime(from);
                toDate = StringUtils.ParseStringToDateTime(to);
            }
            var data = RapidVetUnitOfWork.FollowUpRepository.GetFollowUps(CurrentUser.ActiveClinicId, fromDate, toDate, includeInactivePatients, docIDInt);
            foreach (var d in data)
            {
                var nextAppointment = d.Patient.Client.CalenderEntries.FirstOrDefault(c => c.ClientId == d.Patient.ClientId && c.Date > DateTime.Now);
                var temp = new FollowUpIndexModel()
                    {
                        FollowUpId = d.Id,
                        ClientId = d.Patient.ClientId,
                        ClientTitle = d.Patient.Client.Title != null ? d.Patient.Client.Title.Name : string.Empty,
                        ClientName = d.Patient.Client.Name,
                        CellPhone =
                            RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(d.Patient.Client.Phones,
                                                                                        PhoneTypeEnum.Mobile),
                        HomePhone =
                            RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(d.Patient.Client.Phones,
                                                                                        PhoneTypeEnum.Home),
                        Email = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientEmail(d.Patient.Client.Emails),
                        Address =
                            RapidVetUnitOfWork.LetterTemplatesRepository.GetClientAddress(d.Patient.Client.Addresses),
                        PatientName = d.Patient.Name,
                        PatientKind = d.Patient.AnimalRace.AnimalKind.Value,
                        PatientGender = d.Patient.GenderId == (int)GenderEnum.Female ? "נקבה" : "זכר",
                        Created = d.Created.ToShortDateString(),
                        Comment = d.Comment,
                        NextAppointment = nextAppointment != null ? nextAppointment.Date.ToShortDateString() : "",
                        Doctors = d.User.Name,
                        DoctorId = d.UserId,
                        DateTime = d.DateTime.ToShortDateString()

                    };

                if (temp.CellPhone != null)
                {
                    temp.CellPhoneComment = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhoneComment(d.Patient.Client.Phones,
                                                                                        temp.CellPhone);
                }
                if (string.IsNullOrWhiteSpace(temp.CellPhoneComment))
                {
                    temp.CellPhoneComment = " ";
                }

                if (temp.HomePhone != null)
                {
                    temp.HomePhoneComment = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhoneComment(d.Patient.Client.Phones,
                                                                                        temp.HomePhone);
                }
                if (string.IsNullOrWhiteSpace(temp.HomePhoneComment))
                {
                    temp.HomePhoneComment = " ";
                }

                result.Add(temp);
            }

            return result;
        }


        private List<FollowUpIndexModel> GetClients(string clients, bool single)
        {
            var result = new List<FollowUpIndexModel>();
            if (single)
            {
                result.Add(JsonConvert.DeserializeObject<FollowUpIndexModel>(clients));
            }
            else
            {
                result = JsonConvert.DeserializeObject<List<FollowUpIndexModel>>(clients);
            }
            return result;
        }


        private string GetTempalteWithData(string template, FollowUpIndexModel item)
        {
            var nvc = new NameValueCollection();
            var client = RapidVetUnitOfWork.ClientRepository.GetClient(item.ClientId);
            var doctor = CurrentUser.IsDoctor ? CurrentUser : CurrentUser.DefaultDrId.HasValue ? RapidVetUnitOfWork.UserRepository.GetItem(CurrentUser.DefaultDrId.Value) : null;
            var followUp = RapidVetUnitOfWork.FollowUpRepository.GetFollowUp(item.FollowUpId);

            nvc.Add(getTemplateKeyWord("ClinicAddress"), CurrentUser.ActiveClinic.Address);
            nvc.Add(getTemplateKeyWord("ClinicName"), CurrentUser.ActiveClinic.Name);
            nvc.Add(getTemplateKeyWord("ClinicPhone"), CurrentUser.ActiveClinic.Phone);
            nvc.Add(getTemplateKeyWord("ClinicFax"), CurrentUser.ActiveClinic.Fax);
            nvc.Add(getTemplateKeyWord("ClinicEmail"), CurrentUser.ActiveClinic.Email);
            nvc.Add(getTemplateKeyWord("ClinicLogo"), "<img src=\"/ClinicGroups/LogoImage\" style=\"max-height: 150px;max-width: 150px;\">");
            nvc.Add(getTemplateKeyWord("ClientTitle"), item.ClientTitle ?? string.Empty);
            nvc.Add(getTemplateKeyWord("ClientFirstName"), client.FirstName);
            nvc.Add(getTemplateKeyWord("ClientLastName"), client.LastName);
            nvc.Add(getTemplateKeyWord("ClientName"), item.ClientName);
            nvc.Add(getTemplateKeyWord("ClientMobilePhone"), item.CellPhone);
            nvc.Add(getTemplateKeyWord("ClientHomePhone"), item.HomePhone);
            nvc.Add(getTemplateKeyWord("ClientAddress"), item.Address);
            nvc.Add(getTemplateKeyWord("PatientName"), item.PatientName);
            nvc.Add(getTemplateKeyWord("PatientAnimalKind"), item.PatientKind);
            nvc.Add(getTemplateKeyWord("DoctorName"), item.Doctors);
            nvc.Add(getTemplateKeyWord("ClientTitle"), item.ClientTitle);
            nvc.Add(getTemplateKeyWord("Comment"), item.Comment);
            nvc.Add(getTemplateKeyWord("ActiveDoctorName"), doctor != null ? doctor.Name : "");
            nvc.Add(getTemplateKeyWord("ActiveDoctorLicence"), doctor != null ? doctor.LicenceNumber : "");
            nvc.Add(getTemplateKeyWord("Date"), DateTime.Now.ToShortDateString());
            nvc.Add(getTemplateKeyWord("ClientReferer"), followUp.Patient.Client.ReferredBy);
            nvc.Add(getTemplateKeyWord("PatientRace"), followUp.Patient.AnimalRace.Value);
            nvc.Add(getTemplateKeyWord("PatientColor"), followUp.Patient.AnimalColor != null ? followUp.Patient.AnimalColor.Value : "");
            nvc.Add(getTemplateKeyWord("PatientSex"), followUp.Patient.GenderId == 1 ? (Resources.Gender.Male) : (followUp.Patient.GenderId == 2 ? Resources.Gender.Female : Resources.Gender.UndefinedSex));
            nvc.Add(getTemplateKeyWord("ClientBalance"), followUp.Patient.Client.Balance.ToString());
            nvc.Add(getTemplateKeyWord("ClientIdCardNumber"), followUp.Patient.Client.IdCard);
            nvc.Add(getTemplateKeyWord("ClientCity"), RapidVetUnitOfWork.LetterTemplatesRepository.GetClientCity(followUp.Patient.Client.Addresses));
            nvc.Add(getTemplateKeyWord("ClientZip"), RapidVetUnitOfWork.LetterTemplatesRepository.GetClientZipCode(followUp.Patient.Client.Addresses));
            nvc.Add(getTemplateKeyWord("ClientStreetAndNumber"), RapidVetUnitOfWork.LetterTemplatesRepository.GetClientStreetAndNumber(followUp.Patient.Client.Addresses));
            nvc.Add(getTemplateKeyWord("ClientRecordNumber"), followUp.Patient.Client.LocalId.ToString());

            if (followUp.Patient.BirthDate.HasValue)
            {
                AgeHelper age = new AgeHelper(followUp.Patient.BirthDate.Value, DateTime.Today);
                nvc.Add(getTemplateKeyWord("PatientAge"), age.Years + "." + age.Months);
            }
            else
            {
                nvc.Add(getTemplateKeyWord("PatientAge"), "");
            }

            if (followUp.Patient.Client.BirthDate.HasValue)
            {
                AgeHelper age = new AgeHelper(followUp.Patient.Client.BirthDate.Value, DateTime.Today);
                nvc.Add(getTemplateKeyWord("ClientAge"), age.Years + "." + age.Months);
                nvc.Add(getTemplateKeyWord("ClientBirthdate"), followUp.Patient.Client.BirthDate.Value.ToShortDateString());
            }
            else
            {
                nvc.Add(getTemplateKeyWord("ClientAge"), "");
            }
            return RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template);
        }
    }
}
