﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using Newtonsoft.Json.Linq;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Finance;
using RapidVet.Model.Patients;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Quotations;
using RapidVet.Model.TreatmentPackages;
using RapidVet.Model.Visits;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Patients;
using RapidVet.WebModels.Quotations;
using RestSharp;
using RapidVet.Model.Clinics;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class QuotationsController : BaseController
    {
        /// <summary>
        /// displays client quotations
        /// </summary>
        /// <param name="id">clientId</param>
        /// <returns>ActionResult</returns>
        public ActionResult Index(int id)
        {
            var quotations = RapidVetUnitOfWork.QuotationRepository.GetClientQuotations(id);
            if (quotations.Count > 0)
                return View();

            if (!quotations.Any())
                return RedirectToAction("Quotation", "Quotations", new { id = id });

            var quotation = quotations.First();
            return RedirectToAction("Quotation", "Quotations", new { id = id, quotationId = quotation.Id });
        }

        public JsonResult GetClientQuotations(int id)
        {
            if (IsClientInCurrentClinic(id))
            {
                var quotations = RapidVetUnitOfWork.QuotationRepository.GetClientQuotations(id);
                var model = quotations.Select(AutoMapper.Mapper.Map<Quotation, QuotationJsonModel>).ToList();
                foreach (var q in model)
                {
                    q.EditUrl = Url.Action("Quotation", "Quotations", new { id = id, quotationId = q.Id });
                    q.ExecutionUrl = Url.Action("ExecuteQuotation", "Quotations", new { id = id, quotationId = q.Id });
                    q.FinanceUrl = Url.Action("InvoiceReceipt", "FinanceDocument", new { quotationId = q.Id, clientId = id });
                }
                return Json(model, JsonRequestBehavior.AllowGet);

            }
            throw new SecurityException();
        }


        public ActionResult Quotation(int id, int quotationId = 0)
        {
            ViewBag.BackUrl = HttpContext.Request == null ? null : HttpContext.Request.UrlReferrer;
            var model = GetClientDetailsModel(id);
            return View(model);
        }

        /// <summary>
        /// returns json data regarding the quotation
        /// </summary>
        /// <param name="id">clientId</param>
        /// <param name="quotationId">quotationId</param>
        /// <returns></returns>
        public JsonResult GetQuotationData(int id, int quotationId)
        {
            var packages = GetTreatmentPackagesList();

            var clientPatients = RapidVetUnitOfWork.PatientRepository.GetClientActivePatientsSimple(id);
            var patients = clientPatients.Select(AutoMapper.Mapper.Map<Patient, PatientIndexListModel>).ToList();

            var selectedPatientId = 0;
            var quotation = RapidVetUnitOfWork.QuotationRepository.GetQuotation(quotationId);

            if (quotation != null)
            {
                selectedPatientId = quotation.PatientId;
            }
            else if (patients.Count() == 1)
            {
                selectedPatientId = patients.First().Id;
            }

            var comments = RapidVetUnitOfWork.QuotationRepository.GetClinicQuotaionPrefix(CurrentUser.ActiveClinicId);

            var treatments = new List<QuotationTreatmentModel>();
            if (quotation != null)
            {
                treatments =
                    quotation.Treatments.Select(AutoMapper.Mapper.Map<QuotationTreatment, QuotationTreatmentModel>)
                             .ToList();

                comments = quotation.Comments;
            }

            var priceListCategories =
                RapidVetUnitOfWork.CategoryRepository.GetClinicActiveCategories(CurrentUser.ActiveClinicId, true);
            var categories =
                priceListCategories.Select(Mapper.Map<PriceListCategory, PriceListCategoryModel>).ToList();

            Dictionary<int, decimal> Dic = new Dictionary<int, decimal>();
            var priceListItems = RapidVetUnitOfWork.ItemRepository.GetPriceListItemsForClient(id, out Dic);

            var items = priceListItems.Select(Mapper.Map<PriceListItem, PriceListItemSingleTariffFlatModel>).ToList();
            decimal Val = 0;

            foreach (var item in items)
            {
                if (Dic.TryGetValue(item.Id, out Val))
                   item.Price = Val;
            }

            var model = new QuotationWebModel()
                {
                    ClientId = id,
                    Patients = patients,
                    PatientId = selectedPatientId,
                    Categories = categories,
                    Items = items,
                    Packages = packages,
                    Name = quotation != null ? quotation.Name : string.Empty,
                    Treatments = treatments,
                    Comments = comments,
                    // Discount = quotation != null ? quotation.Discount : 0,
                    // PercentDiscount = quotation != null ? quotation.PercentDiscount : 0

                };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTreatmentPackagesData()
        {
            var model = GetTreatmentPackagesList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQuotationTreatments(int id)
        {
            var treatments = RapidVetUnitOfWork.QuotationRepository.GetTreatments(id);
            var model = treatments.Select(AutoMapper.Mapper.Map<QuotationTreatment, QuotationTreatmentModel>)
                                  .ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetTreatmentPackageData(int id)
        {
            var package = RapidVetUnitOfWork.TreatmentPackageRepository.GetPackage(id);

            foreach (var treatment in package.Treatments)
            {
                treatment.Id = 0;
            }

            var model = package.Treatments.Select(Mapper.Map<PackageItem, QuotationTreatmentModel>).ToList();
            foreach (var quotationTreatmentModel in model)
            {
                quotationTreatmentModel.Comments = string.Empty;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveTreatmentPackage(int? packageId, string newPackageName, string treatments)
        {
            var status = new OperationStatus() { Success = false };
            if (!string.IsNullOrWhiteSpace(treatments))
            {
                var jArr = JArray.Parse(treatments);
                var quotationTreatments = jArr.ToObject<List<QuotationTreatmentModel>>();
                var packageItems =
                    quotationTreatments.Select(AutoMapper.Mapper.Map<QuotationTreatmentModel, PackageItem>).ToList();
                TreatmentPackage package;

                if ((!packageId.HasValue || packageId.Value == 0) && !string.IsNullOrWhiteSpace(newPackageName))
                {

                    package = new TreatmentPackage()
                        {
                            ClinicId = CurrentUser.ActiveClinicId,
                            Name = newPackageName,
                            Treatments = packageItems.Select(AutoMapper.Mapper.Map<PackageItem, PackageItem>).ToList()
                        };

                    status = RapidVetUnitOfWork.TreatmentPackageRepository.Create(package);
                    if (status.Success)
                    {
                        SetSuccessMessage();
                        status.ItemId = package.Id;
                    }
                    else
                    {
                        SetErrorMessage();
                    }

                }
                else if (packageId.HasValue && packageId.Value > 0)
                {
                    status = RapidVetUnitOfWork.TreatmentPackageRepository.Save(packageId.Value, packageItems);
                    if (status.Success)
                    {
                        status.ItemId = packageId.Value;
                        SetSuccessMessage();
                    }
                    else
                    {
                        status.ItemId = packageId.Value;
                        SetErrorMessage();
                    }
                }
            }
            return Json(status);
        }

        /// <summary>
        /// returns json data regarding the quotation
        /// </summary>
        /// <param name="id">patient id</param>
        /// <param name="quotationId">quotation id</param>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]
        public JsonResult SaveQuotation(QuotationWebModel model, bool print, bool printPrices, bool printComments,
                                        string treatments, bool executeTreatments)
        {
            Quotation quotation = null;
            var status = new OperationStatus() { Success = false };
            var result = new JsonResult() { Data = status.Success };

            var quotationTreatments = GetQuotationTreatments(treatments);

            if (model.Id == 0)
            {
                quotation = Mapper.Map<QuotationWebModel, Quotation>(model);
                quotation.Treatments = quotationTreatments;
                quotation.UserId = CurrentUser.Id;
                quotation.Created = DateTime.Now;
                quotation.ClinicId = CurrentUser.ActiveClinicId;
                status = RapidVetUnitOfWork.QuotationRepository.Create(quotation);
            }

            else if (model.Id > 0)
            {
                quotation = RapidVetUnitOfWork.QuotationRepository.GetQuotation(model.Id);
                Mapper.Map(model, quotation);
                quotation.Updated = DateTime.Now;
                status = RapidVetUnitOfWork.QuotationRepository.Save(quotation);
                var logContents = new List<string>();
                status = RapidVetUnitOfWork.QuotationRepository.UpdateQuotationTreatments(quotation.Id, quotationTreatments, executeTreatments, out logContents);
                if (status.Success)
                    foreach (var c in logContents)
                        RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, "תיק לקוח - מחיקת פריט", c, String.Empty);

                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
            }

            var quotationId = quotation != null ? quotation.Id : 0;

            var printUrl = Url.Action("Print", "Quotations",
                                     new
                                     {
                                         id = quotationId,
                                         printPrices = printPrices,
                                         printComments = printComments
                                     });


            if (status.Success)
            {
                if (executeTreatments)
                {
                    //if(quotation.User == null)
                    //{
                    //    quotation.User = CurrentUser;
                    //}
                    return executeQuotationTreatments(quotation.PatientId, quotation);
                }
            }


            result.Data = new
            {
                success = status.Success,
                print = print,
                url = printUrl,
                quotationId = quotationId
            };

            return result;
        }

        //id is quotationId
        [HttpGet]
        public ActionResult Print(int id, bool printPrices, bool printComments)
        {
            var quotation = RapidVetUnitOfWork.QuotationRepository.GetQuotation(id);
            var model = Mapper.Map<Quotation, QuotationWebModel>(quotation);
            ViewBag.Prices = printPrices;
            ViewBag.Comments = printComments;

            if (model.Patient.BirthDate.HasValue)
            {
                AgeHelper age = new AgeHelper(model.Patient.BirthDate, DateTime.Today);
                ViewBag.AgeInYears = age.Years;
                ViewBag.AgeInMonths = age.Months;
            }
            else
            {
                ViewBag.AgeInYears = "";
                ViewBag.AgeInMonths = "";
            }

            return View(model);
        }


        [HttpGet]
        public ActionResult Preface()
        {
            var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(CurrentUser.ActiveClinicId);
            var model = new QuotationPrefaceModel()
                {
                    QuotationPreface = clinic.QuotationPreface
                };
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Preface(QuotationPrefaceModel model)
        {
            var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(CurrentUser.ActiveClinicId);
            clinic.QuotationPreface = model.QuotationPreface;
            RapidVetUnitOfWork.Save();
            return RedirectToAction("Preface", "Quotations");
        }


        private List<TreatmentPackageModel> GetTreatmentPackagesList()
        {
            var treatmentPackages =
                RapidVetUnitOfWork.TreatmentPackageRepository.GetAllTreatmentPackages(CurrentUser.ActiveClinicId)
                                  .ToList();
            return
                treatmentPackages.Select(AutoMapper.Mapper.Map<TreatmentPackage, TreatmentPackageModel>).ToList();
        }

        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            var quotation = RapidVetUnitOfWork.QuotationRepository.GetQuotation(id);
            var model = AutoMapper.Mapper.Map<Quotation, QuotationWebModel>(quotation);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public ActionResult Delete(int id, string name)
        {
            var quotation = RapidVetUnitOfWork.QuotationRepository.GetQuotation(id);
            if (IsPatientInCurrentClinic(quotation.PatientId))
            {
                var clientId = quotation.Patient.ClientId;
                var status = RapidVetUnitOfWork.QuotationRepository.Delete(quotation);
                if (status)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index", "Quotations", new { id = clientId });
            }
            throw new SecurityException();
        }

        //id is patient id
        private JsonResult executeQuotationTreatments(int id, Quotation quotation)//string treatments, string comments, decimal totalPrice)
        {
            var result = new JsonResult()
                {
                    Data = new { success = false }
                };

            if (IsPatientInCurrentClinic(id))
            {
                var status = ExecuteQuotation(id, quotation);//treatments, comments, totalPrice);

                if (status.Success)
                {
                    SetSuccessMessage();
                    result.Data = new
                        {
                            success = true,
                            url = Url.Action("Index", "Visit", new { id = id, visitId = status.ItemId })
                        };
                }
                else
                {
                    SetErrorMessage();
                }

                return result;
            }
            throw new SecurityException();
        }

        public ActionResult ExecuteQuotation(int id, int quotationId)
        {
            if (IsClientInCurrentClinic(id))
            {
                var quotation = RapidVetUnitOfWork.QuotationRepository.GetQuotation(quotationId);
                var treatments = quotation.Treatments.Select(Mapper.Map<QuotationTreatment, VisitTreatment>).ToList();
                var visit = new Visit()
                {
                    PatientId = quotation.PatientId,
                    CreatedById = CurrentUser.Id,
                    CreatedDate = DateTime.Now,
                    VisitDate = DateTime.Now,
                    MainComplaint = Resources.Quotations.Manifestation,
                    Treatments = treatments,
                    // Price = quotation.TotalPrice,
                    DoctorId = CurrentUser.Id,
                    Close = false,
                    ClientIdAtTimeOfVisit = quotation.Patient.ClientId,
                    Active = true
                };

                visit.Treatments = UpadteVisitTreatments(visit.Treatments);

                var status = RapidVetUnitOfWork.VisitRepository.AddNewVisit(visit);
                if (status.Success)
                {
                    SetSuccessMessage();
                    return RedirectToAction("Index", "Visit", new { id = quotation.PatientId, visitId = visit.Id });
                }
                SetErrorMessage();
                return RedirectToAction("Index", "Quotations", new { id = id });

            }
            throw new SecurityException();
        }



        private OperationStatus ExecuteQuotation(int patientId, Quotation quotation)// string treatments, string comments, decimal totalPrice)
        {

            //var executionTreatments = GetQuotationTreatmentModels(treatments);
            var executionTreatments = quotation.Treatments.Where(e => e.WasTreatmentMovedToExecution && e.VisitId == null).ToList();

            var priceListItems = new List<PriceListItem>();

            foreach (var treatment in executionTreatments)
            {
                var priceListItem = RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(treatment.PriceListItemId.Value);
                priceListItems.Add(priceListItem);
            }

            var totalPrice = executionTreatments.Sum(t => t.Price * t.Amount - t.Discount);

            var preventiveItems =
                priceListItems.Where(
                    p =>
                    p.ItemTypeId == (int)PriceListItemTypeEnum.PreventiveTreatments ||
                    p.ItemTypeId == (int)PriceListItemTypeEnum.Vaccines).ToList();

            var visitPreventive = preventiveItems.Select(Mapper.Map<PriceListItem, PreventiveMedicineItem>).ToList();
            foreach (var preventiveItem in visitPreventive)
            {
                preventiveItem.PatientId = patientId;
                var quotationItem =
                    executionTreatments.FirstOrDefault(e => e.PriceListItemId == preventiveItem.Id);
                if (quotationItem != null)
                {
                    preventiveItem.Quantity = quotationItem.Amount;
                    preventiveItem.Discount = quotationItem.Discount.Value;//(quotationItem.Amount * quotationItem.Price) - quotationItem.TotalAmount;
                    preventiveItem.Price = quotationItem.Price;
                    preventiveItem.ReminderCount = 0;
                    preventiveItem.ExternalyPreformed = false;
                    preventiveItem.QuotationTreatmentId = quotationItem.Id;
                }
            }

            //var examinationItems = priceListItems.Where(p => p.ItemTypeId == (int)PriceListItemTypeEnum.Examinations);
            //var examinationIds = examinationItems.Select(t => t.Id).ToList();
            //var quotationExaminations = executionTreatments.Where(e => examinationIds.Contains(e.PriceListItemId.Value));
            //var visitExaminations =
            //    quotationExaminations.Select(Mapper.Map<QuotationTreatment, VisitExamination>).ToList();

            var examinationItems = priceListItems.Where(p => p.ItemTypeId == (int)PriceListItemTypeEnum.Examinations);
            var visitExaminations = new List<VisitExamination>();
            foreach (var item in examinationItems)
            {
                var quotationItem =
                    executionTreatments.FirstOrDefault(e => e.PriceListItemId == item.Id);

                if (quotationItem != null)
                {
                    var temp = new VisitExamination()
                        {
                            Id = quotationItem.Id,
                            PriceListItemId = item.Id,
                            CategoryName = item.Category.Name,
                            Name = item.Name,
                            Quantity = quotationItem.Amount,
                            Discount = quotationItem.Discount.Value,//(quotationItem.Amount * quotationItem.Price) - quotationItem.TotalAmount,
                            Price = quotationItem.Amount * quotationItem.Price - quotationItem.Discount.Value,
                            UnitPrice = quotationItem.Price,
                            QuotationTreatmentId = quotationItem.Id,
                            AddedBy = quotation.User == null ? CurrentUser.Name : quotation.User.Name,
                            AddedByUserId = quotation.User == null ? CurrentUser.Id : quotation.UserId
                        };

                    visitExaminations.Add(temp);
                }
            }

            var treatmentItems = priceListItems.Where(
                p =>
                p.ItemTypeId != (int)PriceListItemTypeEnum.PreventiveTreatments &&
                p.ItemTypeId != (int)PriceListItemTypeEnum.Vaccines &&
                p.ItemTypeId != (int)PriceListItemTypeEnum.Examinations).ToList();

            var treatmentIds = treatmentItems.Select(t => t.Id).ToList();
            var quotationTreatments = executionTreatments.Where(e => treatmentIds.Contains(e.PriceListItemId.Value));
            var visitTreatments =
                quotationTreatments.Select(Mapper.Map<QuotationTreatment, VisitTreatment>).ToList();

            var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(patientId);
            int? visitId;
            var lastVisitDate = RapidVetUnitOfWork.VisitRepository.GetLastPatientVisitDate(patient.Id, out visitId);
            Visit visit;
            OperationStatus status;

            if (lastVisitDate.HasValue && lastVisitDate.Value.Date.Equals(DateTime.Now.Date)) //the patient has an open visit from today. add quotation to it.
            {
                visit = RapidVetUnitOfWork.VisitRepository.GetVisit(visitId.Value);
                //add data to current visit
                if (String.IsNullOrWhiteSpace(visit.MainComplaint))
                    visit.MainComplaint = Resources.Quotations.Manifestation;

                foreach (var t in visitTreatments)
                {
                    t.AddedBy = quotation.User == null ? CurrentUser.Name : quotation.User.Name;
                    t.AddedByUserId = quotation.User == null ? CurrentUser.Id : quotation.UserId;
                    visit.Treatments.Add(t);
                }
                foreach (var vp in visitPreventive) { visit.PreventiveMedicineItems.Add(vp); }
                foreach (var ve in visitExaminations) { visit.Examinations.Add(ve); }
                visit.Price += totalPrice;
                status = RapidVetUnitOfWork.VisitRepository.UpdateVisit(visit);
            }
            else
            {
                visit = new Visit()
                    {
                        PatientId = patientId,
                        CreatedById = CurrentUser.Id,
                        CreatedDate = DateTime.Now,
                        VisitDate = DateTime.Now,
                        MainComplaint = Resources.Quotations.Manifestation,
                        Treatments = visitTreatments,
                        PreventiveMedicineItems = visitPreventive,
                        Examinations = visitExaminations,
                        Price = totalPrice,
                        DoctorId = CurrentUser.Id,
                        Close = false,
                        ClientIdAtTimeOfVisit = patient.ClientId,
                        Active = true
                    };
                visit.Treatments = UpadteVisitTreatments(visit.Treatments);
                status = RapidVetUnitOfWork.VisitRepository.AddNewVisit(visit);
            }

            if (status.Success)
            {
                var treatmentIdsForVisit = executionTreatments.Select(t => t.Id).ToList();

                foreach (var treatmentId in treatmentIdsForVisit)
                {
                    var quotationTreatment = quotation.Treatments.SingleOrDefault(t => t.Id == treatmentId);

                    if (quotationTreatment != null)
                    {
                        quotationTreatment.VisitId = visit.Id;
                    }
                }

                List<string> logContents = new List<string>();
                var statusUqt = RapidVetUnitOfWork.QuotationRepository.UpdateQuotationTreatments(quotation.Id, quotation.Treatments.ToList(), true, out logContents);
                if (statusUqt.Success)
                    foreach (var c in logContents)
                        RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, "תיק לקוח - מחיקת פריט", c, String.Empty);

                SetSuccessMessage();
                status.ItemId = visit.Id;
            }
            else
            {
                SetErrorMessage();
                status.ItemId = 0;
            }

            return status;
        }

        private List<QuotationTreatment> GetQuotationTreatments(string treatments)
        {
            var result = new List<QuotationTreatment>();

            if (!string.IsNullOrWhiteSpace(treatments))
            {
                var jArr = JArray.Parse(treatments);
                result = jArr.ToObject<List<QuotationTreatment>>();
            }

            return result;
        }

        private List<QuotationTreatmentModel> GetQuotationTreatmentModels(string treatments)
        {
            var result = new List<QuotationTreatmentModel>();

            if (!string.IsNullOrWhiteSpace(treatments))
            {
                var jArr = JArray.Parse(treatments);
                result = jArr.ToObject<List<QuotationTreatmentModel>>();
            }

            return result;
        }

        [HttpPost]
        public JsonResult DuplicateQuotation(int id, string name)
        {
            var sourceQuotation = RapidVetUnitOfWork.QuotationRepository.GetQuotation(id);
            var quotation = Mapper.Map<Quotation, Quotation>(sourceQuotation);
            quotation.Name = name;
            var status = RapidVetUnitOfWork.QuotationRepository.Create(quotation);
            if (status.Success)
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return Json(status.Success);
        }

        //------------private--------------------------------
        private ICollection<VisitTreatment> UpadteVisitTreatments(ICollection<VisitTreatment> treatments)
        {
            foreach (var treatment in treatments)
            {
                treatment.Discount = (treatment.Quantity * treatment.UnitPrice) - treatment.Price;
                treatment.CategoryName =
                    RapidVetUnitOfWork.CategoryRepository.GetNameByPriceListItemId(treatment.PriceListItemId);
            }
            return treatments;
        }

        [HttpPost]
        public string SendEmail(string quotationName, string patientName, string treatments, string email)
        {
            var mailClient = getMailClient();
            string from = string.Format("{0}<{1}>", !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.EmailBusinessName) ? CurrentUser.ActiveClinicGroup.EmailBusinessName : CurrentUser.ActiveClinic.Name,
                                                CurrentUser.ActiveClinicGroup.EmailAddress);

            RestRequest request = new RestRequest();
            request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_RAPID_DOMAIN"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", from);
            request.AddParameter("subject", String.Format("הצעת מחיר {0}", quotationName));
            request.AddParameter("html", patientName + Environment.NewLine + treatments);
            request.Method = Method.POST;

            request.AddParameter("to", email);
            var message = mailClient.Execute(request);
            if ((int)message.StatusCode == 200)
            {
                RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, 1, MessageType.Email);
                string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                        Resources.Auditing.SendingModule, Resources.Auditing.MailSent,
                                                        Resources.Auditing.ClientName, patientName,
                                                        Resources.Auditing.Address, email,
                                                        Resources.Auditing.MailSubject, String.Format("הצעת מחיר {0}", quotationName),
                                                        Resources.Auditing.ReturnCode, (int)message.StatusCode + " - " + message.StatusCode);

                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.MailSent, logMessage, "");
            }

            var status = string.Empty;
            if (string.IsNullOrWhiteSpace(status))
                SetSuccessMessage("הצעת המחיר נשלחו בהצלחה");
            else
                SetErrorMessage("אירעה שגיאה במשלוח הצעת המחיר");

            return null;
        }
    }
}
