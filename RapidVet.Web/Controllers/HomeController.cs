﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.WebModels.ClinicTasks;
using RapidVet.WebModels.FullCalender;
using RapidVet.WebModels.PreventiveMedicine;
using RapidVet.FilesAndStorage;
using System.Security;
using RapidVet.RapidConsts;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {           
            if(CurrentUser != null)
            {
                var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctorsForSchedule(CurrentUser.ActiveClinicId, CurrentUser.Id);
                doctors = doctors.FindAll(x => x.ShowOnMainWin);
                var tasks = RapidVetUnitOfWork.ClinicTaskRepository.GetClinicOpenTasks(CurrentUser.ActiveClinicId).OrderByDescending(t => t.StartDate).Take(10);

                var model = new WeeklyCalenderWebModel()
                {
                    Doctors = new List<SelectListItem>(),
                    Tasks = new List<ClinicTaskWebModel>()
                };

                foreach (var doctor in doctors)
                {
                    var r = RapidVetUnitOfWork.ClinicRepository.GetUsersClinicsRolesNO_DIARY(doctor.Id);
                    if (r != null)
                    {
                        continue;
                    }
                    //var user = RapidVetUnitOfWork.ClinicRepository.GetUserByUN(doctor.Username);
                    //var user = RapidVetUnitOfWork.ClinicRepository.GetUserByID(doctor.Id);
                    //var r = DataContext.UsersClinicsRoles.Select(u => u.RoleId).Where(ucr => ucr.User.Id == doctor.i);
                    //if (user!=null && user.IsUserInRole(RolesConsts.NO_DIARY))
                       // continue;

                    model.Doctors.Add(new SelectListItem() { Text = doctor.FirstName + " " + doctor.LastName, Value = doctor.Id.ToString() + (doctor.IsNoDiary ? "=true" : "") });
                }
                foreach (var task in tasks)
                {
                    model.Tasks.Add(new ClinicTaskWebModel()
                        {
                            Name = task.Name,
                            Description = task.Description,
                            StartDate = task.StartDate.HasValue ? task.StartDate.Value.ToString("dd/MM/yyyy") : ""
                        });
                }
                int maxTaskRows = CurrentUser.IsClinicGroupManager ? 7 : 11;
                model.RowsNum = maxTaskRows - model.Tasks.Count; // default 11 rows in table

                var generalMessage = RapidVetUnitOfWork.AdminRepository.GetGeneralSettings();
                if (generalMessage != null)
                {
                    model.GeneralText = generalMessage.GeneralText;
                }

                if (CurrentUser.IsAdministrator)
                {
                    ArchivesUploadHelper.DeleteClinicExportZipFiles(Server.MapPath("~/ClinicExportFiles"));
                }

                var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);

                ViewBag.Display = CurrentUser.IsAdministrator ? (int)UserHomePageDisplay.Admin : CurrentUser.HomePageDisplay;
                ViewBag.ExpireDays = clinicGroup.TrialPeriod ? Math.Round((clinicGroup.ExpirationDate.Value.AddHours(23).AddMinutes(59) - DateTime.Now).TotalDays).ToString() : "";


                if (CurrentUser.IsAdministrator)
                {
                    ViewBag.NumberOfClinics = RapidVetUnitOfWork.ClinicRepository.GetNumberOfClinics();
                    ViewBag.NumberOfActiveClinics = RapidVetUnitOfWork.UserRepository.GetNumberOfActiveClinics();
                    ViewBag.NumberOfUsers = RapidVetUnitOfWork.UserRepository.GetNumberOfUsers();
                    ViewBag.NumberOfActiveUsers = RapidVetUnitOfWork.UserRepository.GetNumberOfActiveUsers();
                }

                if(CurrentUser.IsClinicGroupManager)
                {
                    double size;
                    var clinic = RapidVetUnitOfWork.ClinicRepository.GetClinic(CurrentUser.ActiveClinicId);
                    ViewBag.StorageUsagePercent = GetArchiveUsagePercent(CurrentUser.ActiveClinicId, out size);
                    ViewBag.StorageUsageQuota = clinic.ArchiveAllocation;
                    ViewBag.StorageUsageGB = size;
                    ViewBag.MailsSentThisMonth = RapidVetUnitOfWork.MessageDistributionRepository.MailsSentThisMonth(CurrentUser.ActiveClinicId);
                }
                else
                {
                    ViewBag.StorageUsagePercent = 0;
                    ViewBag.StorageUsageQuota = 0;
                    ViewBag.StorageUsageGB = 0;
                    ViewBag.MailsSentThisMonth = 0; 
                }
                return View(model);
            }
            else
            {
               return RedirectToAction("LogOn", "Account");
            }
        }             

        public ActionResult RemindersOfToday()
        {
            var items = RapidVetUnitOfWork.PreventiveMedicineRepository.GetRemindersForHomePage(CurrentUser.ActiveClinicId).ToList();
            return View(items);
        }

        public JsonResult GetRemindersOfToday()
        {
            var items = RapidVetUnitOfWork.PreventiveMedicineRepository.GetRemindersForHomePage(CurrentUser.ActiveClinicId).ToList();
            return new JsonResult()
                {
                    Data = RenderPartialViewToString("RemindersOfToday", items),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

        }
        public ActionResult About()
        {
            return View();
        }

        public ActionResult Demo()
        {
            return View();
        }

        public ActionResult UploadTest()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadTest(HttpPostedFileBase file)
        {
            // Image image = new Bitmap(file.InputStream);
            RapidVet.FilesAndStorage.PatientUploadHelper.UploadPatientProfileImage(1, 1, 1, 300, file);
            return View();
        }

        public ActionResult ProfilImage()
        {
            DateTime? lastModified;
            var stream = RapidVet.FilesAndStorage.PatientUploadHelper.GetProfileImageStream(1, 1, 1, 300, out lastModified);
            return new FileStreamResult(stream, "image/png");
        }

        public ActionResult PopupTest()
        {
            return View();
        }

        public ActionResult Unauthorised()
        {
            return View("_Unauthorized");
        }
    }
}
