﻿using System;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using RapidVet.Repository;
using RapidVet.Unicell;
using RapidVet.WebModels;
using RapidVet.WebModels.Users;
using RapidVetMembership;
using RestSharp;

namespace RapidVet.Web.Controllers
{
    public class AccountController : BaseController
    {


        private IFormsAuthenticationService _formsAuthenticationService;

        public AccountController(IUserRepository userRepository, RapidVet.Repository.Clinics.ClinicGroupRepository clinicGroupRepository, IFormsAuthenticationService formsAuthenticationService)
        {
            _formsAuthenticationService = formsAuthenticationService;
        }



        //
        // GET: /Account/LogOn

        public ActionResult LogOn()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        //
        // POST: /Account/LogOn

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {

            if (ModelState.IsValid)
            {
                UserValidationStatus status;
                var concurrentUser = MembershipService.ValidateUser(model.UserName, model.Password, out status);
                if (concurrentUser != null) //connected
                {
                    if (!concurrentUser.User.IsAdministrator && !concurrentUser.User.IsClinicGroupManager &&
                        !concurrentUser.User.IsClinicManager && !concurrentUser.User.IsSecratery && !concurrentUser.User.IsDoctor)
                        status = UserValidationStatus.WithNoPermission;
                }

                switch (status)
                {
                    case UserValidationStatus.Valid:

                        _formsAuthenticationService.SetAuthCookie(concurrentUser.UserName.ToString(), model.RememberMe);
                        var user = RapidVetUnitOfWork.UserRepository.GetItem(concurrentUser.User.Id);
                        concurrentUser.User.LastLoginDate = DateTime.Now;
                        user.LastLoginDate = DateTime.Now;
                        RapidVetUnitOfWork.UserRepository.Save(user);
                        RapidVetUnitOfWork.AuditingRepository.Audit(user, Resources.Controllers.Account.UserLoggedIn, Request.UserHostAddress, "");
                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                            && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\") && !returnUrl.ToLower().Contains("logoff"))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    case UserValidationStatus.IncorrectPassword:
                        ModelState.AddModelError("", RapidVet.Resources.GlobalValidationErrors.InvalidPassword);
                        break;
                    case UserValidationStatus.Blocked:
                        ModelState.AddModelError("", RapidVet.Resources.GlobalValidationErrors.BlockUser);
                        break;
                    case UserValidationStatus.TooManyUsers:
                        ModelState.AddModelError("", RapidVet.Resources.GlobalValidationErrors.TooManyUsers + Environment.NewLine + Environment.NewLine + Resources.GlobalValidationErrors.ErrorMsgSecondLine);
                        break;
                    case UserValidationStatus.IncorrectUserName:
                        ModelState.AddModelError("", RapidVet.Resources.GlobalValidationErrors.InvalidPassword);
                        break;
                    case UserValidationStatus.WithNoPermission:
                        ModelState.AddModelError("", RapidVet.Resources.GlobalValidationErrors.UserWithNoPermission);
                        break;
                    case UserValidationStatus.TrialExpired:
                        ModelState.AddModelError("", RapidVet.Resources.GlobalValidationErrors.TrialExpired);
                        break;
                    default:
                        break;
                }

            }


            ConcurrentUserInfo.RemoveByUserName(model.UserName);

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/LogOff
        public ActionResult LogOff()
        {

            if (CurrentUser != null && !CurrentUser.IsAdministrator)
            {

                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Controllers.Account.UserLoggedOut, Request.UserHostAddress, "");
                ConcurrentUserInfo.RemoveByUserName(CurrentUser.Username);
            }

            FormsAuthentication.SignOut();

            return RedirectToAction("LogOn", "Account");
        }


        //
        // GET: /Account/ChangePassword

        public void ResetMyPasswordRequest(string username)
        {
            var user = RapidVetUnitOfWork.UserRepository.GetItemByUserName(username);
            var message = Resources.Controllers.Account.UserNotFound;

            if (user != null)
            {
                user.PasswordResetGuid = System.Guid.NewGuid();
                user.LastPasswordResetRequest = DateTime.Now;
                RapidVetUnitOfWork.AuditingRepository.Audit(user, Resources.Controllers.Account.ResetPasswordRequest, "", "");
                string hostUrl = HttpContext.Request.Url.ToString().Substring(0, HttpContext.Request.Url.ToString().IndexOf('/', 8));

                string templateText = System.IO.File.ReadAllText(Server.MapPath("~/Views/Account/ForgotPassTemplate.html"), Encoding.GetEncoding("windows-1255"));
                templateText = templateText.Replace("{passurl}", hostUrl + Url.Action("ResetPassword", "Account", new { UserId = user.Id, Guid = user.PasswordResetGuid }));

                var to = !string.IsNullOrWhiteSpace(user.Email) ? user.Email : user.ClinicGroup.EmailAddress;

                var mailClient = getMailClient();
                string from = "noreply@rapidvet.co.il";// string.Format("{0}<{1}>", "RapidVet", "NoReply@rapid-image.com");

                RestRequest request = new RestRequest();
                request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_RAPID_DOMAIN"], ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", from);
                request.AddParameter("subject", Resources.Controllers.Account.ResetPassword);
                request.AddParameter("html", templateText);
                request.Method = Method.POST;
                request.AddParameter("to", to);

                if (!string.IsNullOrWhiteSpace(to))
                {
                    var execute = mailClient.Execute(request);
                    if ((int)execute.StatusCode == 200)
                    {
                        RapidVetUnitOfWork.MessageDistributionRepository.Increment(RapidVetUnitOfWork.ClinicGroupRepository.GetFirstClinic(user.ClinicGroupId.Value).Id, 1, Model.Clinics.MessageType.Email);
                        message = string.Format(Resources.Controllers.Account.MailSentSuccess, to);
                        SetSuccessMessage(message);
                        
                        string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                 Resources.Auditing.SendingModule, Resources.Auditing.ResetPasswordMail,
                                                                 Resources.Auditing.ClientName, user.Name,
                                                                 Resources.Auditing.Address, to,
                                                                 Resources.Auditing.MailSubject, Resources.Controllers.Account.ResetPassword,
                                                                 Resources.Auditing.ReturnCode, (int)execute.StatusCode + " - " + execute.StatusCode);

                        RapidVetUnitOfWork.AuditingRepository.Audit(user, Resources.Auditing.MailSent, logMessage, "");

                    }
                    else
                    {
                        message = string.Format(Resources.Controllers.Account.MailSentFailure, to);
                        SetErrorMessage(message);
                    }
                }
                else
                {
                    message = Resources.Controllers.Account.NoMailDefined;
                }

                RapidVetUnitOfWork.Save();
            }
            else
            {
                SetErrorMessage(message);
            }



        }

        //
        // POST: /Account/ChangePassword



        public ActionResult ForgotPassword()
        {
            return PartialView("_ForgotPassword");
        }

        public ActionResult ResetPassword(int UserId, Guid Guid)
        {
            if (UserId > 0)
            {
                var user = RapidVetUnitOfWork.UserRepository.GetItem(UserId);
                if (user.PasswordResetGuid.Equals(Guid))
                {
                    var model = new ChangePasswordModel()
                    {
                        Id = user.Id,
                        UserName = user.Username
                    };

                    return View(model);
                }
            }

            SetErrorMessage(Resources.Controllers.Account.ResetLinkInvalid);
            return RedirectToAction("LogOn");
        }

        [HttpPost]
        public ActionResult ResetPassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                var user = RapidVetUnitOfWork.UserRepository.GetItem(model.Id.Value);
                RapidVetUnitOfWork.AuditingRepository.Audit(user, Resources.Controllers.Account.ResetPasswordCompleted, "", "");
                user.Password = model.NewPassword;
                user.PasswordResetGuid = new Guid();
                RapidVetUnitOfWork.Save();

                SetSuccessMessage();

                return RedirectToAction("LogOn");


            }
            return View(model);
        }


        [Authorize]
        public ActionResult ChangeActiveClinic(int id, string returnUrl)
        {
            if (CurrentUser != null)
            {
                //if(CurrentUser.IsAdministrator)
                //{
                //    Session["ActiveClinicId"] = id;
                //}

                CurrentUser.ActiveClinicId = id;
            }
            if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [Administrator]
        public ActionResult ChangeActiveClinicGroup(int id, string returnUrl)
        {
            if (CurrentUser != null)
            {
                if (CurrentUser.IsAdministrator)
                {
                    Session["ActiveClinicGroupId"] = id;
                }
                CurrentUser.ActiveClinicGroupId = id;
                CurrentUser.ActiveClinicGroup = null;
            }
            if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
