﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.Mvc;
using RapidVet.Model.Finance;
using RapidVet.Model.Visits;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Visits;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class VisitsController : BaseController
    {

        //id is visit id
        public JsonResult Visit(int id)
        {
            var visit = RapidVetUnitOfWork.VisitRepository.GetVisit(id);
            var model = new VisitModel();
            model = visit != null 
                ? AutoMapper.Mapper.Map<Visit, VisitModel>(visit) 
                : new VisitModel();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //is is clientId
        public JsonResult VisitSectionsData(int id)
        {
            var clientTariffId = RapidVetUnitOfWork.ClientRepository.GetClientTariffId(id);
            var diagnoses = Diagnoses();
            var treatments = Treatments(id, clientTariffId);
            var examinations = Examinations(id, clientTariffId);
            var medications = Medications();
            var model = new VisitSectionsJsonModel()
            {
                Diagnoses = diagnoses,
                Examinations = examinations,
                Medications = medications,
                Treatments = treatments
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        private ICollection<DiagnosisAdminModel> Diagnoses()
        {
            var diagnoses = RapidVetUnitOfWork.DiagnosisRepository.GetList(CurrentUser.ActiveClinicId);
            return diagnoses.Select(AutoMapper.Mapper.Map<Diagnosis, DiagnosisAdminModel>).ToList();
        }

        private ICollection<CategoryJsonModel> Treatments(int clientId, int clientTariffId)
        {
            var itemTypes = ItemTypeIds("itemTypeTreatmentsCodes");
            var treatments = RapidVetUnitOfWork.VisitRepository.GetVisitPriceListData(clientId, itemTypes, clientTariffId);
            return treatments.Select(AutoMapper.Mapper.Map<PriceListCategory, CategoryJsonModel>).ToList();

        }

        private ICollection<CategoryJsonModel> Examinations(int clientId, int clientTariffId)
        {
            var itemTypes = ItemTypeIds("itemTypeExaminationsCodes");
            var examinations =
                RapidVetUnitOfWork.VisitRepository.GetVisitPriceListData(clientId, itemTypes, clientTariffId);
            return examinations.Select(AutoMapper.Mapper.Map<PriceListCategory, CategoryJsonModel>).ToList();
        }

        private ICollection<MedicationJsonModel> Medications()
        {
            var medication = RapidVetUnitOfWork.MedicationRepository.GetClinicGroupMedications(CurrentUser.ActiveClinicGroupId);
            return medication.Select(AutoMapper.Mapper.Map<Medication, MedicationJsonModel>).ToList();
        }


        private int[] ItemTypeIds(string appSettingKey)
        {
            var typesStr = WebConfigurationManager.AppSettings[appSettingKey];
            var types = typesStr.Split(',');
            var itemTypes = new int[types.Length];
            for (int i = 0; i < types.Length; i++)
            {
                itemTypes[i] = int.Parse(types[i]);
            }
            return itemTypes;
        }

    }
}
