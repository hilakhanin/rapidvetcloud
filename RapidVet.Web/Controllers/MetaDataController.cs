﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clients;
using RapidVet.Model.Finance;
using RapidVet.Model.Patients;
using RapidVet.Model.Visits;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Medications;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class MetaDataController : BaseController
    {

        /// <summary>
        /// get all categories in clinic
        /// </summary>
        /// <param name="id"> clinic id</param>
        /// <returns>json</returns>
        public JsonResult GetClinicCategories(int id)
        {
            var categories = RapidVetUnitOfWork.CategoryRepository.GetClinicActiveCategories(id);
            var result = categories.Select(category => new SerializationHelperObject()
                {
                    Id = category.Id,
                    Value = category.Name
                }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// returns all animal kinds in clinic group
        /// </summary>
        /// <returns>json</returns>
        public JsonResult GetAnimalKinds()
        {
            var animalkInds = RapidVetUnitOfWork.AnimalKindRepository.GetList(CurrentUser.ActiveClinicGroupId);
            var model = animalkInds.Select(AutoMapper.Mapper.Map<AnimalKind, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// get all regional councils
        /// </summary>
        /// <returns>json</returns>
        public JsonResult GetRegionalCouncils()
        {
            var regionalCouncils = RapidVetUnitOfWork.MetaDataRepository.GetRegionalCouncils();
            var model = regionalCouncils.Select(AutoMapper.Mapper.Map<RegionalCouncil, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// get all medications which are dangerous drugs for current clinic group
        /// </summary>
        /// <returns>json</returns>
        public JsonResult GetClinicGroupDangerousDrugs()
        {
            var drugs = RapidVetUnitOfWork.MetaDataRepository.GetDangerousDrugMedications(CurrentUser.ActiveClinicGroupId);
            var model = drugs.Select(AutoMapper.Mapper.Map<Medication, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClientStatuses()
        {
            var statuses = RapidVetUnitOfWork.ClientStatusRepository.GetAll(CurrentUser.ActiveClinicGroupId);
            var model = statuses.Select(AutoMapper.Mapper.Map<ClientStatus, SelectListItem>);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDiagnoses()
        {
            var diagnoses = RapidVetUnitOfWork.DiagnosisRepository.GetList(CurrentUser.ActiveClinicId);
            return Json(diagnoses, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTreatments()
        {
            var treatments = RapidVetUnitOfWork.VisitRepository.GetAllPriceListItems(CurrentUser.ActiveClinicId);
            var model = treatments.Select(AutoMapper.Mapper.Map<PriceListItem, PriceListItemModel>);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTreatmentsByCategories(string list)
        {

            if (string.IsNullOrEmpty(list) || list == "0" || list == "")
            {
                return GetTreatments();
            }
            var listObj = list.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var treatments = RapidVetUnitOfWork.VisitRepository.GetAllPriceListItems(CurrentUser.ActiveClinicId);
            var treamentsResults = new List<PriceListItem>();
            foreach (var treatment in treatments)
            {
                var categoryId = treatment.CategoryId.ToString();
                if (listObj.Contains(categoryId))
                {
                    treamentsResults.Add(treatment);
                }
            }
            var model = treamentsResults.Select(AutoMapper.Mapper.Map<PriceListItem, PriceListItemModel>);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExaminations()
        {
            var examinations = RapidVetUnitOfWork.VisitRepository.GetAllExaminations(CurrentUser.ActiveClinicId);
            var model = examinations.Select(AutoMapper.Mapper.Map<PriceListItem, PriceListItemModel>);
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetMedications()
        {
            var medications = RapidVetUnitOfWork.VisitRepository.GetAllMedications(CurrentUser.ActiveClinicId);
            var model = medications.Select(AutoMapper.Mapper.Map<Medication, RapidVet.WebModels.Medications.MedicationJsonModel>);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTimeTypes()
        {
            var list = from FollowUpTimeRange f in Enum.GetValues(typeof(FollowUpTimeRange))
                       select new SelectListItem()
                       {
                           Text = RapidVet.Resources.Enums.FollowUpTimeRange.ResourceManager.GetString(f.ToString()),
                           Value = ((int)f).ToString()
                       };
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAnimalColors()
        {
            var clinicGroup = RapidVetUnitOfWork.AnimalColorRepository.GetList(CurrentUser.ActiveClinicGroupId);
            var animalColors =
                clinicGroup.AnimalColors.Select(ac => new SelectListItem() { Value = ac.Id.ToString(), Text = ac.Value });
            return Json(animalColors, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCities()
        {
            var cities = RapidVetUnitOfWork.ClientRepository.GetCities();
            var citiesSelectList = cities.Select(c => new SelectListItem() { Value = c.Id.ToString(), Text = c.Name });
            return Json(citiesSelectList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTariffs()
        {
            var tariffs = RapidVetUnitOfWork.TariffRepository.GetActiveTariffList(CurrentUser.ActiveClinicId);
            var tariffsSelectList = tariffs.Select(c => new SelectListItem() { Value = c.Id.ToString(), Text = c.Name });
            return Json(tariffsSelectList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPriceListItemTypes()
        {
            var priceListItemTypes = RapidVetUnitOfWork.MetaDataRepository.GetPriceListItemTypes().ToList();
            return Json(priceListItemTypes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClinicDoctors()
        {
            var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId);
            return Json(doctors, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClinicDoctorsSelectList()
        {
            var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId);
            var doctorsModel = doctors.Select(c => new SelectListItem() { Value = c.Id.ToString(), Text = c.Name });
            return Json(doctorsModel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClinicCityList()
        {
            var items = RapidVetUnitOfWork.ClientRepository.GetCitiesOfClinicById(CurrentUser.ActiveClinicId);

            return Json(items, JsonRequestBehavior.AllowGet);
        }
    }
}
