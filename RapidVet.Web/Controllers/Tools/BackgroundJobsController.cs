﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model.Expenses;
using RapidVet.Model.Finance;
using RapidVet.Model.CliinicInventory;
using RapidVet.RapidConsts;
using RapidVet.WebModels.Inventory;
using RapidVet.Model.Tools;
using RapidVet.WebModels.Tools;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class BackgroundJobsController : BaseController
    {

        public ActionResult Index(bool displayFinishedJobs = false)
        {           
            var jobs = RapidVetUnitOfWork.BackgroundJobsRepository.GetBackgroundJobsByClinicId(CurrentUser.ActiveClinicId, displayFinishedJobs).ToList();
            var model = AutoMapper.Mapper.Map<List<ClinicBackgroundJob>, List<ClinicBackgroundJobWebModel>>(jobs);
            ViewBag.DisplayFinishedJobs = displayFinishedJobs;
            return View(model);
        }

        public ActionResult CancelJob(string id)
        {
            RapidVetUnitOfWork.BackgroundJobsRepository.Cancel(id, CurrentUser.Id);
            return RedirectToAction("Index");
        }
    }
}
