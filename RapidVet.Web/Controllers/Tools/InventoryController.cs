﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model.Expenses;
using RapidVet.Model.Finance;
using RapidVet.Model.CliinicInventory;
using RapidVet.RapidConsts;
using RapidVet.WebModels.Inventory;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class InventoryController : BaseController
    {

        public ActionResult Index(bool displayUnmanagedItems = false)
        {
            if(CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }
            var items = RapidVetUnitOfWork.InventoryRepository.GetInventoryRelatedPriceListItems(CurrentUser.ActiveClinicId, displayUnmanagedItems).ToList();
            ViewBag.DisplayUnmanagedItems = displayUnmanagedItems;
            return View(items);
        }

        //id is priceListItemId
        public ActionResult Edit(int id)
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId, RolesConsts.INVENTORY_ENTITY_MANAGEMENT, CurrentUser.Id))
            {
                var item = RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(id);
                if (item.ItemsInUnit < 1)
                {
                    item.ItemsInUnit = 1;
                }

                if (item.DefultConsuptionAmount < 1)
                {
                    item.DefultConsuptionAmount = 1;
                }
                return View(item);
            }
            return View("_Unauthorized");
        }

        //id is priceListItemId
        public JsonResult GetSuppliers(int id)
        {
            var suppliers = RapidVetUnitOfWork.SupplierRepository.GetClinicSuppliers(CurrentUser.ActiveClinicId);
            var itemSuppliers = RapidVetUnitOfWork.InventoryRepository.GetPriceListItemSuppliers(id);
            var model = new List<PriceListItemSupplierWebModel>();

            foreach (var s in suppliers)
            {
                var webModelItem = new PriceListItemSupplierWebModel()
                    {
                        SupplierId = s.Id,
                        SupplierName = s.Name,
                        DefaultSupplier = false,
                        ConnectedSupplier = false,
                        CostPrice = 0
                    };

                var itemSupplier = itemSuppliers.SingleOrDefault(i => i.SupplierId == s.Id);

                if (itemSupplier != null)
                {
                    webModelItem =
                        AutoMapper.Mapper.Map<PriceListItemSupplier, PriceListItemSupplierWebModel>(itemSupplier);
                }

                model.Add(webModelItem);
            }


            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveItem(PriceListItem model, string suppliers)
        {
            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId, RolesConsts.INVENTORY_ENTITY_MANAGEMENT, CurrentUser.Id))
            {
                var priceListItem = RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(model.Id);
                priceListItem.Manufacturer = model.Manufacturer;
                priceListItem.Catalog = model.Catalog;
                priceListItem.RequireBarcode = model.RequireBarcode;
                priceListItem.ItemsInUnit = model.ItemsInUnit >= 1 ? model.ItemsInUnit : 1;
                priceListItem.DefultConsuptionAmount = model.DefultConsuptionAmount >= 2
                                                           ? model.DefultConsuptionAmount
                                                           : 1;
                priceListItem.InventoryActive = model.InventoryActive;
                priceListItem.Updated = DateTime.Now;
                var status = RapidVetUnitOfWork.Save();

                if (!string.IsNullOrWhiteSpace(suppliers))
                {
                    var itemSuppliers = JsonConvert.DeserializeObject<List<PriceListItemSupplierWebModel>>(suppliers);
                    var dbPriceListItemSuppliers =
                        itemSuppliers.Where(i => i.ConnectedSupplier)
                                     .Select(AutoMapper.Mapper.Map<PriceListItemSupplierWebModel, PriceListItemSupplier>)
                                     .ToList();
                    foreach (var item in dbPriceListItemSuppliers)
                    {
                        item.PriceListItemId = model.Id;
                    }
                    RapidVetUnitOfWork.InventoryRepository.UpdateItemSuppliers(model.Id, dbPriceListItemSuppliers);
                }

                return Json(status.Success);
            }
            throw new SecurityException();
        }


        [HttpGet]
        public ActionResult Settings()
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }


            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_ENTITY_MANAGEMENT,
                                                                             CurrentUser.Id))
            {
                return View();
            }
            return View("_Unauthorized");
        }

        [HttpPost]
        public JsonResult Settings(string data)
        {
            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_ENTITY_MANAGEMENT,
                                                                             CurrentUser.Id))
            {
                var categories =
                    RapidVetUnitOfWork.CategoryRepository.GetClinicActiveCategories(CurrentUser.ActiveClinicId);
                var viewData = JsonConvert.DeserializeObject<List<SelectListItem>>(data);
                foreach (var category in categories)
                {
                    var fromView = viewData.Single(v => v.Value == category.Id.ToString());
                    category.IsInventory = fromView.Selected;
                }

                var status = RapidVetUnitOfWork.Save();

                if (status.Success)
                {
                    SetSuccessMessage("הנתונים נשמרו בהצלחה");
                }
                else
                {
                    SetErrorMessage();
                }
                return Json(status.Success);
            }

            throw new SecurityException();
        }


        public JsonResult GetCategories()
        {
            var categories = RapidVetUnitOfWork.CategoryRepository.GetClinicActiveCategories(CurrentUser.ActiveClinicId);
            var model = categories.Select(AutoMapper.Mapper.Map<PriceListCategory, SelectListItem>).ToList();
            foreach (var category in categories)
            {
                var item = model.Single(m => m.Value == category.Id.ToString());
                item.Selected = category.IsInventory;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Orders()
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }


            var orders =
                RapidVetUnitOfWork.InventoryRepository.GetAllOrdersForClinic(CurrentUser.ActiveClinicId).ToList();
            return View(orders);
        }

        [HttpGet]
        public ActionResult CreateOrder()
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_CREATE_ORDER,
                                                                             CurrentUser.Id))
            {
                ViewBag.CurrentUserName = CurrentUser.Name;
                return View();
            }
            return View("_Unauthorized");
        }

        [HttpPost]
        public ActionResult CreateOrder(int supplierId, string comment, string items)
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_CREATE_ORDER,
                                                                             CurrentUser.Id))
            {
                if (!string.IsNullOrWhiteSpace(items))
                {
                    var orderItems = JsonConvert.DeserializeObject<List<InventoryPriceListItemWebModel>>(items);
                    var order = new InventoryOrder()
                        {
                            Created = DateTime.Now,
                            ClinicId = CurrentUser.ActiveClinicId,
                            Comments = comment,
                            SupplierId = supplierId,
                            UserId = CurrentUser.Id,
                            UserName = CurrentUser.Name,
                            Items =
                                orderItems.Select(
                                    AutoMapper.Mapper.Map<InventoryPriceListItemWebModel, InventoryPriceListItemOrder>)
                                          .ToList()
                        };

                    foreach (var item in order.Items)
                    {
                        item.UnitPrice = RapidVetUnitOfWork.InventoryRepository.GetPriceListItemSupplierPrice(item.Id,
                                                                                                              order
                                                                                                                  .SupplierId);
                    }

                    var status = RapidVetUnitOfWork.InventoryRepository.CreateOrder(order);
                    if (status.Success)
                    {
                        SetSuccessMessage();
                        return Json(true);
                    }
                }
                SetErrorMessage();
                return Json(false);
            }
            throw new SecurityException();
        }

        //id is order id
        public ActionResult ViewOrder(int id)
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_CREATE_ORDER,
                                                                             CurrentUser.Id))
            {
                 var order = RapidVetUnitOfWork.InventoryRepository.GetOrder(id);

                 if (CurrentUser.ActiveClinicId == order.ClinicId)
                 {                     
                     return View(order);
                 }
            }
            throw new SecurityException();
        }

        //id is order id
        public ActionResult PrintOrder(int id)
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_CREATE_ORDER,
                                                                             CurrentUser.Id))
            {
                var order = RapidVetUnitOfWork.InventoryRepository.GetOrder(id);

                if (CurrentUser.ActiveClinicId == order.ClinicId)
                {
                    ViewBag.TaxRate = RapidVetUnitOfWork.FinanceDocumentReposetory.GetTaxRateByMonth(CurrentUser.ActiveClinicId, order.Created);

                    return View(order);
                }
            }
            throw new SecurityException();
        }


        public JsonResult GetPriceListItemsBySupplier(int supplierId)
        {
            var items = RapidVetUnitOfWork.InventoryRepository.GetInventoryRelatedPriceListItems(CurrentUser.ActiveClinicId, false);
            var model = new List<InventoryPriceListItemWebModel>();
            foreach (var item in items)//.Where(i => i.InventoryActive))
            {
                var supplierPrice = item.Suppliers.SingleOrDefault(s => s.SupplierId == supplierId);
                model.Add(new InventoryPriceListItemWebModel()
                    {
                        Id = item.Id,
                        CategoryName = item.Category.Name,
                        Name = item.Name,
                        Price = supplierPrice != null ? supplierPrice.CostPrice : (decimal)0,
                        RequireBarcode = item.RequireBarcode
                    });
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClinicSuppliers()
        {
            var suppliers = RapidVetUnitOfWork.SupplierRepository.GetClinicSuppliers(CurrentUser.ActiveClinicId);
            var model = suppliers.Select(AutoMapper.Mapper.Map<Supplier, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //id is orderId
        public ActionResult ReciveOrder(int id)
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_RECIEVE_ORDER,
                                                                             CurrentUser.Id))
            {
                var order = RapidVetUnitOfWork.InventoryRepository.GetOrder(id);
                if (CurrentUser.ActiveClinicId == order.ClinicId)
                {
                    ViewBag.CurrentUserName = CurrentUser.Name;
                    return View(order);
                }
                throw new SecurityException();
            }
            return View("_Unauthorized");
        }

        public JsonResult GetOrderItems(int id)
        {
            var order = RapidVetUnitOfWork.InventoryRepository.GetOrder(id);

            if (CurrentUser.ActiveClinicId == order.ClinicId)
            {
                var items = order.Items.Where(i => !i.PriceListItem.RequireBarcode).ToList();

                var model =
                    items.Select(AutoMapper.Mapper.Map<InventoryPriceListItemOrder, InventoryOrderedItemWebModel>)
                         .ToList();

                var barcodeItems = order.Items.Where(i => i.PriceListItem.RequireBarcode).ToList();
                foreach (var bi in barcodeItems)
                {
                    for (int i = 0; i < bi.OrderedQuantity; i++)
                    {
                        model.Add(new InventoryOrderedItemWebModel()
                            {
                                id = bi.PriceListItemId,
                                name = bi.PriceListItem.Name,
                                category = bi.PriceListItem.Category.Name,
                                price = bi.UnitPrice,
                                quantity = 1,
                                orderItemId = bi.InventoryOrderId,
                                requireBarcode = bi.PriceListItem.RequireBarcode,
                                catalog = bi.PriceListItem.Catalog
                            });
                    }

                }

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            throw new SecurityException();
        }

        public JsonResult IsOrderRecieved(int id)
        {
            var order = RapidVetUnitOfWork.InventoryRepository.GetOrder(id);

            if (CurrentUser.ActiveClinicId == order.ClinicId)
            {
                return Json(order.Recived, JsonRequestBehavior.AllowGet);
            }
            throw new SecurityException();
        }

        //id is orderId
        [HttpPost]
        public ActionResult ReciveOrder(int id, string supplierInvoice, string supplierShipmentId, string recivingUserName, string items, string userName)
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_RECIEVE_ORDER,
                                                                             CurrentUser.Id))
            {
                var order = RapidVetUnitOfWork.InventoryRepository.GetOrder(id);
                if (order.ClinicId == CurrentUser.ActiveClinicId)
                {
                    var now = DateTime.Now;
                    order.SupplierInvoice = supplierInvoice;
                    order.SupplierShipmentId = supplierShipmentId;
                    order.Recived = true;
                    order.RecivedDate = now;
                    order.RecivingUserName = recivingUserName;
                    order.RecivingUserId = CurrentUser.Id;

                    var viewItems = JsonConvert.DeserializeObject<List<InventoryOrderedItemWebModel>>(items);

                    foreach (var orderItem in order.Items)
                    {
                        if (orderItem.PriceListItem.RequireBarcode)
                        {
                            var itemsFromView = viewItems.Where(v => v.orderItemId == orderItem.Id);
                            orderItem.RecievedQuantity = itemsFromView.Count();
                            orderItem.ActualUnitPrice = itemsFromView.Any()
                                                            ? itemsFromView.First().actualUnitPrice
                                                            : 0;

                        }
                        else
                        {
                            var itemFromView = viewItems.Single(v => v.orderItemId == orderItem.Id);
                            orderItem.ActualUnitPrice = itemFromView.actualUnitPrice;
                            orderItem.RecievedQuantity = itemFromView.recieved;
                        }
                    }

                    foreach (var item in viewItems.Where(v => v.orderItemId == 0))
                    {
                        var appendix = new InventoryPriceListItemOrder()
                            {
                                PriceListItemId = item.id,
                                OrderedQuantity = 0,
                                RecievedQuantity = item.recieved,
                                ActualUnitPrice = item.actualUnitPrice,
                                InventoryOrderId = order.Id,
                                UnitPrice = item.price
                            };
                        RapidVetUnitOfWork.InventoryRepository.AddOrderItem(appendix);
                        order.Items.Add(appendix);
                    }



                    foreach (var item in viewItems.Where(v => v.recieved > 0))
                    {
                        var inventoryItem = new Inventory()
                            {
                                ClinicId = CurrentUser.ActiveClinicId,
                                Created = now,
                                PriceListItemId = item.id,
                                InventoryOperationTypeId = (int)InventoryOperationType.Order,
                                OrderId = order.Id,
                                Quantity = item.recieved,
                                UnitPrice = item.actualUnitPrice,
                                Barcode = item.barcode,
                                UserId = CurrentUser.Id,
                                UserName = userName
                            };

                        RapidVetUnitOfWork.InventoryRepository.CreateInventoryItem(inventoryItem);
                    }

                    var status = RapidVetUnitOfWork.Save();

                    if (status.Success)
                    {
                        SetSuccessMessage();
                        return RedirectToAction("Index");
                    }
                    SetErrorMessage();
                    return RedirectToAction("ReciveOrder", "Inventory", new { id = id });
                }
                throw new SecurityException();
            }
            return View("_Unauthorized");
        }


        public ActionResult UpdateInventory()
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_UPDATE,
                                                                             CurrentUser.Id))
            {
                ViewBag.CurrentUserName = CurrentUser.Name;
                return View();
            }
            return View("_Unauthorized");
        }

        public JsonResult GetInventoryPriceListItems()
        {
            var items = RapidVetUnitOfWork.InventoryRepository.GetInventoryActivePriceListItems(CurrentUser.ActiveClinicId);
            var model = items.Select(AutoMapper.Mapper.Map<PriceListItem, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllInventoryRelatedPriceListItems()
        {
            var items =
                RapidVetUnitOfWork.InventoryRepository.GetAllInventoryRelatedPriceListItems(CurrentUser.ActiveClinicId)
                                  .ToList();

            var model = items.Select(i => new InventoryPriceListItemWebModel()
                {
                    CategoryName = i.Category.Name,
                    Name = i.Name,
                    Id = i.Id,
                    Price = 0,
                    Quantity = 1,
                    RequireBarcode = i.RequireBarcode
                }).ToList();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateInventory(int priceListItemId, int quantity, decimal unitPrice)
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_UPDATE,
                                                                             CurrentUser.Id))
            {
                var inventory = new Inventory()
                    {
                        ClinicId = CurrentUser.ActiveClinicId,
                        Created = DateTime.Now,
                        Quantity = quantity,
                        PriceListItemId = priceListItemId,
                        UnitPrice = unitPrice,
                        InventoryOperationTypeId = (int)InventoryOperationType.Manual,
                        UserName = CurrentUser.Username,
                        UserId = CurrentUser.Id
                    };
                RapidVetUnitOfWork.InventoryRepository.CreateInventoryItem(inventory);
                var status = RapidVetUnitOfWork.Save();

                if (status.Success)
                {
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("Index");
            }
            return View("_Unauthorized");
        }

        public ActionResult ValueReport()
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_REPORTS,
                                                                             CurrentUser.Id))
            {
                var items =
                    RapidVetUnitOfWork.InventoryRepository.GetAllInventoryRelatedPriceListItems(
                        CurrentUser.ActiveClinicId);

                var model = new List<InventoryValueReportModel>();
                foreach (var priceListItem in items)
                {
                    var inventoryItems = RapidVetUnitOfWork.InventoryRepository.GetInventoryItems(
                        CurrentUser.ActiveClinicId, priceListItem.Id);

                    var inventoryUpdates = inventoryItems.Where(i => i.OrderId == null && i.VisitId == null);
                    inventoryItems = inventoryItems.Where(i => i.OrderId != null || i.VisitId != null);

                    var sum = (decimal)0;
                    decimal quanity = 0;

                    var itemSuppliers =
                        RapidVetUnitOfWork.InventoryRepository.GetPriceListItemSuppliers(
                            priceListItem.Id);

                    if (itemSuppliers.Any())
                    {
                        decimal supplierPrice = 0;
                        var defaultSupplier = itemSuppliers.FirstOrDefault(s => s.DefaultSupplier);
                        if (defaultSupplier != null)
                        {
                            supplierPrice = defaultSupplier.CostPrice;
                        }
                        else
                        {
                            var anySupplier = itemSuppliers.FirstOrDefault();
                            if (anySupplier != null)
                            {
                                supplierPrice = anySupplier.CostPrice;
                            }
                        }

                        if (inventoryItems.Any())
                        {

                            foreach (var inventoryItem in inventoryItems)
                            {
                                quanity += inventoryItem.Quantity;
                                sum += inventoryItem.Quantity * inventoryItem.UnitPrice;
                            }
                        }


                        if (inventoryUpdates.Any())
                        {

                            var updatesQuantity = inventoryUpdates.Sum(u => u.Quantity);
                            decimal updatesSum = inventoryUpdates.Sum(u => u.Quantity * u.UnitPrice);
                            quanity += updatesQuantity;
                            sum += updatesSum;
                        }

                    }

                    var modelItem = new InventoryValueReportModel()
                        {
                            PriceListItemId = priceListItem.Id,
                            Category = priceListItem.Category.Name,
                            Name = priceListItem.Name,
                            Sum = sum,
                            Quantity = quanity
                        };
                    model.Add(modelItem);
                }
                return View(model);
            }
            return View("_Unauthorized");
        }

        public ActionResult StatusReport()
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_REPORTS,
                                                                             CurrentUser.Id))
            {
                return View();
            }
            return View("_Unauthorized");
        }

        public JsonResult GetStatusReportData(int? priceListItemId, string from, string to)
        {
            var fromDate = StringUtils.ParseStringToDateTime(from);
            var toDate = StringUtils.ParseStringToDateTime(to);
            var inventoryItems = RapidVetUnitOfWork.InventoryRepository.GetInventoryItems(
                CurrentUser.ActiveClinicId,
                priceListItemId, fromDate, toDate);
            var model = inventoryItems.Select(AutoMapper.Mapper.Map<Inventory, InventoryStatusReportModel>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //id is priceListItemId
        public JsonResult GetCurrentInventoryQuantity(int id)
        {
            var inventoryItems = RapidVetUnitOfWork.InventoryRepository.GetInventoryItems(CurrentUser.ActiveClinicId, id);
            decimal result = 0;
            if (inventoryItems.Any())
            {
                result = inventoryItems.Sum(i => i.Quantity);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //id is visit id
        public ActionResult UpdateAfterVisit(int id)
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            if (RapidVetUnitOfWork.InventoryRepository.IsUserInInventoryRole(CurrentUser.ActiveClinicGroupId,
                                                                             RolesConsts.INVENTORY_UPDATE,
                                                                             CurrentUser.Id))
            {
                return View();
            }
            return View("_Unauthorized");

        }

        //id is visit id
        public JsonResult GetVisitInventoryPriceListItems(int id)
        {

            var visit = RapidVetUnitOfWork.VisitRepository.GetVisit(id);
            if (IsPatientInCurrentClinic(visit.PatientId.Value))
            {
                var inventoryItems =
                    RapidVetUnitOfWork.InventoryRepository.GetAllInventoryRelatedPriceListItems(
                        CurrentUser.ActiveClinicId);
                var model = new List<InventoryPriceListItemWebModel>();
                foreach (var treatment in visit.Treatments)
                {
                    var inventoryItem = inventoryItems.SingleOrDefault(i => i.Id == treatment.PriceListItemId);
                    if (inventoryItem != null)
                    {
                        model.Add(new InventoryPriceListItemWebModel()
                            {
                                CategoryName = inventoryItem.Category.Name,
                                Name = inventoryItem.Name,
                                Id = inventoryItem.Id,
                                Quantity = treatment.Quantity
                            });
                    }
                }

                foreach (var treatment in visit.Examinations)
                {
                    var inventoryItem = inventoryItems.SingleOrDefault(i => i.Id == treatment.PriceListItemId);
                    if (inventoryItem != null)
                    {
                        model.Add(new InventoryPriceListItemWebModel()
                        {
                            CategoryName = inventoryItem.Category.Name,
                            Name = inventoryItem.Name,
                            Id = inventoryItem.Id,
                            Quantity = treatment.Quantity
                        });
                    }
                }

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public JsonResult UpdateAfterVisit(int visitId, string items)
        {
            if (!string.IsNullOrWhiteSpace(items))
            {
                var viewItems = JsonConvert.DeserializeObject<List<InventoryPriceListItemWebModel>>(items);
                var now = DateTime.Now;
                foreach (var vt in viewItems)
                {
                    var inventory = new Inventory()
                        {
                            ClinicId = CurrentUser.ActiveClinicId,
                            Created = now,
                            VisitId = visitId,
                            InventoryOperationTypeId = (int)InventoryOperationType.Visit,
                            PriceListItemId = vt.Id,
                            Quantity = vt.Quantity * -1,
                            UnitPrice = 0,
                            UserId = CurrentUser.Id,
                            UserName = CurrentUser.Name,
                        };
                    RapidVetUnitOfWork.InventoryRepository.CreateInventoryItem(inventory);
                }

                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    SetSuccessMessage("שינויי מלאי נשמרו בהצלחה");
                }
                else
                {
                    SetErrorMessage(" אירעה שגיאה בשמירה למלאי");
                }
                return Json(status.Success);
            }
            SetErrorMessage("אין פריטים לשמירה");
            return Json(false);
        }

        //id is orderid
        public ActionResult Cancel(int id)
        {
            if (CurrentUser.ActiveClinicGroup.StockModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            RapidVetUnitOfWork.InventoryRepository.RemoveOrder(id);
            var res = RapidVetUnitOfWork.Save();
            if (res.Success)
            {
                SetSuccessMessage("הזמנה בוטלה בהצלחה");
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("Orders");
        }
    }
}
