﻿using Newtonsoft.Json.Linq;
using RapidVet.RapidConsts;
using RapidVet.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using RapidVet.Model.Tools;
using RapidVet.Model.Users;

namespace RapidVet.Web.Controllers.Tools
{
    public class InterfacesController : Controller
    {
        private enum Status
        {
            SecurityError = 0, UserError, Success, BadRequest
        }

        [HttpPost]
        public string CreateDoctor(CreateDoctorRequest request)
        {
            Status status = Status.Success;
            int UserId = 0;

            if (string.IsNullOrWhiteSpace(request.ApiKey) || string.IsNullOrWhiteSpace(request.InterfaceName) 
                || string.IsNullOrWhiteSpace(request.LastName) || string.IsNullOrWhiteSpace(request.Password) || string.IsNullOrWhiteSpace(request.UserName) || request.ClinicGroupId == 0 || request.ClinicId == 0)
            {
                status = Status.BadRequest;
            }
            else
            {
                var interfaceRepo = new InterfacesRepository();

                var myInterface = interfaceRepo.GetInterfaceByName(request.InterfaceName);
                if (myInterface.ApiKey.Equals(new Guid(request.ApiKey)))
                {
                    using (var repo = new UserRepository())
                    {
                        var user = repo.Create(request.FirstName ?? "", request.LastName, request.UserName, request.Password, request.ClinicGroupId, null, request.IsActive);
                        var roles = repo.GetRoles();
                        var doctorRole = roles.Single(r => r.RoleName.ToLower().Equals("doctor"));
                        var userClinicRoles = new List<UsersClinicsRoles>();

                        if (user.ClinicGroupId == request.ClinicGroupId)
                        {
                            //Roles.AddUserToRole(request.UserName, RolesConsts.DOCTOR);
                            userClinicRoles.Add(new UsersClinicsRoles()
                            {
                                ClinicId = request.ClinicId,
                                Role = doctorRole,
                                RoleId = doctorRole.RoleId,
                                UserId = user.Id
                            });

                            repo.UpdateUserClinicsRoles(userClinicRoles, request.IsActive);
                            UserId = user.Id;


                        }
                        else
                        {
                            status = Status.UserError;
                        }
                    }
                }
                else
                {
                    status = Status.SecurityError;
                }

                if (status == Status.Success)
                {
                    return string.Format("{0}: {1}", Status.Success, UserId.ToString());
                }
            }

            return status.ToString();
        }
    }
}

