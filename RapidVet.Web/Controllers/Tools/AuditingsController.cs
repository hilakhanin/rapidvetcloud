﻿using RapidVet.Model.Auditings;
using RapidVet.Model.Clinics;
using RapidVet.WebModels.Auditing;
using RapidVet.WebModels.Clinics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RapidVet.Web.Controllers.Tools
{
    public class AuditingsController : BaseController
    {
        //
        // GET: /Auditings/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetLogs(string fromDate, string toDate, string userId, string logType, string clinicGroup, string clinic)
        {
            var now = DateTime.Now;
            DateTime fromDateTime;
            DateTime toDateTime;
            int userIdint;
            int logTypeint;
            int clinicint;
            int clinicGroupint;
            if (!DateTime.TryParse(fromDate, out fromDateTime))
                fromDateTime = new DateTime(now.Year, now.Month, 1);

            if (!DateTime.TryParse(toDate, out toDateTime))
                toDateTime = new DateTime(now.Year, now.Month, now.Day);

            if (!int.TryParse(userId, out userIdint))
                userIdint = -1;

            if (!int.TryParse(logType, out logTypeint))
                logTypeint = -1;

            if (!int.TryParse(clinicGroup, out clinicGroupint))
                clinicGroupint = -1;

            if (!int.TryParse(clinic, out clinicint))
                clinicint = -1;

            int clientID = CurrentUser.IsAdministrator ? clinicint : CurrentUser.ActiveClinicId;
            var logs = RapidVetUnitOfWork.AuditingRepository.GetAuditings(clientID, fromDateTime, toDateTime, userIdint, logTypeint, clinicGroupint);
            var model = AutoMapper.Mapper.Map<List<Auditing>, List<AuditingWebModel>>(logs);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetMetaData(string clinicGroup)
        {
            var logTypes = RapidVetUnitOfWork.AuditingRepository.GetAuditingsTypes();
            var clinicGroups = CurrentUser.IsAdministrator ? RapidVetUnitOfWork.ClinicGroupRepository.GetAllNotDepended() : null;
            return new JsonResult()
            {
                Data = new { LogTypes = logTypes, ClinicGroups = clinicGroups },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GetMetaDataByClinicGroupSelection(string clinicGroup)
        {
            int clinicGroupID;
            if (!int.TryParse(clinicGroup, out clinicGroupID))
                clinicGroupID = -1;

            //var activeClinicId = CurrentUser.ActiveClinicId;
            var activeClinicGroupId = CurrentUser.ActiveClinicGroupId;
            //var activeUserId = CurrentUser.Id;
            var users = RapidVetUnitOfWork.UserRepository.GetListByClinicGroup(clinicGroupID == -1 ? activeClinicGroupId : clinicGroupID);
            var clinics = CurrentUser.IsAdministrator ? RapidVetUnitOfWork.ClinicRepository.GetClearList(clinicGroupID) : null;
            return new JsonResult()
            {
                Data = new { Users = users, Clinics = clinics },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        //
        // GET: /Auditings/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Auditings/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Auditings/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Auditings/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Auditings/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Auditings/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Auditings/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
