﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model.Clients;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Expenses;
using RapidVet.Model.Finance;
using RapidVet.Model.CliinicInventory;
using RapidVet.Model.Patients;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Visits;
using RapidVet.RapidConsts;
using RapidVet.Repository.Clinics;
using RapidVet.WebModels.Inventory;

namespace RapidVet.Web.Controllers
{
    public class ImportRapidFileController : BaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            try
            {
                if (file != null && file.ContentLength > 0)
                {
                    var sr = new StreamReader(file.InputStream, Encoding.GetEncoding(1255));
                    string line;

                    //Add rabies preventive medicine item
                    var rabiesPriceListItems =
                        RapidVetUnitOfWork.PreventiveMedicineRepository.GetRabisVaccinePriceListItems(
                            CurrentUser.ActiveClinicId);
                    var rabiesPriceListItem = rabiesPriceListItems.FirstOrDefault();
                    if (rabiesPriceListItem == null)
                    {
                        return View("NoRabiesPriceListItem");
                    }

                    var lineNum = 0;
                    var faultyLines = new List<int>();
                    while ((line = sr.ReadLine()) != null)
                    {
                        lineNum++;
                        var item = line.Split('|');

                        //check electronic number exists
                        var patientDb = string.IsNullOrEmpty(item[15])
                                            ? null
                                            : RapidVetUnitOfWork.PatientRepository.GetPatientByElectronicNumber(item[15]);
                        //Doc Check
                        var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId);
                        var vet = doctors.FirstOrDefault(v => v.Name == item[0]);

                        //Patient not exists, Add Data
                        if (patientDb == null || !IsPatientInCurrentClinic(patientDb.Id))
                        {
                            //AnimalKind & AnimalRace
                            var animalKind =
                                RapidVetUnitOfWork.AnimalKindRepository.GetItemByNameWithRaces(
                                    CurrentUser.ActiveClinicGroupId, item[11]);
                            if (animalKind == null)
                            {
                                animalKind = new AnimalKind()
                                    {
                                        Active = true,
                                        ClinicGroupId = CurrentUser.ActiveClinicGroupId,
                                        Value = item[11],
                                        AnimalRaces = new Collection<AnimalRace>()
                                            {
                                                new AnimalRace()
                                                    {
                                                        Active = true,
                                                        Value = item[12],
                                                    }
                                            }
                                    };
                                RapidVetUnitOfWork.AnimalKindRepository.Create(animalKind);
                            }
                            else
                            {
                                item[12] = String.IsNullOrEmpty(item[12]) ? RepositoryConsts.ANIMAL_RACE_NAME_UNKNOWN : item[12];
                                if (animalKind.AnimalRaces.All(ar => ar.Value != item[12]))
                                {
                                    var newAnimalRace = new AnimalRace()
                                        {
                                            Active = true,
                                            Value = item[12],
                                            AnimalKindId = animalKind.Id,
                                        };
                                    RapidVetUnitOfWork.AnimalRaceRepository.Create(newAnimalRace);
                                }
                            }
                            //AnimalColor
                            var animalColor =
                                RapidVetUnitOfWork.AnimalColorRepository.GetItemByName(CurrentUser.ActiveClinicGroupId,
                                                                                       item[13]);
                            if (animalColor == null)
                            {
                                animalColor = new AnimalColor()
                                    {
                                        Active = true,
                                        ClinicGroupId = CurrentUser.ActiveClinicGroupId,
                                        Value = item[13],
                                    };
                                RapidVetUnitOfWork.AnimalColorRepository.Create(animalColor);
                            }
                            //Address
                            var cities = RapidVetUnitOfWork.ClientRepository.GetCities();
                            
                            var city = cities.FirstOrDefault(c => c.Name == item[6]);
                            if (city == null && item[6]== null)
                            {
                                var Cities = new Collection<City>();
                             
                                Cities.Add(new City()
                                {
                                   Name = item[6],
                                });
                                
                                
                            }
                            city = cities.FirstOrDefault(c => c.Name == item[6]);
                            var addresses = new Collection<Address>();
                            if (city != null)
                            {
                                addresses.Add(new Address()
                                    {
                                        CityId = city.Id,
                                        Street = item[5],
                                        ZipCode = item[7]
                                    });
                                
                            }
                            //Phones
                            var phoneTypes = RapidVetUnitOfWork.ClientRepository.GetPhoneTypes();
                            var phones = new Collection<Phone>();
                            if (phoneTypes.FirstOrDefault(p => p.Name == "בית") != null &&
                                phoneTypes.FirstOrDefault(p => p.Name == "נייד") != null)
                            {
                                if (!string.IsNullOrEmpty(item[8]))
                                {
                                    phones.Add(new Phone()
                                        {
                                            PhoneNumber = item[8],
                                            PhoneTypeId = phoneTypes.First(p => p.Name == "בית").Id
                                        });
                                }
                                if (!string.IsNullOrEmpty(item[9]))
                                {
                                    phones.Add(new Phone()
                                        {
                                            PhoneNumber = item[9],
                                            PhoneTypeId = phoneTypes.First(p => p.Name == "נייד").Id
                                        });
                                }
                            }
                            //Patient
                            patientDb = new Patient()
                                {
                                    Name = item[10],
                                    Active = true,
                                    LicenseNumber = !string.IsNullOrEmpty(item[14]) ? item[14] : null,
                                    ElectronicNumber = !string.IsNullOrEmpty(item[15]) ? item[15] : null,
                                    Sterilization = item[16] == "1",
                                    GenderId = item[17] == "1" ? (int)GenderEnum.Female : (int)GenderEnum.Male, 
                                    AnimalColorId = animalColor.Id,
                                    AnimalRaceId = animalKind.AnimalRaces.First(ar => ar.Value == item[12]).Id,
                                    BirthDate = ParseDate(item[18]),
                                    LocalId = ClinicLocalIdManager.GetNextLocalPatientId(CurrentUser.ActiveClinicId)
                                };
                            //Client
                            var client = new Client()
                                {
                                    FirstName = item[4],
                                    LastName = item[3],
                                    //Active = true,
                                    IdCard = item[2],
                                    ExternalVet = vet == null ? item[0] : "",
                                    CreatedDate = DateTime.Now,
                                    Addresses = addresses,
                                    Phones = phones,
                                    Patients = new Collection<Patient>() { patientDb },
                                    VetId = vet != null ? (int?)vet.Id : null,
                                    ClinicId = CurrentUser.ActiveClinicId,
                                    LocalId = ClinicLocalIdManager.GetNextLocalClientId(CurrentUser.ActiveClinicId),
                                    CreatedById = CurrentUser.Id,
                                };
                    
                            RapidVetUnitOfWork.ClientRepository.Create(client);
                            
                            RapidVetUnitOfWork.ClientRepository.SetClientNameWithPatients(client.Id, true);
                        }
                   
                       var preventiveMedicineItem = new PreventiveMedicineItem()
                            {
                                PriceListItemTypeId = rabiesPriceListItem.ItemTypeId,
                                PriceListItemId = rabiesPriceListItem.Id,
                                PatientId = patientDb.Id,
                                Preformed = ParseDate(item[19]),
                                PreformingUserId = vet != null ? (int?)vet.Id : null,
                                ExternalyPreformed = true,
                                Scheduled = ParseDate(item[21]),
                                Price = 0,
                                Discount = 0,
                                Quantity = 1,
                            };

                        var visit = new Visit()
                            {
                                PatientId = patientDb.Id,
                                CreatedDate = DateTime.Now,
                                UpdatedById = UserId,
                                VisitDate = DateTime.Now,
                                Close = true,
                                Price = 0,
                                DoctorId = vet != null ? vet.Id : CurrentUser.Id,
                                PreventiveMedicineItems = { preventiveMedicineItem },
                                ClientIdAtTimeOfVisit = patientDb.ClientId,
                                Active = true
                            };
                        try
                        {
                            RapidVetUnitOfWork.VisitRepository.AddNewVisit(visit);
                        }
                        catch (System.InvalidOperationException e)
                        {
                            faultyLines.Add(lineNum);
                        }
                    }
                    if (faultyLines.Count > 0)
                    {
                        return View("LinesNotUploaded",faultyLines);
                    }
                    return RedirectToAction("Success");
                }
                return View("NoFileUploaded");
            }
            catch (System.IndexOutOfRangeException e)
            {
                SetErrorMessage("קובץ בפורמט לא תקין");
                return View();
            }
        }

        private DateTime? ParseDate(string dateString)
        {
            DateTime date;
            const string dateFormat = "dd/MM/yyyy"; 
            var unparsedDate = dateString;
            DateTime.TryParseExact(unparsedDate, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
            return (date == DateTime.MinValue) ? null : (DateTime?)date;
        }

        public ActionResult Success()
        {
            SetSuccessMessage();
            return View();
        }
    }
}
