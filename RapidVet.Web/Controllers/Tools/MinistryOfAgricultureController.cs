﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CsvHelper;
using CsvHelper.Configuration;
using Newtonsoft.Json;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.WebModels.MinistryOfAgricultureReport;

namespace RapidVet.Web.Controllers
{
    public class MinistryOfAgricultureController : BaseController
    {
        /*---------------------------------report creation ------------------------------------------*/
        [HttpPost]
        public ActionResult CreateChipImplantReport(ChipImplant data, int patientId, String comments)
        {
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(patientId);
            var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(patient.Client.Clinic.RegionalCouncilId);

            if (data.Date == null)
            {
                data.Date = string.Empty;
            }

            var reportString = MinistryOfAgricultureReportHelpers.ChipImplantReport(data, patient, patient.Client,
                                                                              patient.Client.Clinic, regionalCouncil.Name);

            var report = new MinistryOfAgricultureReport()
                {
                    Comments = comments,
                    DateTime = DateTime.Now,
                    PatientId = patientId,
                    ReportType = MinistryOfAgricultureReportType.ChipImplant,
                    Report = reportString
                };

            return SaveReport(report);
        }

        [HttpPost]
        public ActionResult CreateRabiesVaccineReport(RabiesVaccine data, int patientId, string comments)
        {
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(patientId);
            var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(patient.Client.Clinic.RegionalCouncilId);

            if (data.Date == null)
            {
                data.Date = string.Empty;
            }

            var reportString = MinistryOfAgricultureReportHelpers.RabiesVaccineReport(data, patient, patient.Client,
                                                                              patient.Client.Clinic, regionalCouncil.Name);
            var report = new MinistryOfAgricultureReport()
            {
                Comments = comments,
                DateTime = DateTime.Now,
                PatientId = patientId,
                ReportType = MinistryOfAgricultureReportType.RabiesVaccination,
                Report = reportString
            };

            return SaveReport(report);
        }

        [HttpPost]
        public ActionResult CreateAttackReport(Attack data,
                                               int patientId, String comments)
        {
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(patientId);
            var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(patient.Client.Clinic.RegionalCouncilId);

            if (data.Date == null)
            {
                data.Date = string.Empty;
            }

            if (data.CancellationDate == null)
            {
                data.CancellationDate = string.Empty;
            }
            var reportString = MinistryOfAgricultureReportHelpers.AttackReport(data, patient, patient.Client,
                                                                              patient.Client.Clinic, regionalCouncil.Name);

            var report = new MinistryOfAgricultureReport()
            {
                Comments = comments,
                DateTime = DateTime.Now,
                PatientId = patientId,
                ReportType = MinistryOfAgricultureReportType.Attack,
                Report = reportString
            };

            return SaveReport(report);
        }

        [HttpPost]
        public ActionResult CreateQuarantineAdmissionReport(QuarantineAdmission data,
                                                            int patientId, String comments)
        {

            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(patientId);
            var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(patient.Client.Clinic.RegionalCouncilId);

            if (data.Date == null)
            {
                data.Date = string.Empty;
            }

            var reportString = MinistryOfAgricultureReportHelpers.QuarantineAdmissionReport(data, patient, patient.Client,
                                                                              patient.Client.Clinic,regionalCouncil.Name);

            var report = new MinistryOfAgricultureReport()
            {
                Comments = comments,
                DateTime = DateTime.Now,
                PatientId = patientId,
                ReportType = MinistryOfAgricultureReportType.QuarantineAdmission,
                Report = reportString
            };

            return SaveReport(report);

        }

        [HttpPost]
        public ActionResult CreateDogLicenseReport(DogLicense data,
                                                          int patientId, String comments)
        {

            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(patientId);
            var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(patient.Client.Clinic.RegionalCouncilId);

            if (data.Date == null)
            {
                data.Date = string.Empty;
            }


            var reportString = MinistryOfAgricultureReportHelpers.DogLicenseReport(data, patient, patient.Client, patient.Client.Clinic,regionalCouncil.Name);

            var report = new MinistryOfAgricultureReport()
            {
                Comments = comments,
                DateTime = DateTime.Now,
                PatientId = patientId,
                ReportType = MinistryOfAgricultureReportType.DogLicenseGranting,
                Report = reportString
            };

            return SaveReport(report);

        }

        [HttpPost]
        public ActionResult CreateQuarantineDischargeReport(QuarantineDischarge data,
                                                         int patientId, String comments)
        {

            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(patientId);
            var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(patient.Client.Clinic.RegionalCouncilId);

            if (data.Date == null)
            {
                data.Date = string.Empty;
            }


            var reportString = MinistryOfAgricultureReportHelpers.QuarantineDischargeReport(data, patient, patient.Client,
                                                                              patient.Client.Clinic,regionalCouncil.Name);

            var report = new MinistryOfAgricultureReport()
            {
                Comments = comments,
                DateTime = DateTime.Now,
                PatientId = patientId,
                ReportType = MinistryOfAgricultureReportType.QuarantineDischarge,
                Report = reportString
            };

            return SaveReport(report);

        }

        [HttpPost]
        public ActionResult CreateRevokingLicenseReport(RevokingLicense data,
                                                       int patientId, String comments)
        {

            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(patientId);
            var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(patient.Client.Clinic.RegionalCouncilId);

            if (data.Date == null)
            {
                data.Date = string.Empty;
            }

            var reportString = MinistryOfAgricultureReportHelpers.RevokingLicenseReport(data, patient, patient.Client,
                                                                              patient.Client.Clinic,regionalCouncil.Name);

            var report = new MinistryOfAgricultureReport()
            {
                Comments = comments,
                DateTime = DateTime.Now,
                PatientId = patientId,
                ReportType = MinistryOfAgricultureReportType.RevokingLicense,
                Report = reportString
            };

            return SaveReport(report);

        }

        [HttpPost]
        public ActionResult CreateDangerousDogReport(DangerousDog data,
                                                     int patientId, String comments)
        {
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(patientId);
            var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(patient.Client.Clinic.RegionalCouncilId);

            if (data.Date == null)
            {
                data.Date = string.Empty;
            }

            if (data.InsuranceFinishDate == null)
            {
                data.InsuranceFinishDate = string.Empty;
            }

            var reportString = MinistryOfAgricultureReportHelpers.DangerousDogReport(data, patient, patient.Client,
                                                                              patient.Client.Clinic,regionalCouncil.Name);


            var report = new MinistryOfAgricultureReport()
            {
                Comments = comments,
                DateTime = DateTime.Now,
                PatientId = patientId,
                ReportType = MinistryOfAgricultureReportType.DangerousDog,
                Report = reportString
            };

            return SaveReport(report);

        }

        public ActionResult CreateComplaintReport(ComplaintReport data,
                                                     int patientId, String comments)
        {

            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(patientId);
            var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(patient.Client.Clinic.RegionalCouncilId);

            if (data.Date == null)
            {
                data.Date = string.Empty;
            }


            var reportString = MinistryOfAgricultureReportHelpers.ComplaintReport(data, patient, patient.Client,
                                                                              patient.Client.Clinic,regionalCouncil.Name);

            var report = new MinistryOfAgricultureReport()
            {
                Comments = comments,
                DateTime = DateTime.Now,
                PatientId = patientId,
                ReportType = MinistryOfAgricultureReportType.ComplaintReport,
                Report = reportString
            };

            return SaveReport(report);

        }

        public ActionResult CreateDogFindingReport(DogFinding data,
                                                    int patientId, String comments)
        {

            var patient = RapidVetUnitOfWork.PatientRepository.GetPatientForLetter(patientId);
            var regionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(patient.Client.Clinic.RegionalCouncilId);

            if (data.Date == null)
            {
                data.Date = string.Empty;
            }


            var reportString = MinistryOfAgricultureReportHelpers.DogFindingReport(data, patient, patient.Client,
                                                                              patient.Client.Clinic,regionalCouncil.Name);

            var report = new MinistryOfAgricultureReport()
            {
                Comments = comments,
                DateTime = DateTime.Now,
                PatientId = patientId,
                ReportType = MinistryOfAgricultureReportType.FindingADog,
                Report = reportString
            };

            return SaveReport(report);

        }


        public ActionResult SaveReport(MinistryOfAgricultureReport report)
        {
            var status = RapidVetUnitOfWork.PatientLetterRepository.CreateMinistryOfAgricultureReport(report);
            if (status.Success)
            {
                SetSuccessMessage("הנתונים נשמרו בהצלחה");
            }
            else
            {
                SetErrorMessage();
            }
            return RedirectToAction("Index", "Letters", new { id = report.PatientId });
        }

        /*--------------------------------report locate, print, export-------------------------------*/
        //
        // GET: /MinistryOfAgriculture/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetReportTypeOptions()
        {
            var rm = Resources.Enums.MinistryOfAgricultureReportType.ResourceManager;
            var model = new List<SelectListItem>();
            foreach (var report in Enum.GetValues(typeof(MinistryOfAgricultureReportType)))
            {
                model.Add(new SelectListItem()
                    {
                        Selected = false,
                        Text = rm.GetString(report.ToString()),
                        Value = ((int)report).ToString()
                    });
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetReports(string from, string to, int typeId)
        {
            var data = GetData(from, to, typeId);
            var model = data.Select(Map);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReports(String from, String to, int typeId)
        {
            var data = GetData(from, to, typeId);
            var model = data.Select(Map).ToList();
            return View(model);
        }

        private FileResult GetReportsFile(List<String> data)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.Default);

            var index = 0;
            foreach (var item in data)
            {
                //insert index to line starting from 30 slot
                index++;
                var sb = new StringBuilder(item);
                sb.Remove(30, 5);
                sb.Insert(30, OpenFormatUtils.GetAlphaString(index.ToString(), 5));
                //write line
                writer.WriteLine(sb.ToString());
            }

            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/ascii", "MinistryOfAgricultureReport.txt");
        }

        public ActionResult ExportToFile(string from, string to, int typeId)
        {
            var data = GetData(from, to, typeId);
            var reportStrings = data.Select(r => r.Report).ToList();
            return GetReportsFile(reportStrings);
        }

        private IEnumerable<MinistryOfAgricultureReport> GetData(string from, string to, int typeId)
        {
            //DateTime fromDate, toDate;

            //  fromDate = String.IsNullOrEmpty(@from) ? DateTime.MinValue : DateTime.ParseExact(@from, "dd/MM/yyyy", null);

            //toDate = String.IsNullOrEmpty(to) ? DateTime.MaxValue : DateTime.ParseExact(to, "dd/MM/yyyy", null);
            var fromDate = StringUtils.ParseStringToDateTime(from);
            var toDate = StringUtils.ParseStringToDateTime(to);

            if (toDate == DateTime.MinValue)
            {
                toDate = DateTime.MaxValue;
            }
            else
            {
                var date = DateTime.ParseExact(to, "dd/MM/yyyy", null);
                date = new DateTime(date.Year, date.Month, date.Day);
                toDate = date.AddDays(1).AddTicks(-1);
            }

            var data =
                RapidVetUnitOfWork.MinistryOfAgricultureReportsRepository.GetReportsForClinic(
                    CurrentUser.ActiveClinicId, fromDate, toDate, typeId);
            return data;
        }

        private MinistryOfAgricultureReportItem Map(MinistryOfAgricultureReport r)
        {
            var rm = Resources.Enums.MinistryOfAgricultureReportType.ResourceManager;
            return new MinistryOfAgricultureReportItem()
                {
                    Id = r.Id,
                    DateTime = r.DateTime.ToString("dd/MM/yyyy"),
                    Comments = r.Comments,
                    ClientName = r.Patient.Client.Name,
                    PatientName = r.Patient.Name,
                    ReportType = rm.GetString(r.ReportType.ToString())
                };
        }

    }
}
