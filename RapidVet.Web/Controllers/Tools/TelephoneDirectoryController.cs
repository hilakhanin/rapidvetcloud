﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.WebModels.Clinics;
using RapidVet.Helpers;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class TelephoneDirectoryController : BaseController
    {
        //
        // GET: /TelephoneDirectory/


        // Table of all telephone directories
        public ActionResult Index()
        {  
            return View();
        }

        public ActionResult Print(string records)
        {
            var itemsList = StringUtils.GetIntList(records);
            
            var model = RapidVetUnitOfWork.TelephoneDirectoryRepository.GetList(CurrentUser.ActiveClinicId);

            model = model.Where(r => itemsList.Contains(r.Id));

            return View(model);
        }

        public JsonResult GetAllRecords()
        {
            var records = RapidVetUnitOfWork.TelephoneDirectoryRepository.GetList(CurrentUser.ActiveClinicId);
            var directoryTypes = RapidVetUnitOfWork.TelephoneDirectoryRepository.GetDirectoryTypes(CurrentUser.ActiveClinicId).ToList();
            var model = new { records, directoryTypes };
           
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        // Create/Edit telephone directory
        public ActionResult Edit(int id)
        {
            var telephoneDirectoryTypes = RapidVetUnitOfWork.TelephoneDirectoryRepository.GetDropdownList(CurrentUser.ActiveClinicId);
            //Create
            if (id == 0)
            {
                return View(new TelephoneDirectoryEdit() { ClinicId = CurrentUser.ActiveClinicId, TelephoneDirectoryTypes = telephoneDirectoryTypes });
            }
            //Edit
            else
            {
                var item = RapidVetUnitOfWork.TelephoneDirectoryRepository.GetItem(id);
                if (item == null)
                {
                    return View("Error");
                }
                var model = AutoMapper.Mapper.Map<TelephoneDirectory, TelephoneDirectoryEdit>(item);
                model.TelephoneDirectoryTypes = telephoneDirectoryTypes;
                return View(model);
            }
        }

        // Edit post telephone directory
        [HttpPost]
        public ActionResult Edit(TelephoneDirectoryEdit model)
        {
            OperationStatus status;
            //Create
            if (model.Id == 0)
            {
                var telephoneDirectory = AutoMapper.Mapper.Map<TelephoneDirectoryEdit, TelephoneDirectory>(model);
                status = RapidVetUnitOfWork.TelephoneDirectoryRepository.Create(telephoneDirectory);
            }
            //Update
            else
            {
                var telephoneDirectoryDb = RapidVetUnitOfWork.TelephoneDirectoryRepository.GetItem(model.Id);
                AutoMapper.Mapper.Map(model, telephoneDirectoryDb);
                if(model.TelephoneDirectoryTypeId == null)
                {
                    telephoneDirectoryDb.TelephoneDirectoryTypeId = null;
                }
                status = RapidVetUnitOfWork.TelephoneDirectoryRepository.Update(telephoneDirectoryDb);
            }
            return RedirectToAction("Index");
        }

        //// Delete telephone directory
        //public PartialViewResult Delete(int id)
        //{
        //    var telephoneDirectory = RapidVetUnitOfWork.TelephoneDirectoryRepository.GetItem(id);
        //    return PartialView("_Delete", telephoneDirectory);
        //}

        [HttpPost]
        public JsonResult Delete(int id)
        {           
            var status = RapidVetUnitOfWork.TelephoneDirectoryRepository.Delete(id);
            
            if (status.Success)
            {
                SetSuccessMessage();                
            }
            else
            {
                SetErrorMessage();
            }

            return Json(status);
        }

        public ActionResult Temp()
        {
            return View();
        }

        public JsonResult GetDirectoryTypes()
        {
            var directoryTypes = RapidVetUnitOfWork.TelephoneDirectoryRepository.GetDirectoryTypes(CurrentUser.ActiveClinicId).ToList();
            var model =
                directoryTypes.Select(AutoMapper.Mapper.Map<TelephoneDirectoryType, TelephoneDirectoryTypeJsonModel>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveDirectoryType(TelephoneDirectoryType model)
        {
            OperationStatus status;
            var result = new JsonResult();

            if (model.Id == 0)
            {
                model.Active = true;
                model.Updated = DateTime.Now;
                status = RapidVetUnitOfWork.TelephoneDirectoryRepository.CreateDirectoryType(model);
                result.Data = new { success = status.Success, itemId = model.Id };
            }
            else
            {
                var directoryType = RapidVetUnitOfWork.TelephoneDirectoryRepository.GetDirectoryType(model.Id);
                if (directoryType.ClinicId == model.ClinicId)
                {
                    directoryType.Name = model.Name;
                    directoryType.Updated = DateTime.Now;
                    status = RapidVetUnitOfWork.Save();
                    result.Data = new { success = status.Success, itemId = directoryType.Id };
                }
                else return null;
            }
            if (status.Success)
            {
                SetSuccessMessage("הנתונים נשמרו בהצלחה");
            }
            else
            {
                SetErrorMessage();
            }
            return result;
        }

        [HttpPost]
        public JsonResult DeleteDirectoryType(TelephoneDirectoryType model)
        {
            var directoryType = RapidVetUnitOfWork.TelephoneDirectoryRepository.GetDirectoryType(model.Id);
            if (directoryType.ClinicId == model.ClinicId)
            {
                directoryType.Active = false;
                directoryType.Updated = DateTime.Now;
                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    SetSuccessMessage("הנתונים נשמרו בהצלחה");
                }
                else
                {
                    SetErrorMessage();
                }
                return Json(status.Success);
            }
            return null;
        }

        public ActionResult DirectoryTypes()
        {            
            return View();            
        }

    }
}
