﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.WebModels.ClinicTasks;

namespace RapidVet.Web.Controllers.Tools
{
    public class ClinicTasksController : BaseController
    {
        //
        // GET: /ClinicTasks/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Print(String list)
        {
            var ids = JsonConvert.DeserializeObject<List<int>>(list);
            var tasks = RapidVetUnitOfWork.ClinicTaskRepository.GetTasks(ids);
            return View(tasks);
        }

        [HttpGet]
        public JsonResult GetUserOptions()
        {

            var clinicGroupUsers =
                RapidVetUnitOfWork.ClinicGroupRepository.GetClinicGroupUsers(CurrentUser.ActiveClinicGroupId).ToList();
            var clinicUsers =
                clinicGroupUsers.Where(u => u.UsersClinicsRoleses.Any(c => c.ClinicId == CurrentUser.ActiveClinicId))
                                .ToList();

            var model =
                clinicUsers.Select(u => new SelectListItem()
                    {
                        Text = u.Name,
                        Selected = false,
                        Value = u.Id.ToString()
                    });
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetStatusOptions()
        {
            var rm = RapidVet.Resources.Enums.ClinicTaskStatus.ResourceManager;
            var list = (from object status in Enum.GetValues(typeof(ClinicTaskStatus))
                        select new SelectListItem()
                            {
                                Selected = false,
                                Value = ((int)status).ToString(),
                                Text = rm.GetString(status.ToString())
                            }).ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUrgencyOptions()
        {
            var list = (from object u in Enum.GetValues(typeof(ClinicTasksUrgency))
                        select new SelectListItem()
                            {
                                Selected = false,
                                Value = ((int)u).ToString(),
                                Text =
                                    RapidVet.Resources.Enums.ClinicTasksUrgency.ResourceManager.GetString(u.ToString())
                            }).ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetTasks()
        {
            var tasks = RapidVetUnitOfWork.ClinicTaskRepository.GetClinicTasks(CurrentUser.ActiveClinicId);
            var model = AutoMapper.Mapper.Map<List<ClinicTask>, List<ClinicTaskWebModel>>(tasks);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateClinicTask(String clinicTask)
        {
            var parsed = JsonConvert.DeserializeObject<ClinicTaskWebModel>(clinicTask);
            var parsedTask = AutoMapper.Mapper.Map<ClinicTaskWebModel, ClinicTask>(parsed);
            //parsedTask.StartDate = String.IsNullOrEmpty(parsed.StartDate) ? null : DateTime.ParseExact(parsed.StartDate, "dd/MM/yyyy", null);
            if (!String.IsNullOrEmpty(parsed.StartDate))
            {
                parsedTask.StartDate = DateTime.ParseExact(parsed.StartDate, "dd/MM/yyyy", null);
            }
            if (!String.IsNullOrEmpty(parsed.TargetDate))
            {
                parsedTask.TargetDate = DateTime.ParseExact(parsed.TargetDate, "dd/MM/yyyy", null);
            }
            parsedTask.ClinicId = CurrentUser.ActiveClinicId;

            RapidVetUnitOfWork.ClinicTaskRepository.AddTask(parsedTask);
            var res = RapidVetUnitOfWork.Save();
            if (res.Success)
            {
                SetSuccessMessage("משימה נוצרה בהצלחה");
            }
            else
            {
                SetErrorMessage();
            }
            return Json(res.Success ? parsedTask.Id : 0);
        }

        [HttpPost]
        public JsonResult UpdateclinicTask(String clinicTask)
        {
            var parsed = JsonConvert.DeserializeObject<ClinicTaskWebModel>(clinicTask);
            var parsedTask = AutoMapper.Mapper.Map<ClinicTaskWebModel, ClinicTask>(parsed);
            //parsedTask.StartDate = String.IsNullOrEmpty(parsed.StartDate) ? null : DateTime.ParseExact(parsed.StartDate, "dd/MM/yyyy", null);
            if (!String.IsNullOrEmpty(parsed.StartDate))
            {
                parsedTask.StartDate = DateTime.ParseExact(parsed.StartDate, "dd/MM/yyyy", null);
            }
            if (!String.IsNullOrEmpty(parsed.TargetDate))
            {
                parsedTask.TargetDate = DateTime.ParseExact(parsed.TargetDate, "dd/MM/yyyy", null);
            }

            var task = RapidVetUnitOfWork.ClinicTaskRepository.GetTask(parsed.Id);
            AutoMapper.Mapper.Map<ClinicTask, ClinicTask>(parsedTask, task);
            var res = RapidVetUnitOfWork.Save();
            if (res.Success)
            {
                SetSuccessMessage("משימה עודכנה בהצלחה");
            }
            else
            {
                SetErrorMessage();
            }
            return Json(res.Success);
        }

        [HttpPost]
        public JsonResult RemoveClinicTask(int taskId)
        {
            if (taskId == 0)
            {
                return Json(true);
            }
            var foundAndRemoved = RapidVetUnitOfWork.ClinicTaskRepository.RemoveTask(taskId);
            if (foundAndRemoved)
            {
                var res = RapidVetUnitOfWork.Save();
                return Json(res.Success);
            }
            return Json(true);
        }
    }
}
