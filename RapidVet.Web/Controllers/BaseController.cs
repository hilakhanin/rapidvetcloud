﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using RapidVet.Helpers;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.Users;
using RapidVet.Repository;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Patients;
using RapidVetMembership;
using RapidVet.RapidConsts;
using RapidVet.Model.ClinicGroups;
using System.Globalization;
using System.Configuration;
using RapidVet.Repository.Clinics;
using Microsoft.AspNet.SignalR;
//using RestSharp;
//using RestSharp.Authenticators;

namespace RapidVet.Web.Controllers
{

    public class BaseController : Controller
    {
        private RapidVetUnitOfWork _rapidVetUnitOfWork;
        protected RapidVetUnitOfWork RapidVetUnitOfWork
        {
            get { return _rapidVetUnitOfWork ?? (_rapidVetUnitOfWork = new RapidVetUnitOfWork()); }
        }

        private List<TemplateKeyWords> _templateKeyWords;
        protected List<TemplateKeyWords> TemplateKeyWords
        {
            get { return _templateKeyWords ?? (_templateKeyWords = RapidVetUnitOfWork.TemplateKeyWordsRepository.GetAllTemplateKeyWords(CultureInfo.CurrentCulture.Name)); }
        }

        protected string getTemplateKeyWord(string keyword)
        {
            return TemplateKeyWords.Single(t => t.TemplateKeyWord.Equals(keyword)).KeyWord;
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            SetCurrentUserViewBag();

            //Load Last client to View Bag
            if (User.Identity.IsAuthenticated && !CurrentUser.IsAdministrator)
            {
                ViewBag.LastClients = RapidVetUnitOfWork.LastClientRepository.GetLastClients(CurrentUser.Id,
                                                                                             CurrentUser.ActiveClinicId) ?? new List<RapidVet.Model.Clients.LastClient>().AsQueryable();
            }
            base.OnActionExecuted(filterContext);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            var savePath = System.Configuration.ConfigurationManager.AppSettings["LOG_PATH"];
            if (savePath != null)
                BaseLogger.LogWriter.WriteExPath(filterContext.Exception, savePath);

            if (filterContext.Exception is SecurityException)
            {
                if (filterContext.ExceptionHandled)
                {
                    return;
                }
                if (_currentUser != null)
                {
                    ConcurrentUserInfo.RemoveByUserName(CurrentUser.Username);
                }
                FormsAuthentication.SignOut();
                filterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Base/SecurityExceptionResult.cshtml"
                };
                filterContext.ExceptionHandled = true;
            }
        }

        public ActionResult SecurityExceptionResult()
        {
            if (_currentUser != null)
            {
                ConcurrentUserInfo.RemoveByUserName(CurrentUser.Username);
            }
            FormsAuthentication.SignOut();
            return View();
        }

        private IMembershipService _mebershipebershipService;

        public IMembershipService MembershipService
        {
            get { return _mebershipebershipService ?? (_mebershipebershipService = new MembershipService()); }
            set
            {
                _mebershipebershipService = value;
                SetCurrentUserViewBag();
            }
        }

        private void SetCurrentUserViewBag()
        {
            if (HttpContext != null && (MembershipService.IsAuthenticated))
            {

                ViewBag.CurrentUser = CurrentUser;
            }
        }

        public int? UserId
        {
            get
            {
                if (CurrentUser != null)
                {
                    return CurrentUser.Id;
                }
                else
                {
                    return null;
                }
            }
        }


        private User _currentUser;

        public bool IsCurrentUserNotNull { get { return _currentUser != null; } }

        public User CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    _currentUser = ConcurrentUserInfo.GetConcurrentUserInfoByGuid(User.Identity.Name);
                    if (_currentUser != null && _currentUser.ActiveClinicGroup == null)
                    {
                        using (var clinicGroupRepo = new Repository.Clinics.ClinicGroupRepository())
                        {
                            _currentUser.ActiveClinicGroup = clinicGroupRepo.GetItem(_currentUser.ActiveClinicGroupId);
                        }
                    }
                }
                if (_currentUser == null)
                {
                    Response.Redirect("/base/SecurityExceptionResult", true);
                }
                return _currentUser;
            }
        }


        public void UpdatCurrentUser(User user)
        {
            var cUser = ConcurrentUserInfo.ConcurrentUsers.Single(u => u.User.Id == user.Id);
            if (!CurrentUser.IsAdministrator)
            {
                user.ActiveClinicId = cUser.User.ActiveClinicId;
                user.ActiveClinicGroupId = cUser.User.ActiveClinicGroupId;
                // cUser.DefaultIssuerId = user.DefaultIssuerId;
            }

            cUser.User = user;
        }

        public bool IsPatientInCurrentClinic(int patientId)
        {
            return RapidVetUnitOfWork.PatientRepository.IsPatientInClinic(patientId, CurrentUser.ActiveClinicId);
        }

        public bool IsClientInCurrentClinic(int clientId)
        {
            return RapidVetUnitOfWork.ClientRepository.IsClientInClinic(CurrentUser.ActiveClinicId, clientId);
        }


        public bool IsClinicGroupMember(int clinicGroupId)
        {
            return CurrentUser.ClinicGroupId == null || CurrentUser.ClinicGroupId == clinicGroupId;
        }

        public bool IsSmsActive()
        {
            return !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.UnicellUserName) &&
                   !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.UnicellPassword);
        }

        public bool IsStikerActive()
        {
            return CurrentUser.ActiveClinicGroup.StikerColumns > 0 &&
                   CurrentUser.ActiveClinicGroup.StikerRows > 0 &&
                   CurrentUser.ActiveClinicGroup.StikerRowHight > 0;
        }

        public bool IsEmailActive()
        {
            return !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.EmailAddress);
        }

        public bool IsTreatmentStikerExist()
        {
            return !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.TreatmentStikerTemplate);
        }
        public bool IsTreatmentEmailExist()
        {
            return !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.TreatmentEmailSubject) &&
                   !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.TreatmentEmailTemplate);
        }
        public bool IsTreatmentSmsExist()
        {
            return !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.TreatmentSmsTemplate);
        }
        public bool IsClientStikerExist()
        {
            return !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.ClientsStikerTemplate);
        }
        public bool IsClientEmailExist() //for client reports
        {
            return !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.ClientsEmailSubject) &&
                   !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.ClientsEmailTemplate);
        }
        public bool IsClientSmsExist()
        {
            return !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.ClientsSmsTemplate);
        }
        public bool IsPersonalLetterExist()
        {
            return RapidVetUnitOfWork.LetterTemplatesRepository.IsPersonalLetterExist(CurrentUser.ActiveClinicId);
        }

        public bool IsFollowUpEmailExist()
        {
            return !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.FollowUpsEmailSubject) &&
                   !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.FollowUpsEmailTemplate);
        }

        public bool IsFollowUpSmsExist()
        {
            return !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.FollowUpsSmsTemplate);
        }

        public ClientDetailsModel GetClientDetailsModelFromPatient(int patientId, string dtFrom = "01/01/2017", string dtTo = "31/12/2017", string isToShowMoney = "true")
        {
            var clientId = RapidVetUnitOfWork.PatientRepository.GetClientId(patientId);
            return GetClientDetailsModel(clientId);
        }

        public ClientDetailsModel GetClientDetailsModel(int clientId)
        {
            var client = RapidVetUnitOfWork.ClientRepository.GetClient(clientId);
            var model = AutoMapper.Mapper.Map<Client, ClientDetailsModel>(client);
            if (!model.Balance.HasValue)
            {
                model.Balance = 0;
            }
            model.NextClientAppointment = RapidVetUnitOfWork.CalenderRepository.GetNextClientAppointment(client.Id);            
            
            return model;
        }
        public int GetPatientNextEntryId(int patientId)
        {
            return RapidVetUnitOfWork.CalenderRepository.GetCalenderEntryId(patientId);
        }
        public int GetPatientNextWatchId(int patientId)
        {
            return RapidVetUnitOfWork.CalenderRepository.GetCalenderWatchId(patientId);
        }

        public ActionResult PatientName(int id)
        {
            ViewBag.PatientName = RapidVetUnitOfWork.PatientRepository.GetName(id);
            return View();
        }

        //id is patientId
        public ActionResult GetClientDetailsBar(int id, bool isPatientId = true)
        {
            var model = isPatientId
                            ? GetClientDetailsModelFromPatient(id)
                            : GetClientDetailsModel(id);
            return PartialView("~/Views/Clients/_ClientDetailsBar.cshtml", model);
        }

        public PartialViewResult GetPatientDetailsBar(int id)
        {
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);

            if (patient.Client.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException();
            }

            ViewBag.weight = RapidVetUnitOfWork.PatientRepository.GetLastWeight(patient.Id);

            return PartialView("~/Views/Patients/_PatientDetailsBar.cshtml", patient);
        }
        //id is patientId
        public PartialViewResult GetPatientBreadcrumbs(int id)
        {
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(id);
            var model = new PatientBreadcrumbs
                {
                    ClientId = patient.ClientId,
                    ClientName = patient.Client.Name,
                    PatientId = patient.Id,
                    PatientName = patient.Name
                };
            return PartialView("~/Views/Patients/_PatientBreadcrubs.cshtml", model);
        }

        protected void SetSuccessMessage(string message = "")
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                message = Resources.Global.SuccessMessage;
            }
            Response.Cookies.Add(new HttpCookie("successmsg", Url.Encode(message)));
        }

        protected void SetErrorMessage(string message = "")
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                message = Resources.Global.ErrorMessage;
            }
            Response.Cookies.Add(new HttpCookie("errormsg", Url.Encode(message)));
        }

        protected void SetCustomMessage(string cookieName, string message)
        {
            Response.Cookies.Add(new HttpCookie(cookieName, Url.Encode(message)));
        }

        protected string RenderPartialViewToString()
        {
            return RenderPartialViewToString(null, null);
        }

        protected string RenderPartialViewToString(string viewName)
        {
            return RenderPartialViewToString(viewName, null);
        }

        protected string RenderPartialViewToString(object model)
        {
            return RenderPartialViewToString(null, model);
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }

        }

        protected List<PriceListItemSingleTariffFlatModel> UpdatePrices(
            List<PriceListItemSingleTariffFlatModel> modelList, int tariffId, List<PriceListItem> sourceList)
        {            
            foreach (var modelItem in modelList)
            {
                var source = sourceList.Single(s => s.Id == modelItem.Id);
                var tariffPrice = source.ItemsTariffs.FirstOrDefault(it => it.TariffId == tariffId);
                modelItem.Price = tariffPrice != null ? tariffPrice.Price : 0;               
            }

            return modelList;
        }

        public PartialViewResult ClientPatientsDropDownList(int id)
        {
            var patients = RapidVetUnitOfWork.PatientRepository.GetClientPatients(id).ToList();
            ViewBag.HideInActive = CurrentUser.ActiveClinic.HideInActiveAnimals;
            return PartialView("~/Views/Clients/_ClientPatientsDDL.cshtml", patients);
        }

        public RestSharp.RestClient getMailClient()
        {
            RestSharp.RestClient client = new RestSharp.RestClient();
            client.BaseUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["MAILGUN_BASE_URL"]);
            client.Authenticator =
                   new RestSharp.Authenticators.HttpBasicAuthenticator("api", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_API_KEY"]);

            return client;
        }

        public double GetArchiveUsagePercent(int clinicId, out double size)
        {
            var clinic = RapidVetUnitOfWork.ClinicRepository.GetClinic(clinicId);
            var clinicPatientsProfilePicSize = RapidVetUnitOfWork.ClinicRepository.GetClinicPatientsProfilePicSize(clinicId);
            var archiveSize = RapidVetUnitOfWork.ArchivesRepository.GetArchiveSizeForClinic(clinicId);

            size = (clinicPatientsProfilePicSize + archiveSize) / (1024 * 1024 * 1024);

            return (size / clinic.ArchiveAllocation) * 100;
        }

        public decimal GetTaxRate()
        {
            ClinicRepository clcr = new ClinicRepository();
            var taxRates = clcr.GetAllTaxRateChanges(CurrentUser.ActiveClinicId);
            if (taxRates.Count > 0)
            {
                taxRates = taxRates.Where(t => t.TaxRateChangedDate <= DateTime.Now).OrderByDescending(t => t.TaxRateChangedDate).ToList();
            }

            return taxRates.Count > 0 ? taxRates.First().Rate : 0;
        }

        public bool IsIssuerInClinic(int issuerId)
        {
            if (issuerId == 0)
            {
                return false;
            }
            var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(issuerId);
            return issuer.ClinicId == CurrentUser.ActiveClinicId;
        }

        //phone type ids
        protected int HomePhone()
        {
            return int.Parse(ConfigurationManager.AppSettings["HomePhoneTypeId"]);
        }

        protected int WorkPhone()
        {
            return int.Parse(ConfigurationManager.AppSettings["WorkPhoneTypeId"]);
        }

        protected int CellPhone()
        {
            return int.Parse(ConfigurationManager.AppSettings["CellPhoneTypeId"]);
        }
        public ClientDetailsModel GetNextEntryId(int patientId)
        {
            var clientId = RapidVetUnitOfWork.PatientRepository.GetClientId(patientId);
            return GetClientDetailsModel(clientId);
        }
    }
}
