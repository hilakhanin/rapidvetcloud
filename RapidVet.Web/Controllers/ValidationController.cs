﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class ValidationController : BaseController
    {
        public JsonResult IsUserNameExists(string username)
        {
            var validationMessage = Resources.User.DuplicateUserName;
            int id = 0;
            int.TryParse(Request.Params["Id"], out id);
            var isExist = RapidVetUnitOfWork.MetaDataRepository.IsUserNameExist(username, id);
            return !isExist
                       ? Json(true, JsonRequestBehavior.AllowGet)
                       : Json(validationMessage, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IsIdCardExist(string idcard, int clientId)
        {
            var originalInput = idcard;
           var vMessage = "ת.ז לא תקינה";
           if (clientId > 0)
           {
               var client = RapidVetUnitOfWork.ClientRepository.GetClient(clientId);
               if(!string.IsNullOrEmpty(client.IdCard) && client.IdCard.Equals(idcard))
               {
                   return Json(true, JsonRequestBehavior.AllowGet);
               }
           }
           // Validate correct input
           if(!System.Text.RegularExpressions.Regex.IsMatch(idcard, @"^\d{5,9}$"))
           {
                //return TzStatus.R_ELEGAL_INPUT;
               return Json(vMessage, JsonRequestBehavior.AllowGet);
           }

           // The number is too short - add leading 0000
           if( idcard.Length < 9 ) {
                while( idcard.Length < 9 ) {
                    idcard = '0' + idcard;
                }
           }

           // CHECK THE ID NUMBER
           int mone = 0;
           int incNum;
           for( int i = 0 ; i < 9 ; i++ ) {
                incNum = Convert.ToInt32(idcard[i].ToString());
                incNum *= ( i % 2 ) + 1;
                if( incNum > 9 )
                    incNum -= 9;
                mone += incNum;
           }
           if( mone % 10 != 0 )
           {                
                //return TzStatus.R_NOT_VALID;
                return Json(vMessage, JsonRequestBehavior.AllowGet);
           }  
          
           var validationMessage = "ת.ז כבר קיימת במערכת";
          
           var isExist = RapidVetUnitOfWork.MetaDataRepository.IsClientIdCardExistInClinic(originalInput.PadLeft(9, '0'), clientId,
                                                                                           CurrentUser.ActiveClinicId);
           return !isExist
                      ? Json(true, JsonRequestBehavior.AllowGet)
                      : Json(validationMessage, JsonRequestBehavior.AllowGet);

          // return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IsCatalogExist(string catalog)
        {
            var validationMessage = "מקט קיים במערכת";

            var id = 0;
            int.TryParse(Request.Params["Id"], out id);

            var isExist = RapidVetUnitOfWork.MetaDataRepository.IsCatalogExist(catalog, id, CurrentUser.ActiveClinicId);
            return !isExist
                       ? Json(true, JsonRequestBehavior.AllowGet)
                       : Json(validationMessage, JsonRequestBehavior.AllowGet);
        }
    }
}
