using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web.Mvc;
using AutoMapper;
using RapidVet.Exeptions;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.Visits;
using RapidVet.RapidConsts.Controllers;
using RapidVet.Repository.Clinics;
using RapidVet.Resources;
using RapidVet.WebModels.Clinics;
using Clinic = RapidVet.Model.Clinics.Clinic;
using Client = RapidVet.Model.Clients.Client;
using Diagnosis = RapidVet.Model.Visits.Diagnosis;
using EntityType = RapidVet.Helpers.EntityType;
using OperationType = RapidVet.Helpers.OperationType;
using RapidVet.Model.Expenses;

namespace RapidVet.Web.Controllers
{
    public class ClinicsController : BaseController
    {
        public static int EMAILS_NO_CHARGE_NUMBER = 1000;
        /// <summary>
        /// displays all clinics in active clinic group
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult Index()
        {
            var model = RapidVetUnitOfWork.ClinicRepository.GetList(CurrentUser.ActiveClinicGroupId);
            return View(model);
        }


        /// <summary>
        /// displays create clinic form
        /// </summary>
        /// <returns>PartialViewResult</returns>
        [HttpGet]
        public PartialViewResult Create()
        {
            var model = new ClinicCreate()
                {
                    ClinicGroupId = CurrentUser.ActiveClinicGroupId
                };
            return PartialView("_Create", model);
        }

        /// <summary>
        /// handles creation of clinic
        /// </summary>
        /// <param name="model">ClinicCreate object</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult Create(ClinicCreate model)
        {
            var clinic = AutoMapper.Mapper.Map<ClinicCreate, Clinic>(model);
            clinic.CreatedById = UserId;

            if (CurrentUser.IsAdministrator || CurrentUser.IsClinicGroupManager)
            {
                //clinic.TaxRates.Add(new TaxRate()
                //    {
                //        TaxRateChangedDate = DateTime.Now,
                //        Rate = ClinicConsts.TAX_RATE
                //    });

                //default tariff
                var defaultTariff = CreateDefaultTariff();
                clinic.Tariffs.Add(defaultTariff);

                //other defaults: regional council & city
                var defaultRegionalCouncil =
                    RapidVetUnitOfWork.RegionalCouncilsRepository.GetAllWithCities().FirstOrDefault();
                if (defaultRegionalCouncil != null)
                {
                    clinic.RegionalCouncilId = defaultRegionalCouncil.Id;
                    var defaultCity = defaultRegionalCouncil.Cities.OrderBy(c => c.Name).FirstOrDefault();
                    if (defaultCity != null)
                    {
                        clinic.CityId = defaultCity.Id;
                    }
                }
                //animal kind
                var animalKind = CurrentUser.ActiveClinicGroup.AnimalKinds.FirstOrDefault();
                if (animalKind != null)
                {
                    clinic.AnimalKindId = animalKind.Id;
                }

                clinic.Diagnoses.Add(new Diagnosis()
                {
                    Active = true,
                    ClinicId = 1,
                    Name = Resources.Diagnosis.GeneralDiagnosis
                });

                clinic.DefaultNewClientStatusId = RapidVetUnitOfWork.ClientStatusRepository.GetAll(CurrentUser.ActiveClinicGroupId).First().Id;
                var expenseGroups = new List<ExpenseGroup>();
                expenseGroups.Add(CreateDefaultExpenseGroup());
                clinic.ExpenseGroups = expenseGroups;
                //save
                var status = RapidVetUnitOfWork.ClinicRepository.Create(clinic);
                CurrentUser.ActiveClinicGroup.Clinics.Add(clinic);


                if (status.Success)
                {


                    clinic.DefualtTariffId = defaultTariff.Id;
                    RapidVetUnitOfWork.ClinicRepository.AddTaxRate(new TaxRate() { Rate = ClinicConsts.TAX_RATE, TaxRateChangedDate = DateTime.Now, ClinicId = clinic.Id });
                    RapidVetUnitOfWork.Save();

                    SetSuccessMessage();
                    CurrentUser.UpdateClinic(clinic);
                }
                else
                {
                    SetErrorMessage();
                }
            }

            return RedirectToAction("Index", "Clinics");
        }

        public ActionResult Edit()
        {
            var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(CurrentUser.ActiveClinicId);
            var cities = RapidVetUnitOfWork.ClientRepository.GetCities();
            var model = AutoMapper.Mapper.Map<Clinic, ClinicEdit>(clinic);
            var city = cities.Find(x => x.Id == model.CityId);
            if (city != null)
                model.CityName = city.Name;

            return View(model);
        }


        [HttpPost]
        public ActionResult Edit(ClinicEdit model)
        {
            model.CityId = getCityID(model.CityName);
            if (CurrentUser.IsClinicManager || CurrentUser.IsClinicGroupManager || CurrentUser.IsAdministrator)
            {
                if (ModelState.IsValid)
                {
                    var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(model.Id);
                    if (clinic.Id != CurrentUser.ActiveClinicId)
                    {
                        throw new SecurityException(Exceptions.AccessDenied);
                    }
                    Mapper.Map(model, clinic);

                    if (!model.MainVetId.HasValue)
                    {
                        clinic.MainVetId = null;
                        clinic.MainVet = null;
                    }
                    // clinic.ClinicGroupID = CurrentUser.ActiveClinicGroupId;
                    clinic.Updated = DateTime.Now;
                    var status = RapidVetUnitOfWork.ClinicRepository.Update(clinic);
                    if (status.Success)
                    {
                        SetSuccessMessage();
                        CurrentUser.UpdateClinic(clinic);
                    }
                    else
                    {
                        SetErrorMessage();
                    }
                    return RedirectToAction("Edit", "Clinics");
                }
            }
            else
            {
                ModelState.AddModelError("", Resources.GlobalValidationErrors.UnAuthorisedAccess);
            }
            return View(model);
        }



        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            var model = RapidVetUnitOfWork.ClinicRepository.GetItem(id);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public ActionResult Delete(int id, string groupName)
        {
            var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(id);

            clinic.Active = false;
            clinic.UpdatedById = UserId;
            clinic.UpdatedDate = DateTime.Now;
            var status = new OperationStatus() { Success = false };

            if (CurrentUser.IsAdministrator || CurrentUser.IsClinicGroupManager)
            {
                status = RapidVetUnitOfWork.ClinicRepository.Update(clinic);
            }
            return RedirectToAction("Index", "Clinics");
        }

        [HttpPost]
        public ActionResult RemoveIssuer(int id)
        {
            var status = RapidVetUnitOfWork.ClinicRepository.RemoveIssuer(id);
            if (status.Success)
            {
                ViewBag.message = string.Format(RapidVet.Resources.Controllers.ClinicController.RemoveIssuerSuccess
                                                        , status.Message);
                return RedirectToAction("Details", new { id = id });
            }
            throw new OperationStatusException(status);
        }

        public ActionResult Details(int id)
        {
            var model = RapidVetUnitOfWork.ClinicRepository.GetItem(id);
            return View(model);
        }

        //id is clinicId
        [HttpGet]
        public ActionResult Activate(int id)
        {
            var model = RapidVetUnitOfWork.ClinicRepository.GetItem(id);
            return PartialView("_Activate", model);
        }


        [HttpPost]
        public ActionResult Activate(int id, string groupName)
        {
            var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(id);

            clinic.Active = true;
            clinic.UpdatedById = UserId;
            clinic.UpdatedDate = DateTime.Now;
            var status = new OperationStatus() { Success = false };
            if (CurrentUser.IsAdministrator || CurrentUser.IsClinicGroupManager)
            {
                status = RapidVetUnitOfWork.ClinicRepository.Update(clinic);
            }
            return RedirectToAction("Index", "Clinics");
        }

        [HttpGet]
        public ActionResult PatientHistorySetting()
        {
            var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(CurrentUser.ActiveClinicId);
            return View(clinic);
        }


        [HttpPost]
        public ActionResult PatientHistorySetting(Clinic model)
        {
            var status = new OperationStatus() { Success = true };
            bool isUpdated = false;

            if (CurrentUser.IsClinicManager || CurrentUser.IsClinicGroupManager || CurrentUser.IsAdministrator)
            {
                var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(model.Id);
                if (clinic.Id != CurrentUser.ActiveClinicId)
                {
                    throw new SecurityException(Exceptions.AccessDenied);
                }

                if (clinic.ShowComplaint != model.ShowComplaint || clinic.ShowExaminations != model.ShowExaminations || clinic.ShowDiagnosis != model.ShowDiagnosis ||
                    clinic.ShowTreatments != model.ShowTreatments || clinic.ShowMedicins != model.ShowMedicins || clinic.ShowTemperature != model.ShowTemperature ||
                    clinic.ShowWeight != model.ShowWeight || clinic.ShowOldVisits != model.ShowOldVisits || clinic.ShowVisitNote != model.ShowVisitNote ||
                    clinic.HideInActiveAnimals != model.HideInActiveAnimals)
                {
                    var currentClinicInGroup = (CurrentUser.ActiveClinicGroup == null || CurrentUser.ActiveClinicGroup.Clinics == null ? null : CurrentUser.ActiveClinicGroup.Clinics.FirstOrDefault(x => x.Id == clinic.Id)) ?? new Clinic();
                    currentClinicInGroup.ShowOldVisits = clinic.ShowOldVisits = model.ShowOldVisits;
                    currentClinicInGroup.ShowVisitNote = clinic.ShowVisitNote = model.ShowVisitNote;
                    currentClinicInGroup.ShowComplaint = clinic.ShowComplaint = model.ShowComplaint;
                    currentClinicInGroup.ShowExaminations = clinic.ShowExaminations = model.ShowExaminations;
                    currentClinicInGroup.ShowDiagnosis = clinic.ShowDiagnosis = model.ShowDiagnosis;
                    currentClinicInGroup.ShowTreatments = clinic.ShowTreatments = model.ShowTreatments;
                    currentClinicInGroup.ShowMedicins = clinic.ShowMedicins = model.ShowMedicins;
                    currentClinicInGroup.ShowTemperature = clinic.ShowTemperature = model.ShowTemperature;
                    currentClinicInGroup.ShowWeight = clinic.ShowWeight = model.ShowWeight;
                    currentClinicInGroup.HideInActiveAnimals = clinic.HideInActiveAnimals = model.HideInActiveAnimals;
                    isUpdated = true;
                }
                if (isUpdated)
                {
                    status = RapidVetUnitOfWork.ClinicRepository.Update(clinic);
                }
                if (status.Success)
                {
                    SetSuccessMessage("����� ���� ������");
                }
                else
                {
                    SetErrorMessage();
                }
                return RedirectToAction("PatientHistorySetting", "Clinics");

            }
            else
            {
                ModelState.AddModelError("", Resources.GlobalValidationErrors.UnAuthorisedAccess);
            }
            return View(model);
        }

        public JsonResult GetClinicClients()
        {
            var clients = RapidVetUnitOfWork.ClientRepository.GetAllClients(CurrentUser.ActiveClinicId).ToList();
            var model = clients.Select(AutoMapper.Mapper.Map<Client, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FinanceDetails()
        {
            var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(CurrentUser.ActiveClinicId);
            var model = AutoMapper.Mapper.Map<Clinic, FinanceClinicEdit>(clinic);
            model.TaxRate = GetTaxRate();
            return View(model);
        }

        [HttpPost]
        public ActionResult FinanceDetails(FinanceClinicEdit model)
        {
            if (CurrentUser.IsClinicManager || CurrentUser.IsClinicGroupManager || CurrentUser.IsAdministrator)
            {
                if (ModelState.IsValid)
                {
                    var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(model.Id);
                    if (clinic.Id != CurrentUser.ActiveClinicId)
                    {
                        throw new SecurityException(Exceptions.AccessDenied);
                    }
                    Mapper.Map(model, clinic);
                    clinic.Updated = DateTime.Now;
                    // clinic.ClinicGroupID = CurrentUser.ActiveClinicGroupId;
                    var status = RapidVetUnitOfWork.ClinicRepository.Update(clinic);
                    if (status.Success)
                    {
                        CurrentUser.UpdateClinic(clinic);
                        SetSuccessMessage();
                    }
                    else
                    {
                        SetErrorMessage();
                    }
                    return RedirectToAction("FinanceDetails", "Clinics");
                }
            }
            else
            {
                ModelState.AddModelError("", Resources.GlobalValidationErrors.UnAuthorisedAccess);
            }
            return View(model);
        }

        //public ActionResult ChangeAdvancedPaymentPercent()
        //{
        //    var lastChanges = RapidVetUnitOfWork.ClinicRepository.GetAllAdvancePaymentPercentChanges(CurrentUser.ActiveClinicId);
        //    return View(lastChanges);
        //}

        public ActionResult ChangeAdvancedPaymentPercent(int id)
        {
            if (IsIssuerInClinic(id))
            {
                return View();
            }
            throw new SecurityException();
        }

        //private bool IsIssuerInClinic(int issuerId)
        //{
        //    var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(issuerId);
        //    return issuer.ClinicId == CurrentUser.ActiveClinicId;
        //}

        //id is issuerId
        public JsonResult GetAdvancedPaymentPercentsData(int id)
        {
            var model = GetAdvancedPaymentPercentData(id);

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        private List<AdvancedPaymentsPercentWebModel> GetAdvancedPaymentPercentData(int issuerId)
        {
            var percents = RapidVetUnitOfWork.ClinicRepository.GetAllAdvancePaymentPercentChanges(CurrentUser.ActiveClinicId, issuerId);
            var result = percents.Select(Mapper.Map<AdvancedPaymentsPercent, AdvancedPaymentsPercentWebModel>).OrderByDescending(r => r.DateTime).ToList();

            return result;
        }


        //[HttpPost]
        //public ActionResult ChangeAdvancedPaymentPercent(decimal? advancedPaymentPercent)
        //{
        //    if (CurrentUser.IsClinicManager || CurrentUser.IsClinicGroupManager || CurrentUser.IsAdministrator)
        //    {
        //        if (advancedPaymentPercent.HasValue)
        //        {
        //            var advancedPaymentsPercent = new AdvancedPaymentsPercent()
        //                {
        //                    ClinicId = CurrentUser.ActiveClinicId,
        //                    Percent = advancedPaymentPercent.Value,
        //                    PercentChangedDate = DateTime.Now
        //                };
        //            var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(CurrentUser.ActiveClinicId);
        //            clinic.AdvancedPaymentPercent = advancedPaymentPercent.Value;
        //            RapidVetUnitOfWork.ClinicRepository.UpdateAdvancePaymentPercent(advancedPaymentsPercent);
        //            var status = RapidVetUnitOfWork.Save();
        //            if (status.Success)
        //            {
        //                SetSuccessMessage("���� ���� ���� ������");
        //            }
        //            else
        //            {
        //                SetErrorMessage();
        //            }
        //            return RedirectToAction("ChangeAdvancedPaymentPercent", "Clinics");
        //        }
        //    }
        //    else
        //    {
        //        ModelState.AddModelError("", Resources.GlobalValidationErrors.UnAuthorisedAccess);
        //    }
        //    return RedirectToAction("ChangeAdvancedPaymentPercent", "Clinics");
        //}

        [HttpPost]
        public JsonResult UpdateAdvancedPaymentPercent(AdvancedPaymentsPercentWebModel model)
        {
            var isAllowd = CurrentUser.IsClinicManager || CurrentUser.IsClinicGroupManager ||
                       CurrentUser.IsAdministrator;
            var date = StringUtils.ParseStringToDateTime(model.Date);

            if (date > DateTime.MinValue && model.Percent > -1 && isAllowd)
            {
                var percent = RapidVetUnitOfWork.ClinicRepository.GetAdvancedPaymentPercent(model.Id);
                percent.PercentChangedDate = date;
                percent.Percent = model.Percent;
                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    var clinic = RapidVetUnitOfWork.ClinicRepository.GetClinic(CurrentUser.ActiveClinicId);
                    CurrentUser.UpdateClinic(clinic);
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
            }
            else
            {
                SetErrorMessage();
            }
            var result = GetAdvancedPaymentPercentData(model.IssuerId);
            return Json(result);
        }

        public ActionResult ChangeTaxRate()
        {
            return View();
        }

        public JsonResult GetTaxRatesData()
        {
            var model = GetRatesData();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateTaxRate(TaxRateWebModel model)
        {
            var isAllowd = CurrentUser.IsClinicManager || CurrentUser.IsClinicGroupManager ||
                       CurrentUser.IsAdministrator;
            var date = StringUtils.ParseStringToDateTime(model.Date);
            if (date > DateTime.MinValue && model.Rate > -1 && isAllowd)
            {
                var rate = Mapper.Map<TaxRateWebModel, TaxRate>(model);
                rate.TaxRateChangedDate = date;
                rate.ClinicId = CurrentUser.ActiveClinicId;
                var status = RapidVetUnitOfWork.ClinicRepository.AddTaxRate(rate);
                if (status.Success)
                {
                    string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}",
                                                                Resources.Auditing.Action, Resources.Auditing.CreateTax,
                                                                Resources.Auditing.Percent, rate.Rate,
                                                                Resources.Auditing.EffectiveDate, rate.TaxRateChangedDate.ToShortDateString());

                    RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.TaxAudit, logMessage, "");
                    var clinic = RapidVetUnitOfWork.ClinicRepository.GetClinic(CurrentUser.ActiveClinicId);
                    CurrentUser.UpdateClinic(clinic);
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                    return RedirectToAction("ChangeTaxRate");
                }
            }
            else
            {
                SetErrorMessage();
                return RedirectToAction("ChangeTaxRate");
            }
            //var result = GetRatesData();
            //return Json(result);
            return RedirectToAction("ChangeTaxRate");
        }

        [HttpPost]
        public ActionResult CreateAdvancedPaymentPercent(AdvancedPaymentsPercentWebModel model)
        {
            var isAllowd = CurrentUser.IsClinicManager || CurrentUser.IsClinicGroupManager ||
                       CurrentUser.IsAdministrator;
            var date = StringUtils.ParseStringToDateTime(model.Date);
            var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(model.IssuerId);
            if (date > DateTime.MinValue && model.Percent > -1 && isAllowd)
            {
                var percent = Mapper.Map<AdvancedPaymentsPercentWebModel, AdvancedPaymentsPercent>(model);
                percent.PercentChangedDate = date;
                percent.ClinicId = CurrentUser.ActiveClinicId;
                var status = RapidVetUnitOfWork.ClinicRepository.AddAdvancedPaymentPercent(percent);
                if (status.Success)
                {
                    var clinic = RapidVetUnitOfWork.ClinicRepository.GetClinic(CurrentUser.ActiveClinicId);
                    CurrentUser.UpdateClinic(clinic);
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                    return RedirectToAction("ChangeAdvancedPaymentPercent", new { id = model.IssuerId, name = issuer.Name });
                }
            }
            else
            {
                SetErrorMessage();
                return RedirectToAction("ChangeAdvancedPaymentPercent", new { id = model.IssuerId, name = issuer.Name });
            }
            //var result = GetRatesData();
            //return Json(result);
            return RedirectToAction("ChangeAdvancedPaymentPercent", new { id = model.IssuerId, name = issuer.Name });
        }

        [HttpPost]
        public JsonResult UpdateTaxRate(TaxRateWebModel model)
        {
            var isAllowd = CurrentUser.IsClinicManager || CurrentUser.IsClinicGroupManager ||
                       CurrentUser.IsAdministrator;
            var date = StringUtils.ParseStringToDateTime(model.Date);
            if (date > DateTime.MinValue && model.Rate > -1 && isAllowd)
            {
                var rate = RapidVetUnitOfWork.ClinicRepository.GetTaxRate(model.Id);
                var oldDate = rate.TaxRateChangedDate;
                var oldRate = rate.Rate;
                rate.TaxRateChangedDate = date;
                rate.Rate = model.Rate;
                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}",
                                                               Resources.Auditing.Action, Resources.Auditing.UpadteTax,
                                                               Resources.Auditing.Percent, rate.Rate,
                                                               Resources.Auditing.EffectiveDate, rate.TaxRateChangedDate.ToShortDateString());
                    string logMessageOldContent = string.Format("{0}: {1}<br />{2}: {3}",
                                                               Resources.Auditing.Percent, oldRate,
                                                               Resources.Auditing.EffectiveDate, oldDate.ToShortDateString());
                    RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.TaxAudit, logMessage, logMessageOldContent);
                    var clinic = RapidVetUnitOfWork.ClinicRepository.GetClinic(CurrentUser.ActiveClinicId);
                    CurrentUser.UpdateClinic(clinic);
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }
            }
            else
            {
                SetErrorMessage();
            }
            var result = GetRatesData();
            return Json(result);
        }

        //id is taxRate id
        [HttpPost]
        public JsonResult DeleteTaxRate(int id)
        {
            var tax = RapidVetUnitOfWork.ClinicRepository.GetTaxRate(id);
            var taxDate = tax.TaxRateChangedDate;
            var taxRate = tax.Rate;
            var status = RapidVetUnitOfWork.ClinicRepository.RemoveTaxRate(id);
            if (status.Success)
            {
                string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}",
                                                              Resources.Auditing.Action, Resources.Auditing.DeleteTax,
                                                              Resources.Auditing.Percent, taxRate,
                                                              Resources.Auditing.EffectiveDate, taxDate.ToShortDateString());
                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.TaxAudit, logMessage, "");
                var clinic = RapidVetUnitOfWork.ClinicRepository.GetClinic(CurrentUser.ActiveClinicId);
                CurrentUser.UpdateClinic(clinic);
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return Json(status.Success);
        }

        //id is advanced payment percent id
        [HttpPost]
        public JsonResult DeleteAdvancedPaymentPercent(int id)
        {
            var status = RapidVetUnitOfWork.ClinicRepository.RemoveAdvancedPaymentPercent(id);
            if (status.Success)
            {
                var clinic = RapidVetUnitOfWork.ClinicRepository.GetClinic(CurrentUser.ActiveClinicId);
                CurrentUser.UpdateClinic(clinic);
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage();
            }
            return Json(status.Success);
        }

        private List<TaxRateWebModel> GetRatesData()
        {
            var rates = RapidVetUnitOfWork.ClinicRepository.GetAllTaxRateChanges(CurrentUser.ActiveClinicId);
            var result = rates.Select(Mapper.Map<TaxRate, TaxRateWebModel>).OrderByDescending(r => r.DateTime).ToList();
            return result;
        }


        public JsonResult GetClinicDoctors()
        {
            var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId);
            var model = doctors.Select(Mapper.Map<Model.Users.User, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /*--------------------private-----------------------*/
        private Model.Finance.Tariff CreateDefaultTariff()
        {
            var defaultTarrif = new Model.Finance.Tariff()
            {
                Active = true,
                Name = Resources.ClinicGroup.DefaultTariffName,
                CreatedDate = DateTime.Now
            };
            return defaultTarrif;
        }

        private Model.Expenses.ExpenseGroup CreateDefaultExpenseGroup()
        {
            var defaultExpenseGroup = new Model.Expenses.ExpenseGroup()
            {
                Name = RapidConsts.ExpensesConstants.EquipmentAndPermanentAssets,
                IsTextDeductible = true,
                RecognitionPercent = 100
            };
            return defaultExpenseGroup;
        }

        public ActionResult Autocomplete(string term)
        {
            var cities = RapidVetUnitOfWork.ClientRepository.GetCities(term);
            var filteredItems = cities.Select(x => x.Name).ToArray();
            //var filteredItems = items.Where(item => item.IndexOf(term, StringComparison.InvariantCultureIgnoreCase) >= 0);
            return Json(filteredItems, JsonRequestBehavior.AllowGet);
        }

        private int getCityID(string city)
        {
            var cities = RapidVetUnitOfWork.ClientRepository.GetCities();
            int id;
            if (int.TryParse(city, out id) || cities == null || String.IsNullOrEmpty(city))
                return id;

            var c = cities.Find(x => x.Name.Equals(city));
            return c == null ? -1 : c.Id;
        }
    }
}