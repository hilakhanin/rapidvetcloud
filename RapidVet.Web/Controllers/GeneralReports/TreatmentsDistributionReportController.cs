﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using CsvHelper;
using CsvHelper.Configuration;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model.Visits;
using RapidVet.Web.Attributes;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.VaccinesAndTreatments;
using RapidVet.WebModels.Visits;

namespace RapidVet.Web.Controllers.GeneralReports
{
    public class TreatmentsDistributionReportController : BaseController
    {
        [ViewReports]
        public ActionResult Index(int? doctorId, string fromTime, string toTime, bool toPrint = false)
        {
            ViewBag.ToPrint = toPrint;
            var now = DateTime.Now.Date;
            var model = CreateModel(doctorId, fromTime ?? now.AddDays(-now.Day + 1).ToString("dd/MM/yyyy"), toTime ?? now.ToString("dd/MM/yyyy"));
            model.Doctors =
                RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId)
                                  .Select(d => new SelectListItem()
                                      {
                                          Text = d.Name,
                                          Value = d.Id.ToString()
                                      }).ToList();
            return View(model);
        }

        [ExcelExport]
        public FileResult CsvFile(int? doctorId, string fromTime, string toTime)
        {
            var model = CreateModel(doctorId, fromTime, toTime);

            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8 };
            var csv = new CsvWriter(writer, csvConfig);

            csv.WriteRecords(model.TreatmentsDistributions);
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", string.Format("TreatmentDistributionReport.csv"));
        }

        [ViewReports]
        public FileResult Graph(int? doctorId, string fromTime, string toTime)
        {
            var model = CreateModel(doctorId, fromTime, toTime);
            var doctorName = string.Empty;
            if (doctorId.HasValue)
            {
                var doctor = RapidVetUnitOfWork.UserRepository.GetItem(doctorId.Value);
                doctorName = doctor.Name;
            }

            var chartImage = new Chart();

            chartImage.Width = 800;
            chartImage.Height = 600;
            chartImage.ChartAreas.Add("chart");
            chartImage.ChartAreas["chart"].AxisX.MajorGrid.LineColor = Color.LightGray;
            chartImage.ChartAreas["chart"].AxisY.MajorGrid.LineColor = Color.LightGray;
            chartImage.ChartAreas[0].AxisX.Interval = 1;
            chartImage.ChartAreas[0].Area3DStyle.Enable3D = true;

            chartImage.Titles.Add("דוח התפלגות טיפולים\n" + doctorName);
            chartImage.Series.Add("report");
            chartImage.Series["report"].ChartType = SeriesChartType.Pie;
            chartImage.Series["report"]["PieLabelStyle"] = "outside";
            //chartImage.Series["report"].IsValueShownAsLabel = true;

            foreach (var item in model.TreatmentsDistributions.OrderByDescending(i => i.Percent))
            {
                var nameString = !string.IsNullOrEmpty(item.Name) ? item.Name : "טיפול ללא שם";
                var roundedVal = decimal.Round(item.Percent, 2, MidpointRounding.AwayFromZero);

                chartImage.Series["report"].Points.Add(new DataPoint()
                    {
                        AxisLabel = "(" + nameString + " (" + roundedVal + "% ",

                        YValues = new double[] { (double)roundedVal }
                    });
            }

            using (var strm = new MemoryStream())
            {

                chartImage.SaveImage(strm, ChartImageFormat.Png);
                strm.Seek(0, SeekOrigin.Begin);
                return File(strm.ToArray(), "image/png", "treatmentDistribution.png");
            }
        }

        private TreatmentDistributionWebModel CreateModel(int? doctorId, string fromTime, string toTime)
        {
            DateTime from;
            DateTime to;
            if (string.IsNullOrWhiteSpace(fromTime) || string.IsNullOrWhiteSpace(toTime))
            {
                to = DateTime.Now;
                from = to.AddDays(-7);
            }
            else
            {
                from = DateTime.MinValue;
                to = DateTime.MinValue;
                var format = "dd/MM/yyyy";
                var unparsedFrom = fromTime;
                DateTime.TryParseExact(unparsedFrom, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out from);
                if (from == DateTime.MinValue)
                {
                    from = DateTime.Now;
                }
                var unparsedTo = toTime;
                DateTime.TryParseExact(unparsedTo, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out to);
                if (to == DateTime.MinValue)
                {
                    to = DateTime.Now;
                }
            }
            var model = new TreatmentDistributionWebModel
               {
                   TreatmentsDistributions = new List<TreatmentDistribution>()
               };

            var visitTreatments = RapidVetUnitOfWork.VisitRepository.GetAllVisitTreatmentsOfDocAtDateRange(CurrentUser.ActiveClinicId, doctorId, from, to.AddDays(1)).GroupBy(v => v.Name);
            decimal totalQuantity = 0;
            foreach (var visitTreatment in visitTreatments)
            {
                var currentQuantity = visitTreatment.Sum(treatment => treatment.Quantity);
                model.TreatmentsDistributions.Add(new TreatmentDistribution()
                {
                    Name = visitTreatment.First().Name,
                    Quantity = currentQuantity,
                });
                totalQuantity += currentQuantity;
            }
            foreach (var treatment in model.TreatmentsDistributions)
            {
                treatment.Percent = ((decimal)treatment.Quantity / (decimal)totalQuantity) * 100;
            }
            return model;
        }
    }
}