﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CsvHelper;
using CsvHelper.Configuration;
using Newtonsoft.Json;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Web.Attributes;
using RapidVet.WebModels.GeneralReports;
using Newtonsoft.Json.Serialization;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class PreformedPreventiveController : BaseController
    {
        [ViewReports]
        public ActionResult Index()
        {
            var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.From = fromDate.ToShortDateString();
            ViewBag.To = fromDate.AddMonths(1).AddDays(-1).ToShortDateString();
            return View();
        }

        public JsonResult GetData(PreformedPreventiveReportFilterModel filter)
        {
            var data = GetReportData(filter);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Print(PreformedPreventiveReportFilterModel filter)
        {
            var data = GetReportData(filter);
            ViewBag.From = filter.From;
            ViewBag.To = filter.To;
            ViewBag.To = filter.To;
            return View(data);
        }

        [ViewReports]
        public FileResult Export(PreformedPreventiveReportFilterModel filter)
        {
            var data = GetReportData(filter);

            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8 };
            var csv = new CsvWriter(writer, csvConfig);
            csv.WriteRecords(data);
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", "preformedPreventive.csv");
        }

        private List<PreformedPreventiveReportModel> GetReportData(PreformedPreventiveReportFilterModel filter)
        {
            var result = new List<PreformedPreventiveReportModel>();
            var fromDate = StringUtils.ParseStringToDateTime(filter.From);
            var toDate = StringUtils.ParseStringToDateTime(filter.To);
            var selectedItems = new List<int>();
            if (!string.IsNullOrWhiteSpace(filter.SelectedItems))
            {
                selectedItems = StringUtils.GetIntList(filter.SelectedItems);
                //JsonConvert.DeserializeObject<List<int>>(filter.SelectedItems);
            }

            var preventiveItems = RapidVetUnitOfWork.PreventiveMedicineRepository.GetReportDataItems(
                                                            CurrentUser.ActiveClinicId,
                                                            fromDate, toDate,
                                                            filter.AnimalKindId,
                                                            filter.IncludeExternalPreformances,
                                                            filter.IncludeInactivePatients,
                                                            selectedItems);

            return preventiveItems;
        }


        public void GetPreformedPreventiveData(string strFromDate, string strToDate, string strSelectedItems, 
            string AnimalKindId, string IncludeExternalPreformances, string IncludeInactivePatients)
        {
            if (AnimalKindId.Trim().Length == 0)
                AnimalKindId = "0";

            var result = new List<PreformedPreventiveReportModel>();
            var fromDate = StringUtils.ParseStringToDateTime(strFromDate);
            var toDate = StringUtils.ParseStringToDateTime(strToDate);
            var selectedItems = new List<int>();
            if (!string.IsNullOrWhiteSpace(strSelectedItems))
            {
                selectedItems = StringUtils.GetIntList(strSelectedItems);
            }

            var preventiveItems = RapidVetUnitOfWork.PreventiveMedicineRepository.GetReportDataItems(
                                                            CurrentUser.ActiveClinicId,
                                                            fromDate, toDate,
                                                            Convert.ToInt32(AnimalKindId),
                                                            Convert.ToBoolean(IncludeExternalPreformances),
                                                            Convert.ToBoolean(IncludeInactivePatients),
                                                            selectedItems);

            //StringBuilder sb = new StringBuilder();
            //StringWriter sw = new StringWriter(sb);

            //using (JsonWriter writer = new JsonTextWriter(sw))
            //{
            //    var serializer = new JsonSerializer();
            //    serializer.Serialize(writer, preventiveItems);

            //    Response.Write(writer);
            //}
            //StringBuilder sb = new StringBuilder();
            //StringWriter sw = new StringWriter(sb);
            //JsonWriter jsonWriter = new JsonTextWriter(sw);
            //var serializer = new JsonSerializer();

            //jsonWriter.Formatting = Formatting.Indented;
            
            //serializer.Serialize(jsonWriter, preventiveItems);

            ////Response.Write(sb.ToString());
            //Response.BinaryWrite(jsonWriter.re);

            using (MemoryStream ms = new MemoryStream())
            {
                _serializeJson(preventiveItems, ms);
                byte[] bytes = ms.ToArray();
                //Response.Write(bytes, Formatting.Indented);
                Response.ContentType = "application/json";
                Response.BinaryWrite(bytes);
                Response.Flush();
            }
            

            
        }
        private static void _serializeJson<T>(T obj, Stream stream)
        {
            try
            {
                using (var streamWriter = new StreamWriter(stream, Encoding.UTF8, 1024))
                using (var jsonWriter = new JsonTextWriter(streamWriter))
                {
                    var serializer = new JsonSerializer();
                    serializer.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    serializer.Formatting = Formatting.Indented;
                    serializer.Serialize(jsonWriter, obj);
                }
            }
            catch (Exception e)
            {
                //Logger.WriteError(e.ToString());
                Console.WriteLine(e.ToString());
            }
        }
    }
}
