﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CsvHelper;
using CsvHelper.Configuration;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model.Visits;
using RapidVet.Web.Attributes;
using RapidVet.WebModels.Visits;
using RestSharp;
using RapidVet.Model.Clinics;

namespace RapidVet.Web.Controllers.GeneralReports
{
    public class TreatmentReportsController : BaseController
    {
        [ViewReports]
        public ActionResult Index()
        {
            ViewBag.Sms = IsSmsActive() && IsTreatmentSmsExist();
            ViewBag.Stikers = IsStikerActive() && IsTreatmentStikerExist();
            ViewBag.Email = IsEmailActive() && IsTreatmentEmailExist();
            ViewBag.Letter = IsPersonalLetterExist();

            return View();
        }

        [ViewReports]
        public JsonResult GetFiltersList()
        {
            var filters = RapidVetUnitOfWork.ClinicRepository.GetVisitFilters(CurrentUser.ActiveClinicId);
            var model = filters.Select(AutoMapper.Mapper.Map<VisitFilter, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [ViewReports]
        public ActionResult GetReportData(VisitFilterWebModel filter)
        {
            var data = GetReportResults(filter, CurrentUser.ActiveClinicId);


            var arrangedData = data.GroupBy(d => d.nameVisit).Select(a => new VisitReportGrouped
                {
                    Items = a.ToList(),
                    Name = a.First().nameVisit,
                    Sum = a.Sum(s => s.chargeFee.HasValue ? (decimal)s.chargeFee.Value : 0)
                }).ToList();

            var serializer = new JavaScriptSerializer {MaxJsonLength = Int32.MaxValue};
            var result = new ContentResult
            {
                Content = serializer.Serialize(arrangedData),
                ContentType = "application/json"
            };
            return result;
        }

        private List<VisitReportItem> GetReportResults(VisitFilterWebModel filter, int clinicId)
        {
            var data = RapidVetUnitOfWork.VisitRepository.GetFilteredVisits(filter, clinicId);
            return data;
        }

        [ViewReports]
        [HttpGet]
        public JsonResult GetFilterData(int id)
        {
            var filter = RapidVetUnitOfWork.ClinicRepository.GetVisitFilter(id);
            var model = AutoMapper.Mapper.Map<VisitFilter, VisitFilterWebModel>(filter);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [ViewReports]
        [HttpPost]
        public JsonResult SaveFilter(VisitFilterWebModel model, string name)
        {
            VisitFilter filter;

            if (model.Id > 0)
            {
                filter = RapidVetUnitOfWork.ClinicRepository.GetVisitFilter(model.Id);
            }
            else
            {
                filter = new VisitFilter
                {
                    ClinicId = CurrentUser.ActiveClinicId,
                    Name = name,
                };
                RapidVetUnitOfWork.ClinicRepository.AddVisitFilter(filter);
            }

            AutoMapper.Mapper.Map(model, filter);

            if (!string.IsNullOrWhiteSpace(model.FromDate) && !string.IsNullOrWhiteSpace(model.ToDate))
            {
                filter.FromDate = StringUtils.ParseStringToDateTime(model.FromDate);
                filter.ToDate = StringUtils.ParseStringToDateTime(model.ToDate);
            }

            var status = RapidVetUnitOfWork.Save();

            if (status.Success)
            {
                SetSuccessMessage("הנתונים נשמרו בהצלחה");
            }
            else
            {
                SetErrorMessage();
            }

            return Json(status.Success);
        }

        [ViewReports]
        public ActionResult Print(VisitFilterWebModel filter)
        {
            var data = GetReportResults(filter, CurrentUser.ActiveClinicId);

            ViewBag.nameVisit = filter.nameVisitCb;
            ViewBag.datePerformed = filter.datePerformedCb;
            ViewBag.clientName = filter.clientNameCb;
            ViewBag.patientName = filter.patientNameCb;
            ViewBag.serial = filter.serialCb;
            ViewBag.address = filter.addressCb;
            ViewBag.homePhone = filter.homePhoneCb;
            ViewBag.cellPhone = filter.cellPhoneCb;
            ViewBag.animalKind = filter.animalKindCb;
            ViewBag.animalRace = filter.animalRaceCb;
            ViewBag.animalGender = filter.animalGenderCb;
            ViewBag.animalColor = filter.animalColorCb;
            ViewBag.animalAge = filter.animalAgeCb;
            ViewBag.sterilization = filter.sterilizationCb;
            ViewBag.electronicNum = filter.electronicNumCb;
            ViewBag.birthDate = filter.birthDateCb;
            ViewBag.licenseNum = filter.licenseNumCb;
            ViewBag.chargeFee = filter.chargeFeeCb;
            ViewBag.performingDoc = filter.performingDocCb;
            ViewBag.numOfUnits = filter.numOfUnitsCb;
            ViewBag.visitDesc = filter.visitDescCb;
            ViewBag.treatingDoctor = filter.treatingDoctorCb;
            ViewBag.receipt = filter.receiptCb;

            ViewBag.Colspan = (int)(ViewBag.nameVisit ? 1 : 0) +
                              (int)(ViewBag.datePerformed ? 1 : 0) +
                              (int)(ViewBag.clientName ? 1 : 0) +
                              (int)(ViewBag.patientName ? 1 : 0) +
                              (int)(ViewBag.serial ? 1 : 0) +
                              (int)(ViewBag.address ? 1 : 0) +
                              (int)(ViewBag.homePhone ? 1 : 0) +
                              (int)(ViewBag.cellPhone ? 1 : 0) +
                              (int)(ViewBag.animalKind ? 1 : 0) +
                              (int)(ViewBag.animalRace ? 1 : 0) +
                              (int)(ViewBag.animalGender ? 1 : 0) +
                              (int)(ViewBag.animalColor ? 1 : 0) +
                              (int)(ViewBag.animalAge ? 1 : 0) +
                              (int)(ViewBag.sterilization ? 1 : 0) +
                              (int)(ViewBag.electronicNum ? 1 : 0) +
                              (int)(ViewBag.birthDate ? 1 : 0) +
                              (int)(ViewBag.licenseNum ? 1 : 0) +
                              (int)(ViewBag.chargeFee ? 1 : 0) +
                              (int)(ViewBag.performingDoc ? 1 : 0) +
                              (int)(ViewBag.numOfUnits ? 1 : 0) +
                              (int)(ViewBag.visitDesc ? 1 : 0) +
                              (int)(ViewBag.treatingDoctor ? 1 : 0) +
                              (int)(ViewBag.receipt ? 1 : 0);

            ViewBag.fontSize = filter.printFontSize;

            return View(data);
        }

        [ViewReports]
        public ActionResult Stickers(VisitFilterWebModel filter, bool print = true)
        {
            var nvcResults = GetNvcResults(filter);
            var model = new List<string>();
            var template = GetTemplate(TreatmentPrintOutput.Stiker);
            foreach (var nvc in nvcResults)
            {
                model.Add(RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template));
            }
            ViewBag.ColumnCount = CurrentUser.ActiveClinicGroup.StikerColumns;
            ViewBag.RowCount = CurrentUser.ActiveClinicGroup.StikerRows;
            ViewBag.RowHeight = CurrentUser.ActiveClinicGroup.StikerRowHight;
            ViewBag.Margin = CurrentUser.ActiveClinicGroup.StickerSideMargin;
            ViewBag.MarginTop = CurrentUser.ActiveClinicGroup.StikerTopMargin;
            ViewBag.Print = print;
            var filterString = string.Format(
                "FromDate={0}&ToDate={1}&ClientId={2}&AnimalKindId={3}&FromYear={4}&FromMonth={5}&ToYear={6}&ToMonth={7}&DoctorId={8}&CategoryId={9}" +
                "&TreatmentId={10}&DiagnosisId={11}&FromSum={12}&ToSum={13}&VisitDescription={14}&MainComplaint={15}&printFontSize={16}&LetterId={17}"
                          ,
                    filter.FromDate, filter.ToDate, filter.ClientId, filter.AnimalKindId, filter.FromYear, filter.FromMonth, filter.ToYear, filter.ToMonth, filter.DoctorId,
                    filter.CategoryId, filter.TreatmentId, filter.DiagnosisId, filter.FromSum, filter.ToSum, filter.VisitDescription, filter.MainComplaint, filter.printFontSize);

            ViewBag.PrintUrl = string.Format("{0}?{1}&print={2}", Url.Action("Stickers", "TreatmentReports"), filterString, true);
            return View(model);
        }

        [ViewReports]
        public ActionResult Letters(VisitFilterWebModel filter, int LetterId, bool print = false)
        {
            var nvcResults = GetNvcResults(filter);
            var template = (RapidVetUnitOfWork.LetterTemplatesRepository.GetPersonalTemplate(LetterId)).Content;//GetTemplate(ClientPrintOutput.Letter);
            var model = new List<string>();
            foreach (var nvc in nvcResults)
            {
                nvc.Remove("\r\n");
                model.Add(RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template));
            }
            ViewBag.Print = print;

            var filterString = string.Format(
                "FromDate={0}&ToDate={1}&ClientId={2}&AnimalKindId={3}&FromYear={4}&FromMonth={5}&ToYear={6}&ToMonth={7}&DoctorId={8}&CategoryId={9}" +
                "&TreatmentId={10}&DiagnosisId={11}&FromSum={12}&ToSum={13}&VisitDescription={14}&MainComplaint={15}&printFontSize={16}&LetterId={17}"
                ,
                filter.FromDate, filter.ToDate, filter.ClientId, filter.AnimalKindId, filter.FromYear, filter.FromMonth, filter.ToYear, filter.ToMonth, filter.DoctorId,
                filter.CategoryId, filter.TreatmentId, filter.DiagnosisId, filter.FromSum, filter.ToSum, filter.VisitDescription, filter.MainComplaint, 
                filter.printFontSize,LetterId);
          
            ViewBag.PrintUrl = string.Format("{0}?{1}&print={2}", Url.Action("Letters", "TreatmentReports"), filterString, true);

            return View(model);
        }

        string emailTemplatePrefix = "<div style=\"position:relative;display:block;margin:auto;direction:rtl;\">";
        string emailTemplatePostfix = "</div>";

        [ViewReports]
        [HttpPost]
        public JsonResult Email(VisitFilterWebModel filter)
        {
            var nvcResults = GetNvcResults(filter);
            var template = GetTemplate(TreatmentPrintOutput.Email);
            var subject = CurrentUser.ActiveClinicGroup.TreatmentEmailSubject;

            var mailClient = getMailClient();
            string from = string.Format("{0}<{1}>", !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.EmailBusinessName) ? CurrentUser.ActiveClinicGroup.EmailBusinessName : CurrentUser.ActiveClinic.Name,
                                                CurrentUser.ActiveClinicGroup.EmailAddress);
            int messagesCounter = 0;
            foreach (var nvc in nvcResults)
            {
                int counter = 0;
                RestRequest request = new RestRequest();
                request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_RAPID_DOMAIN"], ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", from);
                request.AddParameter("subject", RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, subject));
                request.AddParameter("html", emailTemplatePrefix + RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template) + emailTemplatePostfix);
                request.Method = Method.POST;
                 
                var client = RapidVetUnitOfWork.ClientRepository.GetClient(int.Parse(nvc.Get("ClientId")));
                if (client.Emails.Any())
                {
                    StringBuilder emailAdress = new StringBuilder();

                    foreach (var email in client.Emails)
                    {
                        request.AddParameter("to", email.Name);
                        emailAdress.Append(email.Name);
                        emailAdress.Append("; ");
                        counter++;
                        messagesCounter++;
                    }

                    if (counter > 0)
                    {
                        var message = mailClient.Execute(request);
                        if ((int)message.StatusCode == 200)
                        {
                            RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, counter, MessageType.Email);

                            string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                Resources.Auditing.SendingModule, Resources.Auditing.VisitReport,
                                                                Resources.Auditing.ClientName, client.Name,
                                                                Resources.Auditing.Address, emailAdress.ToString(),
                                                                Resources.Auditing.MailSubject, RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, subject),
                                                                Resources.Auditing.ReturnCode, (int)message.StatusCode + " - " + message.StatusCode);

                            RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.MailSent, logMessage, "");
                        }
                    }
                }
            }

            SetSuccessMessage(string.Format(Resources.LetterTemplate.EmailSuccessMessage, messagesCounter, nvcResults.Count - messagesCounter, nvcResults.Count));

            return null;
        }

        [ViewReports]
        [HttpPost]
        public JsonResult Sms(VisitFilterWebModel filter)
        {
            var nvcResults = GetNvcResults(filter);
            var template = GetTemplate(TreatmentPrintOutput.Sms);
            var listLength = nvcResults.Count;
            var successCounter = 0;

            foreach (var nvc in nvcResults)
            {
                nvc.Remove("\r\n");
                var clientPhone = nvc.Get(getTemplateKeyWord("ClientMobilePhone"));
                if (!string.IsNullOrWhiteSpace(clientPhone))
                {
                    var message = RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template);
                    var sms = new Unicell.Sms(CurrentUser.ActiveClinicGroup.UnicellUserName,
                                              CurrentUser.ActiveClinicGroup.UnicellPassword, clientPhone, message,
                                              string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.SignatureSmsTemplate) ? "" : CurrentUser.ActiveClinicGroup.SignatureSmsTemplate);
                    var sendResult = sms.Send();
                    if (sendResult)
                    {
                        successCounter++;
                        RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, 1, MessageType.Sms);

                        string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                              Resources.Auditing.SendingModule, Resources.Auditing.VisitReport,
                                                              Resources.Auditing.ClientName, nvc.Get(getTemplateKeyWord("ClientName")),
                                                              Resources.Auditing.PhoneNumber, clientPhone,
                                                              Resources.Auditing.SmsContent, message,
                                                              Resources.Auditing.ReturnCode, sendResult);

                        RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.SmsSent, logMessage, "");                        
                    }
                }
                else
                {
                    listLength--;
                }
            }
            if (successCounter == listLength)
            {
                SetSuccessMessage(string.Format(Resources.LetterTemplate.SmsSuccessMessage, successCounter, nvcResults.Count() - successCounter < 0 ? 0 : nvcResults.Count() - successCounter, nvcResults.Count()));
                //SetSuccessMessage("ההודעות נשלחו בהצלחה");
            }
            else
            {
                SetErrorMessage("אירעה שגיאה בשליחת ההודעות");
            }

            return null;
        }

        [ExcelExport]
        public FileResult Excel(VisitFilterWebModel filter)
        {
            var data = GetReportResults(filter, CurrentUser.ActiveClinicId);

            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8 };
            var csv = new CsvWriter(writer, csvConfig);

            csv.WriteRecords(data);
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", "TreatmentsReport.csv");
        }

        private List<NameValueCollection> GetNvcResults(VisitFilterWebModel filter)
        {
            var data = GetReportResults(filter, CurrentUser.ActiveClinicId);
            var result = GetFormattedTemplates(data);
            return result;
        }

        private List<NameValueCollection> GetFormattedTemplates(IEnumerable<VisitReportItem> data)
        {
            var result = new List<NameValueCollection>();
            var date = DateTime.Now.ToString("dd/MM/yyyy");
            var doctor = CurrentUser.IsDoctor ? CurrentUser : CurrentUser.DefaultDrId.HasValue ? RapidVetUnitOfWork.UserRepository.GetItem(CurrentUser.DefaultDrId.Value) : null;

            foreach (var visitReportItem in data)
            {
                var nvc = new NameValueCollection
                {
                    {"\r\n", "<br />"},
                    {getTemplateKeyWord("ClinicAddress"),CurrentUser.ActiveClinic.Address},
                    {getTemplateKeyWord("ClinicName"), CurrentUser.ActiveClinic.Name},
                    {getTemplateKeyWord("ClinicPhone"), CurrentUser.ActiveClinic.Phone},
                    {getTemplateKeyWord("ClinicFax"), CurrentUser.ActiveClinic.Fax},
                    {getTemplateKeyWord("ClinicEmail"), CurrentUser.ActiveClinic.Email},
                    {getTemplateKeyWord("ClinicLogo"), "<img src=\"/ClinicGroups/LogoImage\" style=\"max-height: 150px;max-width: 150px;\">"},                   
                    {getTemplateKeyWord("Date"), date},                    
                    {getTemplateKeyWord("ClientAddress"), ""},
                    {getTemplateKeyWord("DoctorName"), ""},
                    {getTemplateKeyWord("ClientName"), ""},
                    {getTemplateKeyWord("ClientFirstName"), ""},
                    {getTemplateKeyWord("ClientLastName"), ""},
                    {getTemplateKeyWord("PatientName"), ""},
                    {getTemplateKeyWord("PriceListItemName"), ""},
                    {getTemplateKeyWord("Sterilization"), ""},
                    {getTemplateKeyWord("PatientSex"), ""},
                    {getTemplateKeyWord("PatientRace"), ""},
                    {getTemplateKeyWord("PatientAnimalKind"), ""},
                    {getTemplateKeyWord("ClientHomePhone"), ""},
                    {getTemplateKeyWord("ClientMobilePhone"), ""},
                    {getTemplateKeyWord("PatientColor"), ""},
                    {getTemplateKeyWord("PatientBirthDate"), ""},
                    {getTemplateKeyWord("Summery"), ""},
                    {getTemplateKeyWord("PatientChip"), ""},
                    {getTemplateKeyWord("ClientIdCardNumber"), ""},
                    {getTemplateKeyWord("ClientIdCard"), ""},
                    {"ClientId", ""},
                    {getTemplateKeyWord("ActiveDoctorName"), doctor != null ? doctor.Name : ""},
                    {getTemplateKeyWord("ActiveDoctorLicence"), doctor != null ? doctor.LicenceNumber : ""},
                    {getTemplateKeyWord("ClientReferer"), ""},
                    {getTemplateKeyWord("PatientAge"), ""},
                    {getTemplateKeyWord("ClientBalance"), ""},
                    {getTemplateKeyWord("ClientAge"), ""},
                    {getTemplateKeyWord("ClientTitle"), ""},
                    {getTemplateKeyWord("ClientCity"), ""},
                    {getTemplateKeyWord("ClientZip"), ""},
                    {getTemplateKeyWord("ClientBirthdate"), ""},
                    {getTemplateKeyWord("ClientStreetAndNumber"), ""},
                    {getTemplateKeyWord("ClientRecordNumber"), ""}                    
                };
                UpdateNvc(nvc, visitReportItem);
                result.Add(nvc);
            }
            return result;
        }

        private NameValueCollection UpdateNvc(NameValueCollection nvc, VisitReportItem item)
        {
            nvc.Set(getTemplateKeyWord("DoctorName"), item.performingDoc); // performing doctor name
            nvc.Set(getTemplateKeyWord("ClientFirstName"), item.clientFirstName); // client first name
            nvc.Set(getTemplateKeyWord("ClientLastName"), item.clientLastName); // client last name
            nvc.Set(getTemplateKeyWord("ClientName"), item.clientName); // client name
            nvc.Set(getTemplateKeyWord("ClientAddress"),item.address); // client addresss
            nvc.Set(getTemplateKeyWord("PatientName"), item.patientName); // patient name
            nvc.Set(getTemplateKeyWord("PriceListItemName"), item.nameVisit); // treatment name //prop - nameVisit
            nvc.Set(getTemplateKeyWord("Sterilization"), item.sterilization); // animal sterilized
            nvc.Set(getTemplateKeyWord("PatientSex"), item.animalGender); // animal gender
            nvc.Set(getTemplateKeyWord("PatientRace"), item.animalRace); // animal race
            nvc.Set(getTemplateKeyWord("PatientAnimalKind"), item.animalKind); // animal kind
            nvc.Set(getTemplateKeyWord("ClientHomePhone"), item.homePhone); // homePhone
            nvc.Set(getTemplateKeyWord("ClientMobilePhone"), item.cellPhone); // cellPhone
            nvc.Set(getTemplateKeyWord("PatientColor"), item.animalColor); // animal color
            nvc.Set(getTemplateKeyWord("PatientBirthDate"),item.birthDate.HasValue ? item.birthDate.Value.ToShortDateString() : "-"); // animal birthdate
            nvc.Set(getTemplateKeyWord("Summery"), item.visitDesc); // visit description
            nvc.Set(getTemplateKeyWord("PatientChip"), item.electronicNum); // electronic number
            nvc.Set(getTemplateKeyWord("ClientIdCardNumber"), item.serial); // client id number
            nvc.Set(getTemplateKeyWord("ClientIdCard"), item.serial); // client id number
            nvc.Set("ClientId", item.clientId.ToString()); // client id number
            nvc.Set(getTemplateKeyWord("ClientReferer"), item.clientReferer);
             nvc.Set(getTemplateKeyWord("PatientAge"), item.animalAge);
             nvc.Set(getTemplateKeyWord("ClientBalance"), item.clientBalance.HasValue ? item.clientBalance.Value.ToString() : "");
             nvc.Set(getTemplateKeyWord("ClientAge"), item.clientAge);
             nvc.Set(getTemplateKeyWord("ClientTitle"), item.clientTitle);
             nvc.Set(getTemplateKeyWord("ClientCity"), item.clientCity);
             nvc.Set(getTemplateKeyWord("ClientZip"), item.clientZip);
             nvc.Set(getTemplateKeyWord("ClientBirthdate"), item.clientBirthdate);
             nvc.Set(getTemplateKeyWord("ClientStreetAndNumber"), item.clientStreetAndNumber);
             nvc.Set(getTemplateKeyWord("ClientRecordNumber"), item.clientRecordNumber.ToString());      
            return nvc;
        }

        private string GetTemplate(TreatmentPrintOutput output)
        {
            var template = string.Empty;
            switch (output)
            {
                case TreatmentPrintOutput.Stiker:
                    template = CurrentUser.ActiveClinicGroup.TreatmentStikerTemplate;
                    break;
                case TreatmentPrintOutput.Letter:
                  //  template = CurrentUser.ActiveClinicGroup.PersonalLetterTemplate;
                    break;
                case TreatmentPrintOutput.Sms:
                    template = CurrentUser.ActiveClinicGroup.TreatmentSmsTemplate;
                    break;
                case TreatmentPrintOutput.Email:
                    template = CurrentUser.ActiveClinicGroup.TreatmentEmailTemplate;
                    break;
            }
            return template ?? string.Empty;
        }
    }
}