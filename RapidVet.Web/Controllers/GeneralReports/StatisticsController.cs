﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model.Visits;
using RapidVet.Web.Attributes;
using RapidVet.WebModels.DangerousDrugs;

namespace RapidVet.Web.Controllers
{
    public class StatisticsController : BaseController
    {

        /*-------------------------------Dangerous Drugs report --------------*/

        [ViewReports]
        public ActionResult DangerousDrugsReport()
        {
            return View();
        }

        [ViewReports]
        [HttpGet]
        public JsonResult GetDrugReport(String from, String to, int? medicationId)
        {
            var model = GetDrugs(from, to, medicationId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [ViewReports]
        public ActionResult PrintDrugReport(String from, String to, int? medicationId)
        {
            var model = GetDrugs(from, to, medicationId);
            return View(model);
        }

        private List<DangerousDrugReportItem> GetDrugs(String from, String to, int? medicationId)
        {
            DateTime fromDate, toDate;
            fromDate = String.IsNullOrEmpty(@from) ? DateTime.MinValue : DateTime.ParseExact(@from, "dd/MM/yyyy", null);
            if (String.IsNullOrEmpty(to))
            {
                toDate = DateTime.MaxValue;
            }
            else
            {
                toDate = DateTime.ParseExact(to, "dd/MM/yyyy", null);
                toDate = toDate.AddDays(1).AddTicks(-1);
            }
      

            var id = medicationId.HasValue ? medicationId.Value : 0;
            var res = RapidVetUnitOfWork.StatitsticsRepository.GetDangerousDrugs(fromDate, toDate, id,
                                                                                   CurrentUser.ActiveClinicGroupId).ToList();
            var drugs = AutoMapper.Mapper.Map<List<DangerousDrug>, List<DangerousDrugReportItem>>(res);
            return drugs;
        }

    }
}
