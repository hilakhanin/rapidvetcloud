﻿using System;
using System.Globalization;
using System.Web.Mvc;
using RapidVet.Web.Attributes;
using System.IO;
using System.Text;
using CsvHelper.Configuration;
using CsvHelper;

namespace RapidVet.Web.Controllers
{
    public class CalenderReportsController : BaseController
    {
        [ViewReports]
        public ActionResult EntriesReports(int? year)
        {
            if (!year.HasValue)
            {
                year = DateTime.Now.Year;
            }
            var entries = RapidVetUnitOfWork.CalenderRepository.GetCalenderEntriesByYear(CurrentUser.ActiveClinicId, year.Value);

            return View(entries);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromTime"></param>
        /// <param name="toTime"></param>
        /// <param name="ShowSelection">0 = show All, 1 = no shows, 2 = cancels</param>
        /// <param name="toPrint"></param>
        /// <returns></returns>
        [ViewReports]
        public ActionResult NoShowsReports(string fromTime, string toTime, int? showSelection, bool toPrint = false)
        {
            ViewBag.ToPrint = toPrint;
            var entries = getEntries(fromTime, toTime, showSelection);
            return View(entries);
        }

        private System.Collections.Generic.List<Model.FullCalender.CalenderEntry> getEntries(string fromTime, string toTime, int? showSelection)
        {
            DateTime from;
            DateTime to;
            if (string.IsNullOrWhiteSpace(fromTime) || string.IsNullOrWhiteSpace(toTime))
            {
                to = DateTime.Now;
                from = to.AddDays(-7);
            }
            else
            {
                from = DateTime.MinValue;
                to = DateTime.MinValue;
                var format = "dd/MM/yyyy";
                var unparsedFrom = fromTime;
                DateTime.TryParseExact(unparsedFrom, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out from);
                if (from == DateTime.MinValue)
                {
                    from = DateTime.Now;
                }
                var unparsedTo = toTime;
                DateTime.TryParseExact(unparsedTo, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out to);
                if (to == DateTime.MinValue)
                {
                    to = DateTime.Now;
                }
            }

            var entries = RapidVetUnitOfWork.CalenderRepository.GetNoShowAndCancelsCalenderEntriesByDateRange(CurrentUser.ActiveClinicId, from.Date, to.Date.AddDays(1), showSelection ?? 0);
            return entries;
        }

        [ExcelExport]
        public ActionResult DownloadCsv(string fromTime, string toTime, int? showSelection)
        {
            //DateTime from;
            //DateTime to;
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8 };
            var csv = new CsvWriter(writer, csvConfig);
            var entries = getEntries(fromTime, toTime, showSelection);
            if (entries == null || entries.Count == 0)
                return Redirect(Request.UrlReferrer.ToString());

            csv.WriteRecord(new
            {
                c1 = RapidVet.Resources.Calendar.Date,
                c2 = RapidVet.Resources.Calendar.Hour,
                c3 = RapidVet.Resources.Calendar.TreatedName,
                c4 = RapidVet.Resources.Calendar.QueueLengthInMinutes,
                c5 = RapidVet.Resources.Calendar.EventType
            });
            foreach (var i in entries)
                csv.WriteRecord(new
                {
                    c1 = i.Date.ToString("dd/MM/yyyy"),
                    c2 = i.Date.ToString("H:mm"),
                    c3 = i.Patient == null ? String.Empty : i.Patient.Name,
                    c4 = i.Duration,
                    c5 = i.IsCancelled ? "ביטול" : "אי הופעה"
                });
            writer.Flush();
            stream.Position = 0;
            return File(stream, "text/csv", "NoShowAndCancells.csv");
        }
    }
}