﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CsvHelper;
using CsvHelper.Configuration;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model.Visits;
using RapidVet.Web.Attributes;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.Visits;
using Newtonsoft.Json.Linq;
using RestSharp;
using RapidVet.Model.Clinics;
using RapidVet.Model.Tools;
using System.Threading;

namespace RapidVet.Web.Controllers.GeneralReports
{
    public class ClientsReportsController : BaseController
    {
        [ViewReports]
        public ActionResult Index()
        {
            ViewBag.Sms = IsSmsActive() && IsClientSmsExist();
            ViewBag.Stikers = IsStikerActive() && IsClientStikerExist();
            ViewBag.Email = IsEmailActive() && IsClientEmailExist();
            ViewBag.Letter = IsPersonalLetterExist();
            ViewBag.ClinicId = CurrentUser.ActiveClinicId;
            var objCities =  RapidVetUnitOfWork.ClientRepository.GetCitiesOfClinicById(CurrentUser.ActiveClinicId);
            ViewBag.CityOptions = objCities;

            return View();
        }

        [ViewReports]
        public ActionResult GetReportData(ClientFilter filter)
        {
            var data = GetReportResults(filter, CurrentUser.ActiveClinicId);
            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
            var result = new ContentResult
            {
                Content = serializer.Serialize(data),
                ContentType = "application/json"
            };
            return result;
        }

        private List<ClientReportItem> GetReportResults(ClientFilter filter, int clinicId)
        {
            var data = RapidVetUnitOfWork.ClientRepository.GetFilteredClients(filter, clinicId);
            return data;
        }

        [ViewReports]
        public ActionResult Print(ClientFilter filter)
        {
            var data = GetReportResults(filter, CurrentUser.ActiveClinicId);

            ViewBag.idCard = filter.idCardCb;
            ViewBag.clientName = filter.clientNameCb;
            ViewBag.patientName = filter.patientNameCb;
            ViewBag.address = filter.addressCb;
            ViewBag.workPhone = filter.workPhoneCb;
            ViewBag.homePhone = filter.homePhoneCb;
            ViewBag.cellPhone = filter.cellPhoneCb;
            ViewBag.animalKind = filter.animalKindCb;
            ViewBag.animalRace = filter.animalRaceCb;
            ViewBag.animalGender = filter.animalGenderCb;
            ViewBag.animalColor = filter.animalColorCb;
            ViewBag.animalAge = filter.animalAgeCb;
            ViewBag.email = filter.emailCb;
            ViewBag.electronicNum = filter.electronicNumCb;
            ViewBag.localId = filter.localIdCb;
            ViewBag.licenseNum = filter.licenseNumCb;
            ViewBag.referedBy = filter.referedByCb;
            ViewBag.clientStatus = filter.clientStatusCb;
            ViewBag.clientTitle = filter.clientTitleCb;
            ViewBag.treatingDoc = filter.treatingDocCb;
            ViewBag.clientCreateDate = filter.clientCreateDateCb;
            ViewBag.clientDetailsUpdatedDate = filter.clientDetailsUpdatedDateCb;
            ViewBag.lastTreatmentDate = filter.lastTreatmentDateCb;
            ViewBag.clientAge = filter.clientAgeCb;
            ViewBag.balance = filter.balanceCb;
            ViewBag.patientComments = filter.patientCommentsCb;
            ViewBag.externalVet = filter.externalVetCb;
            ViewBag.pureBred = filter.pureBredCb;
            ViewBag.sterilized = filter.sterilizedCb;
            ViewBag.SAGIRNum = filter.SAGIRNumCb;
            ViewBag.patientWeight = filter.patientWeightCb;
            ViewBag.owner_farm = filter.owner_farmCb;

            ViewBag.Colspan = (int)(ViewBag.idCard ? 1 : 0) +
                              (int)(ViewBag.clientName ? 1 : 0) +
                              (int)(ViewBag.patientName ? 1 : 0) +
                              (int)(ViewBag.address ? 1 : 0) +
                              (int)(ViewBag.workPhone ? 1 : 0) +
                              (int)(ViewBag.homePhone ? 1 : 0) +
                              (int)(ViewBag.cellPhone ? 1 : 0) +
                              (int)(ViewBag.animalKind ? 1 : 0) +
                              (int)(ViewBag.animalRace ? 1 : 0) +
                              (int)(ViewBag.animalGender ? 1 : 0) +
                              (int)(ViewBag.animalColor ? 1 : 0) +
                              (int)(ViewBag.animalAge ? 1 : 0) +
                              (int)(ViewBag.email ? 1 : 0) +
                              (int)(ViewBag.electronicNum ? 1 : 0) +
                              (int)(ViewBag.localId ? 1 : 0) +
                              (int)(ViewBag.licenseNum ? 1 : 0) +
                              (int)(ViewBag.referedBy ? 1 : 0) +
                              (int)(ViewBag.clientStatus ? 1 : 0) +
                              (int)(ViewBag.clientTitle ? 1 : 0) +
                              (int)(ViewBag.treatingDoc ? 1 : 0) +
                              (int)(ViewBag.clientCreateDate ? 1 : 0) +
                              (int)(ViewBag.clientDetailsUpdatedDate ? 1 : 0) +
                              (int)(ViewBag.lastTreatmentDate ? 1 : 0) +
                              (int)(ViewBag.clientAge ? 1 : 0) +
                              (int)(ViewBag.balance ? 1 : 0) +
                              (int)(ViewBag.patientComments ? 1 : 0) +
                              (int)(ViewBag.externalVet ? 1 : 0) +
                              (int)(ViewBag.sterilized ? 1 : 0) +
                              (int)(ViewBag.pureBred ? 1 : 0) +
                              (int)(ViewBag.SAGIRNum ? 1 : 0) +
                              (int)(ViewBag.patientWeight ? 1 : 0) +
                              (int)(ViewBag.owner_farm ? 1 : 0);

            return View(data);
        }

        [ViewReports]
        public ActionResult Stickers(ClientFilter filter, bool print = true)
        {
            var doctor = CurrentUser.IsDoctor ? CurrentUser : CurrentUser.DefaultDrId.HasValue ? RapidVetUnitOfWork.UserRepository.GetItem(CurrentUser.DefaultDrId.Value) : null;
            var nvcResults = GetNvcResults(filter, CurrentUser.ActiveClinicId, doctor != null ? doctor.Id : 0);
            var model = new List<string>();
            var template = GetTemplate(ClientPrintOutput.Stiker, CurrentUser.ActiveClinicGroupId);
            foreach (var nvc in nvcResults)
            {
                model.Add(RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template));
            }
            ViewBag.ColumnCount = CurrentUser.ActiveClinicGroup.StikerColumns;
            ViewBag.RowCount = CurrentUser.ActiveClinicGroup.StikerRows;
            ViewBag.RowHeight = CurrentUser.ActiveClinicGroup.StikerRowHight;
            ViewBag.Margin = CurrentUser.ActiveClinicGroup.StickerSideMargin;
            ViewBag.MarginTop = CurrentUser.ActiveClinicGroup.StikerTopMargin;
            ViewBag.Print = print;
            var filterString = string.Format(
                        "OnlyNonFilteredClients={0}&OnlyClientsWithoutVisits={1}&ClientStatusId={2}&CityId={3}&EmailKeyWord={4}&VisitedFrom={5}&VisitedTo={6}&CreatedFrom={7}&CreatedTo={8}&AgeFrom={9}" +
                        "&AgeTo={10}&TreatingDoctorId={11}&ExternalVetKeyWord={12}&OnlyWithOrWithoutFutureAppointment={13}&NoReference={14}&ReferredByKeyWord={15}&OnlyWithNoVisitFrom={16}&AnimalKindId={17}" +
                        "&PatientActiveStatus={18}&AnimalRaceId={19}&AnimalColorId={20}&PureBred={21}&AnimalGender={22}&Sterilized={23}&Fertilized={24}&TreatedFrom={25}&TreatedTo={26}" +
                        "&OnlyWithNoTreatmentFrom={27}&OnlyNotVaccined={28}&OnlyWithElectornicNumber={29}&OnlyGoneThroughTreatment={30}&TreatmentId={31}&OnlyWithPositiveBalance={32}&FromBalance={33}" +
                        "&ToBalance={34}&PriceListId={35}&PaymentsNumMoreThan={36}&PaymentFrom={37}&PaymentTo={38}",

            filter.OnlyNonFilteredClients, filter.OnlyClientsWithoutVisits, filter.ClientStatusId, filter.CityId, filter.EmailKeyWord, filter.VisitedFrom, filter.VisitedTo, filter.CreatedFrom, filter.CreatedTo,
            filter.AgeFrom, filter.AgeTo, filter.TreatingDoctorId, filter.ExternalVetKeyWord, filter.OnlyWithOrWithoutFutureAppointment, filter.NoReference, filter.ReferredByKeyWord, filter.OnlyWithNoVisitFrom, filter.AnimalKindId,
            filter.PatientActiveStatus, filter.AnimalRaceId, filter.AnimalColorId, filter.PureBred, filter.AnimalGender, filter.Sterilized, filter.Fertilized, filter.TreatedFrom, filter.TreatedTo,
            filter.OnlyWithNoTreatmentFrom, filter.OnlyNotVaccined, filter.OnlyWithElectornicNumber, filter.OnlyGoneThroughTreatment, filter.TreatmentId, filter.OnlyWithPositiveBalance, filter.FromBalance, filter.ToBalance,
            filter.PriceListId, filter.PaymentsNumMoreThan, filter.PaymentFrom, filter.PaymentTo);

            ViewBag.PrintUrl = string.Format("{0}?{1}&print={2}", Url.Action("Stickers", "ClientsReports"), filterString, true);
            return View(model);
        }

        [ViewReports]
        public ActionResult Letters(ClientFilter filter, int LetterId, bool print = false)
        {
            var doctor = CurrentUser.IsDoctor ? CurrentUser : CurrentUser.DefaultDrId.HasValue ? RapidVetUnitOfWork.UserRepository.GetItem(CurrentUser.DefaultDrId.Value) : null;
            var nvcResults = GetNvcResults(filter, CurrentUser.ActiveClinicId, doctor != null ? doctor.Id : 0);
                    
            var template = (RapidVetUnitOfWork.LetterTemplatesRepository.GetPersonalTemplate(LetterId)).Content;//GetTemplate(ClientPrintOutput.Letter);
            var model = new List<string>();

            foreach (var nvc in nvcResults)
            {
                nvc.Remove("\r\n"); //to avoid double line breakes
                model.Add(RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template));
            }
            ViewBag.Print = print;
            var filterString = string.Format(
                        "OnlyNonFilteredClients={0}&OnlyClientsWithoutVisits={1}&ClientStatusId={2}&CityId={3}&EmailKeyWord={4}&VisitedFrom={5}&VisitedTo={6}&CreatedFrom={7}&CreatedTo={8}&AgeFrom={9}" +
                        "&AgeTo={10}&TreatingDoctorId={11}&ExternalVetKeyWord={12}&OnlyWithOrWithoutFutureAppointment={13}&NoReference={14}&ReferredByKeyWord={15}&OnlyWithNoVisitFrom={16}&AnimalKindId={17}" +
                        "&PatientActiveStatus={18}&AnimalRaceId={19}&AnimalColorId={20}&PureBred={21}&AnimalGender={22}&Sterilized={23}&Fertilized={24}&TreatedFrom={25}&TreatedTo={26}" +
                        "&OnlyWithNoTreatmentFrom={27}&OnlyNotVaccined={28}&OnlyWithElectornicNumber={29}&OnlyGoneThroughTreatment={30}&TreatmentId={31}&OnlyWithPositiveBalance={32}&FromBalance={33}" +
                        "&ToBalance={34}&PriceListId={35}&PaymentsNumMoreThan={36}&PaymentFrom={37}&PaymentTo={38}&LetterId={39}",

            filter.OnlyNonFilteredClients, filter.OnlyClientsWithoutVisits, filter.ClientStatusId, filter.CityId, filter.EmailKeyWord, filter.VisitedFrom, filter.VisitedTo, filter.CreatedFrom, filter.CreatedTo,
            filter.AgeFrom, filter.AgeTo, filter.TreatingDoctorId, filter.ExternalVetKeyWord, filter.OnlyWithOrWithoutFutureAppointment, filter.NoReference, filter.ReferredByKeyWord, filter.OnlyWithNoVisitFrom, filter.AnimalKindId,
            filter.PatientActiveStatus, filter.AnimalRaceId, filter.AnimalColorId, filter.PureBred, filter.AnimalGender, filter.Sterilized, filter.Fertilized, filter.TreatedFrom, filter.TreatedTo,
            filter.OnlyWithNoTreatmentFrom, filter.OnlyNotVaccined, filter.OnlyWithElectornicNumber, filter.OnlyGoneThroughTreatment, filter.TreatmentId, filter.OnlyWithPositiveBalance, filter.FromBalance, filter.ToBalance,
            filter.PriceListId, filter.PaymentsNumMoreThan, filter.PaymentFrom, filter.PaymentTo, LetterId);

            ViewBag.PrintUrl = string.Format("{0}?{1}&print={2}", Url.Action("Letters", "ClientsReports"), filterString, true);
            return View(model);
        }


        string emailTemplatePrefix = "<div style=\"position:relative;display:block;margin:auto;direction:rtl;\">";
        string emailTemplatePostfix = "</div>";

        [ViewReports]
        [HttpPost]
        public JsonResult Email(ClientFilter filter)
        {
            var doctor = CurrentUser.IsDoctor ? CurrentUser : CurrentUser.DefaultDrId.HasValue ? RapidVetUnitOfWork.UserRepository.GetItem(CurrentUser.DefaultDrId.Value) : null;
            var nvcResults = GetNvcResults(filter, CurrentUser.ActiveClinicId, doctor != null ? doctor.Id : 0);

            var template = GetTemplate(ClientPrintOutput.Email, CurrentUser.ActiveClinicGroupId);
            var subject = CurrentUser.ActiveClinicGroup.ClientsEmailSubject;

           // var messages = new List<MailMessage>();
            var mailClient = getMailClient();
            string from = string.Format("{0}<{1}>", !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.EmailBusinessName) ? CurrentUser.ActiveClinicGroup.EmailBusinessName : CurrentUser.ActiveClinic.Name,
                                                CurrentUser.ActiveClinicGroup.EmailAddress);

            foreach (var nvc in nvcResults)
            {
                int counter = 0;
                RestRequest request = new RestRequest();
                request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_RAPID_DOMAIN"], ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", from);
                request.AddParameter("subject", RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, subject));
                request.AddParameter("html", emailTemplatePrefix + RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template) + emailTemplatePostfix);
                request.Method = Method.POST;

                var client = RapidVetUnitOfWork.ClientRepository.GetClient(int.Parse(nvc.Get("ClientId")));
                if (client.Emails.Any())
                {
                    StringBuilder emailAdress = new StringBuilder();

                    foreach (var email in client.Emails)
                    {
                        request.AddParameter("to", email.Name);
                        emailAdress.Append(email.Name);
                        emailAdress.Append("; ");
                        counter++;
                    }

                    if (counter > 0)
                    {
                        var message = mailClient.Execute(request);
                        if ((int)message.StatusCode == 200)
                        {
                            RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, counter, MessageType.Email);
                            string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                  Resources.Auditing.SendingModule, Resources.Auditing.ClientsReport,
                                                                  Resources.Auditing.ClientName, client.Name,
                                                                  Resources.Auditing.Address, emailAdress.ToString(),
                                                                  Resources.Auditing.MailSubject, RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, subject),
                                                                  Resources.Auditing.ReturnCode, (int)message.StatusCode + " - " + message.StatusCode);

                            RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.MailSent, logMessage, "");
                               
                        }
                    }
                }
            }

          
            SetSuccessMessage("שליחת המיילים התבצעה בהצלחה");

            return null;
        }

        [ViewReports]
        [HttpPost]
        public JsonResult CountSmsMessages(ClientFilter filter)
        {
            var doctor = CurrentUser.IsDoctor ? CurrentUser : CurrentUser.DefaultDrId.HasValue ? RapidVetUnitOfWork.UserRepository.GetItem(CurrentUser.DefaultDrId.Value) : null;
            var nvcResults = GetNvcResults(filter, CurrentUser.ActiveClinicId, doctor != null ? doctor.Id : 0);

            var template = GetTemplate(ClientPrintOutput.Sms, CurrentUser.ActiveClinicGroupId);
            var listLength = nvcResults.Count;

            foreach (var nvc in nvcResults)
            {
                nvc.Remove("\r\n");
                var clientPhone = nvc.Get(getTemplateKeyWord("ClientMobilePhone"));
                var clientName = nvc.Get(getTemplateKeyWord("ClientName"));
                if (string.IsNullOrWhiteSpace(clientPhone))
                {
                    listLength--;
                }
            }

            return Json(listLength, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Sms(ClientFilter filter)
        {           
            string jobGuid = Guid.NewGuid().ToString();
            var jobId = Hangfire.BackgroundJob.Schedule(() => SendSmsMessages(filter, CurrentUser.Id, CurrentUser.ActiveClinicGroupId,
                CurrentUser.ActiveClinicId, Hangfire.JobCancellationToken.Null, jobGuid), TimeSpan.FromSeconds(15));

            ClinicBackgroundJob job = new ClinicBackgroundJob()
            {
                BackgroundJobType = BackgroundJobType.SMS,
                ClinicId = CurrentUser.ActiveClinicId,
                InitiatingUserId = CurrentUser.Id,
                JobGuid = jobGuid,
                JobId = jobId,
                JobModule = Resources.Auditing.ClientsReport,
                StartTime = DateTime.Now,
                WasCancelled = false
            };

            RapidVetUnitOfWork.BackgroundJobsRepository.Create(job);
            SetSuccessMessage(Resources.LetterTemplate.SmsGeneralSentAction);

            return null;
        }

        public void SendSmsMessages(ClientFilter filter, int userId, int clinicGroupId, int clinicId, Hangfire.IJobCancellationToken cancellationToken, string jobGuid)
        {
            Model.Users.User CurrentUser = RapidVetUnitOfWork.UserRepository.GetItem(userId);
            var activeClinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(clinicGroupId);
            var activeClinic = RapidVetUnitOfWork.ClinicRepository.GetItem(clinicId);
            CurrentUser.UpdateClinicGroup(activeClinicGroup);
            CurrentUser.UpdateClinic(activeClinic);          
            var nvcResults = GetNvcResults(filter, clinicId, userId);
            var template = GetTemplate(ClientPrintOutput.Sms, clinicGroupId);
            var listLength = nvcResults.Count;
            var successCounter = 0;

            var clinicJob = RapidVetUnitOfWork.BackgroundJobsRepository.GetBackgroundJobByGuid(jobGuid);

            if (clinicJob.Processed == 0 && clinicJob.Total == 0)
            {
                foreach (var nvc in nvcResults)
                {
                    try
                    {
                        cancellationToken.ThrowIfCancellationRequested();
                        Thread.Sleep(1000);

                        nvc.Remove("\r\n");
                        var clientPhone = nvc.Get(getTemplateKeyWord("ClientMobilePhone"));
                        var clientName = nvc.Get(getTemplateKeyWord("ClientName"));
                        if (!string.IsNullOrWhiteSpace(clientPhone))
                        {
                            var message = RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template);
                            var sms = new Unicell.Sms(CurrentUser.ActiveClinicGroup.UnicellUserName,
                                                      CurrentUser.ActiveClinicGroup.UnicellPassword, clientPhone, message,
                                                      string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.SignatureSmsTemplate) ? "" : CurrentUser.ActiveClinicGroup.SignatureSmsTemplate);
                            var sendResult = sms.Send();
                            if (sendResult)
                            {
                                RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, 1, MessageType.Sms);

                                string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                      Resources.Auditing.SendingModule, Resources.Auditing.ClientsReport,
                                                                      Resources.Auditing.ClientName, clientName,
                                                                      Resources.Auditing.PhoneNumber, clientPhone,
                                                                      Resources.Auditing.SmsContent, message,
                                                                      Resources.Auditing.ReturnCode, sendResult);

                                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.SmsSent, logMessage, "");


                                successCounter++;
                                clinicJob.Processed = successCounter;
                                clinicJob.Total = listLength;
                                RapidVetUnitOfWork.BackgroundJobsRepository.Update(clinicJob);
                            }
                        }
                        else
                        {
                            listLength--;
                        }
                    }
                    catch (OperationCanceledException)
                    {
                        // Console.WriteLine("Cancellation requested, exiting...");
                        throw;
                    }
                }

                clinicJob.Processed = successCounter;
                clinicJob.Total = listLength;
                clinicJob.EndTime = DateTime.Now;
                RapidVetUnitOfWork.BackgroundJobsRepository.Update(clinicJob);
            }
            //if (successCounter == listLength)
            //{
            //    SetSuccessMessage(string.Format(Resources.LetterTemplate.SmsSuccessMessage, successCounter, nvcResults.Count() - successCounter < 0 ? 0 : nvcResults.Count() - successCounter, nvcResults.Count()));
            //}
            //else
            //{
            //    SetErrorMessage("אירעה שגיאה בשליחת ההודעות");
            //}

            //return null;
        }



        [ExcelExport]
        public FileResult Excel(ClientFilter filter)
        {
            var data = GetReportResults(filter, CurrentUser.ActiveClinicId);

            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8 };
            var csv = new CsvWriter(writer, csvConfig);

            csv.WriteRecords(data);
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", "ClientsReport.csv");
        }

        private List<NameValueCollection> GetNvcResults(ClientFilter filter, int clinicId, int doctorId)
        {
            var data = GetReportResults(filter, clinicId);
            var result = GetFormattedTemplates(data, clinicId, doctorId);
            return result;
        }

        private List<NameValueCollection> GetFormattedTemplates(IEnumerable<ClientReportItem> data, int clinicId, int doctorId)
        {
            var clinic = RapidVetUnitOfWork.ClinicRepository.GetClinic(clinicId);
            var result = new List<NameValueCollection>();
            var date = DateTime.Now.ToString("dd/MM/yyyy");

            Model.Users.User doctor = null;
            if(doctorId > 0)
            {
                doctor = RapidVetUnitOfWork.UserRepository.GetItem(doctorId);
            }
            //var doctor = CurrentUser.IsDoctor ? CurrentUser : CurrentUser.DefaultDrId.HasValue ? RapidVetUnitOfWork.UserRepository.GetItem(CurrentUser.DefaultDrId.Value) : null;

            foreach (var visitReportItem in data)
            {
                var nvc = new NameValueCollection
                {
                    {"\r\n", "<br />"},
                    {getTemplateKeyWord("ClinicAddress"),clinic.Address},
                    {getTemplateKeyWord("ClinicName"), clinic.Name},
                    {getTemplateKeyWord("ClinicPhone"), clinic.Phone},
                    {getTemplateKeyWord("ClinicFax"), clinic.Fax},
                    {getTemplateKeyWord("ClinicEmail"), clinic.Email},
                    {getTemplateKeyWord("ClinicLogo"), "<img src=\"/ClinicGroups/LogoImage\" style=\"max-height: 150px;max-width: 150px;\">"},
                    {getTemplateKeyWord("Date"), date},                    
                    {getTemplateKeyWord("ClientAddress"), ""},
                    {getTemplateKeyWord("DoctorName"), ""},
                    {getTemplateKeyWord("ClientName"), ""},
                    {getTemplateKeyWord("PatientName"), ""},
                    {getTemplateKeyWord("PriceListItemName"), ""},//***
                    {getTemplateKeyWord("Sterilization"), ""},
                    {getTemplateKeyWord("PatientSex"), ""},
                    {getTemplateKeyWord("PatientRace"), ""},
                    {getTemplateKeyWord("PatientAnimalKind"), ""},
                    {getTemplateKeyWord("ClientWorkPhone"), ""},
                    {getTemplateKeyWord("ClientHomePhone"), ""},
                    {getTemplateKeyWord("ClientMobilePhone"), ""},
                    {getTemplateKeyWord("PatientColor"), ""},
                    {getTemplateKeyWord("PatientBirthDate"), ""},//***
                    {getTemplateKeyWord("Summery"), ""},//**
                    {getTemplateKeyWord("PatientChip"), ""},
                    {getTemplateKeyWord("ClientIdCardNumber"), ""},
                    {getTemplateKeyWord("ClientIdCard"), ""},
                    {getTemplateKeyWord("ClientZip"), ""},
                    {"ClientId", ""},
                    {getTemplateKeyWord("ActiveDoctorName"), doctor != null ? doctor.Name : ""},
                    {getTemplateKeyWord("ActiveDoctorLicence"), doctor != null ? doctor.LicenceNumber : ""},
                    {getTemplateKeyWord("PatientAge"), ""},
                    {getTemplateKeyWord("ClientCity"), ""},
                    {getTemplateKeyWord("ClientZip"), ""},
                    {getTemplateKeyWord("ClientBirthdate"), ""},
                    {getTemplateKeyWord("ClientStreetAndNumber"), ""}            
                };
                UpdateNvc(nvc, visitReportItem);
                result.Add(nvc);
            }
            return result;
        }

        private NameValueCollection UpdateNvc(NameValueCollection nvc, ClientReportItem item)
        {
            nvc.Set(getTemplateKeyWord("DoctorName"), item.treatingDoc); // performing doctor name
            nvc.Set(getTemplateKeyWord("ClientFirstName"), item.clientFirstName); // client first name
            nvc.Set(getTemplateKeyWord("ClientLastName"), item.clientLastName); // client last name
            nvc.Set(getTemplateKeyWord("ClientName"), item.clientName); // client full name
            nvc.Set(getTemplateKeyWord("ClientAddress"), item.address); //client's address
            nvc.Set(getTemplateKeyWord("ClientZip"), item.zipcode); //client's address
            nvc.Set(getTemplateKeyWord("PatientName"), item.patientName); // patient name
            nvc.Set(getTemplateKeyWord("Sterilization"), item.sterilized); // animal sterilized
            nvc.Set(getTemplateKeyWord("PatientSex"), item.animalGender); // animal gender
            nvc.Set(getTemplateKeyWord("PatientRace"), item.animalRace); // animal race
            nvc.Set(getTemplateKeyWord("PatientAnimalKind"), item.animalKind); // animal kind
            nvc.Set(getTemplateKeyWord("ClientWorkPhone"), item.workPhone); // workPhone
            nvc.Set(getTemplateKeyWord("ClientHomePhone"), item.homePhone); // homePhone
            nvc.Set(getTemplateKeyWord("ClientMobilePhone"), item.cellPhone); // cellPhone
            nvc.Set(getTemplateKeyWord("PatientColor"), item.animalColor); // animal color
            nvc.Set(getTemplateKeyWord("ClientTitle"), item.clientTitle); // visit description
            nvc.Set(getTemplateKeyWord("PatientChip"), item.electronicNum); // electronic number
            nvc.Set(getTemplateKeyWord("ClientIdCardNumber"), item.idCard); // client id number
            nvc.Set(getTemplateKeyWord("ClientIdCard"), item.idCard); // client id number
            nvc.Set(getTemplateKeyWord("ClientReferer"), item.referedBy); // refered by
            nvc.Set(getTemplateKeyWord("ClientStatus"), item.clientStatus); // client status
            nvc.Set(getTemplateKeyWord("Comment"), item.patientComments); // patient comments
            nvc.Set(getTemplateKeyWord("ClientRecordNumber"), item.localId.ToString()); // mispar tik lakoah
            nvc.Set(getTemplateKeyWord("ClientBalance"), item.balance.HasValue ? item.balance.Value.ToString() : null); // client balance
            nvc.Set("ClientId", item.clientId.ToString()); // client id number
            nvc.Set(getTemplateKeyWord("PatientAge"), item.animalAge);
            nvc.Set(getTemplateKeyWord("ClientCity"), item.client_city);            
            nvc.Set(getTemplateKeyWord("ClientBirthdate"), item.client_birthdate);
            nvc.Set(getTemplateKeyWord("ClientStreetAndNumber"), item.client_street_and_number);   
            nvc.Set(getTemplateKeyWord("ClientAge"), item.clientAge); 
            return nvc;
        }

        private string GetTemplate(ClientPrintOutput output, int clinicGroupId)
        {
            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(clinicGroupId);
            var template = string.Empty;
            switch (output)
            {
                case ClientPrintOutput.Stiker:
                    template = clinicGroup.ClientsStikerTemplate;
                    break;
                case ClientPrintOutput.Letter:
                    //template = CurrentUser.ActiveClinicGroup.PersonalLetterTemplate;
                    break;
                case ClientPrintOutput.Sms:
                    template = clinicGroup.ClientsSmsTemplate;
                    break;
                case ClientPrintOutput.Email:
                    template = clinicGroup.ClientsEmailTemplate;
                    break;
            }
            return template ?? string.Empty;
        }
    }
}