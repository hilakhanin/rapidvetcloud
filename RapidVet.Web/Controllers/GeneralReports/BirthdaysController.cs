﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Patients;
using RapidVet.Web.Attributes;
using RapidVet.WebModels.GeneralReports;
using RestSharp;
using RapidVet.Model.Clinics;

namespace RapidVet.Web.Controllers
{
    public class BirthdaysController : BaseController
    {
        //
        // GET: /Birthdays/

        [ViewReports]
        public ActionResult Index()
        {
            ViewBag.LettersActive = LettersActive();
            ViewBag.EmailActive = EmailActive();
            ViewBag.SmsActive = SmsActive();
            ViewBag.StickerActive = StickerActive();
            return View();
        }

        [ViewReports]
        public JsonResult GetBirthdays(String birthday, Boolean allMonth)
        {
            var model = BirthdaysList(birthday, allMonth, null);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [ViewReports]
        public ActionResult PrintBirthdays(String birthday, Boolean allMonth, int? statusId)
        {
            var model = BirthdaysList(birthday, allMonth, statusId);
            return View(model);
        }

        [ViewReports]
        public ActionResult BirthdayLetters(String birthday, bool allMonth, int? statusId, int LetterId, bool print = true)
        {
            var template = (RapidVetUnitOfWork.LetterTemplatesRepository.GetPersonalTemplate(LetterId)).Content;
            var model = GetFormattedTemplates(birthday, allMonth, statusId, template);

            ViewBag.Print = print;
            ViewBag.PrintUrl = Url.Action("BirthdayLetters",
                                          new
                                              {
                                                  birthday = birthday,
                                                  allMonth = allMonth,
                                                  statusId = statusId,
                                                  LetterId = LetterId,
                                                  print = true
                                              });
            return View(model);
        }

        [ViewReports]
        public ActionResult BirthdayStickers(String birthday, bool allMonth, int? statusId, bool print = true)
        {
            var model = GetFormattedTemplates(birthday, allMonth, statusId, GetTemplate(BirthdayContactPrintOutput.Sticker));


            ViewBag.ColumnCount = CurrentUser.ActiveClinicGroup.StikerColumns;
            ViewBag.RowCount = CurrentUser.ActiveClinicGroup.StikerRows;
            ViewBag.RowHeight = CurrentUser.ActiveClinicGroup.StikerRowHight;
            ViewBag.Margin = CurrentUser.ActiveClinicGroup.StickerSideMargin;
            ViewBag.MarginTop = CurrentUser.ActiveClinicGroup.StikerTopMargin;

            ViewBag.Print = print;
            ViewBag.PrintUrl = Url.Action("BirthdayStickers",
                                          new
                                          {
                                              birthday = birthday,
                                              allMonth = allMonth,
                                              statusId = statusId,
                                              print = true
                                          });
            return View(model);
        }


        string emailTemplatePrefix = "<div style=\"position:relative;display:block;margin:auto;direction:rtl;\">";
        string emailTemplatePostfix = "</div>";


        [ViewReports]
        public ActionResult BirthdayEmails(String birthday, bool allMonth, int? statusId)
        {
            var patients = BirthdaysList(birthday, allMonth, statusId);
            var mailClient = getMailClient();
            string from = string.Format("{0}<{1}>", !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.EmailBusinessName) ? CurrentUser.ActiveClinicGroup.EmailBusinessName : CurrentUser.ActiveClinic.Name,
                                                CurrentUser.ActiveClinicGroup.EmailAddress);
            int messagesCounter = 0;
            var template = GetTemplate(BirthdayContactPrintOutput.Email);
            var subjectTemplate = GetTemplate(BirthdayContactPrintOutput.EmailSubject);
            var nvc = GetNVC();

            foreach (var patient in patients)
            {
                var client = RapidVetUnitOfWork.ClientRepository.GetClient(patient.ClientId);

                int counter = 0;
                RestRequest request = new RestRequest();
                request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_RAPID_DOMAIN"], ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", from);
                request.AddParameter("subject", GetMessage(nvc, subjectTemplate, patient));
                request.AddParameter("html", emailTemplatePrefix + GetMessage(nvc, template, patient) + emailTemplatePostfix);
                request.Method = Method.POST;
               
                StringBuilder emailAdress = new StringBuilder();
                
                foreach (var email in client.Emails)
                {
                    request.AddParameter("to", email.Name);
                    emailAdress.Append(email.Name);
                    emailAdress.Append("; ");
                    counter++;
                }
                if (counter > 0)
                {
                    var message = mailClient.Execute(request);
                    if ((int)message.StatusCode == 200)
                    {
                        RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, counter, MessageType.Email);
                    
                        string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                 Resources.Auditing.SendingModule, Resources.Auditing.Birthdays,
                                                                 Resources.Auditing.ClientName, client.Name,
                                                                 Resources.Auditing.Address, emailAdress.ToString(),
                                                                 Resources.Auditing.MailSubject, GetMessage(nvc, subjectTemplate, patient),
                                                                 Resources.Auditing.ReturnCode, (int)message.StatusCode + " - " + message.StatusCode);

                        RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.MailSent, logMessage, "");
                        messagesCounter += counter;
                    }
                }
            }
            
           // SetSuccessMessage("שליחת המיילים התבצעה בהצלחה");
            SetSuccessMessage(string.Format(Resources.LetterTemplate.EmailSuccessMessage, messagesCounter, patients.Count() - messagesCounter < 0 ? 0 : patients.Count() - messagesCounter, patients.Count()));
            
            return null;
        }

        [ViewReports]
        public ActionResult BirthdaySms(String birthday, bool allMonth, int? statusId)
        {
            var patients = BirthdaysList(birthday, allMonth, statusId);

            var template = GetTemplate(BirthdayContactPrintOutput.Sms);           
            var nvc = GetNVC();

            var totalPhones = 0;
            var successCounter = 0;
            foreach (var patient in patients)
            {
                var message = GetMessage(nvc, template, patient);
                message = message.Replace("<br />", "\r\n");
                var client = RapidVetUnitOfWork.ClientRepository.GetClient(patient.ClientId);
                var phones = client.Phones.Where(p => p.PhoneTypeId == CellPhone());
                totalPhones += phones.Count();

                foreach (var phone in phones)
                {
                    var sms = new Unicell.Sms(CurrentUser.ActiveClinicGroup.UnicellUserName,
                                       CurrentUser.ActiveClinicGroup.UnicellPassword, phone.PhoneNumber, message, string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.SignatureSmsTemplate) ? "" : CurrentUser.ActiveClinicGroup.SignatureSmsTemplate);
                    var sendResult = sms.Send();
                    if (sendResult)
                    {
                        RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, 1, MessageType.Sms);

                        string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                               Resources.Auditing.SendingModule, Resources.Auditing.Birthdays,
                                                               Resources.Auditing.ClientName, client.Name,
                                                               Resources.Auditing.PhoneNumber, phone.PhoneNumber,
                                                               Resources.Auditing.SmsContent, message,
                                                               Resources.Auditing.ReturnCode, sendResult);

                        RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.SmsSent, logMessage, "");

                        successCounter++;
                    }
                }

            }
            if (successCounter > 0 || totalPhones == 0)
            {
                SetSuccessMessage(string.Format(Resources.LetterTemplate.SmsSuccessMessage, successCounter, totalPhones - successCounter < 0 ? 0 : totalPhones - successCounter, totalPhones));
                //SetSuccessMessage(successCounter+" הודעות נשלחו בהצלחה");
            }
            else
            {
                SetErrorMessage("אירעה שגיאה בשליחת ההודעות");
            }
            return null;
        }

        private IEnumerable<Patient> GetPatients(String birthday, Boolean allMonth, int? statusId)
        {
            var date = StringUtils.ParseStringToDateTime(birthday);
            var month = date.Month;
            int? day = date.Day;
            if (allMonth)
            {
                day = null;
            }

            var patients = RapidVetUnitOfWork.GeneralReportsRepository.GetBirthdays(CurrentUser.ActiveClinicId, month,
                day);
            if (statusId.HasValue)
            {
                patients = patients.Where(p => p.Client.StatusId == statusId.Value).ToList();
            }
            return patients;
        }

        private List<String> GetFormattedTemplates(String birthday, Boolean allMonth, int? statusId, string template)
        {
            var data = BirthdaysList(birthday, allMonth, statusId);

            var result = new List<string>();

            //var template = GetTemplate(output);
            var date = DateTime.Now.ToShortDateString();
            var nvc = GetNVC();

            foreach (var patient in data)
            {
                result.Add(GetMessage(nvc, template, patient));
            }

            return result;

        }

        private List<BirthdayWebModel> BirthdaysList(String birthday, Boolean allMonth, int? statusId)
        {
            var patients = GetPatients(birthday, allMonth, statusId);
            var model = new List<BirthdayWebModel>();
            //map
            foreach (var patient in patients)
            {
                var item = new BirthdayWebModel();
                //client
                item.ClientId = patient.ClientId;
                item.ClientName = patient.Client.Name;
                item.ClientFirstName = patient.Client.FirstName;
                item.ClientLastName = patient.Client.LastName;
                item.ClientBirthdate = patient.Client.BirthDate.HasValue ? patient.Client.BirthDate.Value.ToShortDateString() : "-";
                //email
                var email = patient.Client.Emails.FirstOrDefault();
                if (email != null)
                {
                    item.Email = email.Name;
                }
                item.ClientReferer = patient.Client.ReferredBy ?? "";
                item.ClientStatusId = patient.Client.StatusId.HasValue ? patient.Client.StatusId.Value : 0;
                item.Status = patient.Client.ClientStatus != null ? patient.Client.ClientStatus.Name : "";
                item.DoctorName = patient.Client.VetId.HasValue ? patient.Client.Vet.Name : "";
                item.ClientBalance = RapidVetUnitOfWork.ClientRepository.GetClientBalance(patient.ClientId);
                if (patient.Client.BirthDate.HasValue)
                {
                    AgeHelper age = new AgeHelper(patient.Client.BirthDate.Value, DateTime.Today);
                    item.ClientAge = age.Years + "." + age.Months;
                }
                else
                {
                    item.ClientAge = "";
                }
                item.ClientTitle = patient.Client.TitleId.HasValue ? patient.Client.Title.Name : "";
                item.ClientIdCardNumber = patient.Client.IdCard;
                item.ClientRecordNumber = patient.Client.LocalId;

                //adress
                var address = patient.Client.Addresses.FirstOrDefault();
                if (address != null)
                {
                    item.Address = address.Street + " " + address.City.Name;
                    item.Zipcode = address.ZipCode;
                    item.ClientStreetAndNumber = address.Street;
                    item.City = address.City != null ? address.City.Name : "";
                }

                //phones
                var phone = patient.Client.Phones.FirstOrDefault(p => p.PhoneTypeId == HomePhone());
                item.Phone = phone == null ? "" : phone.PhoneNumber;

                var workPhone = patient.Client.Phones.FirstOrDefault(p => p.PhoneTypeId == WorkPhone());
                item.WorkPhone = workPhone == null ? "" : workPhone.PhoneNumber;
                
                var cellPhone = patient.Client.Phones.FirstOrDefault(p => p.PhoneTypeId == CellPhone());
                item.CellPhone = cellPhone == null ? "" : cellPhone.PhoneNumber;

                item.PatientName = patient.Name;
                item.PatientAnimalKind = patient.AnimalRace.AnimalKind.Value;
                item.PatientRace = patient.AnimalRace.Value;
                item.PatientColor = patient.AnimalColor != null ? patient.AnimalColor.Value : "";
                if (patient.BirthDate.HasValue)
                {
                    AgeHelper age = new AgeHelper(patient.BirthDate.Value, DateTime.Today);
                    item.PatientAge = age.Years + "." + age.Months;
                }
                else
                {
                    item.PatientAge = "";
                }
                item.PatientSex = patient.GenderId == 1 ? Resources.Gender.Male : (patient.GenderId == 2 ? Resources.Gender.Female : Resources.Gender.UndefinedSex);
                item.BirthDate = patient.BirthDate.HasValue ? patient.BirthDate.Value.ToShortDateString() : "-";
                

                //add
                model.Add(item);
            }
            return model;
        }

        private NameValueCollection GetNVC()
        {
            var nvc = new NameValueCollection();
            var doctor =  CurrentUser.IsDoctor ? CurrentUser : CurrentUser.DefaultDrId.HasValue ? RapidVetUnitOfWork.UserRepository.GetItem(CurrentUser.DefaultDrId.Value) : null;

            nvc.Add("\r\n", "<br />");
            nvc.Add(getTemplateKeyWord("ClientFirstName"), "");
            nvc.Add(getTemplateKeyWord("ClientLastName"), "");            
            nvc.Add(getTemplateKeyWord("ClientName"), "");
            nvc.Add(getTemplateKeyWord("ClientAddress"), "");
            nvc.Add(getTemplateKeyWord("ClientZip"), "");
            nvc.Add(getTemplateKeyWord("PatientName"), "");
            nvc.Add(getTemplateKeyWord("PatientBirthDate"), "");
            nvc.Add(getTemplateKeyWord("DoctorName"), "");
            nvc.Add(getTemplateKeyWord("ClinicAddress"), CurrentUser.ActiveClinic.Address);
            nvc.Add(getTemplateKeyWord("ClinicName"), CurrentUser.ActiveClinic.Name);
            nvc.Add(getTemplateKeyWord("ClinicPhone"), CurrentUser.ActiveClinic.Phone);
            nvc.Add(getTemplateKeyWord("ClinicFax"), CurrentUser.ActiveClinic.Fax);
            nvc.Add(getTemplateKeyWord("ClinicEmail"), CurrentUser.ActiveClinic.Email);
            nvc.Add(getTemplateKeyWord("ActiveDoctorName"), doctor != null ? doctor.Name : "");
            nvc.Add(getTemplateKeyWord("ActiveDoctorLicence"), doctor != null ? doctor.LicenceNumber : "");
            nvc.Add(getTemplateKeyWord("Date"), DateTime.Now.ToShortDateString());
            nvc.Add(getTemplateKeyWord("ClientReferer"), "");
            nvc.Add(getTemplateKeyWord("PatientAnimalKind"), "");
            nvc.Add(getTemplateKeyWord("PatientRace"), "");
            nvc.Add(getTemplateKeyWord("PatientColor"), "");
            nvc.Add(getTemplateKeyWord("PatientAge"), "");
            nvc.Add(getTemplateKeyWord("PatientSex"), "");
            nvc.Add(getTemplateKeyWord("ClientBalance"), "");
            nvc.Add(getTemplateKeyWord("ClientAge"), "");
            nvc.Add(getTemplateKeyWord("ClientTitle"), "");
            nvc.Add(getTemplateKeyWord("ClientIdCardNumber"), "");
            nvc.Add(getTemplateKeyWord("ClientCity"), "");
            nvc.Add(getTemplateKeyWord("ClientHomePhone"), "");
            nvc.Add(getTemplateKeyWord("ClientMobilePhone"), "");
            nvc.Add(getTemplateKeyWord("ClientBirthdate"), "");
            nvc.Add(getTemplateKeyWord("ClientStreetAndNumber"), "");
            nvc.Add(getTemplateKeyWord("ClientRecordNumber"), "");
            return nvc;
        }

        private String GetMessage(NameValueCollection nvc,String template,BirthdayWebModel patient)
        {

            nvc.Set(getTemplateKeyWord("ClientFirstName"), patient.ClientFirstName);
            nvc.Set(getTemplateKeyWord("ClientLastName"), patient.ClientLastName);
            nvc.Set(getTemplateKeyWord("ClientName"), patient.ClientName);
            nvc.Set(getTemplateKeyWord("ClientAddress"), patient.Address);
            nvc.Set(getTemplateKeyWord("ClientZip"), patient.Zipcode);
            nvc.Set(getTemplateKeyWord("PatientName"), patient.PatientName);
            nvc.Set(getTemplateKeyWord("PatientBirthDate"), patient.BirthDate);
            nvc.Set(getTemplateKeyWord("DoctorName"), patient.DoctorName);
            nvc.Set(getTemplateKeyWord("ClientReferer"), patient.ClientReferer);
            nvc.Set(getTemplateKeyWord("PatientAnimalKind"), patient.PatientAnimalKind);
            nvc.Set(getTemplateKeyWord("PatientRace"), patient.PatientRace);
            nvc.Set(getTemplateKeyWord("PatientColor"), patient.PatientColor);
            nvc.Set(getTemplateKeyWord("PatientAge"), patient.PatientAge);
            nvc.Set(getTemplateKeyWord("PatientSex"), patient.PatientSex);
            nvc.Set(getTemplateKeyWord("ClientBalance"), patient.ClientBalance.ToString());
            nvc.Set(getTemplateKeyWord("ClientAge"), patient.ClientAge);
            nvc.Set(getTemplateKeyWord("ClientTitle"), patient.ClientTitle);
            nvc.Set(getTemplateKeyWord("ClientIdCardNumber"), patient.ClientIdCardNumber);
            nvc.Set(getTemplateKeyWord("ClientCity"), patient.City);
            nvc.Set(getTemplateKeyWord("ClientHomePhone"), patient.Phone);
            nvc.Set(getTemplateKeyWord("ClientMobilePhone"), patient.CellPhone);
            nvc.Set(getTemplateKeyWord("ClientBirthdate"), patient.ClientBirthdate);
            nvc.Set(getTemplateKeyWord("ClientStreetAndNumber"), patient.ClientStreetAndNumber);
            nvc.Set(getTemplateKeyWord("ClientRecordNumber"), patient.ClientRecordNumber.ToString());
            var result = RapidVetUnitOfWork.LetterTemplatesRepository.FormatString(nvc, template);
            return result;
        }

        private string GetTemplate(BirthdayContactPrintOutput output)
        {
            var template = string.Empty;
            switch (output)
            {
                //case BirthdayContactPrintOutput.Letter:
                //    template = CurrentUser.ActiveClinicGroup.BirthdayLetterTemplate;
                //    break;
                case BirthdayContactPrintOutput.Sticker:
                    template = CurrentUser.ActiveClinicGroup.BirthdayStickerTemplate;
                    break;
                    ;
                case BirthdayContactPrintOutput.Sms:
                    template = CurrentUser.ActiveClinicGroup.BirthdaySmsTemplate;
                    break;
                case BirthdayContactPrintOutput.Email:
                    template = CurrentUser.ActiveClinicGroup.BirthdayEmailTemplate;
                    break;
                case BirthdayContactPrintOutput.EmailSubject:
                    template = CurrentUser.ActiveClinicGroup.BirthdayEmailSubject;
                    break;

            }
            return template;
        }

        //isActive functions
        private bool LettersActive()
        {
            return IsPersonalLetterExist();
        }

        private bool StickerActive()
        {
            return CurrentUser.ActiveClinicGroup.BirthdayStickerTemplate != null;
        }

        private bool EmailActive()
        {
            return IsEmailActive() && CurrentUser.ActiveClinicGroup.BirthdayEmailTemplate != null &&
                   CurrentUser.ActiveClinicGroup.BirthdayEmailSubject != null;
        }

        private bool SmsActive()
        {
            return IsSmsActive() && CurrentUser.ActiveClinicGroup.BirthdaySmsTemplate != null;
        }


    }
}
