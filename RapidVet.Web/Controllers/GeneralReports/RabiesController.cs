﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CsvHelper;
using CsvHelper.Configuration;
using RapidVet.Enums;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Web.Attributes;
using RapidVet.WebModels.GeneralReports;
using RestSharp;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class RabiesController : BaseController
    {
        //
        // GET: /Rabies/

        [ViewReports]
        public ActionResult Index()
        {
            var model = new RabiesReportWebModel();
            //animal kind
            var animalOptions =
                RapidVetUnitOfWork.AnimalKindRepository.GetList(CurrentUser.ActiveClinicGroupId).ToList();
            model.AnimalTypeOptions = new List<SelectListItem>();
            model.AnimalTypeOptions.Add(new SelectListItem() { Text = "הכל", Value = "0" });
            model.AnimalTypeOptions.AddRange(AutoMapper.Mapper.Map<List<AnimalKind>, List<SelectListItem>>(animalOptions));

            //sterilization
            var sterilizationRm = Resources.Enums.RabiesReportSterilizationOptions.ResourceManager;
            model.SterilizationOptions = new List<SelectListItem>();
            foreach (var option in Enum.GetValues(typeof(RabiesReportSterilizationOptions)))
            {
                model.SterilizationOptions.Add(new SelectListItem()
                    {
                        Text = sterilizationRm.GetString(option.ToString()),
                        Value = ((int)option).ToString()
                    });
            }

            model.SterilizationOption = 0;
            //regoinal councils
            model.RegionalCouncilOptions = new List<SelectListItem>();
            model.RegionalCouncilOptions.Add(new SelectListItem() { Text = "הכל", Value = "0" });
            var regionalCouncils = RapidVetUnitOfWork.MetaDataRepository.GetRegionalCouncils();
            model.RegionalCouncilOptions.AddRange(AutoMapper.Mapper.Map<List<RegionalCouncil>, List<SelectListItem>>(regionalCouncils));
            //cities
            model.CityOptions = RapidVetUnitOfWork.ClientRepository.GetCitiesOfClinicById(CurrentUser.ActiveClinicId);
            model.CityOptions.Insert(0, new SelectListItem() { Text = "הכל", Value = "0" });
            var rm = Resources.Enums.RabiesReportOptions.ResourceManager;
            //post options
            model.PostOptions = new List<SelectListItem>()
                {
                    new SelectListItem()
                        {
                            Text = rm.GetString(RabiesReportOptions.Print.ToString()),
                            Value = Url.Action("Print")
                        },
                    new SelectListItem()
                        {
                            Text = rm.GetString(RabiesReportOptions.PrintAshdod.ToString()),
                            Value = Url.Action("PrintAsAshdod")
                        },
                    new SelectListItem()
                        {
                            Text = rm.GetString(RabiesReportOptions.SendToRegionalCouncilByMail.ToString()),
                            Value = Url.Action("SendToRegionalCouncilByMail")
                        }
                };

            if (CurrentUser.IsExcelExport)
            {

                model.PostOptions.Add(new SelectListItem()
                    {
                        Text = rm.GetString(RabiesReportOptions.Excel.ToString()),
                        Value = Url.Action("ExportToCSV")
                    });

                model.PostOptions.Add(new SelectListItem()
                    {
                        Text = rm.GetString(RabiesReportOptions.Sorin.ToString()),
                        Value = Url.Action("ExportToSorin")
                    });

                model.PostOptions.Add(new SelectListItem()
                    {
                        Text = rm.GetString(RabiesReportOptions.Meloona.ToString()),
                        Value = Url.Action("ExportToMeloona")
                    });

                model.PostOptions.Add(new SelectListItem()
                    {
                        Text = rm.GetString(RabiesReportOptions.Rapid.ToString()),
                        Value = Url.Action("ExportToRapidVet")
                    });

                model.PostOptions.Add(new SelectListItem()
                    {
                        Text = rm.GetString(RabiesReportOptions.Dolittle.ToString()),
                        Value = Url.Action("ExportToDolittle")
                    });

                model.PostOptions.Add(new SelectListItem()
                    {
                        Text = rm.GetString(RabiesReportOptions.Doctorat.ToString()),
                        Value = Url.Action("ExportToDoctorat")
                    });

                model.PostOptions.Add(new SelectListItem()
                    {
                        Text = rm.GetString(RabiesReportOptions.ReverseDoctorat.ToString()),
                        Value = Url.Action("ExportToReverseDoctorat")
                    });
            }

            return View(model);
        }

        public ActionResult SendToRegionalCouncilByMail(RabiesReportPostModel model)
        {
            List<string> success = new List<string>();
            List<string> failure = new List<string>();
            List<string> noEmail = new List<string>();
            List<RegionalCouncil> regionalCouncils;
            var originalRegionalCouncil = model.RegionalCouncilId;
            
            if (model.RegionalCouncilId.HasValue && model.RegionalCouncilId > 0)
            {
                regionalCouncils = new List<RegionalCouncil>();
                regionalCouncils.Add(RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(model.RegionalCouncilId.Value));
            }
            else
            {
                regionalCouncils = RapidVetUnitOfWork.RegionalCouncilsRepository.GetAllWithEmails();
            }

            foreach (var regionalCouncil in regionalCouncils)
            {
                if (!string.IsNullOrWhiteSpace(regionalCouncil.Email))
                {
                    model.RegionalCouncilId = regionalCouncil.Id;
                    var data = GetData(model);//.Select(Map).ToList();
                    if (data.Count > 0)
                    {
                        var result = sendMailToRegionalCouncil(data, model);
                        if (result)
                        {
                            success.Add(string.Format(Resources.Rabies.RegionalCouncilSuccessMessage, regionalCouncil.Name, data.Count));
                        }
                        else
                        {
                            failure.Add(string.Format(Resources.Rabies.RegionalCouncilFailureMessage, regionalCouncil.Name));
                        }
                    }
                    else
                    {
                        failure.Add(string.Format(Resources.Rabies.RegionalCouncilNoDataToSend, regionalCouncil.Name));
                    }
                }
                else
                {
                    noEmail.Add(string.Format(Resources.Rabies.RegionalCouncilNoMailError, regionalCouncil.Name));
                }
            }

            if (!(originalRegionalCouncil.HasValue && originalRegionalCouncil.Value > 0))
            {
                var regionalCouncilsWithoutEmail = RapidVetUnitOfWork.RegionalCouncilsRepository.GetAllWithoutEmails();
                foreach (var regionalCouncil in regionalCouncilsWithoutEmail)
                {
                    noEmail.Add(string.Format(Resources.Rabies.RegionalCouncilNoMailError, regionalCouncil.Name));
                }
            }

            ViewBag.Success = success;
            ViewBag.Failure = failure;
            ViewBag.NoEmail = noEmail;
            
            return View();
        }

        string emailTemplatePrefix = "<div style=\"position:relative;display:block;margin:auto;direction:rtl;\">";
        string emailTemplatePostfix = "</div>";

        private bool sendMailToRegionalCouncil(List<RabiesReportItem> data, RabiesReportPostModel model)
        {
            var RegionalCouncil = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCoucil(model.RegionalCouncilId.Value);

            var mailClient = getMailClient();

            string from = string.Format("{0}<{1}>", !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.EmailBusinessName) ? CurrentUser.ActiveClinicGroup.EmailBusinessName : CurrentUser.ActiveClinic.Name,
                                               CurrentUser.ActiveClinicGroup.EmailAddress);
            int mailCounter = 1;
            RestRequest request = new RestRequest();
            request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_RAPID_DOMAIN"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", from);
            request.AddParameter("to", RegionalCouncil.Email);
            if(CurrentUser.ActiveClinicGroup.RabiesReportCC)
            {
                request.AddParameter("cc", CurrentUser.ActiveClinicGroup.EmailAddress);
                mailCounter++;
            }
            request.AddParameter("subject", Resources.Rabies.RegionalCouncilMailSubject + CurrentUser.ActiveClinic.Name);
            request.AddParameter("html", emailTemplatePrefix + Resources.Rabies.RegionalCouncilMailFooter + emailTemplatePostfix);
            request.AddFileBytes("attachment", getReportContent(ReportToStream(data)), string.Format("Rabies Report {0} - {1}.csv", model.From.Replace('/', '.'), model.To.Replace('/', '.')), "text/csv");
            request.Method = Method.POST;
            var message = mailClient.Execute(request);
            if ((int)message.StatusCode == 200)
            {
                RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, mailCounter, MessageType.Email);
                string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                     Resources.Auditing.SendingModule, Resources.Auditing.MailToRegionalCouncil,
                                                     Resources.Auditing.RegionalCouncilName, RegionalCouncil.Name,
                                                     Resources.Auditing.Address, RegionalCouncil.Email,
                                                     Resources.Auditing.MailSubject, Resources.Rabies.RegionalCouncilMailSubject + " " + CurrentUser.ActiveClinic.Name,
                                                     Resources.Auditing.ReturnCode, (int)message.StatusCode + " - " + message.StatusCode);

                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.MailSent, logMessage, "");

                return true;
            }

            return false;
        }

        private byte[] getReportContent(Stream report)
        {
            using (var memoryStream = new MemoryStream())
            {
                report.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        [ExcelExport]
        public ActionResult ExportToCSV(RabiesReportPostModel model)
        {
            //debug...
            var data = GetData(model);//.Select(Map).ToList();

            return Excel(data);
        }

        private FileResult Excel(List<RabiesReportItem> originalData)
        {
            var stream = ReportToStream(originalData);

            return File(stream, "text/csv", "RabiesReport.csv");
        }

        private Stream ReportToStream(List<RabiesReportItem> originalData)
        {
            var data = AutoMapper.Mapper.Map<List<RabiesReportItemCsv>>(originalData);
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);

            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8, HasHeaderRecord = false };
            var csv = new CsvWriter(writer, csvConfig);

            csv.WriteField("תאריך חיסון");
            csv.WriteField("ת.ז");
            csv.WriteField("שם משפחה");
            csv.WriteField("שם פרטי");
            csv.WriteField("כתובת");
            csv.WriteField("עיר");
            csv.WriteField("טלפון בית");
            csv.WriteField("טלפון נייד");
            csv.WriteField("דוא\"ל");
            csv.WriteField(" הצג טלפון");
            csv.WriteField("שם חיה");
            csv.WriteField("גזע");
            csv.WriteField("מין");
            csv.WriteField("תאריך לידה");
            csv.WriteField("צבע");
            csv.WriteField("סוג");
            csv.WriteField("מס אלקטרוני");
            csv.WriteField("מספר רישוי");
            csv.WriteField("מספר אצווה");
            csv.WriteField("עיקור/סירוס");

            csv.NextRecord();

            //todo: ignore exemption?
            csv.WriteRecords(data);
            writer.Flush();
            stream.Position = 0;

            return stream;
        }

        public ActionResult Print(RabiesReportPostModel model)
        {
            var res = GetPrintModel(model);

            if (!res.ReportItemsByRegionalCouncils.Any())
            {
                return View("MeloonaError", model: "לא נמצאו נתונים");
            }

            return View(res);
        }

        public ActionResult PrintAsAshdod(RabiesReportPostModel model)
        {
            var res = GetPrintModel(model);
            return View(res);
        }

        [ExcelExport]
        public ActionResult ExportToSorin(RabiesReportPostModel model)
        {
            var data = GetData(model);//.Select(Map).ToList();

            return Sorin(data);
        }

        private FileResult Sorin(List<RabiesReportItem> data)
        {
            //setup
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8); //TODO: encoding?
            int index = 0;
            //write data
            foreach (var item in data)
            {
                index++;
                var sb = new StringBuilder();
                //vaccinating vet name(30)
                sb.Append(OpenFormatUtils.GetAlphaString(item.VaccinatingVetName, 30));
                //index - start with 00001 (5)
                var tempStr = "00000" + index.ToString();
                sb.Append(tempStr.Substring(tempStr.Length - 5));
                //Electronic number(16)
                sb.Append(OpenFormatUtils.GetAlphaString(item.ElectronicNumber, 16));
                //SAGIR number(6)
                sb.Append(OpenFormatUtils.GetAlphaString(item.SAGIRNumber, 6));
                //AnimalKind(1)
                if (CurrentUser.ClinicGroupId.HasValue)
                {
                    var dogKind = RapidVetUnitOfWork.AnimalKindRepository.GetClinicGroupDogKind(CurrentUser.ClinicGroupId.Value);
                    var catKind =
                        RapidVetUnitOfWork.AnimalKindRepository.GetClinicGroupCatKind(CurrentUser.ClinicGroupId.Value);

                    if (dogKind != null && item.AnimalKindId == dogKind.Id)
                    {
                        sb.Append(OpenFormatUtils.GetAlphaString("1", 1));
                    }
                    else if (catKind != null && item.AnimalKindId == catKind.Id)
                    {
                        sb.Append(OpenFormatUtils.GetAlphaString("2", 1));
                    }
                    else
                    {
                        sb.Append(OpenFormatUtils.GetEmptyString(1, false));
                    }
                }
                else
                {
                    sb.Append(OpenFormatUtils.GetEmptyString(1, false));
                }
                //Animal name(10)
                sb.Append(OpenFormatUtils.GetAlphaString(item.AnimalName, 10));
                //Animal Gender(1)
                switch ((GenderEnum)item.AnimalGenderTypeId)
                {
                    case GenderEnum.Female:
                        {
                            sb.Append(OpenFormatUtils.GetAlphaString("2", 1));
                            break;
                        }
                    case GenderEnum.Male:
                        {
                            sb.Append(OpenFormatUtils.GetAlphaString("1", 1));
                            break;
                        }
                    default:
                        {
                            sb.Append(OpenFormatUtils.GetEmptyString(1, false));
                            break;
                        }
                }
                //Animal Race(15)
                sb.Append(OpenFormatUtils.GetAlphaString(item.AnimalRace, 15));
                //Animal color(10)
                sb.Append(OpenFormatUtils.GetAlphaString(item.Color, 10));
                //Animal birth year(4)
                sb.Append(OpenFormatUtils.GetAlphaString(item.BirthYear, 4));
                //Animal birth month(2)
                sb.Append(OpenFormatUtils.GetAlphaString(FormatMonth(item.BirthMonth), 2));
                //Sterilization(1)
                switch (item.SterilizationBool)
                {
                    case true:
                        {
                            sb.Append(OpenFormatUtils.GetAlphaString("2", 1));
                            break;
                        }
                    case false:
                        {
                            sb.Append(OpenFormatUtils.GetAlphaString("1", 1));
                            break;
                        }
                }
                //vaccination date(8)
                var date = item.VaccinationDate.Replace("/", "");
                sb.Append(OpenFormatUtils.GetAlphaString(date, 8));
                //batch number(10)
                sb.Append(OpenFormatUtils.GetEmptyString(10, false));
                //OwnerIdCard(9)
                sb.Append(OpenFormatUtils.GetAlphaString(item.OwnerIdCard, 9));
                //OwnerName(20) 
                sb.Append(OpenFormatUtils.GetAlphaString(item.OwnerFullName, 20));
                //Owner address(35)
                sb.Append(OpenFormatUtils.GetAlphaString(item.Address, 35));
                //house number(10)
                sb.Append(OpenFormatUtils.GetEmptyString(10, false));
                //mailbox(6)
                sb.Append(OpenFormatUtils.GetEmptyString(6, false));
                //city(20)
                sb.Append(OpenFormatUtils.GetAlphaString(item.City, 20));
                //zipcode(5)
                sb.Append(OpenFormatUtils.GetAlphaString(item.AreaCode, 5));
                //Home phone(11)
                sb.Append(OpenFormatUtils.GetAlphaString(item.HomePhone, 11));
                //cell phone(11)
                sb.Append(OpenFormatUtils.GetAlphaString(item.CellPhone, 11));
                //email(25)
                sb.Append(OpenFormatUtils.GetAlphaString(item.Email, 25));
                writer.WriteLine(sb.ToString());
            }
            //finish
            writer.Flush();
            stream.Position = 0;
            return File(stream, "text/ascii", "RabiesReportSorin.txt");
        }

        [ExcelExport]
        public ActionResult ExportToMeloona(RabiesReportPostModel model)
        {
            if (String.IsNullOrEmpty(model.MeloonaCode))
            {
                return View("MeloonaError", model: "יש להכניס קוד וטרינר לאתר מלונה");
            }
            var data = GetData(model);//.Select(Map).ToList();
            return Meloona(data, model.MeloonaCode);
        }

        private FileResult Meloona(List<RabiesReportItem> data, String code)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8); //TODO: encoding?

            //write data
            foreach (var item in data)
            {
                var sb = new StringBuilder();
                //animal name(8)
                sb.Append(OpenFormatUtils.GetAlphaString(item.AnimalName, 8));
                //animal race(20)
                sb.Append(OpenFormatUtils.GetAlphaString(item.AnimalRace, 20));
                //animal kind(10)
                sb.Append(OpenFormatUtils.GetAlphaString(item.AnimalKind, 10));
                //animal gender(2)
                switch ((GenderEnum)item.AnimalGenderTypeId)
                {
                    case GenderEnum.Female:
                        {
                            sb.Append(OpenFormatUtils.GetAlphaString("1", 2));
                            break;
                        }
                    case GenderEnum.Male:
                        {
                            sb.Append(OpenFormatUtils.GetAlphaString("0", 2));
                            break;
                        }
                    default:
                        {
                            sb.Append(OpenFormatUtils.GetEmptyString(2, false));
                            break;
                        }
                }
                //sterilization(2) 0 - isn't, 1- is;
                switch (item.SterilizationBool)
                {
                    case true:
                        {
                            sb.Append(OpenFormatUtils.GetAlphaString("1", 2));
                            break;
                        }
                    case false:
                        {
                            sb.Append(OpenFormatUtils.GetAlphaString("0", 2));
                            break;
                        }
                }
                //primary color(10)
                sb.Append(OpenFormatUtils.GetAlphaString(item.Color, 10));
                //secondary color(10)
                sb.Append(OpenFormatUtils.GetEmptyString(10, false));
                //animal year of birth(4)
                sb.Append(OpenFormatUtils.GetAlphaString(item.BirthYear, 4));
                //month of vaccination(2)
                sb.Append(OpenFormatUtils.GetAlphaString(FormatMonth(item.MonthOfVaccination), 2));
                //electronic number(15)
                sb.Append(OpenFormatUtils.GetAlphaString(item.ElectronicNumber, 15));
                //Owner id card(10)
                sb.Append(OpenFormatUtils.GetAlphaString(item.OwnerIdCard, 10));
                //Owner first name(14)
                sb.Append(OpenFormatUtils.GetAlphaString(item.OwnerFirstName, 14));
                //owner last name(10)
                sb.Append(OpenFormatUtils.GetAlphaString(item.OwnerLastName, 10));
                //owner address(25)
                sb.Append(OpenFormatUtils.GetAlphaString(item.Address, 25));
                //owner house number(5)
                sb.Append(OpenFormatUtils.GetEmptyString(5, false));
                //owner apt. number(5)
                sb.Append(OpenFormatUtils.GetEmptyString(5, false));
                //city(5) - always 3000
                sb.Append(OpenFormatUtils.GetAlphaString("3000", 5));
                //home phone(10)
                sb.Append(OpenFormatUtils.GetAlphaString(item.HomePhone, 10));
                //cell phone(10)
                sb.Append(OpenFormatUtils.GetAlphaString(item.CellPhone, 10));
                //vet code(3)
                sb.Append(OpenFormatUtils.GetAlphaString(code, 3));
                //vaccinationDate(10) dd/MM/yyyy
                sb.Append(OpenFormatUtils.GetAlphaString(item.VaccinationDate, 10));

                writer.WriteLine(sb.ToString());
            }
            //finish
            writer.Flush();
            stream.Position = 0;
            return File(stream, "text/ascii", "RabiesMeloonaReport.txt");
        }

        [ExcelExport]
        public ActionResult ExportToRapidVet(RabiesReportPostModel model)
        {
            var data = GetData(model);//.Select(Map).ToList();
            return RapidFormat(data);
        }

        private FileResult RapidFormat(IEnumerable<RabiesReportItem> data)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8); //TODO: encoding?

            //write data
            foreach (var item in data)
            {
                var sb = new StringBuilder();
                //performing vet name
                sb.Append(item.VaccinatingVetName);
                RapidDelimiter(sb);
                //client id
                sb.Append(item.ClientId);
                RapidDelimiter(sb);
                //client id card
                sb.Append(item.OwnerIdCard);
                RapidDelimiter(sb);
                //owner last name
                sb.Append(item.OwnerLastName);
                RapidDelimiter(sb);
                //owner first name
                sb.Append(item.OwnerFirstName);
                RapidDelimiter(sb);
                //address
                sb.Append(item.Address);
                RapidDelimiter(sb);
                //city
                sb.Append(item.City);
                RapidDelimiter(sb);
                //zipcode
                sb.Append(item.AreaCode);
                RapidDelimiter(sb);
                //home phone
                sb.Append(item.HomePhone);
                RapidDelimiter(sb);
                //call phone
                sb.Append(item.CellPhone);
                RapidDelimiter(sb);
                //animal name
                sb.Append(item.AnimalName);
                RapidDelimiter(sb);
                //animal kind
                sb.Append(item.AnimalKind);
                RapidDelimiter(sb);
                //animal race
                sb.Append(item.AnimalRace);
                RapidDelimiter(sb);
                //animal color
                sb.Append(item.Color);
                RapidDelimiter(sb);
                //license number
                sb.Append(item.LicenceNumber);
                RapidDelimiter(sb);
                //electronic number
                sb.Append(item.ElectronicNumber);
                RapidDelimiter(sb);
                //sterilization 0 or 1
                switch (item.SterilizationBool)
                {
                    case true:
                        {
                            sb.Append(1);
                            break;
                        }
                    case false:
                        {
                            sb.Append(0);
                            break;
                        }
                }
                RapidDelimiter(sb);
                //gender 0 = male, 1 = female
                switch ((GenderEnum)item.AnimalGenderTypeId)
                {
                    case GenderEnum.Female:
                        {
                            sb.Append(1);
                            break;
                        }
                    case GenderEnum.Male:
                        {
                            sb.Append(0);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
                RapidDelimiter(sb);
                //animal birthdate
                sb.Append(item.Birthdate);
                RapidDelimiter(sb);
                //vaccination date
                sb.Append(item.VaccinationDate);
                RapidDelimiter(sb);
                //next vaccination date 
                sb.Append(item.Scheduled);
                RapidDelimiter(sb);
                //doctor license number 
                sb.Append(item.VetLicenseNum);
                RapidDelimiter(sb);
                //write
                writer.WriteLine(sb.ToString());
            }
            //finish
            writer.Flush();
            stream.Position = 0;
            return File(stream, "text/ascii", "RabiesReportRapidFormat.txt");
        }

        [ExcelExport]
        public ActionResult ExportToDolittle(RabiesReportPostModel model)
        {
            var data = GetData(model);//.Select(Map).ToList();
            return Dolittle(data);
        }

        private FileResult Dolittle(List<RabiesReportItem> data)
        {
            //setup
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8); //TODO: encoding?
            //write data
            foreach (var item in data)
            {
                var sb = new StringBuilder();
                //sterilization(2)
                var sterilization = "";
                switch (item.SterilizationBool)
                {
                    case true:
                        {
                            sterilization = "כן";
                            break;
                        }
                    case false:
                        {
                            sterilization = "לא";
                            break;
                        }
                }
                sb.Append(OpenFormatUtils.Reverse(sterilization));
                //10 spaces
                sb.Append(OpenFormatUtils.GetEmptyString(10, false));
                //electronic number(20)
                sb.Append(OpenFormatUtils.GetAlphaString(item.ElectronicNumber, 20));
                //space
                AddSpace(sb);
                //color(15) R
                sb.Append(OpenFormatUtils.GetAlphaAndReverse(item.Color, 15));
                //space
                AddSpace(sb);
                //birthdate(8) dd/mm/yyyy
                sb.Append(OpenFormatUtils.GetAlphaString(FormatDdMmYy(item.Birthdate), 8));
                //space
                AddSpace(sb);
                //gender "z" or "n" (1)
                switch ((GenderEnum)item.AnimalGenderTypeId)
                {
                    case GenderEnum.Female:
                        {
                            sb.Append("נ");
                            break;
                        }
                    case GenderEnum.Male:
                        {
                            sb.Append("ז");
                            break;
                        }
                    default:
                        {
                            AddSpace(sb);
                            break;
                        }
                }
                //race(16) R
                sb.Append(OpenFormatUtils.GetAlphaAndReverse(item.AnimalRace, 16));
                //animal name(16) R
                sb.Append(OpenFormatUtils.GetAlphaAndReverse(item.AnimalName, 16));
                //2 spaces
                AddSpace(sb);
                AddSpace(sb);
                //owner home phone (11)
                sb.Append(OpenFormatUtils.GetAlphaString(item.HomePhone, 11));
                //owner address(31) $
                sb.Append(OpenFormatUtils.GetAlphaAndReverse(item.Address, 31));
                //owner first name(21) R
                sb.Append(OpenFormatUtils.GetAlphaAndReverse(item.OwnerFirstName, 21));
                //owner last name (21) R
                sb.Append(OpenFormatUtils.GetAlphaAndReverse(item.OwnerLastName, 21));
                //owner id card(11) R - leading spaces
                if (item.OwnerIdCard != null)
                {
                    var reverse = OpenFormatUtils.Reverse(item.OwnerIdCard);
                    sb.Append(OpenFormatUtils.GetAlphaAndReverse(reverse, 11));
                }
                else
                {
                    sb.Append(OpenFormatUtils.GetEmptyString(11, false));
                }
                //owner city(16) R
                sb.Append(OpenFormatUtils.GetAlphaAndReverse(item.City, 16));
                //spacce
                AddSpace(sb);
                //vaccination date(8) dd/mm/yyyy
                sb.Append(OpenFormatUtils.GetAlphaString(FormatDdMmYy(item.VaccinationDate), 8));

                writer.WriteLine(sb.ToString());
            }
            //finish
            writer.Flush();
            stream.Position = 0;
            return File(stream, "text/ascii", "RabiesReportDolittle.txt");
        }

        [ExcelExport]
        public ActionResult ExportToDoctorat(RabiesReportPostModel model)
        {
            var data = GetData(model);//.Select(Map).ToList();
            return Doctorat(data, false);
        }

        public ActionResult ExportToReverseDoctorat(RabiesReportPostModel model)
        {
            var data = GetData(model);//.Select(Map).ToList();
            return Doctorat(data, true);
        }

        [ValidateInput(false)]
        public JsonResult GetExportSummaryForDoctorat(RabiesReportPostModel model)
        {
            var data = GetData(model);//.Select(Map).ToList();
            List<string> excludedFromExport = new List<string>();

            foreach (var item in data)
            {
                if (item.AnimalKind.Equals("כלב") && (item.ElectronicNumber != null && item.ElectronicNumber.Length != 10 && item.ElectronicNumber.Length != 15))
                {
                    excludedFromExport.Add(string.Format(Resources.Controllers.RabiesController.DoctoratExportErrorItem, item.OwnerFullName, item.AnimalName));
                }
            }

            return Json(excludedFromExport, JsonRequestBehavior.AllowGet);
        }


        private FileResult Doctorat(List<RabiesReportItem> data, bool reverse)
        {
            //setup
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.GetEncoding(862)); //TODO: encoding?

            //write data
            foreach (var item in data)
            {
                if (item.AnimalKind.Equals("חתול") || (item.AnimalKind.Equals("כלב") && item.ElectronicNumber != null && (item.ElectronicNumber.Length == 10 || item.ElectronicNumber.Length == 15)))
                {
                    var sb = new StringBuilder();
                    //id(9)
                    AddField(sb, item.OwnerIdCard, 9, false);
                    //full name(25) R
                    AddField(sb, item.OwnerFullName, 25, reverse);
                    //address(20) R
                    AddField(sb, item.Address, 20, reverse);
                    //zip code(5) 
                    AddField(sb, item.AreaCode, 5, false);
                    //city(15) R
                    AddField(sb, item.City, 15, reverse);
                    //Home phone(10) 
                    AddField(sb, item.HomePhone, 10, false);
                    //cell phone(10)
                    AddField(sb, item.CellPhone, 10, false);
                    //license number(10)
                    AddField(sb, item.LicenceNumber, 10, false);
                    //electronic number(10)
                    AddField(sb, item.ElectronicNumber, 15, false);
                    //animal kind(4) R
                    AddField(sb, item.AnimalKind, 4, reverse);
                    //animal name(15) R
                    AddField(sb, item.AnimalName, 15, reverse);
                    //animal race(15) R
                    AddField(sb, item.AnimalRace, 15, reverse);
                    //animal color(20) R
                    AddField(sb, item.Color, 20, reverse);
                    //animal gender(11)-R "Zahar Mesoras" or "Nekeva Me'ukeret" if sterilization, else just the gender
                    switch (item.SterilizationBool)
                    {
                        case true:
                            {
                                switch ((GenderEnum)item.AnimalGenderTypeId)
                                {
                                    case GenderEnum.Female:
                                        {
                                            AddField(sb, "נקבה מעוקרת", 11, reverse);
                                            break;
                                        }
                                    case GenderEnum.Male:
                                        {
                                            AddField(sb, "זכר מסורס", 11, reverse);
                                            break;
                                        }
                                    default:
                                        {
                                            AddField(sb, OpenFormatUtils.GetEmptyString(11, false), 11, reverse);
                                            break;
                                        }
                                }
                                break;
                            }
                        case false:
                            {
                                switch ((GenderEnum)item.AnimalGenderTypeId)
                                {
                                    case GenderEnum.Female:
                                        {
                                            AddField(sb, "נקבה", 11, reverse);
                                            break;
                                        }
                                    case GenderEnum.Male:
                                        {
                                            AddField(sb, "זכר ", 11, reverse);
                                            break;
                                        }
                                    default:
                                        {
                                            AddField(sb, OpenFormatUtils.GetEmptyString(11, false), 11, reverse);
                                            break;
                                        }
                                }

                                break;
                            }

                    }

                    //animal birthdate(10) dd/mm/yyyy
                    AddField(sb, item.Birthdate, 10, false);
                    //vaccination date(10) dd/mm/yyyy
                    AddField(sb, item.VaccinationDate, 10, false);
                    //display phone(1) 1=no 2=yes
                    switch (item.DisplayPhone)
                    {
                        case "כן":
                            {
                                AddField(sb, "2", 1, false);
                                break;
                            }
                        case "לא":
                            {
                                AddField(sb, "1", 1, false);
                                break;
                            }
                    }
                    AddField(sb, item.BatchNumber, 8, false);

                    writer.WriteLine(sb.ToString());
                }
            }
            //finish
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/ascii", "RabiesReportDoctorat.txt");
        }

        private RabiesReportPrintModel GetPrintModel(RabiesReportPostModel model)
        {
            var items = GetData(model);//.Select(Map);
            var res = new RabiesReportPrintModel();
            res.From = model.From;
            res.To = model.To;
            res.ReportItemsByRegionalCouncils = new List<List<RabiesReportItem>>();

            var regionalCouncilsIds = items.Select(r => r.RegionalCouncilId).Distinct();

            foreach (var rc in regionalCouncilsIds)
            {
                if (rc != 1)
                {
                    var cityIds = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCouncilCities(rc).Select(c => c.Id);

                    var cityItems = items.Where(i => cityIds.Contains(i.CityId)).OrderBy(c => c.CityId).ThenBy(c => c.VaccinationDate).ToList();
                    if (cityItems != null)
                    {
                        res.ReportItemsByRegionalCouncils.Add(cityItems);
                    }
                }
                else
                {
                    var cityIds = RapidVetUnitOfWork.RegionalCouncilsRepository.GetRegionalCouncilCities(rc).Select(c => c.Id);

                    foreach (var cityId in cityIds)
                    {
                        var cityItems = items.Where(i => i.CityId == cityId).OrderBy(c => c.VaccinationDate).ToList();
                        if (cityItems != null)
                        {
                            res.ReportItemsByRegionalCouncils.Add(cityItems);
                        }
                    }
                }
            }



            //var cities = items.Select(i => i.City).Distinct();
            //foreach (var city in cities)
            //{
            //    var cityItems = items.Where(i => i.City == city).ToList();
            //    res.ReportItemsByRegionalCouncils.Add(cityItems);
            //}
            //   res.ClinicHeader = AutoMapper.Mapper.Map<Clinic, ClinicHeader>(CurrentUser.ActiveClinic);
            return res;
        }

        private RabiesReportItem Map(PreventiveMedicineItem item)
        {
            var res = new RabiesReportItem();
            var patient = item.Patient;
            var client = patient.Client;

            res.VaccinationDate = item.Preformed.HasValue ? item.Preformed.Value.ToShortDateString() : "";
            res.MonthOfVaccination = item.Preformed.HasValue ? item.Preformed.Value.Month : 0;

            res.OwnerFirstName = client.FirstName;
            res.OwnerLastName = client.LastName;

            res.OwnerBirthDate = client.BirthDate.HasValue ? client.BirthDate.Value.ToShortDateString() : "";

            res.OwnerIdCard = client.IdCard;
            res.DisplayPhone = client.ApproveDisplayPhone ? "כן" : "לא";

            //address..
            var address = client.Addresses.FirstOrDefault();
            if (address != null)
            {
                res.Address = string.Format("{0} {1}", address.Street, address.City != null ? address.City.Name : "");
                res.City = address.City != null ? address.City.Name : "";
                res.CityId = address.CityId;
                res.AreaCode = address.ZipCode;
                if (address.City != null)
                {
                    res.RegionalCouncilId = address.City.RegionalCouncilId;

                    if (address.City.RegionalCouncilId != 1)
                    {
                        res.RegionalCouncil = address.City.RegionalCouncil.Name;
                    }
                    else
                    {
                        res.RegionalCouncil = address.City.Name;
                    }
                }
                else
                {
                    res.RegionalCouncilId = 1;
                    res.RegionalCouncil = "";
                }

            }
            //phones 
            var cellId = int.Parse(ConfigurationManager.AppSettings["CellPhoneTypeId"]);
            var cell = client.Phones.FirstOrDefault(p => p.PhoneTypeId == cellId);
            if (cell != null)
            {
                res.CellPhone = cell.PhoneNumber;
            }
            var homeId = int.Parse(ConfigurationManager.AppSettings["HomePhoneTypeId"]);
            var home = client.Phones.FirstOrDefault(p => p.PhoneTypeId == homeId);
            if (home != null)
            {
                res.HomePhone = home.PhoneNumber;
            }
            //emails
            var email = client.Emails.FirstOrDefault();
            if (email != null)
            {
                res.Email = email.Name;
            }
            res.AnimalName = patient.Name;
            res.AnimalRace = patient.AnimalRace.Value;
            res.AnimalKind = patient.AnimalRace.AnimalKind.Value;
            res.AnimalKindId = patient.AnimalRace.AnimalKindId;

            var rm = Resources.Gender.ResourceManager;
            res.Gender = rm.GetString(((GenderEnum)patient.GenderId).ToString());
            res.AnimalGenderTypeId = patient.GenderId;
            res.Birthdate = patient.BirthDate.HasValue ? patient.BirthDate.Value.ToShortDateString() : "-";
            res.Color = patient.AnimalColor != null ? patient.AnimalColor.Value : "";
            res.LicenceNumber = patient.LicenseNumber;
            res.ElectronicNumber = patient.ElectronicNumber;
            res.SterilizationBool = patient.Sterilization;
            res.IsDangerous = patient.AnimalDangerous ? "כן" : "לא";
            res.Exemption = patient.Exemption;
            res.ExemptionCause = patient.ExemptionCause;


            res.VaccinatingVetName = item.PreformingUser != null ? item.PreformingUser.Name : null;
            res.SAGIRNumber = patient.SAGIRNumber;
            if (patient.BirthDate.HasValue)
            {
                res.BirthYear = patient.BirthDate.Value.Year.ToString();
                res.BirthMonth = patient.BirthDate.Value.Month;
            }
            res.Scheduled = item.Scheduled.HasValue ? item.Scheduled.Value.ToShortDateString() : "";
            res.BatchNumber = item.BatchNumber;
            res.ClientPaidOrReturnedTollVaucher = item.ClientPaidOrReturnedTollVaucher ? "+" : "-";
            res.VetLicenseNum = item.PreformingUser != null ? item.PreformingUser.LicenceNumber : "";
            res.ClientId = patient.ClientId;

            return res;
        }

        private List<RabiesReportItem> GetData(RabiesReportPostModel model)
        {
            DateTime fromDate, toDate;
            fromDate = String.IsNullOrEmpty(model.From) ? DateTime.MinValue : DateTime.ParseExact(model.From, "dd/MM/yyyy", null);
            if (String.IsNullOrEmpty(model.To))
            {
                toDate = DateTime.MaxValue;
            }
            else
            {
                toDate = DateTime.ParseExact(model.To, "dd/MM/yyyy", null);
                toDate = toDate.AddDays(1).AddTicks(-1);
            }
            var vaccines = new List<RabiesReportItem>();

            var rabiesPriceListItems =
                RapidVetUnitOfWork.PreventiveMedicineRepository.GetRabisVaccinePriceListItems(CurrentUser.ActiveClinicId);
            var rabiesIds = rabiesPriceListItems.Select(r => r.Id).ToList();

            if (rabiesPriceListItems.Any())
            {

                bool? sterilization = null;
                switch (model.SterilizationOption)
                {
                    case RabiesReportSterilizationOptions.Sterilized:
                        {
                            sterilization = true;
                            break;
                        }
                    case RabiesReportSterilizationOptions.Unsterilized:
                        {
                            sterilization = false;
                            break;
                        }
                }
                int? animalKind = null;
                if (model.AnimalTypeId.HasValue && model.AnimalTypeId.Value > 0)
                {
                    animalKind = model.AnimalTypeId.Value;
                }
                int? cityId = null;
                if (model.CityId.HasValue && model.CityId.Value > 0)
                {
                    cityId = model.CityId.Value;
                }


                int? regionalCouncil = null;
                if (model.RegionalCouncilId.HasValue && model.RegionalCouncilId > 0)
                {
                    regionalCouncil = model.RegionalCouncilId.Value;
                }
                vaccines = RapidVetUnitOfWork.GeneralReportsRepository.GetRabiesVaccinated(fromDate, toDate,
                                                                                               animalKind,
                                                                                               sterilization,
                                                                                               regionalCouncil,
                                                                                               CurrentUser
                                                                                                   .ActiveClinicId,
                                                                                               rabiesIds, cityId);


                //foreach (var patient in patients)
                //{
                //    var patientVaccines =
                //        patient.PreventiveMedicine.Where(
                //            p =>
                //            rabiesIds.Contains(p.PriceListItemId) && p.Preformed.HasValue &&
                //            fromDate <= p.Preformed.Value && p.Preformed.Value <= toDate);
                //    vaccines.AddRange(patientVaccines);
                //}
            }
            return vaccines.OrderByDescending(v => v.VaccinationDate).ToList();

        }

        private String FormatMonth(int month)
        {
            if (month == 0)
            {
                return "";
            }
            if (month < 10)
            {
                return "0" + month;
            }
            return month.ToString();
        }

        private void RapidDelimiter(StringBuilder sb)
        {
            sb.Append("|");
        }

        private void AddSpace(StringBuilder sb)
        {
            sb.Append(OpenFormatUtils.GetEmptyString(1, false));
        }

        private string FormatDdMmYy(String ddMMyyyy)
        {
            if (!String.IsNullOrEmpty(ddMMyyyy))
            {
                if (ddMMyyyy.Length > 8)
                {
                    return ddMMyyyy.Remove(6, 2);
                }
                return ddMMyyyy;
            }
            return "";
        }

        private void AddField(StringBuilder sb, String str, int length, bool reverse)
        {
            if (reverse)
            {
                sb.Append(OpenFormatUtils.GetAlphaAndReverse(str, length));
            }
            else
            {
                sb.Append(OpenFormatUtils.GetAlphaString(str, length));
            }
        }
    }
}
