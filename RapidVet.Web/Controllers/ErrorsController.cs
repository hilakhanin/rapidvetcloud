﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RapidVet.Web.Controllers
{
    public class ErrorsController : BaseController
    {
        public ActionResult General(Exception exception)
        {
            return View("SystemError");
        }
    }
}