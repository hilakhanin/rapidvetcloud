﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using RapidVet.Enums.DataMigration;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVetDataMigration;
using RapidVetDataMigration.Labs;
using RapidVetDataMigration.PreventiveMedicine;
using MigrationOrder = RapidVet.Enums.DataMigration.MigrationOrder;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    [Administrator]
    public class DataMigrationController : BaseController
    {
        public static string _originPath = System.Web.Hosting.HostingEnvironment.MapPath("/MigrationData/");
        public static string _dbPath = string.Empty;
        private static MainMigration mainMigrator;

        static DataMigrationController()
        {
            mainMigrator = new MainMigration();
        }

        public ActionResult Index()
        {
            if (mainMigrator.IsBusy)
            {
                return View("MigrationFollowUp");
            }
            var directories = System.IO.Directory.EnumerateDirectories(_originPath);
            var names = directories.Select(StringUtils.GetDirectoryName).ToList();
            var model = names.Select(d => new SelectListItem()
                {
                    Text = d,
                    Value = d
                }).ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult StartMigration(string path)
        {
            if (mainMigrator.IsBusy)
            {
                return RedirectToAction("Index");
            }
            if (!string.IsNullOrWhiteSpace(path))
            {
                _dbPath = _originPath + "\\" + path;
                var test = new MigrationFilesTest(_dbPath);
                var errorList = test.RunTest();
                if (errorList.Count > 0)
                {
                    return View("MigrationErrors", errorList);
                }
                //start the new migration thread
                mainMigrator.StartMigration(_dbPath, CurrentUser.Id);
                return RedirectToAction("Index");
            }
            return View("Error");
        }

        public JsonResult CheckMigrationStatus()
        {
            return new JsonResult()
                {
                    Data = new
                        {
                            stage = mainMigrator.CurrentStage,
                            status = mainMigrator.MigrationStageStatuses
                        },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
        }


        public JsonResult GetMigrationStages()
        {
            var list = from MigrationOrder o in Enum.GetValues(typeof(MigrationOrder))
                       select new SelectListItem()
                       {
                           Text = RapidVet.Resources.Enums.MigrationOrder.ResourceManager.GetString(o.ToString()),
                           Value = ((int)o).ToString()
                       };
            return Json(list, JsonRequestBehavior.AllowGet);
        }


       
        public ActionResult MigrationComplete(bool print = false)
        {
            var model = RapidVetUnitOfWork.ClinicGroupRepository.GetClinicUsers(mainMigrator._clinicGroupId);
            ViewBag.ClinicName = RapidVetUnitOfWork.ClinicGroupRepository.GetName(mainMigrator._clinicId);
            ViewBag.Print = print;
            return View(model);
        }

        public ActionResult Error()
        {
            return View("Error");
        }


        //public JsonResult ManageMigrations(int stage)
        //{
        //    var currentStageEnded = (MigrationOrder)stage;
        //    var badResult = new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = false
        //                }
        //        };

        //    //try
        //    //{
        //        switch (currentStageEnded)
        //        {
        //            case MigrationOrder.Clinic:
        //                return MigrateClinics();

        //            case MigrationOrder.Doctors:
        //                return MigrateDoctors();

        //            case MigrationOrder.Employees:
        //                return MigrateEmployees();

        //            case MigrationOrder.PriceList:
        //                return MigratePriceLists();

        //            case MigrationOrder.Clients:
        //                return MigrateClients();

        //            case MigrationOrder.Patients:
        //                return MigratePatients();

        //            case MigrationOrder.Diagnoses:
        //                return MigrateDiagnoses();

        //            case MigrationOrder.Referrals:
        //                return MigrateReferrals();

        //            case MigrationOrder.Suppliers:
        //                return MigrateSuppliers();

        //            case MigrationOrder.ExpenseGroups:
        //                return MigrateExpenseGroups();

        //            case MigrationOrder.TelephoneDirectory:
        //                return MigrateTelephoneDirectory();

        //            case MigrationOrder.Visits:
        //                return MigrateVisits();

        //            case MigrationOrder.MedicalProcedures:
        //                return MigrateMedicalProcedures();

        //            case MigrationOrder.Medications:
        //                return MigrateMedications();

        //            case MigrationOrder.DangerousDrugs:
        //                return MigrateDangerousDrugs();

        //            case MigrationOrder.Quotations:
        //                return MigrateQuotations();

        //            case MigrationOrder.Calendars:
        //                return MigrateCalendars();

        //            case MigrationOrder.Holidays:
        //                return MigrateHolidays();

        //            case MigrationOrder.Attendance:
        //                return MigrateAttendance();

        //            case MigrationOrder.PreventiveMedicinePriceListItems:
        //                return MigratePreventiveMedicineItemsToPriceList();

        //            case MigrationOrder.PreventiveMedicineRecords:
        //                return MigratePreventiveMedicineRecords();

        //            case MigrationOrder.PreventiveMedicineTemplates:
        //                return MigratePreventiveMedicineTemplates();

        //            case MigrationOrder.InvoiceAndRecipts:
        //                return MigrateInvoiceAndRecipts();
        //            case MigrationOrder.PreventiveMedicineReminders:
        //                return MigreatePreventiveMedicineReminders();

        //            case MigrationOrder.LabsTemplates:
        //                return MigrateLabTestsTemplates();

        //            case MigrationOrder.LabResults:
        //                return MigrateLabResults();

        //            case MigrationOrder.Expenses:
        //                return MigrateExpenses();

        //        }
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //    UpdateLog(e);
        //    //    return badResult;
        //    //}

        //    return badResult;
        //}

        //public JsonResult MigrateExpenses()
        //{
        //    var migrator = new ExpensesMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Expenses);
        //    }
        //    return new JsonResult()
        //    {
        //        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //        Data = new
        //        {
        //            success = success,
        //            stage = (int)MigrationOrder.Expenses + 1
        //        }
        //    };
        //}

        //public JsonResult MigrateLabResults()
        //{
        //    var migrator = new LabResultsMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.LabResults);
        //    }
        //    return new JsonResult()
        //    {
        //        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //        Data = new
        //        {
        //            success = success,
        //            stage = (int)MigrationOrder.LabResults + 1
        //        }
        //    };
        //}

        //public JsonResult MigrateInvoiceAndRecipts()
        //{
        //    var migrator = new InvoiceAnReciptsMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.InvoiceAndRecipts);
        //    }
        //    return new JsonResult()
        //    {
        //        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //        Data = new
        //        {
        //            success = success,
        //            stage = (int)MigrationOrder.InvoiceAndRecipts + 1
        //        }
        //    };
        //}

        //public JsonResult MigrateLabTestsTemplates()
        //{
        //    var migrator = new LabsTemplateMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.LabsTemplates);
        //    }
        //    return new JsonResult()
        //    {
        //        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //        Data = new
        //        {
        //            success = success,
        //            stage = (int)MigrationOrder.LabsTemplates + 1
        //        }
        //    };
        //}

        //public JsonResult MigreatePreventiveMedicineReminders()
        //{
        //    var migrator = new PreventiveMedicineRemindersMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.PreventiveMedicineReminders);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.PreventiveMedicineReminders + 1
        //                }
        //        };
        //}

        //public JsonResult MigratePreventiveMedicineTemplates()
        //{
        //    var migrator = new PreventiveMedicineTemplateMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.PreventiveMedicineTemplates);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.PreventiveMedicineTemplates + 1
        //                }
        //        };
        //}

        //public JsonResult MigratePreventiveMedicineRecords()
        //{
        //    var migrator = new PreventiveMedicineRecordsMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.PreventiveMedicineRecords);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.PreventiveMedicineRecords + 1
        //                }
        //        };
        //}

        //public JsonResult MigratePreventiveMedicineItemsToPriceList()
        //{
        //    var migrator = new PreventiveMedicinePriceListItemsMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.PreventiveMedicinePriceListItems);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.PreventiveMedicinePriceListItems + 1
        //                }
        //        };

        //}

        //public JsonResult MigrateAttendance()
        //{
        //    var migrator = new AttendanceMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Attendance);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Attendance + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateHolidays()
        //{
        //    var migrator = new HolidaysMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Holidays);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Holidays + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateCalendars()
        //{
        //    var migrator = new CalendarMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Calendars);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Calendars + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateQuotations()
        //{
        //    var migrator = new QuotationMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Quotations);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Quotations + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateDangerousDrugs()
        //{
        //    var migrator = new DangerousDrugsMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.DangerousDrugs);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.DangerousDrugs + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateMedications()
        //{
        //    var migrator = new MedicationMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Medications);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Medications + 1
        //                }
        //        };
        //}


        //public JsonResult MigrateMedicalProcedures()
        //{
        //    var migrator = new MedicalProceduresDischargePapersMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.MedicalProcedures);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.MedicalProcedures + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateVisits()
        //{
        //    var migrator = new VisitMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Visits);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Visits + 1
        //                }
        //        };
        //}


        //public JsonResult MigrateClinics()
        //{
        //    var migrator = new ClinicGroupMigration(_dbPath);
        //    var clinicId = migrator.RunMigration(CurrentUser.Id);
        //    var success = clinicId > 0;
        //    if (success)
        //    {
        //        _clinicId = clinicId;
        //        _clinicGroupId = RapidVetUnitOfWork.ClinicGroupRepository.GetId(clinicId);
        //        UpdateLog(MigrationOrder.Clinic);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Clinic + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateDoctors()
        //{
        //    var migrator = new DoctorsMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Doctors);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Doctors + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateEmployees()
        //{
        //    var migrator = new EmployeesMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Employees);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Employees + 1
        //                }
        //        };
        //}

        //public JsonResult MigratePriceLists()
        //{
        //    var migrator = new PriceListMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.PriceList);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.PriceList + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateClients()
        //{
        //    var migrator = new ClientMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Clients);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Clients + 1
        //                }
        //        };
        //}

        //public JsonResult MigratePatients()
        //{
        //    var migrator = new PatientMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Patients);
        //        UpdateClinicClientsWithSearch(_clinicId);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Patients + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateDiagnoses()
        //{
        //    var migrator = new DiagnosesMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Diagnoses);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Diagnoses + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateReferrals()
        //{
        //    var migrator = new ReferralsMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicGroupId, _clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Referrals);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Referrals + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateSuppliers()
        //{
        //    var migrator = new SuppliersMigration(_dbPath);
        //    var success = migrator.RunSuppliersMigration(_clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.Suppliers);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.Suppliers + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateExpenseGroups()
        //{
        //    var migrator = new SuppliersMigration(_dbPath);
        //    var success = migrator.RunExpenseGroupMigration(_clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.ExpenseGroups);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.ExpenseGroups + 1
        //                }
        //        };
        //}

        //public JsonResult MigrateTelephoneDirectory()
        //{
        //    var migrator = new TelephoneDirectoryMigration(_dbPath);
        //    var success = migrator.RunMigration(_clinicId);
        //    if (success)
        //    {
        //        UpdateLog(MigrationOrder.TelephoneDirectory);
        //    }
        //    return new JsonResult()
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //                {
        //                    success = success,
        //                    stage = (int)MigrationOrder.TelephoneDirectory + 1
        //                }
        //        };
        //}


        ////id is clinicId
        //public ActionResult MigrationComplete(bool print = false)
        //{
        //    var model = RapidVetUnitOfWork.ClinicGroupRepository.GetClinicUsers(_clinicGroupId);
        //    ViewBag.ClinicName = RapidVetUnitOfWork.ClinicGroupRepository.GetName(_clinicGroupId);
        //    ViewBag.Print = print;
        //    return View(model);
        //}

        //public ActionResult Error()
        //{
        //    return View("Error");
        //}




        ////id is clinicId
        //public void UpdateClinicClientsWithSearch(int? id)
        //{
        //    var clinicId = id != null ? id.Value : _clinicId;
        //    RapidVetUnitOfWork.ClinicRepository.UpdateClientsSearchString(clinicId);
        //}



        //private void UpdateLog(MigrationOrder order)
        //{
        //    var entry = new DataMigrationLog()
        //        {
        //            ClinicGroupId = _clinicGroupId,
        //            ClinicId = _clinicId,
        //            LastSuccessfullStageId = (int)order,
        //            LastSuccessfullStageName = order.ToString(),
        //            DateTime = DateTime.Now
        //        };
        //    var status = RapidVetUnitOfWork.DataMigrationRepository.UpdateLog(entry);
        //}

        //private void UpdateLog(Exception e)
        //{
        //    var entry = RapidVetUnitOfWork.DataMigrationRepository.GetEntry(_clinicGroupId, _clinicId);
        //    if (entry != null)
        //    {
        //        var sb = new StringBuilder();
        //        sb.AppendLine("----------exception---------");
        //        sb.AppendLine(e.ToString());

        //        if (e.InnerException != null)
        //        {
        //            sb.AppendLine("----------inner exception---------");
        //            sb.AppendLine(e.InnerException.ToString());
        //        }

        //        entry.Exception = sb.ToString();
        //        var status = RapidVetUnitOfWork.Save();
        //    }

        //}



    }
}
