﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;
using RapidVet.FilesAndStorage;
using RapidVet.Helpers;
using RapidVet.WebModels.DataMigration;
using RapidVetDataMigration.Archive;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    [Administrator]
    public class ArchiveMigrationController : BaseController
    {
        public static string _originPath = System.Web.Hosting.HostingEnvironment.MapPath("/MigrationData/");

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetFilePaths()
        {
            /*###  DO NOT COMMIT OR DEPLOY THIS LINE ###*/
            CurrentUser.ActiveClinicGroup.MigrationFolder =
                HostingEnvironment.MapPath("/MigrationData/1");
            /*###*/

            if (!string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.MigrationFolder))
            {
                var migrator = new ArchiveRecordsMigration(CurrentUser.ActiveClinicGroup.MigrationFolder);

                var originFilePaths = migrator.GetFilePaths(CurrentUser.ActiveClinicId);

                var destinationFilePaths =
                    Directory.EnumerateDirectories(string.Format("{0}\\{1}",
                                                                 CurrentUser.ActiveClinicGroup.MigrationFolder,
                                                                 "archive"));
                var names = destinationFilePaths.Select(StringUtils.GetDirectoryName).ToList();
                var destinationPaths = names.Select(n => new SelectListItem()
                    {
                        Text = n,
                        Value = n
                    }).ToList();

                return Json(new { origins = originFilePaths.ToList(), destinations = destinationPaths },
                            JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        [HttpPost]
        public ActionResult MigrateArchiveRecords(string mapping)
        {
            /*###  DO NOT COMMIT OR DEPLOY THIS LINE ###*/
            CurrentUser.ActiveClinicGroup.MigrationFolder =
                HostingEnvironment.MapPath("/MigrationData/1");
            /*###*/

            if (!string.IsNullOrWhiteSpace(mapping) && !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.MigrationFolder))
            {
                var mapped = JsonConvert.DeserializeObject<List<ArchiveMigrationMapping>>(mapping);
                var paths = mapped.ToDictionary(item => item.Name,
                                                item =>
                                                string.Format("{0}\\archive\\{1}",
                                                              CurrentUser.ActiveClinicGroup.MigrationFolder,
                                                              item.Destination));

                var migrator = new ArchiveRecordsMigration(CurrentUser.ActiveClinicGroup.MigrationFolder);
                var success = migrator.RunMigration(CurrentUser.ActiveClinicGroupId, CurrentUser.ActiveClinicId, paths);
                if (success)
                {
                    return RedirectToAction("MigrateArchiveFiles", "ArchiveMigration",
                                            new
                                                {
                                                    id = CurrentUser.ActiveClinicId
                                                });
                }
            }
            return View("Error");
        }

        //id is clinicId
        public ActionResult MigrateArchiveFiles(int id)
        {
            return View();
        }

        public JsonResult MigrateFiles(int clinicId)
        {
            var clinicArchiveItems = RapidVetUnitOfWork.ArchivesRepository.GetMigratedDocumnets(clinicId).ToList();
            var counter = 0;
            foreach (var item in clinicArchiveItems)
            {
                try
                {
                    counter++;
                    using (var stream = new FileStream(item.FileMigrationLocation, FileMode.Open))
                    {
                        //ArchivesUploadHelper.UploadDocument(clinicId, item.Id, item.FileExtension, stream, false);
                    }
                }
                catch (Exception e)
                {
                    counter--;
                    RapidVetUnitOfWork.DataMigrationRepository.LogFailedFile(clinicId, item.Id,
                                                                             item.FileMigrationLocation);
                }
            }

            if (counter == clinicArchiveItems.Count())
            {
                SetSuccessMessage();
            }
            else
            {
                SetErrorMessage("אירעה שגיאה, חלק מהקבצים לא הומרו");
            }

            return Json(true);
        }

    }
}

