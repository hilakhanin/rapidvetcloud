﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace RapidVet.Web.Controllers
{
    public class EasyCardController : BaseController
    {
        //
        // GET: /EasyCard/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Redirect(int issuerId, double amount, int terminalId)
        {
            var baseUrl = WebConfigurationManager.AppSettings["EasyCardRedirectUrl"].ToString();

            string redirectUrl;

            var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(issuerId);
            var terminal = RapidVetUnitOfWork.IssuerRepository.GetEasyCardTerminal(terminalId);
          
            if (issuer != null && issuer.Id == terminal.IssuerId)
            {
                redirectUrl = string.Format(baseUrl, terminal.TerminalId, terminal.Password, amount);
                //redirectUrl = string.Format(baseUrl, 3097, "test123456", amount);
                return Redirect(redirectUrl);
            }
            else
            {
                var details = string.Format(baseUrl, terminal.TerminalId, terminal.Password, amount);
                var savePath = System.Configuration.ConfigurationManager.AppSettings["LOG_PATH"];
                
                var ex =  new Exception(" :לא הוגדרו פרטי איזיקארד/מספר מנפיק לא תקין" + Environment.NewLine + details + Environment.NewLine + " Current User: " + CurrentUser.Name + " ClinicId: "+ CurrentUser.ActiveClinicId);
                if (savePath != null)
                    BaseLogger.LogWriter.WriteMsgPath("Critical",ex.Message.ToString(), savePath,"");

          
                throw new SecurityException();
            }
           // if (issuer != null && !string.IsNullOrEmpty(issuer.EasyCardClientId) &&
            //    !string.IsNullOrEmpty(issuer.EasyCardPassword))
            //{
            //    redirectUrl = string.Format(baseUrl, issuer.EasyCardClientId, issuer.EasyCardPassword, amount);
            //    //redirectUrl = string.Format(baseUrl, 3097, "test123456", amount);
            //    return Redirect(redirectUrl);
            //}
            //else
            //{
            //    throw new Exception("לא הוגדרו פרטי איזיקארד/מספר מנפיק לא תקין");
            //}
            //return View();//compilation patch - delete this line
        }

        public ActionResult Test()
        {
            return View();
        }

    }
}
