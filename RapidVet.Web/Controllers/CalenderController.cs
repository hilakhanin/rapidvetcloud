﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RapidVet.Enums;
using RapidVet.Model;
using RapidVet.Model.Clinics;
using RapidVet.Model.FullCalender;
using RapidVet.Model.Users;
using RapidVet.WebModels.Clinics;
using RapidVet.WebModels.FullCalender;
using RapidVet.WebModels.Users;
using CsvHelper;
using CsvHelper.Configuration;
using RapidVet.Helpers;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.ClinicGroups;
using RestSharp;
using RapidVet.WebModels.Doctors;
using RapidVetMembership;

namespace RapidVet.Web.Controllers
{
    public class CalenderController : BaseController
    {
        List<TemplateKeyWords> TemplateKeyWords;

        public ActionResult Index()
        {
            string ClientName = "";
            string ClientId = "";

            Session["ClientName"] = null;
            if (Request.QueryString["ClientName"] != null)
            {
                ClientName = Server.UrlEncode(Request.QueryString["ClientName"].ToString());

                Session["ClientName"] = ClientName;
            }

            Session["ClientId"] = null;
            if (Request.QueryString["ClientId"] != null)
            {
                ClientId = Server.UrlEncode(Request.QueryString["ClientId"].ToString());

                Session["ClientId"] = ClientId;
            }

            return RedirectToAction("Weekly");
        }

        public ActionResult Weekly(int? id)
        {
            var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId);
                       
            var model = new WeeklyCalenderWebModel()
                {
                    Doctors = new List<SelectListItem>(),
                    SelectedDoctorId = id.HasValue ? id.Value.ToString() : CurrentUser.Id.ToString()
                };

            foreach (var doctor in doctors)
            {
                if (!doctor.Active || doctor.IsNoDiary)
                    continue;

                User dUser = RapidVetUnitOfWork.ClinicRepository.GetUserByUN(doctor.Username);
                if (dUser != null)
                    continue;

                model.Doctors.Add(new SelectListItem() { Text = doctor.FirstName + " " + doctor.LastName, Value = doctor.Id.ToString() });
            }
            ViewBag.EntryId = "0";
            if (Request.QueryString["EntryId"] != null && (Session["EntryId"] == null || Session["EntryId"].ToString().Length == 1))
            {
                ViewBag.EntryId = Request.QueryString["EntryId"].ToString();                
            }
            if (Request.QueryString["EntryId"] != null)
            {
                Session["EntryId"] = Request.QueryString["EntryId"].ToString();
            }
            

            return View(model);
        }

        public void ShowCalendarChanged(bool Val, string Id)
        {
            int id;
            if (!int.TryParse(Id, out id))
                return;

            bool isPrivateCalendars = !CurrentUser.IsAdministrator || !CurrentUser.IsClinicManager;
            RapidVetUnitOfWork.ClinicRepository.Save(CurrentUser.Id, CurrentUser.ActiveClinicId, new List<UsersSettingsShowUserModel>()
            {
                new UsersSettingsShowUserModel(){
                     DoctorId = id,
                     Show = Val                     
                }
            }, isPrivateCalendars);
        }

        public ActionResult DocsDaily()
        {
            bool isPrivateCalendars = !CurrentUser.IsAdministrator || !CurrentUser.IsClinicManager;
            var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctorsForSchedule(CurrentUser.ActiveClinicId, CurrentUser.Id, isPrivateCalendars);

            var model = new WeeklyCalenderWebModel()
                {
                    Doctors = new List<SelectListItem>(),
                    Calendars = new List<CalendarModel>()
                };

            foreach (var doctor in doctors)
            {
                var n = String.Format("{0} {1}", doctor.FirstName, doctor.LastName);

                if (RapidVetUnitOfWork.ClinicRepository.GetUserByUN(doctor.Username) != null || doctor.IsNoDiary)
                    continue;

                if (doctor.ShowOnMainWin)
                    model.Doctors.Add(new SelectListItem() { Text = n, Value = doctor.Id.ToString() });                

                model.Calendars.Add(new CalendarModel() { Name = n, ID = doctor.Id, Show = doctor.ShowOnMainWin });
            }

            return View(model);
        }

        public ActionResult RoomsDaily()
        {
            var rooms = RapidVetUnitOfWork.RoomRepository.GetRoomsForClinic(CurrentUser.ActiveClinicId);

            var model = new RoomsCalenderWebModel()
            {
                Rooms = new List<SelectListItem>()
            };

            foreach (var room in rooms)
            {
                model.Rooms.Add(new SelectListItem() { Text = room.Name, Value = room.Id.ToString() });
            }

            return View(model);
        }

        public JsonResult GetHolidays()
        {
            var holidays = RapidVetUnitOfWork.HolidayRepository.GetHolidaysForClinic(CurrentUser.ActiveClinicId);
            var holidaysJson = new List<CalenderEntryWebModel>();

            foreach (var holiday in holidays)
            {
                var cEntry = new CalenderEntryWebModel()
                    {
                        id = holiday.Id,
                        title = holiday.Name,
                        start = holiday.StartDay,
                        end = holiday.EndDay,
                        color = "#FFDD93",
                        description = "holiday",
                        textColor = "black",
                        allDay = false,
                        editable = false,
                        className = "transparentHoliday",
                        cls = "open",
                        background = "black",
                        comment = ""
                    };
                if (holiday.HasHolidayEve)
                {
                    cEntry.start = cEntry.start.AddDays(-1).Date + new TimeSpan(14, 0, 0);
                }
                if (cEntry.start == cEntry.end)
                {
                    cEntry.end = cEntry.end.AddDays(1).AddTicks(-1);
                }
                holidaysJson.Add(cEntry);
            }
            return Json(holidaysJson, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDocsCalenderEntries(int doctorId, string start, string end)
        {

            DateTime startDate = DateTime.Parse(start);//JavaTimeStampToDateTime(start);
            DateTime endDate = DateTime.Parse(end); //JavaTimeStampToDateTime(end);

            var entries = RapidVetUnitOfWork.CalenderRepository.GetDocsCalenderEntry(CurrentUser.ActiveClinicId, doctorId, startDate, endDate);
            var entriesJson = entries.Select(entry => new CalenderEntryWebModel()
                {
                    id = entry.Id,
                    title = entry.Title != null ? entry.Title : (entry.Client != null ? entry.Client.Name + " - " + (entry.Patient !=null ? entry.Patient.Name : "") : ""),
                    ////title = entry.Title != null ? entry.Title : (entry.Client != null ? entry.Client.NameWithPatients : ""),
                    start = entry.Date.ToUniversalTime(),
                    end = entry.Date.AddMinutes(entry.Duration).ToUniversalTime(),
                    color = !entry.IsNoShow.HasValue ? entry.Color : (entry.IsNoShow.Value ? "#dc464c" : "#8ac119"),
                    description = "calenderentry",
                    textColor = "black",
                    allDay = false,
                    editable = true,
                    startEditable = true, //to enable dragging                    
                    clientId = entry.ClientId,
                    isNoShow = entry.IsNoShow,
                    comment = entry.RoomId != null ? string.IsNullOrWhiteSpace(entry.Comments) ? string.Format("{0} - {1}", "חדר מקושר", entry.Room.Name) : entry.Comments + "<br/>" + string.Format("{0} - {1}", "חדר מקושר", entry.Room.Name) : string.IsNullOrWhiteSpace(entry.Comments) ? "" : entry.Comments
                }).ToList();

            return Json(entriesJson, JsonRequestBehavior.AllowGet);
        }

        DateTime JavaTimeStampToDateTime(long javaTimeStamp)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(javaTimeStamp);
        }

        public JsonResult GetRoomsCalenderEntries(int roomId)
        {
            var entries = RapidVetUnitOfWork.CalenderRepository.GetRoomsCalenderEntries(roomId);
            var entriesJson = entries.Select(entry => new CalenderEntryWebModel()
            {
                id = entry.Id,
                title = entry.Title ?? (entry.Client != null ? entry.Client.NameWithPatients : ""),
                start = entry.Date,
                end = entry.Date.AddMinutes(entry.Duration),
                color = !entry.IsNoShow.HasValue ? entry.Color : (entry.IsNoShow.Value ? "#dc464c" : "#8ac119"),
                description = "calenderentry",
                textColor = "black",
                allDay = false,
                startEditable = true,
                editable = true,
                isNoShow = entry.IsNoShow,
                clientId = entry.ClientId,
                comment = string.IsNullOrWhiteSpace(entry.Comments) ? "" : entry.Comments
            }).ToList();

            return Json(entriesJson, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDocsBlockedDays(int doctorId)
        {
            var blockedDaysOfDoc = RapidVetUnitOfWork.CalenderRepository.GetBlockedDaysOfDoc(CurrentUser.ActiveClinicId, doctorId);
            var entriesJson = blockedDaysOfDoc.Select(blockedDay => new CalenderEntryWebModel()
            {
                id = blockedDay.Id,
                title = GetHolidayNameString(blockedDay.DayDate.Date) + "יום חסום " + blockedDay.Comment,
                start = blockedDay.DayDate.Date,
                end = blockedDay.DayDate.Date.AddDays(1).AddSeconds(-1),
                color = "firebrick",
                description = "mblockedday",
                textColor = "white",
                allDay = false,
                editable = false,
                className = "transparent",
                cls = "open",
                background = "white",
                rendering = "background",
                comment = ""
            }).OrderByDescending(e => e.start).ToList();

            return Json(entriesJson, JsonRequestBehavior.AllowGet);
        }

        private string GetHolidayNameString(DateTime date)
        {
            var str = RapidVetUnitOfWork.CalenderRepository.GetHolidayNameIfExists(CurrentUser.ActiveClinicId, date);
            if (str != null)
            {
                str = str + " - ";
                return str;
            }
            return "";
        }

        public JsonResult GetDocsWeeklySchedule(int docId, string date)
        {
            var dailySchedules = RapidVetUnitOfWork.UserRepository.GetDailySchedules(docId, CurrentUser.ActiveClinicId);
            var recesses = RapidVetUnitOfWork.RecessRepository.GetRecessesForUser(docId, CurrentUser.ActiveClinicId);
            var weeklySchedulesJson = new List<CalenderEntryWebModel>();

            var startOfWeek = DateTime.ParseExact(date.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", CultureInfo.InvariantCulture).StartOfWeek(DayOfWeek.Sunday).Date;
            foreach (var recess in recesses)
            {
                var cEntry = new CalenderEntryWebModel()
                {
                    id = recess.Id,
                    title = "הפסקה",
                    start = startOfWeek.AddDays(recess.DayId).AddHours(recess.Start.Hour).AddMinutes(recess.Start.Minute),
                    end = startOfWeek.AddDays(recess.DayId).AddHours(recess.End.Hour).AddMinutes(recess.End.Minute),
                    color = "green",
                    description = "recess",
                    textColor = "black",
                    cls = "open",
                    background = "black",
                    allDay = false,
                    editable = false,
                    className = "transparentRecess",
                    rendering = "background",
                    comment = ""

                };
                weeklySchedulesJson.Add(cEntry);
            }

            foreach (var dailySchedule in dailySchedules)
            {
                var cEntry = new CalenderEntryWebModel()
                {
                    id = dailySchedule.Id,
                    title = "שעות עבודה",
                    start = startOfWeek.AddDays(dailySchedule.DayId).AddHours(dailySchedule.Start.Hour).AddMinutes(dailySchedule.Start.Minute),
                    end = startOfWeek.AddDays(dailySchedule.DayId).AddHours(dailySchedule.End.Hour).AddMinutes(dailySchedule.End.Minute),
                    color = "lightblue",
                    description = "qworkhours",
                    textColor = "black",
                    allDay = false,
                    className = "transparent",
                    background = "black",
                    cls = "open",
                    editable = false,
                    rendering = "background",
                    comment = ""
                };
                weeklySchedulesJson.Add(cEntry);
            }

            return Json(weeklySchedulesJson, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDailySchedule(int docId, string date)
        {
            var dateParsed = DateTime.ParseExact(date.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            var dailySchedule = RapidVetUnitOfWork.UserRepository.GetDailyScheduleOfUserByDay(docId, dateParsed.DayOfWeek);
            var dailyRecesses = RapidVetUnitOfWork.RecessRepository.GetRecessesOfUserByDay(docId, dateParsed.DayOfWeek);

            var cEntries = new List<CalenderEntryWebModel>();

            if (dailySchedule != null)
            {
                cEntries.Add(new CalenderEntryWebModel()
                        {
                            id = dailySchedule.Id,
                            title = "שעות עבודה",
                            start = dateParsed.Date.AddHours(dailySchedule.Start.Hour).AddMinutes(dailySchedule.Start.Minute),
                            end = dateParsed.Date.AddHours(dailySchedule.End.Hour).AddMinutes(dailySchedule.End.Minute),
                            color = "lightblue",
                            description = "qworkhours",
                            textColor = "black",
                            allDay = false,
                            className = "transparent",
                            editable = false,
                            background = "black",
                            cls = "open",
                            rendering = "background",
                            comment = ""
                        });
            }
            foreach (var dailyRecess in dailyRecesses)
            {
                cEntries.Add(new CalenderEntryWebModel()
                {
                    id = dailyRecess.Id,
                    title = "הפסקה",
                    start = dateParsed.Date.AddHours(dailyRecess.Start.Hour).AddMinutes(dailyRecess.Start.Minute),
                    end = dateParsed.Date.AddHours(dailyRecess.End.Hour).AddMinutes(dailyRecess.End.Minute),
                    color = "green",
                    description = "recess",
                    textColor = "black",
                    className = "transparentRecess",
                    allDay = false,
                    editable = false,
                    background = "black",
                    cls = "open",
                    rendering = "background",
                    comment = ""
                });
            }

            return Json(cEntries, JsonRequestBehavior.AllowGet);
        }


        public ActionResult CalenderEntryMetaData()
        {
            string clientName = "";
            string ClientId = "";

            if (Session["ClientName"] != null)
            {
                clientName = Session["ClientName"].ToString();

                Session["ClientName"] = null;
            }

            if (Session["ClientId"] != null)
            {
                ClientId = Session["ClientId"].ToString();

                Session["ClientId"] = null;
            }

            var activeClinicId = CurrentUser.ActiveClinicId;
            var activeClinicGroupId = CurrentUser.ActiveClinicGroupId;
            var activeUserId = CurrentUser.Id;

            var rooms = RapidVetUnitOfWork.RoomRepository.GetRoomsForClinic(activeClinicId).OrderBy(r => r.Name);
            var users = RapidVetUnitOfWork.UserRepository.GetListByClinicGroup(activeClinicGroupId).OrderBy(u => u.FirstName).ToList();
            var clients = RapidVetUnitOfWork.ClientRepository.GetAllClients(activeClinicId).Take(100).OrderBy(c => c.FirstName).ToList();
            var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(activeClinicId).OrderBy(d => d.FirstName);

            var defaultDuration = CurrentUser.ActiveClinic.EntryDuration ?? 45;

            return new JsonResult()
            {
                Data = new { Rooms = rooms, Users = users, Doctors = doctors, DefaultDuration = defaultDuration, CurrentUser = activeUserId, ClientName = clientName, ClientId = ClientId },//, Clients = clients },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public ActionResult GetCalenderEntry(int calenderEntryId)
        {
            var calenderEntry = RapidVetUnitOfWork.CalenderRepository.GetCalenderEntry(calenderEntryId, true);
            var calenderEntryJson = new CalenderEntryJson
                {
                    entryId = calenderEntry.Id,
                    date = calenderEntry.Date.ToString("dd/MM/yyyy"),
                    time = calenderEntry.Date.ToString("HH:mm"),
                    duration = calenderEntry.Duration,
                    roomId = calenderEntry.RoomId,
                    doctorId = calenderEntry.DoctorId,
                    byUserId = calenderEntry.CreatedByUserId,
                    clientId = calenderEntry.ClientId,
                    patientId = calenderEntry.PatientId,
                    comments = calenderEntry.Comments,
                    color = calenderEntry.Color,
                    title = calenderEntry.Title,
                    isNewClient = calenderEntry.IsNewClient,
                    isNoShow = calenderEntry.IsNoShow,
                    isGeneralEntry = calenderEntry.IsGeneralEntry,
                    clientName = calenderEntry.IsGeneralEntry ? "" : calenderEntry.Client.Name
                };

            return new JsonResult()
            {
                Data = calenderEntryJson,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }


        public ActionResult SaveCalenderEntry(string calenderEntryString)
        {
            var jsonResult = new JsonResult() { JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            var status = new OperationStatus() { Success = true };
            if (!string.IsNullOrWhiteSpace(calenderEntryString))
            {
                var jss = new JavaScriptSerializer();
                var calenderEntryJson = jss.Deserialize<CalenderEntryJson>(calenderEntryString);

                var format = "dd/MM/yyyy H:mm";
                var unparsed = string.Format("{0} {1}", calenderEntryJson.date, calenderEntryJson.time);
                var date = DateTime.MinValue;
                DateTime.TryParseExact(unparsed, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
                if (date == DateTime.MinValue)
                {
                    date = DateTime.Now;
                }

                var isBlocked = RapidVetUnitOfWork.CalenderRepository.IsDateRangeBlockedForDoctor(CurrentUser.ActiveClinicId, calenderEntryJson.doctorId, date, date.AddMinutes(calenderEntryJson.duration), calenderEntryJson.entryId);
                if (isBlocked)
                {
                    SetErrorMessage("נתקלת ביום חסום");
                    jsonResult.Data = false;
                    return jsonResult;
                }

                if (calenderEntryJson.entryId == 0)
                {
                    var calenderEntry = new CalenderEntry()
                        {
                            Date = date,
                            ClinicId = CurrentUser.ActiveClinicId,
                            Duration = calenderEntryJson.duration,
                            RoomId = calenderEntryJson.roomId,
                            CreatedByUserId = calenderEntryJson.byUserId,
                            DoctorId = calenderEntryJson.doctorId,
                            ClientId = calenderEntryJson.clientId,
                            PatientId = calenderEntryJson.patientId,
                            Comments = calenderEntryJson.comments,
                            Color = calenderEntryJson.color,
                            Title = !string.IsNullOrEmpty(calenderEntryJson.title) ? calenderEntryJson.title : null,
                            IsNewClient = calenderEntryJson.isNewClient,
                            IsNoShow = null,
                            IsGeneralEntry = calenderEntryJson.isGeneralEntry
                        };
                    status = RapidVetUnitOfWork.CalenderRepository.CreateCalenderEntry(calenderEntry);
                }
                else
                {
                    var calenderEntry = RapidVetUnitOfWork.CalenderRepository.GetCalenderEntry(calenderEntryJson.entryId);
                    if (calenderEntry == null)
                    {
                        return jsonResult;
                    }
                    calenderEntry.Date = date;
                    calenderEntry.Duration = calenderEntryJson.duration;
                    calenderEntry.RoomId = calenderEntryJson.roomId;
                    calenderEntry.CreatedByUserId = calenderEntryJson.byUserId;
                    calenderEntry.DoctorId = calenderEntryJson.doctorId;
                    calenderEntry.ClientId = calenderEntryJson.clientId;
                    calenderEntry.PatientId = calenderEntryJson.patientId;
                    calenderEntry.Comments = calenderEntryJson.comments;
                    calenderEntry.Color = calenderEntryJson.color;
                    calenderEntry.Title = !string.IsNullOrEmpty(calenderEntryJson.title) ? calenderEntryJson.title : null;
                    calenderEntry.IsNewClient = calenderEntryJson.isNewClient;
                    calenderEntry.IsNoShow = calenderEntryJson.isNoShow;
                    calenderEntry.IsGeneralEntry = calenderEntryJson.isGeneralEntry;

                    status = RapidVetUnitOfWork.CalenderRepository.UpdateCalenderEntry(calenderEntry);
                }
            }
            if (status.Success)
            {
                SetSuccessMessage("הפגישה נשמרה בהצלחה");
            }
            else
            {
                SetErrorMessage("");
            }
            jsonResult.Data = status.Success;

            ViewBag.EntryId = "0";
            Session["EntryId"] = "0";

            return jsonResult;
        }


        public ActionResult UpdateCalenderEntryMovement(int calenderEntryId, string dateStart, string dateEnd)
        {
            var jsonResult = new JsonResult() { JsonRequestBehavior = JsonRequestBehavior.AllowGet };

            var parsedDateStart = DateTime.ParseExact(dateStart.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            var parsedDateEnd = DateTime.ParseExact(dateEnd.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", CultureInfo.InvariantCulture);

            var calenderEntry = RapidVetUnitOfWork.CalenderRepository.GetCalenderEntry(calenderEntryId, true);
            if (calenderEntry == null)
            {
                SetErrorMessage();
                return View("Error");
            }
            var contentNew = String.Format("{0}, מועד חדש - {1}", calenderEntry.IsGeneralEntry ? calenderEntry.Title : calenderEntry.Client.Name, calenderEntry.Date.ToString("dd/MM/yyyy HH:mm"));
            var contentOld = String.Format("מועד ישן - {0}", parsedDateStart.ToString("dd/MM/yyyy HH:mm"));
            var isBlocked = RapidVetUnitOfWork.CalenderRepository.IsDateRangeBlockedForDoctor(CurrentUser.ActiveClinicId, calenderEntry.DoctorId, parsedDateStart, parsedDateEnd, calenderEntryId);
            if (isBlocked)
            {
                SetErrorMessage("נתקלת ביום חסום");
                jsonResult.Data = false;
            }
            calenderEntry.Date = parsedDateStart;
            calenderEntry.Duration = (int)(parsedDateEnd - parsedDateStart).TotalMinutes;
            calenderEntry.Updated = DateTime.Now;
            RapidVetUnitOfWork.CalenderRepository.UpdateCalenderEntry(calenderEntry);
            jsonResult.Data = true;
            RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, "תיק לקוח - הזזת פגישה", contentNew, contentOld);
            return jsonResult;
        }

        public ActionResult AddBlockedDay(int DoctorId, string Comment, string DayDate)
        {
            var status = RapidVetUnitOfWork.CalenderRepository.AddBlockedDay(
                    new BlockedDay()
                        {
                            DoctorId = DoctorId,
                            ClinicId = CurrentUser.ActiveClinicId,
                            Comment = Comment,
                            DayDate = DateTime.ParseExact(DayDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                        });
            return new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public ActionResult RemoveBlockedDay(int blockedDayId)
        {
            var status = RapidVetUnitOfWork.CalenderRepository.RemoveBlockedDay(blockedDayId);
            if (status.Success)
            {
                SetSuccessMessage("יום חסום הוסר בהצלחה");
            }
            else
            {
                SetErrorMessage();
            }
            return new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public ActionResult IsDateRangeBlockedForDoctor(int doctorId, string dateStart, string dateEnd, int? cEntryId)
        {
            var parsedDateStart = DateTime.ParseExact(dateStart.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", CultureInfo.InvariantCulture);//DateTime.Parse(dateStart);//
            var parsedDateEnd = DateTime.ParseExact(dateEnd.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            var isBlocked = RapidVetUnitOfWork.CalenderRepository.IsDateRangeBlockedForDoctor(CurrentUser.ActiveClinicId, doctorId, parsedDateStart, parsedDateEnd, cEntryId);
            if (isBlocked)
            {
                SetErrorMessage("נתקלת ביום חסום");
            }
            return new JsonResult()
            {
                Data = isBlocked,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public ActionResult GetBlockedIdForDoctor(int doctorId, string date)
        {
            var parsedDate = DateTime.ParseExact(date.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            var blockDayId = RapidVetUnitOfWork.CalenderRepository.GetBlockedDayIdForDoc(CurrentUser.ActiveClinicId, doctorId, parsedDate);
            return new JsonResult()
            {
                Data = blockDayId,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public ActionResult ClientCreated(int? id)
        {
            ViewBag.ClientId = id;
            ViewBag.ClientName = RapidVetUnitOfWork.ClientRepository.GetClient(id.Value).Name;
            return View();
        }

        public ActionResult Memos(string date, bool toPrint = false)
        {
            var parsedDate = DateTime.ParseExact(date.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            var calenderEntries = RapidVetUnitOfWork.CalenderRepository.GetAllCalenderEntriesFromDayDate(CurrentUser.ActiveClinicId, parsedDate);
            ViewBag.DateString = parsedDate.ToString("dd/MM/yyyy");
            ViewBag.ToPrint = toPrint;
            ViewBag.Sms = IsSmsActive() &&
                         !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.CalenderEntrySmsTemplate);
            ViewBag.Email = IsEmailActive() && !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.CalenderEntryEmailSubject) && !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.CalenderEntryEmailTemplate);

            return View(calenderEntries);
        }

        string emailTemplatePrefix = "<div style=\"position:relative;display:block;margin:auto;direction:rtl;\">";
        string emailTemplatePostfix = "</div>";


        public ActionResult SendCalenderEntriesMemos(string idsString, string sendingMethod)
        {

            int[] ids = Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(idsString).ToArray();
            var result = "";
            var calenderEntries = ids.Select(id => RapidVetUnitOfWork.CalenderRepository.GetCalenderEntry(id, true)).ToList();
            int messagesCounter = 0;

            if (sendingMethod == "email")
            {
                var template = CurrentUser.ActiveClinicGroup.CalenderEntryEmailTemplate;
                var subject = CurrentUser.ActiveClinicGroup.CalenderEntryEmailSubject;

                template = template.Replace("&lt;", "<");
                template = template.Replace("&gt;", ">");
                template = template.Replace("\r\n", "<br />");

                subject = subject.Replace("&lt;", "<");
                subject = subject.Replace("&gt;", ">");

                var mailClient = getMailClient();
                string from = string.Format("{0}<{1}>", !string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.EmailBusinessName) ? CurrentUser.ActiveClinicGroup.EmailBusinessName : CurrentUser.ActiveClinic.Name,
                                                    CurrentUser.ActiveClinicGroup.EmailAddress);

                foreach (var entry in calenderEntries)
                {
                    int counter = 0;
                    RestRequest request = new RestRequest();
                    request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MAILGUN_RAPID_DOMAIN"], ParameterType.UrlSegment);
                    request.Resource = "{domain}/messages";
                    request.AddParameter("from", from);
                    request.AddParameter("subject", GetMessage(subject, entry));
                    request.AddParameter("html", emailTemplatePrefix + GetMessage(template, entry) + emailTemplatePostfix);
                    request.Method = Method.POST;

                    StringBuilder emailAdress = new StringBuilder();

                    foreach (var email in entry.Client.Emails)
                    {
                        request.AddParameter("to", email.Name);
                        emailAdress.Append(email.Name);
                        emailAdress.Append("; ");
                        counter++;
                    }

                    if (counter > 0)
                    {
                        var message = mailClient.Execute(request);
                        if ((int)message.StatusCode == 200)
                        {
                            RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, counter, MessageType.Email);
                            string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                 Resources.Auditing.SendingModule, Resources.Auditing.CalendarReminders,
                                                                 Resources.Auditing.ClientName, entry.Client.Name,
                                                                 Resources.Auditing.Address, emailAdress.ToString(),
                                                                 Resources.Auditing.MailSubject, GetMessage(subject, entry),
                                                                 Resources.Auditing.ReturnCode, (int)message.StatusCode + " - " + message.StatusCode);

                            RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.MailSent, logMessage, "");
                            messagesCounter += counter;
                        }
                    }
                }

                SetSuccessMessage(string.Format(Resources.LetterTemplate.EmailSuccessMessage, messagesCounter, calenderEntries.Count() - messagesCounter < 0 ? 0 : calenderEntries.Count() - messagesCounter, calenderEntries.Count()));
            }
            if (sendingMethod == "sms")
            {
                var template = CurrentUser.ActiveClinicGroup.CalenderEntrySmsTemplate;

                foreach (var entry in calenderEntries)
                {
                    var message = GetMessage(template, entry);
                    foreach (var phone in entry.Client.Phones.Where(p => p.PhoneTypeId == (int)PhoneTypeEnum.Mobile))
                    {
                        var sms = new Unicell.Sms(CurrentUser.ActiveClinicGroup.UnicellUserName, CurrentUser.ActiveClinicGroup.UnicellPassword, phone.PhoneNumber, message, string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.SignatureSmsTemplate) ? "" : CurrentUser.ActiveClinicGroup.SignatureSmsTemplate);
                        var sendResult = sms.Send();
                        if (sendResult == false)
                        {
                            return new JsonResult()
                            {
                                Data = "שגיאה בשליחת הודעת טקסט",
                                JsonRequestBehavior = JsonRequestBehavior.AllowGet
                            };
                        }
                        else
                        {
                            RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, 1, MessageType.Sms);

                            string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                                Resources.Auditing.SendingModule, Resources.Auditing.CalendarReminders,
                                                                Resources.Auditing.ClientName, entry.Client.Name,
                                                                Resources.Auditing.PhoneNumber, phone.PhoneNumber,
                                                                Resources.Auditing.SmsContent, message,
                                                                Resources.Auditing.ReturnCode, sendResult);

                            RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.SmsSent, logMessage, "");
                            messagesCounter += 1;

                        }

                    }
                }

                SetSuccessMessage(string.Format(Resources.LetterTemplate.SmsSuccessMessage, messagesCounter, calenderEntries.Count() - messagesCounter < 0 ? 0 : calenderEntries.Count() - messagesCounter, calenderEntries.Count()));
            }

            return new JsonResult()
            {
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        private string GetMessage(string template, CalenderEntry entry)
        {
            //any template keyword change should also be updated in ClinicGroup/GetCalendarTemplateKeywords!
            var builder = new StringBuilder(template);
            builder.Replace(getTemplateKeyWord("ClientFirstName"), entry.Client.FirstName);
            builder.Replace(getTemplateKeyWord("ClientLastName"), entry.Client.LastName);
            builder.Replace(getTemplateKeyWord("ClientName"), entry.Client.Name);
            builder.Replace(getTemplateKeyWord("AppointmentTime"), entry.Date.ToShortTimeString());
            builder.Replace(getTemplateKeyWord("ClientHomePhone"), RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(entry.Client.Phones, PhoneTypeEnum.Home));
            builder.Replace(getTemplateKeyWord("ClientWorkPhone"), RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(entry.Client.Phones, PhoneTypeEnum.Work));
            builder.Replace(getTemplateKeyWord("ClientMobilePhone"), RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(entry.Client.Phones, PhoneTypeEnum.Mobile));
            builder.Replace(getTemplateKeyWord("ClinicAddress"), CurrentUser.ActiveClinic.Address);
            builder.Replace(getTemplateKeyWord("ClinicName"), CurrentUser.ActiveClinic.Name);
            builder.Replace(getTemplateKeyWord("ClinicPhone"), CurrentUser.ActiveClinic.Phone);
            builder.Replace(getTemplateKeyWord("ClinicFax"), CurrentUser.ActiveClinic.Fax);
            builder.Replace(getTemplateKeyWord("ClinicEmail"), CurrentUser.ActiveClinic.Email);
            if (entry.Patient != null)
            {
                builder.Replace(getTemplateKeyWord("PatientName"), entry.Patient.Name);
            }
            else
            {
                builder.Replace(getTemplateKeyWord("PatientName"), "");
            }

            builder.Replace(getTemplateKeyWord("Comment"), entry.Comments);
            builder.Replace(getTemplateKeyWord("DoctorName"), entry.Doctor.Name);
            return builder.ToString();
        }

        public JsonResult SearchCalenderyEntries(int? doctorId, int? clientId, int searchTimeType, bool searchCancelled, string fromDate, string toDate)
        {
            var calenderEntries = RapidVetUnitOfWork.CalenderRepository.GetCalenderEntriesByParams(CurrentUser.ActiveClinicId, doctorId, clientId, searchTimeType, searchCancelled, fromDate, toDate).OrderBy(c => c.Date);
            var calenderEntriesJson = calenderEntries.Select(calenderEntry => new CalenderEntryJson()
            {
                entryId = calenderEntry.Id,
                date = calenderEntry.Date.ToString("dd/MM/yyyy"),
                time = calenderEntry.Date.ToString("HH:mm"),
                duration = calenderEntry.Duration,
                doctorId = calenderEntry.DoctorId,
                byUserId = calenderEntry.CreatedByUserId,
                comments = calenderEntry.Comments,
                color = Resources.Enums.DayOfWeek.ResourceManager.GetString(calenderEntry.Date.DayOfWeek.ToString()),
                title = calenderEntry.Doctor.Name
            }).ToList();

            return Json(calenderEntriesJson, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveCalenderEntry(int calenderEntryId)
        {
            var meeting = RapidVetUnitOfWork.CalenderRepository.GetCalenderEntry(calenderEntryId, true);
            var content = String.Format("{0}, פגישה - {1}", meeting.IsGeneralEntry ? String.Format("פגישת כללית - {0}", meeting.Title) : String.Format("לקוח - {0}", meeting.Client.Name), meeting.Date.ToString("dd/MM/yyyy HH:mm"));
            var status = RapidVetUnitOfWork.CalenderRepository.RemoveCalenderEntry(calenderEntryId);
            if (status.Success)
            {
                SetSuccessMessage("הפעולה התבצעה בהצלחה");
                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, "תיק לקוח - ביטול פגישה", content, "old");
                var data = filterWaitingListByDeletedMeeting(GetWaitingList(), meeting);
                if (data != null && data.Count > 0)
                    return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                SetErrorMessage();
            }
            return Json(JsonRequestBehavior.AllowGet);
        }

        private List<WaitingListItemJson> filterWaitingListByDeletedMeeting(List<WaitingListItemJson> list, CalenderEntry meeting)
        {
            try
            {
                if (meeting == null || list == null || list.Count == 0)
                    return null;

                var d = meeting.Date;
                var tFrom = d.TimeOfDay;
                var tTo = d.TimeOfDay.Add(new TimeSpan(0, meeting.Duration, 0));
                return list.FindAll(x => dayIs(x, d.DayOfWeek) && timeFillIn(TimeSpan.Parse(x.FromTime), TimeSpan.Parse(x.ToTime), x.AppointmentDurationInMinutes, tFrom, tTo)).OrderByDescending(x => x.UrgencyTypeId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        private bool timeFillIn(TimeSpan waitingFrom, TimeSpan waitingTo, int duration, TimeSpan from, TimeSpan to)
        {
            if (waitingFrom >= to || waitingTo <= from)
                return false;

            TimeSpan minFrom = waitingFrom < from ? from : waitingFrom;
            TimeSpan firstTo = minFrom.Add(new TimeSpan(0, duration, 0));
            return firstTo <= to;
        }

        private bool dayIs(WaitingListItemJson x, DayOfWeek dayOfWeek)
        {
            if (x == null)
                return false;

            switch (dayOfWeek)
            {
                case DayOfWeek.Friday:
                    return x.IsFriday;
                case DayOfWeek.Monday:
                    return x.IsMonday;
                case DayOfWeek.Saturday:
                    return x.IsSaturday;
                case DayOfWeek.Sunday:
                    return x.IsSunday;
                case DayOfWeek.Thursday:
                    return x.IsThursday;
                case DayOfWeek.Tuesday:
                    return x.IsTuesday;
                case DayOfWeek.Wednesday:
                    return x.IsWednesday;
            }
            return false;
        }

        public ActionResult PrintCalenderEntries(int? doctorId, int? clientId, int searchTimeType, string fromDate, string toDate)
        {
            var calenderEntries = RapidVetUnitOfWork.CalenderRepository.GetCalenderEntriesByParams(CurrentUser.ActiveClinicId, doctorId, clientId, searchTimeType, false, fromDate, toDate).OrderBy(c => c.Date).ToList();

            if (calenderEntries.Count > 0)
            {
                return View(calenderEntries);
            }
            else
            {
                return View("NoData");
            }
        }

        public ActionResult GetWaitingListItems()
        {
            var data = GetWaitingList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveWaitingListItem(string waitingListItemString)
        {
            var jsonResult = new JsonResult() { JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            var status = new OperationStatus() { Success = true };
            if (!string.IsNullOrWhiteSpace(waitingListItemString))
            {
                var jss = new JavaScriptSerializer();
                var waitingListItemJson = jss.Deserialize<WaitingListItemJson>(waitingListItemString);

                if (waitingListItemJson.Id == 0)
                {
                    var waitingListItem = new WaitingListItem()
                    {
                        ClinicId = CurrentUser.ActiveClinicId,
                        DoctorId = waitingListItemJson.DoctorId,
                        ClientId = waitingListItemJson.ClientId,
                        Comments = waitingListItemJson.Comments,
                        Id = waitingListItemJson.Id,
                        IsSunday = waitingListItemJson.IsSunday,
                        IsMonday = waitingListItemJson.IsMonday,
                        IsTuesday = waitingListItemJson.IsTuesday,
                        IsWednesday = waitingListItemJson.IsWednesday,
                        IsThursday = waitingListItemJson.IsThursday,
                        IsFriday = waitingListItemJson.IsFriday,
                        IsSaturday = waitingListItemJson.IsSaturday,
                        FromTime = waitingListItemJson.FromTime,
                        ToTime = waitingListItemJson.ToTime,
                        AppointmentDurationInMinutes = waitingListItemJson.AppointmentDurationInMinutes,
                        UrgencyTypeId = waitingListItemJson.UrgencyTypeId,
                        DateStamp = DateTime.Now,
                        PatientID = waitingListItemJson.PatientId
                    };
                    status = RapidVetUnitOfWork.WaitingListRepository.Create(waitingListItem);
                }
                else
                {
                    var waitingListItem = RapidVetUnitOfWork.WaitingListRepository.GetWaitingListItem(waitingListItemJson.Id);
                    if (waitingListItem == null)
                    {
                        return jsonResult;
                    }
                    waitingListItem.ClinicId = CurrentUser.ActiveClinicId;
                    waitingListItem.DoctorId = waitingListItemJson.DoctorId;
                    waitingListItem.ClientId = waitingListItemJson.ClientId;
                    waitingListItem.Comments = waitingListItemJson.Comments;
                    waitingListItem.IsSunday = waitingListItemJson.IsSunday;
                    waitingListItem.IsMonday = waitingListItemJson.IsMonday;
                    waitingListItem.IsTuesday = waitingListItemJson.IsTuesday;
                    waitingListItem.IsWednesday = waitingListItemJson.IsWednesday;
                    waitingListItem.IsThursday = waitingListItemJson.IsThursday;
                    waitingListItem.IsFriday = waitingListItemJson.IsFriday;
                    waitingListItem.IsSaturday = waitingListItemJson.IsSaturday;
                    waitingListItem.FromTime = waitingListItemJson.FromTime;
                    waitingListItem.ToTime = waitingListItemJson.ToTime;
                    waitingListItem.AppointmentDurationInMinutes = waitingListItemJson.AppointmentDurationInMinutes;
                    waitingListItem.UrgencyTypeId = waitingListItemJson.UrgencyTypeId;
                    waitingListItem.PatientID = waitingListItemJson.PatientId;

                    status = RapidVetUnitOfWork.WaitingListRepository.Update(waitingListItem);
                }
            }
            if (status.Success)
            {
                SetSuccessMessage("הממתין נשמר בהצלחה");
            }
            else
            {
                SetErrorMessage();
            }
            jsonResult.Data = status.Success;
            return jsonResult;
        }

        public ActionResult RemoveWaitingListItem(int waitingListItemId)
        {
            RapidVetUnitOfWork.WaitingListRepository.Remove(waitingListItemId);
            return Json(JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadCsv(int? doctorId, int? clientId, int searchTimeType, string fromDate, string toDate)
        {
            var culture = new System.Globalization.CultureInfo(Resources.Calendar.CultureInfo);
            var calenderEntries = RapidVetUnitOfWork.CalenderRepository.GetCalenderEntriesByParams(CurrentUser.ActiveClinicId, doctorId, clientId, searchTimeType, false, fromDate, toDate).OrderBy(c => c.Date).ToList();
            var calenderEntriesCsv = calenderEntries.Select(calenderEntry => new CsvCalenderEntry()
            {
                date = calenderEntry.Date.ToString("dd/MM/yyyy"),
                time = calenderEntry.Date.ToString("HH:mm"),
                duration = calenderEntry.Duration,
                comments = calenderEntry.Comments,
                day = culture.DateTimeFormat.GetDayName(calenderEntry.Date.DayOfWeek),
                doctorName = calenderEntry.Doctor.Name
            }).ToList();

            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8 };
            var csv = new CsvWriter(writer, csvConfig);
            csv.Configuration.HasHeaderRecord = false;

            if (calenderEntriesCsv.Count > 0)
            {

                csv.WriteRecord(new
                {
                    a1 = "",
                    a2 = "",
                    a3 = "",
                    a4 = "",
                    a5 = "",
                    a6 = ""
                });

                csv.WriteRecord(new
                {
                    a1 = Resources.Calendar.EntriesScueduledForClient + calenderEntries.First().Client.Name,
                    a2 = "",
                    a3 = "",
                    a4 = "",
                    a5 = "",
                    a6 = ""
                });

                csv.WriteRecord(new
                {
                    a1 = "",
                    a2 = "",
                    a3 = "",
                    a4 = "",
                    a5 = "",
                    a6 = ""
                });

                csv.WriteRecord(new
                {
                    a1 = Resources.Calendar.Date,
                    a2 = Resources.Calendar.Day,
                    a3 = Resources.Calendar.Hour,
                    a4 = Resources.Calendar.MeetingDuration,
                    a5 = Resources.Calendar.Doctor,
                    a6 = Resources.Calendar.Comments
                });

                csv.WriteRecords(calenderEntriesCsv);
                writer.Flush();
                stream.Position = 0;

                return File(stream, "text/csv", "CalenderEntriesResults.csv");
            }
            else
            {
                return View("NoData");
            }
        }

        public ActionResult GetJewishDate(string date)
        {
            var parsedDate = DateTime.ParseExact(date.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            var fullString = string.Format("<span>{0} - {1}<br/>{2}</span>",
                                           CalenderHelper.GetHebrewJewishDayOfWeekString(parsedDate),
                                           parsedDate.ToShortDateString(),
                                           CalenderHelper.GetHebrewJewishDateString(parsedDate));
            return Json(fullString, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetWeekJewishDate(string date)
        {
            var parsedDate = DateTime.ParseExact(date.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            var weekStart = parsedDate.StartOfWeek(DayOfWeek.Sunday);
            string[] dateStrings = new string[7];
            for (int i = 0; i < 7; i++)
            {
                dateStrings[i] = CalenderHelper.GetHebrewJewishDayOfWeekString(weekStart.AddDays(i)) + " - " + weekStart.AddDays(i).ToShortDateString() + "<br/>" + CalenderHelper.GetHebrewJewishDateString(weekStart.AddDays(i));
            }

            return Json(dateStrings, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetAsNoShow(int calenderEntryId)
        {
            var jsonResult = new JsonResult();
            var calenderEntry = RapidVetUnitOfWork.CalenderRepository.GetCalenderEntry(calenderEntryId);
            calenderEntry.IsNoShow = true;
            var status = RapidVetUnitOfWork.Save();
            if (status.Success)
            {
                SetSuccessMessage("לקוח הפגישה נשמר כלא הופיע");
            }
            else
            {
                SetErrorMessage();
            }
            jsonResult.Data = status.Success;
            return jsonResult;
        }

        public ActionResult SetAsShown(int calenderEntryId)
        {
            var jsonResult = new JsonResult();
            var calenderEntry = RapidVetUnitOfWork.CalenderRepository.GetCalenderEntry(calenderEntryId);
            calenderEntry.IsNoShow = false;
            var status = RapidVetUnitOfWork.Save();

            if (status.Success)
            {
                SetSuccessMessage("לקוח הפגישה נשמר כהופיע");
            }
            else
            {
                SetErrorMessage();
            }
            jsonResult.Data = status.Success;
            return jsonResult;
        }

        public ActionResult PrintWaitingList(int? doctorId)
        {
            var data = GetWaitingList();
            var model = new List<WaitingListItemJson>();
            if (doctorId.HasValue && doctorId.Value > 0)
            {
                model = data.Where(d => d.DoctorId == doctorId.Value).ToList();
            }
            else
            {
                model = data;
            }

            return View(model);
        }

        private List<WaitingListItemJson> GetWaitingList()
        {
            var waitingListItems = RapidVetUnitOfWork.WaitingListRepository.GetWaitingListForClinic(CurrentUser.ActiveClinicId);

            var waitingListItemsJson = (from waitingListItem in waitingListItems
                                        let homePhone = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(waitingListItem.Client.Phones, PhoneTypeEnum.Home)
                                        let workPhone = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(waitingListItem.Client.Phones, PhoneTypeEnum.Work)
                                        let mobilePhone = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(waitingListItem.Client.Phones, PhoneTypeEnum.Mobile)
                                        select new WaitingListItemJson()
                                        {
                                            Id = waitingListItem.Id,
                                            ClinicId = waitingListItem.ClinicId,
                                            ClientId = waitingListItem.ClientId,
                                            IsSunday = waitingListItem.IsSunday,
                                            IsMonday = waitingListItem.IsMonday,
                                            IsTuesday = waitingListItem.IsTuesday,
                                            IsWednesday = waitingListItem.IsWednesday,
                                            IsThursday = waitingListItem.IsThursday,
                                            IsFriday = waitingListItem.IsFriday,
                                            IsSaturday = waitingListItem.IsSaturday,
                                            FromTime = waitingListItem.FromTime,
                                            ToTime = waitingListItem.ToTime,
                                            AppointmentDurationInMinutes = waitingListItem.AppointmentDurationInMinutes,
                                            DoctorId = waitingListItem.DoctorId ?? 0,
                                            DoctorName = waitingListItem.Doctor == null ? "" : waitingListItem.Doctor.Name,
                                            Comments = waitingListItem.Comments,
                                            ClientName = waitingListItem.Client.Name,
                                            PatientName = waitingListItem.Animal == null ? String.Empty : waitingListItem.Animal.Name,
                                            PatientId = waitingListItem.PatientID,
                                            HomePhone = homePhone,
                                            WorkPhone = workPhone,
                                            MobilePhone = mobilePhone,
                                            UrgencyTypeId = waitingListItem.UrgencyTypeId,
                                            DateStamp = waitingListItem.DateStamp.ToString("dd/MM/yyyy H:mm")
                                        }).ToList();

            return waitingListItemsJson;
        }

        public ActionResult CreateNewWaitingListItemFromCalendarEntry(int calenderEntryId)
        {
            var calenderEntry = RapidVetUnitOfWork.CalenderRepository.GetCalenderEntry(calenderEntryId);

            var waitingListItemJson = new WaitingListItemJson()
            {
                ClinicId = CurrentUser.ActiveClinicId,
                DoctorId = calenderEntry.DoctorId,
                ClientId = calenderEntry.ClientId != null ? calenderEntry.ClientId.Value : 0,
                Comments = calenderEntry.Comments,
                IsSunday = calenderEntry.Date.DayOfWeek == DayOfWeek.Sunday,
                IsMonday = calenderEntry.Date.DayOfWeek == DayOfWeek.Monday,
                IsTuesday = calenderEntry.Date.DayOfWeek == DayOfWeek.Tuesday,
                IsWednesday = calenderEntry.Date.DayOfWeek == DayOfWeek.Wednesday,
                IsThursday = calenderEntry.Date.DayOfWeek == DayOfWeek.Thursday,
                IsFriday = calenderEntry.Date.DayOfWeek == DayOfWeek.Friday,
                IsSaturday = calenderEntry.Date.DayOfWeek == DayOfWeek.Saturday,
                FromTime = calenderEntry.Date.ToString("HH:mm"),
                ToTime = calenderEntry.Date.AddMinutes(calenderEntry.Duration).ToString("HH:mm"),
                AppointmentDurationInMinutes = calenderEntry.Duration,
                UrgencyTypeId = 0,
            };

            return Json(waitingListItemJson, JsonRequestBehavior.AllowGet);
        }
    }
    //DateTime Helper
    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }
    }
}