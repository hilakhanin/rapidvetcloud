﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using RapidVet.Enums;
using RapidVet.Enums.Finances;
using RapidVet.Model.Clients;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.InternalSettlements;
using RapidVet.Model.Patients;
using RapidVet.Repository.Clinics;
using RapidVet.WebModels;
using RapidVet.WebModels.Clients;
using RapidVet.WebModels.Clients.Details;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Patients;
//using Syncfusion.DocIO;
//using Syncfusion.DocIO.DLS;
using RapidVet.FilesAndStorage;
using System.Text;
using RapidVet.Model.Clinics;

namespace RapidVet.Web.Controllers
{
    public class ClientsController : BaseController
    {
        static string archivePath = System.Configuration.ConfigurationManager.AppSettings["ARCHIVE_PATH"];

        public ActionResult Index()
        {
            var clients = RapidVetUnitOfWork.ClientRepository.GetAllClients(CurrentUser.ActiveClinicId);
            var model = clients.Select(AutoMapper.Mapper.Map<Client, ClientIndexModel>).ToList();

            return View(model);
        }

        //id is clinicId
        [HttpGet]
        public ActionResult Create(bool isDirectedFromCalender = false)
        {
            if (RapidVetUnitOfWork.ClinicGroupRepository.ClinicGroupHasClinics(CurrentUser.ActiveClinicGroupId))
            {
                var model = new ClientCreateModel()
                    {
                        TariffId = CurrentUser.ActiveClinic.DefualtTariffId,
                        StatusId = CurrentUser.ActiveClinic.DefaultNewClientStatusId,
                        Addresses = new Collection<AddressModel>() { new AddressModel() { CityId = CurrentUser.ActiveClinic.CityId } },
                        //  Active = true
                    };

                //i'm a the current user and a doctor
                if (CurrentUser.IsDoctor)
                {
                    model.VetId = CurrentUser.Id;
                }//current user's default dr
                else if (CurrentUser.DefaultDrId != null)
                {
                    model.VetId = CurrentUser.DefaultDrId.Value;
                }
                else if (CurrentUser.ActiveClinic.MainVetId.HasValue) //clinic's default dr
                {
                    model.VetId = CurrentUser.ActiveClinic.MainVetId.Value;
                }
                else
                {
                    model.VetId = 0;
                }

                return View(model);
            }
            return RedirectToAction("EditMain", "ClinicGroups");
        }

        [HttpPost]
        public ActionResult SendSms(string phone, string message, int clientId)
        {
            var client = RapidVetUnitOfWork.ClientRepository.GetClient(clientId);
            var sms = new RapidVet.Unicell.Sms(CurrentUser.ActiveClinicGroup.UnicellUserName,
                                               CurrentUser.ActiveClinicGroup.UnicellPassword, phone, message, string.IsNullOrWhiteSpace(CurrentUser.ActiveClinicGroup.SignatureSmsTemplate) ? "" : CurrentUser.ActiveClinicGroup.SignatureSmsTemplate);
            var result = sms.Send();

            if (result)
            {
                RapidVetUnitOfWork.MessageDistributionRepository.Increment(CurrentUser.ActiveClinicId, 1, MessageType.Sms);

                string logMessage = string.Format("{0}: {1}<br />{2}: {3}<br />{4}: {5}<br />{6}: {7}<br />{8}: {9}",
                                                    Resources.Auditing.SendingModule, Resources.Auditing.ClientFile,
                                                    Resources.Auditing.ClientName, client.Name,
                                                    Resources.Auditing.PhoneNumber, phone,
                                                    Resources.Auditing.SmsContent, message,
                                                    Resources.Auditing.ReturnCode, result);

                RapidVetUnitOfWork.AuditingRepository.Audit(CurrentUser, Resources.Auditing.SmsSent, logMessage, "");

            }

            return new JsonResult() { Data = result };
        }

        [HttpPost]
        public ActionResult Create(ClientCreateModel model)
        {
            model.Emails = GetEmailModelList();
            var emails = model.Emails.Select(AutoMapper.Mapper.Map<EmailModel, Email>).ToList();

            model.Phones = GetPhoneModelList();
            var phones = model.Phones.Select(AutoMapper.Mapper.Map<PhoneModel, Phone>).ToList();

            var cities = RapidVetUnitOfWork.ClientRepository.GetCities();
            model.Addresses = GetAddressModelList(cities);
            var addresses = model.Addresses.Select(AutoMapper.Mapper.Map<AddressModel, Address>).ToList();

            if (ModelState.IsValid)
            {
                var client = AutoMapper.Mapper.Map<ClientCreateModel, Client>(model);
                client.Emails = emails;
                client.Addresses = addresses;
                client.Phones = phones;
                client.ClinicId = CurrentUser.ActiveClinicId;
                // client.Active = true;
                client.LocalId = ClinicLocalIdManager.GetNextLocalClientId(CurrentUser.ActiveClinicId);
                client.CreatedById = CurrentUser.Id;
                client.CreatedDate = DateTime.Now;
                var status = RapidVetUnitOfWork.ClientRepository.Create(client);
                if (status.Success)
                {
                    if (model.isDirectedFromCalender.HasValue && model.isDirectedFromCalender.Value)
                    {
                        return RedirectToAction("ClientCreated", "Calender", new { id = status.ItemId });
                    }
                    return RedirectToAction("Create", "Patients", new { id = status.ItemId });
                }
            }

            return View(model);
        }


        //id is client id
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var client = RapidVetUnitOfWork.ClientRepository.GetClient(id);
            var model = AutoMapper.Mapper.Map<Client, ClientCreateModel>(client);

            if (model.VetId == 0)
            {
                //i'm a the current user and a doctor
                if (CurrentUser.IsDoctor)
                {
                    model.VetId = CurrentUser.Id;
                }//current user's default dr
                else if (CurrentUser.DefaultDrId != null)
                {
                    model.VetId = CurrentUser.DefaultDrId.Value;
                }
                else if (CurrentUser.ActiveClinic.MainVetId.HasValue) //clinic's default dr
                {
                    model.VetId = CurrentUser.ActiveClinic.MainVetId.Value;
                }
                else
                {
                    model.VetId = 0;
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ClientCreateModel model)
        {
            model.Emails = GetEmailModelList();
            var updatedEmails = model.Emails.Select(AutoMapper.Mapper.Map<EmailModel, Email>).ToList();

            model.Phones = GetPhoneModelList();
            var updatedPhones = model.Phones.Select(AutoMapper.Mapper.Map<PhoneModel, Phone>).ToList();

            var cities = RapidVetUnitOfWork.ClientRepository.GetCities();
            model.Addresses = GetAddressModelList(cities);
            var updatedAddresses = model.Addresses.Select(AutoMapper.Mapper.Map<AddressModel, Address>).ToList();

            if (ModelState.IsValid)
            {
                var client = RapidVetUnitOfWork.ClientRepository.GetClient(model.Id);
                AutoMapper.Mapper.Map(model, client);
                client.UpdatedById = CurrentUser.Id;
                client.UpdatedDate = DateTime.Now;

                var status = RapidVetUnitOfWork.ClientRepository.Update(client, updatedEmails, updatedPhones,
                                                                        updatedAddresses);
                if (status.Success)
                {
                    return RedirectToAction("Patients", "Clients", new { id = model.Id });
                }
            }
            return View(model);

        }

        /// <summary>
        /// gets values from Request.Form and converts them to AddressModel
        /// </summary>
        /// <returns>List(AddressModel)</returns>
        private List<AddressModel> GetAddressModelList(List<City> cities = null)
        {
            var addresses = new List<AddressModel>();
            foreach (var key in Request.Form.AllKeys)
            {
                if (key.StartsWith("city"))
                {
                    var id = key.Split('_');
                    var city = Request.Form[key];
                    var street = Request.Form["street_" + id[1]];
                    var zip = Request.Form["zip_" + id[1]];
                    var dbId = Request.Form["idCity_" + id[1]];

                    var addressId = 0;
                    int.TryParse(dbId, out addressId);
                    int cityID = getCityID(cities, city);
                    if (cityID == -1)
                        return null;

                    if (!string.IsNullOrWhiteSpace(city))
                    {
                        var addressModel = new AddressModel()
                            {
                                Id = addressId,
                                CityId = cityID,
                                Street = street,
                                ZipCode = zip
                            };


                        addresses.Add(addressModel);
                    }
                }
            }
            return addresses;
        }

        private int getCityID(List<City> cities, string city)
        {
            int id;
            if (int.TryParse(city, out id) || cities == null || String.IsNullOrEmpty(city))
                return id;

            var c = cities.Find(x => x.Name.Equals(city));
            return c == null ? -1 : c.Id;
        }

        /// <summary>
        /// gets values from Request.Form and converts them to PhoneModel
        /// </summary>
        /// <returns>List(PhoneModel)</returns>
        private List<PhoneModel> GetPhoneModelList()
        {
            var phones = new List<PhoneModel>();
            foreach (var key in Request.Form.AllKeys)
            {
                if (key.StartsWith("phone") && !key.StartsWith("phoneType"))
                {
                    var id = key.Split('_');
                    var phone = Request.Form[key];
                    var phoneType = Request.Form["phoneType_" + id[1]];
                    var phoneComment = Request.Form["comment_" + id[1]];
                    var dbId = Request.Form["idPhone_" + id[1]];

                    var phoneId = 0;
                    int.TryParse(dbId, out phoneId);


                    if (!string.IsNullOrWhiteSpace(phone) && !string.IsNullOrWhiteSpace(phoneType))
                    {
                        var phoneModel = new PhoneModel()
                            {
                                PhoneNumber = phone,
                                PhoneTypeId = int.Parse(phoneType),
                                Comment = phoneComment
                            };

                        phones.Add(phoneModel);
                    }
                }
            }
            return phones;
        }

        /// <summary>
        /// gets values from Request.Form and converts them to EmailModel
        /// </summary>
        /// <returns>List(EmailModel)</returns>
        private List<EmailModel> GetEmailModelList()
        {
            var emails = new List<EmailModel>();
            foreach (var key in Request.Form.AllKeys)
            {
                if (key.StartsWith("email"))
                {
                    var id = key.Split('_');
                    var email = Request.Form[key];
                    var dbId = Request.Form["idEmail_" + id[1]];
                    var emailId = 0;
                    int.TryParse(dbId, out emailId);

                    if (!string.IsNullOrWhiteSpace(email))
                    {
                        var emailModel = new EmailModel()
                            {
                                Id = emailId,
                                Name = email
                            };

                        emails.Add(emailModel);
                    }
                }
            }
            return emails;
        }


        //id is clientId
        public ActionResult Patients(int id)
        {
            var clinicGroup = RapidVetUnitOfWork.ClinicGroupRepository.GetItem(CurrentUser.ActiveClinicGroupId);
            if (!CurrentUser.IsAdministrator && clinicGroup.TrialPeriod && clinicGroup.ExpirationDate.Value.AddHours(23).AddMinutes(59) < DateTime.Now)
            {
                return RedirectToAction("LogOff", "Account");///Account/LogOff
            }

            else
            {
                var patients = RapidVetUnitOfWork.PatientRepository.GetClientPatients(id).ToList();
                var client = RapidVetUnitOfWork.ClientRepository.GetClient(id);
                var model = new PatientIndexModel()
                    {                        
                        Patients = patients.Where(p => p.Active).Select(AutoMapper.Mapper.Map<Patient, PatientIndexListModel>).ToList(),
                        InactivePatients = patients.Where(p => !p.Active).Select(AutoMapper.Mapper.Map<Patient, PatientIndexListModel>).ToList(),
                        Client = GetClientDetailsModel(id),
                        HasSmsAccount = !string.IsNullOrEmpty(CurrentUser.ActiveClinicGroup.UnicellUserName)
                    };
                RapidVetUnitOfWork.LastClientRepository.SetViewClient(CurrentUser.Id, CurrentUser.ActiveClinicId, id);
                if (CurrentUser.ActiveClinic.ShowPopUpWhenClientInDebt && client != null && client.Balance.HasValue && client.Balance.Value < 0)
                {
                    addMessageCookie(Resources.Client.ClientInDebtPopupMessage);
                }

                return View(model);
            }
        }

        private void addMessageCookie(string message)
        {
            if (Response.Cookies["comments"] == null || !Response.Cookies.AllKeys.Contains("comments"))
            {
                Response.Cookies.Add(new HttpCookie("comments", message));
                return;
            }

            var comments = Response.Cookies["comments"] as HttpCookie;
            if (comments == null)
                return;

            if (string.IsNullOrWhiteSpace(comments.Value))
            {
                comments.Value = String.Format("{0}", message);
            }
            else
            {
                comments.Value += String.Format("|{0}", message);
            }
        }

        public PartialViewResult Email()
        {
            return PartialView("_Email", new EmailModel());
        }

        public PartialViewResult Phone()
        {
            return PartialView("_Phone", new PhoneModel());
        }

        public PartialViewResult Address()
        {
            return PartialView("_Address", new AddressModel());

        }

        //public ActionResult ClientAutocomplete(string term)
        //{
        //    var clients =
        //       RapidVetUnitOfWork.ClientRepository.Search(term, CurrentUser.ActiveClinicId);

        //  //  var filteredItems = clients.Select(c => new SelectListItem() { Value = c.Id.ToString(), Text = c.NameWithPatients }).ToArray();
        //    //var filteredItems = items.Where(item => item.IndexOf(term, StringComparison.InvariantCultureIgnoreCase) >= 0);
        //    return Json(clients, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult Search(string query)
        {
            var clients =
                RapidVetUnitOfWork.ClientRepository.Search(query, CurrentUser == null ? -1 : CurrentUser.ActiveClinicId);

            return Json(clients);
        }

        public JsonResult GetZipCodeByCityAndStreet(string city, string street)
        {
            var parsedSteet = new StringBuilder();

            for (var i = 0; i < street.Length; i++)
            {
                int value;

                if (street[i] != '/' && street[i].ToString() != @"\" && !int.TryParse(street[i].ToString(), out value))
                {
                    parsedSteet.Append(street[i]);
                }
            }


            var zipcode = RapidVetUnitOfWork.ClientRepository.GetZipCodeByAddress(city, parsedSteet.ToString().Trim());


            return Json(zipcode, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ClientLetter(int id)
        {
            if (IsClientInCurrentClinic(id))
            {
                var client = RapidVetUnitOfWork.ClientRepository.GetClient(id);
                var model = GetClientDetailsModel(id);
                return View(model);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult ClientLetter(int id, HttpPostedFileBase docFile)
        {

            var client = RapidVetUnitOfWork.ClientRepository.GetClient(id);
            if (client.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException();
            }

            if (docFile != null && (System.IO.Path.GetExtension(docFile.FileName).Trim('.') == "docx" || System.IO.Path.GetExtension(docFile.FileName).Trim('.') == "doc"))
            {

                // Create document.

                try
                {
                    //IWordDocument document = new WordDocument();
                    //// Open template document.
                    //document.Open(docFile.InputStream, FormatType.Docx);

                    //var phone = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(client.Phones, PhoneTypeEnum.Home);

                    //var mobile = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientPhone(client.Phones, PhoneTypeEnum.Mobile);

                    //var address = RapidVetUnitOfWork.LetterTemplatesRepository.GetClientAddress(client.Addresses);

                    ////Find and Replace.
                    //document.Replace("*|FIRSTNAME|*", client.FirstName, true, true);
                    //document.Replace("*|LASTNAME|*", client.LastName, true, true);
                    //document.Replace("*|PHONE|*", phone, true, true);
                    //document.Replace("*|MOBILE|*", mobile, true, true);
                    //var r = document.Replace("*|ADDRESS|*", address, true, true);
                    //var outStram = new MemoryStream();
                    ////Save as .docx Word2007 format
                    //document.Save(outStram, FormatType.Word2007);

                    //return File(outStram.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", docFile.FileName);
                    return null;
                }
                catch (Exception ex)
                {


                }
            }

            return View();
        }

        /// <summary>
        /// shows all client's payments for all his animals
        /// </summary>
        /// <param name="id">id is client id</param>
        /// <returns></returns>
        public ActionResult PaymentsHistory(int id = 0, bool ShowTreatmentDetails = true)
        {
            ViewBag.ShowTreatmentDetails = ShowTreatmentDetails;
            var client = RapidVetUnitOfWork.ClientRepository.GetClient(id);
            if (client.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException("לקוח לא שייך למרפאה");
            }
            RapidVetUnitOfWork.ClientRepository.UpdateClientBalance(client);
            RapidVetUnitOfWork.Save();
            var payments = new List<ClientPaymentInfo>();

            var visits = RapidVetUnitOfWork.VisitRepository.GetClintVisits(id, ShowTreatmentDetails);
            var clinic = RapidVetUnitOfWork.ClinicRepository.GetItem(CurrentUser.ActiveClinicId);
            //var visits = RapidVetUnitOfWork.VisitRepository.GetClientVisitsForPayment(id);
            foreach (var visit in visits)
            {
                var visitString = new StringBuilder();
                visitString.Append(string.Format("טיפול - {0}", visit.Patient != null ? visit.Patient.Name : ""));
                visitString.Append("<br/>");

                if (ShowTreatmentDetails)
                {
                    if (clinic.ShowComplaint)
                    {
                        if (visit.MainComplaint != null && visit.MainComplaint.Length > 0)
                        {
                            visitString.Append("<b>תלונה עיקרית: </b>");
                            visitString.Append(visit.MainComplaint);
                            visitString.Append("<br/>");
                        }
                    }
                    if (clinic.ShowExaminations)
                    {
                        if (visit.Examinations.Count > 0)
                        {
                            visitString.Append("<b>בדיקות:</b> ");

                            foreach (var examination in visit.Examinations)
                            {

                                visitString.Append(examination.PriceListItem != null
                                                       ? examination.PriceListItem.Name
                                                       : examination.Name);

                                if (examination != visit.Examinations.Last())
                                {
                                    visitString.Append(", ");
                                }
                            }
                            visitString.Append("<br/>");
                        }
                    }
                    if (clinic.ShowDiagnosis)
                    {
                        if (visit.Diagnoses.Count > 0)
                        {
                            visitString.Append("<b>אבחנות:</b> ");
                            foreach (var diagnosis in visit.Diagnoses)
                            {
                                if (String.IsNullOrEmpty(diagnosis.Name))
                                    continue;

                                visitString.Append(diagnosis.Name);
                                if (diagnosis != visit.Diagnoses.Last())
                                {
                                    visitString.Append(", ");
                                }
                            }
                            visitString.Append("<br/>");
                        }
                    }
                    if (clinic.ShowTreatments)
                    {
                        if (visit.Treatments.Count > 0)
                        {
                            visitString.Append("<b>טיפולים:</b> ");
                            //visitString.Append("<br/>");
                            foreach (var treatment in visit.Treatments)
                            {
                                visitString.Append(treatment.Name);
                                if (treatment != visit.Treatments.Last())
                                {
                                    visitString.Append(", ");
                                }

                            }

                            if (visit.PreventiveMedicineItems.Count > 0)
                            {
                                visitString.Append(", ");
                            }
                            else
                            {
                                visitString.Append("<br/>");
                            }
                        }
                        if (visit.PreventiveMedicineItems.Count > 0)
                        {
                            if (visit.Treatments.Count == 0)
                            {
                                visitString.Append("<b>טיפולים:</b> ");
                                //visitString.Append("<br/>");
                            }
                            foreach (var preventiveMedicine in visit.PreventiveMedicineItems)
                            {
                                visitString.Append(preventiveMedicine.PriceListItem.Name);
                                if (preventiveMedicine != visit.PreventiveMedicineItems.Last())
                                {
                                    visitString.Append(", ");
                                }
                            }
                            visitString.Append("<br/>");
                        }
                    }
                    if (clinic.ShowMedicins)
                    {
                        if (visit.Medications.Count > 0)
                        {
                            visitString.Append("<b>תרופות:</b> ");
                            foreach (var medication in visit.Medications)
                            {
                                visitString.Append(medication.Name);
                                if (medication != visit.Medications.Last())
                                {
                                    visitString.Append(", ");
                                }
                            }
                            visitString.Append("<br/>");
                        }
                    }
                    if (clinic.ShowVisitNote)
                    {
                        if (!string.IsNullOrWhiteSpace(visit.VisitNote))
                        {
                            visitString.Append("<b>הערת ביקור:</b> ");

                            visitString.Append(visit.VisitNote);

                            visitString.Append("<br/>");
                        }
                    }
                    if (clinic.ShowTemperature)
                    {
                        visitString.Append("<b>טמפרטורה:</b> ");
                        visitString.Append(visit.Temp);
                        visitString.Append("<br/>");
                    }
                    if (clinic.ShowWeight)
                    {
                        visitString.Append("<b>משקל:</b> ");
                        visitString.Append(visit.Weight);
                        visitString.Append("<br/>");
                    }

                    if (visitString.Length == 0) // || (visitString.Length > 0 && visitString.ToString().Equals("<b>תלונה עיקרית:</b><br/>")))
                    {
                        //visitString.Clear();
                        visitString.Append("ביקור במרפאה");
                    }
                }
                if (visit.Price != null)
                    payments.Add(new ClientPaymentInfo()
                        {
                            ClientPaymentInfoType = ClientPaymentInfoType.Treatment,
                            Credit = 0,
                            Debt = visit.Price.Value,
                            Date = visit.VisitDate,
                            Details = visitString.ToString(),
                            // Details = visit.Patient != null ? string.Format("ביקור {0}", visit.Patient.Name) : "יתרת חוב מביקור קודם",
                            InvoiceNumber = visit.Id.ToString(),
                            Active = visit.Active
                        });
            }
            var docs = RapidVetUnitOfWork.FinanceDocumentReposetory.GetClientDocuments(id);
            foreach (var doc in docs)//.Where(d => d.FinanceDocumentTypeId != (int)FinanceDocumentType.Proforma))
            {
                var payment = new ClientPaymentInfo()
                   {
                       Date = doc.Created,
                       InvoiceNumber = doc.SerialNumber.ToString(),
                       Details = doc.Comments,
                       Credit = 0,
                       Debt = 0,
                       PrintDate = doc.OrginalPrintedDate,
                       FinanceDocumentId = doc.Id,
                       ConvertedFromRecipt = doc.ConvertedFromRecipt
                   };
                payments.Add(payment);
                switch (doc.FinanceDocumentType)
                {
                    case FinanceDocumentType.Invoice:
                        payment.ClientPaymentInfoType = ClientPaymentInfoType.Invoice;
                        break;
                    case FinanceDocumentType.Receipt:
                        payment.ClientPaymentInfoType = ClientPaymentInfoType.Reciept;
                        payment.Credit = doc.TotalSum;
                        break;
                    case FinanceDocumentType.InvoiceReceipt:
                        payment.ClientPaymentInfoType = ClientPaymentInfoType.InvoiceReciept;
                        payment.Credit = doc.TotalSum;
                        break;
                    case FinanceDocumentType.Refound:
                        payment.ClientPaymentInfoType = ClientPaymentInfoType.InvoiceCredit;
                        //payment.Debt = Math.Abs(doc.TotalSum);
                        break;
                    case FinanceDocumentType.Proforma:
                        payment.ClientPaymentInfoType = ClientPaymentInfoType.Proforma;
                        payment.Debt = doc.TotalSum;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                if (doc.FinanceDocumentType == FinanceDocumentType.Receipt) //Ariel says show redeemed only for receipt
                {
                    decimal redeemed = 0;
                    redeemed += doc.CashPaymentSum;
                    redeemed += doc.TotalCreditPaymentSum;
                    redeemed += doc.BankTransferSum;
                    redeemed +=
                        doc.Payments.Where(
                            p => p.PaymentTypeId == (int)PaymentType.Cheque && p.DueDate <= DateTime.Now.Date)
                           .Sum(p => p.Sum);
                    payment.Redeemed = redeemed;
                }
                else if (doc.FinanceDocumentType == FinanceDocumentType.Refound)
                {
                    payment.Credit = doc.SumToPayForInvoice;
                }
            }
            //internal settlements
            var settlements = RapidVetUnitOfWork.InternalSettlementsRepository.GetSettlements(id);
            foreach (var settlement in settlements)
            {
                var payment = new ClientPaymentInfo()
                    {
                        Date = settlement.Date,
                        Details = settlement.Description,
                        ClientPaymentInfoType = ClientPaymentInfoType.ManualChange,
                        Credit = settlement.Sum > 0 ? settlement.Sum : 0,
                        Debt = settlement.Sum > 0 ? 0 : Math.Abs(settlement.Sum)
                    };
                payments.Add(payment);
            }
            //appointments no show
            var appointments = RapidVetUnitOfWork.CalenderRepository.GetCalenderEntriesForClient(id);
            appointments = appointments.Where(c => c.IsNoShow == true).ToList();
            foreach (var appointment in appointments)
            {
                var payment = new ClientPaymentInfo()
                {
                    Date = appointment.Date,
                    Details = string.Format("אי הופעה לפגישה - {0}", appointment.Patient != null ? appointment.Patient.Name : ""),
                    ClientPaymentInfoType = ClientPaymentInfoType.Appointment,
                    Credit = 0,
                    Debt = 0,
                    Active = true
                };
                payments.Add(payment);
            }
            var model = new ClientPaymentsWebModel();
            model.ClientId = client.Id;
            model.ClientName = client.Name;
            model.Payments = payments.OrderBy(p => p.Date).ToList();
            return View(model);
        }

        public ActionResult AddInternalSettlement(int id)
        {
            if (IsClientInCurrentClinic(id))
            {
                var model = new AddInternalSettlementModel() { ClientId = id };
                return View(model);
            }
            throw new SecurityException();
        }

        [HttpPost]
        public ActionResult AddInternalSettlement(AddInternalSettlementModel model)
        {
            if (IsClientInCurrentClinic(model.ClientId))
            {
                var internalSettlement = new InternalSettlement()
                    {
                        ClientId = model.ClientId,
                        Date = DateTime.Now,
                        Description = model.Description,
                        Sum = model.Sum
                    };
                RapidVetUnitOfWork.InternalSettlementsRepository.AddSettlement(internalSettlement);
                RapidVetUnitOfWork.Save();

                return RedirectToAction("PaymentsHistory", new { id = model.ClientId });
            }
            throw new SecurityException();
        }

        public JsonResult GetClientPatients(int? clientId)
        {
            if (clientId.HasValue && clientId.Value > 0)
            {
                if (IsClientInCurrentClinic(clientId.Value))
                {
                    var patients = RapidVetUnitOfWork.PatientRepository.GetClientPatients(clientId.Value);
                    var model = patients.Select(Mapper.Map<Patient, SelectListItem>).ToList();
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
            }
            return null;
        }

        public JsonResult GetClientPatientsAndColor(int clientId)
        {
            var client = RapidVetUnitOfWork.ClientRepository.GetClient(clientId);
            var patients = RapidVetUnitOfWork.PatientRepository.GetClientPatients(clientId);
            var model = patients.Select(Mapper.Map<Patient, SelectListItem>).ToList();
            return Json(new { Patients = model, Color = client.ClientStatus.MeetingColor }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClientIdCard(int clientId)
        {
            if (IsClientInCurrentClinic(clientId))
            {
                var client = RapidVetUnitOfWork.ClientRepository.GetClient(clientId);
                return Json(new { idCard = client.IdCard, clientName = client.NameWithPatients }, JsonRequestBehavior.AllowGet);
            }
            throw new SecurityException();
        }

        //id is currentClientId
        public ActionResult ChangeOwnership(int id, int patientId, int destinationOwnerId)
        {
            var patient = RapidVetUnitOfWork.PatientRepository.GetPatient(patientId);
            if (id == patient.ClientId &&
                IsPatientInCurrentClinic(patientId) &&
                IsClientInCurrentClinic(id) &&
                IsClientInCurrentClinic(destinationOwnerId))
            {
                patient.ClientId = destinationOwnerId;
                //create relevant internal settlements & update balances
                decimal sum = 0;
                var now = DateTime.Now;
                var visits = RapidVetUnitOfWork.VisitRepository.GetPatientVisits(patientId);
                if (visits.Any())
                {
                    sum = visits.Sum(v => v.Price).Value;
                }

                //var originalOwnerDeduction = new InternalSettlement
                //    {
                //        ClientId = id,
                //        Description = " העברות בעלות " + patient.Name + " - חיוב על ביקורים קודמים",
                //        Date = now,
                //        Sum = 0 - sum
                //    };

                //var destinationOwnerRefund = new InternalSettlement()
                //    {
                //        ClientId = destinationOwnerId,
                //        Description = " העברות בעלות " + patient.Name + " - זיכוי על ביקורים קודמים",
                //        Date = now,
                //        Sum = sum
                //    };

                //RapidVetUnitOfWork.InternalSettlementsRepository.AddSettlement(originalOwnerDeduction);
                //RapidVetUnitOfWork.InternalSettlementsRepository.AddSettlement(destinationOwnerRefund);

                //update client balances
                //var destinationOwner = RapidVetUnitOfWork.ClientRepository.GetClient(destinationOwnerId);
                //RapidVetUnitOfWork.ClientRepository.UpdateClientBalance(destinationOwner);
                //var originalOwner = RapidVetUnitOfWork.ClientRepository.GetClient(id);
                //RapidVetUnitOfWork.ClientRepository.UpdateClientBalance(originalOwner);

                //save
                var status = RapidVetUnitOfWork.Save();
                if (status.Success)
                {
                    ArchivesUploadHelper.MovePatientFiles(CurrentUser.ActiveClinicGroupId, CurrentUser.ActiveClinicId, id, destinationOwnerId, patientId);
                    RapidVetUnitOfWork.ClientRepository.SetClientNameWithPatients(id);
                    RapidVetUnitOfWork.ClientRepository.SetClientNameWithPatients(destinationOwnerId);
                    SetSuccessMessage();
                }
                else
                {
                    SetErrorMessage();
                }

                return RedirectToAction("Patients", "Clients", new { id = id });
                //return Redirect(HttpContext.Request.UrlReferrer.ToString());
            }
            throw new SecurityException();
        }

        //public String UpdateAllClientBalance()
        //{
        //    RapidVetUnitOfWork.ClientRepository.UpdateClientBalance();
        //    return "ok";
        //}

        public ActionResult AdvancedSearch()
        {
            ViewBag.SearchByContaining = CurrentUser.LastAdvancedSearchByContaining;
            return View();
        }

        [HttpPost]
        public ActionResult AdvancedSearch(AdvancedSearchWebModel model)
        {
            var templatePatientsURL = Url.Action("Index", "Patients", new { id = 0 });
            var templateClientsURL = Url.Action("Patients", "Clients", new { id = 0 });
            //var clientStatus = model.ClientStatus != null ? RapidVetUnitOfWork.ClientStatusRepository.GetItem(int.Parse(model.ClientStatus)).Name : null;
            var results = RapidVetUnitOfWork.ClientRepository.AdvancedSearch(CurrentUser.ActiveClinicGroupId,
                                                                             CurrentUser.ActiveClinicId, model.FirstName,
                                                                             model.LastName, model.IdCard, model.Email,
                                                                             model.Phone, model.CityId, model.ClientStatus,
                                                                             model.Street, model.PatientName,
                                                                             model.AnimalKindId,
                                                                             model.PatientLicense, model.Chip, model.Sagir,
                                                                             model.IncludeInactivePatients,
                                                                             templatePatientsURL, templateClientsURL, model.SearchByContaining);

            if (CurrentUser.LastAdvancedSearchByContaining != model.SearchByContaining)
            {
                CurrentUser.LastAdvancedSearchByContaining = model.SearchByContaining;
                RapidVetUnitOfWork.Save();
            }

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
            var result = new ContentResult
            {
                Content = serializer.Serialize(results),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult Autocomplete(string term)
        {
            var cities = RapidVetUnitOfWork.ClientRepository.GetCities(term);
            var filteredItems = cities.Select(x => x.Name).ToArray();
            //var filteredItems = items.Where(item => item.IndexOf(term, StringComparison.InvariantCultureIgnoreCase) >= 0);
            return Json(filteredItems, JsonRequestBehavior.AllowGet);
        }


    }
}
