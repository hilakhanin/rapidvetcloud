﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI.DataVisualization.Charting;
using CsvHelper;
using CsvHelper.Configuration;
using Newtonsoft.Json;
using RapidVet.Enums;
using RapidVet.Enums.Finances;
using RapidVet.Helpers;
using RapidVet.Model.Finance;
using RapidVet.Web.Attributes;
using RapidVet.WebModels.Finance.Reports;
using RestSharp;
using System.Data;
using System.Web.Configuration;
using RapidVet.WebModels.Doctors;
using RapidVet.WebModels.FullCalender;

namespace RapidVet.Web.Controllers.Finance
{
    [Authorize]
    public class FinanceReportsController : BaseController
    {

        public ActionResult Index()
        {
            return View();
        }

        [ViewFinancialReports]
        public ActionResult Income()
        {
            return View();
        }

        public ActionResult PrintIncome(IncomeReportFilter filter)
        {
            var model = GetIncomeReportData(filter);
            return View(model);
        }

        public ActionResult PrintInvoiceReport(string from, string to, bool showOnlyConverted, int issuerId)
        {
            var model = GetInvoiceReportDataHelper(from, to, showOnlyConverted, issuerId);
            return View(model);
        }

        public ActionResult PrintReciptReport(string from, string to, int issuerId)
        {
            var model = GetReciptReportDataHelper(from, to, issuerId);
            return View(model);
        }

        [ExcelExport]
        public FileResult ExportIncomeReport(IncomeReportFilter filter)
        {
            var data = GetIncomeReportData(filter);
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8 };
            var csv = new CsvWriter(writer, csvConfig);
            csv.Configuration.HasHeaderRecord = false;
            csv.WriteRecord(new
            {
                c1 = RapidVet.Resources.Income.Date,
                c2 = RapidVet.Resources.Income.ClientName,
                c3 = RapidVet.Resources.Income.Status,
                c4 = RapidVet.Resources.Income.DocId,
                c5 = RapidVet.Resources.Income.DocType,
                c6 = RapidVet.Resources.Income.PriceSum
            });
            foreach (var item in data)
            {
                csv.WriteRecord(new
                {
                    c1 = item.CreatedDate,
                    c2 = item.ClientName,
                    c3 = item.ClientStatus,
                    c4 = item.DocumentSerialNumber,
                    c5 = item.FinanceDocumentType,
                    c6 = item.TotalToPay
                });
            }
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", "intakesReport.csv");
        }

        public ActionResult GetIncomeData(IncomeReportFilter filter)
        {
            var model = GetIncomeReportData(filter);
            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
            var result = new ContentResult
            {
                Content = serializer.Serialize(model),
                ContentType = "application/json"
            };
            return result;
        }

        [ViewFinancialReports]
        public ActionResult ClientsInDebt()
        {
            bool isPrivateCalendars = !CurrentUser.IsAdministrator || !CurrentUser.IsClinicManager;
            var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctorsForSchedule(CurrentUser.ActiveClinicId, CurrentUser.Id, isPrivateCalendars);

            var model = new WeeklyCalenderWebModel()
                {
                    Doctors = new List<SelectListItem>(),
                    Calendars = new List<CalendarModel>()
                };

            foreach (var doctor in doctors)
            {
                var n = String.Format("{0} {1}", doctor.FirstName, doctor.LastName);

                if (RapidVetUnitOfWork.ClinicRepository.GetUserByUN(doctor.Username) != null)
                    continue;

                if (doctor.ShowOnMainWin)
                    model.Doctors.Add(new SelectListItem() { Text = n, Value = doctor.Id.ToString() });                
            }

            return View(model);
        }
        private DataTable GetDataTableWithCaptions(DataTable dt, string GridCaptions, string GridNames, string SortBy, string RemoveColumns)
        {
            if (GridCaptions.Trim().Length > 0 && GridNames.Trim().Length > 0)
            {
                bool isCaptionChange = false;
                string[] arrCaptions = GridCaptions.Split(';');
                string[] arrNames = GridNames.Split(';');
                string[] arrCaptiondToDel = new string[dt.Columns.Count];
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    isCaptionChange = false;
                    for (int index = 0; index < arrCaptions.Length && index < arrNames.Length; index++)
                    {
                        if (arrCaptions[index].Trim().Length > 0 && arrNames[index].Trim().Length > 0 && dt.Columns[i].ColumnName == arrNames[index].Trim())
                        {
                            dt.Columns[i].Caption = arrCaptions[index];
                            isCaptionChange = true;
                            break;
                        }
                    }
                    if (!isCaptionChange)
                        arrCaptiondToDel[i] = dt.Columns[i].ColumnName;
                }
                for (int i = 0; i < arrCaptiondToDel.Length; i++)
                {
                    for (int u = 0; u < dt.Columns.Count; u++)
                    {
                        if (arrCaptiondToDel[i] == dt.Columns[u].ColumnName)
                        {
                            dt.Columns.Remove(arrCaptiondToDel[i]);
                            break;
                        }
                    }
                }
            }

            if (RemoveColumns.Length > 0)
            {
                string[] arrCaptiondToDel = RemoveColumns.Split(';');
                for (int i = 0; i < arrCaptiondToDel.Length; i++)
                {
                    for (int u = 0; u < dt.Columns.Count; u++)
                    {
                        if (arrCaptiondToDel[i] == dt.Columns[u].ColumnName)
                        {
                            dt.Columns.Remove(arrCaptiondToDel[i]);
                            break;
                        }
                    }
                }
            }

            try
            {
                if (SortBy.Trim() != "")
                {
                    DataView dv = dt.DefaultView;
                    dv.Sort = SortBy;
                    dt = dv.ToTable();
                }
            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        [HttpPost]
        public void GetClientInDebtDataGrid(string isToSearch, string fromSum, string toSum, string vetID, string dtNotVisitedUfter, string dtFrom, string dtTo, string ExportType = "0",
            string GridCaptions = "", string GridNames = "", string SortBy = "")
        {
            if (vetID.Trim() == "")
                vetID = "0";

            var clients = RapidVetUnitOfWork.FinanceReportRepository.GetClientsInMinusBalance(CurrentUser.ActiveClinicId, Convert.ToDouble(fromSum), Convert.ToDouble(toSum), Convert.ToInt64(vetID), dtNotVisitedUfter == "0" ? new DateTime() : Convert.ToDateTime(dtNotVisitedUfter), dtFrom == "0" ? new DateTime() : Convert.ToDateTime(dtFrom), dtTo == "0" ? new DateTime() : Convert.ToDateTime(dtTo));

            if (ExportType == "0")//grid
            {
                Response.Write(JsonConvert.SerializeObject(Json(clients, JsonRequestBehavior.AllowGet).Data, Formatting.Indented));
            }
            else if (ExportType=="1")//excel
            {
                MemoryStream ms1 = new MemoryStream();
                using (OfficeOpenXml.ExcelPackage package = new OfficeOpenXml.ExcelPackage(ms1))
                {
                    OfficeOpenXml.ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Main");

                    DataTable dt = FinanceHelper.ToDataTable(clients);

                    dt = GetDataTableWithCaptions(dt, GridCaptions, GridNames, SortBy, "");
                    
                    worksheet.Cells["A1"].LoadFromDataTable(dt, true);
                    package.Save();
                }
                Response.Write(Convert.ToBase64String(ms1.ToArray()));
            }
            else if (ExportType == "2")//pdf
            {
                DataTable dtPDF = FinanceHelper.ToDataTable(clients);
                dtPDF = GetDataTableWithCaptions(dtPDF, GridCaptions, GridNames, SortBy, "ClientUrl;");

                if (WebConfigurationManager.AppSettings["PDFExportTempFile"] != null && WebConfigurationManager.AppSettings["PDFExportFontURL"] != null && WebConfigurationManager.AppSettings["PDFExportLogoUrl"] != null)
                {
                    string PDFURL = WebConfigurationManager.AppSettings["PDFExportTempFile"].ToString();

                    PDFHelper.CreateDoc(PDFURL, WebConfigurationManager.AppSettings["PDFExportLogoUrl"].ToString(), dtPDF, "דו''ח חובות", Server.MapPath(WebConfigurationManager.AppSettings["PDFExportFontURL"].ToString()));

                    Byte[] bytes = System.IO.File.ReadAllBytes(PDFURL);
                    Response.Write(Convert.ToBase64String(bytes));
                }
            }
            else if (ExportType == "3")//print
            {
                DataTable dtPrint = FinanceHelper.ToDataTable(clients);
                dtPrint = GetDataTableWithCaptions(dtPrint, GridCaptions, GridNames, SortBy, "ClientUrl;");

                if (WebConfigurationManager.AppSettings["PDFExportLogoUrl"] != null)
                {
                    string htm = PrintHelper.GetHTMLDoc(WebConfigurationManager.AppSettings["PDFExportLogoUrl"].ToString(), dtPrint, "דו''ח חובות");
                    Response.Write(htm);
                }
            }
        }

        [HttpPost]
        public ActionResult GetClientInDebtData(bool isToSearch, string fromSum, string toSum, string vetID, string dtNotVisitedUfter, string dtFrom, string dtTo)
        {
            if (vetID.Trim() == "")
                vetID = "0";

            var clients = RapidVetUnitOfWork.FinanceReportRepository.GetClientsInMinusBalance(CurrentUser.ActiveClinicId, Convert.ToDouble(fromSum), Convert.ToDouble(toSum), Convert.ToInt64(vetID), dtNotVisitedUfter == "0" ? new DateTime() : Convert.ToDateTime(dtNotVisitedUfter), dtFrom == "0" ? new DateTime() : Convert.ToDateTime(dtFrom), dtTo == "0" ? new DateTime() : Convert.ToDateTime(dtTo));

            return Json(clients, JsonRequestBehavior.AllowGet);

            //var clients = RapidVetUnitOfWork.FinanceReportRepository.GetClientsInMinusBalance(CurrentUser.ActiveClinicId);
            // var usersIds = RapidVetUnitOfWork.IssuerRepository.GetUsersIdsByIssuerId(issuerId);
            //foreach (var client in clients)
            //{
            //    var lastVisitDate = RapidVetUnitOfWork.ClientRepository.GetLastVisitDate(client.Id);
            //var debt = (decimal)0;

            //foreach (var visit in client.Patients.Where(p => p.Visits.Any(v => usersIds.Contains(v.DoctorId))).SelectMany(patient => patient.Visits.OrderByDescending(v => v.VisitDate)))
            //{
            //    if (visit.VisitDate > lastVisitDate)
            //    {
            //        lastVisitDate = visit.VisitDate;
            //    }

            //    if (visit.ReciptId == null && visit.Price.HasValue)
            //    {
            //        debt += (decimal)visit.Price.Value;
            //    }
            //}
            //var internalSettelments = RapidVetUnitOfWork.InternalSettlementsRepository.GetSettlements(client.Id);
            //if (internalSettelments.Any())
            //{
            //    debt = debt + internalSettelments.Sum(i => i.Sum) * -1;
            //}


            //if (client.Balance < 0)
            //{
            //    model.Add(new ClientsInDebtWebModel()
            //    {
            //        ClientName = client.Name,
            //        DebtAmount = client.Balance.HasValue ? -(decimal)client.Balance : 0,
            //        LastVisitDate = lastVisitDate,
            //        LastVisitStr =
            //            lastVisitDate > DateTime.MinValue ? lastVisitDate.ToShortDateString() : string.Empty,
            //        //ClientUrl = Url.Action("Patients", "Clients", new { id = client.Id })
            //    });
            //    //}
            //}

        }
        public JsonResult GetClientInDebtData()
        {
            var model = new List<ClientsInDebtWebModel>();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [ViewFinancialReports]
        public ActionResult PaymentMethodsSegmentation()
        {
            return View();
        }

        public JsonResult GetPaymentMethodsSegmentation(string from, string to, bool sumByCreatedDate, int issuerId)
        {
            var fromDate = StringUtils.ParseStringToDateTime(from);
            var todate = StringUtils.ParseStringToDateTime(to);

            var payments = RapidVetUnitOfWork.FinanceReportRepository.GetPayments(CurrentUser.ActiveClinicId, fromDate,
                                                                                  todate, sumByCreatedDate, issuerId);
            decimal cash = 0;
            var cashItems = payments.Where(p => p.PaymentTypeId == (int)PaymentType.Cash);
            if (cashItems.Any())
            {
                cash = cashItems.Sum(c => c.Sum);
            }

            decimal cheques = 0;
            var chequeItems = payments.Where(p => p.PaymentTypeId == (int)PaymentType.Cheque);
            if (chequeItems.Any())
            {
                cheques = chequeItems.Sum(c => c.Sum);
            }

            decimal creditCards = 0;
            var creditItems = payments.Where(p => p.PaymentTypeId == (int)PaymentType.CreditCard);
            if (creditItems.Any())
            {
                creditCards = creditItems.Sum(c => c.Sum);
            }
            decimal transfers = 0;
            var transferItems = payments.Where(p => p.PaymentTypeId == (int)PaymentType.Transfer);
            if (transferItems.Any())
            {
                transfers = transferItems.Sum(t => t.Sum);
            }

            var totalIncludeVat = cash + cheques + creditCards + transfers;
            //   var totalNoVat = totalIncludeVat * ((100 - CurrentUser.ActiveClinic.TaxRate) / 100);

            return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new
                        {
                            Cash = cash,
                            Cheques = cheques,
                            CreditCards = creditCards,
                            Transfers = transfers,
                            // TotalNoVat = totalNoVat,
                            TotalIncludeVat = totalIncludeVat
                        }
                };
        }

        [ViewFinancialReports]
        public ActionResult ChequesReport()
        {
            return View();
        }

        public JsonResult GetChequesData(string from, string to, int documentTypeId, bool paidCheques, int issuerId)
        {
            var model = GetChequesReportData(from, to, documentTypeId, paidCheques, issuerId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintChequesReport(string from, string to, int documentTypeId, bool paidCheques, int issuerId)
        {
            var model = GetChequesReportData(from, to, documentTypeId, paidCheques, issuerId);
            return View(model);
        }

        [ViewFinancialReports]
        public ActionResult SalesReport()
        {
            return View();
        }

        public JsonResult GetIssuers()
        {
            var data = RapidVetUnitOfWork.IssuerRepository.GetDropdownList(CurrentUser.ActiveClinicId, CurrentUser.IsAdministrator ? -1 : CurrentUser.Id);

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllIssuersDropDown()
        {
            var data = RapidVetUnitOfWork.IssuerRepository.GetDropdownList(CurrentUser.ActiveClinicId, CurrentUser.IsAdministrator ? -1 : CurrentUser.Id, true);

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [ViewFinancialReports]
        public JsonResult GetSalesReportData(int? categoryId, int? priceListItemId, int? issuerId, string from, string to, bool groupBySeller, bool showAllItems)
        {
            var fromDate = StringUtils.ParseStringToDateTime(from);
            var toDate = StringUtils.ParseStringToDateTime(to);
            var financeDocumentItems =
                RapidVetUnitOfWork.FinanceReportRepository.GetSalesReportData(CurrentUser.ActiveClinicId, fromDate, toDate,
                categoryId, priceListItemId, issuerId, groupBySeller, showAllItems);
            var model = new List<SalesReportWebModel>();
            var itemIds = new HashSet<int>();
            var userIds = new HashSet<int>();

            // Get all ids

            foreach (var item in financeDocumentItems)
            {
                if (item.PriceListItemId.HasValue && !itemIds.Contains(item.PriceListItemId.Value))
                {
                    itemIds.Add(item.PriceListItemId.Value);
                }
                else if (showAllItems && !item.PriceListItemId.HasValue && !itemIds.Contains(0))
                {
                    itemIds.Add(0);
                }
            }

            foreach (var itemId in itemIds)
            {
                IQueryable<FinanceDocumentItem> items;

                if (itemId != 0)
                {
                    items = financeDocumentItems.Where(f => f.PriceListItemId.Value == itemId)
                                                .OrderBy(f => f.FinanceDocument.Created);
                }
                else
                {
                    items = financeDocumentItems.Where(f => !f.PriceListItemId.HasValue &&
                        (!f.Description.Equals(Resources.Visit.OpenBalance) // && !f.Description.Equals(Resources.Finance.VetTreatment)
                        && !f.Description.Equals(Resources.PriceListItemType.All)))
                                                .OrderBy(f => f.FinanceDocument.Created);
                }

                if (itemId != 0)
                {
                    var priceListItem = RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(itemId);
                    if (items.Any())
                    {
                        model.AddRange(
                            items.Select(AutoMapper.Mapper.Map<FinanceDocumentItem, SalesReportWebModel>).ToList());
                        foreach (var modelItem in model.Where(i => i.PriceListItemId == itemId))
                        {
                            modelItem.ItemName = priceListItem.Name;
                            modelItem.CategoryName = priceListItem.Category.Name;
                            modelItem.ItemType = priceListItem.PriceListItemType.Name;
                        }
                    }
                }
                else
                {
                    foreach (var customItem in items)
                    {
                        model.Add(new SalesReportWebModel()
                        {
                            ItemName = customItem.Description,
                            CategoryName = "",
                            ItemType = "",
                            Quantity = customItem.Quantity.Value,
                            PriceListItemId = 0,
                            ClientName = customItem.FinanceDocument.ClientName,
                            Doctor = customItem.Visit != null ? customItem.Visit.Doctor.Name : "",
                            Price = customItem.TotalBeforeVAT * customItem.FinanceDocument.VAT,
                            Created = customItem.FinanceDocument.Created
                        });
                    }
                }

            }


            if (groupBySeller)
            {
                foreach (var item in financeDocumentItems)
                {
                    if (!userIds.Contains(item.AddedByUserId))
                    {
                        userIds.Add(item.AddedByUserId);
                    }
                }
            }

            ViewBag.itemIds = itemIds;
            ViewBag.userIds = userIds;
            ViewBag.GroupBySeller = groupBySeller;

            return Json(RenderPartialViewToString("_SalesReportResults", model), JsonRequestBehavior.AllowGet);
        }

        [ViewFinancialReports]
        public ActionResult InvoiceReport()
        {
            return View();
        }

        [ViewFinancialReports]
        public ActionResult ReciptReport()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetInvoiceReportData(string from, string to, bool showOnlyConverted, int issuerId)
        {
            var model = GetInvoiceReportDataHelper(from, to, showOnlyConverted, issuerId);
            //return Json(model, JsonRequestBehavior.AllowGet);
            return Content(SimpleJson.SerializeObject(model));
        }

        [ExcelExport]
        public FileResult InvoiceReportExportToExcel(string from, string to, bool showOnlyConverted, int issuerId)
        {
            var data = GetInvoiceReportDataHelper(from, to, showOnlyConverted, issuerId);
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8 };
            var csv = new CsvWriter(writer, csvConfig);
            csv.WriteRecords(data);
            writer.Flush();
            stream.Position = 0;
            return File(stream, "text/csv", "incomeReport.csv");
        }

        public JsonResult GetReciptReportData(string from, string to, int issuerId)
        {
            var model = GetReciptReportDataHelper(from, to, issuerId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [ExcelExport]
        public FileResult ReciptReportExportToExcel(string from, string to, int issuerId)
        {
            var data = GetReciptReportDataHelper(from, to, issuerId);
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8 };
            var csv = new CsvWriter(writer, csvConfig);
            csv.WriteRecords(data);
            writer.Flush();
            stream.Position = 0;
            return File(stream, "text/csv", "incomeReport.csv");
        }

        [ViewFinancialReports]
        public ActionResult PeriodTaxCalculation()
        {
            //   ViewBag.IssuerOptions = RapidVetUnitOfWork.IssuerRepository.GetDropdownList(CurrentUser.ActiveClinicId);
            return View();
        }

        [ViewFinancialReports]
        [HttpGet]
        public JsonResult GetPeriodTaxCalculationData(String from, String to, int issuerId)
        {
            DateTime fromDate;
            DateTime toDate;
            if (!String.IsNullOrEmpty(from) && !String.IsNullOrEmpty(to))
            {
                fromDate = DateTime.ParseExact(from, "dd/MM/yyyy", null);
                toDate = DateTime.ParseExact(to, "dd/MM/yyyy", null);
                toDate = toDate.AddDays(1).AddTicks(-1);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            var clinic = CurrentUser.ActiveClinic;
            var model = new PeriodTaxCalculationModel();

            var invoices = RapidVetUnitOfWork.FinanceReportRepository.GetInvoices(fromDate, toDate, clinic.Id);

            invoices = invoices.Where(i => i.IssuerId == issuerId);

            //tax stuff
            model.AdvancedPaymentPercent = clinic.AdvancedPaymentPercent;

            decimal sum = Enumerable.Sum(invoices, invoice => invoice.TotalBeforeVATAfterDisscount);
            model.TuroverNoVAT = sum;

            //vat stuff
            var noVatDeals = invoices.Where(d => d.VAT == 1);
            model.NoVATDeals = Enumerable.Sum(noVatDeals, deal => deal.TotalBeforeVATAfterDisscount);

            var invoiceReceiptTypeId = (int)FinanceDocumentType.InvoiceReceipt;

            var vatDeals = invoices.Where(d => d.VAT != 1);
            model.WithVATDeals = Enumerable.Sum(vatDeals, deal => deal.TotalBeforeVATAfterDisscount);

            model.VATonDeals = Enumerable.Sum(vatDeals, deal => deal.FinanceDocumentTypeId == invoiceReceiptTypeId ?
                deal.TotalSum - deal.TotalBeforeVATAfterDisscount : deal.SumToPayForInvoice - deal.TotalBeforeVATAfterDisscount);

            model.PermanentAssetsInputTax = RapidVetUnitOfWork.FinanceReportRepository.GetPermanentAssetsInputTax(clinic.Id, fromDate, toDate);

            model.OtherInputTax = RapidVetUnitOfWork.FinanceReportRepository.GetOtherInputTax(clinic.Id, fromDate, toDate);


            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [ViewFinancialReports]
        public ActionResult AnnualBalance()
        {
            return View();
        }

        [ViewFinancialReports]
        public JsonResult GetAnnualBalanceReport(int? issuerId, int? year)
        {
            var model = GetAnnualReportData(issuerId, year);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintAnuualBalanceReport(int? issuerId, int? year)
        {
            var model = GetAnnualReportData(issuerId, year);
            ViewBag.Year = year;
            return View(model);
        }

        public FileResult ExportAnnualBalance(int? issuerId, int? year)
        {
            var data = GetAnnualReportData(issuerId, year);

            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8 };
            var csv = new CsvWriter(writer, csvConfig);

            csv.WriteRecords(data);
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", string.Format("{0}_AnnualBalance.csv", year));
        }

        [ViewFinancialReports]
        public ActionResult ExpensesDistribution()
        {
            var model = new List<SelectListItem>();
            var clinicExpenseGroups =
                RapidVetUnitOfWork.ExpenseGroupRepository.GetClinicExpenseGroups(CurrentUser.ActiveClinicId);
            foreach (var eg in clinicExpenseGroups.OrderBy(x => x.Name))
            {
                model.Add(new SelectListItem()
                    {
                        Text = eg.Name,
                        Value = eg.Id.ToString(),
                        Selected = false
                    });
            }
            return View(model);
        }

        [ViewFinancialReports]
        public FileResult GetExpenseDistributionChart(String fromDate, String toDate, String groupIds)
        {
            var from = DateTime.ParseExact(fromDate, "dd/MM/yyyy", null);
            var to = DateTime.ParseExact(toDate, "dd/MM/yyyy", null);

            var ids = JsonConvert.DeserializeObject<List<int>>(groupIds);
            var chartImage = new Chart();
            chartImage.Width = 800;
            chartImage.Height = 400;
            chartImage.ChartAreas.Add("chart");
            chartImage.ChartAreas["chart"].AxisX.MajorGrid.LineColor = Color.LightGray;
            chartImage.ChartAreas["chart"].AxisY.MajorGrid.LineColor = Color.LightGray;


            chartImage.Series.Add("expenses");
            chartImage.Series["expenses"].ChartType = SeriesChartType.Column;
            chartImage.Series["expenses"].IsValueShownAsLabel = true;

            chartImage.ChartAreas[0].AxisY.Maximum = 100;
            chartImage.ChartAreas[0].AxisY.Minimum = 0;

            //we want: for each expense group a column representing its percent of the expenses
            var totalExpenses =
                RapidVetUnitOfWork.ExpenseRepository.GetClinicExpenses(CurrentUser.ActiveClinicId, from, to, null, Resources.Expences.EnteredDate);

            if (totalExpenses.Any())
            {
                chartImage.Titles.Add("התפלגות הוצאות באחוזים");
                var totalSum = totalExpenses.Sum(e => e.TotalAmount);

                foreach (var id in ids)
                {
                    var title = "";
                    var expenseGroup = RapidVetUnitOfWork.ExpenseGroupRepository.GetGroup(id);
                    if (expenseGroup != null)
                    {
                        title = expenseGroup.Name;
                    }
                    var point = new DataPoint();
                    var expenses = totalExpenses.Where(e => e.ExpenseGroupId == id).ToList();
                    decimal sum = 0;
                    decimal percent = 0;
                    if (expenses.Count > 0)
                    {
                        sum = totalExpenses.Where(e => e.ExpenseGroupId == id).Sum(e => e.TotalAmount);
                        percent = sum / totalSum;
                        percent = percent * 100;
                        percent = Math.Round(percent, 2, MidpointRounding.ToEven);
                    }
                    point.AxisLabel = title;
                    point.YValues = new double[] { (double)percent };

                    chartImage.Series["expenses"].Points.Add(point);
                }
            }
            else
            {
                chartImage.Titles.Add("לא נמצאו הוצאות בתאריכים המבוקשים");
            }
            using (var strm = new MemoryStream())
            {

                chartImage.SaveImage(strm, ChartImageFormat.Png);
                strm.Seek(0, SeekOrigin.Begin);
                return File(strm.ToArray(), "image/png", "expenseDistribution.png");
            }
        }

        [ViewFinancialReports]
        public ActionResult CashFlow()
        {
            return View();
        }

        //for now this isn't used
        [ViewFinancialReports]
        [HttpGet]
        public JsonResult GetDailyCashFlow(String from, String to)
        {
            var fromDate = DateTime.ParseExact(from, "dd/MM/yyyy", null);
            var toDate = DateTime.ParseExact(to, "dd/MM/yyyy", null);

            var model = new List<DailyCashFlowModel>();

            var expenses = RapidVetUnitOfWork.ExpenseRepository.GetExpenses(CurrentUser.ActiveClinicId, fromDate, toDate);
            var financeDocuments =
               RapidVetUnitOfWork.FinanceDocumentReposetory.GetDocumentsForClinicFromDate(CurrentUser.ActiveClinicId, fromDate, toDate)
                             .Where(d => d.CashPaymentSum > 0);

            for (var day = fromDate.Date; day.Date <= toDate.Date; day = day.AddDays(1))
            {
                //outgoing expenses
                decimal outgoing = 0;
                var dailyExpenses =
                    expenses.Where(
                        e => e.PaymentDate.HasValue &&
                        e.PaymentDate.Value.Day == day.Day && e.PaymentDate.Value.Month == day.Month &&
                        e.PaymentDate.Value.Year == day.Year).ToList();
                if (dailyExpenses.Count > 0)
                {
                    outgoing = dailyExpenses.Sum(e => e.TotalAmount);
                }
                //incoming cash
                decimal incoming = 0;
                var dailyIncome =
                    financeDocuments.Where(e => e.Created.Day == day.Day &&
                                                e.Created.Month == day.Month &&
                                                e.Created.Year == day.Year).ToList();
                if (dailyIncome.Count > 0)
                {
                    incoming = dailyIncome.Sum(inc => inc.CashPaymentSum);
                }
                model.Add(new DailyCashFlowModel()
                    {
                        Date = day.ToString("dd/MM/yyyy"),
                        Incoming = incoming,
                        Outgoing = outgoing
                    });

            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [ViewFinancialReports]
        [HttpGet]
        public JsonResult GetMonthlyCashFlow(int month, int year, int issuerId)
        {
            var fromDate = new DateTime(year, month, 1);
            var toDate = fromDate.AddMonths(1); //this assumes in repository criteria is < toDate

            var model = new List<DailyCashFlowModel>();

            var expenses = RapidVetUnitOfWork.ExpenseRepository.GetExpenses(CurrentUser.ActiveClinicId, fromDate, toDate);
            //var financeDocuments =
            //   RapidVetUnitOfWork.FinanceDocumentReposetory.GetDocumentsForClinicFromDate(CurrentUser.ActiveClinicId, fromDate, toDate, issuerId);
            //               //  .Where(d => d.CashPaymentSum > 0);

            var payments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetPaymentsInRange(CurrentUser.ActiveClinicId, issuerId, fromDate, toDate);

            for (var day = fromDate.Date; day.Date < toDate.Date; day = day.AddDays(1))
            {
                //outgoing expenses
                decimal outgoing = 0;
                var dailyExpenses =
                    expenses.Where(
                        e => e.PaymentDate.HasValue &&
                        e.PaymentDate.Value.Day == day.Day && e.PaymentDate.Value.Month == day.Month &&
                        e.PaymentDate.Value.Year == day.Year).ToList();
                if (dailyExpenses.Count > 0)
                {
                    outgoing = dailyExpenses.Sum(e => e.TotalAmount);
                }
                //incoming cash
                decimal incoming = 0;
                //var dailyIncome =
                //    financeDocuments.Where(e => e.Created.Day == day.Day &&
                //                                e.Created.Month == day.Month &&
                //                                e.Created.Year == day.Year).ToList();

                var dailyIncome = payments.Where(p =>
                    ((p.PaymentTypeId == (int)PaymentType.Cash && p.Created.Value.Day == day.Day && p.Created.Value.Month == day.Month && p.Created.Value.Year == day.Year) ||
                        (p.PaymentTypeId != (int)PaymentType.Cash && p.DueDate.Value.Day == day.Day && p.DueDate.Value.Month == day.Month && p.DueDate.Value.Year == day.Year))).ToList();

                if (dailyIncome.Count > 0)
                {
                    incoming = dailyIncome.Sum(inc => inc.Sum);
                }
                model.Add(new DailyCashFlowModel()
                {
                    Date = day.ToString("dd/MM/yyyy"),
                    Incoming = incoming,
                    Outgoing = outgoing
                });

            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [ViewFinancialReports]
        public FileResult GetYearlyCashFlowChart(int year, int issuerId)
        {
            var chartImage = new Chart();
            chartImage.Width = 800;
            chartImage.Height = 400;
            chartImage.ChartAreas.Add("chart");
            chartImage.ChartAreas["chart"].AxisX.MajorGrid.LineColor = Color.LightGray;
            chartImage.ChartAreas["chart"].AxisY.MajorGrid.LineColor = Color.LightGray;

            chartImage.Titles.Add("תזרים מזומנים");

            chartImage.Series.Add("incoming");
            chartImage.Series["incoming"].ChartType = SeriesChartType.Column;
            chartImage.Series["incoming"].IsValueShownAsLabel = true;


            chartImage.Series.Add("outgoing");
            chartImage.Series["outgoing"].ChartType = SeriesChartType.Column;
            chartImage.Series["outgoing"].IsValueShownAsLabel = true;

            chartImage.ChartAreas[0].AxisX.Interval = 1;

            chartImage.Legends.Add(new Legend("legend"));
            chartImage.Series["incoming"].Legend = "legend";
            chartImage.Series["outgoing"].Legend = "legend";

            var firstDay = new DateTime(year, 1, 1);
            var lastDay = new DateTime(year, 12, 31);
            var allExpenses = RapidVetUnitOfWork.ExpenseRepository.GetExpenses(CurrentUser.ActiveClinicId, firstDay,
                                                                                 lastDay);
            //var allIncomes = RapidVetUnitOfWork.FinanceDocumentReposetory.GetDocumentsForClinicFromDate(CurrentUser.ActiveClinicId,
            //                                                                                            firstDay, lastDay, issuerId).Where(d => d.CashPaymentSum > 0);
            var payments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetPaymentsInRange(CurrentUser.ActiveClinicId, issuerId, firstDay, lastDay);


            //we want: a graph for incoming and outgoing expenses by months
            for (var month = 1; month <= 12; month++)
            {
                decimal outgoing = 0;
                var expenses = allExpenses.Where(e => e.PaymentDate.HasValue && e.PaymentDate.Value.Month == month).ToList();
                if (expenses.Count > 0)
                {
                    outgoing = expenses.Sum(e => e.TotalAmount);
                }
                decimal incoming = 0;
                var incomes = payments.Where(p =>
                    ((p.PaymentTypeId == (int)PaymentType.Cash && p.Created.Value.Month == month) ||
                     (p.PaymentTypeId != (int)PaymentType.Cash && p.DueDate.Value.Month == month))).ToList();//fd => fd.Created.Month == month).ToList();
                if (incomes.Count > 0)
                {
                    incoming = incomes.Sum(inc => inc.Sum);
                }

                //insert to graph

                chartImage.Series["incoming"].Points.Add(new DataPoint()
                    {
                        AxisLabel = month.ToString(),
                        YValues = new double[] { (double)incoming }
                    });
                chartImage.Series["outgoing"].Points.Add(new DataPoint()
                    {
                        AxisLabel = month.ToString(),
                        YValues = new double[] { (double)outgoing }
                    });

            }

            chartImage.Series["incoming"].Name = "הכנסות בש\"ח";
            chartImage.Series["outgoing"].Name = "הוצאות בש\"ח";

            using (var strm = new MemoryStream())
            {

                chartImage.SaveImage(strm, ChartImageFormat.Png);
                strm.Seek(0, SeekOrigin.Begin);
                return File(strm.ToArray(), "image/png", "cashflow.png");
            }
        }

        [ViewFinancialReports]
        public ActionResult YearlyCashFlow()
        {
            return View();
        }
        //##########################################################################
        //#############  from this point on - everything is private  ###############
        //##########################################################################

        private List<InvoiceReportWebModel> GetInvoiceReportDataHelper(string from, string to, bool showOnlyConverted, int issuerId)
        {
            DateTime? fromDate = null;
            DateTime? toDate = null;
            if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to) && from != "undefined" && to != "undefined")
            {
                fromDate = StringUtils.ParseStringToDateTime(from);
                toDate = StringUtils.ParseStringToDateTime(to).AddDays(1);
            }

            var documents = RapidVetUnitOfWork.FinanceReportRepository.GetInvoiceReportData(CurrentUser.ActiveClinicId, fromDate, toDate, showOnlyConverted, issuerId);
            var model = new List<InvoiceReportWebModel>();
            var inVoiceFinanceDocumentIds = documents.FindAll(x => (FinanceDocumentType)x.FinanceDocumentTypeId == FinanceDocumentType.Invoice || (FinanceDocumentType)x.FinanceDocumentTypeId == FinanceDocumentType.InvoiceReceipt).Select(x => x.FinanceDocumentId).ToList();
            var recieptFinanceDocumentIds = documents.FindAll(x => (FinanceDocumentType)x.FinanceDocumentTypeId == FinanceDocumentType.Receipt).Select(x => x.FinanceDocumentId).ToList();
            var refoundFinanceDocumentIds = documents.FindAll(x => (FinanceDocumentType)x.FinanceDocumentTypeId == FinanceDocumentType.Refound).Select(x => x.FinanceDocumentId).ToList();
            var inVoicePayments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetPaymentsForDocumentList(inVoiceFinanceDocumentIds, FinanceDocumentType.Invoice);
            var recieptPayments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetPaymentsForDocumentList(recieptFinanceDocumentIds, FinanceDocumentType.Receipt);
            var refoundPayments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetPaymentsForDocumentList(refoundFinanceDocumentIds, FinanceDocumentType.Refound);
            foreach (var item in documents)
            {
                item.Date = item.Created.ToShortDateString();
                item.FinanceDocumentType = Resources.Enums.Finances.FinanceDocumentType.ResourceManager.GetString(((FinanceDocumentType)item.FinanceDocumentTypeId).ToString());
                var fdID = item.FinanceDocumentId;
                //var item = AutoMapper.Mapper.Map<FinanceDocument, InvoiceReportWebModel>(document);
                List<InvoiceReportPaymentWebModel> payments = new List<InvoiceReportPaymentWebModel>();
                switch ((FinanceDocumentType)item.FinanceDocumentTypeId)
                {
                    case FinanceDocumentType.Invoice:
                    case FinanceDocumentType.InvoiceReceipt:
                        payments = inVoicePayments.FindAll(x => x.InvoiceId == fdID);
                        break;
                    case FinanceDocumentType.Receipt:
                        payments = recieptPayments.FindAll(x => x.ReciptId == fdID);
                        break;
                    case FinanceDocumentType.Refound:
                        payments = refoundPayments.FindAll(x => x.RefoundId == fdID);
                        break;
                    default:
                        break;
                }

                //תמיד לקח את הראשון, צריך לבדוק אם זה תקין לפי האפיון, ואם בכלל צריך את האחרון
                var firstCreditPayment = payments.Find(p => p.PaymentTypeId == (int)PaymentType.CreditCard);
                if (firstCreditPayment != null)
                {
                    item.CreditCardBusinessType = firstCreditPayment.IssuerCreditTypeName;
                    item.CreditCardCompany = firstCreditPayment.CreditCardCodeName;
                }

                var transferCheques = payments.FindAll(p => p.PaymentTypeId == (int)PaymentType.Transfer || p.PaymentTypeId == (int)PaymentType.Cheque);
                if (transferCheques != null && transferCheques.Count > 0)
                {
                    if (!String.IsNullOrEmpty(transferCheques[0].BankCodeName))
                        item.BankName = transferCheques[0].BankCodeName;

                    item.BankBranchName = transferCheques[0].BankBranch;
                    item.BankAccountNumber = transferCheques[0].BankAccount;
                    var firstChequePayment = transferCheques.Find(p => p.PaymentTypeId == (int)PaymentType.Cheque);
                    if (firstChequePayment != null)
                        item.ChequeNumber = firstChequePayment.ChequeNumber;
                }

                if (item.FinanceDocumentTypeId == (int)FinanceDocumentType.Refound)
                {
                    item.Cash = item.SumToPayForInvoice;
                    item.TotalAmount = item.SumToPayForInvoice;
                }

                if (string.IsNullOrEmpty(item.ClientName))
                    item.ClientName = Resources.Migration.TempClient;

                model.Add(item);
            }
            return model;
        }

        private List<InvoiceReportWebModel> GetReciptReportDataHelper(string from, string to, int issuerId)
        {
            DateTime? fromDate = null;
            DateTime? toDate = null;
            if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to) && from != "undefined" && to != "undefined")
            {
                fromDate = StringUtils.ParseStringToDateTime(from);
                toDate = StringUtils.ParseStringToDateTime(to).AddDays(1);
            }
            var model = RapidVetUnitOfWork.FinanceReportRepository.GetReciptReportDataAll(CurrentUser.ActiveClinicId, fromDate, toDate, issuerId).ToList();
          
            return model;
        }


        private List<ChequeReportModel> GetChequesReportData(string from, string to, int documentTypeId,
                                                             bool paidCheques, int issuerId)
        {
            var fromDate = StringUtils.ParseStringToDateTime(from);
            var toDate = StringUtils.ParseStringToDateTime(to);
            var payments = RapidVetUnitOfWork.FinanceReportRepository.GetCheques(CurrentUser.ActiveClinicId, fromDate,
                                                                                 toDate, documentTypeId, paidCheques, issuerId);
            var results = new List<ChequeReportModel>();
            if (payments != null && payments.Any())
            {
                foreach (var p in payments)
                {
                    var clientName = string.Empty;
                    var dateStr = string.Empty;
                    var documentSerial = 0;

                    if (p.Invoice != null)
                    {
                        clientName = p.Invoice.ClientName;
                        dateStr = p.Invoice.Created.ToShortDateString();
                        documentSerial = p.Invoice.SerialNumber;
                    }
                    else if (p.Recipt != null)
                    {
                        clientName = p.Recipt.ClientName;
                        dateStr = p.Recipt.Created.ToShortDateString();
                        documentSerial = p.Recipt.SerialNumber;
                    }

                    var reportItem = AutoMapper.Mapper.Map<FinanceDocumentPayment, ChequeReportModel>(p);
                    reportItem.DateStr = dateStr;
                    reportItem.ClientName = clientName;
                    reportItem.FinanceDocumentSerial = documentSerial;

                    results.Add(reportItem);
                }
            }

            return results;
        }



        private List<DetectionWebModel> GetIncomeReportData(IncomeReportFilter filter)
        {
            if (!filter.IssuerId.HasValue)
                return new List<DetectionWebModel>();

            var fromDate = StringUtils.ParseStringToDateTime(filter.From);
            var toDate = StringUtils.ParseStringToDateTime(filter.To);
            var documents = RapidVetUnitOfWork.FinanceReportRepository
                                              .GetIncomeReport(CurrentUser.ActiveClinicId,
                                                               fromDate, toDate, filter.IssuerId,
                                                               filter.SelectedPaymentType, filter.NotSelectedDocIds);


            var result = documents.Select(AutoMapper.Mapper.Map<FinanceDocument, DetectionWebModel>).ToList();

            foreach (var item in result)
            {
                if (string.IsNullOrEmpty(item.ClientName))
                {
                    item.ClientName = Resources.Migration.TempClient;
                }
                item.PrintUrl = Url.Action("Print", "FinanceDocument", new { id = item.Id });
            }

            return result;
        }

        private List<AnnualBalanceWebModel> GetAnnualReportData(int? issuerId, int? year)
        {
            var result = new List<AnnualBalanceWebModel>();
            var startYear = year.HasValue && year.Value > 1900 ? year.Value : DateTime.Now.Year;
            for (int i = 1; i < 13; i++)
            {
                var monthStart = new DateTime(startYear, i, 1);
                decimal income = 0;
                decimal vatToPay = 0;
                decimal advancedPayments = 0;
                var advances =
                    RapidVetUnitOfWork.FinanceReportRepository.GetLastAdvanceItemInMonth(CurrentUser.ActiveClinicId,
                                                                                         monthStart, issuerId.Value);
                var permanentAssetExpenses =
                   RapidVetUnitOfWork.FinanceReportRepository.SumPermanentAssetExpensesInMonth(
                       CurrentUser.ActiveClinicId, monthStart);

                var notPermanentExpenses =
                    RapidVetUnitOfWork.FinanceReportRepository.GetNotPermanentExpensesInMonth(
                        CurrentUser.ActiveClinicId, monthStart);

                var documents = RapidVetUnitOfWork.FinanceReportRepository.GetIssuerDocuments(issuerId, monthStart, CurrentUser.ActiveClinicId);

                if (documents.Any())
                {
                    var documentsForCalculations = documents.ToList();
                    vatToPay = documentsForCalculations.Sum(d => d.VatPayed);
                    income = Decimal.Round(documentsForCalculations.Sum(d => d.TotalSum) - vatToPay, 2);
                    advancedPayments = RapidVetUnitOfWork.FinanceReportRepository.GetAdvancedPaymentPercentByDate(CurrentUser.ActiveClinicId, monthStart, issuerId.Value) * income;

                }

                var monthlyBalance = new AnnualBalanceWebModel()
                    {
                        MonthName = Resources.Enums.Months.ResourceManager.GetString(((Months)i).ToString()),
                        Advances = advancedPayments,//advances,
                        Expenses = notPermanentExpenses,
                        Income = income,
                        PermanentAssets = permanentAssetExpenses,
                        VatToPay = vatToPay,

                    };

                result.Add(monthlyBalance);
            }
            var bottomLine = new AnnualBalanceWebModel()
                {
                    MonthName = "סה\"כ",
                    Advances = result.Sum(l => l.Advances),
                    Expenses = result.Sum(l => l.Expenses),
                    Income = result.Sum(l => l.Income),
                    PermanentAssets = result.Sum(l => l.PermanentAssets),
                    VatToPay = result.Sum(l => l.VatToPay)
                };
            result.Add(bottomLine);

            return result;
        }




    }
}
