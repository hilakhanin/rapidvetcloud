﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using RapidVet.Enums.Finances;

namespace RapidVet.Web.Controllers.Finance
{
    [Authorize]
    public class FinanceDocumentCollectivePrintingController : BaseController
    {
        //
        // GET: /FinanceDocumentCollectivePrinting/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetDocumentTypeOptions()
        {
            var options = new List<SelectListItem>();
            var rm = Resources.Enums.Finances.FinanceDocumentType.ResourceManager;
            foreach (var docType in Enum.GetValues(typeof(FinanceDocumentType)))
            {
                options.Add(new SelectListItem()
                    {
                        Text = rm.GetString(docType.ToString()),
                        Selected = false,
                        Value = ((int)docType).ToString()
                    });
            }
            return Json(options, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetFromSerialNumberOptions(FinanceDocumentType documentType)
        {
            var clinicId = CurrentUser.ActiveClinic.Id;
            var clinicDocuments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetDocumentsForClinic(clinicId,
                                                                                                     documentType);
            //var options = clinicDocuments.Select(d => new SelectListItem()
            //    {
            //        Selected = false,
            //        Text = d.SerialNumber.ToString(),
            //        Value = d.Id.ToString()
            //    }).ToList();

            var data = new { Max = clinicDocuments.DefaultIfEmpty().Max(d => d == null ? 0 : d.SerialNumber), Min = clinicDocuments.DefaultIfEmpty().Min(d => d == null ? 0 : d.SerialNumber) };

            return Json(data, JsonRequestBehavior.AllowGet);
        }
  
        public ActionResult PrintByDate(String from, String to, FinanceDocumentType documentType)
        {
            if (!String.IsNullOrEmpty(from) && !String.IsNullOrEmpty(to) && from!="undefined" && to!="undefined")
            {
                var fromDate = DateTime.ParseExact(from, "dd/MM/yyyy", null);
                var toDate = DateTime.ParseExact(to, "dd/MM/yyyy", null).AddDays(1).AddTicks(-1);

                var clinicId = CurrentUser.ActiveClinic.Id;
                var clinicDocuments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetDocumentsForClinic(clinicId,
                                                                                                         documentType);
                var relevantDocuments = clinicDocuments.Where(fd => fd.Created >= fromDate && fd.Created <= toDate);
                String ids = "";
                foreach (var doc in relevantDocuments)
                {
                    ids += "," + doc.Id;
                }
                if (ids.Length >= 2)
                {
                    ids = ids.Substring(1);
                }

                return RedirectToAction("PrintMultiDocuments", "FinanceDocument", new {documentIds = ids});
            }
            return RedirectToAction("PrintMultiDocuments", "FinanceDocument", new { documentIds = "" });
        }

        public ActionResult PrintBySerialRange(String from, String to, FinanceDocumentType documentType, int issuerId)
        {
            if (!String.IsNullOrEmpty(from) && !String.IsNullOrEmpty(to) && from != "undefined" && to != "undefined")
            {
                var fromSerial = int.Parse(from);
                var toSerial = int.Parse(to);

                var clinicId = CurrentUser.ActiveClinic.Id;
                var clinicDocuments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetDocumentsForClinic(clinicId,
                                                                                                         documentType);
                var relevantDocuments = clinicDocuments.Where(fd => fd.SerialNumber >= fromSerial && fd.SerialNumber <= toSerial && fd.IssuerId == issuerId);
                String ids = "";
                foreach (var doc in relevantDocuments)
                {
                    ids += "," + doc.Id;
                }
                if (ids.Length >= 2)
                {
                    ids = ids.Substring(1);
                }

                return RedirectToAction("PrintMultiDocuments", "FinanceDocument", new { documentIds = ids });
            }
            return RedirectToAction("PrintMultiDocuments", "FinanceDocument", new { documentIds = "" });
        }

    }
}
