﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVet.Enums.Finances;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class FinanceMetaDataController : BaseController
    {
        public JsonResult GetClinicIssuers()
        {
            var issuers = RapidVetUnitOfWork.IssuerRepository.GetList(CurrentUser.ActiveClinicId, CurrentUser.Id);
            var model = issuers.Select(AutoMapper.Mapper.Map<Issuer, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFinanceDocumentTypes()
        {
            var list = from FinanceDocumentType f in Enum.GetValues(typeof(FinanceDocumentType))
                       select new SelectListItem()
                       {
                           Text = RapidVet.Resources.Enums.Finances.FinanceDocumentType.ResourceManager.GetString(f.ToString()),
                           Value = ((int)f).ToString(),
                       };

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPaymentTypes()
        {
            var list = from PaymentType p in Enum.GetValues(typeof(PaymentType))
                       select new SelectListItem()
                       {
                           Text = RapidVet.Resources.Enums.Finances.PaymentType.ResourceManager.GetString(p.ToString()),
                           Value = ((int)p).ToString(),
                       };

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPriceListCategories()
        {
            var categories = RapidVetUnitOfWork.CategoryRepository.GetClinicActiveCategories(CurrentUser.ActiveClinicId);
            var model = categories.Select(AutoMapper.Mapper.Map<PriceListCategory, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //id is category id
        public JsonResult GetPriceListItemsForCategory(int id)
        {
            var items = RapidVetUnitOfWork.PriceListRepository.GetItemsByCategory(id);
            var model = items.Select(AutoMapper.Mapper.Map<PriceListItem, SelectListItem>).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }


    }
}
