﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RapidVet.Enums.Finances;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Quotations;
using RapidVet.Model.Visits;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Finance.Invoice;
using RapidVet.Repository.Clinics;
using RapidVet.Model;
using RapidVet.Model.InternalSettlements;



namespace RapidVet.Web.Controllers
{

    [Authorize]
    public class FinanceDocumentController : BaseController
    {
        public ActionResult InvoiceReceipt(int? clientId)
        {
            if (!clientId.HasValue && Request.Cookies["ClientId"] != null)
            {
                //if (DateTime.Now < Request.Cookies["ClientId"].Expires)
                //{
                clientId = int.Parse(Request.Cookies["ClientId"].Value);

                var ClientCookie = new HttpCookie("ClientId");
                ClientCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(ClientCookie);
                //}
            }

            ViewBag.ClientId = clientId.HasValue ? clientId.Value : 0;
            ViewBag.TaxRate = GetTaxRate();
            ViewBag.VatFactor = 1 + ViewBag.TaxRate * (decimal)0.01;
            ViewBag.Receipt = "true";
            ViewBag.Invoice = "true";
            ViewBag.Title = CurrentUser.ActiveClinic.InvoiceReciptOnly ? "חשבונית מס קבלה חדשה" : "חשבונית ללקוח";
            ViewBag.DocType = (int)FinanceDocumentType.InvoiceReceipt;
            ViewBag.DefaultTariff = CurrentUser.ActiveClinic.DefualtTariffId;
            ViewBag.QuotationId = 0;
            int defaultIssuerId = CurrentUser.DefaultIssuerEmployerId.HasValue && CurrentUser.DefaultIssuerEmployerId.Value != 0 ? CurrentUser.DefaultIssuerEmployerId.Value : 0;
            ViewBag.DefaultIssuerId = IsIssuerInClinic(defaultIssuerId) ? defaultIssuerId : 0;
            ViewBag.UpdateItemInVisit = !CurrentUser.ActiveClinic.DontAddToVisitNewItemsInInvoice;

            return View();
        }

        //private decimal getTaxRate()
        //{
        //    decimal TaxRate = CurrentUser.ActiveClinic.TaxRate;
        //    if (TaxRate == 0)
        //    {
        //        int id = Convert.ToInt32(CurrentUser.ActiveClinic.Id);
        //        ClinicRepository clcr = new ClinicRepository();
        //        var rate = clcr.GetAllTaxRateChanges(id).FirstOrDefault();
        //        TaxRate = rate != null ? rate.Rate : 0;
        //    }

        //    return TaxRate;
        //}

        public ActionResult Invoice(int? clientId, int? quotationId)
        {
            ViewBag.ClientId = clientId.HasValue ? clientId.Value : 0;
            ViewBag.TaxRate = GetTaxRate();
            ViewBag.VatFactor = 1 + ViewBag.TaxRate * (decimal)0.01;
            ViewBag.DocType = (int)FinanceDocumentType.Invoice;
            ViewBag.Receipt = "false";
            ViewBag.Invoice = "true";
            ViewBag.Title = "חשבונית מס חדשה";
            ViewBag.DefaultTariff = CurrentUser.ActiveClinic.DefualtTariffId;
            ViewBag.QuotationId = quotationId.HasValue ? quotationId.Value : 0;
            int defaultIssuerId = CurrentUser.DefaultIssuerEmployerId.HasValue && CurrentUser.DefaultIssuerEmployerId.Value != 0 ? CurrentUser.DefaultIssuerEmployerId.Value : 0;
            ViewBag.DefaultIssuerId = IsIssuerInClinic(defaultIssuerId) ? defaultIssuerId : 0;
            ViewBag.UpdateItemInVisit = !CurrentUser.ActiveClinic.DontAddToVisitNewItemsInInvoice;

            return View("InvoiceReceipt");
        }

        public ActionResult Proforma(int? clientId)
        {
            ViewBag.ClientId = clientId.HasValue ? clientId.Value : 0;
            ViewBag.TaxRate = GetTaxRate();
            ViewBag.VatFactor = 1 + ViewBag.TaxRate * (decimal)0.01;
            ViewBag.DocType = (int)FinanceDocumentType.Proforma;
            ViewBag.Receipt = "false";
            ViewBag.Invoice = "true";
            ViewBag.Title = "חשבונית עסקה חדשה";
            ViewBag.DefaultTariff = CurrentUser.ActiveClinic.DefualtTariffId;
            ViewBag.QuotationId = 0;
            int defaultIssuerId = CurrentUser.DefaultIssuerEmployerId.HasValue && CurrentUser.DefaultIssuerEmployerId.Value != 0 ? CurrentUser.DefaultIssuerEmployerId.Value : 0;
            ViewBag.DefaultIssuerId = IsIssuerInClinic(defaultIssuerId) ? defaultIssuerId : 0;
            ViewBag.UpdateItemInVisit = false;// !CurrentUser.ActiveClinic.DontAddToVisitNewItemsInInvoice;

            return View("InvoiceReceipt");
        }

        public ActionResult Receipt(int? clientId)
        {
            ViewBag.ClientId = clientId.HasValue ? clientId.Value : 0;
            ViewBag.TaxRate = GetTaxRate();
            ViewBag.TaxRateOrg = GetTaxRate();
            ViewBag.VatFactor = 1 + ViewBag.TaxRate * (decimal)0.01;
            ViewBag.DocType = (int)FinanceDocumentType.Receipt;
            ViewBag.Receipt = "true";
            ViewBag.Invoice = "false";
            ViewBag.Title = "קבלה חדשה";
            ViewBag.DefaultTariff = CurrentUser.ActiveClinic.DefualtTariffId;
            ViewBag.QuotationId = 0;
            ViewBag.NoMham = true;
            int defaultIssuerId = CurrentUser.DefaultIssuerEmployerId.HasValue && CurrentUser.DefaultIssuerEmployerId.Value != 0 ? CurrentUser.DefaultIssuerEmployerId.Value : 0;
            ViewBag.DefaultIssuerId = IsIssuerInClinic(defaultIssuerId) ? defaultIssuerId : 0;
            ViewBag.UpdateItemInVisit = !CurrentUser.ActiveClinic.DontAddToVisitNewItemsInInvoice;

            return View("InvoiceReceipt");
        }

        public ActionResult Refound(int? clientId)
        {
            ViewBag.ClientId = clientId.HasValue ? clientId.Value : 0;
            ViewBag.TaxRate = GetTaxRate();
            ViewBag.VatFactor = 1 + ViewBag.TaxRate * (decimal)0.01;
            ViewBag.DocType = (int)FinanceDocumentType.Refound;
            ViewBag.Receipt = "false";
            ViewBag.Invoice = "true";
            ViewBag.Title = "חשבונית זיכוי חדשה";
            ViewBag.DefaultTariff = CurrentUser.ActiveClinic.DefualtTariffId;
            ViewBag.QuotationId = 0;
            int defaultIssuerId = CurrentUser.DefaultIssuerEmployerId.HasValue && CurrentUser.DefaultIssuerEmployerId.Value != 0 ? CurrentUser.DefaultIssuerEmployerId.Value : 0;
            ViewBag.DefaultIssuerId = IsIssuerInClinic(defaultIssuerId) ? defaultIssuerId : 0;
            ViewBag.UpdateItemInVisit = !CurrentUser.ActiveClinic.DontAddToVisitNewItemsInInvoice;

            return View("InvoiceReceipt");
        }

        public ActionResult Print(int id, bool original = false)
        {
            FinanceDocument model = RapidVetUnitOfWork.FinanceDocumentReposetory.GetItem(id);
            if (model.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException();
            }
            ViewBag.OriginalString = "העתק";
            if (!model.OrginalPrintedDate.HasValue)
            {
                model.OrginalPrintedDate = DateTime.Now;
                RapidVetUnitOfWork.Save();
                ViewBag.OriginalString = "מקור";
            }
            else
            {
                ViewBag.OriginalDateString = model.OrginalPrintedDate.Value.ToShortDateString() + " " + model.OrginalPrintedDate.Value.ToShortTimeString();
            }

            ViewBag.PrinterClass = "stdPrint";

            if (model.ClientId == null && String.IsNullOrEmpty(model.ClientName))
            {
                model.ClientName = "לקוח מזדמן";
            }
            return View(model);
        }

        public ActionResult PrintMultiDocuments(string documentIds)
        {
            var model = new List<String>();
            if (!String.IsNullOrEmpty(documentIds))
            {
                var splitIds = documentIds.Split(',');
                model = splitIds.Where(id => !string.IsNullOrWhiteSpace(id)).ToList();
            }
            return View(model);
        }

        [HttpGet]
        public JsonResult GetNewInvoiceReceipt(int? clientId, FinanceDocumentType documentType, int issuerId)
        {
            var clinic = CurrentUser.ActiveClinic; // issuerId == 1 ? RapidVetUnitOfWork.ClinicRepository.GetItem(1) : //protection against general request
            var model = new EmptyFinanceDocument();

            var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId);
            model.DoctorOptions = doctors.Select(u => new SelectListItem()
                {
                    Selected = false,
                    Text = u.Name,
                    Value = u.Id.ToString()
                }).ToList();

            model.IssuerOptions = RapidVetUnitOfWork.IssuerRepository.GetDropdownList(CurrentUser.ActiveClinicId, -1);
            model.BankCodeOptions = RapidVetUnitOfWork.InvoiceRepository.GetBankCodes().Select(bc => new SelectListItem()
                {
                    Selected = false,
                    Text = bc.Name,
                    Value = bc.Id.ToString()
                }).ToList();

            model.CreditPaymentTypeOptions = Helpers.FinanceHelper.CreditPaymentTypeOptions();

            if(issuerId == 0)
            {
                if (model.IssuerOptions.Count > 0)
                    issuerId = int.Parse(model.IssuerOptions.First().Value); //select first issuer
            }

            model.CreditCardCodeOptions =
                RapidVetUnitOfWork.InvoiceRepository.GetCreditCardCodesForClinic(clinic.Id, issuerId).Select(cc => new SelectListItem()
                    {
                        Selected = false,
                        Text = cc.Name,
                        Value = cc.Id.ToString()
                    }).ToList();

            model.TariffOptions =
                RapidVetUnitOfWork.TariffRepository.GetActiveTariffList(CurrentUser.ActiveClinicId).Select(t => new SelectListItem()
                                      {
                                          Selected = false,
                                          Text = t.Name,
                                          Value = t.Id.ToString()
                                      }).ToList();

            model.AddedBy = CurrentUser.Name;
            model.AddedByUserId = CurrentUser.Id;
            //var DefaultDrId = model.DoctorOptions.FirstOrDefault(i => i.Value == CurrentUser.DefaultDrId.ToString());
            //if (DefaultDrId != null)
            //    model.DefaultDrId = DefaultDrId.Value;

            var obj = model.DoctorOptions.FirstOrDefault(i => i.Value == CurrentUser.DefaultDrId.ToString());
            if (obj != null)
            {
                obj.Selected = true;
            }


            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetPriceListItems(int? tariff,bool noMham)
        {
            var tariffId = 0;
            if (!tariff.HasValue || tariff.Value == 0)
            {
                tariffId = CurrentUser.ActiveClinic.DefualtTariffId;
            }
            else
            {
                tariffId = tariff.Value;
            }
            var model = new PriceListJsonModel();
            var itemsDB = RapidVetUnitOfWork.PriceListRepository.GetAllItems(CurrentUser.ActiveClinicId);
            var items = itemsDB.Select(AutoMapper.Mapper.Map<PriceListItem, PriceListItemSingleTariffFlatModel>).ToList();
            model.Items = UpdatePrices(items, tariffId, itemsDB);
            //categories:
            var categoriesDb =
                   RapidVetUnitOfWork.CategoryRepository.GetClinicActiveCategories(CurrentUser.ActiveClinicId).ToList();
            model.Categories =
             categoriesDb.Select(AutoMapper.Mapper.Map<PriceListCategory, PriceListCategoryModel>).ToList();
            for (int i = 0; i < model.Items.Count; i++)
            {
                model.Items[i].NoVAT = noMham; 
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetClientDetails(int? clientId)
        {
            var model = new InvoiceClientDetails();
            if (clientId.HasValue && clientId.Value != 0)
            {
                var client = RapidVetUnitOfWork.ClientRepository.GetClientWithFinanceDocuments(clientId.Value);
                model.Name = client.Name;
                var address = client.Addresses.FirstOrDefault();
                model.Address = address == null ? String.Empty : address.Street + " " + address.City.Name;
                var phone = client.Phones.FirstOrDefault();
                model.Phone = phone == null ? String.Empty : phone.PhoneNumber;
                model.DefaultTariffId = client.TariffId.HasValue
                                            ? client.TariffId.Value
                                            : CurrentUser.ActiveClinic.DefualtTariffId;
                model.OpenInvoices =
                    client.FinanceDocuments.Where(
                        fd =>
                        fd.FinanceDocumentType == FinanceDocumentType.Invoice && fd.FinanceDocumentStatus != FinanceDocumentStatus.Closed).Select(fd => new OpenInvoiceWebmodel()
                            {
                                Id = fd.Id,
                                Date = fd.Created.ToShortDateString(),
                                Sum = fd.TotalSum.ToString("F"),
                                SumToPay = (fd.TotalSum - fd.AmountPayed).ToString("F"),
                                SerialNumber = fd.SerialNumber
                            }).ToList();
                model.Balance = client.Balance.HasValue ? client.Balance.Value : RapidVetUnitOfWork.ClientRepository.GetClientBalance(clientId.Value);
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetIssuerData(int issuerId)
        {
            if(issuerId == 0)
            {
                List<SelectListItem> ddl = RapidVetUnitOfWork.IssuerRepository.GetDropdownList(CurrentUser.ActiveClinicId, -1);
                if(ddl.Count > 0)
                    issuerId = int.Parse(ddl.First().Value);
            }
            var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(issuerId); 
            var creditDealTypes = RapidVetUnitOfWork.IssuerRepository.GetCreditTypes(issuerId);
            var terminals = RapidVetUnitOfWork.IssuerRepository.GetIssuersActiveEasyCardTerminals(issuerId);
            var model = new IssuerJsonModel();
            model.IsEasyCard = terminals.Count > 0;
            model.Terminals = AutoMapper.Mapper.Map<List<EasyCardSettings>, List<EasyCardSettingsJsonModel>>(terminals);
            model.CreditTypes = AutoMapper.Mapper.Map<List<IssuerCreditType>, List<IssuerCreditTypeJsonModel>>(creditDealTypes.ToList());
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetBilledItems(int? clientId)
        {
            decimal totalDiscount = 0;
            var model = new List<FinanceDocumentItemWebmodel>();
            if (!CurrentUser.ActiveClinic.DontMoveItemsFromVisitToInvoice)
            {
                DateTime visitsFromDate;
                if(!CurrentUser.ActiveClinic.MoveItemsFromVisitToInvoiceFromDate.HasValue || CurrentUser.ActiveClinic.MoveItemsFromVisitToInvoiceFromDate.Value.Equals(DateTime.MinValue))
                {
                    visitsFromDate = new DateTime(2000,1,1);
                }
                else
                {
                    visitsFromDate = CurrentUser.ActiveClinic.MoveItemsFromVisitToInvoiceFromDate.Value;
                }

                if (clientId.HasValue && clientId.Value != 0)
                {
                    var unpaidVisits = RapidVetUnitOfWork.InvoiceRepository.GetUnpaidVisits(clientId.Value);
                    foreach (var visit in unpaidVisits)
                    {
                        var treatments = AutoMapper.Mapper.Map<List<VisitTreatment>, List<FinanceDocumentItemWebmodel>>(visit.Treatments.Where(x => visit.VisitDate >= visitsFromDate && !x.Invoiced && (x.Name == null || !x.Name.Contains("התאמת יתרה"))).ToList());
                        var medications = AutoMapper.Mapper.Map<List<VisitMedication>, List<FinanceDocumentItemWebmodel>>(visit.Medications.Where(x => visit.VisitDate >= visitsFromDate && !x.Invoiced).ToList());
                        var examinations = AutoMapper.Mapper.Map<List<VisitExamination>, List<FinanceDocumentItemWebmodel>>(visit.Examinations.Where(x => visit.VisitDate >= visitsFromDate && !x.Invoiced).ToList());
                        var preventiveMedicines = AutoMapper.Mapper.Map<List<PreventiveMedicineItem>, List<FinanceDocumentItemWebmodel>>(visit.PreventiveMedicineItems.Where(x => visit.VisitDate >= visitsFromDate && !x.Invoiced).ToList());
                        model.AddRange(treatments);
                        model.AddRange(medications);
                        model.AddRange(examinations);
                        model.AddRange(preventiveMedicines);
                        //decimal discount = treatments.Sum(t => t.Total) + medications.Sum(m => m.Total) +
                        //               examinations.Sum(e => e.Total) + preventiveMedicines.Sum(p => p.Total) - visit.Price.GetValueOrDefault();
                        //totalDiscount += discount;
                    }

                    if (!CurrentUser.ActiveClinic.MoveZeroSumTreatmentsToInvoice)
                    {
                        model = model.Where(s => s.Total != 0).ToList();
                    }

                    model.OrderBy(item => item.VisitId);
                    //var client = RapidVetUnitOfWork.ClientRepository.GetClient(clientId.Value);
                    //var sum = model.Sum(m => m.Total);
                    //if (sum < client.Balance * -1)
                    //{
                    //    var dif = client.Balance.Value + sum;
                    //    var item = new FinanceDocumentItemWebmodel
                    //        {
                    //            CatalogNumber = "0000",
                    //            Description = "כללי",
                    //            Discount = 0,
                    //            DiscountPercentage = 0,
                    //            UnitPrice = dif * -1,
                    //            Quantity = 1
                    //        };
                    //    model.Add(item);
                }
            }

            return Json(new { items = model, discount = totalDiscount }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetQuotationItems(int id)
        {

            var quotation = RapidVetUnitOfWork.QuotationRepository.GetQuotation(id);
            var model = new FinanceDocumentFromQuotation();
            model.QuotationItems = quotation.Treatments.Select(AutoMapper.Mapper.Map<QuotationTreatment, FinanceDocumentItemWebmodel>).ToList();
            //each quotation unit price is before any discount. now we need to figure in the total discount percent given on the quotation
            var sum = model.QuotationItems.Sum(i => i.Total);
            //  model.GlobalDiscount = sum - quotation.TotalPrice;

            model.AddedBy = quotation.User.Name;
            model.AddedByUserId = quotation.User.Id;

            return Json(model, JsonRequestBehavior.AllowGet);
        }

         [HttpGet]
        public JsonResult GetLastChequeDetails(int id = 0)
        {
            if (id > 0)
            {
                var payment = RapidVetUnitOfWork.FinanceDocumentReposetory.GetLastChequePayment(id);
                var cheque = AutoMapper.Mapper.Map<FinanceDocumentPayment, CheckPaymentWebModel>(payment);
                return Json(cheque, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult SaveDocument(string docJson)
        //{
        //    var model = JsonConvert.DeserializeObject<FinanceDocumentFromJson>(docJson);
        //    return null;
        //}

        //Save list of payment items
        List<FinanceDocumentPayment> payments = new List<FinanceDocumentPayment>();

        /// <summary>
        /// Save new document to data base and return redirect link
        /// </summary>
        /// <param name="docJson"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveDocument(string docJson)
        {
            string strclientId = "";
            var monthStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var monthEnd = monthStart.AddMonths(1);

            var model = JsonConvert.DeserializeObject<FinanceDocumentFromJson>(docJson);

            //check the issuer is in clinic 
            var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(model.IssuerId);
            if (issuer.ClinicId != CurrentUser.ActiveClinicId)
            {
                throw new SecurityException();
            }

            Visit newVisit = saveVisitDetails(model);

            bool isCreditdCardProspne = false;
            // Set CreditPaymentType base on  EasyCardDealType
            if (model.CreditPayment)
            {
                if (model.EasyCard)
                {
                    var creditType = RapidVetUnitOfWork.IssuerRepository.GetIssuerCreditTypeByName(model.EasyCardDealType);//issuer.CreditTypes.FirstOrDefault(c => c.Name == model.EasyCardDealType);
                    if (creditType != null)
                    {
                        model.CreditPaymentType = creditType.Id;
                        if  (creditType.PostponedPayment && !(creditType.Name.ToLower().Contains(Resources.Credit.CreditPayment)))
                        {
                            isCreditdCardProspne = true;
                        }

                    }
                }
                else
                {
                    var creditType = RapidVetUnitOfWork.IssuerRepository.GetIssuerCreditType(model.CreditPaymentType);//issuer.CreditTypes.FirstOrDefault(c => c.Id == model.CreditPaymentType);
                    if (creditType != null)
                    {                       
                        //if (creditType.Name.ToLower().Contains(Resources.Credit.RegularPayment) || (creditType.PostponedPayment && !creditType.IgnorePayments))
                        if (creditType.PostponedPayment && !(creditType.Name.ToLower().Contains(Resources.Credit.CreditPayment)))
                        {
                            isCreditdCardProspne = true;
                        }

                    }
                }
            }
            //check that doctor is in clinic group
            var user = RapidVetUnitOfWork.UserRepository.GetItem(model.DoctorId);
            if (user.ClinicGroupId != CurrentUser.ActiveClinicGroupId)
            {
                throw new SecurityException();
            }

            // var roundingString = model.RoundingFactor.ToString();
            // model.RoundingFactor = model.RoundingFactor != 0 ? Decimal.Parse(roundingString.Substring(0, roundingString.LastIndexOf('.') +3)) : (decimal)0.00;

            model.RoundingFactor = Math.Round(model.RoundingFactor, 2);

            //save document
            //root && items

            var document = AutoMapper.Mapper.Map<FinanceDocument>(model);

            document.FinanceDocumentTypeId = model.DocumentTypeId;

            for (var i = 0; i < document.Items.Count; i++)
            {
                if (document.Items[i].UnitPrice.HasValue)
                {

                    if (!document.Items[i].Discount.HasValue)
                    {
                        document.Items[i].Discount = 0;
                    }

                    if (!document.Items[i].DiscountPercentage.HasValue)
                    {
                        document.Items[i].DiscountPercentage = 0;
                    }

                    if (!document.Items[i].Quantity.HasValue)
                    {
                        document.Items[i].Quantity = 1;
                    }

                    decimal one = 1;
                    if (document.ReciptPriningOnly != null && document.ReciptPriningOnly == true && document.TotalBeforeVATAfterDisscount == document.TotalSum)
                    {
                        decimal VAT = Convert.ToDecimal((((document.Items[i].UnitPrice * 100) / document.Items[i].TotalBeforeVAT) / 100));
                        document.Items[i].TotalBeforeVAT = Convert.ToDecimal((document.Items[i].TotalBeforeVAT * VAT).ToString("N2"));
                        document.Items[i].UnitPrice = Convert.ToDecimal((document.Items[i].UnitPrice.Value * VAT).ToString("N2"));
                    }
                    else
                    {
                        document.Items[i].TotalBeforeVAT = Math.Round(((document.Items[i].UnitPrice.Value * document.VAT * (one - (document.Items[i].DiscountPercentage.Value / (decimal)100)) - document.Items[i].Discount.Value) / document.VAT) * document.Items[i].Quantity.Value, 2);
                    }
                    document.Items[i].UnitPrice = Math.Round(document.Items[i].UnitPrice.Value, 2);
                }
            }


            if (document.FinanceDocumentType == FinanceDocumentType.Invoice ||
                document.FinanceDocumentType == FinanceDocumentType.Proforma)
            {
                document.SumToPayForInvoice = (decimal)model.TotalToPay;
            }
            else if (document.FinanceDocumentType == FinanceDocumentType.Refound)
            {
                if (model.TotalToPay > 0)
                {
                    document.SumToPayForInvoice = (decimal)model.TotalToPay * -1;
                }
                else
                {
                    document.SumToPayForInvoice = (decimal)model.TotalToPay;
                }

            }
            //add invoices to recipt
            if (document.FinanceDocumentType == FinanceDocumentType.Receipt)
            {
                var remaingingSum = document.TotalSum;
                foreach (var relatedDoc in model.RelatedDocuments.OrderBy(d => d.Id))
                {
                    var invoice = RapidVetUnitOfWork.FinanceDocumentReposetory.GetFinanceDocument(relatedDoc.Id);
                    if (remaingingSum >= invoice.TotalSum - invoice.AmountPayed)
                    {
                        remaingingSum -= invoice.TotalSum - invoice.AmountPayed;
                        document.RelatedDocuments.Add(invoice);
                        //if (invoice.FinanceDocumentStatus == FinanceDocumentStatus.PartlyClose)
                        //{
                        //    invoice.DocumentCloseNote += string.Format(", נסגר חלקית בקבלה {0} בסכום של {1}", document.SerialNumber, (invoice.TotalSum - invoice.AmountPayed).ToString("F"));
                        //}
                        //else //if open
                        //{
                        //    invoice.DocumentCloseNote += string.Format("נסגר במלאו בקבלה {0} ,", document.SerialNumber);
                        //}
                        invoice.FinanceDocumentStatus = FinanceDocumentStatus.Closed;
                      //  document.DocumentCloseNote += string.Format("סוגר במלאו חשבונית {0} ,", invoice.SerialNumber);
                    }
                    else if (remaingingSum > 0)
                    {
                        invoice.FinanceDocumentStatus = FinanceDocumentStatus.PartlyClose;
                        invoice.AmountPayed = remaingingSum;
                        remaingingSum = 0;
                        document.RelatedDocuments.Add(invoice);
                        //document.DocumentCloseNote += string.Format("סוגר חלקית חשבונית {0} בסכום של {1}", invoice.SerialNumber, invoice.AmountPayed.ToString("F"));
                        //invoice.DocumentCloseNote += string.Format("נסגר במלאו בקבלה {0} ,", document.SerialNumber);
                    }
                  //  invoice.DocumentCloseNote = !string.IsNullOrWhiteSpace(invoice.DocumentCloseNote) ? invoice.DocumentCloseNote.TrimEnd(',') : null;
                    if(document.Items == null)
                    {
                        document.Items = new List<FinanceDocumentItem>();
                    }

                    document.Items.Add(new FinanceDocumentItem()
                    {
                        AddedBy = CurrentUser.Name,
                        AddedByUserId = CurrentUser.Id,
                        Description = string.Format(Resources.Finance.FromInvoiceNumber, invoice.SerialNumber),
                        FinanceDocumentId = document.Id,
                        PriceListItemId = null,
                        Quantity = 1,
                        VisitId = null,
                        UnitPrice = invoice.TotalBeforeVAT,
                        TotalBeforeVAT = invoice.TotalBeforeVAT,
                        Discount = 0,
                        DiscountPercentage =0
                    });
                  //fix for global discount calculation 
                    model.TotalAfterDiscount += (double)(invoice.TotalBeforeVAT - invoice.Discount);
                }
                //if (model.RelatedDocuments.Any())
                //{
                //    document.DocumentCloseNote = !string.IsNullOrWhiteSpace(document.DocumentCloseNote) ? document.DocumentCloseNote.TrimEnd(',') : null;
                //}
            }

            FinanceDocument recipt = null;
            //if this document is invoice recipt and check for recit printing
            if (document.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt && !CurrentUser.ActiveClinic.InvoiceReciptOnly)
            {

                if (model.Cheques.Count(c => c.ChequeDate >= monthEnd) > 0 || isCreditdCardProspne)
                {
                    recipt = AutoMapper.Mapper.Map<FinanceDocument>(model);
                    recipt.FinanceDocumentType = FinanceDocumentType.Receipt;

                    for (var i = 0; i < recipt.Items.Count; i++)
                    {
                        if (recipt.Items[i].UnitPrice.HasValue)
                        {

                            if (!recipt.Items[i].Discount.HasValue)
                            {
                                recipt.Items[i].Discount = 0;
                            }

                            if (!recipt.Items[i].DiscountPercentage.HasValue)
                            {
                                recipt.Items[i].DiscountPercentage = 0;
                            }

                            if (!recipt.Items[i].Quantity.HasValue)
                            {
                                recipt.Items[i].Quantity = 1;
                            }

                            decimal one = 1;
                            recipt.Items[i].TotalBeforeVAT = Math.Round(((recipt.Items[i].UnitPrice.Value * recipt.VAT * (one - (recipt.Items[i].DiscountPercentage.Value / (decimal)100)) - recipt.Items[i].Discount.Value) / recipt.VAT) * recipt.Items[i].Quantity.Value, 2);
                            recipt.Items[i].UnitPrice = Math.Round(recipt.Items[i].UnitPrice.Value, 2);
                        }
                    }
                    if (recipt.ClientId == 0)
                    {
                        recipt.ClientId = null;
                    }
                    if (recipt != null && recipt.Client != null)
                        strclientId = recipt.Client.ToString();
                    recipt.Created = DateTime.Now;
                    recipt.CreatorUserId = CurrentUser.Id;
                    recipt.ClinicId = CurrentUser.ActiveClinicId;
                    recipt.Discount = 0;
                    recipt.CashPaymentSum = 0;
                    recipt.BankTransferSum = 0;
                    recipt.TotalChequesPayment = 0;
                    recipt.TotalCreditPaymentSum = 0;
                    recipt.UserId = model.DoctorId;
                    //remove visit id from empty items
                    foreach (var item in recipt.Items.Where(v => v.VisitId == 0).ToList())
                    {
                        item.VisitId = null;
                    }
                }
            }

            document.UserId = model.DoctorId;
            if (newVisit != null)
            {
                newVisit.Recipt = document;
            }
            if (document.ClientId == 0)
            {
                document.ClientId = null;
            }
            if (document != null && document.Client != null)
                strclientId = document.Client.ToString();
            document.Created = DateTime.Now;
            document.CreatorUserId = CurrentUser.Id;
            document.ClinicId = CurrentUser.ActiveClinicId;
            if (model.NoVAT)
            {
                document.VAT = 1;
            }
            if (document.Items == null)
            {
                document.Items = new List<FinanceDocumentItem>();
            }

            if (document.FinanceDocumentType == FinanceDocumentType.Proforma || document.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt || document.FinanceDocumentType == FinanceDocumentType.Invoice || document.FinanceDocumentType == FinanceDocumentType.Refound)
            {
                document.Discount = Math.Round((decimal)model.GlobalDiscount, 2);
                if (model.GlobalDiscountIsPercent)
                {
                    document.Discount = Math.Round(((decimal)model.GlobalDiscount / 100) * (decimal)model.TotalBeforeVAT, 2);
                }
            }
            else
            {
                document.Discount = Math.Round(((decimal)model.TotalBeforeVAT * ((decimal)model.VAT)) - (decimal)model.TotalAfterDiscount, 2);
            }
            //Items
            foreach (var visitId in document.Items.GroupBy(d => d.VisitId).Select(d => d.First().VisitId))
            {
                if (visitId.HasValue && visitId > 0)
                {
                    var visit = RapidVetUnitOfWork.VisitRepository.GetVisit(visitId.Value);
                    visit.Recipt = document;
                    visit.UpdatedDate = DateTime.Now;
                    visit.Close = true;
                   // RapidVetUnitOfWork.FinanceDocumentReposetory.SetInvoicedItems(model.items);
                }
            }

            //remove visit id from empty items
            foreach (var item in document.Items.Where(v => v.VisitId == 0).ToList())
            {
                item.VisitId = null;
            }

            //fix for global discount
            if (document.Discount > 0 && model.ClientId > 0)
            {
                RapidVetUnitOfWork.InternalSettlementsRepository.AddSettlement(new InternalSettlement()
                {
                    ClientId = model.ClientId,
                    Date = DateTime.Now,
                    Description = Resources.Visit.GlobalDiscount,
                    Sum = Math.Round(((decimal)model.TotalBeforeVAT - (decimal)model.TotalAfterDiscount - (decimal)model.RoundingFactor) * document.VAT, 2)
                });
            }


            //payments

            //Cash
            if (model.CashPayment)
            {
                var payment = new FinanceDocumentPayment()
                    {
                        PaymentType = PaymentType.Cash,
                        Sum = model.CashPaymentSum,
                        IsNoMham = !model.NoMham
                    };


                AddPaymentToDocument(document, payment);
            }

            //Cheque

            if (model.ChequePayment)
            {
                foreach (var cheque in model.Cheques)
                {
                    var payment = new FinanceDocumentPayment()
                        {
                            PaymentType = PaymentType.Cheque,
                            Sum = cheque.Sum,
                            BankCodeId = cheque.BankCodeId,
                            BankAccount = cheque.Account,
                            DueDate = cheque.ChequeDate,
                            BankBranch = cheque.Branch,
                            ChequeNumber = cheque.ChequeNumber,
                            IsNoMham = !model.NoMham
                        };
                    if (cheque.ChequeDate >= monthEnd && recipt != null)
                    {
                        recipt.TotalChequesPayment += payment.Sum;
                        document.TotalChequesPayment -= payment.Sum;
                        AddPaymentToDocument(recipt, payment);
                    }
                    else
                    {
                        AddPaymentToDocument(document, payment);
                    }
                }
            }

            //Tranfer
            if (model.BankTransfer)
            {
                document.BankTransferSum = model.BankTransferTotal;
                foreach (var tranfer in model.BankTransfers)
                {
                    var payment = new FinanceDocumentPayment()
                    {
                        PaymentType = PaymentType.Transfer,
                        Sum = tranfer.BankTransferSum,
                        BankCodeId = tranfer.TransferBankCodeId,
                        BankAccount = tranfer.TransferAccount,
                        DueDate = tranfer.TransferDate,
                        BankBranch = tranfer.TransferBranch,
                        BankTransferNumber = tranfer.BankTransferNumber,
                        WitholdingTax = tranfer.WitholdingTax,
                        IsNoMham = !model.NoMham
                    };
                    AddPaymentToDocument(document, payment);
                }

            }
            
            //Credit
            if (model.CreditPayment)
            {

                DateTime dueDate;
                //if (DateTime.Now.Day < 15)
                //{
                //    dueDate = new DateTime(DateTime.Now.AddMonths(1).Year, DateTime.Now.AddMonths(1).Month, 2);
                //}
                //else
                //{
                //    dueDate = new DateTime(DateTime.Now.AddMonths(1).Year, DateTime.Now.AddMonths(1).Month, 15);
                //}
                dueDate = new DateTime(DateTime.Now.AddMonths(1).Year, DateTime.Now.AddMonths(1).Month, 1);

                var payment = new FinanceDocumentPayment()
                    {
                        PaymentType = PaymentType.CreditCard,
                        CreditCardCodeId = model.CreditCardCodeId,
                        CreditOkNumber = model.CreditOkNumber,
                        CreditDealNumber = model.CreditDealNumber,
                        CreditValidDate = model.CreditValidDate,
                        IssuerCreditTypeId = model.CreditPaymentType,
                        IsEasyCard = model.EasyCard,
                        Sum = model.FirstCreditPaymentSum.Value,
                        DueDate = dueDate,
                        ClinicId = model.ClientId,
                        CardNumberLastFour = model.CardNumberLastFour,
                        EasyCardDealId = model.EasyCardDealId,
                        EasyCardTerminalId = model.EasyCardTerminalId,
                        IsNoMham = !model.NoMham
                    };

                if (isCreditdCardProspne && document.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt && recipt != null)
                {
                    //before dan's changes:
                    AddPaymentToDocument(recipt, payment);
                    recipt.TotalCreditPaymentSum += payment.Sum;
                    document.TotalCreditPaymentSum -= payment.Sum;
                    //with dan:
                    //AddPaymentToDocument(document, payment);
                    //// recipt.TotalCreditPaymentSum += payment.Sum;
                    //document.TotalCreditPaymentSum = payment.Sum;
                }
                else
                {
                    AddPaymentToDocument(document, payment);
                }
                if (model.NumOfCreditPayments > 1)
                {
                    for (int i = 1; i < model.NumOfCreditPayments; i++)
                    {
                        var extraPayment = new FinanceDocumentPayment()
                          {
                              PaymentType = PaymentType.CreditCard,
                              CreditCardCodeId = model.CreditCardCodeId,
                              CreditOkNumber = model.CreditOkNumber,
                              CreditDealNumber = model.CreditDealNumber,
                              CreditValidDate = model.CreditValidDate,
                              IssuerCreditTypeId = model.CreditPaymentType,
                              IsEasyCard = model.EasyCard,
                              Sum = model.FurtherCreditPaymentsSum.Value,
                              DueDate = dueDate.AddMonths(i),
                              ClinicId = model.ClientId,
                              CardNumberLastFour = model.CardNumberLastFour,
                              EasyCardDealId = model.EasyCardDealId,
                              EasyCardTerminalId = model.EasyCardTerminalId,
                              IsNoMham = !model.NoMham
                          };

                        if (isCreditdCardProspne && document.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt && recipt != null)
                        {
                            //before dan's changes
                            AddPaymentToDocument(recipt, extraPayment);
                            recipt.TotalCreditPaymentSum += extraPayment.Sum;
                            document.TotalCreditPaymentSum -= extraPayment.Sum;
                            //dan's changes
                            //AddPaymentToDocument(recipt, extraPayment);
                            //recipt.TotalCreditPaymentSum += extraPayment.Sum;
                            ////document.TotalCreditPaymentSum -= extraPayment.Sum;
                        }
                        else
                        {
                            AddPaymentToDocument(document, extraPayment);
                        }
                    }

                }

            }

            OperationStatus result;

            //remove document with zero sum
            if (document.TotalSum == 0)
            {
                //detach finance document item from invoice recipt
                foreach (var item in document.Items)
                {
                    item.FinanceDocument = null;
                    item.FinanceDocumentId = 0;
                }

                List<FinanceDocument> invoiceReciptsToRemove = new List<FinanceDocument>();

                if (recipt.Client != null)
                {
                    foreach (var doc in recipt.Client.FinanceDocuments)
                    {
                        if (doc.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt)
                        {
                            invoiceReciptsToRemove.Add(doc);
                        }
                    }

                    foreach (var doc in invoiceReciptsToRemove)
                    {
                        recipt.Client.FinanceDocuments.Remove(doc);
                    }
                }
                RapidVetUnitOfWork.FinanceDocumentReposetory.MarkDocumentAsDeleted(document);

                if (newVisit != null)
                {
                    newVisit.Recipt = recipt;
                    // RapidVetUnitOfWork.VisitRepository.UpdateVisit(newVisit);
                }

                document = null;

                result = RapidVetUnitOfWork.FinanceDocumentReposetory.AddDocuments(recipt);
            }
            else
            {
                result = RapidVetUnitOfWork.FinanceDocumentReposetory.AddDocuments(document, recipt);
            }

            if (result.Success)
            {
                if (document != null || recipt != null)
                {
                    int clientId = 0;
                    if (document != null && document.ClientId.HasValue)
                    {
                        clientId = document.ClientId.Value;
                    }
                    else if (recipt != null && recipt.ClientId.HasValue)
                    {
                        clientId = recipt.ClientId.Value;
                    }

                    if (clientId != 0)
                    {
                        strclientId = clientId.ToString();
                        var client = RapidVetUnitOfWork.ClientRepository.GetClient(clientId);
                        RapidVetUnitOfWork.ClientRepository.UpdateClientBalance(client);
                        RapidVetUnitOfWork.Save();
                    }
                }

                string url = "";

                FinanceDocumentType docType = document != null ? document.FinanceDocumentType : recipt.FinanceDocumentType;

                if (CurrentUser.ActiveClinic.ApplyDividingIncomeModel && (docType == FinanceDocumentType.Invoice || docType == FinanceDocumentType.InvoiceReceipt || docType == FinanceDocumentType.Receipt || docType == FinanceDocumentType.Refound))
                {

                    if (document != null && recipt != null)
                    {
                        if (CurrentUser.ActiveClinic.CancelAutomaticPrinting)
                        {
                            url = Url.Action("Divisions", "FinanceDocumentDivision", new { documentId = document.Id + "," + recipt.Id });
                        }
                        else
                        {
                            url = Url.Action("Divisions", "FinanceDocumentDivision", new { printFinanceDocument = document.Id + "," + recipt.Id, documentId = document.Id + "," + recipt.Id });
                        }
                    }
                    else if (document != null)
                    {
                        if (CurrentUser.ActiveClinic.CancelAutomaticPrinting)
                        {
                            url = Url.Action("Divisions", "FinanceDocumentDivision", new { documentId = document.Id });
                        }
                        else
                        {
                            url = Url.Action("Divisions", "FinanceDocumentDivision", new { printFinanceDocument = document.Id, documentId = document.Id });
                        }
                    }
                    else if (recipt != null)
                    {
                        if (CurrentUser.ActiveClinic.CancelAutomaticPrinting)
                        {
                            url = Url.Action("Divisions", "FinanceDocumentDivision", new { documentId = recipt.Id });
                        }
                        else
                        {
                            url = Url.Action("Divisions", "FinanceDocumentDivision", new { printFinanceDocument = recipt.Id, documentId = recipt.Id });
                        }
                    }
                }
                else
                {
                    if (CurrentUser.ActiveClinic.CancelAutomaticPrinting)
                    {
                        url = Url.Action("Index", "Home");
                    }
                    else
                    {
                        if (document != null && recipt != null)
                        {
                            url = Url.Action("Index", "Home", new { printFinanceDocument = document.Id + "," + recipt.Id });
                        }
                        else if (document != null)
                        {
                            url = Url.Action("Index", "Home", new { printFinanceDocument = document.Id });
                        }
                        else if (recipt != null)
                        {
                            url = Url.Action("Index", "Home", new { printFinanceDocument = recipt.Id });
                        }
                    }
                }
                string redirectToClient = "";
                if (strclientId != "")
                    redirectToClient= "../Clients/Patients/" + strclientId.Trim();
                return Json(data: new { Success = true, redirect = url, redirectToClient = redirectToClient });
            }
            else
            {
                return Json(data: new { Success = false });
            }
        }

        private Visit saveVisitDetails(FinanceDocumentFromJson model)
        {
            Visit newVisit = null;
            if (model.ClientId > 0)
            {
                var client = RapidVetUnitOfWork.ClientRepository.GetClient(model.ClientId);

                //Check that clinet is in clinic
                if (client.ClinicId != CurrentUser.ActiveClinicId)
                {
                    throw new SecurityException();
                }

                //rfund items
                if (model.DocumentTypeId == (int)FinanceDocumentType.Refound)
                {
                }

                //add items to  visit
                if (model.items.Count(i => i.UpdateItemInVisit && i.VisitId == 0) > 0 && model.DocumentTypeId != (int)FinanceDocumentType.Refound && model.TotalToPay > 0)
                {

                    newVisit = new Visit()
                    {
                        CreatedById = CurrentUser.Id,
                        DoctorId = model.DoctorId,
                        CreatedDate = DateTime.Now,
                        VisitDate = DateTime.Now,
                        Price = 0,
                        ClientIdAtTimeOfVisit = model.ClientId,
                        Active = true
                    };

                    foreach (var item in model.items.Where(i => i.UpdateItemInVisit && i.VisitId == 0))// && i.PriceListItemId.HasValue))
                    {
                        if (item.PriceListItemId != null && item.PriceListItemId != 0)
                        {
                            var priceListItem = RapidVetUnitOfWork.PriceListRepository.GetPriceListItem(item.PriceListItemId.Value);
                            var treatment = new VisitTreatment()
                            {
                                Name = priceListItem.Name,
                                Quantity = item.Quantity,
                                UnitPrice = item.UnitPrice,
                                PriceListItem = priceListItem,
                                Discount = Math.Round((decimal)item.PriceWithVAT * item.Quantity - (decimal)item.TotalBeforeVAT * (decimal)model.VAT, 2),//item.Discount 
                                Price = Math.Round(item.UnitPrice, 2), //item.TotalBeforeVAT,
                                CategoryName = priceListItem.Category.Name,
                                Visit = newVisit,
                                Invoiced = true
                            };
                            newVisit.Price += item.Quantity * item.PriceWithVAT;
                            newVisit.Treatments.Add(treatment);

                            if(treatment.Discount > 0)
                            {                                
                                RapidVetUnitOfWork.InternalSettlementsRepository.AddSettlement(new InternalSettlement()
                                {
                                    ClientId = model.ClientId,
                                    Date = DateTime.Now,
                                    Description = Resources.Visit.ItemDiscount,
                                    Sum = treatment.Discount
                                });
                            }
                        }
                        else if (item.Description.Equals(Resources.Visit.OpenBalance))
                        {
                            var debtVisit = new Visit()
                            {
                                CreatedById = CurrentUser.Id,
                                DoctorId = model.DoctorId,
                                CreatedDate = DateTime.Now,
                                VisitDate = DateTime.Now,
                                Price = 0,
                                ClientIdAtTimeOfVisit = model.ClientId,
                                Active = true,
                                DebtVisit = true
                            };

                            var debtTreatment = new VisitTreatment()
                            {
                                Name = Resources.Visit.OpenBalance,
                                Quantity = 1,
                                UnitPrice = item.PriceWithVAT * (-1),
                                PriceListItem = null,
                                Discount = 0,
                                Price = item.PriceWithVAT * (-1),
                                CategoryName = "",
                                Visit = debtVisit
                            };
                            debtVisit.Price += item.Quantity * item.PriceWithVAT * (-1);
                            debtVisit.Treatments.Add(debtTreatment);
                            RapidVetUnitOfWork.VisitRepository.AddNewVisit(debtVisit);
                        }
                        else
                        {
                            var treatment = new VisitTreatment()
                            {
                                Name = item.Description,
                                Quantity = item.Quantity,
                                UnitPrice = item.UnitPrice,
                                PriceListItem = null,
                                Discount = Math.Round((decimal)item.PriceWithVAT * item.Quantity - (decimal)item.TotalBeforeVAT * (decimal)model.VAT, 2),// item.Discount,
                                Price = Math.Round(item.UnitPrice, 2), //item.TotalBeforeVAT,
                                CategoryName = item.CatalogNumber,
                                Visit = newVisit,
                                Invoiced = true
                            };
                            newVisit.Price += item.Quantity * item.PriceWithVAT;
                            newVisit.Treatments.Add(treatment);

                            if (treatment.Discount > 0)
                            {
                                RapidVetUnitOfWork.InternalSettlementsRepository.AddSettlement(new InternalSettlement()
                                {
                                    ClientId = model.ClientId,
                                    Date = DateTime.Now,
                                    Description = Resources.Visit.ItemDiscount,
                                    Sum = treatment.Discount
                                });
                            }
                        }
                    }

                    if (newVisit.Price > 0)
                    {
                        RapidVetUnitOfWork.VisitRepository.AddNewVisit(newVisit);
                    }
                }

                if (model.items.Count(i => i.VisitId.HasValue && i.VisitId != 0) > 0 && model.DocumentTypeId != (int)FinanceDocumentType.Refound && model.DocumentTypeId != (int)FinanceDocumentType.Proforma)
                {
                    RapidVetUnitOfWork.FinanceDocumentReposetory.SetInvoicedItems(model.items);
                }
            }

            return newVisit;
        }

        private void AddPaymentToDocument(FinanceDocument document, FinanceDocumentPayment payment)
        {
            if (document.FinanceDocumentType == FinanceDocumentType.Refound)
            {
                if (payment.Sum > 0)
                {
                    payment.Sum = payment.Sum * -1;
                }
                payment.Refound = document;
            }
            else if (document.FinanceDocumentType == FinanceDocumentType.Receipt)
            {
                payment.Recipt = document;
            }
            else if (document.FinanceDocumentType == FinanceDocumentType.Invoice)
            {
                payment.Invoice = document;
            }
            else if (document.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt)
            {
                payment.Recipt = document;
                payment.Invoice = document;
            }

            payment.IssuerId = document.IssuerId;
            payment.ClinicId = CurrentUser.ActiveClinicId;
            payments.Add(payment);
            RapidVetUnitOfWork.FinanceDocumentReposetory.AddPayment(payment);
        }

        public ActionResult RedirectToLastClient()
        {
            var lastClient = RapidVetUnitOfWork.LastClientRepository.GetLastClients(CurrentUser.Id,
                                                                                CurrentUser.ActiveClinicId, 1).FirstOrDefault();
            if (lastClient != null)
            {
                return RedirectToAction("Patients", "Clients", new { id = lastClient.ClientId });
            }
            return RedirectToAction("Index", "Clients");
        }

        public ActionResult GetDigitalSignedDocument(int id)
        {
            if (!string.IsNullOrEmpty(CurrentUser.ActiveClinic.CoSignUserName) && !string.IsNullOrEmpty(CurrentUser.ActiveClinic.CoSignPassword))
            {
                // Model Setup
                var model = RapidVetUnitOfWork.FinanceDocumentReposetory.GetItem(id);
                if (model.ClinicId != CurrentUser.ActiveClinicId)
                {
                    throw new SecurityException();
                }

                if (string.IsNullOrEmpty(model.SignatureKey))
                {
                    ViewBag.OriginalString = "העתק";
                    if (!model.OrginalPrintedDate.HasValue)
                    {
                        model.OrginalPrintedDate = DateTime.Now;
                        RapidVetUnitOfWork.Save();
                        ViewBag.OriginalString = "מקור";
                    }
                    else
                    {
                        ViewBag.OriginalDateString = model.OrginalPrintedDate.Value.ToShortDateString() + " " +
                                                     model.OrginalPrintedDate.Value.ToShortTimeString();
                    }

                    ViewBag.PrinterClass = "stdPrint";  //CurrentUser.ThermalPrinter == true ? "termalPrint" : "stdPrint";

                    if (model.ClientId == null && String.IsNullOrEmpty(model.ClientName))
                    {
                        model.ClientName = "לקוח מזדמן";
                    }

                    // Signature page setup
                    ViewBag.IsSignature = true;
                    var htmlToString = RenderPartialViewToString("Print", model);
                    var guid = RapidVet.Helpers.CoSinge.Singe(htmlToString, CurrentUser.ActiveClinic.CoSignUserName,
                                                              CurrentUser.ActiveClinic.CoSignPassword);

                    // Redirect to page
                    if (!string.IsNullOrEmpty(guid))
                    {
                        model.SignatureKey = guid;
                        RapidVetUnitOfWork.Save();
                        return Redirect("/SingedDocs/" + model.SignatureKey);
                    }
                    return Content(RapidVet.Resources.Exceptions.MustAddCoSign);
                }
                // Redirect to page
                return Redirect("/SingedDocs/" + model.SignatureKey);
            }
            return Content(RapidVet.Resources.Exceptions.MustAddCoSign);
        }
    }
}
