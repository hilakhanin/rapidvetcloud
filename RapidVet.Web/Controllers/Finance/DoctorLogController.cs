﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using RapidVet.Enums;
using RapidVet.Enums.Finances;
using RapidVet.Model.Finance;
using RapidVet.Model.FullCalender;
using RapidVet.Model.Visits;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Finance.Reports;
using RapidVet.WebModels.Visits;
using RapidVet.Web.Attributes;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using System.Text;
using RapidVet.Web.Controllers.Finance;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class DoctorLogController : BaseController
    {
        public ActionResult Index(string date, string issuerId, bool toPrint = false, string toDate = null)
        {
            DateTime day;
            if (string.IsNullOrWhiteSpace(date))
            {
                day = DateTime.Now;
            }
            else
            {
                day = DateTime.MinValue;
                var format = "dd/MM/yyyy";
                var unparsed = date;
                DateTime.TryParseExact(unparsed, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out day);
                if (day == DateTime.MinValue)
                {
                    day = DateTime.Now;
                }
            }

            if (string.IsNullOrWhiteSpace(issuerId))
            {
                issuerId = RapidVetUnitOfWork.IssuerRepository.GetDropdownList(CurrentUser.ActiveClinicId, CurrentUser.Id, false).Select(x => x.Value).FirstOrDefault() ?? "-1";
            }

            if (toPrint)
            {
                if (!string.IsNullOrWhiteSpace(toDate))
                {
                    var toDay = DateTime.MinValue;
                    var format = "dd/MM/yyyy";
                    var unparsed = toDate;
                    DateTime.TryParseExact(unparsed, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out toDay);
                    if (toDay == DateTime.MinValue)
                    {
                        toDay = DateTime.Now;
                    }
                    ViewBag.PrintDateRange = true;
                    ViewBag.FromDate = day;
                    ViewBag.ToDate = toDay;
                }
                else
                {
                    ViewBag.PrintDateRange = false;
                }
            }


            ViewBag.DateString = day.ToString("dd/MM/yyyy");
            ViewBag.ToPrint = toPrint;
            return View();
        }

        public ActionResult DayLog(string date, string issuerId, bool toPrint = false)
        {
            DateTime day;
            if (string.IsNullOrWhiteSpace(date))
            {
                day = DateTime.Now;
            }
            else
            {
                day = DateTime.MinValue;
                var format = "dd/MM/yyyy";
                var unparsed = date;
                DateTime.TryParseExact(unparsed, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out day);
                if (day == DateTime.MinValue)
                {
                    day = DateTime.Now;
                }
            }
            if (string.IsNullOrWhiteSpace(issuerId))
            {
                issuerId = RapidVetUnitOfWork.IssuerRepository.GetDropdownList(CurrentUser.ActiveClinicId, CurrentUser.Id, false).Select(x => x.Value).FirstOrDefault();
            }

            issuerId = issuerId == null || issuerId.ToLower().Equals("null") ? "-1" : issuerId;//in order to prevent null reference on int parser
            int issuerRelatedDoctors;
            var visits = RapidVetUnitOfWork.VisitRepository.GetVisitsOfDay(day, CurrentUser.ActiveClinicId, int.Parse(issuerId), out issuerRelatedDoctors).ToList();       

            var payments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetDayPayments(day, CurrentUser.ActiveClinicId, int.Parse(issuerId)).ToList();          

            var cash =
                payments.Where(p => p.PaymentTypeId == (int)PaymentType.Cash).Sum(p => p.Sum);

            var transfer =
               payments.Where(p => p.PaymentTypeId == (int)PaymentType.Transfer).Sum(p => p.Sum);

            var cheques =
                payments.Where(p => p.PaymentTypeId == (int)PaymentType.Cheque && p.Created >= day && p.Created < day.AddDays(1)).Sum(p => p.Sum);

            var chequesDueToday =
               payments.Where(p => p.PaymentTypeId == (int)PaymentType.Cheque && p.DueDate >= day && p.DueDate < day.AddDays(1) && !(p.Created >= day && p.Created < day.AddDays(1))).Sum(p => p.Sum);


            var chequeNumber = payments.Count(p => p.PaymentTypeId == (int)PaymentType.Cheque && p.Created >= day && p.Created < day.AddDays(1));
            var credit =
                payments.Where(p => p.PaymentTypeId == (int)PaymentType.CreditCard).Sum(p => p.Sum);

            var documents =
                payments.Where(p => p.InvoiceId != null && p.Created >= day && p.Created < day.AddDays(1)).Sum(p => p.Sum);

            var total = payments.Where(p => p.Created >= day && p.Created < day.AddDays(1)).Sum(p => p.Sum);
            var visitsModel = new List<VisitFinanceModel>();
            var paymentClientIds = payments.Where(p=>p.Recipt != null).Select(p=>p.Recipt.ClientId).ToList();
            paymentClientIds.AddRange(payments.Where(p=>p.Invoice != null).Select(p=>p.Invoice.ClientId).ToList());
            paymentClientIds.AddRange(payments.Where(p=>p.Refound != null).Select(p=>p.Refound.ClientId).ToList());
            paymentClientIds = paymentClientIds.Distinct().ToList();
                   
            //add payments with recipts
            //foreach (var vm in visits.FindAll(x => x.Recipts != null && x.Recipts.Count > 1))
            foreach (var vm in visits.FindAll(x => x.Recipts != null && x.Recipts.Count > 0))
            {
                ////foreach (var i in vm.Recipts.Where(x => x.SerialNumber != vm.Recipt.SerialNumber && !visitsModel.Any(y => y.ClientId == vm.Recipt.ClientId && y.SerialNumber == x.SerialNumber)))
                foreach (var i in vm.Recipts)
                {
                    if (!visitsModel.Any(c => c.ClientId == vm.Recipt.ClientId && c.SerialNumber == i.SerialNumber))
                    {
                        visitsModel.Add(new VisitFinanceModel()
                        {
                            VisitId = vm.Id,
                            Price = i.TotalSum,
                            SerialNumber = i.SerialNumber,
                            ClientId = vm.Recipt.ClientId.Value,
                            ClientName = vm.Recipt.ClientName,
                            DoctorName = RapidVetUnitOfWork.IssuerRepository.GetItem(i.IssuerId).Name,
                            FinanceDocumentId = vm.Recipt.Id,
                            IsAddedByCalender = false,
                            MainComplaint = vm.MainComplaint,
                            VisitDate = day.ToShortDateString()
                        });
                    }
                }
            }    

            //add clients without visits
            var paymentsWithoutVisits = payments.Where(p => (p.PaymentTypeId == (int)PaymentType.Cash || p.PaymentTypeId == (int)PaymentType.Transfer ||
                                        (p.PaymentTypeId == (int)PaymentType.Cheque && p.Created >= day && p.Created < day.AddDays(1)) ||
                                        (p.PaymentTypeId == (int)PaymentType.CreditCard && p.Recipt != null && p.Recipt.ClientId.HasValue) &&
                                        !visitsModel.Any(v => v.ClientId == p.Recipt.ClientId.Value))).Distinct();

            //var paymentsWithoutVisits = payments.Where(p => (p.PaymentTypeId == (int)PaymentType.Cash || p.PaymentTypeId == (int)PaymentType.Transfer ||
            //                            (p.PaymentTypeId == (int)PaymentType.Cheque && p.Created >= day && p.Created < day.AddDays(1)) ||
            //                            p.PaymentTypeId == (int)PaymentType.CreditCard) && p.Recipt != null && p.Recipt.ClientId.HasValue &&
            //                            !visitsModel.Any(v => v.ClientId == p.Recipt.ClientId.Value)).Distinct();

            //add payments without visits
            foreach (var payment in paymentsWithoutVisits)
            {
                var item = payment.Recipt.Items.FirstOrDefault();
                                
                if (!visitsModel.Any(c => c.ClientId == payment.Recipt.ClientId && payment.Recipt.SerialNumber == c.SerialNumber))
                {
                    visitsModel.Add(new VisitFinanceModel()
                    {
                        VisitId = null,
                        Price = payment.Recipt.TotalSum,
                        SerialNumber = payment.Recipt.SerialNumber,
                        ClientId = payment.Recipt.ClientId == null ? 0 : payment.Recipt.ClientId.Value,
                        ClientName = payment.Recipt.ClientName,
                        DoctorName = RapidVetUnitOfWork.IssuerRepository.GetItem(int.Parse(issuerId)).Name,
                        FinanceDocumentId = payment.Recipt.Id,
                        IsAddedByCalender = false,
                        MainComplaint = "",//visit != null ? visit.MainComplaint : "",
                        VisitDate = day.ToShortDateString()//visit != null ? visit.VisitDate.ToShortDateString() : day.ToShortDateString()
                    });
                }
            }            

            //clients who are listed as shown in calender but not in visit
            var calenderEntriesWithShownClients = RapidVetUnitOfWork.CalenderRepository.GetShownClientsOnDayCalenderEntries(CurrentUser.ActiveClinicId, day);
            foreach (var calenderEntriesWithShownClient in calenderEntriesWithShownClients)
            {
                if (visitsModel.All(v => v.ClientId != calenderEntriesWithShownClient.ClientId))
                {
                    visitsModel.Add(AutoMapper.Mapper.Map<CalenderEntry, VisitFinanceModel>(calenderEntriesWithShownClient));
                }
            }

            //add visits without recipts
            foreach (var vm in visits.FindAll(x => x.Recipt == null))
            {
                if (!visitsModel.Any(c => c.ClientId == vm.ClientIdAtTimeOfVisit))
                {
                    visitsModel.Add(new VisitFinanceModel()
                    {
                        VisitId = vm.Id,
                        Price = null,
                        SerialNumber = 0,
                        ClientId = vm.ClientIdAtTimeOfVisit,
                        ClientName = vm.ClientAtTimeOfVisit.Name,
                        DoctorName = vm.Doctor.Name,
                        FinanceDocumentId = null,
                        IsAddedByCalender = false,
                        MainComplaint = vm.MainComplaint,
                        VisitDate = vm.VisitDate.ToShortDateString()
                    });
                }
            }

            visitsModel = visitsModel.OrderBy(v => v.VisitId ?? 10000000000000).ThenBy(f => f.SerialNumber).ToList();

            var checksNotFromToday = payments.Where(p => p.PaymentTypeId == (int)PaymentType.Cheque && (p.DueDate < day || p.DueDate > day.AddDays(1))).Sum(p => p.Sum);
            var creditPaymentsNotFromToday = payments.Where(p => p.PaymentTypeId == (int)PaymentType.CreditCard && (p.DueDate < day || p.DueDate > day.AddDays(1))).Sum(p => p.Sum);
            var model = new DoctorLogModel()
            {
                TotalCash = cash,
                TotalChecks = cheques,
                TotalCreditCard = credit,
                TotalChecksNumberToday = chequeNumber,
                TotalDocuments = documents,
                TotalTransfers = transfer,
                Day = day,
                Total = total,
                Visits = visitsModel,
                TotalToCashToday = total - checksNotFromToday - creditPaymentsNotFromToday + chequesDueToday,
                Checks = new List<CheckPaymentWebModel>(),
                IssuerRelatedDoctors = issuerRelatedDoctors
            };

            var checks = RapidVetUnitOfWork.FinanceDocumentReposetory.GetChecksWithDueDate(day, CurrentUser.ActiveClinicId, int.Parse(issuerId));
            model.Checks = AutoMapper.Mapper.Map<List<FinanceDocumentPayment>, List<CheckPaymentWebModel>>(checks.ToList());

            foreach (var cheque in model.Checks)
            {
                if (string.IsNullOrWhiteSpace(cheque.ClientName))
                {
                    cheque.ClientName = Resources.Migration.TempClient;
                }
            }

            ViewBag.ToPrint = toPrint;
            return View(model);
        }

        public ActionResult IncomingsReport()
        {
            return View();
        }

        public JsonResult GetIncomingsReportData(string fromTime, string toTime, string fromDate, string toDate, int issuerId)
        {
            var model = getIncomingsReportData(fromTime, toTime, fromDate, toDate, issuerId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private List<IncomingsReportWebModel> getIncomingsReportData(string fromTime, string toTime, string fromDate, string toDate, int issuerId)
        {
            var now = DateTime.Now.ToShortDateString();
            var filterFromDate = DateTime.MinValue;
            if (!string.IsNullOrWhiteSpace(fromTime))
            {
                DateTime.TryParse(string.Format("{0} {1}", fromDate, fromTime), out filterFromDate);
            }
            if (filterFromDate == DateTime.MinValue)
            {
                filterFromDate = DateTime.Now.Date;
            }

            var filterToDate = DateTime.MinValue;
            if (!string.IsNullOrWhiteSpace(toTime))
            {
                DateTime.TryParse(string.Format("{0} {1}", toDate, toTime), out filterToDate);
            }
            if (filterToDate == DateTime.MinValue)
            {
                filterToDate = DateTime.Now.AddDays(1).Date;
            }

            var financeDocuments =
                RapidVetUnitOfWork.FinanceReportRepository.IncommingsReportData(CurrentUser.ActiveClinicId, filterFromDate, filterToDate, issuerId);
            var model = new List<IncomingsReportWebModel>();

            foreach (var document in financeDocuments)
            {
                var modelItem = AutoMapper.Mapper.Map<FinanceDocument, IncomingsReportWebModel>(document);

                modelItem.ClientName = string.IsNullOrWhiteSpace(modelItem.ClientName) ? Resources.Migration.TempClient : modelItem.ClientName;

                modelItem.Payments = document.Payments == null ? 0 : document.Payments.Count(p => p.PaymentTypeId == (int)PaymentType.CreditCard);          
                modelItem.Payments += document.Payments == null ? 0 : document.Payments.Count(p => p.PaymentTypeId == (int)PaymentType.Cheque);             

                model.Add(modelItem);
            }
            return model;
        }

        [ExcelExport]
        public FileResult ExportToExcel(string fromTime, string toTime, string fromDate, string toDate, int issuerId)
        {
            var data = getIncomingsReportData(fromTime, toTime, fromDate, toDate, issuerId);
            var csvData = AutoMapper.Mapper.Map<List<IncomingsReportCsvWebModel>>(data);
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            var csvConfig = new CsvConfiguration { Encoding = Encoding.UTF8, HasHeaderRecord = false };
            var csv = new CsvWriter(writer, csvConfig);

            csv.WriteField("תאריך ושעה");
            csv.WriteField("שם הלקוח");
            csv.WriteField("מספר מסמך");
            csv.WriteField("סוג מסמך");
            csv.WriteField("סה\"כ במזומן");
            csv.WriteField("סה\"כ בהעברה בנקאית");
            csv.WriteField("סה\"כ באשראי");
            csv.WriteField("סה\"כ בהמחאות");
            csv.WriteField("מספר תשלומים");

            csv.NextRecord();
            csv.WriteRecords(csvData);
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", "incomeReport.csv");
        }
    }
}