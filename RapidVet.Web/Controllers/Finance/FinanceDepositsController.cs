﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using RapidVet.Enums.Finances;
using RapidVet.Model.Finance;
using RapidVet.WebModels.Finance;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class FinanceDepositsController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Deposits(int id)
        {
            var deposits = RapidVetUnitOfWork.FinanceDocumentReposetory.GetAllPaymentsOfDeposit(CurrentUser.ActiveClinicId, id).ToList();
            var deposit = RapidVetUnitOfWork.DepositsRepository.GetDeposit(id);
            if (deposit != null)
            {
                ViewBag.DepositDate = deposit.Date;
            }
      
            return View(deposits);
        }

        public ActionResult PrintDeposits(int id)
        {
            var deposits = RapidVetUnitOfWork.FinanceDocumentReposetory.GetAllPaymentsOfDeposit(CurrentUser.ActiveClinicId, id).ToList();
            var deposit = RapidVetUnitOfWork.DepositsRepository.GetDeposit(id);
            if (deposit != null)
            {
                ViewBag.DepositDate = deposit.Date;
             
            }
            if (deposits.Any())
            {
                var first = deposits.First();
                ViewBag.Issuer = first.Recipt.Issuer;
            }
            return View(deposits);
        }

        [HttpGet]
        public JsonResult GetMetaData()
        {
            var issuers = RapidVetUnitOfWork.IssuerRepository.GetDropdownList(CurrentUser.ActiveClinicId, CurrentUser.IsAdministrator ? -1 : CurrentUser.Id);
      
            var paymentTypes = from PaymentType m in Enum.GetValues(typeof(PaymentType))
                               select new SelectListItem()
                               {
                                   Text = RapidVet.Resources.Enums.Finances.PaymentType.ResourceManager.GetString(m.ToString()),
                                   Value = ((int)m).ToString()
                               };
            var documentTypes = from FinanceDocumentType m in Enum.GetValues(typeof(FinanceDocumentType))
                                where m == FinanceDocumentType.Receipt || m == FinanceDocumentType.InvoiceReceipt
                                select new SelectListItem()
                                {
                                    Text = RapidVet.Resources.Enums.Finances.FinanceDocumentType.ResourceManager.GetString(m.ToString()),
                                    Value = ((int)m).ToString()
                                };
            var depositOptions = new List<SelectListItem>()
                {
                    new SelectListItem() {Text = "רשומות שטרם הופקדו" , Value = "false"},
                    new SelectListItem() {Text = "רשומות שהופקדו", Value = "true"},          
                };

            return new JsonResult()
            {
                Data = new { Issuers = issuers, PaymentTypes = paymentTypes, DocumentTypes = documentTypes, DepositOptions = depositOptions },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GetPayments(string fromDate, string toDate, int? issuerId, int? paymentTypeId, int? documentTypeId, bool? isDeposited)
        {
            var startDate = DateTime.Now.AddMonths(-6);
            var endDate = DateTime.Now;
            if (!string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
            {
                startDate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                endDate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
            }
            var payments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetPaymentsForDeposit(CurrentUser.ActiveClinicId, startDate, endDate, issuerId, paymentTypeId, documentTypeId, isDeposited);
            var paymentsJson = new List<FinancePaymentJson>();
            foreach (var payment in payments)
            {
                var newPaymentJson = AutoMapper.Mapper.Map<FinanceDocumentPayment, FinancePaymentJson>(payment);
                switch (payment.Recipt.FinanceDocumentTypeId)
                {
                    case (int)FinanceDocumentType.Invoice:
                        newPaymentJson.documentTypeName = "חשבונית מס";
                        break;
                    case (int)FinanceDocumentType.InvoiceReceipt:
                        newPaymentJson.documentTypeName = "חשבונית מס קבלה";
                        break;
                    case (int)FinanceDocumentType.Proforma:
                        newPaymentJson.documentTypeName = "חשבונית עסקה";
                        break;
                    case (int)FinanceDocumentType.Receipt:
                        newPaymentJson.documentTypeName = "קבלה";
                        break;
                    case (int)FinanceDocumentType.Refound:
                        newPaymentJson.documentTypeName = "זיכוי";
                        break;
                }

                if (payment.PaymentTypeId == (int)PaymentType.Cheque)
                {
                    newPaymentJson.bankName = payment.BankCode.Name;
                }
                if (payment.PaymentTypeId == (int)PaymentType.CreditCard)
                {
                    newPaymentJson.creditCardCompanyName = payment.CreditCardCode.Name;
                    if (payment.IssuerCreditType != null)
                        newPaymentJson.creditCardPaymentTypeName = payment.IssuerCreditType.Name;
                }

                if(string.IsNullOrWhiteSpace(payment.Recipt.ClientName))
                {
                    newPaymentJson.clientName = "לקוח מזדמן";
                }

                newPaymentJson.PaymentType = RapidVet.Resources.Enums.Finances.PaymentType.ResourceManager.GetString(newPaymentJson.PaymentType);

                paymentsJson.Add(newPaymentJson);
            }
            return new JsonResult
            {
                Data = paymentsJson,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult SavePayments(int issuerId, string paymentsString)
        {
            var jsonResult = new JsonResult() { Data = 0, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            var jss = new JavaScriptSerializer();
            var paymentsList = jss.Deserialize<List<FinancePaymentJson>>(paymentsString);
            if (paymentsList == null || !paymentsList.Any())
            {
                return jsonResult;
            }

            decimal sum = paymentsList.Select(p => RapidVetUnitOfWork.FinanceDocumentReposetory.GetFinancePayment(p.id)).Select(tmpPaymentDb => tmpPaymentDb.Sum).Sum();
            var depositPayment = new Deposit()
                       {
                           Date = DateTime.Now,
                           DepositAmountIncludingTax = sum
                       };
            RapidVetUnitOfWork.DepositsRepository.Create(depositPayment);
            var depositNumber = RapidVetUnitOfWork.FinanceDocumentReposetory.GetIssuerLastDepositNumber(issuerId) + 1;
            foreach (var payment in paymentsList)
            {
                var tmpPaymentDb = RapidVetUnitOfWork.FinanceDocumentReposetory.GetFinancePayment(payment.id);
                if (tmpPaymentDb == null)
                {
                    return jsonResult;
                }
                if (tmpPaymentDb.DepositId.HasValue)
                {                   
                    jsonResult.Data = -1;
                    return jsonResult;
                }

                tmpPaymentDb.DepositId = depositPayment.Id;
                tmpPaymentDb.DepositNumber = depositNumber;             

            }
            
            var status = RapidVetUnitOfWork.Save();
            jsonResult.Data = depositPayment.Id;
            return jsonResult;

            //var documentsId = new HashSet<int>();
            //foreach (var payment in paymentsList)
            //{
            //    documentsId.Add(payment.documentId);
            //}
            //foreach (var documentId in documentsId)
            //{
            //    var payments = paymentsList.Where(p => p.documentId == documentId).ToList();
            //    decimal sum = 0;
            //    foreach (var payment in paymentsList)
            //    {
            //        var tmpPaymentDb = RapidVetUnitOfWork.FinanceDocumentReposetory.GetFinancePayment(payment.id);
            //        if (tmpPaymentDb == null)
            //        {
            //            return jsonResult;
            //        }
            //        if (tmpPaymentDb.DepositId.HasValue)
            //        {
            //            return jsonResult;
            //        }
            //        sum += tmpPaymentDb.Sum;
            //    }
            //    var paymentDb = RapidVetUnitOfWork.FinanceDocumentReposetory.GetFinancePayment(payments.First().id);
            //    if (paymentDb.ReciptId != null)
            //    {
            //        var depositPayment = new Deposit()
            //            {
            //                ClientName = paymentDb.Recipt.ClientName,
            //                FinanceDocumentId = paymentDb.ReciptId.Value,
            //                FinanceDocumentTypeId = paymentDb.Recipt.FinanceDocumentTypeId,
            //                Date = DateTime.Now,
            //                DepositAmountIncludingTax = sum
            //            };
            //        RapidVetUnitOfWork.DepositsRepository.Create(depositPayment);
            //        foreach (var payment in paymentsList)
            //        {
            //            var tmpPaymentDb = RapidVetUnitOfWork.FinanceDocumentReposetory.GetFinancePayment(payment.id);
            //            tmpPaymentDb.DepositId = depositPayment.Id;
            //        }
            //    }
            //}

        }
    }
}