﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Ionic.Zip;
using RapidVet.Enums.Finances;
using RapidVet.Helpers;
using RapidVet.Model.Finance;

namespace RapidVet.Web.Controllers.Finance
{
    public class OpenFormatController : BaseController
    {
        //
        // GET: /TaxUnifyReports/

        //סימולטור לבדיקת הקובץ
        //https://www.misim.gov.il/tmbakmmsml/

        public ActionResult Index()
        {
            return View();
        }

        //string :from,to int:issuerId
        public ActionResult GetData(string from, string to, int issuerId)
        {

            var proccessStartTime = DateTime.Now;
            //Create Temp open fomat folder
            //if (!System.IO.Directory.Exists(Server.MapPath("\tempOpenFormat")))
            //{
            //    System.IO.Directory.CreateDirectory(Server.MapPath("\tempOpenFormat"));
            //}

            var docNumber = 0;
            var itemNumber = 0;
            var paymentNumber = 0;
            var startDate = DateTime.ParseExact(from, "dd/MM/yyyy", null);
            var endDate = DateTime.ParseExact(to, "dd/MM/yyyy", null);
            string mainId = OpenFormatUtils.GetRandomeCode();
            string tempFolder = @"\tempOpenFormat\" + mainId;
            tempFolder = Server.MapPath(tempFolder);
            System.IO.Directory.CreateDirectory(tempFolder);
            var bkmvDataFilePath = System.IO.Path.Combine(tempFolder, "BKMVDATA.TXT");
            var iniFilePath = System.IO.Path.Combine(tempFolder, "INI.TXT");
            var zipbkmvDataFilePath = System.IO.Path.Combine(tempFolder, "BKMVDATA.ZIP");
            var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(issuerId);

            if (issuer.CompanyId == null || issuer.CompanyId.Length != 9)
            {
                return View("InvalidIssuerId");
            }


            if (issuer.TaxDedationFileNumber == null || issuer.TaxDedationFileNumber.Length != 9)
            {
                return View("InvalidDedactionId");
            }

            var documents =
                RapidVetUnitOfWork.FinanceDocumentReposetory.GetIssuerDocumentsAndPaymentsForOpenFormat(issuerId,
                                                                                                        startDate,
                                                                                                        endDate);
            var recordNumber = 1;
            using (var file = new System.IO.StreamWriter(bkmvDataFilePath, false, Encoding.GetEncoding("ISO-8859-8-i")))
            {

                //Open record
                var openRecordBuilder = new StringBuilder();
                openRecordBuilder.Append("A100");
                openRecordBuilder.Append(OpenFormatUtils.GetIntString(recordNumber, 9));
                openRecordBuilder.Append(issuer.CompanyId);
                openRecordBuilder.Append(mainId);
                openRecordBuilder.Append(OpenFormatUtils.SystemConst);
                file.WriteLine(openRecordBuilder.ToString());
                //documents record
                foreach (var doc in documents.Where(d => d.SerialNumber > 0))
                {
                    if (doc.SerialNumber == 74)
                    {

                    }
                    docNumber++;
                    recordNumber++;
                    // add the document record
                    var documentRecordBuilder = new StringBuilder();
                    //1200
                    documentRecordBuilder.Append("C100");
                    //1201
                    documentRecordBuilder.Append(OpenFormatUtils.GetIntString(recordNumber, 9));
                    //1202
                    documentRecordBuilder.Append(issuer.CompanyId);
                    string docTypeIdForOpenFormat;
                    switch (doc.FinanceDocumentType)
                    {
                        case FinanceDocumentType.Invoice:
                            docTypeIdForOpenFormat = "305";
                            break;
                        case FinanceDocumentType.Receipt:
                            docTypeIdForOpenFormat = "400";
                            break;
                        case FinanceDocumentType.InvoiceReceipt:
                            docTypeIdForOpenFormat = "320";
                            break;
                        case FinanceDocumentType.Proforma:
                            docTypeIdForOpenFormat = "300";
                            break;
                        case FinanceDocumentType.Refound:
                            docTypeIdForOpenFormat = "330";
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    //1203
                    documentRecordBuilder.Append(docTypeIdForOpenFormat);
                    //1204
                    documentRecordBuilder.Append(OpenFormatUtils.GetAlphaString(doc.SerialNumber.ToString(), 20));
                    //1205
                    documentRecordBuilder.Append(OpenFormatUtils.GetDate(doc.Created));
                    //1206
                    documentRecordBuilder.Append(OpenFormatUtils.GetTime(doc.Created));
                    //1207
                    documentRecordBuilder.Append(OpenFormatUtils.GetAlphaString(doc.ClientName, 50));
                    //1208
                    documentRecordBuilder.Append(OpenFormatUtils.GetAlphaString(doc.ClientAddress, 50));
                    //1209
                    documentRecordBuilder.Append(OpenFormatUtils.GetEmptyString(10, false));
                    //1210
                    documentRecordBuilder.Append(OpenFormatUtils.GetEmptyString(30, false));
                    //1211
                    documentRecordBuilder.Append(OpenFormatUtils.GetEmptyString(8, false));
                    //1212
                    documentRecordBuilder.Append(OpenFormatUtils.GetEmptyString(30, false));
                    //1213
                    documentRecordBuilder.Append("IL");
                    //1214
                    documentRecordBuilder.Append(OpenFormatUtils.GetAlphaString(doc.ClientPhone, 15));
                    //1215
                    if (doc.Client != null && !string.IsNullOrEmpty(doc.Client.IdCard))
                    {
                        documentRecordBuilder.Append(OpenFormatUtils.GetIntString(doc.Client.IdCard, 9));
                    }
                    else
                    {
                        documentRecordBuilder.Append(OpenFormatUtils.GetEmptyString(9, false));
                    }
                    //1216
                    documentRecordBuilder.Append(OpenFormatUtils.GetDate(doc.Created));
                    //1217
                    documentRecordBuilder.Append(OpenFormatUtils.GetEmptyString(15, false));
                    //1218
                    documentRecordBuilder.Append(OpenFormatUtils.GetEmptyString(3, false));
                    //1219
                    documentRecordBuilder.Append(OpenFormatUtils.GetDecimalString(doc.TotalBeforeVATAfterDisscount + doc.Discount, 15));
                    //1220
                    documentRecordBuilder.Append(OpenFormatUtils.GetDecimalString(doc.Discount * (-1), 15));
                    //1221
                    documentRecordBuilder.Append(OpenFormatUtils.GetDecimalString(doc.TotalBeforeVATAfterDisscount, 15));
                    //1222
                    documentRecordBuilder.Append(OpenFormatUtils.GetDecimalString(doc.VatPayed, 15));
                    //1223
                    documentRecordBuilder.Append(OpenFormatUtils.GetDecimalString(doc.TotalSum, 15));
                    //1224 
                    documentRecordBuilder.Append(OpenFormatUtils.GetDecimalString(0, 12));
                    //1225
                    if (doc.ClientId != null)
                    {
                        documentRecordBuilder.Append(OpenFormatUtils.GetAlphaString(doc.ClientId.ToString(), 15));
                    }
                    else
                    {
                        documentRecordBuilder.Append(OpenFormatUtils.GetAlphaString("0", 15));
                    }
                    //1226
                    documentRecordBuilder.Append(OpenFormatUtils.GetEmptyString(10, false));
                    //1228
                    documentRecordBuilder.Append(0);
                    //1230
                    documentRecordBuilder.Append(OpenFormatUtils.GetDate(doc.Created));
                    //1231
                    documentRecordBuilder.Append(OpenFormatUtils.GetEmptyString(7, false));
                    //1233
                    documentRecordBuilder.Append(OpenFormatUtils.GetEmptyString(9, false));
                    //1234
                    documentRecordBuilder.Append(OpenFormatUtils.GetIntString(doc.Id, 7));

                    file.WriteLine(documentRecordBuilder.ToString());
                    //add item records
                    var docRecordNumber = 0;
                    // add empty item to remove error on empty docs
                    if (doc.Items.Count == 0)
                    {
                        doc.Items.Add(new FinanceDocumentItem()
                            {
                                Quantity = 0,
                                UnitPrice = 0,
                                DiscountPercentage = 0,
                                Discount = 0,
                                TotalBeforeVAT = 0
                            });
                    }
                    if (doc.FinanceDocumentType != FinanceDocumentType.Receipt)
                    {
                        foreach (var item in doc.Items.FindAll(x => x.Id > 0))
                        {
                            itemNumber++;
                            recordNumber++;
                            docRecordNumber++;
                            var itemBuilder = new StringBuilder();
                            //1250(4)
                            itemBuilder.Append("D110");
                            //1251 (9)
                            itemBuilder.Append(OpenFormatUtils.GetIntString(recordNumber, 9));
                            //1252 (9)
                            itemBuilder.Append(
                                OpenFormatUtils.GetAlphaString(issuer.CompanyId, 9));
                            //1253 (3)
                            itemBuilder.Append(docTypeIdForOpenFormat);
                            //1254 (20)
                            itemBuilder.Append(OpenFormatUtils.GetAlphaString(doc.SerialNumber.ToString(), 20));
                            //1255 (4)
                            itemBuilder.Append(OpenFormatUtils.GetIntString(docRecordNumber, 4));
                            //1256 (3)
                            itemBuilder.Append(OpenFormatUtils.GetEmptyString(3, true));
                            //1257 (20)
                            itemBuilder.Append(OpenFormatUtils.GetEmptyString(20, false));
                            //1258 (1)
                            itemBuilder.Append("0");
                            //1259 (20)
                            itemBuilder.Append(OpenFormatUtils.GetAlphaString(item.CatalogNumber, 20));
                            //1260 (30)
                            if (string.IsNullOrEmpty(item.Description))
                            {
                                itemBuilder.Append(OpenFormatUtils.GetAlphaString("כללי", 30));
                            }
                            else
                            {
                                itemBuilder.Append(OpenFormatUtils.GetAlphaString(item.Description, 30));
                            }

                            //1261 (50)
                            itemBuilder.Append(OpenFormatUtils.GetEmptyString(50, false));
                            //1262 (30)
                            itemBuilder.Append(OpenFormatUtils.GetEmptyString(30, false));
                            //1263(20)
                            itemBuilder.Append(OpenFormatUtils.GetAlphaString("יחידה", 20));
                            //1264 ()
                            itemBuilder.Append(OpenFormatUtils.GetQuantinty(item.Quantity.Value));
                            //1265
                            itemBuilder.Append(OpenFormatUtils.GetDecimalString(item.UnitPrice.Value * (doc.TotalSum < 0 ? -1 : 1), 15));
                            //1266                            
                            itemBuilder.Append(OpenFormatUtils.GetDecimalString(-1 * (item.TotalDiscount * item.FinanceDocument.VAT), 15));
                            //1267
                            itemBuilder.Append(OpenFormatUtils.GetDecimalString(item.TotalBeforeVAT * (doc.TotalSum < 0 ? -1 : 1), 15));
                            //1268
                            var vatString = doc.VatPrcent.ToString("F");
                            vatString = vatString.Replace(".", "");
                            itemBuilder.Append(OpenFormatUtils.GetIntString(vatString, 4));
                            //1270
                            itemBuilder.Append(OpenFormatUtils.GetEmptyString(7, false));
                            //1272
                            itemBuilder.Append(OpenFormatUtils.GetDate(doc.Created));
                            //1273
                            itemBuilder.Append(OpenFormatUtils.GetIntString(doc.Id, 7));

                            file.WriteLine(itemBuilder.ToString());
                        }
                    }

                    //add payment record
                    if (doc.FinanceDocumentType == FinanceDocumentType.Receipt || doc.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt)
                    {
                        foreach (var payment in doc.Payments)
                        {
                            paymentNumber++;
                            recordNumber++;
                            docRecordNumber++;
                            var paymentBuilder = new StringBuilder();
                            //1300
                            paymentBuilder.Append("D120");
                            //1301
                            paymentBuilder.Append(OpenFormatUtils.GetIntString(recordNumber, 9));
                            //1302
                            paymentBuilder.Append(
                                OpenFormatUtils.GetAlphaString(issuer.CompanyId, 9));
                            //1303
                            paymentBuilder.Append(docTypeIdForOpenFormat);
                            //1304
                            paymentBuilder.Append(OpenFormatUtils.GetAlphaString(doc.SerialNumber.ToString(), 20));
                            //1305
                            paymentBuilder.Append(OpenFormatUtils.GetIntString(docRecordNumber, 4));
                            //1306
                            var paymentType = "";
                            switch (payment.PaymentType)
                            {
                                case PaymentType.Cheque:
                                    paymentType = "2";
                                    break;
                                case PaymentType.Cash:
                                    paymentType = "1";
                                    break;
                                case PaymentType.CreditCard:
                                    paymentType = "3";
                                    break;
                                case PaymentType.Transfer:
                                    paymentType = "4";
                                    break;
                            }
                            paymentBuilder.Append(paymentType);
                            if (payment.PaymentType == PaymentType.Cheque)
                            {
                                if (doc.SerialNumber == 9021)
                                {
                                }
                                //1307
                                paymentBuilder.Append(OpenFormatUtils.GetIntString(payment.BankCode.Code, 10));
                                //1308
                                paymentBuilder.Append(OpenFormatUtils.GetIntString(payment.BankBranch.Replace("/", ""),
                                                                                   10));
                                //1309
                                paymentBuilder.Append(OpenFormatUtils.GetIntString(payment.BankAccount, 15));
                                //1310 
                                paymentBuilder.Append(OpenFormatUtils.GetIntString(payment.ChequeNumber, 10));
                            }
                            else
                            {
                                //1307
                                paymentBuilder.Append(OpenFormatUtils.GetEmptyString(10, false));
                                //1308
                                paymentBuilder.Append(OpenFormatUtils.GetEmptyString(10, false));
                                //1309
                                paymentBuilder.Append(OpenFormatUtils.GetEmptyString(15, false));
                                //1310
                                paymentBuilder.Append(OpenFormatUtils.GetEmptyString(10, false));
                            }
                            //1311
                            if (payment.PaymentType == PaymentType.Cheque ||
                                payment.PaymentType == PaymentType.CreditCard)
                            {
                                if (payment.DueDate.HasValue)
                                {
                                    paymentBuilder.Append(OpenFormatUtils.GetDate(payment.DueDate.Value));
                                }
                                else
                                {
                                    paymentBuilder.Append(OpenFormatUtils.GetDate(DateTime.Now));
                                }

                            }
                            else
                            {
                                paymentBuilder.Append(OpenFormatUtils.GetEmptyString(8, false));
                            }
                            //1312
                            paymentBuilder.Append(OpenFormatUtils.GetDecimalString(payment.Sum, 15));

                            if (payment.PaymentType == PaymentType.CreditCard)
                            {
                                //1313
                                var creditCardCode = "1";
                                switch (payment.CreditCardCode.Name)
                                {
                                    case "ישראכרט":
                                        creditCardCode = "1";
                                        break;
                                    case "כאל":
                                    case "ויזה":
                                    case "ויזה כ.א.ל":
                                        creditCardCode = "2";
                                        break;
                                    case "דיינרס":
                                        creditCardCode = "3";
                                        break;
                                    case "אמריקן אקספרס":
                                        creditCardCode = "4";
                                        break;
                                    case "לאומי כארד":
                                        creditCardCode = "6";
                                        break;
                                }
                                paymentBuilder.Append(creditCardCode);
                                //1314
                                paymentBuilder.Append(OpenFormatUtils.GetAlphaString(payment.CreditCardCode.Name, 20));
                                //1315
                                var creditPaymentType = "5";
                                switch (payment.IssuerCreditType.Name)
                                {
                                    case "רגיל":
                                        creditPaymentType = "1";
                                        break;
                                    case "תשלומים":
                                        creditPaymentType = "2";
                                        break;
                                    case "קרדיט":
                                        creditPaymentType = "3";
                                        break;
                                    case "חיוב נדחה":
                                        creditPaymentType = "4";
                                        break;
                                }
                                paymentBuilder.Append(creditPaymentType);
                            }
                            else
                            {
                                //1313
                                paymentBuilder.Append(" ");
                                //1314
                                paymentBuilder.Append(OpenFormatUtils.GetEmptyString(20, false));
                                //1315
                                paymentBuilder.Append(" ");
                            }



                            //1320
                            paymentBuilder.Append(OpenFormatUtils.GetEmptyString(7, false));
                            //1322
                            paymentBuilder.Append(OpenFormatUtils.GetDate(doc.Created));
                            //1323
                            paymentBuilder.Append(OpenFormatUtils.GetIntString(doc.Id, 7));

                            file.WriteLine(paymentBuilder.ToString());
                        }
                    }

                    //Abb B110 Record
                    recordNumber++;
                    var clientRecordBuilder = new StringBuilder();
                    //1400
                    clientRecordBuilder.Append("B110");
                    //1401
                    clientRecordBuilder.Append(OpenFormatUtils.GetIntString(recordNumber, 9));
                    //1402
                    clientRecordBuilder.Append(
OpenFormatUtils.GetAlphaString(issuer.CompanyId, 9));
                    //1403
                    if (doc.ClientId != null)
                    {
                        clientRecordBuilder.Append(OpenFormatUtils.GetIntString(doc.ClientId.Value, 15));
                    }
                    else
                    {
                        clientRecordBuilder.Append(OpenFormatUtils.GetIntString(0, 15));
                    }
                    //1404
                    clientRecordBuilder.Append(OpenFormatUtils.GetAlphaString(doc.ClientName, 50));
                    //1405
                    clientRecordBuilder.Append(OpenFormatUtils.GetEmptyString(15, true));
                    //1406
                    clientRecordBuilder.Append(OpenFormatUtils.GetEmptyString(30, true));
                    //1407
                    clientRecordBuilder.Append(OpenFormatUtils.GetAlphaString(doc.ClientAddress, 50));
                    //1408
                    clientRecordBuilder.Append(OpenFormatUtils.GetEmptyString(10, false));
                    //1409
                    clientRecordBuilder.Append(OpenFormatUtils.GetEmptyString(30, false));
                    //1410
                    clientRecordBuilder.Append(OpenFormatUtils.GetEmptyString(8, false));
                    //1411
                    clientRecordBuilder.Append(OpenFormatUtils.GetEmptyString(30, false));
                    //1412
                    clientRecordBuilder.Append("IL");
                    //1413
                    clientRecordBuilder.Append(OpenFormatUtils.GetEmptyString(15, false));
                    //1414
                    clientRecordBuilder.Append("+" + OpenFormatUtils.GetEmptyString(14, true));
                    //1415
                    clientRecordBuilder.Append("+" + OpenFormatUtils.GetEmptyString(14, true));
                    //1416
                    clientRecordBuilder.Append("+" + OpenFormatUtils.GetEmptyString(14, true));
                    //1417
                    clientRecordBuilder.Append(OpenFormatUtils.GetEmptyString(4, true));
                    //1419
                    if (doc.Client != null)
                    {
                        clientRecordBuilder.Append(OpenFormatUtils.GetIdNumber(doc.Client.IdCard));
                    }
                    else
                    {
                        clientRecordBuilder.Append(OpenFormatUtils.GetIdNumber(""));
                    }
                    file.WriteLine(clientRecordBuilder.ToString());
                }
                //Close Record



                recordNumber++;
                var closeRecordBuilder = new StringBuilder();

                closeRecordBuilder.Append("Z900");
                closeRecordBuilder.Append(OpenFormatUtils.GetIntString(recordNumber, 9));
                closeRecordBuilder.Append(issuer.CompanyId);
                closeRecordBuilder.Append(mainId);
                closeRecordBuilder.Append(OpenFormatUtils.SystemConst);
                closeRecordBuilder.Append(OpenFormatUtils.GetIntString(recordNumber, 15));
                file.WriteLine(closeRecordBuilder.ToString());
            }

            //Zip the file
            using (ZipFile zip = new ZipFile())
            {
                zip.AddFile(bkmvDataFilePath, "");
                zip.Save(zipbkmvDataFilePath);
            }

            System.IO.File.Delete(bkmvDataFilePath);

            //Create the ini file
            using (var file = new System.IO.StreamWriter(iniFilePath, false, Encoding.GetEncoding("ISO-8859-8-i")))
            {
                //Open Record
                var openRecordBuilder = new StringBuilder();
                //1000 (4)
                openRecordBuilder.Append("A000");
                //1001 (5)
                openRecordBuilder.Append(OpenFormatUtils.GetEmptyString(5, false));
                //1002 (15)
                openRecordBuilder.Append(OpenFormatUtils.GetIntString(recordNumber, 15));
                //1003 (9)
                openRecordBuilder.Append(
OpenFormatUtils.GetAlphaString(issuer.CompanyId, 9));
                //1004 (15)
                openRecordBuilder.Append(mainId);
                //1005 (8)
                openRecordBuilder.Append(OpenFormatUtils.SystemConst);
                //1006 (8)
                openRecordBuilder.Append(
                    OpenFormatUtils.GetIntString(ConfigurationManager.AppSettings["SoftwareRegistrationNumber"].ToString(), 8));
                //1007 (20)
                openRecordBuilder.Append(
     OpenFormatUtils.GetAlphaString(ConfigurationManager.AppSettings["SoftwareName"].ToString(), 20));
                //1008  (20)
                openRecordBuilder.Append(
OpenFormatUtils.GetAlphaString(ConfigurationManager.AppSettings["SoftwareVersion"].ToString(), 20));
                //1009 (9)
                openRecordBuilder.Append(
OpenFormatUtils.GetAlphaString(ConfigurationManager.AppSettings["RapidCropId"].ToString(), 9));
                //1010 (20)
                openRecordBuilder.Append(
OpenFormatUtils.GetAlphaString(ConfigurationManager.AppSettings["RapidCropName"].ToString(), 20));
                //1011 (1)
                openRecordBuilder.Append("2");
                //1012 (50)
                openRecordBuilder.Append(OpenFormatUtils.GetAlphaString("c:/", 50));
                //1013 (1)
                openRecordBuilder.Append("1");
                //1014 (1)
                openRecordBuilder.Append(" ");
                //1015 (9)
                openRecordBuilder.Append(
OpenFormatUtils.GetAlphaString(issuer.CompanyId, 9));
                //1016 (9)
                openRecordBuilder.Append(OpenFormatUtils.GetIntString(issuer.TaxDedationFileNumber, 9));
                //1017 (10)
                openRecordBuilder.Append(OpenFormatUtils.GetEmptyString(10, false));
                //1018 (50)
                openRecordBuilder.Append(OpenFormatUtils.GetAlphaString(issuer.Name, 50));
                //1019 (50)
                openRecordBuilder.Append(OpenFormatUtils.GetEmptyString(50, false));
                //1020 (10)
                openRecordBuilder.Append(OpenFormatUtils.GetEmptyString(10, false));
                //1021 (30)
                openRecordBuilder.Append(OpenFormatUtils.GetEmptyString(30, false));
                //1022 (8)
                openRecordBuilder.Append(OpenFormatUtils.GetEmptyString(8, false));
                //1023 (4)
                openRecordBuilder.Append(OpenFormatUtils.GetEmptyString(4, false));
                //1024
                openRecordBuilder.Append(OpenFormatUtils.GetDate(startDate));
                //1025
                openRecordBuilder.Append(OpenFormatUtils.GetDate(endDate));
                //1026
                openRecordBuilder.Append(OpenFormatUtils.GetDate(proccessStartTime));
                //1027
                openRecordBuilder.Append(OpenFormatUtils.GetTime(proccessStartTime));
                //1028
                openRecordBuilder.Append("0");
                //1029
                openRecordBuilder.Append("1");
                //1030
                openRecordBuilder.Append(OpenFormatUtils.GetAlphaString("Zip", 20));
                //1032
                openRecordBuilder.Append("ILS");
                //1034
                openRecordBuilder.Append("0");
                file.WriteLine(openRecordBuilder.ToString());

                //Document record
                var docRecordBuilder = new StringBuilder();
                docRecordBuilder.Append("C100");
                docRecordBuilder.Append(OpenFormatUtils.GetIntString(docNumber, 15));
                file.WriteLine(docRecordBuilder.ToString());

                //item record
                var itemRecordBuilder = new StringBuilder();
                itemRecordBuilder.Append("D110");
                itemRecordBuilder.Append(OpenFormatUtils.GetIntString(itemNumber, 15));
                file.WriteLine(itemRecordBuilder.ToString());

                //payment record
                var paymentsRecordBuilder = new StringBuilder();
                paymentsRecordBuilder.Append("D120");
                paymentsRecordBuilder.Append(OpenFormatUtils.GetIntString(paymentNumber, 15));
                file.WriteLine(paymentsRecordBuilder.ToString());

                //B100 record
                var B100RecordBuilder = new StringBuilder();
                B100RecordBuilder.Append("B100");
                B100RecordBuilder.Append(OpenFormatUtils.GetIntString(0, 15));
                file.WriteLine(B100RecordBuilder.ToString());

                //B110 record
                var B110RecordBuilder = new StringBuilder();
                B110RecordBuilder.Append("B110");
                B110RecordBuilder.Append(OpenFormatUtils.GetIntString(docNumber, 15));
                file.WriteLine(B110RecordBuilder.ToString());
            }


            //Zip all files to zip and send as stream
            ZipFile outputZip = new ZipFile();
            outputZip.AddDirectory(tempFolder, "");
            var stream = new MemoryStream();
            outputZip.Save(stream);
            stream.Position = 0;
            return new FileStreamResult(stream, "application/zip");

        }
    }
}
