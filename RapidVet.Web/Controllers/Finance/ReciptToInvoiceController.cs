﻿using RapidVet.WebModels.Finance;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RapidVet.Web.Controllers
{
    public class ReciptToInvoiceController : BaseController
    {
        //
        // GET: /ReciptToInvoice/

        public ActionResult Index()
        {

            ViewBag.TaxRate = GetTaxRate();

            if (Request.HttpMethod == "POST")
            {
                int completed = 0;

                completed = RapidVetUnitOfWork.FinanceDocumentReposetory.MonthelyReciptToInvoice(CurrentUser.ActiveClinicId, CurrentUser.Id, GetTaxRate());

                SpecialInvoiceConversionWebModel model = new SpecialInvoiceConversionWebModel
                {
                    Converted = completed,
                    Url = "/FinanceReports/InvoiceReport/?RedirectedFromReciptToInvoice=1",
                    AllowedToViewFinancialReports = CurrentUser.IsViewFinancialReports
                };

                return View("Sucsess", model);
            }
            return View();
        }


        public ActionResult Special(int? issuerId, string fromTime, string toTime)
        {
            DateTime from;
            DateTime to;
            if (string.IsNullOrWhiteSpace(fromTime) || string.IsNullOrWhiteSpace(toTime))
            {
                var now = DateTime.Now;
                to = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month));
                from = new DateTime(now.Year, now.Month, 1);
            }
            else
            {
                from = DateTime.MinValue;
                to = DateTime.MinValue;
                var format = "dd/MM/yyyy";
                var unparsedFrom = fromTime;
                DateTime.TryParseExact(unparsedFrom, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out from);
                if (from == DateTime.MinValue)
                {
                    from = DateTime.Now;
                }
                var unparsedTo = toTime;
                DateTime.TryParseExact(unparsedTo, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out to);
                if (to == DateTime.MinValue)
                {
                    to = DateTime.Now;
                }
            }
          
            if(!issuerId.HasValue)
            {
                issuerId = (RapidVetUnitOfWork.IssuerRepository.GetList(CurrentUser.ActiveClinicId, CurrentUser.Id).OrderBy(i => i.Id).First()).Id;
            }

            var model = RapidVetUnitOfWork.FinanceDocumentReposetory.GetPaymentsWithoutInvoice(CurrentUser.ActiveClinicId,from,to,issuerId.Value);

            foreach (var item in model)
            {
                if (string.IsNullOrWhiteSpace(item.Recipt.ClientName))
                {
                    item.Recipt.ClientName = Resources.Migration.TempClient;
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Special(int[] paymentsIds)
        {
            int completed = 0;

            foreach (var itemId in paymentsIds)
            {
                var payment = RapidVetUnitOfWork.FinanceDocumentReposetory.GetFinancePayment(itemId);
                if (payment.ClinicId == CurrentUser.ActiveClinicId)
                {
                    completed += RapidVetUnitOfWork.FinanceDocumentReposetory.PaymentToInvoice(payment, CurrentUser.Id, GetTaxRate());
                }
            }

            SpecialInvoiceConversionWebModel model = new SpecialInvoiceConversionWebModel
            {
                Converted = completed,
                Url = "/FinanceReports/InvoiceReport/?RedirectedFromReciptToInvoice=1",
                AllowedToViewFinancialReports = CurrentUser.IsViewFinancialReports
            };
            
            return View("Sucsess-Special", model);
        }
    }
}
