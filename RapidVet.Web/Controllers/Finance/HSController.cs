﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Ionic.Zip;
using RapidVet.Enums.Finances;
using RapidVet.Helpers;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class HSController : BaseController
    {
        //
        // GET: /HS/
        public ActionResult Index()
        {
            if (CurrentUser.ActiveClinicGroup.HSModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            return View();
        }

        private List<string> invalidIssuerSettings(int issuerId)
        {
            List<string> invalidSettings = new List<string>();
            var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(issuerId);

            if(string.IsNullOrWhiteSpace(issuer.HSVatAccount))
            {
                invalidSettings.Add(string.Format(Resources.Issuer.HsIssuerError, Resources.Issuer.HSVatAccount));
            }

            if (string.IsNullOrWhiteSpace(issuer.HSIncomeAccount))
            {
                invalidSettings.Add(string.Format(Resources.Issuer.HsIssuerError, Resources.Issuer.HSIncomeAccount));
            }

            if (string.IsNullOrWhiteSpace(issuer.HSCashAccount))
            {
                invalidSettings.Add(string.Format(Resources.Issuer.HsIssuerError, Resources.Issuer.HSCashAccount));
            }

            if (string.IsNullOrWhiteSpace(issuer.HSChequeAccount))
            {
                invalidSettings.Add(string.Format(Resources.Issuer.HsIssuerError, Resources.Issuer.HSChequeAccount));
            }

            if (string.IsNullOrWhiteSpace(issuer.HSRandoClientAccount))
            {
                invalidSettings.Add(string.Format(Resources.Issuer.HsIssuerError, Resources.Issuer.HSRandoClientAccount));
            }

            if (string.IsNullOrWhiteSpace(issuer.HSTranferAccount))
            {
                invalidSettings.Add(string.Format(Resources.Issuer.HsIssuerError, Resources.Issuer.HSTranferAccount));
            }

            if (string.IsNullOrWhiteSpace(issuer.HSInvoiceMovementType))
            {
                invalidSettings.Add(string.Format(Resources.Issuer.HsIssuerError, Resources.Issuer.HSInvoiceMovementType));
            }

            if (string.IsNullOrWhiteSpace(issuer.HSRefundMovementType))
            {
                invalidSettings.Add(string.Format(Resources.Issuer.HsIssuerError, Resources.Issuer.HSRefundMovementType));
            }

            if (string.IsNullOrWhiteSpace(issuer.HSClientSortKey))
            {
                invalidSettings.Add(string.Format(Resources.Issuer.HsIssuerError, Resources.Issuer.HSClientSortKey));
            }


            return invalidSettings;

        }

        /// <summary>
        /// מחזיר קובץ תנועות קופה חשבשבת
        /// עם נתוני הפקדות
        /// </summary>
        /// <returns></returns>
        public ActionResult KUPAIN(string from, string to, int issuerId)
        {
            if (CurrentUser.ActiveClinicGroup.HSModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            var errors = invalidIssuerSettings(issuerId);

            if(errors.Count > 0)
            {
                return View("InvalidIssuerSettings", errors);
            }

            
            var tempFolder = InitTempFolder();
            CreateHeshinFile(tempFolder, issuerId);

            var startDate = DateTime.ParseExact(from, "dd/MM/yyyy", null);
            var endDate = DateTime.ParseExact(to, "dd/MM/yyyy", null);


            CreateKUPAIN(tempFolder, issuerId, startDate, endDate);

            //Zip all files to zip and send as stream
            ZipFile outputZip = new ZipFile();
            outputZip.AddDirectory(tempFolder, "");
            var stream = new MemoryStream();
            outputZip.Save(stream);
            stream.Position = 0;
            return new FileStreamResult(stream, "application/zip");
        }

        /// <summary>
        /// קובץ תנועות יומן - חשבוניות
        /// </summary>
        /// <returns></returns>
        public ActionResult MOVEINInvoice(string from, string to, int issuerId)
        {
            if (CurrentUser.ActiveClinicGroup.HSModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            var errors = invalidIssuerSettings(issuerId);

            if (errors.Count > 0)
            {
                return View("InvalidIssuerSettings", errors);
            }


            var startDate = DateTime.ParseExact(from, "dd/MM/yyyy", null);
            var endDate = DateTime.ParseExact(to, "dd/MM/yyyy", null);

            var tempFolder = InitTempFolder();
            CreateHeshinFile(tempFolder, issuerId);

            CreateMOVEIN(tempFolder, issuerId, startDate, endDate, false, true);

            //Zip all files to zip and send as stream
            ZipFile outputZip = new ZipFile();
            outputZip.AddDirectory(tempFolder, "");
            var stream = new MemoryStream();
            outputZip.Save(stream);
            stream.Position = 0;
            return new FileStreamResult(stream, "application/zip");
        }

        /// <summary>
        /// מחזיר קובץ תנועות יומן
        /// </summary>
        /// <returns></returns>
        public ActionResult MOVEINRecipt(string from, string to, int issuerId)
        {
            if (CurrentUser.ActiveClinicGroup.HSModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            var errors = invalidIssuerSettings(issuerId);

            if (errors.Count > 0)
            {
                return View("InvalidIssuerSettings", errors);
            }


            var startDate = DateTime.ParseExact(from, "dd/MM/yyyy", null);
            var endDate = DateTime.ParseExact(to, "dd/MM/yyyy", null);

            var tempFolder = InitTempFolder();
            CreateHeshinFile(tempFolder, issuerId);

            CreateMOVEIN(tempFolder, issuerId, startDate, endDate, true, false);

            //Zip all files to zip and send as stream
            ZipFile outputZip = new ZipFile();
            outputZip.AddDirectory(tempFolder, "");
            var stream = new MemoryStream();
            outputZip.Save(stream);
            stream.Position = 0;
            return new FileStreamResult(stream, "application/zip");
        }

        /// <summary>
        /// מחזיר את כל הקבצים
        /// </summary>
        /// <returns></returns>
        public ActionResult AllFiles(string from, string to, int issuerId)
        {
            if (CurrentUser.ActiveClinicGroup.HSModule != true)
            {
                return View("_UnauthorizedModuleAccess");
            }

            var errors = invalidIssuerSettings(issuerId);

            if (errors.Count > 0)
            {
                return View("InvalidIssuerSettings", errors);
            }


            var startDate = DateTime.ParseExact(from, "dd/MM/yyyy", null);
            var endDate = DateTime.ParseExact(to, "dd/MM/yyyy", null);

            var tempFolder = InitTempFolder();
            CreateHeshinFile(tempFolder, issuerId);
            CreateKUPAIN(tempFolder, issuerId, startDate, endDate);
            CreateMOVEIN(tempFolder, issuerId, startDate, endDate, true, true);

            //Zip all files to zip and send as stream
            ZipFile outputZip = new ZipFile();
            outputZip.AddDirectory(tempFolder, "");
            var stream = new MemoryStream();
            outputZip.Save(stream);
            stream.Position = 0;
            return new FileStreamResult(stream, "application/zip");
        }

        private string GetPaymentAccount(FinanceDocumentPayment payment, Issuer issuer, CreditCardCode creditCode)
        {
            if (payment.PaymentType == PaymentType.Cash)
            {
                return issuer.HSCashAccount;
            }
            if (payment.PaymentType == PaymentType.CreditCard)
            {
                return creditCode.ExternalAccountId;
            }

            if (payment.PaymentType == PaymentType.Cheque)
            {
                return issuer.HSChequeAccount;
            }
            if (payment.PaymentType == PaymentType.Transfer)
            {
                return issuer.HSTranferAccount;
            }
            throw new Exception("סוג מסמך שגוי");
        }

        private string InitTempFolder()
        {
            var mainTempFolder = Server.MapPath("/tempHsFolder");
            if (!System.IO.Directory.Exists(mainTempFolder))
            {
                System.IO.Directory.CreateDirectory(mainTempFolder);
            }

            var tempFolder = System.IO.Path.Combine(mainTempFolder, Guid.NewGuid().ToString());
            System.IO.Directory.CreateDirectory(tempFolder);
            return tempFolder;
        }

        private void CreateMOVEIN(string tempFolder, int issuerId, DateTime startDate, DateTime endDate, bool recipt,
                                  bool invoice)
        {
            var datPath = System.IO.Path.Combine(tempFolder, "MOVEIN.DOC");
            var prmPath = System.IO.Path.Combine(tempFolder, "MOVEIN.PRM");

            var globalPrmPath = Server.MapPath("/HSPRMS/movein.prm");
            //Copy the prm file
            System.IO.File.Copy(globalPrmPath, prmPath);

            var issure = RapidVetUnitOfWork.IssuerRepository.GetItem(issuerId);


            using (var file = new System.IO.StreamWriter(datPath, false, Encoding.Default))//Encoding.GetEncoding("Windows-1255")))
            {
                if (recipt)
                {
                    var reciptPayments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetReciptsForHS(startDate, endDate,
                                                                                                     issuerId);


                    foreach (var payment in reciptPayments)
                    {
                        var docBuilder = new StringBuilder();

                        //movement_type(3)
                        docBuilder.Append(OpenFormatUtils.GetEmptyString(3,false));
                        //asmahta1(9)
                        docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.Recipt.SerialNumber.ToString(), 9));
                        //asmahta2 (9)
                        docBuilder.Append(OpenFormatUtils.GetEmptyString(9, false));
                        //asmahta_date(10)
                        docBuilder.Append(OpenFormatUtils.GetDateForHS(payment.Recipt.Created));
                        //value_date (10)
                        if (payment.DueDate.HasValue)
                        {
                            docBuilder.Append(OpenFormatUtils.GetDateForHS(payment.DueDate.Value));
                        }
                        else
                        {
                            docBuilder.Append(OpenFormatUtils.GetDateForHS(payment.Recipt.Created));
                        }
                        //currency (4)
                        docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse("ש\"ח", 4));
                        //hand_written_comment (22) - doc type + doc serial number + doc comments

                        int commentLength = 22;

                        switch (payment.Recipt.FinanceDocumentType)
                        {
                            case FinanceDocumentType.Invoice:
                                docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse("חש", 2));
                                commentLength -= 2;
                                break;
                            case FinanceDocumentType.Refound:
                                docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse("ח/שז", 4));
                                commentLength -= 4;
                                break;
                            case FinanceDocumentType.InvoiceReceipt:
                                docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse("חש/קב", 5));
                                commentLength -= 5;
                                break;
                            case FinanceDocumentType.Receipt:
                                docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse("קבלה", 4));
                                commentLength -= 4;
                                break;
                        }
                        docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.Recipt.SerialNumber.ToString(), payment.Recipt.SerialNumber.ToString().Length));
                        commentLength -= payment.Recipt.SerialNumber.ToString().Length;
                        docBuilder.Append(OpenFormatUtils.GetAlphaString("-", 1));
                        commentLength--;
                        docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse(payment.Recipt.Comments, commentLength));           
                        //debit_account1 (8)
                        if (payment.Recipt.TotalSum > 0)
                        {
                            CreditCardCode creditCode = null;
                            if (payment.CreditCardCodeId.HasValue)
                                creditCode = RapidVetUnitOfWork.CreditCardCodeRepository.GetCreditCardCode(payment.CreditCardCodeId.Value);

                            docBuilder.Append(OpenFormatUtils.GetAlphaString(GetPaymentAccount(payment, issure, creditCode), 8));
                        }
                        else
                        {
                            if (payment.Recipt.Client != null)
                            {
                                if (!string.IsNullOrWhiteSpace(issure.HSGeneralClientAccount))
                                {
                                    docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSGeneralClientAccount, 8));
                                }
                                else
                                {
                                    docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.Recipt.Client.HsId.ToString(), 8));
                                }
                            }
                            else
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSRandoClientAccount, 8));
                            }
                        }
                        //debit_account2 (8)
                        docBuilder.Append(OpenFormatUtils.GetEmptyString(8, false));
                        //credit_account1 (8)
                        if (payment.Recipt.TotalSum <= 0)
                        {
                            CreditCardCode creditCode = null;
                            if (payment.CreditCardCodeId.HasValue)
                                creditCode = RapidVetUnitOfWork.CreditCardCodeRepository.GetCreditCardCode(payment.CreditCardCodeId.Value);

                            docBuilder.Append(OpenFormatUtils.GetAlphaString(GetPaymentAccount(payment, issure, creditCode), 8));
                        }
                        else
                        {
                            if (payment.Recipt.Client != null)
                            {
                                if (!string.IsNullOrWhiteSpace(issure.HSGeneralClientAccount))
                                {
                                    docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSGeneralClientAccount, 8));
                                }
                                else
                                {
                                    docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.Recipt.Client.HsId.ToString(), 8));
                                }
                            }
                            else
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSRandoClientAccount, 8));
                            }
                        }
                        //credit_account2 (8)
                        docBuilder.Append(OpenFormatUtils.GetEmptyString(8, false));
                        //debit_sum1 (9.2)
                        docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(payment.Recipt.TotalSum));
                        //debit_sum2(9.2)
                        docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));

                        //credit_sum1 (9.2)
                        docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(payment.Recipt.TotalSum));
                        //credit_sum2 (9.2)
                        docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                        // foreign_debit_sum1 (9.2)
                        docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                        // foreign_debit_sum2 (9.2)
                        docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                        // foreign_credit_sum1 (9.2)
                        docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                        // foreign_credit_sum2 (9.2) 
                        docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                        // hp (9)
                        if (payment.Recipt.Client != null)
                        {
                            docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.Recipt.Client.IdCard, 9));
                        }
                        else
                        {
                            docBuilder.Append(OpenFormatUtils.GetEmptyString(9, false));
                        }
                        file.WriteLine(docBuilder.ToString());
                    }
                }
                if (invoice)
                {
                    var invoices = RapidVetUnitOfWork.FinanceDocumentReposetory.GetInvoicesForHS(startDate, endDate,
                                                                                         issuerId);

                    foreach (var financeDocument in invoices)
                    {
                        var docBuilder = new StringBuilder();

                        //movement_type(3)
                        if (financeDocument.FinanceDocumentType != FinanceDocumentType.Refound)
                        {
                            if(IsFirstLetterNumber(issure.HSInvoiceMovementType))
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSInvoiceMovementType, 3));
                            }
                            else
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse(issure.HSInvoiceMovementType, 3));
                            }
                        }
                        else
                        {
                            if (IsFirstLetterNumber(issure.HSRefundMovementType))
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSRefundMovementType, 3));
                            }
                            else
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse(issure.HSRefundMovementType, 3));
                            }
                        }
                        
                        //if(financeDocument.TotalSum < 0)
                        //{
                        //    financeDocument.FinanceDocumentType = FinanceDocumentType.Refound;
                        //}

                        //asmahta1(9)
                        docBuilder.Append(OpenFormatUtils.GetAlphaString(financeDocument.SerialNumber.ToString(), 9));
                        //asmahta2 (9)
                        docBuilder.Append(OpenFormatUtils.GetEmptyString(9, false));
                        //asmahta_date(10)
                        docBuilder.Append(OpenFormatUtils.GetDateForHS(financeDocument.Created));
                        //value_date (10)
                        docBuilder.Append(OpenFormatUtils.GetDateForHS(financeDocument.Created));
                        //currency (4)
                        docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse("ש\"ח", 4));
                        //hand_written_comment (22) - doc type + doc serial number + doc comments

                        int commentLength = 22;

                        switch(financeDocument.FinanceDocumentType)
                        {
                            case FinanceDocumentType.Invoice:
                                docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse("חש", 2));
                                commentLength -= 2;
                                break;
                            case FinanceDocumentType.Refound:
                                docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse("ח/שז", 4));
                                commentLength -= 4;
                                break;
                            case FinanceDocumentType.InvoiceReceipt:
                                docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse("חש/קב", 5));
                                commentLength -= 5;
                                break;
                            case FinanceDocumentType.Receipt:
                                docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse("קבלה", 4));
                                commentLength -= 4;
                                break;
                        }
                        docBuilder.Append(OpenFormatUtils.GetAlphaString(financeDocument.SerialNumber.ToString(), financeDocument.SerialNumber.ToString().Length));
                        commentLength -= financeDocument.SerialNumber.ToString().Length;
                        docBuilder.Append(OpenFormatUtils.GetAlphaString("-", 1));
                        commentLength--;
                        docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse(financeDocument.Comments, commentLength));
                        //debit_account1 (8)
                        if (financeDocument.FinanceDocumentType != FinanceDocumentType.Refound && financeDocument.TotalSum >= 0)
                        {
                            if (financeDocument.Client != null)
                            {
                                if (!string.IsNullOrWhiteSpace(issure.HSGeneralClientAccount))
                                {
                                    docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSGeneralClientAccount, 8));
                                }
                                else
                                {
                                    docBuilder.Append(OpenFormatUtils.GetAlphaString(financeDocument.Client.HsId.ToString(), 8));
                                }
                            }
                            else
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSRandoClientAccount, 8));
                            }
                        }
                        else
                        {
                            if (financeDocument.VatPayed != 0)
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSIncomeAccount, 8));
                            }
                            else
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSFreeIncomeAccount, 8));
                            }
                        }
                        //debit_account2 (8)
                        if (financeDocument.FinanceDocumentType != FinanceDocumentType.Refound && financeDocument.TotalSum >= 0)
                        {
                            docBuilder.Append(OpenFormatUtils.GetEmptyString(8, false));
                        }
                        else
                        {
                            if (financeDocument.VatPayed != 0)
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSVatAccount, 8));
                            }
                            else
                            {
                                docBuilder.Append(OpenFormatUtils.GetEmptyString(8, false));
                            }
                        }
                        //credit_account1 (8)
                        if (financeDocument.FinanceDocumentType == FinanceDocumentType.Refound || financeDocument.TotalSum < 0)
                        {
                            if (financeDocument.Client != null)
                            {
                                if (!string.IsNullOrWhiteSpace(issure.HSGeneralClientAccount))
                                {
                                    docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSGeneralClientAccount, 8));
                                }
                                else
                                {
                                    docBuilder.Append(OpenFormatUtils.GetAlphaString(financeDocument.Client.HsId.ToString(), 8));
                                }
                            }
                            else
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSRandoClientAccount, 8));
                            }
                        }
                        else
                        {
                            if (financeDocument.VatPayed != 0)
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSIncomeAccount, 8));
                            }
                            else
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSFreeIncomeAccount, 8));
                            }
                        }
                        //credit_account2 (8)
                        if (financeDocument.FinanceDocumentType == FinanceDocumentType.Refound || financeDocument.TotalSum < 0)
                        {
                            docBuilder.Append(OpenFormatUtils.GetEmptyString(8, false));
                        }
                        else
                        {
                            if (financeDocument.VatPayed != 0)
                            {
                                docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSVatAccount, 8));
                            }
                            else
                            {
                                docBuilder.Append(OpenFormatUtils.GetEmptyString(8, false));
                            }
                        }
                        //debit_sum1 (9.2)
                        if (financeDocument.FinanceDocumentType != FinanceDocumentType.Refound && financeDocument.TotalSum >= 0)
                        {
                            docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(financeDocument.TotalSum));
                        }
                        else
                        {
                            docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(financeDocument.TotalBeforeVATAfterDisscount));

                        }
                        //debit_sum2(9.2)
                        if (financeDocument.FinanceDocumentType != FinanceDocumentType.Refound && financeDocument.TotalSum >= 0)
                        {
                            docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                        }
                        else
                        {
                            if (financeDocument.VatPayed != 0)
                            {
                                docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(financeDocument.VatPayed));
                            }
                            else
                            {
                                docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                            }
                        }

                        //credit_sum1 (9.2)
                        if (financeDocument.FinanceDocumentType == FinanceDocumentType.Refound || financeDocument.TotalSum < 0)
                        {
                            docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(financeDocument.TotalSum));
                        }
                        else
                        {
                            docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(financeDocument.TotalBeforeVATAfterDisscount));

                        }
                        //credit_sum2 (9.2)
                        if (financeDocument.FinanceDocumentType == FinanceDocumentType.Refound || financeDocument.TotalSum < 0)
                        {
                            docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                        }
                        else
                        {
                            if (financeDocument.VatPayed != 0)
                            {
                                docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(financeDocument.VatPayed));
                            }
                            else
                            {
                                docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                            }
                        }
                        // foreign_debit_sum1 (9.2)
                        docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                        // foreign_debit_sum2 (9.2)
                        docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                        // foreign_credit_sum1 (9.2)
                        docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                        // foreign_credit_sum2 (9.2) 
                        docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));
                        // hp (9)
                        if (financeDocument.Client != null)
                        {
                            docBuilder.Append(OpenFormatUtils.GetAlphaString(financeDocument.Client.IdCard, 9));
                        }
                        else
                        {
                            docBuilder.Append(OpenFormatUtils.GetEmptyString(9, false));
                        }
                        file.WriteLine(docBuilder.ToString());
                    }
                }
            }
        }

        private void CreateKUPAIN(string tempFolder, int issuerId, DateTime startDate, DateTime endDate)
        {

            var datPath = System.IO.Path.Combine(tempFolder, "KUPAIN.DAT");
            var prmPath = System.IO.Path.Combine(tempFolder, "KUPAIN.PRM");

            var globalPrmPath = Server.MapPath("/HSPRMS/kupain.prm");
            //Copy the prm file
            System.IO.File.Copy(globalPrmPath, prmPath);

            var issure = RapidVetUnitOfWork.IssuerRepository.GetItem(issuerId);

            var payments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetPaymentsForDeposit(CurrentUser.ActiveClinicId, startDate, endDate, issuerId, null, null, true);

            payments = payments.Where(p => p.PaymentType == PaymentType.Cash || p.PaymentType == PaymentType.Cheque).ToList();

            //var payments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetDepositsForHs(startDate, endDate,
            //                                                                             issuerId);

            using (var file = new System.IO.StreamWriter(datPath, false, Encoding.Default))//Encoding.GetEncoding("Windows-1255")))
            {
                foreach (var payment in payments)
                {
                    var docBuilder = new StringBuilder();
                    //מפתח לקוח (text:8)
                    if (payment.Recipt.Client != null)
                    {
                        docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.Recipt.Client.HsId.ToString(), 8));
                    }
                    else
                    {
                        docBuilder.Append(OpenFormatUtils.GetAlphaString(issure.HSRandoClientAccount, 8));
                    }
                    //מספר קבלה (int:9)
                    docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.Recipt.SerialNumber.ToString(), 9));
                    //סוג תנועה (text: 3)
                    //לפי טבלאת קודים במסמך תנועות קופה
                    if (payment.PaymentType == PaymentType.Cheque)
                    {
                        docBuilder.Append(OpenFormatUtils.GetAlphaString("1", 3));
                    }
                    else //Cash
                    {
                        docBuilder.Append(OpenFormatUtils.GetAlphaString("2", 3));
                    }

                    //תאריך הקבלה (date: 8)
                    docBuilder.Append(OpenFormatUtils.GetShortDateForHS(payment.Recipt.Created));
                    //תאריך ערך (פרעון)(date: 8)
                    if (payment.PaymentType == PaymentType.Cheque)
                    {
                        docBuilder.Append(OpenFormatUtils.GetShortDateForHS(payment.DueDate.Value));
                    }
                    else //Cash
                    {
                        docBuilder.Append(OpenFormatUtils.GetShortDateForHS(payment.Recipt.Created));
                    }
                    //פרטים(text: 20)
                    docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse(payment.Recipt.Comments, 20));
                    //מספר השיק (int: 9)
                    if (payment.PaymentType == PaymentType.Cheque)
                    {
                        docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.ChequeNumber, 9));
                    }
                    else //Cash
                    {
                        docBuilder.Append(OpenFormatUtils.GetEmptyString(9, false));
                    }
                    //מספר בנק (int: 5)
                    if (payment.PaymentType == PaymentType.Cheque)
                    {
                        docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.BankCode.Code.ToString(), 5));
                    }
                    else //Cash
                    {
                        docBuilder.Append(OpenFormatUtils.GetEmptyString(5, false));
                    }
                    //מספר סניף (int: 10)
                    if (payment.PaymentType == PaymentType.Cheque)
                    {
                        docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.BankBranch, 10));
                    }
                    else //Cash
                    {
                        docBuilder.Append(OpenFormatUtils.GetEmptyString(10, false));
                    }
                    //מספר חשבון בנק (int: 20)
                    if (payment.PaymentType == PaymentType.Cheque)
                    {
                        docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.BankAccount, 20));
                    }
                    else //Cash
                    {
                        docBuilder.Append(OpenFormatUtils.GetEmptyString(20, false));
                    }
                    //סכום שקלים (float:9.2 = 12)
                    docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(payment.Sum));

                    //סכום מטח (float:9.2 = 12)
                    docBuilder.Append(OpenFormatUtils.GetDecimalStringForHS(0));

                    //מטבע מוביל  (text: 4)
                    docBuilder.Append(OpenFormatUtils.GetAlphaString("1", 4));

                    //מטבע שערוך  (text: 4)
                    docBuilder.Append(OpenFormatUtils.GetAlphaString("1", 4));

                    //ת.ז  (text: 11)
                    if (payment.Recipt.Client != null)
                    {
                        docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.Recipt.Client.IdCard, 11));
                    }
                    else
                    {
                        docBuilder.Append(OpenFormatUtils.GetEmptyString(11, false));
                    }
                    //שם חשבון  (text: 50)
                    docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse(payment.Recipt.ClientName, 50));


                    //כתובת (text: 50)
                    docBuilder.Append(OpenFormatUtils.GetAlphaAndReverse(payment.Recipt.ClientAddress, 50));


                    //טלפון  (text: 50)
                    docBuilder.Append(OpenFormatUtils.GetAlphaString(payment.Recipt.ClientPhone, 50));

                    file.WriteLine(docBuilder.ToString());
                }
            }
        }




        /// <summary>
        /// Create Heshin files in the folder
        /// Use for recipt and invoice 
        /// </summary>
        /// <param name="path"></param>
        private void CreateHeshinFile(string path, int issuerId)
        {
            var datPath = System.IO.Path.Combine(path, "HESHIN.DAT");
            var prmPath = System.IO.Path.Combine(path, "HESHIN.PRM");

            var globalPrmPath = Server.MapPath("/HSPRMS/heshin.prm");
            //Copy the prm file
            System.IO.File.Copy(globalPrmPath, prmPath);

            var clients = RapidVetUnitOfWork.ClientRepository.GetClinetsForHS(CurrentUser.ActiveClinicId);
            //Preper the clients
            var maxId = clients.Max(c => c.HsId);
            if (maxId == null)
            {
                maxId = CurrentUser.ActiveClinic.HSInitNumber ?? 1000;
            }
            foreach (var client in clients.Where(c => c.HsId == null))
            {
                maxId++;
                client.HsId = maxId;
            }
            RapidVetUnitOfWork.Save();

            var issuer = RapidVetUnitOfWork.IssuerRepository.GetItem(issuerId);

            using (var file = new System.IO.StreamWriter(datPath, false, Encoding.Default))//Encoding.GetEncoding("Windows-1255")))
            {
                foreach (var client in clients)
                {
                    var clientBuilder = new StringBuilder();
                    // client Account (15)
                    clientBuilder.Append(OpenFormatUtils.GetAlphaString(client.HsId.Value.ToString(), 15));
                    //Client Name (50)
                    clientBuilder.Append(OpenFormatUtils.GetAlphaAndReverse(client.Name, 50));
                    //Sort Code (4)
                    clientBuilder.Append(OpenFormatUtils.GetAlphaString(issuer.HSClientSortKey, 4));
                    //Phone(30)
                    var phone = client.Phones.FirstOrDefault();
                    if (phone != null)
                    {
                        clientBuilder.Append(OpenFormatUtils.GetAlphaString(phone.PhoneNumber, 30));
                    }
                    else
                    {
                        clientBuilder.Append(OpenFormatUtils.GetEmptyString(30, false));
                    }


                    var address = client.Addresses.FirstOrDefault();
                    if (address != null)
                    {
                        //Address(50)
                        clientBuilder.Append(OpenFormatUtils.GetAlphaAndReverse(address.Street, 50));
                        //City(20)
                        clientBuilder.Append(OpenFormatUtils.GetAlphaAndReverse(address.City.Name, 20));
                        //Zip(5)
                        clientBuilder.Append(OpenFormatUtils.GetIntString(address.ZipCode, 5));
                    }
                    else
                    {
                        //Address(50)
                        clientBuilder.Append(OpenFormatUtils.GetEmptyString(50, false));
                        //City(20)
                        clientBuilder.Append(OpenFormatUtils.GetEmptyString(20, false));
                        //Zip(5)
                        clientBuilder.Append(OpenFormatUtils.GetEmptyString(5, false));
                    }

                    //Notes(50)
                    clientBuilder.Append(OpenFormatUtils.GetAlphaAndReverse(client.VisibleComment, 50));
                    //hp (9)
                    clientBuilder.Append(OpenFormatUtils.GetAlphaString(client.IdCard, 9));
                    //Email(50)
                    var email = client.Emails.FirstOrDefault();
                    if (email != null)
                    {
                        clientBuilder.Append(OpenFormatUtils.GetAlphaString(email.Name, 50));
                    }
                    else
                    {
                        clientBuilder.Append(OpenFormatUtils.GetEmptyString(50, false));
                    }

                    file.WriteLine(clientBuilder.ToString());


                }
            }
        }

        public bool IsFirstLetterNumber(string input)
        {

            if (!char.IsNumber(input[0]))
            {
                return false;
            }
            return true;
        }

    }
}
