﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RapidVet.Model.Finance;
using RapidVet.WebModels.Finance.Reports;

namespace RapidVet.Web.Controllers
{
    [Authorize]
    public class FinanceDocumentDetectionController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Search(int? issuerId, int? documentTypeId, int? documentSerial, int? clientId,
                                 decimal? totalToPay, string chequeNumber)
        {
            var results = RapidVetUnitOfWork.FinanceReportRepository.SearchDocuments(CurrentUser.ActiveClinicId,
                                                                                     issuerId, documentTypeId,
                                                                                     documentSerial, clientId,
                                                                                     totalToPay, chequeNumber);

            var model = results.Select(AutoMapper.Mapper.Map<FinanceDocument, DetectionWebModel>).ToList();

            foreach (var item in model)
            {
                item.PrintUrl = Url.Action("Print", "FinanceDocument", new { id = item.Id });
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

    }
}
