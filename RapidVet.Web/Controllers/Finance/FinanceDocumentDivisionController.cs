﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using RapidVet.Enums;
using RapidVet.Enums.Finances;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Visits;
using RapidVet.Web.Attributes;
using RapidVet.WebModels.Finance;
using RapidVet.WebModels.Finance.Invoice;
using RapidVet.Model;

namespace RapidVet.Web.Controllers.Finance
{
    [Authorize]
    public class FinanceDocumentDivisionController : BaseController
    {
        public ActionResult Index(int? issuerId, string fromTime, string toTime)
        {
            if(!issuerId.HasValue)
            {
                return View();
            }

            DateTime from;
            DateTime to;
            if (string.IsNullOrWhiteSpace(fromTime) || string.IsNullOrWhiteSpace(toTime))
            {
                var now = DateTime.Now;
                to = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month));
                from = new DateTime(now.Year, now.Month, 1);
            }
            else
            {
                from = DateTime.MinValue;
                to = DateTime.MinValue;
                var format = "dd/MM/yyyy";
                var unparsedFrom = fromTime;
                DateTime.TryParseExact(unparsedFrom, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out from);
                if (from == DateTime.MinValue)
                {
                    from = DateTime.Now;
                }
                var unparsedTo = toTime;
                DateTime.TryParseExact(unparsedTo, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out to);
                if (to == DateTime.MinValue)
                {
                    to = DateTime.Now;
                }
            }

            var docs = RapidVetUnitOfWork.FinanceDocumentReposetory.GetDocumentsFromDate(issuerId.Value, @from, to.AddDays(1));

            foreach (var item in docs)
            {
                if (string.IsNullOrWhiteSpace(item.ClientName))
                {
                    item.ClientName = Resources.Migration.TempClient;
                }
            }

            return View(docs);
        }

        [ViewFinancialReports]
        public ActionResult Report(int? doctorId, int? issuerId, string fromTime, string toTime, int? dateType, bool firstRun = true)
        {
            DateTime from;
            DateTime to;
            if (string.IsNullOrWhiteSpace(fromTime) || string.IsNullOrWhiteSpace(toTime))
            {
                to = DateTime.Now;
                from = to.AddDays(-7);
            }
            else
            {
                from = DateTime.MinValue;
                to = DateTime.MinValue;
                var format = "dd/MM/yyyy";
                var unparsedFrom = fromTime;
                DateTime.TryParseExact(unparsedFrom, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out from);
                if (from == DateTime.MinValue)
                {
                    from = DateTime.Now;
                }
                var unparsedTo = toTime;
                DateTime.TryParseExact(unparsedTo, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out to);
                if (to == DateTime.MinValue)
                {
                    to = DateTime.Now;
                }
            }

            var model = new FinanceDocumentReportModel() { Reports = new List<FinanceDocumentReport>() };
            model.SelectedDateTypeIndex = dateType ?? 0;
            var doctors = RapidVetUnitOfWork.ClinicRepository.GetClinicDoctors(CurrentUser.ActiveClinicId);
            model.Doctors = doctors.Select(d => new SelectListItem() { Text = d.Name, Value = d.Id.ToString() }).ToList();

            var issuers = RapidVetUnitOfWork.IssuerRepository.GetList(CurrentUser.ActiveClinicId, CurrentUser.Id);
            model.Issuers = issuers.Select(i => new SelectListItem() { Text = i.Name, Value = i.Id.ToString() }).ToList();

            if (!firstRun)
            {
                var finanaceDocuments = RapidVetUnitOfWork.FinanceDocumentReposetory.GetDocumentsFromDateAndDoctorAndIssuer(from, to.AddDays(1), doctorId, issuerId, dateType ?? 0);
                foreach (var finanaceDocument in finanaceDocuments)
                {
                    foreach (var division in finanaceDocument.FinanceDocumentDividisions)
                    {
                        if (doctorId.HasValue)
                        {
                            if (division.DoctorId != doctorId.Value)
                            {
                                continue;
                            }
                        }

                        var sumAmount = finanaceDocument.BankTransferSum + finanaceDocument.CashPaymentSum +
                                        finanaceDocument.TotalCreditPaymentSum + finanaceDocument.TotalChequesPayment;


                        decimal percent = 0;
                        if (sumAmount > 0)
                        {
                            percent = division.Amount / sumAmount;
                        }

                        var report = new FinanceDocumentReport()
                            {
                                DoctorName = division.DoctorName,
                                CreatedDate = finanaceDocument.Created,
                                ClientName = finanaceDocument.ClientName,
                                TreatmentName =
                                    RapidVetUnitOfWork.VisitRepository.GetVisitTreatmentsStringByReciept(
                                        finanaceDocument.Id),
                                BankTransferSum = finanaceDocument.BankTransferSum * percent,
                                CashPaymentSum = finanaceDocument.CashPaymentSum * percent,
                                SerialNumber = finanaceDocument.SerialNumber,
                                TotalChequesPayment = finanaceDocument.TotalChequesPayment * percent,
                                TotalCreditPaymentSum = finanaceDocument.TotalCreditPaymentSum * percent,
                                CashedDate =
                                    finanaceDocument.OrginalPrintedDate.HasValue
                                        ? (DateTime?)finanaceDocument.OrginalPrintedDate.Value
                                        : null
                            };

                        if (finanaceDocument.FinanceDocumentType == FinanceDocumentType.Refound)
                        {
                            report.TreatmentName = Resources.Finance.Refund;
                            report.CashPaymentSum = finanaceDocument.SumToPayForInvoice;
                        }

                        model.Reports.Add(report);
                    }
                }
            }

            return View(model);
        }

        public ActionResult Divisions(string documentId, string backURL)
        {
            string[] documents = documentId.Split(',');
            var doc = RapidVetUnitOfWork.FinanceDocumentReposetory.GetItem(int.Parse(documents[0]));
            if (doc != null && doc.ClinicId == CurrentUser.ActiveClinicId && (doc.FinanceDocumentType == FinanceDocumentType.Invoice || doc.FinanceDocumentType == FinanceDocumentType.InvoiceReceipt || doc.FinanceDocumentType == FinanceDocumentType.Receipt || doc.FinanceDocumentType == FinanceDocumentType.Refound))
            {
                if (!String.IsNullOrEmpty(backURL))
                    Session["RedirectToURL"] = backURL;

                    return View();
            }
            var savePath = System.Configuration.ConfigurationManager.AppSettings["LOG_PATH"];
            if (savePath != null)
            {
                BaseLogger.LogWriter.WriteMsg("Devision failed - doc.ClinicId =" + doc.ClinicId + " CurrentUser.ActiveClinicId = " + CurrentUser.ActiveClinicId + " doc.FinanceDocumentType=" + doc.FinanceDocumentType);
            }
            return View("Error");
        }


        public JsonResult SaveDivisions(string divisionsString, string documentIds)
        {
            var jsonResult = new JsonResult() { Data = 0, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            var jss = new JavaScriptSerializer();
            var divisionsList = jss.Deserialize<List<FinanceDocumentDividision>>(divisionsString);
            if (divisionsList == null || !divisionsList.Any())
            {
                return jsonResult;
            }
            // var docId = divisionsList.First().DocumentId;
            string[] documents = documentIds.Split(',');
            var doc1 = RapidVetUnitOfWork.FinanceDocumentReposetory.GetItem(int.Parse(documents[0]));
            FinanceDocument doc2 = null;
            OperationStatus status;

            if (documents.Length > 1)
            {
                doc2 = RapidVetUnitOfWork.FinanceDocumentReposetory.GetItem(int.Parse(documents[1]));
            }

            if (doc1 == null)
            {
                return jsonResult;
            }

            var sumAmount = doc1.TotalSum;
            if (doc2 != null)
            {
                var divisionsListForDoc2 = jss.Deserialize<List<FinanceDocumentDividision>>(divisionsString);
                sumAmount += doc2.TotalSum;

                decimal percent = 0;
                if (sumAmount != 0)
                {
                    for (int i = 0; i < divisionsList.Count; i++)
                    {
                        //doc1 divisions
                        percent = divisionsList[i].Amount / sumAmount;
                        divisionsList[i].Amount = decimal.Round(doc1.TotalSum * percent, 2, MidpointRounding.AwayFromZero);
                        //doc2 divisions
                        percent = divisionsListForDoc2[i].Amount / sumAmount;
                        divisionsListForDoc2[i].Amount = decimal.Round(doc2.TotalSum * percent, 2, MidpointRounding.AwayFromZero);
                        divisionsListForDoc2[i].Document = doc2;
                        divisionsListForDoc2[i].DocumentId = doc2.Id;
                    }
                }

                status = RapidVetUnitOfWork.FinanceDocumentReposetory.SaveDivisions(doc1.Id, divisionsList);
                if (status.Success)
                {
                    status = RapidVetUnitOfWork.FinanceDocumentReposetory.SaveDivisions(doc2.Id, divisionsListForDoc2);
                }
                else
                {
                    SetErrorMessage();
                }
            }
            else
            {
                status = RapidVetUnitOfWork.FinanceDocumentReposetory.SaveDivisions(doc1.Id, divisionsList);
            }



            if (status.Success)
            {
                SetSuccessMessage("החלוקה נשמרה בהצלחה");
                var rtURL = Session["RedirectToURL"];
                Session["RedirectToURL"] = null;
                if (rtURL != null && !String.IsNullOrEmpty(rtURL.ToString()))
                {
                    jsonResult.Data = rtURL.ToString();
                    return jsonResult;
                }
            }
            else
            {
                SetErrorMessage();
            }

            jsonResult.Data = doc1.ClientId;
            return jsonResult;
        }

        public JsonResult GetDivisions(string docId, bool divisionsExists = false)
        {
            string[] documents = docId.Split(',');
            int documentId = int.Parse(documents[0]);
            var doc = RapidVetUnitOfWork.FinanceDocumentReposetory.GetItem(documentId);
            FinanceDocument reciept = null;

            if (documents.Length > 1) // there's also a reciept
            {
                reciept = RapidVetUnitOfWork.FinanceDocumentReposetory.GetItem(int.Parse(documents[1]));
            }
            // divisions exists, gets them and returns as json
            if (divisionsExists)
            {
                var existingDivideList = RapidVetUnitOfWork.FinanceDocumentReposetory.GetDevisions(documentId);
                if (existingDivideList.Any())
                {
                    existingDivideList = existingDivideList.Select(d => new FinanceDocumentDividision()
                        {
                            DoctorName = d.Doctor.Name,
                            DocumentId = documentId,
                            Amount = d.Amount,
                            DoctorId = d.Doctor.Id
                        }).ToList();
                    return Json(existingDivideList, JsonRequestBehavior.AllowGet);
                }
            }

            // divisions don't exist, create them
            var divideList = RapidVetUnitOfWork.FinanceDocumentReposetory
                    .CreateDivisionsForDocument(CurrentUser.ActiveClinicId, documentId, CurrentUser.ActiveClinic.ArrangeIncomeById, CurrentUser.IsDoctor, CurrentUser.Id);

            if (reciept != null)
            {
                divideList[0].Amount += reciept.TotalSum;
            }

            return Json(divideList, JsonRequestBehavior.AllowGet);

        }
    }
}
