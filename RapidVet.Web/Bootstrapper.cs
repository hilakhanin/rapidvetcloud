﻿using System;
using System.Linq;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using RapidVet.Helpers;
using RapidVet.Model;
using RapidVet.Model.Archives;
using RapidVet.Model.Clients;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Clinics;
using RapidVet.Model.Expenses;
using RapidVet.Model.Finance;
using RapidVet.Model.FullCalender;
using RapidVet.Model.CliinicInventory;
using RapidVet.Model.PatientLabTests;
using RapidVet.Model.Patients;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Quotations;
using RapidVet.Model.Visits;
using RapidVet.Repository.Clients;
using RapidVet.Repository.Finances;
using RapidVet.Repository.Patients;
using RapidVet.Repository.Visits;
using RapidVet.WebModels.ClinicTasks;
using RapidVet.WebModels.DangerousDrugs;
using RapidVet.WebModels.DischargePapers;
using RapidVet.WebModels.Expenses;
using RapidVet.WebModels.Finance.Invoice;
using RapidVet.WebModels.Finance.Reports;
using RapidVet.WebModels.GeneralReports;
using RapidVet.WebModels.Inventory;
using RapidVet.WebModels.MedicalProcedures;
using RapidVet.WebModels.Patients;
using RapidVet.WebModels.PreventiveMedicine;
using RapidVet.WebModels.Referrals;
using RapidVet.WebModels.Users;
using RapidVet.WebModels.Visits;
using RapidVetMembership;
using Unity.Mvc3;
using RapidVet.Repository;
using RapidVet.Repository.Clinics;
using RapidVet.WebModels.Clinics;
using RapidVet.Enums;
using RapidVet.Model.Tools;
using RapidVet.Resources.Enums.Finances;

namespace RapidVet.Web
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();
            BuildMap();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            //clinic group
            container.RegisterType<IClinicGroupRepository, ClinicGroupRepository>();


            //clinic
            container.RegisterType<IClinicRepository, ClinicRepository>();
            container.RegisterType<IDiagnosisRepository, DiagnosisRepository>();
            //container.RegisterType<ITreatmentRepository, TreatmentRepository>();
            container.RegisterType<IIssuerRepository, IssuerRepository>();


            //finance
            container.RegisterType<ITariffRepository, TariffRepository>();
            container.RegisterType<IPriceListRepository, PriceListRepository>();
            container.RegisterType<IPriceListCategoryRepository, PriceListCategoryRepository>();
            container.RegisterType<IPriceListItemRepository, PriceListItemRepository>();


            //clients
            container.RegisterType<IClientRepository, ClientRepository>();
            container.RegisterType<IClientStatusRepository, ClientStatusRepository>();


            //Patient
            container.RegisterType<IAnimalKindRepository, AnimalKindRepository>();
            container.RegisterType<IAnimalColorRepository, AnimalColorRepository>();
            container.RegisterType<IAnimalRaceRepository, AnimalRaceRepository>();
            container.RegisterType<IPatientRepository, PatientRepository>();

            //misc
            container.RegisterType<IFileUploader, FileUploader>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IFormsAuthenticationService, FormsAuthenticationService>();

            return container;
        }

        private static void BuildMap()
        {

            //Clinics
            AutoMapper.Mapper.CreateMap<Model.Clinics.Clinic, WebModels.Clinics.ClinicEdit>();
            AutoMapper.Mapper.CreateMap<WebModels.Clinics.ClinicEdit, Model.Clinics.Clinic>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.ClinicGroupID, o => o.Ignore())
                      .ForMember(d => d.Issuers, o => o.Ignore())
                      .ForMember(d => d.PriceListCategories, o => o.Ignore())
                      .ForMember(d => d.Tariffs, o => o.Ignore())
                      .ForMember(d => d.TreatmentPackages, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Clinics.Clinic, WebModels.Clinics.FinanceClinicEdit>();
            AutoMapper.Mapper.CreateMap<WebModels.Clinics.FinanceClinicEdit, Model.Clinics.Clinic>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.ClinicGroupID, o => o.Ignore())
                      .ForMember(d => d.Issuers, o => o.Ignore())
                      .ForMember(d => d.PriceListCategories, o => o.Ignore())
                      .ForMember(d => d.Tariffs, o => o.Ignore())
                      .ForMember(d => d.TreatmentPackages, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Clinics.ClinicGroup, WebModels.ClinicGroups.ClinicGroupCreate>();
            AutoMapper.Mapper.CreateMap<WebModels.ClinicGroups.ClinicGroupCreate, Model.Clinics.ClinicGroup>();

            AutoMapper.Mapper.CreateMap<Model.Clinics.ClinicGroup, WebModels.ClinicGroups.ClinicGroupEdit>()
                .ForMember(d => d.ExpirationDate, s => s.MapFrom(c => c.ExpirationDate.HasValue ? c.ExpirationDate.Value.ToShortDateString() : ""));
            AutoMapper.Mapper.CreateMap<WebModels.ClinicGroups.ClinicGroupEdit, Model.Clinics.ClinicGroup>()
                .ForMember(d => d.CalenderEntrySmsTemplate, s => s.Ignore())
                .ForMember(d => d.CalenderEntryEmailSubject, s => s.Ignore())
                .ForMember(d => d.CalenderEntryEmailTemplate, s => s.Ignore())
                .ForMember(d => d.PreventiveMedicineSmsTemplate, s => s.Ignore())
                 .ForMember(d => d.PreventiveMedicineSmsMultiAnimalsTemplate, s => s.Ignore())
                .ForMember(d => d.PreventiveMedicineLetterTemplate, s => s.Ignore())
                .ForMember(d => d.PreventiveMedicineLetterMultiAnimalsTemplate, s => s.Ignore())
                .ForMember(d => d.PreventiveMedicinePostCardTemplate, s => s.Ignore())
                .ForMember(d => d.PreventiveMedicinePostCardMultiAnimalsTemplate, s => s.Ignore())
                .ForMember(d => d.PreventiveMedicineStikerTemplate, s => s.Ignore())
                .ForMember(d => d.PreventiveMedicineEmailSubject, s => s.Ignore())
                .ForMember(d => d.PreventiveMedicineEmailTemplate, s => s.Ignore())
                .ForMember(d => d.PreventiveMedicineEmailMultiAnimalsSubject, s => s.Ignore())
                .ForMember(d => d.PreventiveMedicineEmailMultiAnimalsTemplate, s => s.Ignore())
                .ForMember(d => d.BirthdayStickerTemplate, s => s.Ignore())
                .ForMember(d => d.BirthdayEmailSubject, s => s.Ignore())
                .ForMember(d => d.BirthdayEmailTemplate, s => s.Ignore())
                .ForMember(d => d.BirthdaySmsTemplate, s => s.Ignore())
                .ForMember(d => d.TreatmentStikerTemplate, s => s.Ignore())
                .ForMember(d => d.TreatmentEmailSubject, s => s.Ignore())
                .ForMember(d => d.TreatmentEmailTemplate, s => s.Ignore())
                .ForMember(d => d.TreatmentSmsTemplate, s => s.Ignore())
                .ForMember(d => d.ClientsStikerTemplate, s => s.Ignore())
                .ForMember(d => d.ClientsEmailSubject, s => s.Ignore())
                .ForMember(d => d.ClientsEmailTemplate, s => s.Ignore())
                .ForMember(d => d.ClientsSmsTemplate, s => s.Ignore())
                .ForMember(d => d.FollowUpsEmailSubject, s => s.Ignore())
                .ForMember(d => d.FollowUpsEmailTemplate, s => s.Ignore())
                .ForMember(d => d.FollowUpsSmsTemplate, s => s.Ignore())
                .ForMember(d => d.HasLogo, s => s.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Clinics.Clinic, WebModels.Clinics.ClinicCreate>();
            AutoMapper.Mapper.CreateMap<WebModels.Clinics.ClinicCreate, Model.Clinics.Clinic>()
                      .ForMember(dst => dst.Active, opt => opt.UseValue(true))
                      .ForMember(dst => dst.CreatedDate, opt => opt.UseValue(DateTime.Now))
                      .ForMember(dst => dst.Id, opt => opt.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Clinics.Clinic, WebModels.Clinics.ClinicEdit>();
            AutoMapper.Mapper.CreateMap<WebModels.Clinics.ClinicEdit, Model.Clinics.Clinic>();

            AutoMapper.Mapper.CreateMap<Model.Clinics.Issuer, WebModels.Clinics.IssuerCreate>();
            AutoMapper.Mapper.CreateMap<WebModels.Clinics.IssuerCreate, Model.Clinics.Issuer>()
                      .ForMember(dst => dst.Id, opt => opt.Ignore())
                      .ForMember(dst => dst.CreatedDate, opt => opt.UseValue(DateTime.Now));

            AutoMapper.Mapper.CreateMap<Issuer, SelectListItem>()
                      .ForMember(d => d.Value, o => o.MapFrom(i => i.Id.ToString()))
                      .ForMember(d => d.Text, o => o.MapFrom(i => i.Name));

            AutoMapper.Mapper.CreateMap<City, SelectListItem>()
                     .ForMember(d => d.Value, o => o.MapFrom(i => i.Id.ToString()))
                     .ForMember(d => d.Text, o => o.MapFrom(i => i.Name));

            AutoMapper.Mapper.CreateMap<LetterTemplate, SelectListItem>()
                .ForMember(d => d.Text, o => o.MapFrom(s => s.TemplateName))
                .ForMember(d => d.Value, o => o.MapFrom(s => s.Id.ToString()));

            AutoMapper.Mapper.CreateMap<Model.Clinics.Clinic, WebModels.Finance.DuplicatePricesClinicModel>()
                      .ForMember(dest => dest.ClinicId, opt => opt.MapFrom(c => c.Id))
                      .ForMember(dest => dest.ClinicName, opt => opt.MapFrom(c => c.Name));

            AutoMapper.Mapper.CreateMap<Model.Clinics.TelephoneDirectory, WebModels.Clinics.TelephoneDirectoryEdit>();
            AutoMapper.Mapper.CreateMap<WebModels.Clinics.TelephoneDirectoryEdit, Model.Clinics.TelephoneDirectory>();

            AutoMapper.Mapper.CreateMap<Model.Clinics.VaccineOrTreatment, WebModels.VaccinesAndTreatments.VaccineOrTreatmentModel>();
            AutoMapper.Mapper.CreateMap<WebModels.VaccinesAndTreatments.VaccineOrTreatmentModel, Model.Clinics.VaccineOrTreatment>();

            AutoMapper.Mapper.CreateMap<Model.Clinics.NextVaccineOrTreatment, WebModels.VaccinesAndTreatments.NextVaccineOrTreatmentModel>();
            AutoMapper.Mapper.CreateMap<WebModels.VaccinesAndTreatments.NextVaccineOrTreatmentModel, Model.Clinics.NextVaccineOrTreatment>();

            AutoMapper.Mapper.CreateMap<Model.Clinics.LabTestType, WebModels.LabTests.LabTestTypeEdit>();
            AutoMapper.Mapper.CreateMap<WebModels.LabTests.LabTestTypeEdit, Model.Clinics.LabTestType>();

            AutoMapper.Mapper.CreateMap<Model.Clinics.LabTestTemplate, WebModels.LabTests.LabTestTemplateEdit>()
                .ForMember(d => d.LabTestTypeName, o => o.MapFrom(s => s.LabTestType.Name));
            AutoMapper.Mapper.CreateMap<WebModels.LabTests.LabTestTemplateEdit, Model.Clinics.LabTestTemplate>();

            AutoMapper.Mapper.CreateMap<Model.Clinics.LabTestTemplate, WebModels.LabTests.LabTestTemplatesWebModel>();

            AutoMapper.Mapper.CreateMap<LabTestType, SelectListItem>()
                      .ForMember(d => d.Text, o => o.MapFrom(s => s.Name))
                      .ForMember(d => d.Value, o => o.MapFrom(s => s.Id.ToString()));

            AutoMapper.Mapper.CreateMap<Model.Clinics.AttendanceLog, WebModels.Clinics.AttendanceLogWebModel>()
                .ForMember(l => l.UserName, o => o.MapFrom(a => a.User.Name));


            AutoMapper.Mapper.CreateMap<TaxRate, WebModels.Clinics.TaxRateWebModel>()
                      .ForMember(d => d.Date, o => o.MapFrom(r => r.TaxRateChangedDate.ToShortDateString()))
                      .ForMember(d => d.DateTime, o => o.MapFrom(r => r.TaxRateChangedDate));

            AutoMapper.Mapper.CreateMap<WebModels.Clinics.TaxRateWebModel, TaxRate>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.ClinicId, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<AdvancedPaymentsPercent, WebModels.Clinics.AdvancedPaymentsPercentWebModel>()
                      .ForMember(d => d.Date, o => o.MapFrom(r => r.PercentChangedDate.ToShortDateString()))
                      .ForMember(d => d.DateTime, o => o.MapFrom(r => r.PercentChangedDate));

            AutoMapper.Mapper.CreateMap<WebModels.Clinics.AdvancedPaymentsPercentWebModel, AdvancedPaymentsPercent>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.ClinicId, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<ClinicBackgroundJob, WebModels.Tools.ClinicBackgroundJobWebModel>()
                     .ForMember(d => d.InitiatingUserName, o => o.MapFrom(s => s.InitiatingUser != null ? s.InitiatingUser.Name : ""))
                     .ForMember(d => d.CancellingUserName, o => o.MapFrom(s => s.CancellingUser != null ? s.CancellingUser.Name : ""));

            //Finance
            AutoMapper.Mapper.CreateMap<Model.Finance.PriceListCategory, WebModels.Finance.PriceListCategoryModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Finance.PriceListCategoryModel, Model.Finance.PriceListCategory>();

            AutoMapper.Mapper.CreateMap<Model.Finance.PriceListCategory, WebModels.Finance.SimpleCategoryModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Finance.SimpleCategoryModel, Model.Finance.PriceListCategory>();

            AutoMapper.Mapper.CreateMap<Model.Finance.Tariff, WebModels.Finance.TariffModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Finance.TariffModel, Model.Finance.Tariff>();

            AutoMapper.Mapper.CreateMap<Model.Finance.Tariff, WebModels.Finance.GridTariffModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Finance.GridTariffModel, Model.Finance.Tariff>();

            AutoMapper.Mapper.CreateMap<Model.Finance.PriceListItem, WebModels.Finance.PriceListItemModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Finance.PriceListItemModel, Model.Finance.PriceListItem>();

            AutoMapper.Mapper.CreateMap<Model.Finance.PriceListItemTariff, WebModels.Finance.ItemTariffGridModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Finance.ItemTariffGridModel, Model.Finance.PriceListItemTariff>()
                      .ForMember(d => d.Price, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Finance.PriceListItemType, WebModels.Finance.PriceListItemTypeModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Finance.PriceListItemTypeModel, Model.Finance.PriceListItemType>()
                      .ForMember(dest => dest.Items, opt => opt.Ignore());

            AutoMapper.Mapper.CreateMap<PriceListItem, SelectListItem>()
                      .ForMember(d => d.Text, o => o.MapFrom(p => p.Name))
                      .ForMember(d => d.Value, o => o.MapFrom(p => p.Id.ToString()));

            AutoMapper.Mapper.CreateMap<PriceListCategory, SelectListItem>()
                      .ForMember(d => d.Text, o => o.MapFrom(p => p.Name))
                      .ForMember(d => d.Value, o => o.MapFrom(p => p.Id.ToString()));




            //Finance > Visit
            AutoMapper.Mapper.CreateMap<Model.Finance.PriceListItem, WebModels.Finance.ItemJsonModel>()
                      .ForMember(dest => dest.ClinicId, opt => opt.MapFrom(i => i.Category.ClinicId))
                      .ForMember(dest => dest.Price, opt => opt.MapFrom(i => i.ItemsTariffs.FirstOrDefault().Price));

            AutoMapper.Mapper.CreateMap<Model.Finance.PriceListCategory, WebModels.Finance.CategoryJsonModel>()
                      .ForMember(dest => dest.Items, opt => opt.MapFrom(c => c.PriceListItems));



            //Patients
            AutoMapper.Mapper.CreateMap<Patient, SelectListItem>()
                      .ForMember(d => d.Text, o => o.MapFrom(p => p.Name))
                      .ForMember(d => d.Value, o => o.MapFrom(p => p.Id.ToString()));

            AutoMapper.Mapper.CreateMap<Model.Patients.AnimalColor, WebModels.Patients.AnimalColorModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Patients.AnimalColorModel, Model.Patients.AnimalColor>()
                      .ForMember(dest => dest.ClinicGroup, opt => opt.Ignore())
                      .ForMember(dest => dest.Patients, opt => opt.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Patients.AnimalKind, WebModels.Patients.AnimalKindModel>()
                      .ForMember(d => d.AnimalIconFileName, s => s.MapFrom(f => f.AnimalIcon.FileName));

            AutoMapper.Mapper.CreateMap<WebModels.Patients.AnimalKindModel, Model.Patients.AnimalKind>()
                      .ForMember(dest => dest.AnimalRaces, source => source.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Patients.AnimalRace, WebModels.Patients.AnimalRaceModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Patients.AnimalRaceModel, Model.Patients.AnimalRace>();


            AutoMapper.Mapper.CreateMap<Model.Patients.Patient, WebModels.Patients.PatientCreateModel>()
                .ForMember(d => d.AnimalKind, s => s.MapFrom(p => p.AnimalRace.AnimalKind));

            AutoMapper.Mapper.CreateMap<WebModels.Patients.PatientCreateModel, Model.Patients.Patient>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.Client, o => o.Ignore())
                      .ForMember(d => d.Archives, o => o.Ignore())
                      .ForMember(d => d.DischargePapers, o => o.Ignore())
                      .ForMember(d => d.LabTests, o => o.Ignore())
                      .ForMember(d => d.MedicalProcedures, o => o.Ignore())
                      .ForMember(d => d.MinistryOfAgricultureReports, o => o.Ignore())
                      .ForMember(d => d.PreventiveMedicine, o => o.Ignore())
                      .ForMember(d => d.Quotations, o => o.Ignore())
                      .ForMember(d => d.Comments, o => o.Ignore())
                      .ForMember(d => d.BirthDate, o => o.Ignore());


            AutoMapper.Mapper.CreateMap<Model.Patients.PatientComment, WebModels.Patients.PatientCommentModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Patients.PatientCommentModel, Model.Patients.PatientComment>()
                      .ForMember(dest => dest.PatientId, opt => opt.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Patients.Patient, WebModels.Patients.PatientIndexModel>();
            AutoMapper.Mapper.CreateMap<Model.Patients.Patient, WebModels.Patients.PatientIndexListModel>()
                      .ForMember(dest => dest.AnimalIconFileName,
                                 opt => opt.MapFrom(p => p.AnimalRace.AnimalKind.AnimalIcon.FileName))
                      .ForMember(dest => dest.ClinicId, opt => opt.MapFrom(p => p.Client.ClinicId));


            //Visits
            AutoMapper.Mapper.CreateMap<Model.Visits.Diagnosis, WebModels.Visits.DiagnosisAdminModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Visits.DiagnosisAdminModel, Model.Visits.Diagnosis>();

            AutoMapper.Mapper.CreateMap<Model.Visits.VisitTreatment, WebModels.Visits.VisitCollectionsModel>();

            AutoMapper.Mapper.CreateMap<Model.Visits.VisitMedication, WebModels.Visits.VisitMedicationModel>()
                .ForMember(dst => dst.MedicationAdministrationTypeId, opt => opt.MapFrom(src => src.DrugAdministrationId))
                .ForMember(dst => dst.MedicationAdministrationTypeName, opt => opt.MapFrom(src => ((MedicationAdministrationType)src.DrugAdministrationId).ToString()))
                .ForMember(dst => dst.Insturctions, opt => opt.MapFrom(src => src.Medication.Insturctions));

            AutoMapper.Mapper.CreateMap<Model.Visits.Medication, WebModels.Visits.MedicationJsonModel>();
            AutoMapper.Mapper.CreateMap<Medication, WebModels.Medications.MedicationJsonModel>();

            AutoMapper.Mapper.CreateMap<Medication, MedicationOption>().ForMember(dst => dst.MedicationId, opt => opt.MapFrom(src => src.Id));
            AutoMapper.Mapper.CreateMap<VisitMedicationModel, VisitMedication>()
                 .ForMember(d => d.Discount, o => o.MapFrom(s => s.Discount.HasValue ? s.Discount.Value : 0))
                 .ForMember(d => d.DiscountPercent, o => o.MapFrom(s => s.DiscountPercent.HasValue ? s.DiscountPercent.Value : 0))
                 .ForMember(dst => dst.DrugAdministrationId, opt => opt.MapFrom(src => src.MedicationAdministrationTypeId))
                 .ForMember(dst => dst.Instructions, opt => opt.MapFrom(src => src.Insturctions));

            AutoMapper.Mapper.CreateMap<Model.Visits.Visit, WebModels.Visits.VisitModel>()
                      .ForMember(d => d.Date, o => o.MapFrom(v => v.VisitDate.ToShortDateString()))
                      .ForMember(d => d.Time, o => o.MapFrom(v => v.VisitDate.ToShortTimeString()))
                      .ForMember(d => d.VisitTreatments, o => o.MapFrom(v => v.Treatments))
                      .ForMember(d => d.VisitExaminations, o => o.MapFrom(v => v.Examinations))
                      .ForMember(d => d.VisitDiagnoses, o => o.MapFrom(v => v.Diagnoses))
                      .ForMember(d => d.VisitMedications, o => o.MapFrom(v => v.Medications))
                      .ForMember(d => d.DangerousDrugs, o => o.MapFrom(v => v.DangerousDrugs))
                      .ForMember(d => d.SelectedDoctorId, o => o.MapFrom(v => v.DoctorId))
                      .ForMember(d => d.HR, o => o.MapFrom(v => v.Pulse))
                      .ForMember(d => d.BCS, o => o.MapFrom(v => v.BCS))
                      .ForMember(d => d.TotalPrice, o => o.MapFrom(v => v.Price.HasValue ? v.Price.Value : (decimal)0));

            AutoMapper.Mapper.CreateMap<WebModels.Visits.VisitModel, Model.Visits.Visit>()
                      .ForMember(d => d.Treatments, o => o.MapFrom(vd => vd.VisitTreatments))
                      .ForMember(d => d.Examinations, o => o.MapFrom(vd => vd.VisitExaminations))
                      .ForMember(d => d.Diagnoses, o => o.MapFrom(vd => vd.VisitDiagnoses))
                      .ForMember(d => d.Medications, o => o.MapFrom(vd => vd.VisitMedications))
                      .ForMember(d => d.DangerousDrugs, o => o.MapFrom(vd => vd.DangerousDrugs))
                      .ForMember(d => d.DoctorId, o => o.MapFrom(vd => vd.SelectedDoctorId))
                      .ForMember(d => d.MainComplaint, o => o.MapFrom(vd => vd.MainComplaint))
                      .ForMember(d => d.Pulse, o => o.MapFrom(vd => vd.HR))
                      .ForMember(d => d.BCS, o => o.MapFrom(vd => vd.BCS))
                      .ForMember(d => d.Price, o => o.MapFrom(vd => vd.TotalPrice));



            AutoMapper.Mapper.CreateMap<Model.Visits.VisitTreatment, WebModels.Visits.VisitChildItem>();
            AutoMapper.Mapper.CreateMap<Model.Visits.VisitExamination, WebModels.Visits.VisitChildItem>();
            AutoMapper.Mapper.CreateMap<Model.Visits.VisitDiagnosis, WebModels.Visits.VisitChildItem>()
                .ForMember(d => d.DiagnosisId, o => o.MapFrom(vd => vd.DiagnosisId.HasValue ? vd.DiagnosisId.Value : 0));

            AutoMapper.Mapper.CreateMap<PreventiveMedicineItem, WebModels.Visits.VisitChildItem>()
                      .ForMember(d => d.Name, o => o.MapFrom(pmi => pmi.PriceListItem.Name))
                      .ForMember(d => d.CategoryName, o => o.MapFrom(pmi => pmi.PriceListItem.Category.Name))
                      .ForMember(d => d.Quantity, o => o.MapFrom(pmi => pmi.Quantity))
                      .ForMember(d => d.Discount, o => o.MapFrom(pmi => pmi.Discount))
                      .ForMember(d => d.UnitPrice, o => o.MapFrom(pmi => pmi.Price));


            AutoMapper.Mapper.CreateMap<Model.Visits.DangerousDrug, WebModels.Visits.DangerousDrugWebModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Visits.DangerousDrugWebModel, Model.Visits.DangerousDrug>();



            AutoMapper.Mapper.CreateMap<WebModels.Visits.VisitChildItem, Model.Visits.VisitTreatment>()
                      .ForMember(d => d.Discount, o => o.MapFrom(s => s.Discount.HasValue ? s.Discount.Value : 0))
                      .ForMember(d => d.DiscountPercent, o => o.MapFrom(s => s.DiscountPercent.HasValue ? s.DiscountPercent.Value : 0))
                      .ForMember(d => d.PriceListItemId,
                                 o => o.MapFrom(vci => vci.PriceListItemId == 0 ? null : vci.PriceListItemId));

            AutoMapper.Mapper.CreateMap<WebModels.Visits.VisitChildItem, Model.Visits.VisitExamination>()
                      .ForMember(d => d.Discount, o => o.MapFrom(s => s.Discount.HasValue ? s.Discount.Value : 0))
                      .ForMember(d => d.DiscountPercent, o => o.MapFrom(s => s.DiscountPercent.HasValue ? s.DiscountPercent.Value : 0))
                      .ForMember(d => d.PriceListItemId,
                                 o => o.MapFrom(vci => vci.PriceListItemId == 0 ? null : vci.PriceListItemId));

            AutoMapper.Mapper.CreateMap<WebModels.Visits.VisitChildItem, Model.Visits.VisitDiagnosis>()
                      .ForMember(d => d.DiagnosisId,
                                 o => o.MapFrom(vci => vci.DiagnosisId == 0 ? null : vci.DiagnosisId));

            AutoMapper.Mapper.CreateMap<Model.Clinics.Clinic, WebModels.Visits.PrescriptionModel>()
                      .ForMember(d => d.ClinicName, o => o.MapFrom(c => c.Name))
                      .ForMember(d => d.ClinicPhone, o => o.MapFrom(c => c.Phone))
                      .ForMember(d => d.ClinicFax, o => o.MapFrom(c => c.Fax))
                      .ForMember(d => d.ClinicAddress, o => o.MapFrom(c => c.Address))
                      .ForMember(d => d.ClinicEmail, o => o.MapFrom(c => c.Email));

            AutoMapper.Mapper.CreateMap<VisitFilter, SelectListItem>()
                   .ForMember(d => d.Text, o => o.MapFrom(u => u.Name))
                   .ForMember(d => d.Value, o => o.MapFrom(p => p.Id.ToString()));

            AutoMapper.Mapper.CreateMap<VisitFilter, VisitFilter>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.ClinicId, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<VisitFilterWebModel, VisitFilter>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.ClinicId, o => o.Ignore())
                      .ForMember(d => d.FromDate, o => o.Ignore())
                      .ForMember(d => d.ToDate, o => o.Ignore());

            AutoMapper.Mapper
                      .CreateMap<VisitFilter, VisitFilterWebModel>()
                      .ForMember(d => d.FromDate,
                                 o =>
                                 o.MapFrom(
                                     f =>
                                     f.FromDate.HasValue && f.FromDate.Value > DateTime.MinValue
                                         ? f.FromDate.Value.ToString("dd/MM/yyyy")
                                         : string.Empty))
                      .ForMember(d => d.ToDate,
                                 o =>
                                 o.MapFrom(
                                     f =>
                                     f.ToDate.HasValue && f.ToDate.Value > DateTime.MinValue
                                         ? f.ToDate.Value.ToString("dd/MM/yyyy")
                                         : string.Empty));



            //clients
            AutoMapper.Mapper.CreateMap<Model.Clients.Client, WebModels.Clients.ClientIndexModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Clients.ClientIndexModel, Model.Clients.Client>();

            AutoMapper.Mapper.CreateMap<Model.Clients.Client, WebModels.Clients.ClientCreateModel>();

            AutoMapper.Mapper.CreateMap<WebModels.Clients.ClientCreateModel, Model.Clients.Client>()
                      .ForMember(dest => dest.Id, opt => opt.Ignore())
                      .ForMember(dest => dest.Emails, opt => opt.Ignore())
                      .ForMember(dest => dest.Addresses, opt => opt.Ignore())
                      .ForMember(dest => dest.Phones, opt => opt.Ignore())
                      .ForMember(dest => dest.Patients, opt => opt.Ignore())
                      .ForMember(dest => dest.ClinicId, opt => opt.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Clients.Client, WebModels.Clients.ClientDetailsModel>()
                      .ForMember(dest => dest.StatusId, opt => opt.MapFrom(c => c.StatusId))
                      .ForMember(dest => dest.Status, opt => opt.MapFrom(c => c.ClientStatus));

            AutoMapper.Mapper.CreateMap<WebModels.Clients.ClientDetailsModel, Model.Clients.Client>()
                      .ForMember(dest => dest.ClinicId, opt => opt.Ignore())
                      .ForMember(dest => dest.Emails, opt => opt.Ignore())
                      .ForMember(dest => dest.Patients, opt => opt.Ignore())
                      .ForMember(dest => dest.Phones, opt => opt.Ignore())
                      .ForMember(dest => dest.StatusId, opt => opt.Ignore())
                      .ForMember(dest => dest.Addresses, opt => opt.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Clients.Details.Phone, WebModels.Clients.Details.PhoneModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Clients.Details.PhoneModel, Model.Clients.Details.Phone>();

            AutoMapper.Mapper.CreateMap<Model.Clients.Details.PhoneType, WebModels.Clients.Details.PhoneTypeModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Clients.Details.PhoneTypeModel, Model.Clients.Details.PhoneType>();


            AutoMapper.Mapper.CreateMap<Model.Clients.Details.Email, WebModels.Clients.Details.EmailModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Clients.Details.EmailModel, Model.Clients.Details.Email>();


            AutoMapper.Mapper.CreateMap<Model.Clients.Details.Address, WebModels.Clients.Details.AddressModel>()
                .ForMember(dest => dest.CityName, o => o.MapFrom(ct => ct.City.Name));

            AutoMapper.Mapper.CreateMap<WebModels.Clients.Details.AddressModel, Model.Clients.Details.Address>();

            AutoMapper.Mapper.CreateMap<Model.Clients.ClientStatus, WebModels.Clients.ClientStatusModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Clients.ClientStatusModel, Model.Clients.ClientStatus>()
                      .ForMember(dest => dest.Clients, opt => opt.Ignore())
                      .ForMember(dest => dest.ClinicGroupId, opt => opt.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Clients.Title, WebModels.Clients.TitleModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Clients.TitleModel, Model.Clients.Title>();


            AutoMapper.Mapper.CreateMap<Model.Clients.Client, SelectListItem>()
                      .ForMember(d => d.Text, o => o.MapFrom(c => c.NameWithPatients))
                      .ForMember(d => d.Value, o => o.MapFrom(c => c.Id.ToString()));

            AutoMapper.Mapper.CreateMap<City, City>()
                   .ForMember(r => r.Id, o => o.Ignore())
                   .ForMember(r => r.Active, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<Client, WebModels.Clients.AdvancedSearchWebModel>()
                      .ForMember(d => d.ClientId, o => o.MapFrom(c => c.Id))
                      .ForMember(d => d.AllPatientNames, o => o.MapFrom(c => c.PatientNames));


            AutoMapper.Mapper.CreateMap<Patient, WebModels.Clients.AdvancedSearchWebModel>()
                     .ForMember(d => d.FirstName, o => o.MapFrom(c => c.Client.FirstName))
                     .ForMember(d => d.LastName, o => o.MapFrom(c => c.Client.LastName))
                     .ForMember(d => d.IdCard, o => o.MapFrom(c => c.Client.IdCard))
                     .ForMember(d => d.Phone, o => o.MapFrom(c => c.Client.Phones.Take(1).SingleOrDefault(p => p.PhoneTypeId == 1).PhoneNumber))
                     .ForMember(d => d.PatientId, o => o.MapFrom(c => c.Id))
                     .ForMember(d => d.AllPatientNames, o => o.MapFrom(c => c.Name))
                     .ForMember(d => d.AnimalKind, o => o.MapFrom(c => c.AnimalRace.AnimalKind.Value))
                     .ForMember(d => d.Address, o => o.MapFrom(c => c.Client.Addresses.Take(1).SingleOrDefault(p => (p.City != null) || (p.Street != null)).GetStreetAndCity()));

            //Medications
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Visits.Medication, WebModels.Medications.MedicationEditWebModel>();
            AutoMapper.Mapper.CreateMap<WebModels.Medications.MedicationEditWebModel, RapidVet.Model.Visits.Medication>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.Active, o => o.UseValue(true));



            //User
            AutoMapper.Mapper.CreateMap<Model.Users.User, UsersSettingsWebModel>()
                      .ForMember(d => d.LicenseNumber, o => o.MapFrom(s => s.LicenceNumber));
            AutoMapper.Mapper.CreateMap<UsersSettingsWebModel, Model.Users.User>()
                      .ForMember(d => d.LicenceNumber, o => o.MapFrom(s => s.LicenseNumber));

            AutoMapper.Mapper.CreateMap<Model.Users.User, WebModels.Users.UserModel>()
                      .ForMember(dest => dest.TitleId, opt => opt.MapFrom(u => u.TitleId))
                      .ForMember(dest => dest.Password, opt => opt.MapFrom(u => u.HashPassword.ToString()));


            AutoMapper.Mapper.CreateMap<WebModels.Users.UserModel, Model.Users.User>()
                      .ForMember(dest => dest.FirstName, o => o.MapFrom(s => s.FirstName ?? ""))
                      .ForMember(dest => dest.ActiveClinic, opt => opt.Ignore())
                      .ForMember(dest => dest.ActiveClinicGroup, opt => opt.Ignore())
                      .ForMember(dest => dest.ClinicGroup, opt => opt.Ignore())
                      .ForMember(dest => dest.ClinicGroupId, opt => opt.Ignore())
                      .ForMember(dest => dest.Roles, opt => opt.Ignore())
                      .ForMember(dest => dest.UsersClinicsRoleses, opt => opt.Ignore())
                      .ForMember(dest => dest.Password, opt => opt.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Users.User, WebModels.Visits.UserBasicModel>()
                      .ForMember(d => d.Name, o => o.MapFrom(u => u.Name))
                      .ForMember(d => d.Id, o => o.MapFrom(u => u.Id));

            AutoMapper.Mapper.CreateMap<Model.Users.User, SelectListItem>()
                      .ForMember(d => d.Text, o => o.MapFrom(u => u.Name))
                      .ForMember(d => d.Value, o => o.MapFrom(u => u.Id.ToString()));

            //Quotation
            AutoMapper.Mapper.CreateMap<Model.Quotations.Quotation, Model.Quotations.Quotation>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.Name, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<Model.Quotations.QuotationTreatment, Model.Quotations.QuotationTreatment>()
                      .ForMember(d => d.QuotationId, o => o.Ignore())
                      .ForMember(d => d.Quotation, o => o.Ignore())
                      .ForMember(d => d.Id, o => o.Ignore());


            AutoMapper.Mapper.CreateMap<Model.Quotations.Quotation, WebModels.Quotations.QuotationModel>()
                      .ForMember(d => d.PatientName, o => o.MapFrom(q => q.Patient.Name));

            AutoMapper.Mapper.CreateMap<Model.Quotations.Quotation, WebModels.Quotations.QuotationJsonModel>()
                      .ForMember(d => d.TreatmentsDescription, o => o.MapFrom(q => q.GetTreatmentsDescription()))
                      .ForMember(d => d.Created, o => o.MapFrom(q => q.Created.ToShortDateString()))
                      .ForMember(d => d.Updated,
                                 o =>
                                 o.MapFrom(
                                     q =>
                                     q.Updated.HasValue && q.Updated > DateTime.MinValue
                                         ? q.Updated.Value.ToShortDateString()
                                         : string.Empty));


            AutoMapper.Mapper.CreateMap<Model.Quotations.Quotation, WebModels.Quotations.QuotationWebModel>();
            //.ForMember(d => d.PercentDiscount, o => o.MapFrom(q => q.PercentDiscount))
            //.ForMember(d => d.Discount, o => o.MapFrom(q => q.Discount));

            AutoMapper.Mapper.CreateMap<WebModels.Quotations.QuotationWebModel, Model.Quotations.Quotation>()
                      .ForMember(d => d.Treatments, o => o.Ignore())
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.Patient, o => o.Ignore());

            AutoMapper.Mapper
                      .CreateMap<Model.Finance.PriceListItem, WebModels.Finance.PriceListItemSingleTariffFlatModel>()
                      .ForMember(d => d.ItemType, o => o.MapFrom(pli => pli.PriceListItemType.Name));

            AutoMapper.Mapper
                      .CreateMap<Model.Quotations.QuotationTreatment, WebModels.Quotations.QuotationTreatmentModel>()
                      .ForMember(d => d.ItemType, o => o.MapFrom(qt => qt.PriceListItem.PriceListItemType.Name))
                      .ForMember(d => d.Catalog, o => o.MapFrom(qt => qt.PriceListItem.Catalog));

            AutoMapper.Mapper
                    .CreateMap<WebModels.Quotations.QuotationTreatmentModel, Model.Quotations.QuotationTreatment>();


            AutoMapper.Mapper.CreateMap<Model.TreatmentPackages.TreatmentPackage, WebModels.Quotations.TreatmentPackageModel>();

            AutoMapper.Mapper
                      .CreateMap<Model.TreatmentPackages.PackageItem, WebModels.Quotations.QuotationTreatmentModel>()
                      .ForMember(d => d.TreatmentPackageItemId, o => o.MapFrom(pi => pi.Id))
                      .ForMember(d => d.Name, o => o.MapFrom(pi => pi.PriceListItem.Name))
                      .ForMember(d => d.ItemType, o => o.MapFrom(pi => pi.PriceListItem.PriceListItemType.Name))
                      .ForMember(d => d.Amount, o => o.MapFrom(pi => pi.Amount))
                      .ForMember(d => d.Price, o => o.MapFrom(pi => pi.Price))
                      .ForMember(d => d.PriceListItemId, o => o.MapFrom(pi => pi.PriceListItemId))
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.Catalog, o => o.MapFrom(pi => pi.PriceListItem.Catalog));

            AutoMapper.Mapper.CreateMap<Model.TreatmentPackages.PackageItem, Model.TreatmentPackages.PackageItem>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.TreatmentPackageId, o => o.Ignore());

            AutoMapper.Mapper
                      .CreateMap<WebModels.Quotations.QuotationTreatmentModel, Model.TreatmentPackages.PackageItem>()
                      .ForMember(d => d.Amount, o => o.MapFrom(qtl => qtl.Amount))
                      .ForMember(d => d.Price, o => o.MapFrom(qtl => qtl.Price))
                      .ForMember(d => d.Id, o => o.MapFrom(qtl => qtl.PriceListItemId))
                      .ForMember(d => d.PriceListItemId, o => o.MapFrom(qtl => qtl.PriceListItemId))
                      .ForMember(d => d.Comments, o => o.MapFrom(qtl => qtl.Comments))
                      .ForMember(d => d.Id, o => o.MapFrom(qtl => qtl.TreatmentPackageItemId));

            AutoMapper.Mapper.CreateMap<WebModels.Quotations.QuotationTreatmentModel, Model.Visits.VisitTreatment>()
                      .ForMember(d => d.PriceListItemId, o => o.MapFrom(qtl => qtl.PriceListItemId))
                      .ForMember(d => d.Quantity, o => o.MapFrom(qtl => qtl.Amount))
                      .ForMember(d => d.UnitPrice, o => o.MapFrom(qtl => qtl.Price))
                      .ForMember(d => d.Price, o => o.MapFrom(qtl => qtl.TotalAmount))
                      .ForMember(d => d.Name, o => o.MapFrom(qtl => qtl.Name))
                      .ForMember(d => d.QuotationTreatmentId, o => o.MapFrom(qtl => qtl.Id))
                      .ForMember(d => d.Discount, o => o.MapFrom(qtl => (decimal)((qtl.Amount * qtl.Price) - qtl.TotalAmount)));

            AutoMapper.Mapper.CreateMap<Model.Finance.PriceListItem, Model.PreventiveMedicine.PreventiveMedicineItem>()
                      .ForMember(d => d.PriceListItemTypeId, o => o.MapFrom(i => i.ItemTypeId))
                      .ForMember(d => d.PriceListItemId, o => o.MapFrom(i => i.Id));



            AutoMapper.Mapper.CreateMap<Model.Quotations.QuotationTreatment, Model.Visits.VisitTreatment>()
                      .ForMember(d => d.PriceListItemId, o => o.MapFrom(qt => qt.PriceListItemId))
                      .ForMember(d => d.Quantity, o => o.MapFrom(qt => qt.Amount))
                      .ForMember(d => d.UnitPrice, o => o.MapFrom(qt => qt.Price))
                      .ForMember(d => d.Price,
                                 o =>
                                 o.MapFrom(qt => ((decimal)((qt.Amount * qt.Price) - (qt.Discount.HasValue ? qt.Discount.Value : 0)))))
                      .ForMember(d => d.CategoryName, o => o.MapFrom(qt => qt.PriceListItem.Category.Name))
                      .ForMember(d => d.QuotationTreatmentId, o => o.MapFrom(qt => qt.Id));

            //preventive medicine

            AutoMapper.Mapper
                      .CreateMap
                <PreventiveMedicineItem, WebModels.PreventiveMedicine.PreventiveMedicineItemWebModel>()
                      .ForMember(d => d.Id,
                                 o =>
                                 o.MapFrom(
                                     p =>
                                     p.ParentPreventiveItemId.HasValue && p.ParentPreventiveItemId.Value > 0
                                         ? p.ParentPreventiveItemId.Value
                                         : p.Id))
                      .ForMember(d => d.Name, o => o.MapFrom(p => p.PriceListItem.Name))
                      .ForMember(d => d.PriceListItemTypeId, o => o.MapFrom(p => p.PriceListItemTypeId))
                      .ForMember(d => d.Next,
                                 o =>
                                 o.MapFrom(
                                     p =>
                                     p.ParentPreventiveItemId.HasValue && p.ParentPreventiveItemId.Value > 0
                                         ? p.Scheduled
                                         : null))
                      .ForMember(d => d.Preformed,
                                 o =>
                                 o.MapFrom(
                                     p => p.Preformed.HasValue ? p.Preformed : DateTime.MinValue));
            //p.PriceListItemId == p.Parent.PriceListItemId &&
            //p.ParentPreventiveItemId.HasValue &&
            //p.ParentPreventiveItemId.Value > 0
            //    ? p.Parent.Preformed.Value
            //    : DateTime.MinValue));

            AutoMapper.Mapper
                      .CreateMap
                <WebModels.PreventiveMedicine.PreventiveMedicinePreformModel, PreventiveMedicineItem>()
                      .ForMember(d => d.Preformed, o => o.UseValue(DateTime.Now))
                      .ForMember(d => d.Comments, o => o.MapFrom(p => p.Comment))
                      .ForMember(d => d.ExternalyPreformed, o => o.MapFrom(p => p.ExternallyPreformed));


            AutoMapper.Mapper.CreateMap<UserPreventiveMedicineReminderFilter, SelectListItem>()
                      .ForMember(d => d.Text, o => o.MapFrom(u => u.Name))
                      .ForMember(d => d.Value, o => o.MapFrom(p => p.Id.ToString()));

            AutoMapper.Mapper.CreateMap<UserPreventiveMedicineReminderFilter, UserPreventiveMedicineReminderFilter>()
                      .ForMember(d => d.UserId, o => o.Ignore())
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.PreventiveMedicineItems, o => o.Ignore())
                      .ForMember(d => d.ClinicId, o => o.Ignore());

            AutoMapper.Mapper
                      .CreateMap<UserPreventiveMedicineReminderFilter, UserPreventiveMedicineReminderFilterWebModel>()
                      .ForMember(d => d.PriceListItemIds, o => o.Ignore())
                      .ForMember(d => d.FromDateStr,
                                 o =>
                                 o.MapFrom(
                                     f =>
                                     f.FromDate.HasValue && f.FromDate.Value > DateTime.MinValue
                                         ? f.FromDate.Value.ToShortDateString()
                                         : string.Empty))
                      .ForMember(d => d.ToDateStr,
                                 o =>
                                 o.MapFrom(
                                     f =>
                                     f.ToDate.HasValue && f.ToDate.Value > DateTime.MinValue
                                         ? f.ToDate.Value.ToShortDateString()
                                         : string.Empty))
                      .ForMember(d => d.FromReminderDateStr,
                                 o =>
                                 o.MapFrom(
                                     f =>
                                     f.FromReminderDate.HasValue && f.FromReminderDate.Value > DateTime.MinValue
                                         ? f.FromReminderDate.Value.ToShortDateString()
                                         : string.Empty))
                      .ForMember(d => d.ToReminderDateStr,
                                 o =>
                                 o.MapFrom(
                                     f =>
                                     f.ToReminderDate.HasValue && f.ToReminderDate.Value > DateTime.MinValue
                                         ? f.ToReminderDate.Value.ToShortDateString()
                                         : string.Empty));


            AutoMapper.Mapper.CreateMap<PreventiveMedicineItem, PreventiveReminderIndexModel>()
                      .ForMember(d => d.ClientId, o => o.MapFrom(p => p.Patient.ClientId))
                      .ForMember(d => d.ClientName, o => o.MapFrom(p => p.Patient.Client.Name))
                      .ForMember(d => d.PatientName, o => o.MapFrom(p => p.Patient.Name))
                      .ForMember(d => d.PatientKind, o => o.MapFrom(p => p.Patient.AnimalRace.AnimalKind.Value))
                      .ForMember(d => d.ClientStatus,
                                 o =>
                                 o.MapFrom(
                                     p =>
                                     p.Patient.Client.StatusId.HasValue && p.Patient.Client.StatusId.Value > 0
                                         ? p.Patient.Client.ClientStatus.Name
                                         : string.Empty))
                      .ForMember(d => d.ClientEmail,
                                 o =>
                                 o.MapFrom(
                                     p =>
                                     p.Patient.Client.Emails.Any() ? p.Patient.Client.Emails.First().Name : string.Empty))
                      .ForMember(d => d.PriceListItemName, o => o.MapFrom(p => p.PriceListItem.Name))
                      .ForMember(d => d.PriceListItemId, o => o.MapFrom(p => p.PriceListItemId))
                      .ForMember(d => d.PreventiveMedicineItemId, o => o.MapFrom(p => p.Id))
                      .ForMember(d => d.Comments, o => o.MapFrom(p => p.ReminderComments))
                      .ForMember(d => d.ScheduledDateStr, o => o.MapFrom(p => p.Scheduled))
                      .ForMember(d => d.LastReminderDateStr, o => o.MapFrom(p => p.LastReminderDate))
                      .ForMember(d => d.NextAppointmentDateStr, o => o.MapFrom(p => p.Patient.CalenderEntries.Any() ? p.Patient.CalenderEntries.First().Date : (DateTime?)null))
                //.ForMember(d => d.ScheduledDateStr,
                //           o =>
                //           o.MapFrom(
                //               p =>
                //               p.Scheduled.HasValue && p.Scheduled.Value > DateTime.MinValue
                //                   ? p.Scheduled.Value.ToShortDateString()
                //                   : string.Empty))
                //.ForMember(d => d.LastReminderDateStr,
                //           o =>
                //           o.MapFrom(
                //               p =>
                //               p.LastReminderDate.HasValue && p.LastReminderDate.Value > DateTime.MinValue
                //                   ? p.LastReminderDate.Value.ToShortDateString()
                //                   : string.Empty))
                //.ForMember(d => d.NextAppointmentDateStr,
                //           o =>
                //           o.MapFrom(
                //               p =>
                //               p.Patient.CalenderEntries.Any()
                //                   ? p.Patient.CalenderEntries.First().Date.ToShortDateString()
                //                   : string.Empty))
                      .ForMember(d => d.PatientColor, o => o.MapFrom(p => p.Patient.AnimalColor.Value))
                      .ForMember(d => d.PatientAge, o => o.MapFrom(p => p.Patient.Age))
                      .ForMember(d => d.PatientGender,
                                 o => o.MapFrom(p => p.Patient.GenderId == (int)GenderEnum.Male ? Resources.Gender.Male : Resources.Gender.Female))
                      .ForMember(d => d.PatientStatus, o => o.MapFrom(p => p.Patient.Active ? Resources.Global.Active : Resources.Global.NotActive));

            AutoMapper.Mapper.CreateMap<PreventiveReminderIndexModel, PreventiveReminderCsvModel>()
                .ForMember(d => d.PatientAge, s => s.MapFrom(a => a.PatientAge.HasValue ? decimal.Round(a.PatientAge.Value, 2, MidpointRounding.AwayFromZero) : a.PatientAge))
                .ForMember(d => d.ClientCellPhone, s => s.MapFrom(a => string.IsNullOrWhiteSpace(a.ClientCellPhone) ? "" : "=\"" + a.ClientCellPhone + "\""))
                .ForMember(d => d.ClientHomePhone, s => s.MapFrom(a => string.IsNullOrWhiteSpace(a.ClientHomePhone) ? "" : "=\"" + a.ClientHomePhone + "\""))
                .ForMember(d => d.ScheduledDateStr,
                           o =>
                           o.MapFrom(
                               p =>
                               p.Scheduled.HasValue && p.Scheduled.Value > DateTime.MinValue
                                   ? p.Scheduled.Value.ToShortDateString()
                                   : string.Empty))
                .ForMember(d => d.LastReminderDateStr,
                           o =>
                           o.MapFrom(
                               p =>
                               p.LastReminderDateStr.HasValue && p.LastReminderDateStr.Value > DateTime.MinValue
                                   ? p.LastReminderDateStr.Value.ToShortDateString()
                                   : string.Empty))
                .ForMember(d => d.NextAppointmentDateStr,
                           o =>
                           o.MapFrom(
                               p =>
                                p.NextAppointmentDateStr.HasValue && p.NextAppointmentDateStr.Value > DateTime.MinValue
                                   ? p.NextAppointmentDateStr.Value.ToShortDateString()
                                   : string.Empty));

            AutoMapper.Mapper.CreateMap<Model.Patients.Patient, WebModels.PreventiveMedicine.CertificateWebModel>()
                      .ForMember(d => d.ClientFullName,
                                 o => o.MapFrom(p => string.Format("{0} {1}", p.Client.FirstName, p.Client.LastName)))
                      .ForMember(d => d.PatientName, o => o.MapFrom(p => p.Name))
                      .ForMember(d => d.PatientChipNumber, o => o.MapFrom(p => p.ElectronicNumber))
                      .ForMember(d => d.PatientAnimalKindName, o => o.MapFrom(p => p.AnimalRace.AnimalKind.Value))
                      .ForMember(d => d.PatientRaceName, o => o.MapFrom(p => p.AnimalRace.Value))
                      .ForMember(d => d.PatientBirthDate, o => o.MapFrom(p => p.BirthDate != null ? p.BirthDate.Value.ToShortDateString() : "-"))
                      .ForMember(d => d.FertilizationSterilization,
                                 o => o.MapFrom(p => p.Sterilization ? Resources.Enums.LetterTemplateEnum.Sterilization : string.Empty))
                      .ForMember(d => d.PatientSex,
                                 o =>
                                 o.MapFrom(
                                     p =>
                                     RapidVet.Resources.Gender.ResourceManager.GetString(
                                         ((GenderEnum)p.GenderId).ToString())));

            AutoMapper.Mapper.CreateMap<PreventiveMedicineItemCreateModel, PreventiveMedicineItem>()
                      .ForMember(d => d.PatientId, o => o.MapFrom(p => p.PatientId))
                      .ForMember(d => d.PriceListItemId, o => o.MapFrom(p => p.PriceListItemId))
                      .ForMember(d => d.Scheduled, o => o.MapFrom(p => p.SchedualedDate))
                      .ForMember(d => d.ReminderComments, o => o.MapFrom(p => p.Comment));

            AutoMapper.Mapper.CreateMap<PreventiveMedicineItem, PreventiveMedicineItemCreateModel>()
                      .ForMember(d => d.PreventiveMedicineItemId, o => o.MapFrom(p => p.Id))
                      .ForMember(d => d.PatientId, o => o.MapFrom(p => p.PatientId))
                      .ForMember(d => d.ClientId, o => o.MapFrom(p => p.Patient.ClientId))
                      .ForMember(d => d.PriceListItemId, o => o.MapFrom(p => p.PriceListItemId))
                      .ForMember(d => d.SchedualedDate, o => o.MapFrom(p => p.Scheduled))
                      .ForMember(d => d.Comment, o => o.MapFrom(p => p.ReminderComments));

            AutoMapper.Mapper.CreateMap<PreventiveMedicineItem, PreformedPreventiveReportModel>()
                      .ForMember(d => d.ClientName, o => o.MapFrom(p => p.Patient.Client.Name))
                      .ForMember(d => d.ClientIdCard, o => o.MapFrom(p => p.Patient.Client.IdCard))
                      .ForMember(d => d.Date, o => o.MapFrom(p => p.Preformed.Value.ToShortDateString()))
                      .ForMember(d => d.PatientChip, o => o.MapFrom(p => p.Patient.ElectronicNumber))
                      .ForMember(d => d.PatientColor, o => o.MapFrom(p => p.Patient.AnimalColor.Value))
                      .ForMember(d => d.PatientGender,
                                 o => o.MapFrom(p => p.Patient.GenderId == (int)GenderEnum.Female ? Resources.Gender.Female : Resources.Gender.Male))
                      .ForMember(d => d.PatientKind, o => o.MapFrom(p => p.Patient.AnimalRace.AnimalKind.Value))
                      .ForMember(d => d.PatientName, o => o.MapFrom(p => p.Patient.Name))
                      .ForMember(d => d.PreformingDoctor,
                      o => o.MapFrom(p => p.ExternalyPreformed ? p.ExternalDrName : p.PreformingUser != null ? p.PreformingUser.Name : "בוצע חיצונית"))
                      .ForMember(d => d.PriceListItemName, o => o.MapFrom(p => p.PriceListItem.Name));


            //archives

            AutoMapper.Mapper
                      .CreateMap<Model.Archives.ArchiveDocument, WebModels.ArchiveDocuments.ArchiveDocumentIndexModel>()
                      .ForMember(d => d.TypeName,
                                 o =>
                                 o.MapFrom(
                                     ad =>
                                     RapidVet.Resources.Enums.ArchivesFileType.ResourceManager.GetString(
                                         ad.FileType.ToString())))
                      .ForMember(d => d.Date, o => o.MapFrom(ad => ad.Created.ToShortDateString()))
                      .ForMember(d => d.Time, o => o.MapFrom(ad => ad.Created.ToShortTimeString()));

            AutoMapper.Mapper
                      .CreateMap<Model.Archives.ArchiveDocument, WebModels.ArchiveDocuments.ArchiveDocumentEditModel>();

            AutoMapper.Mapper
                      .CreateMap<WebModels.ArchiveDocuments.ArchiveDocumentEditModel, Model.Archives.ArchiveDocument>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.FileTypeId, o => o.Ignore())
                      .ForMember(d => d.FileType, o => o.Ignore());

            //patient lab tests
            AutoMapper.Mapper.CreateMap<Model.Clinics.LabTestType, WebModels.PatientsLabTests.PatientLabTestIndexModel>()
                      .ForMember(d => d.Name, o => o.MapFrom(ltt => ltt.Name))
                      .ForMember(d => d.LabTestTypeId, o => o.MapFrom(ltt => ltt.Id));

            AutoMapper.Mapper
                      .CreateMap<Model.PatientLabTests.PatientLabTest, WebModels.PatientsLabTests.PatientLabTestDate>()
                      .ForMember(d => d.Date, o => o.MapFrom(plt => plt.Created.ToShortDateString()));

            AutoMapper.Mapper.CreateMap<Model.Clinics.LabTest, WebModels.PatientsLabTests.PatientLabTestResultModel>()
                      .ForMember(d => d.Name, o => o.MapFrom(lt => lt.Name))
                      .ForMember(d => d.Min, o => o.MapFrom(lt => lt.MinValue))
                      .ForMember(d => d.Max, o => o.MapFrom(lt => lt.MaxValue))
                      .ForMember(d => d.LabTestId, o => o.MapFrom(lt => lt.Id));

            AutoMapper.Mapper
                      .CreateMap
                <Model.PatientLabTests.PatientLabTestResult, WebModels.PatientsLabTests.PatientLabTestResultModel>()
                      .ForMember(d => d.Id, o => o.MapFrom(p => p.Id))
                      .ForMember(d => d.LabTestId, o => o.MapFrom(p => p.LabTest.Id))
                      .ForMember(d => d.Name, o => o.MapFrom(p => p.LabTest.Name))
                      .ForMember(d => d.Min, o => o.MapFrom(p => p.LabTest.MinValue))
                      .ForMember(d => d.Max, o => o.MapFrom(p => p.LabTest.MaxValue))
                      .ForMember(d => d.Result, o => o.MapFrom(p => p.Result));

            AutoMapper.Mapper
                      .CreateMap
                <WebModels.PatientsLabTests.PatientLabTestResultModel, Model.PatientLabTests.PatientLabTestResult>()
                      .ForMember(d => d.Result, o => o.MapFrom(p => p.Result))
                      .ForMember(d => d.LabTestId, o => o.MapFrom(p => p.LabTestId))
                      .ForMember(d => d.PatientLabTest, o => o.Ignore());



            //letters
            AutoMapper.Mapper.CreateMap<Model.Patients.Letter, WebModels.Patients.LetterWebModel>();

            AutoMapper.Mapper
                      .CreateMap
                <WebModels.Patients.MinistryOfAgricultureReportWebModel, Model.Patients.MinistryOfAgricultureReport>();


            //history

            AutoMapper.Mapper.CreateMap<RapidVet.Model.PatientFollowUp.FollowUp, HistoryItem>()
                      .ForMember(h => h.Name,
                                 o => o.MapFrom(f => string.Format("מעקב לתאריך {0} : {1}", f.DateTime.ToShortDateString(), f.Comment)))
                      .ForMember(h => h.Date, o => o.MapFrom(f => f.Created))
                      .ForMember(h => h.Url, o => o.UseValue("#"))
                      .ForMember(h => h.ItemType, o => o.UseValue("followUp"))
                      .ForMember(h => h.HandlingDoctor, o => o.MapFrom(f => f.User.Name))
                      .ForMember(h => h.DaysFromTreatment, o => o.MapFrom(f => (DateTime.Now - f.Created).Days.ToString()))
                      .ForMember(h => h.Sum, o => o.UseValue((decimal)0));


            AutoMapper.Mapper.CreateMap<Quotation, HistoryItem>()
                      .ForMember(h => h.Name, o => o.MapFrom(q => q.Name))
                      .ForMember(h => h.Date, o => o.MapFrom(q => q.Created))
                      .ForMember(h => h.Url, o => o.MapFrom(q => "/Quotations/Index/" + q.Patient.ClientId.ToString() + "?quotationId=" + q.Id.ToString()))
                      .ForMember(h => h.ItemType, o => o.MapFrom(q => "quotation"));

            AutoMapper.Mapper.CreateMap<RapidVet.Model.FullCalender.CalenderEntry, HistoryItem>()
                .ForMember(h => h.Name, o => o.MapFrom(s => "אי הופעה לפגישה"))
                .ForMember(h => h.ItemType, o => o.MapFrom(s => "פגישה"))
                .ForMember(h => h.Active, o => o.UseValue(true))
                .ForMember(h => h.Sum, o => o.UseValue(0))
                .ForMember(h => h.HandlingDoctor, o => o.MapFrom(c => c.Doctor.Name));



            AutoMapper.Mapper.CreateMap<RapidVet.Model.Auditings.Auditing, RapidVet.WebModels.Auditing.AuditingWebModel>()
                .ForMember(h => h.DateStamp, o => o.MapFrom(q => q.DateStamp.ToString("dd/MM/yyyy HH:mm")));

            AutoMapper.Mapper.CreateMap<ArchiveDocument, HistoryItem>()
                      .ForMember(h => h.Name, o => o.MapFrom(a => a.Title))
                      .ForMember(h => h.Date, o => o.MapFrom(a => a.Created))
                      .ForMember(h => h.Url, o => o.MapFrom(q => "/Archives/View/" + q.PatientId + "?documentId=" + q.Id))
                      .ForMember(h => h.ItemType, o => o.MapFrom(q => "archive"));

            AutoMapper.Mapper.CreateMap<Letter, HistoryItem>()
                .ForMember(h => h.Name, o => o.MapFrom(q => RapidVet.Resources.Enums.LetterTemplateEnum.ResourceManager.GetString(q.LetterTemplateType.ToString())))
                      .ForMember(h => h.Date, o => o.MapFrom(a => a.CreatedDate))
                      .ForMember(h => h.Url, o => o.MapFrom(q => "/Letters/ShowLetter/" + q.PatientId.ToString() + "?letterid=" + q.Id.ToString()))
                      .ForMember(h => h.ItemType, o => o.MapFrom(q => "letter"));

            AutoMapper.Mapper.CreateMap<PatientLabTest, HistoryItem>()
                      .ForMember(h => h.Name, o => o.MapFrom(a => "בדיקת " + a.LabTestTemplate.Name + ": " + string.Join(", ", a.Results.Select(r => String.Format("{0}={1}", r.LabTest.Name, r.Result)))))
                      .ForMember(h => h.Date, o => o.MapFrom(a => a.Created))
                      .ForMember(h => h.Url, o => o.MapFrom(q => "/Labs/Index/" + q.PatientId.ToString()))
                      .ForMember(h => h.ItemType, o => o.MapFrom(q => "labtest"));

            AutoMapper.Mapper.CreateMap<MedicalProcedure, HistoryItem>()
                      .ForMember(h => h.Name, o => o.MapFrom(a => a.ProcedureName))
                      .ForMember(h => h.Date, o => o.MapFrom(a => a.Start))
                      .ForMember(h => h.HandlingDoctor, o => o.MapFrom(a => a.SurgeonName))
                      .ForMember(h => h.Url, o => o.MapFrom(a => string.Format("/MedicalProcedures/Edit/{0}", a.Id.ToString())))
                      .ForMember(h => h.ItemType, o => o.UseValue("medicalprocedure"))
                      .ForMember(h => h.DaysFromTreatment, o => o.MapFrom(a => (DateTime.Now - a.Start).Days.ToString()))
                      .ForMember(h => h.Sum, o => o.UseValue((decimal)0));

            AutoMapper.Mapper.CreateMap<DischargePaper, HistoryItem>()
                      .ForMember(h => h.Name, o => o.MapFrom(a => "מכתב שחרור"))
                      .ForMember(h => h.Date, o => o.MapFrom(a => a.DateTime))
                      .ForMember(h => h.Url,
                                 o => o.MapFrom(a => string.Format("/DischargePapers/Edit/{0}", a.Id.ToString())))
                      .ForMember(h => h.ItemType, o => o.UseValue("dischargepaper"))
                      .ForMember(h => h.DaysFromTreatment, o => o.MapFrom(a => (DateTime.Now - a.DateTime).Days.ToString()))
                        .ForMember(h => h.Sum, o => o.UseValue((decimal)0));

            AutoMapper.Mapper.CreateMap<MinistryOfAgricultureReport, HistoryItem>()
                      .ForMember(h => h.Name,
                                 o =>
                                 o.MapFrom(
                                     a =>
                                     "דיווח למשרד החקלאות: " +
                                     RapidVet.Resources.Enums.MinistryOfAgricultureReportType.ResourceManager.GetString(
                                         a.ReportType.ToString())))
                      .ForMember(h => h.Date, o => o.MapFrom(a => a.DateTime))
                      .ForMember(h => h.ItemType, o => o.MapFrom(q => "ministry"))
                      .ForMember(h => h.Url, o => o.MapFrom(q => "#"))
                      .ForMember(h => h.DaysFromTreatment, o => o.MapFrom(q => (DateTime.Now - q.DateTime).Days.ToString()));

            //calender journal
            AutoMapper.Mapper.CreateMap<Holiday, Holiday>()
                      .ForMember(h => h.Id, o => o.Ignore())
                      .ForMember(h => h.ClinicId, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<Room, Room>()
                      .ForMember(r => r.Id, o => o.Ignore())
                      .ForMember(r => r.ClinicId, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<Recess, Recess>()
                      .ForMember(r => r.Id, o => o.Ignore())
                      .ForMember(r => r.UserId, o => o.Ignore())
                      .ForMember(r => r.ClinicId, o => o.Ignore());

            //medical procedures

            AutoMapper.Mapper.CreateMap<MedicalProcedure, MedicalProcedureWebModel>()
                      .ForMember(d => d.ProcedureDate, o => o.MapFrom(mp => mp.Start));

            AutoMapper.Mapper.CreateMap<MedicalProcedureWebModel, MedicalProcedure>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.Patient, o => o.Ignore())
                      .ForMember(d => d.Diagnosis, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<MedicalProcedure, MedicalProcedurePrintModel>()
                      .ForMember(d => d.Letter,
                                 o => o.MapFrom(mp => !string.IsNullOrWhiteSpace(mp.Letter)
                                                          ? mp.Letter
                                                          : null));

            AutoMapper.Mapper.CreateMap<MedicalProcedurePrintModel, MedicalProcedure>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.PatientId, o => o.Ignore());

            //discharge papers
            AutoMapper.Mapper.CreateMap<DischargePaper, DischargePaperWebModel>();
            AutoMapper.Mapper.CreateMap<DischargePaperWebModel, DischargePaper>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.Patient, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<DischargePaper, MedicalProcedurePrintModel>()
                     .ForMember(d => d.Letter,
                                o => o.MapFrom(mp => !string.IsNullOrWhiteSpace(mp.Letter)
                                                         ? mp.Letter
                                                         : null));

            AutoMapper.Mapper.CreateMap<MedicalProcedurePrintModel, DischargePaper>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.PatientId, o => o.Ignore());

            //Referrals

            AutoMapper.Mapper.CreateMap<Referral, ReferralEditWebModel>()
                      .ForMember(d => d.RefferedToUserId,
                                 o =>
                                 o.MapFrom(
                                     r =>
                                     r.RefferedToUserId == null && !string.IsNullOrWhiteSpace(r.RefferedToExternal)
                                         ? -9
                                         : r.RefferedToUserId));

            AutoMapper.Mapper.CreateMap<ReferralEditWebModel, Referral>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.RefferedToUserId,
                                 o => o.MapFrom(rewl => rewl.RefferedToUserId == -9 ? null : rewl.RefferedToUserId));


            //Finance
            AutoMapper.Mapper.CreateMap<VisitTreatment, FinanceDocumentItemWebmodel>()
                      .ForMember(dst => dst.ItemTreatmentID, opt => opt.MapFrom(src => src.Id))
                      .ForMember(dst => dst.CatalogNumber, opt => opt.MapFrom(src => src.PriceListItem.Catalog))
                      .ForMember(dst => dst.Description, opt => opt.MapFrom(src => src.Name))
                      .ForMember(dst => dst.Discount, opt => opt.MapFrom(src => src.DiscountPerUnit))
                      .ForMember(dst => dst.DiscountPercentage, opt => opt.MapFrom(src => src.DiscountPercent));

            AutoMapper.Mapper.CreateMap<VisitMedication, FinanceDocumentItemWebmodel>()
                      .ForMember(dst => dst.ItemMedicationID, opt => opt.MapFrom(src => src.Id))
                      .ForMember(dst => dst.CatalogNumber, opt => opt.UseValue(String.Empty))
                      .ForMember(dst => dst.Description, opt => opt.MapFrom(src => src.Name))
                      .ForMember(dst => dst.Discount,
                                 opt => opt.MapFrom(src => src.DiscountPerUnit))
                      .ForMember(dst => dst.Quantity, opt => opt.MapFrom(src => src.OverallAmount))
                      .ForMember(dst => dst.UnitPrice, opt => opt.MapFrom(src => src.UnitPrice))
                      .ForMember(dst => dst.DiscountPercentage, opt => opt.MapFrom(src => src.DiscountPercent));

            AutoMapper.Mapper.CreateMap<VisitExamination, FinanceDocumentItemWebmodel>()
                      .ForMember(dst => dst.ItemExaminationID, opt => opt.MapFrom(src => src.Id))
                      .ForMember(dst => dst.CatalogNumber, opt => opt.MapFrom(src => src.PriceListItem.Catalog))
                      .ForMember(dst => dst.Description, opt => opt.MapFrom(src => src.Name))
                      .ForMember(dst => dst.Discount, opt => opt.MapFrom(src => src.DiscountPerUnit))
                      .ForMember(dst => dst.DiscountPercentage, opt => opt.MapFrom(src => src.DiscountPercent));

            AutoMapper.Mapper.CreateMap<PreventiveMedicineItem, FinanceDocumentItemWebmodel>()
                      .ForMember(dst => dst.ItemPreventiveMedicineID, opt => opt.MapFrom(src => src.Id))
                      .ForMember(dst => dst.CatalogNumber, opt => opt.MapFrom(src => src.PriceListItem.Catalog))
                      .ForMember(dst => dst.Description, opt => opt.MapFrom(src => src.PriceListItem.Name))
                      .ForMember(dst => dst.Discount, opt => opt.MapFrom(src => src.DiscountPerUnit))
                      .ForMember(dst => dst.UnitPrice, opt => opt.MapFrom(src => src.Price))
                      .ForMember(dst => dst.DiscountPercentage, opt => opt.MapFrom(src => src.DiscountPercent));

            AutoMapper.Mapper.CreateMap<QuotationTreatment, FinanceDocumentItemWebmodel>()
                      .ForMember(dst => dst.CatalogNumber, opt => opt.MapFrom(src => src.PriceListItem.Catalog))
                      .ForMember(dst => dst.Description, opt => opt.MapFrom(src => src.PriceListItem.Name))
                      .ForMember(dst => dst.Quantity, opt => opt.UseValue((decimal)1))
                      .ForMember(dst => dst.UnitPrice, opt => opt.MapFrom(src => src.Price))
                      .ForMember(dst => dst.Discount, opt => opt.MapFrom(src => src.Discount ?? (decimal)0))
                      .ForMember(dst => dst.DiscountPercentage, opt => opt.MapFrom(src => src.PercentDiscount ?? (decimal)0));


            AutoMapper.Mapper.CreateMap<FinanceDocumentFromJson, FinanceDocument>()
                .ForMember(dst => dst.RelatedDocuments, opt => opt.Ignore());
            AutoMapper.Mapper.CreateMap<jsonItem, FinanceDocumentItem>();

            AutoMapper.Mapper.CreateMap<FinanceDocument, DetectionWebModel>()
                      .ForMember(d => d.CreatedDate, o => o.MapFrom(f => f.Created.ToShortDateString()))
                      .ForMember(d => d.ClientName, o => o.MapFrom(f => f.ClientName))
                      .ForMember(d => d.ClientStatus, o => o.MapFrom(f => f.Client.ClientStatus.Name))//f.Client.Active ? "פעיל" : "לא פעיל"))
                      .ForMember(d => d.DocumentSerialNumber, o => o.MapFrom(f => f.SerialNumber))
                      .ForMember(d => d.FinanceDocumentType, o => o.MapFrom(f => Resources.Enums.Finances.FinanceDocumentType.ResourceManager.GetString(f.FinanceDocumentType.ToString())))
                      .ForMember(d => d.TotalToPay, o => o.MapFrom(f => f.FinanceDocumentTypeId == (int)RapidVet.Enums.Finances.FinanceDocumentType.Refound && f.TotalSum > 0 ? f.TotalSum * (-1) : f.TotalSum));

            AutoMapper.Mapper.CreateMap<IssuerCreditType, IssuerCreditTypeJsonModel>();
            AutoMapper.Mapper.CreateMap<CreditCardCode, CreditCardCodesJsonModel>();
            AutoMapper.Mapper.CreateMap<EasyCardSettings, EasyCardSettingsJsonModel>();

            AutoMapper.Mapper.CreateMap<IssuerCreditType, IssuerCreditType>()
                      .ForMember(d => d.IssuerId, o => o.Ignore())
                      .ForMember(d => d.Issuer, o => o.Ignore())
                      .ForMember(d => d.Active, o => o.Ignore())
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.Active, o => o.Ignore())
                      .ForMember(d => d.Updated, o => o.UseValue(DateTime.Now));

            AutoMapper.Mapper.CreateMap<TelephoneDirectoryType, TelephoneDirectoryTypeJsonModel>();

            AutoMapper.Mapper.CreateMap<Model.Visits.Visit, WebModels.Visits.VisitFinanceModel>()
                .ForMember(d => d.VisitDate, o => o.MapFrom(v => v.Recipt == null ? v.VisitDate.ToString("dd/MM/yyyy") : v.Recipt.Created.ToString("dd/MM/yyyy")))
                      .ForMember(d => d.DoctorName, o => o.MapFrom(v => v.Doctor.Name))
                      .ForMember(d => d.ClientName, o => o.MapFrom(v => v.ClientAtTimeOfVisit.Name))
                      .ForMember(d => d.SerialNumber, o => o.MapFrom(v => v.Recipt.SerialNumber))
                //.ForMember(d => d.Price, o => o.MapFrom(v => v.Recipt == null ? v.Price : v.Recipt.TotalSum))
                      .ForMember(d => d.Price, o => o.MapFrom(v => v.Recipt == null ? new Nullable<decimal>() : v.Recipt.TotalSum))
                      .ForMember(d => d.ClientId, o => o.MapFrom(v => v.ClientIdAtTimeOfVisit))
                      .ForMember(d => d.IsAddedByCalender, o => o.MapFrom(v => false));

            AutoMapper.Mapper.CreateMap<Model.FullCalender.CalenderEntry, WebModels.Visits.VisitFinanceModel>()
                      .ForMember(d => d.VisitDate, o => o.MapFrom(v => v.Date.ToString("dd/MM/yyyy")))
                      .ForMember(d => d.DoctorName, o => o.MapFrom(v => v.Doctor.Name))
                      .ForMember(d => d.ClientName, o => o.MapFrom(v => v.Client.Name))
                      .ForMember(d => d.ClientId, o => o.MapFrom(v => v.ClientId))
                      .ForMember(d => d.IsAddedByCalender, o => o.MapFrom(v => true));



            AutoMapper.Mapper.CreateMap<Model.Finance.FinanceDocumentPayment, WebModels.Finance.CheckPaymentWebModel>()
                .ForMember(m => m.BankName, o => o.MapFrom(p => p.BankCode.Name))
                .ForMember(m => m.ClientName, o => o.MapFrom(c => c.InvoiceId.HasValue ? c.Invoice.ClientName : c.Recipt.ClientName));

            AutoMapper.Mapper.CreateMap<Model.Finance.FinanceDocumentPayment, WebModels.Finance.FinancePaymentJson>()
                      .ForMember(j => j.id, o => o.MapFrom(p => p.Id))
                      .ForMember(j => j.date, o => o.MapFrom(p => p.Recipt.Created.ToString("dd/MM/yyyy")))
                      .ForMember(j => j.clientName, o => o.MapFrom(p => p.Recipt.ClientName))
                      .ForMember(j => j.documentTypeId, o => o.MapFrom(p => p.Recipt.FinanceDocumentTypeId.ToString()))
                      .ForMember(j => j.serialNumber, o => o.MapFrom(p => p.Recipt.SerialNumber.ToString()))
                      .ForMember(j => j.amountIncludingTax, o => o.MapFrom(p => p.Sum))
                      .ForMember(j => j.bankCodeId, o => o.MapFrom(p => p.BankCodeId.HasValue ? p.BankCodeId.Value.ToString() : ""))
                      .ForMember(j => j.bankName, o => o.MapFrom(p => p.BankCode != null ? p.BankCode.Name : ""))
                      .ForMember(j => j.branchNumber, o => o.MapFrom(p => p.BankBranch))
                      .ForMember(j => j.accountNumber, o => o.MapFrom(p => p.BankAccount))
                      .ForMember(j => j.checkNumber, o => o.MapFrom(p => p.ChequeNumber))
                      .ForMember(j => j.bankTransferNumber, o => o.MapFrom(p => p.BankTransferNumber))
                      .ForMember(j => j.creditCardCompanyId, o => o.MapFrom(p => p.CreditCardCodeId.HasValue ? p.CreditCardCodeId.Value.ToString() : ""))
                      .ForMember(j => j.lastFourDigits, o => o.MapFrom(p => p.CardNumberLastFour))
                      .ForMember(j => j.creditCardCoupon, o => o.MapFrom(p => p.CreditOkNumber))
                      .ForMember(j => j.documentId, o => o.MapFrom(p => p.ReciptId))
                      .ForMember(j => j.isChecked, o => o.MapFrom(p => false))
                      .ForMember(j => j.isDeposited, o => o.MapFrom(p => p.DepositId.HasValue))
                      .ForMember(j => j.depositNumber, o => o.MapFrom(p => p.DepositNumber))
                      .ForMember(j => j.depositDate, o => o.MapFrom(p => p.Deposit.Date.ToString("dd/MM/yyyy")));

            //finance reports
            AutoMapper.Mapper.CreateMap<FinanceDocumentPayment, ChequeReportModel>()
                      .ForMember(d => d.BankBranch, o => o.MapFrom(p => p.BankBranch))
                      .ForMember(d => d.BankName, o => o.MapFrom(p => p.BankCode.Name))
                      .ForMember(d => d.AccountNumber, o => o.MapFrom(p => p.BankAccount))
                      .ForMember(d => d.ChequeNumber, o => o.MapFrom(p => p.ChequeNumber))
                      .ForMember(d => d.Amount, o => o.MapFrom(p => p.Sum));

            AutoMapper.Mapper.CreateMap<FinanceDocument, InvoiceReportWebModel>()
                      .ForMember(d => d.Date, o => o.MapFrom(f => f.Created.ToShortDateString()))
                      .ForMember(d => d.Cash, o => o.MapFrom(f => f.CashPaymentSum))
                      .ForMember(d => d.Transfer, o => o.MapFrom(f => f.BankTransferSum))
                      .ForMember(d => d.ChequeAmount, o => o.MapFrom(f => f.TotalChequesPayment))
                      .ForMember(d => d.CreditAmount, o => o.MapFrom(f => f.TotalCreditPaymentSum))
                      .ForMember(d => d.TotalAmount,
                                 o =>
                                 o.MapFrom(f => f.FinanceDocumentType == Enums.Finances.FinanceDocumentType.Invoice
                                     ? f.SumToPayForInvoice :
                                     f.CashPaymentSum + f.BankTransferSum + f.TotalCreditPaymentSum + f.TotalChequesPayment))
                      .ForMember(d => d.FinanceDocumentSerialNumber, o => o.MapFrom(f => f.SerialNumber))
                      .ForMember(d => d.FinanceDocumentType,
                                 o =>
                                 o.MapFrom(f => Resources.Enums.Finances.FinanceDocumentType.ResourceManager.GetString(f.FinanceDocumentType.ToString())));

            AutoMapper.Mapper.CreateMap<FinanceDocumentItem, SalesReportWebModel>()
                      .ForMember(d => d.PriceListItemId, o => o.MapFrom(f => f.PriceListItemId.Value))
                      .ForMember(d => d.Created, o => o.MapFrom(f => f.FinanceDocument.Created))
                      .ForMember(d => d.Doctor, o => o.MapFrom(f => f.FinanceDocument.User.Name))
                      .ForMember(d => d.Price, o => o.MapFrom(f => Math.Round(f.TotalBeforeVAT * f.FinanceDocument.VAT)))
                      .ForMember(d => d.ClientName, o => o.MapFrom(f => f.FinanceDocument.ClientId.HasValue ? f.FinanceDocument.ClientName : Resources.Migration.TempClient))
                      .ForMember(d => d.ClientId, o => o.MapFrom(f => f.FinanceDocument.ClientId.HasValue ? f.FinanceDocument.ClientId.Value : 0));

            AutoMapper.Mapper.CreateMap<FinanceDocument, IncomingsReportWebModel>()
                      .ForMember(d => d.Created, o => o.MapFrom(s => s.Created.ToString("dd/MM/yyyy HH:mm")))
                      .ForMember(d => d.Payments, o => o.UseValue(0));

            AutoMapper.Mapper.CreateMap<IncomingsReportWebModel, IncomingsReportCsvWebModel>()
                     .ForMember(d => d.FinanceDocumentType, o => o.MapFrom(p => RapidVet.Resources.Enums.Finances.FinanceDocumentType.ResourceManager.GetString(((RapidVet.Enums.Finances.FinanceDocumentType)p.FinanceDocumentTypeId).ToString())));


            //MetaData
            AutoMapper.Mapper.CreateMap<AnimalKind, SelectListItem>()
                      .ForMember(d => d.Text, o => o.MapFrom(a => a.Value))
                      .ForMember(d => d.Value, o => o.MapFrom(a => a.Id.ToString()));

            AutoMapper.Mapper.CreateMap<RegionalCouncil, SelectListItem>()
                      .ForMember(d => d.Text, o => o.MapFrom(r => r.Name))
                      .ForMember(d => d.Value, o => o.MapFrom(r => r.Id.ToString()));

            AutoMapper.Mapper.CreateMap<Medication, SelectListItem>()
                      .ForMember(d => d.Text, o => o.MapFrom(s => s.Name))
                      .ForMember(d => d.Value, o => o.MapFrom(s => s.Id.ToString()));

            AutoMapper.Mapper.CreateMap<ClientStatus, SelectListItem>()
                      .ForMember(d => d.Text, o => o.MapFrom(s => s.Name))
                      .ForMember(d => d.Value, o => o.MapFrom(s => s.Id.ToString()));

            //regionalCouncil
            AutoMapper.Mapper.CreateMap<RegionalCouncil, RegionalCouncil>()
                      .ForMember(d => d.Clients, o => o.Ignore())
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.Cities, o => o.Ignore());

            //Expenses
            AutoMapper.Mapper.CreateMap<ExpenseGroup, ExpenseGroupWebModel>();

            AutoMapper.Mapper.CreateMap<ExpenseGroupWebModel, ExpenseGroup>()
                      .ForMember(d => d.Id, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<Supplier, SupplierWebModel>();
            AutoMapper.Mapper.CreateMap<SupplierWebModel, Supplier>()
                      .ForMember(d => d.Id, o => o.Ignore());

            AutoMapper.Mapper.CreateMap<Supplier, SelectListItem>()
                      .ForMember(d => d.Value, o => o.MapFrom(s => s.Id.ToString()))
                      .ForMember(s => s.Text, o => o.MapFrom(s => s.Name));


            AutoMapper.Mapper.CreateMap<Expense, ExpenseIndexWebModel>()
                      .ForMember(d => d.ExpenseGroup, o => o.MapFrom(e => e.ExpenseGroup.Name))
                      .ForMember(d => d.Supplier, o => o.MapFrom(e => e.Supplier.Name))
                      .ForMember(d => d.IssuerName, o => o.MapFrom(e => e.Issuer.Name))
                      .ForMember(d => d.PaymentDate, o => o.MapFrom(e => e.PaymentDate.HasValue ? e.PaymentDate.Value.Date.ToShortDateString() : ""))
                      .ForMember(d => d.EnteredDate, o => o.MapFrom(e => e.EnteredDate.Date.ToShortDateString()))
                      .ForMember(d => d.InvoiceDate, o => o.MapFrom(e => e.InvoiceDate.Date.ToShortDateString()));

            AutoMapper.Mapper.CreateMap<Expense, ExpenseCreateEditWebModel>()
                      .ForMember(d => d.InvoiceDate, o => o.MapFrom(s => s.InvoiceDate.ToShortDateString()))
                      .ForMember(d => d.EnteredDate, o => o.MapFrom(s => s.EnteredDate.ToShortDateString()))
                      .ForMember(d => d.PaymentDate, o => o.MapFrom(s => s.PaymentDate.HasValue ? s.PaymentDate.Value.ToShortDateString() : ""));

            AutoMapper.Mapper.CreateMap<ExpenseCreateEditWebModel, Expense>()
                      .ForMember(d => d.Id, o => o.Ignore())
                      .ForMember(d => d.InvoiceDate, o => o.MapFrom(s => DateTime.Parse(s.InvoiceDate)))
                      .ForMember(d => d.EnteredDate, o => o.MapFrom(s => DateTime.Parse(s.EnteredDate)))
                      .ForMember(d => d.PaymentDate, o => o.MapFrom(s => string.IsNullOrEmpty(s.PaymentDate) ? (DateTime?)null : DateTime.Parse(s.PaymentDate)));

            //clinic tasks
            AutoMapper.Mapper.CreateMap<ClinicTask, ClinicTask>()
                      .ForMember(dst => dst.Id, opt => opt.Ignore())
                      .ForMember(dst => dst.ClinicId, opt => opt.Ignore())
                      .ForMember(dst => dst.Updated, opt => opt.UseValue(DateTime.Now));


            AutoMapper.Mapper.CreateMap<ClinicTaskWebModel, ClinicTask>()
                      .ForMember(dst => dst.StartDate, opt => opt.Ignore())
                      .ForMember(dst => dst.TargetDate, opt => opt.Ignore())
                      .ForMember(dst => dst.StatusId, opt => opt.MapFrom(src => src.Status));

            AutoMapper.Mapper.CreateMap<ClinicTask, ClinicTaskWebModel>()
                      .ForMember(dst => dst.StartDate,
                                 opt =>
                                 opt.MapFrom(
                                     src => src.StartDate.HasValue ? src.StartDate.Value.ToString("dd/MM/yyyy") : ""))
                      .ForMember(dst => dst.TargetDate,
                                 opt =>
                                 opt.MapFrom(
                                     src => src.TargetDate.HasValue ? src.TargetDate.Value.ToString("dd/MM/yyyy") : ""))
                      .ForMember(dst => dst.Status, opt => opt.MapFrom(src => src.StatusId));

            //dangerous drugs
            AutoMapper.Mapper.CreateMap<DangerousDrug, DangerousDrugReportItem>()
                      .ForMember(d => d.Date, o => o.MapFrom(s => s.Visit.VisitDate.ToString("dd/MM/yyyy")))
                      .ForMember(d => d.DoctorName, o => o.MapFrom(s => s.Visit.Doctor.Name))
                      .ForMember(d => d.MedicationName, o => o.MapFrom(s => s.Medication.Name))
                      .ForMember(d => d.OwnerName, o => o.MapFrom(s => s.Visit.Patient.Client.Name))
                      .ForMember(d => d.PatientName, o => o.MapFrom(s => s.Visit.Patient.Name))
                      .ForMember(d => d.PatientId, o => o.MapFrom(s => s.Visit.PatientId));

            //Inventory
            AutoMapper.Mapper.CreateMap<PriceListItemSupplier, PriceListItemSupplierWebModel>()
                      .ForMember(d => d.SupplierName, o => o.MapFrom(s => s.Supplier.Name))
                      .ForMember(d => d.ConnectedSupplier, o => o.UseValue(true));

            AutoMapper.Mapper.CreateMap<PriceListItemSupplierWebModel, PriceListItemSupplier>();

            AutoMapper.Mapper.CreateMap<InventoryPriceListItemWebModel, InventoryPriceListItemOrder>()
                      .ForMember(d => d.OrderedQuantity, o => o.MapFrom(i => i.Quantity))
                      .ForMember(d => d.PriceListItemId, o => o.MapFrom(i => i.Id));

            AutoMapper.Mapper.CreateMap<InventoryPriceListItemOrder, InventoryOrderedItemWebModel>()
                      .ForMember(d => d.orderItemId, o => o.MapFrom(i => i.Id))
                      .ForMember(d => d.id, o => o.MapFrom(i => i.PriceListItemId))
                      .ForMember(d => d.category, o => o.MapFrom(i => i.PriceListItem.Category.Name))
                      .ForMember(d => d.catalog, o => o.MapFrom(i => i.PriceListItem.Catalog))
                      .ForMember(d => d.name, o => o.MapFrom(i => i.PriceListItem.Name))
                      .ForMember(d => d.price, o => o.MapFrom(i => i.UnitPrice))
                      .ForMember(d => d.actualUnitPrice, o => o.MapFrom(i => i.ActualUnitPrice))
                      .ForMember(d => d.quantity, o => o.MapFrom(i => i.OrderedQuantity))
                      .ForMember(d => d.recieved, o => o.MapFrom(i => i.RecievedQuantity))
                      .ForMember(d => d.requireBarcode, o => o.MapFrom(i => i.PriceListItem.RequireBarcode));

            AutoMapper.Mapper.CreateMap<Inventory, InventoryStatusReportModel>()
                .ForMember(d => d.Date, o => o.MapFrom(i => i.Created.ToString("dd/MM/yyyy H:mm:ss")))
                      .ForMember(d => d.ActionId, o => o.MapFrom(i => i.InventoryOperationTypeId))
                      .ForMember(d => d.Action,
                                 o =>
                                 o.MapFrom(
                                     i =>
                                     Resources.Enums.InventoryOperationType.ResourceManager.GetString(
                                         i.OperationType.ToString())))
                      .ForMember(d => d.UserName, o => o.MapFrom(i => i.User.Name))
                      .ForMember(d => d.PriceListItem, o => o.MapFrom(i => i.PriceListItem.Name));


            //followUps
            AutoMapper.Mapper.CreateMap<Clinic, WebModels.PatientFollowUps.FollowUpSettingsModel>();
            AutoMapper.Mapper.CreateMap<WebModels.PatientFollowUps.FollowUpSettingsModel, Clinic>();

            //general reports
            AutoMapper.Mapper.CreateMap<Clinic, ClinicHeader>();
            AutoMapper.Mapper.CreateMap<RabiesReportItem, RabiesReportItemCsv>()
                .ForMember(d => d.CellPhone,
                    s =>
                        s.MapFrom(
                            a => string.IsNullOrWhiteSpace(a.CellPhone) ? "" : "=\"" + a.CellPhone + "\""))
                .ForMember(d => d.HomePhone,
                    s =>
                        s.MapFrom(
                            a => string.IsNullOrWhiteSpace(a.HomePhone) ? "" : "=\"" + a.HomePhone + "\""))
                .ForMember(d => d.ElectronicNumber,
                    s =>
                        s.MapFrom(
                            a => string.IsNullOrWhiteSpace(a.ElectronicNumber) ? "" : "=\"" + a.ElectronicNumber + "\""));

            AutoMapper.Mapper.CreateMap<VitalSign, Visit>()
                .ForMember(d => d.VisitDate, o => o.MapFrom(i => i.Time))
                .ForMember(d => d.ManualId, o => o.MapFrom(i => i.Id))
                .ForMember(d => d.Weight, o => o.MapFrom(i => i.VitalSignTypeId == (int)VitalSignType.Weight ? i.Value : 0))
                .ForMember(d => d.Pulse, o => o.MapFrom(i => i.VitalSignTypeId == (int)VitalSignType.Pulse ? (int)i.Value : 0))
                .ForMember(d => d.BCS, o => o.MapFrom(i => i.VitalSignTypeId == (int)VitalSignType.BCS ? (int)i.Value : 0))
                .ForMember(d => d.Temp, o => o.MapFrom(i => i.VitalSignTypeId == (int)VitalSignType.Temp ? i.Value : 0))
                .ForMember(d => d.BPM, o => o.MapFrom(i => i.VitalSignTypeId == (int)VitalSignType.Respiration ? (int)i.Value : 0));



            //----------------------------------------------------------XML Models -----------------------------------------------------------------
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Clients.Client, RapidVet.XmlModels.Client>();
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Clients.Details.Address, RapidVet.XmlModels.Address>()
                .ForMember(d => d.City, o => o.MapFrom(s => s.City != null ? s.City.Name : ""));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Clients.Details.Email, RapidVet.XmlModels.Email>();
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Clients.Details.Phone, RapidVet.XmlModels.Phone>();
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Patients.Patient, RapidVet.XmlModels.Patient>()
                .ForMember(d => d.AnimalColor, o => o.MapFrom(s => s.AnimalColor.Value))
                .ForMember(d => d.Gender,
                                 o => o.MapFrom(p => p.GenderId == 1 ? (Resources.Gender.Male) : (p.GenderId == 2 ? Resources.Gender.Female : Resources.Gender.UndefinedSex)));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Patients.AnimalRace, RapidVet.XmlModels.AnimalRace>()
                .ForMember(d => d.AnimalKind, o => o.MapFrom(s => s.AnimalKind.Value));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Patients.PatientComment, RapidVet.XmlModels.PatientComment>();
            AutoMapper.Mapper.CreateMap<RapidVet.Model.PatientLabTests.PatientLabTest, RapidVet.XmlModels.PatientLabTest>();
            AutoMapper.Mapper.CreateMap<RapidVet.Model.PatientLabTests.PatientLabTestResult, RapidVet.XmlModels.PatientLabTestResult>();
            AutoMapper.Mapper.CreateMap<RapidVet.Model.PreventiveMedicine.PreventiveMedicineItem, RapidVet.XmlModels.PreventiveMedicineItem>()
                .ForMember(d => d.PriceListItem, o => o.MapFrom(p => p.PriceListItem.Name))
                .ForMember(d => d.PriceListItemType, o => o.MapFrom(p => p.PriceListItemType.Name))
                .ForMember(d => d.TollPaymentMethod, o => o.MapFrom(p => RapidVet.Resources.Enums.TollPaymentMethod.ResourceManager.GetString(p.TollPaymentMethod.ToString())));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Patients.MedicalProcedure, RapidVet.XmlModels.MedicalProcedure>()
                .ForMember(d => d.Diagnosis, o => o.MapFrom(s => s.Diagnosis != null ? s.Diagnosis.Name : ""))
                .ForMember(d => d.ProcedureType, o => o.MapFrom(p => RapidVet.Resources.Enums.MedicalProcedureType.ResourceManager.GetString(p.ProcedureType.ToString())));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Visits.Visit, RapidVet.XmlModels.Visit>()
              .ForMember(d => d.Doctor, o => o.MapFrom(p => p.Doctor.Name));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Visits.VisitDiagnosis, RapidVet.XmlModels.VisitDiagnosis>()
                .ForMember(d => d.Diagnosis, o => o.MapFrom(s => s.Diagnosis != null ? s.Diagnosis.Name : ""));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Visits.VisitExamination, RapidVet.XmlModels.VisitExamination>()
                .ForMember(d => d.PriceListItem, o => o.MapFrom(p => p.PriceListItem != null ? p.PriceListItem.Name : ""));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Visits.VisitMedication, RapidVet.XmlModels.VisitMedication>()
               .ForMember(d => d.Medication, o => o.MapFrom(p => p.Medication != null ? p.Medication.Name : ""));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Visits.VisitTreatment, RapidVet.XmlModels.VisitTreatment>()
               .ForMember(d => d.PriceListItem, o => o.MapFrom(p => p.PriceListItem != null ? p.PriceListItem.Name : ""));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Visits.VitalSign, RapidVet.XmlModels.VitalSign>()
             .ForMember(d => d.VitalSignType, o => o.MapFrom(p => RapidVet.Resources.Visit.ResourceManager.GetString(p.VitalSignType.ToString())));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Visits.DangerousDrug, RapidVet.XmlModels.DangerousDrug>()
                .ForMember(d => d.Medication, o => o.MapFrom(p => p.Medication != null ? p.Medication.Name : ""));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Finance.FinanceDocument, RapidVet.XmlModels.FinanceDocument>()
                 .ForMember(d => d.User, o => o.MapFrom(p => p.User.Name))
                 .ForMember(d => d.Issuer, o => o.MapFrom(p => p.Issuer.Name))
                 .ForMember(d => d.FinanceDocumentType, o => o.MapFrom(p => RapidVet.Resources.Visit.ResourceManager.GetString(p.FinanceDocumentType.ToString())));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Finance.FinanceDocumentItem, RapidVet.XmlModels.FinanceDocumentItem>();
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Finance.FinanceDocumentPayment, RapidVet.XmlModels.FinanceDocumentPayment>()
                .ForMember(d => d.Issuer, o => o.MapFrom(p => p.Issuer.Name))
                .ForMember(d => d.IssuerCreditType, o => o.MapFrom(p => p.IssuerCreditType != null ? p.IssuerCreditType.Name : ""))
                .ForMember(d => d.BankCode, o => o.MapFrom(p => p.BankCode != null ? p.BankCode.Code.ToString() : ""))
                .ForMember(d => d.PaymentType, o => o.MapFrom(p => RapidVet.Resources.Enums.Finances.PaymentType.ResourceManager.GetString(p.PaymentType.ToString())))
                .ForMember(d => d.CreditCardCode, o => o.MapFrom(p => p.CreditCardCode != null ? RapidVet.Resources.CreditCardCodes.ResourceManager.GetString(p.CreditCardCode.Name.ToString()) : ""));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Clinics.Clinic, RapidVet.XmlModels.Clinic>()
                  .ForMember(d => d.Clients, o => o.MapFrom(p => p.Clients.ToList()));
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Clinics.LabTest, RapidVet.XmlModels.LabTest>();
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Clinics.LabTestType, RapidVet.XmlModels.LabTestType>();
            AutoMapper.Mapper.CreateMap<RapidVet.Model.Clinics.LabTestTemplate, RapidVet.XmlModels.LabTestTemplate>()
                 .ForMember(d => d.AnimalKind, o => o.MapFrom(s => s.AnimalKind.Value));

        }
    }
}


