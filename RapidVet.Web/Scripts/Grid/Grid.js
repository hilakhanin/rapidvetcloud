﻿var GridURL = "";

function Moneyformat(data, c, d, t) {

    var n = data,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "") + s;
}
function DateFormater(cellValue, options, rowObject) {
    if (cellValue)
        return cellValue.toString().substr(11, 5) + " " + cellValue.toString().substr(8, 2) + "/" + cellValue.toString().substr(5, 2) + "/" + cellValue.toString().substr(0, 4);
    else
        return cellValue;
}
function PdfLinkFormatter(cellValue, options, rowObject) {
    return '<a class="aLink" href="' + cellValue + '">הורד</a>'
}
function LinkFormatterTik(cellValue, options, rowObject) {
    return '<a class="aLink" href="' + cellValue + '">לתיק לקוח</a>'
}
function DocTypeFormatter(cellValue, options, rowObject) {
    return cellValue.split('_').join(' ').split('INVOICE WITH PAYMENT INFO').join('חשבונית מס/קבלה').split('INVOICE').join('חשבונית מס').split('PAYMENT INFO').join('קבלה').split('CREDIT NOTE').join('חשבונית זיכוי');
}
function MoneyFormatter(cellValue, options, rowObject) {
    return Moneyformat(cellValue, 2);
}
function MoneyShekelFormatter(cellValue, options, rowObject) {
    return Moneyformat(cellValue, 2) + " ₪";
}
var isLoaded = false;
function ReLoadGrid(url) {
    //$('#lui_jqGrid').css("display", "block");
    setTimeout(function () { $('#load_jqGrid').css("display", "block"); }, 10);

    $("#jqGrid").jqGrid('setGridParam', { current: true, datatype: "jsonstring", datastr: '[]' }).trigger('reloadGrid');
    var request;
    request = $.ajax({
        url: url,
        type: "POST",
        data: ""
    });
    request.done(function (response, textStatus, jqXHR) {
        RefreshByData(response);
    });
    request.fail(function (jqXHR, textStatus, errorThrown) {
        $('#load_jqGrid').css("display", "none");
    });
}
function RefreshByData(data) {
    pagePosition = $('#jQGrid').getGridParam('page');
    $("#jqGrid").jqGrid('setGridParam', { current: true, datatype: "jsonstring", datastr: data }).trigger('reloadGrid');

    //$('#lui_jqGrid').css("display", "none");
    $('#load_jqGrid').css("display", "none");
}

function SetExport(url, gridName) {
    var SortBy = "";
    $('.ui-icon-circle-triangle-n')[0].parentElement.parentElement.innerHTML = $('.ui-icon-circle-triangle-n')[0].parentElement.parentElement.innerHTML +
       "<a role='link' title='יצא כ-PDF' href='javascript:void(0)' class='ui-jqgrid-titlebar-close HeaderButton PDFIcon' style='width:16px;position:absolute;left:18px;'> " +
           "<span class='ui-icon-pdf' >&nbsp;</span>" +
       "</a>"
    + "<a role='link' title='הדפס' href='javascript:void(0)' class='ui-jqgrid-titlebar-close HeaderButton PrintIcon' style='width:16px;position:absolute;left:34px;'> " +
           "<span class='ui-icon-print' >&nbsp;</span>" +
       "</a>";

    $('.ui-icon-circle-triangle-n')[0].title = "יצא כאקסל";

    var SortBy = $("#" + gridName).jqGrid('getGridParam', 'sortname') + " " + $("#" + gridName).jqGrid('getGridParam', 'sortorder');

    $('.ui-icon.ui-icon-circle-triangle-n').on("click", function (e) {//excel
        try {
            ShowLoder();
        }
        catch (e) {
        } 
        var strNames = "";
        var arrCaptions = $("#" + gridName).jqGrid('getGridParam', 'colNames');
        var arrNames = $("#" + gridName).jqGrid('getGridParam', 'colModel');
        for (var i = 0; i < arrNames.length; i++) {
            strNames += arrNames[i].name + ";";
        }
        $.ajax({
            url: url + "&ExportType=1&GridCaptions=" + escape(arrCaptions.join(';')) + "&GridNames=" + escape(strNames) + "&SortBy=" + escape(SortBy),
            type: "POST",
            data: "",
            success: function (data) {
               
                var a = document.createElement('a');
                var blob = base64ToBlob(data, "xlsx");
                var url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = "ExportDataGrid.xlsx";
                a.click();
                window.URL.revokeObjectURL(url);
                try{
                    hideLoder();
                }
                catch(e)
                {
                }
            },
            error: function (data) {
               
            }
        });
    });

    $('.PDFIcon').on("click", function (e) {//pdf 
        debugger;
        try {
            ShowLoder();
        }
        catch (e) {
        }
        var strNames = "";
        var arrCaptions = $("#" + gridName).jqGrid('getGridParam', 'colNames');
        var arrNames = $("#" + gridName).jqGrid('getGridParam', 'colModel');
        for (var i = 0; i < arrNames.length; i++) {
            strNames += arrNames[i].name + ";";
        }
        $.ajax({
            url: url + "&ExportType=2&GridCaptions=" + escape(arrCaptions.join(';')) + "&GridNames=" + escape(strNames) + "&SortBy=" + escape(SortBy),
            type: "POST",
            data: "",
            success: function (data) {

                var a = document.createElement('a');
                var blob = base64ToBlob(data, "xlsx");
                var url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = "PDFDataGrid.pdf";
                a.click();
                window.URL.revokeObjectURL(url);
                try {
                    hideLoder();
                }
                catch (e) {
                }
            },
            error: function (data) {

            }
        });
    });

    $('.PrintIcon').on("click", function (e) {//print
        try {
            ShowLoder();
        }
        catch (e) {
        }
        var strNames = "";
        var arrCaptions = $("#" + gridName).jqGrid('getGridParam', 'colNames');
        var arrNames = $("#" + gridName).jqGrid('getGridParam', 'colModel');
        for (var i = 0; i < arrNames.length; i++) {
            strNames += arrNames[i].name + ";";
        }
        $.ajax({
            url: url + "&ExportType=3&GridCaptions=" + escape(arrCaptions.join(';')) + "&GridNames=" + escape(strNames) + "&SortBy=" + escape(SortBy),
            type: "POST",
            data: "",
            success: function (data) {
                var wnd = window.open("about:blank", "_blank");
                //wnd.document.title  = "RapidVet Print";
                wnd.document.write('<title>RapidVet Print</title>');
                wnd.document.write(data);
                wnd.print();
                try {
                    hideLoder();
                }
                catch (e) {
                }
            },
            error: function (data) {

            }
        });
    });
}
function base64ToBlob(base64, mimetype, slicesize) {
    if (!window.atob || !window.Uint8Array) {
        // The current browser doesn't have the atob function. Cannot continue
        return null;
    }
    mimetype = mimetype || '';
    slicesize = slicesize || 512;
    var bytechars = atob(base64);
    var bytearrays = [];
    for (var offset = 0; offset < bytechars.length; offset += slicesize) {
        var slice = bytechars.slice(offset, offset + slicesize);
        var bytenums = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            bytenums[i] = slice.charCodeAt(i);
        }
        var bytearray = new Uint8Array(bytenums);
        bytearrays[bytearrays.length] = bytearray;
    }
    return new Blob(bytearrays, { type: mimetype });
};

