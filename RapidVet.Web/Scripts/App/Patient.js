﻿
//this file regards the patient entity


//initiates the patient create form
function InitCreateForm() {
    //$('#animal').cascadingDropdown({
    //    selectBoxes: [
    //        {
    //            selector: '.step1',
    //            paramName: 'id',
    //        },
    //        {
    //            selector: '.step2',
    //            url: '/AnimalRaces/RaceByAnimalKind',
    //            textKey: 'Value',
    //            valueKey: 'Id',
    //            requires: ['.step1'],
    //            requireAll: true,
    //        }]
    //});

    var now = new Date();

    $('#Age').change(AgeToDate);
    $('#BirthDate').change(DateToAge);

    if (!isEmptyOrWhiteSpaces($('#BirthDate').val())) {
        DateToAge();
    }

    $('#sterilizationContainer').click(SterilizationDate);
    $("#BirthDate").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-20:+0",

        //minDate: new Date(now.getFullYear()-100,now.getMonth(),now.getDay()),
        //maxDate: now
    });
    //$("#BirthDate").datepicker("option", "dateFormat", "dd/mm/yy");
    $("#profilePic").rules("add", {
        accept: "jpg|jpeg|png|ico|bmp"
    });
    InitCommentBar();
}


//initiates the patient edit form
function InitEditForm() {
   
    //$('#animal').cascadingDropdown({
    //    selectBoxes: [
    //        {
    //            selector: '.step1',
    //            paramName: 'id',
    //        },
    //        {
    //            selector: '.step2',
    //            url: '/AnimalRaces/RaceByAnimalKind',
    //            textKey: 'Value',
    //            valueKey: 'Id'
    //        }]
    //});
    var now = new Date();

    $('#Age').change(AgeToDate);
    $("#BirthDate").datepicker({
        //minDate: new Date(now.getFullYear() - 100, now.getMonth(), now.getDay()),
        changeMonth: true,
        changeYear: true,
        yearRange: "-20:+0",
        maxDate: now
    });

    $('#BirthDate').change(DateToAge);

    if (!isEmptyOrWhiteSpaces($('#BirthDate').val())) {
        DateToAge();
    }

    //$('#sterilizationContainer').click(SterilizationDate);

    //$("#BirthDate").datepicker();
    //$("#BirthDate").datepicker("option", "dateFormat", "dd/mm/yy");
    $("#profilePic").rules("add", {
        accept: "jpg|jpeg|png|ico|bmp"
    });

    InitCommentBar();
    hideLoder();
}


//append an html containing a new comment
function AppendComment() {
    var source = $('#commentTemplate').children('.comment').clone();
    $('#comments').append(source);
    InitCommentBar();
}


//init the appended comment with proper field names
function InitCommentBar(num) {
    var count = 0;
    $('#comments').children('.comment').each(function () {
        count++;
        var popup = 'popup_' + count;
        var commentBody = 'CommentBody_' + count;
        var validThrough = 'validThrough_' + count;
        var commentId = 'idComment_' + count;
        $(this).find('#ValidThrough').attr('name', validThrough).attr('id', validThrough);
        $(this).find('#CommentBody').attr('name', commentBody).attr('id', commentBody);
        $(this).find('#Popup').attr('name', popup).attr('id', popup);
        $(this).find("input[type='hidden'][name='Popup']").attr('name', popup);
        $(this).find("input[type='hidden'][name='Id']").attr('name', commentId);
    });
    RapidVet.FixInputWidth.init();
    $('.date-selector:visible').datepicker();
    $(".styled:visible").uniform();
}

//calculate age from a date
function AgeToDate() {
    var today = moment();

    //var today = new Date();
    //var curr_month = today.getMonth() + 1;
    //var curr_year = today.getFullYear();
    //var year = 0;
    //var month = 0;
    //var day = today.getDate();
    var age = $('#Age').val().split('.');
    if (age.length == 0) {
        year = curr_year - parseInt(age[0]);
        month = curr_month;
        $('#BirthDate').val(day + '/' + month + '/' + year);
        return;
    } else if (age.length == 1) {
        var fullAge = [parseInt(age[0]), 0, 0];
    }
    else if (age.length > 1) {
        if (age[1].length == 1) {
            age[1] = age[1] + "0";
        }
        var fullAge = [parseInt(age[0]), parseInt((parseFloat(age[1].substring(0, 2))) / 100 * 365 / 30), parseInt((parseFloat(age[1].substring(0, 2))) / 100 * 365 % 30)];
    }

    //if (fullAge[1] == 0) {
    //    fullAge[1] = 12;        
    //}
    //if (fullAge[2] == 0) {
    //    fullAge[2] = 31;
    //}

    $('#BirthDate').val(today.subtract(fullAge[0] * 365 + parseInt(fullAge[1] * 30.417) + fullAge[2], 'days').format("DD/MM/YYYY"));

    //if (curr_month > fullAge[1]) {
    //    month = curr_month - fullAge[1];
    //    year = curr_year - fullAge[0];
    //}
    //else if (fullAge[1] > curr_month) {
    //    month = curr_month - fullAge[1] + 12;
    //    year = curr_year - fullAge[0] - 1;
    //}
    //else if (fullAge[1] == curr_month) {
    //    month = curr_month;
    //    year = curr_year - fullAge[0] - 1;
    //}

    //if (day > fullAge[2]) {
    //    day = day - fullAge[2];
    //}
    //else if (day < fullAge[2]) {
    //    day = fullAge[2] - day;
    //}
    //else // day =  fullAge[2]
    //{

    //}
    //if (fullAge[2] == 30)
    //{
    //    year = year + 1;
    //    day = today.getDate()

    //    if(fullAge[2] == 12)
    //    {
    //        year = year + 1;
    //    }
    //}


    //$('#BirthDate').val(minTwoDigits(day) + '/' + minTwoDigits(month) + '/' + year);
}

function minTwoDigits(n) {
    return (n < 10 ? '0' : '') + n;
}

function DateToAge() {
    var age = getAge($("#BirthDate").val(),3);

    $('#Age').val(age);
    //  $('#Age').attr('disabled', 'disabled');

}

function getAge(dateString, dateType) {
    /*
       function getAge
       parameters: dateString dateType
       returns: boolean
    
       dateString is a date passed as a string in the following
       formats:
    
       type 1 : 19970529
       type 2 : 970529
       type 3 : 29/05/1997
       type 4 : 29/05/97
    
       dateType is a numeric integer from 1 to 4, representing
       the type of dateString passed, as defined above.
    
       Returns string containing the age in years, months and days
       in the format yyy years mm months dd days.
       Returns empty string if dateType is not one of the expected
       values.
    */

    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    if (dateType == 1)
        var dob = new Date(dateString.substring(0, 4),
                            dateString.substring(4, 6) - 1,
                            dateString.substring(6, 8));
    else if (dateType == 2)
        var dob = new Date(dateString.substring(0, 2),
                            dateString.substring(2, 4) - 1,
                            dateString.substring(4, 6));
    else if (dateType == 3)
        var dob = new Date(dateString.substring(6, 10),
                            dateString.substring(3, 5) - 1,
                            dateString.substring(0, 2));
    else if (dateType == 4)
        var dob = new Date(dateString.substring(6, 8),
                            dateString.substring(3, 5) - 1,
                            dateString.substring(0, 2));
    else
        return '';

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();

    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    return monthAge >= 12 || monthAge <= 0 ? yearAge : yearAge + '.' + monthAge;
  //  return yearAge + ' years ' + monthAge + ' months ' + dateAge + ' days'; original return value
 //   return month >= 12 || month <= 0 ? years : years + "." + month;
}



//show sterilization date field only if sterilization checkbox is checked
function SterilizationDate(data) {
    //var className = 'disabled';
    //var container = $('#SterilizationDate').parent('label').parent('div');
    //if ($('#Sterilization').attr('checked')) {
    //    container.removeClass(className);
    //    $('#SterilizationDate').removeAttr('disabled');
    //} else {
    //    if (!container.hasClass(className)) {
    //        container.addClass(className);
    //        $('#SterilizationDate').attr('disabled','disabled');
    //    }
    //}
}

function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null; //str.match(/^$|\s+/) !== null;
}

function validateForm() {
    var valid = true;

    if ($('#comboboxAnimalColor')[0].parentElement.children[1].children[0].value=="") {
        $('#colorError').show();
        valid = false;
    }
   

    if (isEmptyOrWhiteSpaces($('#Name').val())) {
        $('#nameError').show();
        valid = false;
    }
    else {
        $('#nameError').hide();
    }
    
    if (isEmptyOrWhiteSpaces($('#AnimalKindId').val())) {
        $('#kindError').show();
        valid = false;
    }
    else {
        $('#kindError').hide();
    }

    if ($('#comboboxAnimalKind')[0].parentElement.children[1].children[0].value == "") {
        $('#kindError').show();
        valid = false;
    }
    else {
        $('#kindError').hide();
    }


    //if (isEmptyOrWhiteSpaces($('#AnimalRaceId').val())) {
    //    $('#raceError').show();
    //    valid = false;
    //}
    //else {
    //    $('#raceError').hide();
    //}
    if ($('#comboboxAnimalRace')[0].parentElement.children[1].children[0].value == "") {
        $('#AnimalRaceId').val('0');
    }
    else {
        $('#raceError').hide();
    }

    if (valid) {
        ShowLoder();
    }

    return valid;
}
