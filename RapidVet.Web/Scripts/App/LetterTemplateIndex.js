﻿$(function () {

    function PersonalLetterTemplate(data) {
        var self = this;
        self.id = ko.observable(data.Id);
        self.name = ko.observable(data.TemplateName);
        self.editUrl = ko.computed(function () {
            return '/LetterTemplates/EditPersonalTemplate/' + self.id();
        });

    }


    function viewModel() {

        var self = this;
        self.plTemplates = ko.observableArray([]);
        self.modalTitle = ko.observable();
        self.pTemplateName = ko.observable();
        self.pTemplateId = ko.observable();
        self.formAction = ko.observable();
        self.pTemplateToDelete = ko.observable();


        //functions
        self.getPersonalTemplates = function () {
            ShowLoder();

            $.getJSON('/LetterTemplates/PersonalLettersJson', function (data) {
                var mapped = $.map(data, function (item) {
                    return new PersonalLetterTemplate(item);
                });
                self.plTemplates(mapped);
            }).done(function () {
                hideLoder();
            });
        };

        self.showCreatePersonalLetter = function () {
            self.clearModal();
            self.formAction('/LetterTemplates/AddPersonalTemplate');
            self.modalTitle('יצירת תבנית אישית');
            self.showModal();
        };

        self.showEditPersonalTemplate = function (element) {
            self.clearModal();
            self.pTemplateName(element.name());
            self.pTemplateId(element.id());
            self.formAction('/LetterTemplates/EditPersonalTemplateName/' + self.pTemplateId());
            self.modalTitle('עריכת תבנית אישית');
            self.showModal();
        };

        self.submit = function () {
            $('#form').submit();
        };

        self.showDeletePersonalTemplate = function (element) {
            self.pTemplateToDelete(element);
            $('#deletemodal').modal('show');
        };

        self.deletePersonalTemplate = function () {
            $('#deleteForm').submit();
        };

        self.showModal = function () {
            $('#modal').modal('show');
        };

        self.clearModal = function () {
            self.modalTitle(null);
            self.pTemplateName(null);
            self.pTemplateId(null);
            self.formAction(null);
        };


        //execution
        self.getPersonalTemplates();
    }

    ko.applyBindings(new viewModel());

});