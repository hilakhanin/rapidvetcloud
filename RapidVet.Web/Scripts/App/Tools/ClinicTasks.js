﻿function clinicTask(id, name, desc, start, target, status, userId, urgentId) {

    var self = this;
    self.Id = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(desc);
    self.StartDate = ko.observable(start);
    self.TargetDate = ko.observable(target);
    self.Status = ko.observable(status);
    self.DesignatedUserId = ko.observable(userId);
    self.UrgencyId = ko.observable(urgentId);

    //function
    self.remove = function () {
        var qs = {
            taskId: self.Id()
        };
        $.post('ClinicTasks/RemoveClinicTask', $.param(qs), function (res) {
            if (res) {
                viewModel.removeTask(self);
            }
        });
    };

    self.create = function () {
        var qs = {
            clinicTask: ko.toJSON(self)
        };
        $.post('/ClinicTasks/CreateClinicTask', $.param(qs), function (res) {
            getCookieMessages();
            if (res > 0) {
                self.Id(res);
            }
        });
    };
    self.save = function () {
        var qs = {
            clinicTask: ko.toJSON(self)
        };
        $.post('/ClinicTasks/UpdateclinicTask', $.param(qs), function (res) {
            getCookieMessages();
        });
    };

    self.display = ko.observable(true);
}

function SelectListItem(selected, value, text) {
    var self = this;
    self.Selected = ko.observable(selected);
    self.Value = ko.observable(value);
    self.Text = ko.observable(text);
}

function viewModel() {
    var self = this;
    //add filters

    self.clinicTasks = ko.observableArray();

    self.taskStatusOptions = ko.mapping.fromJS([]);
    self.userOptions = ko.mapping.fromJS([]);

    self.taskFilterOptions = ko.mapping.fromJS([]);
    self.statusFilter = ko.observable(-1);

    self.urgencyOptions = ko.mapping.fromJS([]);

    self.shownTasks = ko.computed(function () {
        return ko.utils.arrayFilter(self.clinicTasks(), function (item) {
            if (self.statusFilter() < 0) {
                return true;
            } else {
                return item.Status() == self.statusFilter();
            }
        });
    });

    /*---------------------------add/remove tasks-----------------------------------*/
    self.addTask = function () {
        self.clinicTasks.push(new clinicTask(0, "", "", "", "", 0, 0, 1));
    };
    self.removeTask = function (item) {
        self.clinicTasks.remove(item);
    };

    /*-------------------------------------------------print-----------------------------------*/
    self.print = function () {
        var list = [];
        ko.utils.arrayForEach(self.shownTasks(), function (task) {
            list.push(task.Id());
        });

        window.location = '/ClinicTasks/Print?list=' + JSON.stringify(list);
    };

    /*-----------------------------------init-----------------------------------*/
    self.getTasks = function () {
        $.get('/ClinicTasks/GetTasks', null, function (data) {
            var mapped = $.map(data, function (item) {
                return new clinicTask(item.Id, item.Name, item.Description, item.StartDate, item.TargetDate, item.Status, item.DesignatedUserId, item.UrgencyId);
            });
            self.clinicTasks(mapped);

            self.statusFilter(-1);
            hideLoder();
        });
    };

    self.init = function () {
        ShowLoder();
        $.getJSON('/ClinicTasks/GetUserOptions', function (data) {
            ko.mapping.fromJS(data, self.userOptions);
        });

        $.getJSON('/ClinicTasks/GetStatusOptions', function (data) {
            ko.mapping.fromJS(data, self.taskStatusOptions);
            ko.mapping.fromJS(data, self.taskFilterOptions);
            self.taskFilterOptions.push(new SelectListItem(false, "-1", "הכל"));
        });


        $.getJSON('/ClinicTasks/GetUrgencyOptions', function (data) {
            ko.mapping.fromJS(data, self.urgencyOptions);
        });

        self.getTasks();

        hideLoder();
    };

    self.init();
}