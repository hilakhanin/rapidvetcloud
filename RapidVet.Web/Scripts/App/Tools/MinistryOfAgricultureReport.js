﻿function viewModel() {
    var self = this;

    self.reportTypeOptions = ko.mapping.fromJS([]);
    
    self.reportTypeId = ko.observable();
    self.from = ko.observable();
    self.to = ko.observable();

    self.items = ko.mapping.fromJS([]);

    self.exportToFileUrl = ko.computed(function() {
        var qs = {
            typeId: self.reportTypeId(),
            to: self.from(),
            from: self.to()
        };
        return '/MinistryOfAgriculture/ExportToFile?' + $.param(qs);
    });

    self.print = function () {
        //var selectedReports = [];
        //ko.utils.arrayForEach(self.items(), function (item) {
        //    selectedReports.push(item.Id());
        //});
        var qs = {
            typeId: self.reportTypeId(),
            to: self.from(),
            from: self.to()
        };
        // window.location = '/MinistryOfAgriculture/PrintReports?' + $.param(qs);
        window.open('/MinistryOfAgriculture/PrintReports?' + $.param(qs), '_blank');
    };

    self.search = function () {
        ShowLoder();
        
        var qs = {
            typeId: self.reportTypeId(),
            to: self.from(),
            from: self.to()
        };
        $.get('/MinistryOfAgriculture/GetReports', $.param(qs), function(data) {
            ko.mapping.fromJS(data, self.items);

            hideLoder();
        });
    };

    self.init = function () {
        ShowLoder();
        $.get('/MinistryOfAgriculture/GetReportTypeOptions', function(data) {
            ko.mapping.fromJS(data,self.reportTypeOptions);
            hideLoder();
        });
    };

    self.init();
}