﻿var viewModel;



function Record(data) {
    var self = this;
    self.id = ko.observable(data.Id);
    self.name = ko.observable(data.Name);
    self.address = ko.observable(data.Address);
    self.primaryPhone = ko.observable(data.PrimaryPhone);
    self.secondaryPhone = ko.observable(data.SecondaryPhone);
    self.cellPhone = ko.observable(data.CellPhone);
    self.fax = ko.observable(data.Fax);
    self.contactPerson = ko.observable(data.ContactPerson);
    self.comments = ko.observable(data.Comments);
    self.email = ko.observable(data.Email);
    self.telephoneDirectoryType = ko.observable(data.TelephoneDirectoryType != null ? data.TelephoneDirectoryType.Name : "");
    self.telephoneDirectoryTypeId = ko.observable(data.TelephoneDirectoryTypeId);
    self.editUrl = "TelephoneDirectory/Edit/" + data.Id;
    self.deleteUrl = "TelephoneDirectory/Delete";
}

function SelectItem(value, text) {
    var self = this;
    self.itemValue = value;
    self.itemText = text;
}

function isEmptyOrWhiteSpaces(str){
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null; //str.match(/^$|\s+/) !== null;
}

function viewModel() {

    var self = this;

    self.records = ko.observableArray([]);
    self.telephoneDirectoryTypes = ko.mapping.fromJS([]);
    self.selectedDirectoryType = ko.observable(undefined);
    self.recordToDelete = ko.observable();    
    self.search = ko.observable();
   

    self.computedRecords = ko.computed(function () {
        if (isEmptyOrWhiteSpaces(self.search()) && (typeof self.selectedDirectoryType() == "undefined"))
            return self.records();
        var filteredRecords;
        if(!isEmptyOrWhiteSpaces(self.search()))
        {
            filteredRecords = ko.utils.arrayFilter(self.records(), function (item) {
                return item.name().toLowerCase().indexOf(self.search().toLowerCase()) >= 0 ||
                    (item.address() != null && item.address().toLowerCase().indexOf(self.search().toLowerCase()) >= 0) ||
                    (item.primaryPhone() != null && item.primaryPhone().toLowerCase().indexOf(self.search().toLowerCase()) >= 0) ||
                    (item.secondaryPhone() != null && item.secondaryPhone().toLowerCase().indexOf(self.search().toLowerCase()) >= 0) ||
                    (item.cellPhone() != null && item.cellPhone().toLowerCase().indexOf(self.search().toLowerCase()) >= 0) ||
                    (item.fax() != null && item.fax().toLowerCase().indexOf(self.search().toLowerCase()) >= 0) ||
                    (item.contactPerson() != null && item.contactPerson().toLowerCase().indexOf(self.search().toLowerCase()) >= 0) ||
                    (item.comments() != null && item.comments().toLowerCase().indexOf(self.search().toLowerCase()) >= 0) ||
                    (item.email() != null && item.email().toLowerCase().indexOf(self.search().toLowerCase()) >= 0);
            });

            if (self.selectedDirectoryType() > 0) {
                filteredRecords = ko.utils.arrayFilter(filteredRecords, function (item) {
                    return item.telephoneDirectoryTypeId() != null && item.telephoneDirectoryTypeId() == self.selectedDirectoryType();
                });
            }
        }
        else if(self.selectedDirectoryType() > 0)
        {
            filteredRecords = ko.utils.arrayFilter(self.records(), function (item) {
                return item.telephoneDirectoryTypeId() != null && item.telephoneDirectoryTypeId() == self.selectedDirectoryType();
            });
        }

        return filteredRecords;
    });

    self.selectedRecordsIds = ko.computed(function () {
        var result = '';
        ko.utils.arrayForEach(self.computedRecords(), function (p) {            
                result = p.id() + ',' + result;
            }
        );

        return result;
    });

    self.showDeleteModal = function (element) {
        self.recordToDelete(element);
        $('#deleteModal').modal('show');
    };

    self.hideDeleteModal = function () {
        $('#deleteModal').modal('hide');
        self.recordToDelete(null);
    };

    self.deleteRecord = function () {
        if (self.recordToDelete() != null) {

            ShowLoder();
            $.ajax({
                type: "POST",
                url: self.recordToDelete().deleteUrl,
                data: { 'id': self.recordToDelete().id()},             
                success: function (data) {
                    self.records.remove(self.recordToDelete());
                    hideLoder();
                    $('#deleteModal').modal('hide');
                    getCookieMessages();
                    
                }
            });
        }

    };
   
    self.print = function () {
        var qs = {
            records: self.selectedRecordsIds()
        };
        // window.location = '/MinistryOfAgriculture/PrintReports?' + $.param(qs);
        window.open('/TelephoneDirectory/Print?' + $.param(qs), '_blank');
        //ShowLoder();
        //$.get('/TelephoneDirectory/Print?records=' + self.selectedRecordsIds(), function () {
        //    hideLoder();
        //});
        //$.ajax({
        //    type: "GET",
        //    url: "/TelephoneDirectory/Print",
        //    data: { 'records': self.selectedRecordsIds() },
        //    success: function (data) {

        //        hideLoder();

        //    }
        //});
    };

    //loading data
    self.getData = function () {

        var url = '/TelephoneDirectory/GetAllRecords/';
        ShowLoder();
        $.getJSON(url, function (data) {
            var mappedDirectoryTypes = $.map(data.directoryTypes, function (type) {
                return new SelectItem(type.Id, type.Name);
            });
            self.telephoneDirectoryTypes(mappedDirectoryTypes);
                      
            
            var mappedRecords = $.map(data.records, function (record) {
                return new Record(record);
            });
            self.records(mappedRecords);

            

        }).done(function () {
            hideLoder();
        });
    };

    

    self.getData();
}

$(function () {

    
    viewModel = new viewModel();
    ko.applyBindings(viewModel);

});