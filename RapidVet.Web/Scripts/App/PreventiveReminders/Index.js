﻿var currentData;
var currentPage = 1;
var MAX_IN_PAGE = 250;
var PAGING_A_ID = 'pageA_';
var PAGE_A_TEMPLATE = '<a id="' + PAGING_A_ID + '{0}" {1} data-bind="click: $root.showPageReminders({0})">{0}</a>';

function toggle(source) {
    checkboxes = document.getElementsByName('preventiveItemIds');
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        checkboxes[i].checked = source.checked;
    }
}

function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null; //str.match(/^$|\s+/) !== null;
}

function addPaginationButtons(pages) {
    var htmlStr = '';// '<a href="#" style="direction: ltr;">&laquo;</a>';
    for (var i = 0; i < pages; i++) {
        htmlStr += PAGE_A_TEMPLATE.split('{0}').join(parseInt(i + 1)).replace('{1}', i == 0 ? 'class="active"' : '');
    }
    //htmlStr += '<a href="#" style="direction: ltr;">&raquo;</a>';
    $('.paginationCUST').html(htmlStr);
}

$(function () {

    function Reminder(data) {

        var self = this;
        self.pmiId = ko.observable(data.PmiID);
        self.clientId = ko.observable(data.ClientId);
        self.clientName = ko.observable(data.ClientName);
        self.clientStatus = ko.observable(data.ClientStatus);
        self.clientAddress = ko.observable(data.ClientAddress);
        self.clientHomePhone = ko.observable(data.ClientCellPhone);
        self.clientCellPhone = ko.observable(data.ClientHomePhone);
        self.clientEmail = ko.observable(data.ClientEmail);
        self.patientId = ko.observable(data.PatientId);
        self.patientName = ko.observable(data.PatientName);
        self.patientKind = ko.observable(data.PatientKind);
        self.priceListItemName = ko.observable(data.PriceListItemName);
        self.priceListItemId = ko.observable(data.PriceListItemId);
        self.comments = ko.observable(data.Comments);
        self.preventiveMedicineItemId = ko.observable(data.PreventiveMedicineItemId);
        self.reminderCount = ko.observable(data.ReminderCount);
        self.scheduledDateStr = ko.observable(data.ScheduledDateStr != null ? moment(data.ScheduledDateStr).format("DD/MM/YYYY") : "");
        self.lastReminderDateStr = ko.observable(data.LastReminderDateStr != null ? moment(data.LastReminderDateStr).format("DD/MM/YYYY") : "");
        self.nextAppointmentDateStr = ko.observable(data.NextAppointmentDateStr != null ? moment(data.NextAppointmentDateStr).format("DD/MM/YYYY") : "");
        self.isPast = ko.observable(data.IsPast);
        self.Selected = ko.observable(data.Selected);
        self.patientFileUrl = ko.computed(function () {
            return '/Patients/Index/' + self.patientId();
        });

        self.clientFileUrl = ko.computed(function () {
            return '/Clients/Patients/' + self.clientId();
        });

        self.editUrl = ko.computed(function () {
            return '/PreventiveMedicineReminders/Edit/' + self.preventiveMedicineItemId();
        });
        //self.Selected = ko.observable(false);
        self.state = ko.observable("");
    }

    function viewModel() {
        var self = this;

        self.descending = "icon-arrow-down";
        self.ascending = "icon-arrow-up";

        self.columns = ko.observableArray([
                { property: "scheduledDateStr", header: "תאריך", type: "date", state: ko.observable(""), headerClass: 'sortable header-cell col2' },
                { property: "clientName", header: "שם הבעלים", type: "string", state: ko.observable(""), headerClass: 'sortable header-cell col3' },
                { property: "patientName", header: "שם החיה", type: "string", state: ko.observable(""), headerClass: 'sortable header-cell col4' },
                { property: "patientKind", header: "סוג החיה", type: "string", state: ko.observable(""), headerClass: 'sortable header-cell col5' },
                { property: "clientStatus", header: "סטטוס", type: "string", state: ko.observable(""), headerClass: 'sortable header-cell col6' },
                { property: "priceListItemName", header: "שם החיסון/טיפול", type: "string", state: ko.observable(""), headerClass: 'sortable header-cell col7' },
                { property: "clientAddress", header: "כתובת", type: "string", state: ko.observable(""), headerClass: 'sortable header-cell col8' },
                { property: "clientHomePhone", header: "טלפון בית", type: "string", state: ko.observable(""), headerClass: 'sortable header-cell col9' },
                { property: "clientCellPhone", header: "טלפון נייד", type: "string", state: ko.observable(""), headerClass: 'sortable header-cell col10' },
                { property: "clientEmail", header: 'דוא"ל', type: "string", state: ko.observable(""), headerClass: 'sortable header-cell col11' },
                { property: "lastReminderDateStr", header: "תזכור אחרון", type: "date", state: ko.observable(""), headerClass: 'sortable header-cell col12' },
                { property: "nextAppointmentDateStr", header: "תור הבא", type: "date", state: ko.observable(""), headerClass: 'sortable header-cell col13' },
                { property: "comments", header: "הערות", type: "string", state: ko.observable(""), headerClass: 'sortable header-cell col14' },
                { property: "reminderCount", header: "#", type: "number", state: ko.observable(""), headerClass: 'sortable header-cell col15' },
        ]);

        self.SmsActive = ko.observable($("#Sms").val());
        self.SmsMultiAnimalsActive = ko.observable($("#MultiAnimalsSms").val());
        self.PostCardsActive = ko.observable($("#PostCards").val());
        self.PostCardsMultiAnimalsActive = ko.observable($("#MultiAnimalsPostCards").val());
        self.EmailActive = ko.observable($("#Email").val());
        self.EmailMultiAnimalsActive = ko.observable($("#MultiAnimalsEmail").val());
        self.LetterActive = ko.observable($("#Letter").val());
        self.LetterMultiAnimalsActive = ko.observable($("#MultiAnimalsLetter").val());
        self.smsCount = ko.observable(0);

        //reminders
        self.reminders = ko.observableArray();
        self.reminderToEdit = ko.observable();
        self.currentPage = ko.observable(1);

        //filters
        self.distinctClients = ko.observable($("#DistinctClients").val() == "True" ? true : false);
        self.filters = ko.mapping.fromJS([]);
        self.selectedFilterId = ko.observable();
        self.filterRename = ko.observable();
        self.filterSaveToId = ko.observable();

        self.regionalCouncils = ko.mapping.fromJS([]);
        self.selectedRegionalCouncilId = ko.observable();


        self.animalKinds = ko.mapping.fromJS([]);
        self.selectedAnimalKindId = ko.observable();

        self.doctors = ko.mapping.fromJS([]);
        self.selectedDoctorId = ko.observable();

        self.preventiveItems = ko.mapping.fromJS([]);
        self.preventiveItemIdsList = ko.observableArray([]);
        self.selectedItemIds = ko.computed(function () {
            var result = '';
            if (self.preventiveItemIdsList().length > 0) {
                result = self.preventiveItemIdsList().toString();
            }
            else {
                result = "";
            }
            return result;
        });

        self.cities = ko.mapping.fromJS([]);
        self.citiesIdsList = ko.observableArray([]);
        self.selectedCityIds = ko.computed(function () {
            var result = '';
            if (self.citiesIdsList().length > 0) {
                result = self.citiesIdsList().toString();
            }
            else {
                result = "";
            }
            return result;
        });

        self.elementIdToDelete = ko.observable(undefined);

        self.removeSelectedRemindersMark = function () {
            for (var i = 0; i < self.reminders().length; i++) {
                if (self.reminders()[i].Selected() == true || self.reminders()[i].Selected() == 'true') {
                    self.reminders()[i].Selected(false);
                }
            }
        };

        self.remindersCountClass = ko.observable();
        self.remindersCount = ko.computed(function () {//שורות באותו דף
            if (self.reminders().length > 0) {
                self.remindersCountClass('showRemindersCount');
            }
            else {
                self.remindersCountClass('hideRemindersCount');
            }

            return self.reminders().length;
        });

        self.selectedRemindersIds = ko.computed(function () {
            var result = '';
            ko.utils.arrayForEach(self.reminders(), function (p) {
                if (p.Selected() == true || p.Selected() == 'true') {
                    result = p.preventiveMedicineItemId() + ',' + result;
                }
            });

            return result;
        });

        self.cssClass = ko.computed(function () {
            if (self.selectedRemindersIds().length == 0) {
                return 'btn btn-primary disabled';
            } else {
                return 'btn btn-primary';
            }

        });

        self.PostCardsDisabled = ko.computed(function () {
            if (self.distinctClients()) {
                if (self.PostCardsMultiAnimalsActive() == "True") {
                    return false;
                }
                else {
                    return true;
                }
            } else {
                if (self.PostCardsActive() == "True") {
                    return false;
                }
                else {
                    return true;
                }
            }
        });


        self.SmsDisabled = ko.computed(function () {
            if (self.distinctClients()) {
                if (self.SmsMultiAnimalsActive() == "True") {
                    return false;
                }
                else {
                    return true;
                }
            } else {
                if (self.SmsActive() == "True") {
                    return false;
                }
                else {
                    return true;
                }
            }
        });

        self.EmailDisabled = ko.computed(function () {
            if (self.distinctClients()) {
                if (self.EmailMultiAnimalsActive() == "True") {
                    return false;
                }
                else {
                    return true;
                }
            } else {
                if (self.EmailActive() == "True") {
                    return false;
                }
                else {
                    return true;
                }
            }
        });

        self.LetterDisabled = ko.computed(function () {
            if (self.distinctClients()) {
                if (self.LetterMultiAnimalsActive() == "True") {
                    return false;
                }
                else {
                    return true;
                }
            } else {
                if (self.LetterActive() == "True") {
                    return false;
                }
                else {
                    return true;
                }
            }
        });


        self.PostCardsCssClass = ko.computed(function () {
            if (self.PostCardsDisabled()) {
                return 'btn btn-primary disabled';
            } else {
                return 'btn btn-primary';
            }
        });

        self.SmsCssClass = ko.computed(function () {
            if (self.SmsDisabled()) {
                return 'btn btn-primary disabled';
            } else {
                return 'btn btn-primary';
            }
        });

        self.EmailCssClass = ko.computed(function () {
            if (self.EmailDisabled()) {
                return 'btn btn-primary disabled';
            } else {
                return 'btn btn-primary';
            }
        });

        self.LetterCssClass = ko.computed(function () {
            if (self.LetterDisabled()) {
                return 'btn btn-primary disabled';
            } else {
                return 'btn btn-primary';
            }
        });

        //setup dates
        var today = new Date();
        var day = today.getDate();
        var month = today.getMonth() + 1; //January is 0!
        if (day < 10) {
            day = '0' + day
        }
        if (month < 10) {
            month = '0' + month
        }

        var year = today.getFullYear();
        self.filterFromDate = ko.observable(moment().month(moment().month()).set('date', 1).format("DD/MM/YYYY"));
        self.filterToDate = ko.observable(moment().endOf('month').format("DD/MM/YYYY"));
        // self.filterFromDate = ko.observable("01/" + month + "/" + year);
        // self.filterToDate = ko.observable((new Date(year, month, 0)).getDate() + "/" + month + "/" + year);
        self.filterFromReminderDate = ko.observable();
        self.filterToReminderDate = ko.observable();
        self.filterOnlyFutureReminders = ko.observable(false);
        self.filterIncludeInactiveClients = ko.observable(false);
        self.filterIncludeInactivePatients = ko.observable(false);
        self.dontDisplayClientsWithFutureCalendarEntries = ko.observable(false);
        self.selectedPreventiveIds = ko.mapping.fromJS([]);

        self.filterUrl = ko.computed(function () {

            var url =
                      '?fromDate=' + self.filterFromDate() +
                      '&toDate=' + self.filterToDate() +
                      '&regionalCouncilId=' + self.selectedRegionalCouncilId() +
                      '&animalKindId=' + self.selectedAnimalKindId() +
                      '&doctorId=' + self.selectedDoctorId() +
                      '&inactiveClients=' + self.filterIncludeInactiveClients() +
                      '&inactivePatients=' + self.filterIncludeInactivePatients() +
                      '&futureOnly=' + self.filterOnlyFutureReminders() +
                      '&itemIds=' + self.preventiveItemIdsList() +
                      '&cityIds=' + self.citiesIdsList() +
                      '&fromReminderDate=' + self.filterFromReminderDate() +
                      '&toReminderDate=' + self.filterToReminderDate() +
                      '&selectedRemindersIds=' + self.selectedRemindersIds() +
                      '&RemoveClientsWithFutureCalendarEntries=' + self.dontDisplayClientsWithFutureCalendarEntries() +
                      '&CurrentPage=' + self.currentPage();

            return url;

        });

        self.distictClientsUrl = ko.computed(function () {
            return '&distinctClients=' + self.distinctClients();
        });


        self.printUrl = ko.computed(function () {
            return '/PreventiveMedicineReminders/Print' + self.filterUrl();
        });

        self.lettersUrl = function () {
            if (!self.LetterDisabled()) {
                link = '/PreventiveMedicineReminders/Letters' + self.filterUrl() + self.distictClientsUrl();
                window.open(link, '_blank');
            }
            else {
                return;
            }

        };

        self.postCardsUrl = function () {
            if (!self.PostCardsDisabled()) {
                link = '/PreventiveMedicineReminders/PostCards' + self.filterUrl() + self.distictClientsUrl();
                window.open(link, '_blank');
            }
            else {
                return;
            }
        };

        self.stickersUrl = ko.computed(function () {
            return '/PreventiveMedicineReminders/Stickers' + self.filterUrl() + self.distictClientsUrl();
        });

        self.excelUrl = ko.computed(function () {
            return '/PreventiveMedicineReminders/Excel' + self.filterUrl() + self.distictClientsUrl();
        });

        self.smsUrl = ko.computed(function () {
            return '/PreventiveMedicineReminders/Sms' + self.filterUrl() + self.distictClientsUrl();
        });

        self.emailUrl = ko.computed(function () {
            return '/PreventiveMedicineReminders/Email' + self.filterUrl() + self.distictClientsUrl();
        });

        //filter functions

        self.getPreventiveMedicineItems = function () {
            $.getJSON('/PreventiveMedicineReminders/GetPreventiveItemsList', function (data) {
                ko.mapping.fromJS(data, self.preventiveItems);
            });
        };

        self.getClinicCities = function () {
            $.getJSON('/MetaData/GetClinicCityList', function (data) {
                ko.mapping.fromJS(data, self.cities);
            });
        };

        self.getClinicDoctors = function () {
            $.getJSON('/MetaData/GetClinicDoctorsSelectList', function (data) {
                ko.mapping.fromJS(data, self.doctors);
            });
        };

        self.getRegionalCouncils = function () {
            $.getJSON('/MetaData/GetRegionalCouncils', function (data) {
                ko.mapping.fromJS(data, self.regionalCouncils);
            });
        };

        self.getAnimalKinds = function () {
            $.getJSON('/MetaData/GetAnimalKinds', function (data) {
                ko.mapping.fromJS(data, self.animalKinds);
            });
        };

        self.getFilters = function () {
            $.getJSON('/PreventiveMedicineReminders/GetFiltersList', function (data) {
                self.filters([]);
                ko.mapping.fromJS(data, self.filters);
            });
        };

        self.loadSelectedFilter = function () {

            if (typeof self.selectedFilterId() != "undefined" && self.selectedFilterId() > 0) {

                ShowLoder();

                $.getJSON('/PreventiveMedicineReminders/GetFilterData/' + self.selectedFilterId(), function (data) {

                    self.selectedRegionalCouncilId(data.RegionalCouncilId);
                    self.selectedAnimalKindId(data.AnimalKindId);
                    self.selectedDoctorId(data.DoctorId);
                    self.filterFromDate(data.FromDateStr);
                    self.filterToDate(data.ToDateStr);
                    self.filterFromReminderDate(data.FromReminderDateStr);
                    self.filterToReminderDate(data.ToReminderDateStr);
                    self.filterOnlyFutureReminders(data.ShowOnlyFutureReminders);
                    self.filterIncludeInactiveClients(data.ShowNotActiveClients);
                    self.filterIncludeInactivePatients(data.ShowNotActivePatients);
                    self.dontDisplayClientsWithFutureCalendarEntries(data.RemoveClientsWithFutureCalendarEntries);
                    if (data.PriceListItemIds == null) {
                        self.preventiveItemIdsList([]);
                    } else {
                        self.preventiveItemIdsList([]);
                        self.preventiveItemIdsList(data.PriceListItemIds.toString().split(','));
                    }
                    if (data.CityIds == null) {
                        self.citiesIdsList([]);
                    } else {
                        self.citiesIdsList([]);
                        self.citiesIdsList(data.CityIds.toString().split(','));
                    }
                    //self.preventiveItemIdsList([]);

                    //for(var i=0;i<data.PriceListItemIds.length;i++)
                    //{
                    //    self.preventiveItemIdsList().push(data.PriceListItemIds[i]);                        
                    //}

                    //ko.mapping.fromJS(data.PriceListItemIds, self.selectedPreventiveIds);

                    //ko.utils.arrayForEach(self.preventiveItems(), function (p) {
                    //    var item = ko.utils.arrayFirst(self.preventiveItemIdsList(), function (f) {
                    //        return f == p.Value();
                    //    });
                    //    if (item == null) {
                    //        p.Selected(false);
                    //    } else {
                    //        p.Selected(true);
                    //    }
                    //});

                }).done(function () { hideLoder(); });
            }
        };


        self.saveFilter = function () {

            ShowLoder();
            self.hideFilterModal();
            $.post('/PreventiveMedicineReminders/SaveFilter/', {
                Id: self.filterSaveToId(),
                Name: self.filterRename(),
                RegionalCouncilId: self.selectedRegionalCouncilId(),
                AnimalKindId: self.selectedAnimalKindId(),
                DoctorId: self.selectedDoctorId(),
                FromDate: self.filterFromDate(),
                ToDate: self.filterToDate(),
                FromReminderDate: self.filterFromReminderDate(),
                ToReminderDate: self.filterToReminderDate(),
                ShowOnlyFutureReminders: self.filterOnlyFutureReminders(),
                ShowNotActiveClients: self.filterIncludeInactiveClients(),
                ShowNotActivePatients: self.filterIncludeInactivePatients(),
                PreventiveItemIdsStr: self.selectedItemIds(),
                CityIdsStr: self.selectedCityIds(),
                RemoveClientsWithFutureCalendarEntries: self.dontDisplayClientsWithFutureCalendarEntries()
            })
                .done(function (data) {
                    if (data == true || data == 'true') {
                        self.getFilters();
                    }
                    hideLoder();
                    getCookieMessages();
                });

        };

        self.showFilterModal = function () {
            self.filterRename(null);
            $('#filterModal').modal('show');

        };

        self.hideFilterModal = function () {
            $('#filterModal').modal('hide');
        };

        //general functions

        self.init = function () {
            ShowLoder();

            self.getPreventiveMedicineItems();
            self.getRegionalCouncils();
            self.getAnimalKinds();
            self.getFilters();
            self.getClinicCities();
            self.getClinicDoctors();

            hideLoder();
        };

        self.getReportData = function () {

            ShowLoder();
            self.removeSelectedRemindersMark();
            $.getJSON('/PreventiveMedicineReminders/GetReportData' + self.filterUrl(), function (data) {
                currentData = data;
                self.reminders([]);
                self.showPageReminders(null);
            }).done(function () {
                hideLoder();
            });
        };

        self.showPageRemindersCUST = function () {
            if (event.target.text == null) {
                return;
            }
            self.showPageReminders(event.target.text);
        };

        self.showPageReminders = function (pageNum) {
            ShowLoder();
            try {
                var lastPage = pageNum == null ? 1 : currentPage;
                currentPage = pageNum == null ? 1 : pageNum;
                if (currentData == null) {
                    return;
                }

                $("#spanCurrentDataCount").text(currentData == null ? 0 : currentData.length);
                var data = currentData.slice((currentPage - 1) * MAX_IN_PAGE, MAX_IN_PAGE * currentPage);
                var mapped = $.map(data, function (item) {
                    return new Reminder(item);
                });
                self.reminders(mapped);
                if (pageNum == null) {//null new data set
                    addPaginationButtons(Math.ceil(currentData.length / MAX_IN_PAGE));
                    if (currentData.length > MAX_IN_PAGE)
                        $('.paginationCUST').removeClass("hidePaging");
                    else
                        $('.paginationCUST').addClass("hidePaging");
                  
                    return;
                }

                $('#' + PAGING_A_ID + lastPage).removeClass("active");
                $('#' + PAGING_A_ID + pageNum).addClass("active");
            }
            catch (err) {
                alert(err.message);
            }
            finally {
                hideLoder();
            }
        };

        //Select prevenite treatment and vaccines items
        self.selectAllAsChecked = function () {
            for (var i = 0; i < self.preventiveItems().length; i++) {
                self.preventiveItems()[i].Selected(true);
            }
        };

        self.showDeleteModal = function (element) {
            self.elementIdToDelete(element.preventiveMedicineItemId());

            $('#deleteModal').modal('show');
        };

        self.clearDeleteModal = function () {
            self.elementIdToDelete(undefined);

            $('#deleteModal').modal('hide');
        };

        self.deleteReminder = function () {

            ShowLoder();

            $.post('/PreventiveMedicineReminders/DeleteReminder', {
                ReminderId: self.elementIdToDelete()
            }).done(function () {
                hideLoder();
                getCookieMessages();
                self.clearDeleteModal();
                self.getReportData();
            });

        };

        //sending functions
        self.emailClients = function () {
            if (!self.EmailDisabled()) {
                ShowLoder();

                $.post('/PreventiveMedicineReminders/Email', {
                    FromDate: self.filterFromDate(),
                    ToDate: self.filterToDate(),
                    FromReminderDate: self.filterFromReminderDate(),
                    ToReminderDate: self.filterToReminderDate(),
                    RegionalCouncilId: self.selectedRegionalCouncilId(),
                    AnimalKindId: self.selectedAnimalKindId(),
                    DoctorId: self.selectedDoctorId(),
                    FutureOnly: self.filterOnlyFutureReminders(),
                    InactiveClients: self.filterIncludeInactiveClients(),
                    InactivePatients: self.filterIncludeInactivePatients(),
                    ItemIds: self.selectedItemIds(),
                    CityIds: self.selectedCityIds(),
                    DistinctClients: self.distinctClients(),
                    SelectedRemindersIds: self.selectedRemindersIds(),
                    RemoveClientsWithFutureCalendarEntries: self.dontDisplayClientsWithFutureCalendarEntries()
                }).done(function () {
                    hideLoder();
                    //getCookieMessages();
                    location.reload();
                });
            }
        };

        self.markAsReminded = function () {
            if (self.cssClass() == "btn btn-primary") {
                ShowLoder();
                
                $.post('/PreventiveMedicineReminders/MarkAsReminded', {
                    FromDate: self.filterFromDate(),
                    ToDate: self.filterToDate(),
                    FromReminderDate: self.filterFromReminderDate(),
                    ToReminderDate: self.filterToReminderDate(),
                    RegionalCouncilId: self.selectedRegionalCouncilId(),
                    AnimalKindId: self.selectedAnimalKindId(),
                    DoctorId: self.selectedDoctorId(),
                    FutureOnly: self.filterOnlyFutureReminders(),
                    InactiveClients: self.filterIncludeInactiveClients(),
                    InactivePatients: self.filterIncludeInactivePatients(),
                    ItemIds: self.selectedItemIds(),
                    CityIds: self.selectedCityIds(),
                    DistinctClients: self.distinctClients(),
                    SelectedRemindersIds: Checked(),//self.selectedRemindersIds(),
                    RemoveClientsWithFutureCalendarEntries: self.dontDisplayClientsWithFutureCalendarEntries()
                }).done(function () {
                    hideLoder();
                    //getCookieMessages();
                    self.removeSelectedRemindersMark();
                    self.getReportData();
                });
            }
        };

        self.clearSMSModal = function () {
            $('#sendSmsModal').modal('hide');
        };
        self.smsClients = function () {
            if (!self.SmsDisabled()) {
                ShowLoder();
                self.smsCount(0);

                $.post('/PreventiveMedicineReminders/CountSmsMessages', {
                    FromDate: self.filterFromDate(),
                    ToDate: self.filterToDate(),
                    FromReminderDate: self.filterFromReminderDate(),
                    ToReminderDate: self.filterToReminderDate(),
                    RegionalCouncilId: self.selectedRegionalCouncilId(),
                    AnimalKindId: self.selectedAnimalKindId(),
                    DoctorId: self.selectedDoctorId(),
                    FutureOnly: self.filterOnlyFutureReminders(),
                    InactiveClients: self.filterIncludeInactiveClients(),
                    InactivePatients: self.filterIncludeInactivePatients(),
                    ItemIds: self.selectedItemIds(),
                    CityIds: self.selectedCityIds(),
                    DistinctClients: self.distinctClients(),
                    SelectedRemindersIds: Checked(),//self.selectedRemindersIds(),
                    RemoveClientsWithFutureCalendarEntries: self.dontDisplayClientsWithFutureCalendarEntries()
                }, function (data) {
                    
                    var numberOfMessages = parseInt(data);
                    if (numberOfMessages > 1) {
                        self.smsCount(numberOfMessages);
                        hideLoder();
                        $('#sendSmsModal').modal('show');
                    }
                    else if (numberOfMessages == 1) {
                        self.sendSmsAfterVerification();
                    }
                    else {
                        hideLoder();
                        //location.reload();
                    }
                });
            }
        };

        self.sendSmsAfterVerification = function () {
            ShowLoder();
            $('#sendSmsModal').modal('hide');
            $.post('/PreventiveMedicineReminders/Sms', {
                FromDate: self.filterFromDate(),
                ToDate: self.filterToDate(),
                FromReminderDate: self.filterFromReminderDate(),
                ToReminderDate: self.filterToReminderDate(),
                RegionalCouncilId: self.selectedRegionalCouncilId(),
                AnimalKindId: self.selectedAnimalKindId(),
                DoctorId: self.selectedDoctorId(),
                FutureOnly: self.filterOnlyFutureReminders(),
                InactiveClients: self.filterIncludeInactiveClients(),
                InactivePatients: self.filterIncludeInactivePatients(),
                ItemIds: self.selectedItemIds(),
                CityIds: self.selectedCityIds(),
                DistinctClients: self.distinctClients(),
                SelectedRemindersIds: Checked(),//self.selectedRemindersIds(),
                RemoveClientsWithFutureCalendarEntries: self.dontDisplayClientsWithFutureCalendarEntries()
            }).done(function () {
                hideLoder();
                location.reload();
                //getCookieMessages();
            });
        }

        self.sortClick = function (column) {
            try {

                if (self.reminders().length > 0) {
                    // Call this method to clear the state of any columns OTHER than the target
                    // so we can keep track of the ascending/descending
                    self.clearColumnStates(column);
                    // Get the state of the sort type
                    if (column.state() === "" || column.state() === self.descending) {
                        column.state(self.ascending);
                    }
                    else {
                        column.state(self.descending);
                    }
                    switch (column.type) {
                        case "number":
                            self.numberSort(column);
                            break;
                        case "date":
                            self.dateSort(column);
                            break;
                        case "object":
                            self.objectSort(column);
                            break;
                        case "string":
                        default:
                            self.stringSort(column);
                            break;
                    }
                }
            }
            catch (err) {
                // Always remember to handle those errors that could occur during a user interaction
                alert(err);
            }
            document.cookie = "currentSortProperty=" + column.property;
            document.cookie = "currentSortState=" + column.state().replace('icon-arrow-', '');
        };
        //#region Sort Methods
        self.sortClick = function (column) {
            try {
                // Call this method to clear the state of any columns OTHER than the target
                // so we can keep track of the ascending/descending
                self.clearColumnStates(column);
                // Get the state of the sort type
                if (column.state() === "" || column.state() === self.descending) {
                    column.state(self.ascending);
                }
                else {
                    column.state(self.descending);
                }
                switch (column.type) {
                    case "number":
                        self.numberSort(column);
                        break;
                    case "date":
                        self.dateSort(column);
                        break;
                    case "object":
                        self.objectSort(column);
                        break;
                    case "string":
                    default:
                        self.stringSort(column);
                        break;
                }
            }
            catch (err) {
                // Always remember to handle those errors that could occur during a user interaction
                alert(err);
            }
            document.cookie = "currentSortProperty=" + column.property;
            document.cookie = "currentSortState=" + column.state().replace('icon-arrow-', '');
        };
        // Sort strings
        self.stringSort = function (column) { // Pass in the column object
            self.reminders(self.reminders().sort(function (a, b) {
                // Set strings to lowercase to sort in a predictive way
                var playerA = a[column.property]();
                var playerB = b[column.property]();

                if (!isEmptyOrWhiteSpaces(playerA)) {
                    playerA = playerA.toLowerCase();
                }
                else {
                    playerA = " ";
                }
                if (!isEmptyOrWhiteSpaces(playerB)) {
                    playerB = playerB.toLowerCase();
                } else {
                    playerB = " ";
                }

                if (playerA < playerB) {
                    return (column.state() === self.ascending) ? -1 : 1;
                }
                else if (playerA > playerB) {
                    return (column.state() === self.ascending) ? 1 : -1;
                }
                else {
                    return 0
                }
            }));
        };
        // Sort numbers
        self.numberSort = function (column) {
            self.reminders(self.reminders().sort(function (a, b) {
                var playerA = a[column.property](), playerB = b[column.property]();
                if (column.state() === self.ascending) {
                    return playerA - playerB;
                }
                else {
                    return playerB - playerA;
                }
            }));
        };
        // Sort by date
        self.dateSort = function (column) {
            self.reminders(self.reminders().sort(function (a, b) {
                var dateA = moment(a[column.property](), 'DD/MM/YYYY');
                var dateB = moment(b[column.property](), 'DD/MM/YYYY');
                if (column.state() === self.ascending) {
                    return dateA.diff(dateB, 'days');
                    // return new Date(a[column.property]) - new Date(b[column.property]);
                }
                else {
                    return dateB.diff(dateA, 'days');
                    //return new Date(b[column.property]) - new Date(a[column.property]);
                }
            }));
        };
        // Using a deep get method to find nested object properties
        self.objectSort = function (column) {
            self.reminders(self.reminders().sort(function (a, b) {
                var playerA = self.deepGet(a, column.property),
                    playerB = self.deepGet(b, column.property);
                if (playerA < playerB) {
                    return (column.state() === self.ascending) ? -1 : 1;
                }
                else if (playerA > playerB) {
                    return (column.state() === self.ascending) ? 1 : -1;
                }
                else {
                    return 0
                }
            }));
        };

        self.clearColumnStates = function (selectedColumn) {
            var otherColumns = self.columns().filter(function (col) {
                return col != selectedColumn;
            });
            for (var i = 0; i < otherColumns.length; i++) {
                otherColumns[i].state("");
            }
        };
        self.deepGet = function (object, path) {
            var paths = path.split('.'),
                current = object;
            for (var i = 0; i < paths.length; ++i) {
                if (current[paths[i]] == undefined) {
                    return undefined;
                } else {
                    current = current[paths[i]];
                }
            }
            // If the value of current is not a number, return a lowercase string. If it is, return a number.
            return (isNaN(current)) ? current.toLowerCase() : new Number(current);
        };

        //#endregion
        //execution
        self.init();
    }

    ko.bindingHandlers['class'] = {
        update: function (element, valueAccessor) {
            var currentValue = ko.utils.unwrapObservable(valueAccessor()),
                prevValue = element['__ko__previousClassValue__'],

                // Handles updating adding/removing classes
                addOrRemoveClasses = function (singleValueOrArray, shouldHaveClass) {
                    if (Object.prototype.toString.call(singleValueOrArray) === '[object Array]') {
                        ko.utils.arrayForEach(singleValueOrArray, function (cssClass) {
                            var value = ko.utils.unwrapObservable(cssClass);
                            ko.utils.toggleDomNodeCssClass(element, value, shouldHaveClass);
                        });
                    } else if (singleValueOrArray) {
                        ko.utils.toggleDomNodeCssClass(element, singleValueOrArray, shouldHaveClass);
                    }
                };

            // Remove old value(s) (preserves any existing CSS classes)
            addOrRemoveClasses(prevValue, false);

            // Set new value(s)
            addOrRemoveClasses(currentValue, true);

            // Store a copy of the current value
            element['__ko__previousClassValue__'] = currentValue.concat();
        }
    };

    ko.applyBindings(new viewModel());

});
function Checked() {
    var result = '';
   
    for (var i = 0; i < $('.preventiveItemIds').length ; i++) {
        if ($('.preventiveItemIds')[i].checked) {
            result = $('.preventiveItemIds')[i].id + ',' + result;
        }
    }
    return result;
}