﻿$(function () {


    function viewModel() {

        var self = this;
        self.from = ko.observable($('#from').val());
        self.to = ko.observable($('#to').val());

        self.animalKinds = ko.mapping.fromJS([]);
        self.animalKindId = ko.observable();

        self.preventiveMedicineItems = ko.mapping.fromJS([]);
        self.preventiveItemIdsList = ko.observableArray([]);
        //self.selectedPreventiveItems = ko.computed(function () {
        //    var result = [];
        //    ko.utils.arrayFilter(self.preventiveMedicineItems(), function (p) {
        //        if (p.Selected() == true) {
        //            result.push(p.Value());
        //        }
        //    });

        //    if (result.length > 0) {
        //        result = ko.toJSON(result);
        //    }

        //    return result;
        //});

        self.includeInactivePatients = ko.observable(false);
        self.includeExternalPreformances = ko.observable(false);

        self.filterUrl = ko.computed(function () {
            return '?from=' + self.from() +
                '&to=' + self.to() +
                '&animalKindId=' + self.animalKindId() +
                '&includeInactivePatients=' + self.includeInactivePatients() +
                '&includeExternalPreformances=' + self.includeExternalPreformances() +
                '&selectedItems=' + self.preventiveItemIdsList();
        });

        self.printUrl = ko.computed(function () {
            return '/PreformedPreventive/Print' + self.filterUrl();
        });

        self.exportUrl = ko.computed(function () {
            return '/PreformedPreventive/Export' + self.filterUrl();
        });

        self.results = ko.mapping.fromJS([]);

        //functions
        self.search = function () {

            ShowLoder();

            $.getJSON('/PreformedPreventive/GetData' + self.filterUrl(), function (data) {
                ko.mapping.fromJS(data, self.results);
            }).done(function () { hideLoder(); });
        };

        self.getAnimalKinds = function () {

            $.getJSON('/MetaData/GetAnimalKinds', function (data) {
                ko.mapping.fromJS(data, self.animalKinds);
            });
        };

        self.getPreventiveMedicineItems = function () {
            $.getJSON('/PreventiveMedicineReminders/GetPreventiveItemsList', function (data) {
                ko.mapping.fromJS(data, self.preventiveMedicineItems);
            });
        };

        self.init = function () {
            ShowLoder();

            self.getAnimalKinds();
            self.getPreventiveMedicineItems();

            hideLoder();
        };

        //executions
        self.init();
    }

    ko.applyBindings(new viewModel());


});