﻿function selectLetterTemplateItem(id, name, content) {
    var self = this;
    self.Id = id;
    self.Name = name;
    self.Content = content;
}

function viewModel(lettersActive, emailActive, smsActive, stickerActive) {

    var self = this;
    //activation flags
    self.lettersActive = ko.observable(lettersActive == "True");
    self.emailActive = ko.observable(emailActive == "True");
    self.smsActive = ko.observable(smsActive == "True");
    self.stickerActive = ko.observable(stickerActive == "True");

    //search criteria
    self.criteriaChanged = ko.observable(false);
    self.AllMonth = ko.observable(false);

    self.todaysDate = ko.computed(function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        return dd + '/' + mm + '/' + yyyy;
    });
    self.Date = ko.observable(self.todaysDate());

    //Personal Letter
    self.personalLetters = ko.mapping.fromJS([]);
    self.selectedLetterId = ko.observable("");
    self.selectedLetterContent = ko.observable("");
    self.personalTemplateError = ko.observable(false);
    self.followUpId = ko.observable("");

    self.selectedLetterId.subscribe(function () {
        if (self.selectedLetterId() == "") {
            self.selectedLetterContent("");
        }
        else {
            for (var i = 0; i < self.personalLetters().length; i++) {
                if (self.personalLetters()[i].Id == self.selectedLetterId()) {
                    self.selectedLetterContent(self.personalLetters()[i].Content);
                }
            }
        }
    });

    self.getPersonalLetters = function () {
        $.getJSON('/LetterTemplates/GetClinicsPersonalLetters/', function (data) {
            if (data != null) {
                var mapped = $.map(data, function (item) {
                    return new selectLetterTemplateItem(item.Id, item.TemplateName, item.Content);
                });
                self.personalLetters(mapped);
            }
        });
    };


    self.Date.subscribe(function () {
        self.criteriaChanged(true);
    });
    self.AllMonth.subscribe(function () {
        self.criteriaChanged(true);
    });

    //filters
    self.clientStatusOptions = ko.mapping.fromJS([]);
    self.clientStatusFilter = ko.observable();

    //results
    self.items = ko.mapping.fromJS([]);

    self.filteredItems = ko.computed(function () {
        if (typeof self.clientStatusFilter() === 'undefined') {
            return self.items();
        }
        return ko.utils.arrayFilter(self.items(), function (item) {
            return item.ClientStatusId() == self.clientStatusFilter();
        });
    });



    self.displayResults = ko.observable(false);

    //print results
    self.printLink = ko.computed(function () {
        return '/Birthdays/PrintBirthdays?birthday=' + self.Date() + '&allMonth=' + self.AllMonth() + '&statusId=' + self.clientStatusFilter();
    });

    self.print = function () {
        window.open(self.printLink());
    };

    self.printLettersLink = ko.computed(function () {
        return '/Birthdays/BirthdayLetters?birthday=' + self.Date() + '&allMonth=' + self.AllMonth() + '&statusId=' + self.clientStatusFilter();
    });

    self.printLetters = function () {
        window.open(self.printLettersLink());
    };

    self.printStickersLink = ko.computed(function () {
        return '/Birthdays/BirthdayStickers?birthday=' + self.Date() + '&allMonth=' + self.AllMonth() + '&statusId=' + self.clientStatusFilter();
    });
    self.printStickers = function () {
        window.open(self.printStickersLink());
    };

    //send results

    self.sendEmails = function () {
        ShowLoder();
        var qs =
        {
            birthday: self.Date(),
            allMonth: self.AllMonth(),
            statusId: self.clientStatusFilter()
        };
        $.get('/Birthdays/BirthdayEmails', $.param(qs), function (res) {
            getCookieMessages();
            hideLoder();
        });
    };

    self.sendSms = function () {
        ShowLoder();
        var qs =
     {
         birthday: self.Date(),
         allMonth: self.AllMonth(),
         statusId: self.clientStatusFilter()
     };
        $.get('/Birthdays/BirthdaySms', $.param(qs), function (res) {
            getCookieMessages();
            hideLoder();
        });
    };
    //get data
    self.getBirthdays = function () {
        ShowLoder();
        var qs = {
            birthday: self.Date(),
            allMonth: self.AllMonth()
        };
        $.get('/Birthdays/GetBirthdays', $.param(qs), function (data) {
            ko.mapping.fromJS(data, self.items);
            self.criteriaChanged(false);
            self.displayResults(true);
            hideLoder();
        });
    };

    self.getClientStatuses = function () {
        ShowLoder();
        $.get('/MetaData/GetClientStatuses', null, function (data) {
            ko.mapping.fromJS(data, self.clientStatusOptions);
        }).done(function () {
            hideLoder();
        });

    };

    self.showPersonalLetterModal = function (element) {
        self.personalTemplateError(false);
        self.selectedLetterId("");
       // self.followUpId(element.FollowUpId());
        $('#personalLetterModal').modal('show');
    };
    self.hidePersonalLetterModal = function () {
        self.personalTemplateError(false);
        self.selectedLetterId("");
    //    self.followUpId("");
        $('#personalLetterModal').modal('hide');
    };

    self.sendPersonalLetter = function () {
        if (self.selectedLetterId() > 0) {
            $('#personalLetterModal').modal('hide');
            window.open('/Birthdays/BirthdayLetters/' + "?LetterId=" + self.selectedLetterId() + "&birthday=" + self.Date() + "&allMonth=" + self.AllMonth() + '&statusId=' + self.clientStatusFilter() + "&print=false", '_blank');
            done = true;
        }
        else {
            self.personalTemplateError(true);
        }
    };


    //init
    self.init = function () {
        self.getClientStatuses();
        self.getBirthdays();
        self.getPersonalLetters();
    };

    self.init();
}