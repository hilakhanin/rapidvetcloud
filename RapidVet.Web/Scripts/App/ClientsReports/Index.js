﻿
function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null; //str.match(/^$|\s+/) !== null;
}

function AgeToDate(ageInDecimal) {
    var today = moment();
   
    //  var age = $('#Age').val().split('.');
    var age = ageInDecimal.split('.');
    if (age.length == 0) {
        year = curr_year - parseInt(age[0]);
        month = curr_month;
        $('#BirthDate').val(day + '/' + month + '/' + year);
        return;
    } else if (age.length == 1) {
        var fullAge = [parseInt(age[0]), 0, 0];
    }
    else if (age.length > 1) {
        if (age[1].length == 1) {
            age[1] = age[1] + "0";
        }
        var fullAge = [parseInt(age[0]), parseInt((parseFloat(age[1].substring(0, 2))) / 100 * 365 / 30), parseInt((parseFloat(age[1].substring(0, 2))) / 100 * 365 % 30)];
    }

   return today.subtract(fullAge[0] * 365 + parseInt(fullAge[1] * 30.417) + fullAge[2], 'days').format("DD/MM/YYYY");
}


$(function () {

    // mapper for list item
    function selectItem(value, text) {
        var self = this;
        self.Value = value;
        self.Text = text;
    }

    function selectLetterTemplateItem(id, name, content) {
        var self = this;
        self.Id = id;
        self.Name = name;
        self.Content = content;
    }

    function report(data) {
        var self = this;

        //DateTime
        if (String(data.clientCreateDate).indexOf('/Date(') == 0) {
            var tmp = new Date(parseInt(data.clientCreateDate.replace(/\/Date\((.*?)\)\//gi, "$1")));
            self.clientCreateDate = ko.observable([tmp.getDate(), tmp.getMonth() + 1, tmp.getFullYear()].join('/'));
        } else {
            self.clientCreateDate = ko.observable(data.clientCreateDate);
        }
        if (String(data.clientDetailsUpdatedDate).indexOf('/Date(') == 0) {
            var tmp = new Date(parseInt(data.clientDetailsUpdatedDate.replace(/\/Date\((.*?)\)\//gi, "$1")));
            self.clientDetailsUpdatedDate = ko.observable([tmp.getDate(), tmp.getMonth() + 1, tmp.getFullYear()].join('/'));
        } else {
            self.clientDetailsUpdatedDate = ko.observable(data.clientDetailsUpdatedDate);
        }
        if (String(data.lastTreatmentDate).indexOf('/Date(') == 0) {
            var tmp = new Date(parseInt(data.lastTreatmentDate.replace(/\/Date\((.*?)\)\//gi, "$1")));
            self.lastTreatmentDate = ko.observable([tmp.getDate(), tmp.getMonth() + 1, tmp.getFullYear()].join('/'));
        } else {
            self.lastTreatmentDate = ko.observable(data.lastTreatmentDate);
        }
        
        self.idCard = ko.observable(data.idCard);
        self.workPhone = ko.observable(data.workPhone);
        self.homePhone = ko.observable(data.homePhone);
        self.cellPhone = ko.observable(data.cellPhone);
        self.address = ko.observable(data.address);
        self.treatingDoc = ko.observable(data.treatingDoc);
        self.email = ko.observable(data.email);
        self.localId = ko.observable(data.localId);
        self.referedBy = ko.observable(data.referedBy);
        self.clientStatus = ko.observable(data.clientStatus);
        self.clientTitle = ko.observable(data.clientTitle);
        self.clientName = ko.observable(data.clientName);
        self.clientAge = ko.observable(data.clientAge);
        self.balance = ko.observable(data.balance);
        self.patientName = ko.observable(data.patientName);
        self.animalKind = ko.observable(data.animalKind);
        self.animalRace = ko.observable(data.animalRace);
        self.animalGender = ko.observable(data.animalGender);
        self.patientComments = ko.observable(data.patientComments);
        self.externalVet = ko.observable(data.externalVet);
        self.animalColor = ko.observable(data.animalColor);
        self.animalAge = ko.observable(data.animalAge);
        self.pureBred = ko.observable(data.pureBred);
        self.sterilized = ko.observable(data.sterilized);
        self.electronicNum = ko.observable(data.electronicNum);
        self.SAGIRNum = ko.observable(data.SAGIRNum);
        self.licenseNum = ko.observable(data.licenseNum);
        self.patientWeight = ko.observable(data.patientWeight);
        self.owner_farm = ko.observable(data.owner_farm);

        self.clientId = ko.observable(data.clientId);
        self.clientUrl = ko.computed(function () {
            return '/Clients/Patients/' + self.clientId();
        });

    }

    function viewModel() {
        var self = this;

        //reports
        self.reports = ko.observableArray();

        //GeneralFilters
        self.filters = ko.mapping.fromJS([]);
        self.onlyNonFilteredClients = ko.observable("false");
        self.onlyClientsWithoutVisits = ko.observable(false);

        //Client Filters
        self.clientStatuses = ko.mapping.fromJS([]);
        self.selectedClientStatusId = ko.observableArray([]);
        self.onlyWithInactivePatients = ko.observable(false);
        self.onlyWithNumPatientsMoreThan = ko.observable("");
        self.cities = ko.observable("");
        self.selectedCityId = ko.observableArray([]);
        self.emailKeyWord = ko.observable("");
        self.visitedFrom = ko.observable("");
        self.visitedTo = ko.observable("");
        self.createdFrom = ko.observable("");
        self.createdTo = ko.observable("");
        self.ageFrom = ko.observable("");
        self.ageTo = ko.observable("");
        self.doctors = ko.mapping.fromJS([]);
        self.selectedDoctorId = ko.observable("");
        self.externalVetKeyWord = ko.observable("");
        self.onlyWithOrWithoutFutureAppointment = ko.observable("");
        self.noReference = ko.observable("");
        self.referredByKeyWord = ko.observable("");
        self.onlyWithNoVisitFrom = ko.observable("");

        //Animal Filters
        self.animalKinds = ko.mapping.fromJS([]);
        self.selectedAnimalKindId = ko.observableArray([]);
        self.patientActiveStatuses = ko.mapping.fromJS([]);
        self.selectedPatientActiveStatusId = ko.observable("");
        self.animalRaces = ko.mapping.fromJS([]);
        self.selectedAnimalRaceId = ko.observable("");
        self.animalColors = ko.mapping.fromJS([]);
        self.selectedAnimalColorId = ko.observableArray([]);
        self.pureBred = ko.observable(false);
        self.animalGender = ko.observable("0");
        self.sterilized = ko.observable("");
        self.fertilized = ko.observable("");
        self.treatedFrom = ko.observable("");
        self.treatedTo = ko.observable("");
        self.onlyWithNoTreatmentFrom = ko.observable("");
        self.onlyNotVaccined = ko.observable(false);
        self.onlyWithElectornicNumber = ko.observable(false);
        self.onlyGoneThroughTreatment = ko.observable("");
        self.treatments = ko.mapping.fromJS([]);
        self.selectedTreatmentId = ko.observable("");
        self.personalLetters = ko.mapping.fromJS([]);
        self.selectedLetterId = ko.observable("");
        self.selectedLetterContent = ko.observable("");
        self.personalTemplateError = ko.observable(false);
        self.patientAgeFrom = ko.observable("");
        self.patientAgeTo = ko.observable("");

        //Finance Filters
        self.onlyWithPositiveBalance = ko.observable("");
        self.fromBalance = ko.observable("");
        self.toBalance = ko.observable("");
        self.priceListItems = ko.mapping.fromJS([]);
        self.selectedPriceListId = ko.observable("");
        self.paymentsNumMoreThan = ko.observable("");
        self.paymentFrom = ko.observable("");
        self.paymentTo = ko.observable("");

        //tableOptions
        self.idCardCb = ko.observable(true);
        self.workPhoneCb = ko.observable(false);
        self.homePhoneCb = ko.observable(false);
        self.cellPhoneCb = ko.observable(true);
        self.addressCb = ko.observable(true);
        self.treatingDocCb = ko.observable(false);
        self.emailCb = ko.observable(true);
        self.localIdCb = ko.observable(false);
        self.referedByCb = ko.observable(false);
        self.clientStatusCb = ko.observable(false);
        self.clientTitleCb = ko.observable(true);
        self.clientNameCb = ko.observable(true);
        self.clientCreateDateCb = ko.observable(false);
        self.clientDetailsUpdatedDateCb = ko.observable(false);
        self.lastTreatmentDateCb = ko.observable(false);
        self.clientAgeCb = ko.observable(false);
        self.balanceCb = ko.observable(false);
        self.patientNameCb = ko.observable(true);
        self.animalKindCb = ko.observable(false);
        self.animalRaceCb = ko.observable(false);
        self.animalGenderCb = ko.observable(false);
        self.patientCommentsCb = ko.observable(false);
        self.externalVetCb = ko.observable(false);
        self.animalColorCb = ko.observable(false);
        self.animalAgeCb = ko.observable(false);
        self.pureBredCb = ko.observable(false);
        self.sterilizedCb = ko.observable(false);
        self.electronicNumCb = ko.observable(false);
        self.SAGIRNumCb = ko.observable(false);
        self.licenseNumCb = ko.observable(false);
        self.patientWeightCb = ko.observable(false);
        self.owner_farmCb = ko.observable(false);

        self.smsCount = ko.observable(0);

        self.columnNum = ko.computed(function () {
            return Number(self.idCardCb()) +
                Number(self.workPhoneCb()) +
                Number(self.homePhoneCb()) +
                Number(self.cellPhoneCb()) +
                Number(self.addressCb()) +
                Number(self.treatingDocCb()) +
                Number(self.emailCb()) +
                Number(self.localIdCb()) +
                Number(self.referedByCb()) +
                Number(self.clientStatusCb()) +
                Number(self.clientTitleCb()) +
                Number(self.clientNameCb()) +
                Number(self.clientCreateDateCb()) +
                Number(self.clientDetailsUpdatedDateCb()) +
                Number(self.lastTreatmentDateCb()) +
                Number(self.clientAgeCb()) +
                Number(self.balanceCb()) +
                Number(self.patientNameCb()) +
                Number(self.animalKindCb()) +
                Number(self.animalRaceCb()) +
                Number(self.animalGenderCb()) +
                Number(self.patientCommentsCb()) +
                Number(self.externalVetCb()) +
                Number(self.animalColorCb()) +
                Number(self.animalAgeCb()) +
                Number(self.pureBredCb()) +
                Number(self.sterilizedCb()) +
                Number(self.electronicNumCb()) +
                Number(self.SAGIRNumCb()) +
                Number(self.licenseNumCb()) +
                Number(self.patientWeightCb()) +
                Number(self.owner_farmCb()) +
                Number(self.animalColorCb());
        });

        self.resetPatientFilters = function () {
            self.selectedAnimalKindId([]);
            self.selectedPatientActiveStatusId("");
            self.selectedAnimalRaceId("");
            self.selectedAnimalColorId([]);
            self.pureBred(false);
            self.animalGender("0");
            self.sterilized("");
            self.fertilized("");
            self.treatedFrom("");
            self.treatedTo("");
            self.onlyWithNoTreatmentFrom("");
            self.onlyNotVaccined(false);
            self.onlyWithElectornicNumber(false);
            self.onlyGoneThroughTreatment("");
            self.selectedTreatmentId("");
        };
        
        self.toggleAnimalFilter = ko.observable(false);

        //urls
        self.filterUrl = ko.computed(function () {
            var fromAge = !isEmptyOrWhiteSpaces(self.ageFrom()) ? AgeToDate(self.ageFrom()) : "";
            var toAge = !isEmptyOrWhiteSpaces(self.ageTo()) ? AgeToDate(self.ageTo()) : "";
            var patientFromAge = !isEmptyOrWhiteSpaces(self.patientAgeFrom()) ? AgeToDate(self.patientAgeFrom()) : "";
            var patientToAge = !isEmptyOrWhiteSpaces(self.patientAgeTo()) ? AgeToDate(self.patientAgeTo()) : "";
            var url =
                    '?ShowOnlyClientInfo=' + self.toggleAnimalFilter() +
                    '&OnlyNonFilteredClients=' + self.onlyNonFilteredClients() +
                    '&OnlyClientsWithoutVisits=' + self.onlyClientsWithoutVisits() +
                    '&ClientStatusId=' + self.selectedClientStatusId() +
                    '&OnlyWithInactivePatients=' + self.onlyWithInactivePatients() +
                    '&OnlyWithNumPatientsMoreThan=' + self.onlyWithNumPatientsMoreThan() +
                    '&CityId=' + self.selectedCityId() +
                    '&EmailKeyWord=' + self.emailKeyWord() +
                    '&VisitedFrom=' + self.visitedFrom() +
                    '&VisitedTo=' + self.visitedTo() +
                    '&CreatedFrom=' + self.createdFrom() +
                    '&CreatedTo=' + self.createdTo() +
                    '&AgeFrom=' + fromAge +
                    '&AgeTo=' + toAge +
                    '&TreatingDoctorId=' + self.selectedDoctorId() +
                    '&ExternalVetKeyWord=' + self.externalVetKeyWord() +
                    '&OnlyWithOrWithoutFutureAppointment=' + self.onlyWithOrWithoutFutureAppointment() +
                    '&NoReference=' + self.noReference() +
                    '&ReferredByKeyWord=' + self.referredByKeyWord() +
                    '&OnlyWithNoVisitFrom=' + self.onlyWithNoVisitFrom() +
                    '&AnimalKindId=' + self.selectedAnimalKindId() +
                    '&PatientActiveStatus=' + self.selectedPatientActiveStatusId() +
                    '&AnimalRaceId=' + self.selectedAnimalRaceId() +
                    '&AnimalColorId=' + self.selectedAnimalColorId() +
                    '&PureBred=' + self.pureBred() +
                    '&AnimalGender=' + self.animalGender() +
                    '&Sterilized=' + self.sterilized() +
                    '&Fertilized=' + self.fertilized() +
                    '&PatientAgeFrom=' + patientFromAge +
                    '&PatientAgeTo=' + patientToAge +
                    '&TreatedFrom=' + self.treatedFrom() +
                    '&TreatedTo=' + self.treatedTo() +
                    '&OnlyWithNoTreatmentFrom=' + self.onlyWithNoTreatmentFrom() +
                    '&OnlyNotVaccined=' + self.onlyNotVaccined() +
                    '&OnlyWithElectornicNumber=' + self.onlyWithElectornicNumber() +
                    '&OnlyGoneThroughTreatment=' + self.onlyGoneThroughTreatment() +
                    '&TreatmentId=' + self.selectedTreatmentId() +
                    '&OnlyWithPositiveBalance=' + self.onlyWithPositiveBalance() +
                    '&FromBalance=' + self.fromBalance() +
                    '&ToBalance=' + self.toBalance() +
                    '&PriceListId=' + self.selectedPriceListId() +
                    '&PaymentsNumMoreThan=' + self.paymentsNumMoreThan() +
                    '&PaymentFrom=' + self.paymentFrom() +
                    '&PaymentTo=' + self.paymentTo() +
                    '&idCardCb=' + self.idCardCb() +
                    '&workPhoneCb=' + self.workPhoneCb() +    
                    '&homePhoneCb=' + self.homePhoneCb() +
                    '&cellPhoneCb=' + self.cellPhoneCb() +
                    '&addressCb=' + self.addressCb() +
                    '&treatingDocCb=' + self.treatingDocCb() +
                    '&emailCb=' + self.emailCb() +
                    '&localIdCb=' + self.localIdCb() +
                    '&referedByCb=' + self.referedByCb() +
                    '&clientStatusCb=' + self.clientStatusCb() +
                    '&clientTitleCb=' + self.clientTitleCb() +
                    '&clientNameCb=' + self.clientNameCb() +
                    '&clientCreateDateCb=' + self.clientCreateDateCb() +
                    '&clientDetailsUpdatedDateCb=' + self.clientDetailsUpdatedDateCb() +
                    '&lastTreatmentDateCb=' + self.lastTreatmentDateCb() +
                    '&clientAgeCb=' + self.clientAgeCb() +
                    '&balanceCb=' + self.balanceCb() +
                    '&patientNameCb=' + self.patientNameCb() +
                    '&animalKindCb=' + self.animalKindCb() +
                    '&animalRaceCb=' + self.animalRaceCb() +
                    '&animalGenderCb=' + self.animalGenderCb() +
                    '&patientCommentsCb=' + self.patientCommentsCb() +
                    '&externalVetCb=' + self.externalVetCb() +
                    '&animalColorCb=' + self.animalColorCb() +
                    '&animalAgeCb=' + self.animalAgeCb() +
                    '&pureBredCb=' + self.pureBredCb() +
                    '&sterilizedCb=' + self.sterilizedCb() +
                    '&electronicNumCb=' + self.electronicNumCb() +
                    '&SAGIRNumCb=' + self.SAGIRNumCb() +
                    '&licenseNumCb=' + self.licenseNumCb() +
                    '&patientWeightCb=' + self.patientWeightCb() +
                    '&owner_farmCb=' + self.owner_farmCb();
            return url;
        });

        self.printUrl = ko.computed(function () {
            return '/ClientsReports/Print' + self.filterUrl();
        });

        self.stickersUrl = ko.computed(function () {
            return '/ClientsReports/Stickers' + self.filterUrl();
        });

        self.lettersUrl = ko.computed(function () {
            return '/ClientsReports/Letters' + self.filterUrl();
        });

        self.excelUrl = ko.computed(function () {
            return '/ClientsReports/Excel' + self.filterUrl();
        });

        self.smsUrl = ko.computed(function () {
            return '/ClientsReports/Sms' + self.filterUrl();
        });

        self.emailUrl = ko.computed(function () {
            return '/ClientsReports/Email' + self.filterUrl();
        });

        //filter functions

        self.getClientStatuses = function () {
            $.getJSON('/MetaData/GetClientStatuses', function (data) {
                ko.mapping.fromJS(data, self.clientStatuses);
            });
        };

        self.getAnimalKinds = function () {
            $.getJSON('/MetaData/GetAnimalKinds', function (data) {
                ko.mapping.fromJS(data, self.animalKinds);
            });
        };

        self.getDoctors = function () {
            $.getJSON('/Clinics/GetClinicDoctors', function (data) {
                ko.mapping.fromJS(data, self.doctors);
            });
        };

        self.getTreatments = function () {
            $.getJSON('/MetaData/GetTreatments/', function (data) {
                var mapped = $.map(data, function (item) {
                    return new selectItem(item.Id, item.Name);
                });
                self.treatments(mapped);
            });
        };

        self.getPersonalLetters = function () {
            $.getJSON('/LetterTemplates/GetClinicsPersonalLetters/', function (data) {
                if (data != null) {
                    var mapped = $.map(data, function (item) {
                        return new selectLetterTemplateItem(item.Id, item.TemplateName, item.Content);
                    });
                    self.personalLetters(mapped);
                }
            });
        };

        self.getAnimalRaces = function (newValue) {
            if (typeof newValue !== 'undefined') {
                var qs = {
                    kindId: newValue[0]
                };
                $.get('/Patients/GetAnimalRaceOptions', $.param(qs), function (data) {
                    ko.mapping.fromJS(data, self.animalRaces);
                    self.selectedAnimalRaceId("");
                });
            }
        };
        var ClinicId = 0;
        self.getCities = function () {
            $.getJSON('/MetaData/GetCitiesOfClinicById/' + ClinicId, function (data) {
                var mapped = $.map(data, function (item) {
                    return new selectItem(item.Value, item.Text);
                });
                self.cities(mapped);
            });
        };
        
        self.getAnimalColors = function () {
            $.getJSON('/MetaData/GetAnimalColors/', function (data) {
                var mapped = $.map(data, function (item) {
                    return new selectItem(item.Value, item.Text);
                });
                self.animalColors(mapped);
            });
        };

        self.getAnimalActivityStatus = function () {
            var data = [{ Id: "1", Name: "פעיל" }, { Id: "2", Name: "לא פעיל" }];
            var mapped = $.map(data, function (item) {
                return new selectItem(item.Id, item.Name);
            });
            self.patientActiveStatuses(mapped);
        };
        
        self.getTariffs = function () {
            $.getJSON('/MetaData/GetTariffs/', function (data) {
                var mapped = $.map(data, function (item) {
                    return new selectItem(item.Value, item.Text);
                });
                self.priceListItems(mapped);
            });
        };
        
        //Subscribes
        self.selectedAnimalKindId.subscribe(self.getAnimalRaces);
        self.selectedLetterId.subscribe(function () {
            if(self.selectedLetterId() == "")
            {
                self.selectedLetterContent("");
            }
            else
            {
                for(var i =0; i< self.personalLetters().length; i++)
                {
                    if(self.personalLetters()[i].Id == self.selectedLetterId())
                    {
                        self.selectedLetterContent(self.personalLetters()[i].Content);
                    }
                }
            }
        });

        //general functions
        self.init = function () {
            ShowLoder();

            self.getClientStatuses();
            self.getAnimalKinds();
            self.getDoctors();
            self.getTreatments();
            self.getCities();
            self.getAnimalActivityStatus();
            self.getAnimalColors();
            self.getAnimalRaces("");
            self.getTariffs();
            self.getPersonalLetters();

            hideLoder();;
        };

        self.getReportData = function () {

            ShowLoder();

            $.getJSON('/ClientsReports/GetReportData' + self.filterUrl(), function (data) {
                self.reports([]);
                var mapped = $.map(data, function (item) {
                    return new report(item);
                });
                self.reports(mapped);
            }).done(function () {
                hideLoder();
            });
        };

        //sending functions
        self.emailClients = function () {

            ShowLoder();

            $.post('/ClientsReports/Email', {
                //Global Filters
                ShowOnlyClientInfo: self.toggleAnimalFilter(),
                OnlyNonFilteredClients: self.onlyNonFilteredClients(),
                OnlyClientsWithoutVisits: self.onlyClientsWithoutVisits(),
                //Client Filters
                ClientStatusId: self.selectedClientStatusId().toString(),
                OnlyWithInactivePatients: self.onlyWithInactivePatients(),
                OnlyWithNumPatientsMoreThan: self.onlyWithNumPatientsMoreThan(),
                CityId: self.selectedCityId().toString(),
                EmailKeyWord: self.emailKeyWord(),
                VisitedFrom: self.visitedFrom(),
                VisitedTo: self.visitedTo(),
                CreatedFrom: self.createdFrom(),
                CreatedTo: self.createdTo(),
                AgeFrom: !isEmptyOrWhiteSpaces(self.ageFrom()) ? AgeToDate(self.ageFrom()) : "",
                AgeTo: !isEmptyOrWhiteSpaces(self.ageTo()) ? AgeToDate(self.ageTo()) : "",
                TreatingDoctorId: self.selectedDoctorId(),
                ExternalVetKeyWord: self.externalVetKeyWord(),
                OnlyWithOrWithoutFutureAppointment: self.onlyWithOrWithoutFutureAppointment(),
                NoReference: self.noReference(),
                ReferredByKeyWord: self.referredByKeyWord(),
                OnlyWithNoVisitFrom: self.onlyWithNoVisitFrom(),
                //Animal Filters
                AnimalKindId: self.selectedAnimalKindId().toString(),
                PatientActiveStatus: self.selectedPatientActiveStatusId(),
                AnimalRaceId: self.selectedAnimalRaceId(),
                AnimalColorId: self.selectedAnimalColorId().toString(),
                PureBred: self.pureBred(),
                AnimalGender: self.animalGender(),
                Sterilized: self.sterilized(),
                Fertilized: self.fertilized(),
                TreatedFrom: self.treatedFrom(),
                TreatedTo: self.treatedTo(),
                OnlyWithNoTreatmentFrom: self.onlyWithNoTreatmentFrom(),
                OnlyNotVaccined: self.onlyNotVaccined(),
                OnlyWithElectornicNumber: self.onlyWithElectornicNumber(),
                OnlyGoneThroughTreatment: self.onlyGoneThroughTreatment(),
                TreatmentId: self.selectedTreatmentId(),
                PatientAgeFrom: !isEmptyOrWhiteSpaces(self.patientAgeFrom()) ? AgeToDate(self.patientAgeFrom()) : "",
                PatientAgeTo: !isEmptyOrWhiteSpaces(self.patientAgeTo()) ? AgeToDate(self.patientAgeTo()) : "",
                //Finance Filters
                OnlyWithPositiveBalance: self.onlyWithPositiveBalance(),
                FromBalance: self.fromBalance(),
                ToBalance: self.toBalance(),
                PriceListId: self.selectedPriceListId(),
                PaymentsNumMoreThan: self.paymentsNumMoreThan(),
                PaymentFrom: self.paymentFrom(),
                PaymentTo: self.paymentTo()
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });

        };

        self.smsClients = function () {

            ShowLoder();

            $.post('/ClientsReports/CountSmsMessages', {
                //Global Filters
                ShowOnlyClientInfo: self.toggleAnimalFilter(),
                OnlyNonFilteredClients: self.onlyNonFilteredClients(),
                OnlyClientsWithoutVisits: self.onlyClientsWithoutVisits(),
                //Client Filters
                ClientStatusId: self.selectedClientStatusId().toString(),
                OnlyWithInactivePatients: self.onlyWithInactivePatients(),
                OnlyWithNumPatientsMoreThan: self.onlyWithNumPatientsMoreThan(),
                CityId: self.selectedCityId().toString(),
                EmailKeyWord: self.emailKeyWord(),
                VisitedFrom: self.visitedFrom(),
                VisitedTo: self.visitedTo(),
                CreatedFrom: self.createdFrom(),
                CreatedTo: self.createdTo(),
                AgeFrom: !isEmptyOrWhiteSpaces(self.ageFrom()) ? AgeToDate(self.ageFrom()) : "",
                AgeTo: !isEmptyOrWhiteSpaces(self.ageTo()) ? AgeToDate(self.ageTo()) : "",
                TreatingDoctorId: self.selectedDoctorId(),
                ExternalVetKeyWord: self.externalVetKeyWord(),
                OnlyWithOrWithoutFutureAppointment: self.onlyWithOrWithoutFutureAppointment(),
                NoReference: self.noReference(),
                ReferredByKeyWord: self.referredByKeyWord(),
                OnlyWithNoVisitFrom: self.onlyWithNoVisitFrom(),
                //Animal Filters
                AnimalKindId: self.selectedAnimalKindId().toString(),
                PatientActiveStatus: self.selectedPatientActiveStatusId(),
                AnimalRaceId: self.selectedAnimalRaceId(),
                AnimalColorId: self.selectedAnimalColorId().toString(),
                PureBred: self.pureBred(),
                AnimalGender: self.animalGender(),
                Sterilized: self.sterilized(),
                Fertilized: self.fertilized(),
                TreatedFrom: self.treatedFrom(),
                TreatedTo: self.treatedTo(),
                OnlyWithNoTreatmentFrom: self.onlyWithNoTreatmentFrom(),
                OnlyNotVaccined: self.onlyNotVaccined(),
                OnlyWithElectornicNumber: self.onlyWithElectornicNumber(),
                OnlyGoneThroughTreatment: self.onlyGoneThroughTreatment(),
                TreatmentId: self.selectedTreatmentId(),
                PatientAgeFrom: !isEmptyOrWhiteSpaces(self.patientAgeFrom()) ? AgeToDate(self.patientAgeFrom()) : "",
                PatientAgeTo: !isEmptyOrWhiteSpaces(self.patientAgeTo()) ? AgeToDate(self.patientAgeTo()) : "",
                //Finance Filters
                OnlyWithPositiveBalance: self.onlyWithPositiveBalance(),
                FromBalance: self.fromBalance(),
                ToBalance: self.toBalance(),
                PriceListId: self.selectedPriceListId(),
                PaymentsNumMoreThan: self.paymentsNumMoreThan(),
                PaymentFrom: self.paymentFrom(),
                PaymentTo: self.paymentTo()
            }, function (data) {
                var numberOfMessages = parseInt(data);
                if(numberOfMessages > 1)
                {
                    self.smsCount(numberOfMessages);
                    hideLoder();
                    $('#sendSmsModal').modal('show');
                }
                else if(numberOfMessages == 1)
                {
                    self.sendSmsAfterVerification();
                }
            });
        };

        self.clearSMSModal = function () {
            $('#sendSmsModal').modal('hide');
        };

        self.sendSmsAfterVerification = function () {
            ShowLoder();
            $('#sendSmsModal').modal('hide');
            $.post('/ClientsReports/Sms', {
                //Global Filters
                ShowOnlyClientInfo: self.toggleAnimalFilter(),                     
                OnlyNonFilteredClients: self.onlyNonFilteredClients(),             
                OnlyClientsWithoutVisits: self.onlyClientsWithoutVisits(),
                //Client Filters
                ClientStatusId: self.selectedClientStatusId().toString(),          
                OnlyWithInactivePatients: self.onlyWithInactivePatients(),         
                OnlyWithNumPatientsMoreThan: self.onlyWithNumPatientsMoreThan(),       
                CityId: self.selectedCityId().toString(),                              
                EmailKeyWord: self.emailKeyWord(),                                     
                VisitedFrom: self.visitedFrom(),                                       
                VisitedTo: self.visitedTo(),                                           
                CreatedFrom: self.createdFrom(),                                       
                CreatedTo: self.createdTo(),                                           
                AgeFrom: !isEmptyOrWhiteSpaces(self.ageFrom()) ? AgeToDate(self.ageFrom()) : "",
                AgeTo: !isEmptyOrWhiteSpaces(self.ageTo()) ? AgeToDate(self.ageTo()) : "",
                TreatingDoctorId: self.selectedDoctorId(),                                      
                ExternalVetKeyWord: self.externalVetKeyWord(),                                  
                OnlyWithOrWithoutFutureAppointment: self.onlyWithOrWithoutFutureAppointment(),  
                NoReference: self.noReference(),                                                
                ReferredByKeyWord: self.referredByKeyWord(),                                    
                OnlyWithNoVisitFrom: self.onlyWithNoVisitFrom(),
                //Animal Filters
                AnimalKindId: self.selectedAnimalKindId().toString(),                           
                PatientActiveStatus: self.selectedPatientActiveStatusId(),                      
                AnimalRaceId: self.selectedAnimalRaceId(),                                      
                AnimalColorId: self.selectedAnimalColorId().toString(),                         
                PureBred: self.pureBred(),                                                      
                AnimalGender: self.animalGender(),                                              
                Sterilized: self.sterilized(),                                                  
                Fertilized: self.fertilized(),                                                  
                TreatedFrom: self.treatedFrom(),                                                
                TreatedTo: self.treatedTo(),                                                    
                OnlyWithNoTreatmentFrom: self.onlyWithNoTreatmentFrom(),                        
                OnlyNotVaccined: self.onlyNotVaccined(),                                        
                OnlyWithElectornicNumber: self.onlyWithElectornicNumber(),                      
                OnlyGoneThroughTreatment: self.onlyGoneThroughTreatment(),                      
                TreatmentId: self.selectedTreatmentId(),
                PatientAgeFrom: !isEmptyOrWhiteSpaces(self.patientAgeFrom()) ? AgeToDate(self.patientAgeFrom()) : "",
                PatientAgeTo: !isEmptyOrWhiteSpaces(self.patientAgeTo()) ? AgeToDate(self.patientAgeTo()) : "",
                //Finance Filters
                OnlyWithPositiveBalance: self.onlyWithPositiveBalance(),    
                FromBalance: self.fromBalance(),                            
                ToBalance: self.toBalance(),                                
                PriceListId: self.selectedPriceListId(),                    
                PaymentsNumMoreThan: self.paymentsNumMoreThan(),            
                PaymentFrom: self.paymentFrom(),                            
                PaymentTo: self.paymentTo()                      
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        };

        self.showPersonalLetterModal = function (element) {
            self.personalTemplateError(false);
            self.selectedLetterId("");
            $('#personalLetterModal').modal('show');
        };
        self.hidePersonalLetterModal = function () {
            self.personalTemplateError(false);
            self.selectedLetterId("");
            $('#personalLetterModal').modal('hide');            
        };

        self.sendPersonalLetter = function () {
            if (self.selectedLetterId() > 0) {
                $('#personalLetterModal').modal('hide');                
                window.open(self.lettersUrl()+"&LetterId="+ self.selectedLetterId()+"&print=false", '_blank');
                  done = true;                
            }
            else
            {
                self.personalTemplateError(true);
            }
        };


        //execution
        self.init();
    }

    ko.applyBindings(new viewModel());

});