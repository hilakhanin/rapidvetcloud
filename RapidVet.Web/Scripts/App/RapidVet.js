﻿//this file contains generally used functions

$(function () {
    //disable auto-complete
    $('input').attr('autocomplete', 'off');
    
    initDatePickers();

    try {
        $(".styled:visible").uniform();
    }
    catch (e) {
    }
    $('.time').timepicker({
        'timeFormat': 'H:i',
        'step': 30,
        'minTime': '6:00',
        
    });
    //$('.time').timepicker('option', {
    //    'timeFormat': 'H:i',
    //    'step': 30,
    //    'minTime': '9:00am',
    //    'disableTimeRanges': []
    //});

    $('.simple_color').simpleColor({
        cellWidth: 20,
        cellHeight: 20,
        buttonClass: 'button',
        displayColorCode:'false',
        callback: function (hex) {
         //   alert("color picked! " + hex);
        }
    });

    toastr.options.positionClass = "toast-bottom-full-width";

    //Print 
    if (qs['printFinanceDocument'] != undefined) {
        var url = '/FinanceDocument/PrintMultiDocuments?documentIds=' + qs['printFinanceDocument'];
        //window.open(url, '_blank');
        window.location.href = url;
        window.history.pushState({ path: '/' }, '', '/');
    }
});

///shows a lightbox containing data from url
///target - the url to be called to obtain data
///modalId - the div id in the view in which to place the content was called
function GetModal(target, modalId) {
    $('#' + modalId).load(target, function () {
        var formId = $(this).children('form').attr('id');
        $.validator.unobtrusive.parse('#' + formId);
        $('#' + modalId).modal('toggle');
    });
}

///handles ajax callbacks. 
///data - json that is being returned by the action
///data.success - bool flag, did the action succeeded
///data.indexId - the html element id that is to be refreshed with new data
///data.target - the url toi be called to get the above mentioned data
///data.modalId - the element id of the lightbox
function Success(data) {
    if (data.success) {
        if (data.isRedirect == true) {
            if (data.modalId) {
                $('#' + data.modalId).modal('hide');
            }

            location.reload();
        }
        $.get(data.target, function (htmlData) {
            $('#' + data.indexId).html(htmlData);
        });
        $('.modal-backdrop').remove();
        if (data.modalId) {
            $('#' + data.modalId).modal('hide');
        }
    } else {
        $('#' + data.modalId).html(data.data);
    }
    getCookieMessages();
}


///uses bootstrap tabs to preform ajax get calles. 
///is in use in clinic and clinic group configurations screens
///target - the url to get data from
///containerId - to element id in which to place the above mentiond data
///element - the clickd link in the navigation pane
function PageLoad(target, containerId, element) {

    $.ajax({
        type: 'Get',
        url: target,
        beforeSend: function () {
            $('#loder').show();
        },
        success: function (data) {
            $('#loder').hide();
            $(element).parents('ul').find('a').removeClass('active');
            $(element).addClass('active');
            $.validator.unobtrusive.parse('form');
            $('#' + containerId).html(data);
        },
        Failure: function (data) {
            $('#loder').hide();
            alert('Server malfunction');
        }
    });
}

///removes html element from screen.
///is in use in the create / edit screens in patient and client
///data - the element to be removed
///cssclass - css class selector
function RemoveElement(data, cssClass) {
    var element = $(data);
    while (!$(element).hasClass('control-group')) {
        element = element.parent();
    }
    if ($.find('.' + cssClass).length > 2) {
        element.detach();
    }
}


///shows a hidden div
///data - a json obtained from server
function ShowMessage(data) {
    $('#' + data.modalId).modal('hide');
}

function HideValidFormSubmitBtn(formId, btnId) {
    $('#' + formId).submit(function () {
        if ($('#' + formId).valid()) {
            $('#' + btnId).hide();
        }
    });
}

function ShowLoder() {
    $("#loder").show();
}

function hideLoder() {
    $("#loder").hide();
}

function OpenNewWindow(url) {
    var popUp = window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
    if (popUp == null || typeof (popUp) == 'undefined' || popUp.outerHeight === 0) {
        alert('חוסם חלונות קופצים מופעל , אנא בטל אותו ונסה שנית');
    }
    else {
        popUp.focus();
    }

}

function replaceDateSeperator(str) {
    return encodeURIComponent(str);
}

function closeWindow() {
    window.close();
}

function initDatePickers() {
    try {
        $.validator.methods.date = function (value, element) {
            return this.optional(element) || Globalize.parseDate("14/03/2014", "dd/MM/yyyy", "en-GB");
        };
    }
    catch (e) {
    }
    $('.date:visible').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        yearRange: "-100:+10"
    });
}