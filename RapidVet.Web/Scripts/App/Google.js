﻿$(function () {
    getCookieMessages();
})

function onSignIn(googleUser) {
    // Useful data for your client-side scripts:
    var profile = googleUser.getBasicProfile();
    //console.log("ID: " + profile.getId()); // Don't send this directly to your server!
    console.log("Image URL: " + profile.getImageUrl());

    document.getElementById('googleDetailsLbl').innerHTML = profile.getName() + ' - ' + profile.getEmail();
    document.getElementById('GoogleOfflineValidaionMessage').style.display = 'block';
    // The ID token you need to pass to your backend:
    var id_token = googleUser.getAuthResponse().id_token;

    $("#hd1").val(id_token);
    checkOfflineOAuth();
};

function checkOfflineOAuth() {
    var elem = document.getElementById('googleOfflineCheckLbl');
    $.ajax({
        url: '/Google/ValidateOfflineAuth',
        type: "POST",
        data: 'token=' + $("#hd1").val(),
        async: true,
        success: function (data) {
            if (data == 'ok') {
                elem.innerHTML = 'האימות מול גוגל עבר בהצלחה';
                document.getElementById('GoogleSettingsContainer').style.display = 'block';
                elem.className = "googleProflbl googleValid";
            }
            else {
                window.open(data, "_blank");
            }
        },
        error: function (xhr) {
            elem.innerHTML = 'לא ניתן לבצע בדיקת אימות, נא לפנות לתמיכה';
        }
    })
}

function addNewSyncCal() {
    $.ajax({
        url: '/Google/AddNewSyncCal',
        type: "POST",
        data: 'model=' + '{ "AllowSync" : "' + $('#AllowSync').val() + '" }',
        async: true,
        success: function (data) {
            alert('addedOk');
        },
        error: function (xhr) {
            alert('no');
        }
    })
}

function resetGoogleCalendarID(settingID, elem) {
    elem.disabled = true;
    $.ajax({
        url: '/Google/ResetGoogleCalendarID',
        type: "POST",
        data: 'settingID=' + settingID,
        async: true,
        success: function (data) {
            if (data == 'ok') {
                elem.innerHTML = 'Reset succeded';                
            }
            else {
                elem.disabled = false;
                alert('אירעה שגיאה באיפוש קישור יומן');
            }
        },
        error: function (xhr) {
            elem.disabled = false;
            alert('אירעה שגיאה באיפוש קישור יומן');
        }
    })
}