﻿var searchData = [];
$(function () {

    $('#inactivePatients').css('width', '20px');

    $("#clientsForm").keypress(function () {
        if (event.keyCode == 13) {
            $("#btnSearch").focus();
            document.getElementById('btnSearch').click()
        }
    });

    function viewModel() {

        var self = this;
        self.clients = ko.mapping.fromJS([]);
        self.cities = ko.mapping.fromJS([]);
        self.pages = ko.mapping.fromJS([]);
        self.animalKinds = ko.mapping.fromJS([]);

        //search parameters
        self.firstName = ko.observable();
        self.lastName = ko.observable();
        self.idCard = ko.observable();
        self.email = ko.observable();
        self.phone = ko.observable();
        self.selectedCity = ko.observable();
        self.street = ko.observable();
        self.patientName = ko.observable();
        self.selectedAnimalKind = ko.observable();
        self.license = ko.observable();
        self.chip = ko.observable();
        self.sagir = ko.observable();
        self.includeInactivePatients = ko.observable(true);
        self.selectedPage = ko.observable();
        self.searchByContaining = ko.observable($('#SearchByContaining').val() == "True");

        self.clientStatuses = ko.mapping.fromJS([]);
        self.selectedClientStatusId = ko.observableArray([]);

        self.getCities = function () {
            $.getJSON('/MetaData/GetCities', function (data) {
                ko.mapping.fromJS(data, self.cities);
            });
        };

        self.getAnimalKinds = function () {
            $.getJSON('/MetaData/GetAnimalKinds', function (data) {
                ko.mapping.fromJS(data, self.animalKinds);
            });
        };

        self.getClientStatuses = function () {
            $.getJSON('/MetaData/GetClientStatuses', function (data) {
                ko.mapping.fromJS(data, self.clientStatuses);
            });
        };

        self.initData = function () {
            self.getCities();
            self.getAnimalKinds();
            self.getClientStatuses();
        };

        self.goToPatientClick = function (element) {
            if (element != null && element.ClientUrl() != null) {
                location.href = element.ClientUrl();
                return true;
            }
            return false;
        };

        self.search = function () {           
            ShowLoder();
            $("#errorSpan").attr('class', 'hide');
            var fn = self.firstName();
            var ln = self.lastName();
            var idc = self.idCard();
            var em = self.email();
            var ph = self.phone();
            var strt = self.street();
            var pn = self.patientName();
            var pl = self.license();
            var chp = self.chip();
            var sgr = self.sagir();
            var cID = self.selectedCity();
            if ((fn == null || fn.length < 2) && (ln == null || ln.length < 2) && (idc == null || idc.length < 2) && (em == null || em.length < 2) &&
                (ph == null || ph.length < 2) && (strt == null || strt.length < 2) && (pn == null || pn.length < 2) && (pl == null || pl.length < 2) &&
                (chp == null || chp.length < 2) && (sgr == null || sgr.length < 2) && (cID == null || cID < 0))
            {
                $("#errorSpan").attr('class', 'error-message');
                hideLoder();
                return false;
            }

            $.ajax({
                type: "POST",
                url: '/Clients/AdvancedSearch',
                data: {
                    'firstName': fn,
                    'lastName': ln,
                    'idCard': idc,
                    'email': em,
                    'phone': ph,
                    'cityId': cID,
                    'street': strt,
                    'patientName': pn,
                    'animalKindId': self.selectedAnimalKind(),
                    'patientLicense': pl,
                    'chip': chp,
                    'sagir': sgr,
                    'includeInactivePatients': self.includeInactivePatients(),
                    'clientStatus': self.selectedClientStatusId().toString(),
                    'SearchByContaining': self.searchByContaining()
                },
                success: function (data) {                   
                    searchData = data;
                    self.initPaging();
                    self.bindPageDataToView();
                }
            }).done(function () {
                hideLoder();
            });
        };
      
        self.initPaging = function () {
            self.selectedPage = 1;//current page
            var maxRowsInPage = $('#pagesSumSelect').val();//maximum rows to view
            var pagesNum = Math.ceil(searchData.length / maxRowsInPage);
            var tablePages = document.getElementById("tablePages");//page combo box container
            var pagesCount = document.getElementById("lblPagesCount");//total pages count
            var data = [];
            pagesCount.innerText = pagesNum;
            for (var i = 1; i <= pagesNum; i++) {
                data.push(i);
            }
            ko.mapping.fromJS(data, self.pages);
            if (pagesNum == 0)
            {
                tablePages.className = "hide";
            }
            else
            {
                tablePages.className = "";
            }
        };

        self.bindPageDataToView = function () {
            var maxRowsInPage = $('#pagesSumSelect').val();//maximum rows to view
            var skip = (self.selectedPage * maxRowsInPage) - 1;
            var take = ((self.selectedPage - 1) * maxRowsInPage);
            var data = [];
            for (var i = take; i < ((searchData.length < skip) ? searchData.length : skip) ; i++) {
                data.push(searchData[i]);
            }
            ko.mapping.fromJS(data, self.clients);
        };

        self.setNewTotalPages = function () {
            self.initPaging();
            self.bindPageDataToView();
        };

        //execution
        self.initData();
    }

    ko.applyBindings(new viewModel());

});