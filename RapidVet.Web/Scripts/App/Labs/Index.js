﻿var importXMLTesUploadtUrl = "";

$(function () {
       

    function TestType(data) {
        var self = this;
        self.enableEdit = ko.observable(false);
        self.name = ko.observable(data.Name);
        self.id = ko.observable(data.LabTestTypeId);
        self.labTestTemplates = ko.mapping.fromJS([]);
        ko.mapping.fromJS(data.LabTestTemplates, self.labTestTemplates);
       // self.dates = ko.observableArray([]);
       // for (var i = 0; i < self.labTestTemplates().length; i++)
       // {
       //     for (var j = 0; i < self.labTestTemplates()[i].Dates().length; i++)
       //     {
       //         self.dates.push(self.labTestTemplates()[i].Dates()[j]);
       //     }
       // }
       //// ko.mapping.fromJS(data.LabTestTemplates.Dates, self.dates);
    }

    function viewModel() {
        var self = this;
        self.animalKinds = ko.mapping.fromJS([]);        
        self.enableEdit = ko.observable(false);
        self.patientId = ko.observable($('#patientId').val());
        self.testTypes = ko.observableArray([]);
        self.selectedTypeName = ko.observable();
        self.selectedTypeId = ko.observable();
        self.selectedTestName = ko.observable();
        self.selectedTestDate = ko.observable();
        self.selectedTestId = ko.observable();
        self.selectedTemplateId = ko.observable();
        self.testResults = ko.mapping.fromJS([]);
        self.groupTestResults = ko.mapping.fromJS([]);
        self.alert = ko.observable(false);
        self.printAlert = ko.observable(false);
        self.lastLabTestTemplateId = ko.observable($('#labTestTemplateId').val());
        self.lastLabTestTypeId = ko.observable($('#lastLabTestTypeId').val());
        self.lastLabTestId = ko.observable($('#lastLabTestId').val());
        self.multipleLabsView = ko.observable(false);
        self.multipleLabsDates = ko.observableArray([]);

        //Graph
        self.display = function (labTestId) {
            $('#result').attr('src', '/Labs/TestResultGraph?patientLabTestId=' + labTestId + '&patientId=' + self.patientId());
            $('#graphModal').modal('show').css({
                'width': '75%',
                'margin-right': function () { return -($(this).width() / 2); },
                'height': '58%',
                'top': '30%',
                'min-height': '718px'
            });
        };

        self.newTestUrl = ko.computed(function () {
                return '/Labs/Create/' + self.patientId() 
        });
        self.importXMLTestUrl = ko.computed(function () {
            return '/Labs/ImportXML/' + self.patientId()
        });
        self.importXMLTesUploadtUrl = ko.computed(function () {
            importXMLTesUploadtUrl = '/Labs/UploadXML/' + self.patientId();
            return importXMLTesUploadtUrl;
        });
        self.printLabUrl = ko.computed(function () {
                return '/Labs/Print/' + self.patientId() + '?labTestTemplateId=' + self.selectedTemplateId() + '&labId=' + self.selectedTestId();
        });

        //self.addTestBtnText = ko.computed(function () {

        //    if (self.selectedTypeName()) {
        //        return 'הוסף בדיקה חדשה מסוג ' + self.selectedTypeName();
        //    }
        //    return 'יש לבחור סוג בדיקה';
        //});
        self.getAnimalKinds = function () {
            if ($('#ddlAnimalKinds') && $('#ddlAnimalKinds').html() == "") {
                $.getJSON('/MetaData/GetAnimalKinds', function (data) {
                    var htm = "";
                    htm += "<option value='0'>בחר..</option>";
                    for (var i = 0; i < data.length; i++) {
                        htm += "<option value='" + data[i].Value + "'>" + data[i].Text + "</option>";
                    }
                    $('#ddlAnimalKinds').html(htm);
                });
            }
        };

        self.getData = function () {
            $.getJSON('/Labs/GetPatientData/' + self.patientId(), function (data) {
                var mapped = $.map(data, function (item) {
                    return new TestType(item);
                });
                self.testTypes(mapped);
            });
        };

        self.testTypeSelected = function (element) {
            debugger;
            self.selectedTypeName(element.name());
            self.selectedTypeId(element.id());
            self.clearTestResults();
        };

        self.testTemplateSelected = function (element) {
            self.selectedTemplateId(element.Id());
            self.selectedTestDate('');
            self.selectedTestId('');
            self.clearTestResults();
            self.multipleLabsView(true);
            self.getLabGroupResults();

        };

        self.testDateSelected = function (element) {
            self.selectedTestDate(element.Date());
            self.selectedTestId(element.Id());
            self.multipleLabsView(false);
            self.getLabResults();
        };

        self.editTestDate = function () {
            window.location.href = '/Labs/EditTestDate/' + self.patientId() + '?labId=' + self.selectedTestId();
        }

        self.getLabResults = function () {
            self.clearTestResults();
            $.getJSON('/Labs/GetTestData/' + self.patientId() + '?patientLabTestId=' + self.selectedTestId(), function (data) {
                
                ko.mapping.fromJS(data.Results, self.testResults);
                self.selectedTestName(data.Name);
            });
        };

        self.getLabGroupResults = function () {
          
            $.getJSON('/Labs/GetLastGroupTestData/' + self.patientId() + '?labTestTemplateId=' + self.selectedTemplateId(), function (data) {

                    ko.mapping.fromJS(data, self.groupTestResults);
                    self.selectedTestName(data.LabTestName);

                    if (self.groupTestResults().length > 0) {
                        self.multipleLabsDates([]);
                        for (var i = 0; i < self.groupTestResults()[0].Results().length; i++) {
                            self.multipleLabsDates.push(self.groupTestResults()[0].Results()[i].Created());
                        }
                    }
                    else {
                        self.multipleLabsView(false);
                    }
                });

           
        };

        self.showLastLabTest = function () {
            if (typeof self.lastLabTestId() != "undefined" && self.lastLabTestId() > 0) {
                self.selectedTemplateId(self.lastLabTestTemplateId());
                self.selectedTypeId(self.lastLabTestTypeId());
                self.selectedTestId(self.lastLabTestId());
                self.selectedTypeName($('#lastLabTestTypeName').val());
                self.getLabResults();
            }
        };
            

        self.showLabTemplates = function (element) {
            self.multipleLabsView(false);
            self.clearTestResults();

            
            self.selectedTemplateId(element.Id());
            self.getLabResults();
        };



        self.showResults = function (element) {
            self.multipleLabsView(false);
            self.clearTestResults();

            self.selectedTestDate(element.Date());
            self.selectedTestId(element.Id());
            self.getLabResults();
        };



        self.clearTestResults = function () {
            self.testResults([]);
        };

        self.clearAll = function () {
            self.clearTestResults();
            self.selectedTypeName(null);
            self.selectedTypeId(null);
            self.selectedTestName(null);
            self.selectedTestDate(null);
            self.selectedTestId(null);
        };

        self.showDates = function (element) {          
            var result = false;
            if (element) {
                result = element.Id() == self.selectedTemplateId();
            }
            return result;
        };

        self.showTestTemplates = function (element) {
            var result = false;
            if (element) {
                result = element.id() == self.selectedTypeId();
            }
            return result;
        };

     
        self.newTest = function () {
            
                window.location.href = self.newTestUrl();
            
        };
        importXMLTest = function () {
            $('#formUpload').length > 0
            {
                $('#formUpload')[0].action = self.importXMLTestUrl();
                $('#formUpload')[0].submit();
            }
        };
        
        self.openImportXMLTest = function () {
            if ($('#fuMain').length > 0) {                
                $('#fuMain')[0].click();
            }
        };

        self.print = function () {
            if (typeof self.selectedTemplateId() == 'undefined' || (self.testResults().length == 0 && self.groupTestResults().length ==0)) {
                self.showPrintAlert();
            } else {
                window.open(self.printLabUrl(), "_blank");
            }
        };

        self.showAlert = function () {
            self.alert(true);
        };

        self.hideAlert = function () {
            self.alert(false);
        };

        self.hidePrintAlert = function () {
            self.printAlert(false);
        };
        self.showPrintAlert = function () {
            self.printAlert(true);
        };



        //execution
        self.getData();
        self.showLastLabTest();
        self.getAnimalKinds();
        //self.setLastTestsData();
        //self.getLabGroupResults();

        self.startEditing = function (element) {
            element.enableEdit(true);
        };

        self.finisheEditing = function (element) {
            element.enableEdit(false);
            ShowLoder();
            $.ajax({
                type: "POST",
                url: '/Labs/UpdateResult',
                data: {
                    'Id': element.Id(),
                    'Result': element.Result()
                },
                success: function (data) {
                    if (data == "True") {
                        $.getJSON('/Labs/GetIndicator/' + element.LabTestId() + '?min=' + element.Min() + '&max=' + element.Max() + '&result=' + element.Result(), function (i) {
                            element.Indicator(i);
                        });                        
                        hideLoder();
                    }
                },
                failure: function () {
                    hideLoder();
                }
            });           
        };
    }

    ko.applyBindings(new viewModel());

});
