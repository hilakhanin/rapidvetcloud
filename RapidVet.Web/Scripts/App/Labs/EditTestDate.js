﻿function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

$(function () {

   
   
    function viewModel() {

        var self = this;
        self.patientId = ko.observable($('#patientId').val());
        self.labId = ko.observable($('#labId').val());       
        self.testDate = ko.observable(moment().format('DD/MM/YYYY'));      

        self.getData = function () {
            $.getJSON('/Labs/GetTestData/' + self.patientId() + '?patientLabTestId=' + self.labId(), function (data) {
                self.testDate(data.Created);
            });
        };

        self.saveTest = function () {
                ShowLoder();
                             

                $.ajax({
                    type: "POST",
                    url: '/Labs/UpdatePatientLabTestDate',
                    data: {
                        'id': self.patientId(),
                        'labId': self.labId(),
                        'created': moment().format("DD/MM/YYYY") === self.testDate() ? self.testDate() : new Date(self.testDate()).toISOString()                        
                    },
                    success: function (data) {
                        if (data.Success) {
                            getCookieMessages();
                            setTimeout(function () { window.location.href = '/Labs/Index/' + self.patientId(); }, 1000);
                        } else {
                            getCookieMessages();
                            hideLoder();
                        }
                    },
                    failure: function () {
                        hideLoder();
                    }
                });
        };

        self.getData();
              
        ko.bindingHandlers.datepicker = {
            init: function (element, valueAccessor, allBindingsAccessor) {
                //initialize datepicker with some optional options
                //  var options = allBindingsAccessor().datepickerOptions || { dateFormat: 'dd/mm/yy' };
                //  $(element).datepicker(options);

                $(element).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    yearRange: "-100:+3"
                });

                //handle the field changing
                ko.utils.registerEventHandler(element, "change", function () {
                    var observable = valueAccessor();
                    observable($(element).datepicker("getDate"));
                    $(element).datepicker(valueAccessor());
                });

                //handle disposal (if KO removes by the template binding)
                ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                    $(element).datepicker("destroy");
                });

            },
            //update the control when the view model changes
            update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var value = ko.utils.unwrapObservable(valueAccessor()),
                    current = $(element).datepicker("getDate");

                if (value - current !== 0) {
                    $(element).datepicker("setDate", value);

                }
            }
        };
    }

    ko.applyBindings(new viewModel());
});