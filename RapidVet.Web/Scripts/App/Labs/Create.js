﻿function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

$(function () {

    ko.validation.rules.pattern.message = 'Invalid.';

    ko.validation.configure({
        registerExtenders: true,
        messagesOnModified: true,
        insertMessages: false,
        parseInputAttributes: true,
        messageTemplate: null
    });


    function TemplateItem(data) {
        var self = this;
        self.name = ko.observable(data.Name);
        self.min = ko.observable(data.Min);
        self.max = ko.observable(data.Max);
        self.result = ko.observable().extend({ maxLength: {params: 50, message:'ערך גדול מדי'} });
        self.labTestId = ko.observable(data.LabTestId);
        self.indicator = ko.observable();
        self.indicatorcomp = ko.computed(function () {
            if (!isEmptyOrWhiteSpaces(self.result())){//parseFloat(self.result()) || parseFloat(self.result()) == 0){// && self.max()) {//self.min()
                $.getJSON('/Labs/GetIndicator/' + self.labTestId() + '?min=' + self.min() + '&max=' + self.max() + '&result=' + self.result(), function (i) {
                    self.indicator(i);
                });
            }
        });

    }

    function viewModel() {

        var self = this;
        self.patientId = ko.observable($('#patientId').val());
        self.labTypeId = ko.observable(0);
        self.templates = ko.mapping.fromJS([]);
        self.selectedTemplateId = ko.observable();
        self.selectedTemplateTypeId = ko.observable();
        self.templateTypes = ko.mapping.fromJS([]);
        self.templateItems = ko.computed(function () {          
            return ko.utils.arrayFilter(self.templates(), function (type) {
                return type.LabTestTypeId() == self.selectedTemplateTypeId();
            });
        });
        self.patientLabTemplate = ko.mapping.fromJS([]);
        self.testDate = ko.observable(moment().format('DD/MM/YYYY'));

        self.checkTestsExist = ko.observable(false);
        self.selectedTemplateId.subscribe(function() {
            self.checkTestsExist(false);
        });
        //operations
        self.getData = function () {
            $.getJSON('/Labs/GetPatientTemplates/' + self.patientId(), function (data) {
                ko.mapping.fromJS(data.testTypes, self.templateTypes);
                ko.mapping.fromJS(data.testTemplates, self.templates);
            });
        };

        self.getSelectedTemplateItems = function () {
            if (self.selectedTemplateId() > 0) {
                $.getJSON('/Labs/GetTestTemplateItems/' + self.selectedTemplateId(), function (data) {
                    var mapped = $.map(data, function (item) {
                        return new TemplateItem(item);
                    });
                    self.patientLabTemplate(mapped);
                    self.checkTestsExist(true);
                });
            }
        };

        self.saveTest = function () {
            ko.validation.group(self, { deep: true });
            if (self.isValid()) {
                ShowLoder();


                ko.utils.arrayForEach(self.patientLabTemplate(), function (item) {
                    item.indicator(null);
                });

                $.ajax({
                    type: "POST",
                    url: '/Labs/CreatePatientLabTest',
                    data: {
                        'Id': self.patientId(),
                        'labTestTemplateId': self.selectedTemplateId(),
                        'created': moment().format("DD/MM/YYYY") === self.testDate() ? self.testDate() : new Date(self.testDate()).toISOString(),
                        'Results': ko.toJSON(self.patientLabTemplate())
                    },
                    success: function (data) {
                        if (data.Success) {
                            window.location.href = '/Labs/Index/' + self.patientId();
                        } else {
                            getCookieMessages();
                            hideLoder();
                        }
                    },
                    failure: function () {
                        hideLoder();
                    }
                });
            }
        };


        //execution
        self.getData();

        ko.bindingHandlers.datepicker = {
            init: function (element, valueAccessor, allBindingsAccessor) {
                //initialize datepicker with some optional options
                //  var options = allBindingsAccessor().datepickerOptions || { dateFormat: 'dd/mm/yy' };
                //  $(element).datepicker(options);

                $(element).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    yearRange: "-100:+3"
                });

                //handle the field changing
                ko.utils.registerEventHandler(element, "change", function () {
                    var observable = valueAccessor();
                    observable($(element).datepicker("getDate"));
                    $(element).datepicker(valueAccessor());
                });

                //handle disposal (if KO removes by the template binding)
                ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                    $(element).datepicker("destroy");
                });

            },
            //update the control when the view model changes
            update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var value = ko.utils.unwrapObservable(valueAccessor()),
                    current = $(element).datepicker("getDate");

                if (value - current !== 0) {
                    $(element).datepicker("setDate", value);

                }
            }
        };
    }

    ko.applyBindings(new viewModel());
});