﻿$(function () {

    function viewModel() {

        var self = this;
        self.CurrentExecutionUrl = ko.observable();
        self.clientId = ko.observable($('#clientId').val());
        self.quotations = ko.mapping.fromJS([]);
        self.quotationToDeleteId = ko.observable();
        self.saveAsName = ko.observable();
        self.selectedQuotationId = ko.observable();


        //functions
        self.preExecute = function (executionUrl, treatmentsDescription) {
            self.CurrentExecutionUrl(executionUrl);
            if (treatmentsDescription == "")
                treatmentsDescription = 'No items in the  list';

            var splitted = treatmentsDescription.split("\r\n");
            var htmlContent = "";
            for (var i = 0; i < splitted.length; i++) {
                htmlContent += "<div>" + splitted[i] + "</div>";
            }
            $('#indexMoveQuotationToExecutionItemsList').html(htmlContent);
            $('#indexMoveQuotationToExecutionModel').modal('show');
        };

        self.getData = function () {
            $.getJSON('/Quotations/GetClientQuotations/' + self.clientId(), function (data) {
                ko.mapping.fromJS(data, self.quotations);
            });
        };

        self.showDeleteModal = function (element) {
            self.quotationToDeleteId(element.Id());
            $('#deleteModal').modal('show');
        };
        
        self.sendEmail = function (element) {
            ShowLoder();
            $.ajax({
                type: "POST",
                url: '/Quotations/SendEmail',
                data: {
                    'quotationName': element.Name(),
                    'patientName': element.PatientName(),
                    'treatments': element.TreatmentsDescription(),
                    'email': 'adnan@e-c.co.il'
                },
                success: function (data) {
                    alert(data);
                },
                failure: function () {
                    alert('שגיאה בשליחת דוא"ל');
                }
            }).done(function () {
                getCookieMessages();
                hideLoder();
            });           
        };

        self.hideDeleteModal = function () {
            $('#deleteModal').modal('hide');
            self.quotationToDeleteId(null);
        };

        self.showSaveAsModal = function (element) {
            self.selectedQuotationId(element.Id());
            $('#savePackageAs').modal('show');
        };
        self.hideSaveAsModal = function () {
            $('#savePackageAs').modal('hide');
            self.saveAsName(null);
            self.selectedQuotationId(null);
        };

        self.saveAsNewPackage = function () {

            $('#savePackageAs').modal('hide');
            ShowLoder();

            $.ajax({
                type: "POST",
                url: '/Quotations/DuplicateQuotation',
                data: {
                    'Id': self.selectedQuotationId(),
                    'Name': self.saveAsName(),
                },
                success: function (data) {
                    window.location.reload();
                },
                failure: function () {
                }
            }).done(function () {
                getCookieMessages();
                hideLoder();
            });
        };

        //execution
        self.getData();

    }

    ko.applyBindings(new viewModel());
});

function submitDeleteForm() {
    $('#deleteModal').modal('hide');
    $('#deleteForm').submit();
}