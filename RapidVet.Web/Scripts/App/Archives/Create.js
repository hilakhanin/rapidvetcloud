﻿var vm;
$(function () {
     
    function fileInput(data) {
        var self = this;
        self.name = ko.observable('file_' + data);
    }

    function viewModel() {

        //members
        var self = this;

        self.fileTypes = ko.mapping.fromJS([]);

        self.selectedFileTypeId = ko.observable();

        self.isImage = ko.computed(function () {
            return self.selectedFileTypeId() == 1;
        });

        self.files = ko.observableArray([]);

        self.removeButtonVisible = ko.computed(function () {
            return self.files().length > 1;
        });

        //operations
        self.getData = function () {

            $.getJSON('/Archives/GetFileTypesData', function (data) {
                ko.mapping.fromJS(data, self.fileTypes);
            });

            $.getJSON('/Archives/GetMaxFileSize', function (data) {
                maximumFileUploadSize = data;
            });
        };

        self.addFileInput = function () {
            self.files.push(new fileInput(self.files().length));
        };

        self.removeFileInput = function (element) {
            self.files.remove(element);
        };

        //execution
        self.addFileInput();
        self.getData();
        hideLoder();
    }

    vm = new viewModel();
    ko.applyBindings(vm);   
});

function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null; //str.match(/^$|\s+/) !== null;
}

function createArchiveFile() {
    var valid = true;

    if (isEmptyOrWhiteSpaces($('#drpArchive').val())) {
        $('#fileTypeError').show();
        valid = false;
    }
    else
    {
        $('#fileTypeError').hide();
    }
    if (isEmptyOrWhiteSpaces($('#ArchiveDoc').val()) &&
            isEmptyOrWhiteSpaces($('#ArchiveImg').val()) &&
        isEmptyOrWhiteSpaces($('#ArchiveVed').val())) {
        $('#fileError').text("אנא בחר קובץ");
        $('#fileError').show();
        valid = false;
    }
    else
    {
        $('#fileError').hide();
    }
    if (isEmptyOrWhiteSpaces($('#Title').val())) {
        $('#nameError').show();
        valid = false;
    }
    else
    {
        $('#nameError').hide();
    }
    if(valid)
    {
        //ShowLoder();        
    }

    return valid;
}

function editArchiveFile() {
    var valid = true;

    if (isEmptyOrWhiteSpaces($('#Title').val())) {
        $('#nameError').show();
        valid = false;
    }
    else {
        $('#nameError').hide();
    }
    if (valid) {
        //ShowLoder();        
    }

    return valid;
}

$("#drpArchive").change(function () {
    var caseSwitch = $(this).val();
    switch (caseSwitch) {
        case "1":
            $("#ArchiveImg").show();
            $("#ArchiveVed").hide();
            $("#ArchiveDoc").hide();
            
            break;
        case "2":
            $("#ArchiveImg").hide();
            $("#ArchiveDoc").hide();
            $("#ArchiveVed").show();
            break;
        case "3":
            $("#ArchiveImg").hide();
            $("#ArchiveVed").hide();
            $("#ArchiveDoc").show();
            break;
        default:
            $("#ArchiveImg").show();
            $("#ArchiveVed").hide();
            $("#ArchiveDoc").hide();
            break;
    }
});


$('#image-file').bind('change', function () {
    alert('This file size is: ' + this.files[0].size / 1024 / 1024 + "MB");
});