﻿$(function () {


    function viewModel() {

        function Image(data) {
            var self = this;
            self.Id = ko.observable(data.Id);
            self.Title = ko.observable(data.Title);
            self.TypeName = ko.observable(data.TypeName);
            self.Time = ko.observable(data.Time);
            self.Date = ko.observable(data.Date);
            self.Comments = ko.observable(data.Comments);
            self.EditUrl = ko.observable(data.EditUrl);
            self.ViewUrl = ko.observable(data.ViewUrl);
            self.Selected = ko.observable(false);
        }

        var self = this;

        self.patientId = ko.observable($('#patientId').val());
        self.images = ko.observableArray([]);
        self.videos = ko.mapping.fromJS([]);
        self.other = ko.mapping.fromJS([]);
        self.elementToDelete = ko.observable();
        self.elementToDeleteTypeName = ko.observable();
        self.checkedImageIds = ko.observableArray([]);

        self.searchQuery = ko.observable();
        self.queryResults = ko.mapping.fromJS([]);

        self.sideBySideImagesUrl = ko.computed(function () {
            var url = '/Archives/SideBySideImages/' + self.patientId() + '?imageIds=';
            var ids = '';
            ko.utils.arrayForEach(self.images(), function (image) {
                if (image.Selected()) {
                    ids += image.Id() + ',';
                }
            });
            return url + ids;
        });

        self.getData = function () {
            ShowLoder();

            $.getJSON('/Archives/GetIndexData/' + self.patientId(), function (data) {

                ko.mapping.fromJS(data.Videos, self.videos);
                ko.mapping.fromJS(data.Other, self.other);
                var mappedImages = $.map(data.Images, function (item) {
                    return new Image(item);
                });
                self.images(mappedImages);
            }).done(function () {
                hideLoder();
            });
        };

        self.showDeleteModal = function (element) {
            self.elementToDelete(element);
            self.elementToDeleteTypeName(element.TypeName());
            $('#deleteModal').modal('show');
        };

        self.deleteDocument = function () {
            self.hideDeleteModal();
            ShowLoder();

            $.ajax({
                type: "POST",
                url: '/Archives/Delete/' + self.elementToDelete().Id(),
                success: function (data) {
                    if (data) {
                        window.location.reload();
                    } else {
                        getCookieMessages();

                    }
                }
            }).done(function () {
                hideLoder();
            });
        };

        self.clearDeleteModal = function () {
            self.elementToDelete();
            self.elementToDeleteTypeName();
            self.hideDeleteModal();
        };

        self.hideDeleteModal = function () {
            $('#deleteModal').modal('hide');
        };

        self.clearSelectedImages = function () {

            ko.utils.arrayForEach(self.images(), function (image) {
                image.Selected(false);
            });
        };


        //search
        self.search = function () {
            self.queryResults([]);
            ShowLoder();
            $.getJSON('/Archives/Serach?query=' + self.searchQuery(), function (data) {
                ko.mapping.fromJS(data, self.queryResults);
                if (self.queryResults().length == 0) {
                    getCookieMessages();
                    hideLoder();
                } else {
                    hideLoder();
                }
            });
        };

        self.clearSearch = function () {
            self.queryResults([]);
            self.searchQuery(null);
        };

        self.getData();

    }

    ko.applyBindings(new viewModel());
})