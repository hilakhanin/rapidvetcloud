﻿function viewModel(defaultFrom,defaultTo) {
    var self = this;

    self.from = ko.observable(defaultFrom);
    self.to = ko.observable(defaultTo);

    self.drugOptions = ko.mapping.fromJS([]);
    self.drugFilter = ko.observable();

    self.items = ko.mapping.fromJS([]);

    self.filtersChanged = ko.observable(true);

    self.from.subscribe(function() {
        self.filtersChanged(true);
    });
    self.to.subscribe(function() {
        self.filtersChanged(true);
    });
    self.drugFilter.subscribe(function() {
        self.filtersChanged(true);
    });
    
    self.printUrl = ko.computed(function() {
        return '/Statistics/PrintDrugReport?from=' + self.from() + '&to=' + self.to() + '&medicationId=' + self.drugFilter();
    });

    self.search = function () {
        var qs = {
            from: self.from(),
            to: self.to(),
            medicationId: self.drugFilter()
        };
        $.get('/Statistics/GetDrugReport', $.param(qs), function(data) {
            ko.mapping.fromJS(data, self.items);
            self.filtersChanged(false);
        });
    };

    self.print = function() {
        window.location = self.printUrl();
    };

    self.init = function () {
        ShowLoder();

        $.get('/MetaData/GetClinicGroupDangerousDrugs', function (data) {
            ko.mapping.fromJS(data, self.drugOptions);
            hideLoder();
        });
    };

    self.init();
}