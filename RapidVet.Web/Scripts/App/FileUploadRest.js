﻿var MB = 1000000;
var maximumUploadSize = 50;
var maximumFileUploadSize;
var inputFileControlsCount;
$(function () {

    $("button[type='submit']").click(function () {        
        var $fileUpload = $("input[type='file']");
        var allFiles;
        if (inputFileControlsCount == null)
            inputFileControlsCount = 1;

        for (var i = 0; i < inputFileControlsCount; i++) {
            allFiles = $fileUpload.get(i).files;
        }

        if (!checkFilesSize(allFiles)) {
            $fileUpload.val('');
            return false;
        }
    });
});

//returns false in case of illegal files size
function checkFilesSize(allFiles) {
    var totalSize = 0;
    if (maximumFileUploadSize == null)
        maximumFileUploadSize = 10;

    for (var i = 0; i < allFiles.length; i++) {
        var fs = allFiles[i].size;
        totalSize += fs;
        if (fs > (maximumFileUploadSize * MB)) {
            $('#fileError').text("Each file's size must be up to " + maximumFileUploadSize + "MB");
            $('#fileError').show();
            $('html, body').animate({ scrollTop: $("input[type='file']").offset().top - 200 }, 'slow');
            return false;
        }
        if (totalSize > (maximumUploadSize * MB)) {
            $('#fileError').text("All files size cannot be larger than " + maximumUploadSize + "MB");
            $('#fileError').show();
            $('html, body').animate({ scrollTop: $("input[tye='file']").offset().top - 200 }, 'slow');
            return false;
        }
    }
    return true;
};