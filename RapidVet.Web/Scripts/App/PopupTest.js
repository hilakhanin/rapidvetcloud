﻿function testPopup() {
    
    var popup = window.open("/Home/PopupTest", "rapidPopupTest","width=200,height=230");

    // Detect popup blocker
    setTimeout(function () {
        if (!popup || popup.closed || parseInt(popup.innerWidth) == 0) {
            //POPUP BLOCKED
            if (popup) {
                popup.close();
            }
            alert("על מנת שהמערכת תתפקד במיטבה, יש להסיר חסימת חלונות פופ-אפ");
        }
        popup.close();
    }, 500);
}