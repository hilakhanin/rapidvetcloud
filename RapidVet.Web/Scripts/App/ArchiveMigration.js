﻿$(function () {

    function Origin(data) {
        var self = this;
        self.name = ko.observable(data);
        self.destination = ko.observable();
    }


    function viewModel() {

        var self = this;
        self.destinations = ko.mapping.fromJS([]);
        self.origins = ko.observableArray([]);

        self.json = ko.computed(function () {
            return ko.toJSON(self.origins());
        });


        //functions
        self.getData = function () {

            ShowLoder();

            $.getJSON('/ArchiveMigration/GetFilePaths', function (data) {
                ko.mapping.fromJS(data.destinations, self.destinations);
                var mapped = $.map(data.origins, function (item) {
                    return new Origin(item);
                });
                self.origins(mapped);
            }).done(function () { hideLoder() });
        };


        //executions
        self.getData();

    }

    ko.applyBindings(new viewModel());
});