﻿//deals with client views

//phone number validation
function limitPhoneCharacters() {
    $('.phone-number').keypress(function (event) {
        var code = (event.keyCode ? event.keyCode : event.which);
        if (!(code >= 48 && code <= 57)) //numbers 
        {
            event.preventDefault();
        }
    });
}

function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null; //str.match(/^$|\s+/) !== null;
}

$(function () {
    limitPhoneCharacters();
});

//inits the client create form
function InitCreateForm() {
    $('.createdDate').hide();
    InitEditForm();
}

function InitEditForm() {
    $('#BirthDate').blur(DateToAge);
    InitPhoneBar();
    InitAddressBar();
    InitEmailBar();
    InitIdField();
}

function InitIdField() {    
    $('#IdCard').blur(function () {
        if (!isEmptyOrWhiteSpaces($('#IdCard').val())) {
            $.get('/Validation/IsIdCardExist?idcard=' + $.trim($('#IdCard').val()) + '&clientId=' +
                (!isEmptyOrWhiteSpaces($('#Id').val()) ? $('#Id').val() : 0),
               function (data) {
                   if (data == true) {
                       $('#errorMsg').text("");
                       $('button[type="submit"]').removeAttr('disabled');
                       $('#IdCard').css('background-color', '#e6e6e6');
                   }
                   else {
                       $('#errorMsg').text(data);
                       $('button[type="submit"]').attr('disabled', 'disabled');
                       $('#IdCard').css('background-color', '#f87c90');//#
                   }
               }, 'json');
        }
        else {
            $('#errorMsg').text("");
            $('button[type="submit"]').removeAttr('disabled');
            $('#IdCard').css('background-color', '#e6e6e6');
        }
    });
}

//calculates date from age in the client create / edit form
function DateToAge() {

    var today = new Date();
    var curr_date = today.getDate();
    var curr_month = today.getMonth() + 1;
    var curr_year = today.getFullYear();
    var birth = $('#BirthDate').val();

    var piecesArr = birth.split('/');
    var pieces = piecesArr;
    var success = false;

    if (piecesArr.length > 1) {
        pieces = piecesArr;
        success = true;
    } else {
        piecesArr = birth.split('.');
        success = false;
    }
    if (piecesArr.length > 1 && !success) {
        pieces = piecesArr;
        success = true;
    } else {
        piecesArr = birth.split('-');
        success = false;
    }
    if (piecesArr.length > 1 && !success) {
        pieces = piecesArr;
        success = true;
    }
    var birth_date = pieces[0];
    var birth_month = pieces[1];
    var birth_year = pieces[2];


    var age = 0;
    if (curr_month == birth_month && curr_date >= birth_date) age = parseInt(curr_year - birth_year);
    if (curr_month == birth_month && curr_date < birth_date) age = parseInt(curr_year - birth_year - 1);
    if (curr_month > birth_month) age = parseInt(curr_year - birth_year);
    if (curr_month < birth_month) age = parseInt(curr_year - birth_year - 1);

    $('#Age').val(age);

}

//adds email to client in create / edit form
function AddEmail() {
    var c = AppendEmail();
    InitEmailBar(c);
}

//append an email alament to the appropriate section of the client create / edit form
function AppendEmail() {
    var source = $('#emailTemplate').children('.email').clone();
    var container = $('#emails');
    var count = container.children('.email').size() + 1;
    container.append(source);
    InitEmailBar(count);
    RapidVet.FixInputWidth.init();
}

//initiates the appended html with proper fiels names
function InitEmailBar() {
    var count = 0;
    $('#emails').children('.email').each(function () {
        count++;
        var emailId = 'email_' + count;
        var dbId = "idEmail_" + count;

        $(this).find('#Name').attr('name', emailId).attr('id', emailId);
        $(this).find("input[type='hidden'][name='Id']").attr('name', dbId);
    });
}

//addes address html section to the appropriate location of the client create/edit form
function AppendAddress() {
    var source = $('#addresTemplate').children('.address').clone();
    var container = $('#addresses');
    var count = container.children('.address').size() + 1;
    container.append(source);
    InitAddressBar(count);
    RapidVet.FixInputWidth.init();
}

//initiates the appended html with proper fiels names
function InitAddressBar() {
    var count = 0;
    $('#addresses').children('.address').each(function () {
        count++;
        var city = 'city_' + count;
        var street = 'street_' + count;
        var zip = 'zip_' + count;
        var dbId = "idCity_" + count;
        var cityName = "cityname_" + count;
        $(this).find('#CityId').attr('name', city).attr('id', city);
        $(this).find('#CityName').attr('name', cityName).attr('id', cityName);
        $(this).find('#Street').attr('name', street).attr('id', street);
        $(this).find('#ZipCode').attr('name', zip).attr('id', zip);
        $(this).find("input[type='hidden'][name='Id']").attr('name', dbId);

    });
}

//addes address html section to the appropriate location of the client create/edit form
function AppendPhone() {
    var source = $('#phoneTemplate').children('.phone').clone();
    var container = $('#phones');
    var count = container.children('.phone').size() + 1;
    container.append(source);
    InitPhoneBar(count);
    RapidVet.FixInputWidth.init();
    limitPhoneCharacters();
}


//initiates the appended html with proper fiels names
function InitPhoneBar() {
    var count = 0;
    $('#phones').children('.phone').each(function () {
        count++;
        var phone = 'phone_' + count;
        var phoneType = 'phoneType_' + count;
        var dbId = "idPhone_" + count;
        var phoneComment = 'comment_' + count;
        $(this).find('#PhoneNumber').attr('name', phone).attr('id', phone);
        $(this).find('#PhoneTypeId').attr('name', phoneType).attr('id', phoneType);
        $(this).find("input[type='hidden'][name='Id']").attr('name', dbId);
        $(this).find('#PhoneComment').attr('name', phoneComment).attr('id', phone);
    });
}


function FindZipCode(element) {
    //autocomplete zipcode by city and street

    var count = $(element).parent().find('.zip').attr("id").split('_')[1];

    if (!isEmptyOrWhiteSpaces($("#cityname_" + count).val()) && !isEmptyOrWhiteSpaces($("#street_" + count).val())) {
        $.get('/Clients/GetZipCodeByCityAndStreet?city=' + $("#cityname_" + count).val() + "&street=" + $("#street_" + count).val(), function (data) {
            if (data.Status) {
                $("#zip_" + count).val(data.Value);
            }
            else {
                alert("לא אותר מיקוד עבור הכתובת");
            }
        });
    }
    else {
        alert("יש להזין עיר ורחוב");
    }

    return false;
}

