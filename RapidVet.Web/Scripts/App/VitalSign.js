﻿var isOnAdding = false;

function addManualVitalSign(vitalSignId, patId, tableName) {
    if (isOnAdding === true) {
        return;
    }

    isOnAdding = true;
    var table = document.getElementById(tableName);
    var row = table.insertRow(table.rows.length - 1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    cell1.innerHTML = '<input id="newCreateDP" type="text" class="datepicker_recurring_start" style="width: auto;" autocomplete="off" value="' + getTodayFormat() + '" readonly/>';
    cell2.innerHTML = '<input type="text" id="newVal" style="width: auto;"/>';
    tableName = "'" + tableName + "'";
    var doneAddingData = vitalSignId + "," + patId;
    cell3.innerHTML = '<label><a onclick="saveManualVitalSign(' + doneAddingData + ');" data-toggle="tooltip" data-placement="top" title="אישור הוספה" data-original-title="רישור הוספה"><i class="icon-ok"></i></a><a onclick="cancelAdding(' + tableName + ');" data-toggle="tooltip" data-placement="top" title="ביטול הוספה" data-original-title="ביטול הוספה"><i class="icon-remove"></i></a></label>';
}

function cancelAdding(tableName) {
    tableName = tableName.trim();
    var table = document.getElementById(tableName);
    table.deleteRow(table.rows.length - 2);
    isOnAdding = false;
}

function saveManualVitalSign(vitalSignId, patId) {
    var newAdddate = document.getElementById("newCreateDP");
    var newVal = document.getElementById("newVal");
    ShowLoder();
    $.ajax({
        type: "POST",
        url: '/VitalSigns/SaveManualVitalSign',
        data: {
            'id': patId,
            'vitalSignID': vitalSignId,
            'dateTime': newAdddate.value,
            'val': newVal.value
        },
        success: function (data) {
            if (data === "1") {
                location.reload();
            }
        }
    }).done(function () {
        hideLoder();
        getCookieMessages();
        isOnAdding = false;
    });
}

function editVitalRow(id) {
    startUIEdit([{ id: "lbl_val_" + id, hide: true }, { id: "txt_val_" + id, hide: false }, { id: "startEditing_icons_" + id, hide: true }, { id: "confirm_icons_" + id, hide: false }]);
    if(id.indexOf('_0_') == -1)//MANUAL VITAL DAYA
    {
        startUIEdit([{ id: "lbl_date_" + id, hide: true }, { id: "txt_date_" + id, hide: false }]);
    }
}

function cancelEditVitalRow(id) {
    startUIEdit([{ id: "lbl_val_" + id, hide: false }, { id: "txt_val_" + id, hide: true }, { id: "startEditing_icons_" + id, hide: false }, { id: "confirm_icons_" + id, hide: true }]);
    if (id.indexOf('_0_') == -1)//MANUAL VITAL DAYA
    {
        startUIEdit([{ id: "lbl_date_" + id, hide: false }, { id: "txt_date_" + id, hide: true }]);
    }
}

function saveVitalRow(id, manualId, vitalSignID) {
    var i = id + '_' + manualId + '_' + vitalSignID;
    cancelEditVitalRow(i);
    var elemVal = document.getElementById('val_' + i);
    ShowLoder();
    var userid = document.getElementById("rowUserID");
    var vitalDate = id.indexOf('_0_') == -1 ? document.getElementById("editDP_" + i).value : null;
    $.ajax({
        type: "POST",
        url: '/VitalSigns/SaveVitalSign',
        data: {
            'id': id,
            'manualId': manualId,
            'vitalSignID': vitalSignID,
            'val': elemVal.value,
            'vitalDate': vitalDate
        },
        success: function (data) {
           location.reload();
        }
    }).done(function () {
        hideLoder();
        getCookieMessages();
    });
}