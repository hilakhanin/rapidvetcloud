﻿function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

$(function () {

    function Item(data) {

        var self = this;
        self.id = ko.observable(data.Id);
        self.name = ko.observable(data.Name);
        self.priceListItemId = ko.observable(data.PriceListItemId);
        self.priceListItemTypeId = ko.observable(data.PriceListItemTypeId);
        self.last = ko.observable(data.PreformedStr);
        self.deleteVisible = ko.computed(function () {

            var result = false;

            if (self.last() != null) {
                if (self.last().length > 0) {
                    result = true;
                }
            }

            return result;
        });
        self.next = ko.observable(data.NextStr);
        self.selected = ko.observable(false);
        self.cssClass = ko.computed(function () {
            if (data.IsNextFuture == true) {
                return 'future';
            } else if (data.IsNextFuture == false) {
                return 'past';
            }
            return ' ';
        });
        self.price = ko.observable(data.Price);
        self.isToday = ko.observable(data.IsToday);
        self.isRabiesVaccine = ko.observable(data.IsRabiesVaccine);
    }


    function viewModel() {
        var self = this;
        self.patientId = ko.observable($('#patientId').val());
        self.isDog = ko.observable($('#isDog').val());
        self.vaccines = ko.observableArray([]);
        self.treatments = ko.observableArray([]);


        self.getSelectedIds = function () {
            var ids = '';
            ko.utils.arrayForEach(self.vaccines(), function (vaccine) {
                if (vaccine.selected()) {
                    ids += vaccine.priceListItemId() + ',';
                }
            });

            ko.utils.arrayForEach(self.treatments(), function (treatment) {
                if (treatment.selected()) {
                    ids += treatment.priceListItemId() + ',';
                }
            });

            return ids;
        }

        self.printUrl = ko.computed(function () {

            var url = '/Preventive/Print/' + self.patientId() + '?selected=';
            return url + self.getSelectedIds();
        });


        //modal
        self.regularDr = ko.observable("true");
        this.regularDr.subscribe(function (newValue) {
            self.regularDr(newValue);
        }, this);
        self.selectedItem = ko.observable();
        self.externallyPreformed = ko.observable(true);
        self.comment = ko.observable();
        self.rabiesPrint = ko.observable("dontPrint");
        self.rabiesSectionVisible = ko.observable(false);
        self.selectedItemName = ko.observable();
        self.doctors = ko.mapping.fromJS([]);

        self.selectedDoctorId = ko.observable().extend({
            required: {
                message: "אנא בחר רופא",
                onlyIf: function () {
                    return self.regularDr() === "true";
                }
            }
        });
        self.date = ko.observable(moment().format('DD/MM/YYYY'));
        self.modalCssClass = ko.observable();
        self.modalBodyCssClass = ko.observable();
        self.externalDrName = ko.observable();
        //.extend({
        //    required: {
        //        message: "אנא הזן שם לרופא החיצוני",
        //        onlyIf: function () {
        //            return self.regularDr() === "false";
        //        }
        //    }
        //});
        self.externalDrLicenseNumber = ko.observable();
        self.batchNumber = ko.observable();
        self.electronicNumber = ko.observable();
        self.licenseNumber = ko.observable();
        self.clientPaidOrReturnedTollVaucher = ko.observable();

        //delete modal
        self.elementToDeleteName = ko.observable();
        self.elementToDeleteId = ko.observable();

        //update modal
        self.elementToUpdateName = ko.observable();
        self.elementToUpdateId = ko.observable();
        self.elementToUpdatePriceListItemId = ko.observable();
        self.elementToUpdateDate = ko.observable();
        self.elementToUpdatePrice = ko.observable();
        self.elementToUpdateNextDate = ko.observable();
        self.updateDate = ko.observable();
        self.updateNextDate = ko.observable();
        self.updateNextDateString = ko.observable();
        self.updateScheduledPreventiveItems = ko.observable();
        self.billAccordingToTariff = ko.observable();
        var dateError = "אנא הזן תאריך לביצוע";
        var nextDateError = "אנא הזן תאריך הבא לביצוע";
        var doctorError = "אנא בחר רופא";
        self.updateDateError = ko.observable("");
        self.updateNextDateError = ko.observable("");
        self.updateDoctorId = ko.observable();
        self.updateSelectDoctorError = ko.observable("");

        //load data 
        self.getData = function () {

            $.getJSON('/Preventive/GetPatientData/' + self.patientId(), function (data) {
                var vaccines = $.map(data.Vaccines, function (vac) {
                    return new Item(vac);
                });
                self.vaccines(vaccines);

                var treatments = $.map(data.Treatments, function (trt) {
                    return new Item(trt);
                });
                self.treatments(treatments);
                self.electronicNumber(data.ElectronicNumber);
                self.licenseNumber(data.LicenseNumber);
            });
        };

        self.getDoctors = function () {
            $.getJSON('/Clinics/GetClinicDoctors', function (data) {
                ko.mapping.fromJS(data, self.doctors);
            });
        };

        self.init = function () {

            ShowLoder();

            self.getData();
            self.getDoctors();

            hideLoder();
        };


        //select & unselect operations

        self.selectAll = function () {

            for (var i = 0; i < self.vaccines().length; i++) {
                if (!isEmptyOrWhiteSpaces(self.vaccines()[i].last()) || !isEmptyOrWhiteSpaces(self.vaccines()[i].next())) {
                    self.vaccines()[i].selected(true);
                }
            }

            for (var i = 0; i < self.treatments().length; i++) {
                if (!isEmptyOrWhiteSpaces(self.treatments()[i].last()) || !isEmptyOrWhiteSpaces(self.treatments()[i].next())) {
                    self.treatments()[i].selected(true);
                }
            }
        };

        self.selectToday = function () {

            self.clearAllSelected();

            for (var i = 0; i < self.vaccines().length; i++) {
                if (self.vaccines()[i].isToday()) {
                    self.vaccines()[i].selected(true);
                }
            }

            for (var i = 0; i < self.treatments().length; i++) {
                if (self.treatments()[i].isToday()) {
                    self.treatments()[i].selected(true);
                }
            }
        };

        self.clearAllSelected = function () {

            for (var i = 0; i < self.vaccines().length; i++) {
                self.vaccines()[i].selected(false);
            }

            for (var i = 0; i < self.treatments().length; i++) {
                self.treatments()[i].selected(false);
            }
        };


        //delete 

        self.showDeleteModal = function (element) {
            self.elementToDeleteName(element.name);
            self.elementToDeleteId(element.id);
            $('#deleteModal').modal('show');
        };

        self.deletePreformance = function () {
            $('#deleteModal').modal('hide');
            ShowLoder();
            $.ajax({
                type: "POST",
                url: '/Preventive/Delete',
                data: {
                    'Id': self.elementToDeleteId(),
                    'Name': self.elementToDeleteName()
                },
                success: function (data) {
                    getCookieMessages();
                    if (data.Success) {
                        window.location.reload();
                    }
                    hideLoder();
                }
            });
        };

        self.clearDeleteModal = function () {
            self.elementToDeleteName();
            self.elementToDeleteId();
            self.hideDeleteModal();
        };

        self.hideDeleteModal = function () {
            $('#deleteModal').modal('hide');
        };

        self.showUpdateModal = function (element) {
            self.elementToUpdateName(element.name());
            self.elementToUpdateId(element.id());
            self.elementToUpdatePriceListItemId(element.priceListItemId());
            self.elementToUpdateDate(element.last());
            self.elementToUpdatePrice(element.price());
            self.elementToUpdateNextDate(element.next());
            self.updateDate(false);
            self.updateNextDate(false);
            self.updateNextDateString("עדכן תאריך " + element.name() + " הבא");
            self.updateScheduledPreventiveItems(false);
            self.billAccordingToTariff(false);
            self.updateDateError("");
            self.updateNextDateError("");
            self.updateSelectDoctorError("");
            self.updateDoctorId($('#defaultDr').val());
            self.regularDr("true");
            self.externalDrName("");
            self.externalDrLicenseNumber("");
            $('#updateModal').modal('show');
        };


        self.updateTreatment = function () {
            var valid = true;
            var date = "";
            var nextDate = "";

            if (!self.updateDate() && !self.updateNextDate()) {
                $('#updateModal').modal('hide');
                return;
            }

            if (self.updateDate() && (self.elementToUpdateDate() == "" || self.elementToUpdateDate() == null)) {
                valid = false;
                self.updateDateError(dateError);
            }
            else {
                self.updateDateError("");
            }

            if (self.updateDate() && self.regularDr() == "true" && (self.updateDoctorId() == "" || self.updateDoctorId() == null)) {
                valid = false;
                self.updateSelectDoctorError(doctorError);
            }
            else {
                self.updateSelectDoctorError("");
            }

            if (self.updateNextDate() && (self.elementToUpdateNextDate() == "" || self.elementToUpdateNextDate() == null)) {
                valid = false;
                self.updateNextDateError(nextDateError);
            }
            else {
                self.updateNextDateError("");
            }

            if (valid) {
                if (self.updateDate()) {
                    if (self.elementToUpdateDate() instanceof Date == false) {
                        var dateString = self.elementToUpdateDate().split("/");
                        self.elementToUpdateDate(new Date(dateString[2], dateString[1] - 1, dateString[0]));
                    }
                    date = self.elementToUpdateDate().toISOString();
                }

                if (self.updateNextDate()) {
                    if (self.elementToUpdateNextDate() instanceof Date == false) {
                        var nextDateString = self.elementToUpdateNextDate().split("/");
                        self.elementToUpdateNextDate(new Date(nextDateString[2], nextDateString[1] - 1, nextDateString[0]));
                    }
                    nextDate = self.elementToUpdateNextDate().toISOString();
                }

                $('#updateModal').modal('hide');
                ShowLoder();
                $.ajax({
                    type: "POST",
                    url: '/Preventive/UpdatePreventiveTreatment',
                    data: {
                        'Id': self.elementToUpdateId(),
                        'PriceListItemId': self.elementToUpdatePriceListItemId(),
                        'PatientId': self.patientId(),
                        'UpdateDate': self.updateDate(),
                        'Date': date,
                        'BatchNumber': self.batchNumber(),
                        'UpdateScheduledPreventiveItems': self.updateScheduledPreventiveItems(),
                        'BillAccordingToTariff': self.billAccordingToTariff(),
                        'Price': self.billAccordingToTariff() ? self.elementToUpdatePrice() : 0,
                        'UpdateNextDate': self.updateNextDate(),
                        'NextDate': nextDate,
                        'DoctorId': self.updateDoctorId(),
                        'ExternallyPerformed': self.regularDr().toLowerCase() == "true" ? false : true,
                        'ExternalDoctorName': self.externalDrName(),
                        'ExternalDoctorLicense': self.externalDrLicenseNumber(),
                    },
                    success: function (data) {
                        getCookieMessages();
                        if (data.Success) {
                            window.location.reload();
                        }
                        hideLoder();
                    }
                });
            }
        };

        self.getDate = function (data) {
            var date = new Date(data);
            return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
        };

        function initExecutionModal() {
            self.rabiesPrint("dontPrint");
            self.clientPaidOrReturnedTollVaucher(false);
            self.regularDr("true");
            $("#regularDrDiv").show();
            $("#externalDrDiv").hide();
            $("#cb_chargeForTreatment").attr('checked', true);
            self.externalDrName("");
            self.externalDrLicenseNumber("");
            if (!self.executeMultipleTreatments()) {
                self.executionPrice(self.selectedItem().price());
            }
        }

        self.executeSelectedTreatments = function () {
            if (self.getSelectedIds().length > 0) {
                self.executeMultipleTreatments(true);
                self.selectedItem = ko.observable(undefined);
                if (parseInt($('#defaultDr').val())) {
                    self.selectedDoctorId($('#defaultDr').val());
                }

                var IsRabiesVaccine = false;
                var names = "";
                for (var i = 0; i < self.vaccines().length ; i++) {
                    var vaccine = self.vaccines()[i];
                    if (vaccine.selected()) {
                        names += ", " + vaccine.name();
                    }
                    if (vaccine.selected() && vaccine.isRabiesVaccine()) {
                        IsRabiesVaccine = true;
                    }
                }

                for (var i = 0; i < self.treatments().length ; i++) {
                    var treatment = self.treatments()[i];
                    if (treatment.selected()) {
                        names += ", " + treatment.name();
                    }
                }

                self.selectedItemName(names.substring(1, names.length));
                if (self.isDog() == "True" && IsRabiesVaccine == true) {
                    self.rabiesSectionVisible(true);
                    self.modalCssClass('modalWithRabies');
                    self.modalBodyCssClass('modalBodyWithRabies');
                }
                else {
                    self.rabiesSectionVisible(false);
                    self.modalCssClass('modalWithoutRabies');
                    self.modalBodyCssClass('modalBodyWithoutRabies');
                }

                initExecutionModal();
                $('#executionModal').modal('show');
            }
        };

        self.executeMultipleTreatments = ko.observable(false);
        self.executionPrice = ko.observable();

        self.showExecutionModal = function (element) {
            self.executeMultipleTreatments(false);
            self.selectedItem = ko.observable(element);
            self.selectedItemName(self.selectedItem().name());
            //$.getJSON('/Preventive/GetPrePreformanceData/' + self.patientId() + '?priceListItemId=' + self.selectedItem().priceListItemId(), function (data) {

            //});

            if (parseInt($('#defaultDr').val())) {
                self.selectedDoctorId($('#defaultDr').val());
            }

            if (self.isDog() == "True" && element.isRabiesVaccine() == true) {
                self.rabiesSectionVisible(true);
                self.modalCssClass('modalWithRabies');
                self.modalBodyCssClass('modalBodyWithRabies');
            }
            else {
                self.rabiesSectionVisible(false);
                self.modalCssClass('modalWithoutRabies');
                self.modalBodyCssClass('modalBodyWithoutRabies');
            }

            initExecutionModal();

            if (element.last() == getTodayFormatted()) {
                $('#confirmModal').modal('show');
            }
            else {
                $('#executionModal').modal('show');
            }
        };



        function getTodayFormatted() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            return dd + '/' + mm + '/' + yyyy;
        }

        ko.bindingHandlers.datepicker = {
            init: function (element, valueAccessor, allBindingsAccessor) {
                //initialize datepicker with some optional options
                //  var options = allBindingsAccessor().datepickerOptions || { dateFormat: 'dd/mm/yy' };
                //  $(element).datepicker(options);

                $(element).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    yearRange: "-100:+3"
                });

                //handle the field changing
                ko.utils.registerEventHandler(element, "change", function () {
                    var observable = valueAccessor();
                    observable($(element).datepicker("getDate"));
                    $(element).datepicker(valueAccessor());
                });

                //handle disposal (if KO removes by the template binding)
                ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                    $(element).datepicker("destroy");
                });

            },
            //update the control when the view model changes
            update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var value = ko.utils.unwrapObservable(valueAccessor()),
                    current = $(element).datepicker("getDate");

                if (value - current !== 0) {
                    $(element).datepicker("setDate", value);

                }
            }
        };

        self.preformItem = function () {
            if (self.validate()) {
                $('#executionModal').modal('hide');
                ShowLoder();
                if (!self.executeMultipleTreatments()) {
                    var printDogOwnerLicense = self.rabiesPrint() == 'printDogOwnerLicense';
                    var printRabiesVaccineSlip = self.rabiesPrint() == 'printRabiesVaccineSlip';

                    $.ajax({
                        type: "POST",
                        url: '/Preventive/PreformPreventiveMedicineItem',
                        data: {
                            'PatientId': self.patientId(),
                            'PriceListItemId': self.selectedItem().priceListItemId(),
                            'PriceListItemTypeId': self.selectedItem().priceListItemTypeId(),
                            'PreformingDoctor': self.selectedDoctorId(),
                            'Comment': self.comment(),
                            'Price': self.executionPrice(),
                            'ExternallyPreformed': self.regularDr().toLowerCase() == "true" ? false : true,
                            'PrintDogOwnerLicense': printDogOwnerLicense,
                            'PrintRabiesVaccineSlip': printRabiesVaccineSlip,
                            'ExternalDrName': self.externalDrName(),
                            'ExternalDrLicenseNumber': self.externalDrLicenseNumber(),
                            'BatchNumber': self.batchNumber(),
                            'PerformedOn': moment().format("DD/MM/YYYY") === self.date() ? self.date() : new Date(self.date()).toISOString(),//moment(self.date(), "DD/MM/YYYY").toISOString(),
                            'ElectronicNumber': self.electronicNumber(),
                            'LicenseNumber': self.licenseNumber(),
                            'ClientPaidOrReturnedTollVaucher': self.clientPaidOrReturnedTollVaucher(),
                            'UpdateScheduledPreventiveItems': true,
                            'AddToVisit': true

                        },
                        success: function (data) {
                            hideLoder();
                            if (data.success) {
                                window.location.href = data.url;
                            } else {
                                getCookieMessages();
                                window.location.reload();
                            }
                        },
                        failure: function () {
                            getCookieMessages();
                            window.location.reload();
                        }
                    });
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: '/Preventive/PreformMultiplePreventiveMedicineItems',
                        data: {
                            'PatientId': self.patientId(),
                            'PriceListItemId': 0,
                            'PriceListItemTypeId': 0,
                            'PreformingDoctor': self.selectedDoctorId(),
                            'Comment': self.comment(),
                            'Price': 0,
                            'ExternallyPreformed': self.regularDr().toLowerCase() == "true" ? false : true,
                            'PrintDogOwnerLicense': false,
                            'PrintRabiesVaccineSlip': false,
                            'ExternalDrName': self.externalDrName(),
                            'ExternalDrLicenseNumber': self.externalDrLicenseNumber(),
                            'BatchNumber': self.batchNumber(),
                            'PerformedOn': moment().format("DD/MM/YYYY") === self.date() ? self.date() : new Date(self.date()).toISOString(),//moment(self.date(), "DD/MM/YYYY").toISOString(),
                            'ElectronicNumber': self.electronicNumber(),
                            'LicenseNumber': self.licenseNumber(),
                            'ClientPaidOrReturnedTollVaucher': self.clientPaidOrReturnedTollVaucher(),
                            'UpdateScheduledPreventiveItems': true,
                            'AddToVisit': true,
                            'selectedPriceListItemIds': self.getSelectedIds()

                        },
                        success: function (data) {
                            hideLoder();
                            if (data.success) {
                                window.location.href = data.url;
                            } else {
                                getCookieMessages();
                                window.location.reload();
                            }
                        },
                        failure: function () {
                            getCookieMessages();
                            window.location.reload();
                        }
                    });
                }
            }
        };

        self.validate = function () {
            var errors = ko.validation.group(viewModel, { deep: true });
            errors.showAllMessages(true);
            return errors().length == 0;
        };

        self.init();

    }

    var viewModel = new viewModel();

    //ko.applyBindings(new viewModel());
    ko.validation.registerExtenders();
    ko.validation.init({
        insertMessages: true, messageTemplate: 'error-template'
    });

    ko.applyBindings(viewModel);

});