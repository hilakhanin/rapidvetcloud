﻿$(function () {
    CKEDITOR.replace('editor1');


    function viewModel() {
        var self = this;
        self.patientId = ko.observable($('#PatientId').val());
        self.letterId = ko.observable($('#letterId').val());
        self.LetterTemplateTypeId = ko.observable($('#LetterTemplateTypeId').val());
        self.content = ko.observable();
        self.showInHistory = ko.observable(true);
        self.print = ko.observable(false);



        self.save = function () {
            self.content(CKEDITOR.instances.editor1.getData());

            ShowLoder();

            $.ajax({
                type: "POST",
                url: '/Letters/ShowLetter',
                data: {
                    'Id': self.letterId(),
                    'PatientId': self.patientId(),
                    'Content': self.content(),
                    'LetterTemplateTypeId': self.LetterTemplateTypeId(),
                    'ShowInPatientHistory': self.showInHistory(),
                    'Print': self.print()
                },
                success: function (data) {
                    if (data.success) {
                        if (self.print()) {
                            window.open(data.printUrl, "_blank");
                        }
                        window.location.href = data.url;
                    }
                },
                failure: function () {
                    hideLoder();
                    getCookieMessages();
                }
            });
        };

        self.saveForm = function () {
            self.save();
        };

        self.saveAndPrint = function () {
            self.print(true);
            self.save();
        };
    }


    ko.applyBindings(new viewModel());

});
