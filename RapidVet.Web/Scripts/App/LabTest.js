﻿$(function () {
    
    ko.validation.rules['required'].message = "שדה הכרחי";
    
    function Test(data) {
        var self = this;
        self.id = ko.observable(data.Id);
        self.name = ko.observable(data.Name);
        self.minValue = ko.observable(data.MinValue).extend({
            validation: {
                validator: function (val) {
                    return !isNaN(val);
                },
                message: "יש להכניס מספר חוקי"
            },
            required: true
        });
        self.maxValue = ko.observable(data.MaxValue).extend({
            validation: {
                validator: function (val) {
                    return !isNaN(val);
                },
                message: "יש להכניס מספר חוקי"
            },
            required: true
        });
    }

    function viewModel() {

        var self = this;
        self.id = ko.observable($('#labTestTemplateId').val());
        self.tests = ko.observableArray([]);

        //data

        self.getData = function () {
            getCookieMessages();
            $.getJSON('/LabTests/GetLabTestData/' + self.id(), function (data) {
                var mapped = $.map(data, function (item) {
                    return new Test(item);
                });
                self.tests(mapped);
            });
        };

        self.saveData = function () {
            if (self.validate()) {
                ShowLoder();
                $.post('/LabTests/SaveLabTestData/' + self.id(), { Data: ko.toJSON(self.tests()) }, function(data) {
                 
                    if (data.Success) {
                        window.location.reload();
                    }
                });
            }
        };

        //operations
        self.addTest = function () {
            var temp = {
                'Id': 0,
                'Name': ' ',
                'MinValue': 0,
                'MaxValue': 0
            };
            self.tests.push(new Test(temp));
        };

        self.removeTest = function (element) {
            self.tests.remove(element);
        };

        self.validate = function () {
            var errors = ko.validation.group(this, { deep: true });
            errors.showAllMessages();
            return errors().length == 0;
        };


        self.getData();
    }

    ko.applyBindings(new viewModel());
});