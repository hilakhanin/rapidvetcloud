﻿$(function () {

   
    function Stage(data) {
        var self = this;
        self.number = data.Value;
        self.name = ko.observable(data.Text);
        self.info = ko.observable();
        
        //ststus: 1 - waiting ; 2 - in progress; 3 - done; 4 - failed; 5 - aborted;
        self.status = ko.observable(1);
        self.statusText = ko.computed(function () {
            var result = '';
            if (self.status() == 2) {
                result = '<span class="label label-info">בתהליך</span>';
            } else if (self.status() == 3) {
                result = '<span class="label label-success">הסתיים</span>';
            }
            return result;
        });
    }

    function viewModel() {

        var self = this;
        self.stages = ko.observableArray([]);
        self.currentStage = ko.observable(1);


        self.getStages = function () {

            $.getJSON('/DataMigration/GetMigrationStages', function (data) {
                var mapped = $.map(data, function (item) {
                    return new Stage(item);
                });
                self.stages(mapped);
                self.updateInfo();
            });
        };
        var finish = false;

        self.updateInfo = function () {

            if (!finish) {
                $.getJSON('/DataMigration/CheckMigrationStatus', function (data) {

                    var arrayIndex = data.stage - 1;

                    if (data.stage == self.stages().length) {
                        finish = true;
                    }

                    //process finished
                    for (var i = 0; i < data.stage; i++) {
                        self.stages()[i].status(3);
                    }

                    self.stages()[arrayIndex].status(2);

                    for (var j = 0; j < data.status.length; j++) {
                        var item = data.status[j];
                        var arr = jQuery.grep(self.stages(), function (n, i) {
                            return (n.number == item.MigrationStage);

                        });
                        arr[0].info(item.Status);
                    }

                });
            } else {
                return window.location.href = '/DataMigration/MigrationComplete/';
            }

        };


        //execution
        self.getStages();

        window.setInterval(self.updateInfo, 1000);

    }

    ko.applyBindings(new viewModel());

});