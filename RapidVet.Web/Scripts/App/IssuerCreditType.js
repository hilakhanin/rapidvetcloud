﻿$(function () {

    function CreditType(data) {
        var self = this;
        self.id = ko.observable(data.Id);
        self.name = ko.observable(data.Name);
        self.postponedPayment = ko.observable(data.PostponedPayment);
    }


    function viewModel() {

        var self = this;
        self.issuerId = ko.observable($('#issuerId').val());
        self.types = ko.observableArray([]);

        //actions
        self.add = function () {
            self.types.push(new CreditType({ 'Id': 0, 'Name': null, 'PostponedPayment': false }));
        };

        self.save = function (element) {
            ShowLoder();

            $.ajax({
                type: "POST",
                url: '/IssuerCreditTypes/Save',
                data: {
                    'Id': element.id(),
                    'IssuerId': self.issuerId(),
                    'Name': element.name(),
                    'PostponedPayment': element.postponedPayment()                    
                },
                success: function (data) {
                    if (data.success && element.id() == 0) {
                        element.id(data.ItemId);
                    }
                }
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        };

        self.remove = function (element) {

            if (element.id() > 0) {

                ShowLoder();

                $.ajax({
                    type: "POST",
                    url: '/IssuerCreditTypes/Delete',
                    data: {
                        'Id': element.id(),
                        'IssuerId': self.issuerId(),
                        'Name': element.name(),
                        'PostponedPayment': element.postponedPayment()
                        
                    },
                    success: function (data) {
                        if (data.success && element.id() == 0) {
                            element.id(data.ItemId);
                        }
                    }
                }).done(function () {
                    hideLoder();
                    getCookieMessages();
                });
            }

            self.types.remove(element);
        };

        self.getData = function () {

            ShowLoder();

            $.getJSON('/IssuerCreditTypes/GetIssuerData/' + self.issuerId(), function (data) {

                var mapped = $.map(data, function (item) {
                    return new CreditType(item);
                });
                self.types(mapped);
            }).done(function () {
                hideLoder();
            });
        };

        //execution
        self.getData();

    }

    ko.applyBindings(new viewModel());


});