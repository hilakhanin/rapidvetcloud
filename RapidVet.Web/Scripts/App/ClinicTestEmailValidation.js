﻿var viewModel;

function Percent(data) {
    var self = this;
    self.id = ko.observable(data.Id);
    self.date = ko.observable(data.Date).extend({ required: { params: true, message: "שדה חובה" } });
    self.oldDate = ko.observable(data.Date);
    self.dateChange = ko.computed(function () {
        return self.date() != self.oldDate();
    });

    self.percent = ko.observable(data.Percent).extend({ required: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 0, message: "ערך מינימלי: 0" } })
        .extend({ max: { params: 100, message: "ערך מקסימלי: 100" } });;
    self.oldPercent = ko.observable(data.Percent);
    self.percentChenge = ko.computed(function () {
        return self.percent() != self.oldPercent();
    });

    self.pendingChanges = ko.computed(function () {
        var result = false;
        if (self.dateChange() == true || self.percentChenge() == true) {
            result = true;
        }

        return result;
    });

    self.validate = function () {
        var errors = ko.validation.group(self, { deep: true, observable: true });
        errors.showAllMessages(true);
        return errors().length == 0;
    };
}



function viewModel() {
    var self = this;

    self.emailPort = ko.observable($("#EmailPort").val()).extend({ required: { params: true, message: " שדה חובה " } });
    self.emailSmtp = ko.observable($("#EmailSmtp").val()).extend({ required: { params: true, message: " שדה חובה " } });
    self.emailAddress = ko.observable($("#EmailAddress").val()).extend({ required: { params: true, message: " שדה חובה " } });
    self.emailUserName = ko.observable($("#EmailUserName").val()).extend({ required: { params: true, message: " שדה חובה " } });
    self.emailPassword = ko.observable($("#EmailPassword").val()).extend({ required: { params: true, message: " שדה חובה " } });
    self.isAdmin = ko.observable();
    self.activeUsers = ko.observable();
    self.nonActiveUsers = ko.observable();
    self.maxConcurrentUsers = ko.observable($("#MaxConcurrentUsers").val());                               
    self.MaxGoogleSyncAllowed = ko.observable($("#MaxGoogleSyncAllowed").val());
    self.timeToDisconnectInactiveUsersInMinutes = ko.observable($("#TimeToDisconnectInactiveUsersInMinutes").val());
    self.hsModule = ko.observable($("#HSModule").val());
    self.stockModule = ko.observable($("#StockModule").val());

    self.getData = function () {
        $.get('/ClinicGroups/GetAdministrativeData', null, function (data) {
            self.isAdmin(data.IsAdministrator);
            self.activeUsers(data.ActiveUsers);
            self.nonActiveUsers(data.NonActiveUsers);
        });
    };



    //validation
    self.isValid = function () {
        var errors = ko.validation.group(viewModel, { deep: true });
        errors.showAllMessages(true);

        if (errors().length == 0)
        {
            $("#MaxConcurrentUsers").prop("disabled", false);
            $("#MaxGoogleSyncAllowed").prop("disabled", false);
            $("#TimeToDisconnectInactiveUsersInMinutes").prop("disabled", false);
            $("#HSModule").prop("disabled", false);
            $("#StockModule").prop("disabled", false);

        }
        return errors().length == 0;
    };

    self.submitForm = function () {
        if (self.isValid()) {
            self.testData();
        }
    };

    ////functions
    self.testData = function () {
        ShowLoder();
        var url = "/ClinicGroups/SendTestMail";
        var emailPort = self.emailPort;
        var emailSmtp = self.emailSmtp;
        var emailAddress = self.emailAddress;
        var emailUserName = self.emailUserName;
        var emailPassword = self.emailPassword;
        var isSecuredSmtp = $("#IsSecuredSmtp").val();
        var emailMaxRecipients = (typeof $("#EmailMaxRecipients").val() === "undefined" ? 0 : $("#EmailMaxRecipients").val());
        var emailPauseInSeconds = (typeof $("#EmailPauseInSeconds").val() === "undefined" ? 0 : $("#EmailPauseInSeconds").val());



        var params = {
            EmailPort: emailPort, EmailSmtp: emailSmtp, EmailAddress: emailAddress,
            EmailUserName: emailUserName, EmailPassword: emailPassword, IsSecuredSmtp: isSecuredSmtp,
            EmailMaxRecipients: emailMaxRecipients, EmailPauseInSeconds: emailPauseInSeconds
        };

        $.get(url, params, function (data) {
            console.log("success");
            hideLoder();
            $("#statusMsg").val(data);
            $("#statusMsg").text(data);
        });

    };

    ////execution
    self.getData();
}




$(function () {
    $("#LogoImage").rules("add", {
        accept: "jpg|jpeg|png|ico|bmp"
    });

    ko.validation.init({
        decopercentElement: true,
        decopercentInputElement: true,
        errorElementClass: 'input-validation-error',

    });

    ko.validation.rules['adminPositiveValidation'] = {
        validator: function (val, otherVal) {
            /*important to use otherValue(), because ko.computed returns a function, 
            and the value is inside that function*/
            otherVal = otherVal();
            return  parseInt(val) - otherVal.secondNumber >= 0;
        },
        message: "כמות המשתמשים המבוקשת קטנה מכמות המשתמשים בשימוש"
    };
    ko.validation.registerExtenders();

    viewModel = new viewModel();

    ko.applyBindings(viewModel);
});

