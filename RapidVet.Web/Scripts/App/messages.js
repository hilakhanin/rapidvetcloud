﻿function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            y = y.split('+').join(' ');
            return decodeURIComponent(y);
        }
    }
}
function delCookie(name) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function getCookieMessages() {
    if (toastr.options.positionClass != "toast-top-left") {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-left",            
            "onclick": null
        };
    }

    var successMsg = getCookie("successmsg");
    if (successMsg != null) {
        jaaulde.utils.cookies.del('successmsg');
        toastr.success(successMsg);
    }
    var errorMsg = getCookie("errormsg");
    if (errorMsg != null) {
        jaaulde.utils.cookies.del('errormsg');
        toastr.error(errorMsg);
    }
}

$(function () {   
    getCookieMessages();
});

function setCookieMessage(name,value,days,hours,minutes) {
    if (parseInt(days) || parseInt(hours) || parseInt(minutes)) {
        var date = new Date();
       
        //date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000)); //days*24*60*60*1000 ==> days * hours  * minutes * 1000 
        if (parseInt(days))
        {
            date.setDate(date.getDate() + parseInt(days));
        }

        
        var hour = date.getHours();
        var min = date.getMinutes()

        if (parseInt(minutes))
        {
            min += parseInt(minutes);
            if(min > 59)
            {
                hour += 1;
                min -= 60;
            }
        }

        if (parseInt(hours)) {
            hour += parseInt(hours);
            if (hour > 23) {
                date.setDate(date.getDate() + 1);
                hour -= 24;
            }
        }

        
        date.setHours(hour, min, date.getSeconds(), date.getMilliseconds());

        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}