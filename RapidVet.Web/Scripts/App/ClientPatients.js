﻿var vm;
function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

function clearClientSearch(element) {
    vm.selectedClientId(undefined);
    vm.ClientNameWithPatients("");    
    element.value = "";
    $(".no-results").hide();
};

$(function () {
    getCookieComments();
    $(".client-search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Clients/Search",
                type: "POST",
                data: {
                    query: request.term
                },
                success: function (data) {
                    response(data);

                    if (data.length == 0) {
                        $(".no-results").show();
                        setTimeout(function () { $(".no-results").hide(); }, 5000);
                    }
                    else {
                        $(".no-results").hide();
                    }
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            event.preventDefault();
            vm.ClientNameWithPatients(ui.item.label);
            vm.selectedClientId(ui.item.value.toString());
            this.value = ui.item.label;

            //window.location = "/Clients/Patients/" + ui.item.value;
        },
        focus: function (event, ui) {
            event.preventDefault();
            this.value = ui.item.label;
        }
        ,
        change: function (event, ui) {
            event.preventDefault();
            if (isEmptyOrWhiteSpaces(this.value) ||
                typeof vm.selectedClientId() == "undefined" || vm.ClientNameWithPatients() != this.value) {
                clearClientSearch(this);
            }

        },
        open: function () {
            $(".ui-autocomplete").css("max-height", 120);
            $(".ui-autocomplete").css("max-width", 225);
            $(".ui-autocomplete").css("overflow-y", "auto");
            $(".ui-autocomplete").css("overflow-x", "hidden");

        }
    });
});

$(function () {

    function viewModel() {
        var self = this;
        vm = self;
        self.clients = ko.mapping.fromJS([]);
        self.clientOptions = ko.computed(function () {

            return ko.utils.arrayFilter(self.clients(), function (p) {
                if (p.Value() != self.clientId()) {
                    return p;
                }
            });

        });
        self.selectedClientId = ko.observable();
        self.ClientNameWithPatients = ko.observable("");

        self.patients = ko.mapping.fromJS([]);
        self.selectedPatientId = ko.observable();

        self.clientId = ko.observable($('#clientId').val());

        self.getClients = function () {
            $.getJSON('/Clinics/GetClinicClients', function (data) {
                ko.mapping.fromJS(data, self.clients);
            });
        };

        self.getClientPatients = function () {
            $.getJSON('/Clients/GetClientPatients?clientId=' + self.clientId(), function (data) {
                ko.mapping.fromJS(data, self.patients);
            });
        };


        self.init = function () {
          //  self.getClients();
            self.getClientPatients();
          //  self.getClients();
        };

        self.showModal = function () {
         //   self.getClients();
            $('#ownershipModal').modal('show');
        };

        self.hideModal = function () {
            self.selectedPatientId(null);
            self.selectedClientId(null);
            $('#ownershipModal').modal('hide');
        };

        self.showDiary = function () {
            window.location.href = "/Calender?ClientId=" + self.clientId() + "&" + "ClientName=" + escape($(".breadcrumb li")[2].innerText);
        };

        self.init();
    }

    ko.applyBindings(new viewModel());
});