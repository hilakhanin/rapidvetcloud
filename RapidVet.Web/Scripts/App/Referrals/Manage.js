﻿var vm;
function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

function clearClientSearch(element) {
    vm.selectedClientId(undefined);
    vm.ClientNameWithPatients("");
    vm.idCard("");
    element.value = "";
    $(".no-results").hide();
};

$(function () {

    $(".client-search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Clients/Search",
                type: "POST",
                data: {
                    query: request.term
                },
                success: function (data) {
                    response(data);

                    if (data.length == 0) {
                        $(".no-results").show();
                        setTimeout(function () { $(".no-results").hide(); }, 5000);
                    }
                    else {
                        $(".no-results").hide();
                    }
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            event.preventDefault();
            vm.ClientNameWithPatients(ui.item.label);
            vm.selectedClientId(ui.item.value.toString());
            this.value = ui.item.label;

            //window.location = "/Clients/Patients/" + ui.item.value;
        },
        focus: function (event, ui) {
            event.preventDefault();
            this.value = ui.item.label;
        }
        ,
        change: function (event, ui) {
            event.preventDefault();
            if (isEmptyOrWhiteSpaces(this.value) ||
                typeof vm.selectedClientId() == "undefined" || vm.ClientNameWithPatients() != this.value) {
                clearClientSearch(this);
            }

        },
        open: function () {
            $(".ui-autocomplete").css("max-height", 120);
            $(".ui-autocomplete").css("max-width", 225);
            $(".ui-autocomplete").css("overflow-y", "auto");
            $(".ui-autocomplete").css("overflow-x", "hidden");

        }
    });
});

$(function () {

    function viewModel() {

        var self = this;
        vm = self;
        self.clients = ko.mapping.fromJS([]);
        var selectedClient = $('#initialClientId').val();
        self.selectedClientId = ko.observable(selectedClient);
        self.ClientNameWithPatients = ko.observable("");
        self.idCard = ko.observable();

        self.patients = ko.mapping.fromJS([]);
        var selectedPatient = $('#initialPatientId').val();
        self.selectedPatientId = ko.observable(selectedPatient);

        self.getClients = function () {
            $.getJSON('/Clinics/GetClinicClients', function (data) {
                ko.mapping.fromJS(data, self.clients);
            }).done(function () {
                if (typeof self.selectedClientId() == "undefined") {
                    self.selectedClientId(selectedClient);
                }
            });
        };

        self.referralDoctors = ko.mapping.fromJS([]);
        var referredDoctorId = $('#initialreferredDoctorId').val();
        self.selectedReferralDoctorId = ko.observable(referredDoctorId);

        self.isExternalReferral = ko.computed(function () {
            return self.selectedReferralDoctorId() == -9;
        });

        self.getReferralDoctors = function () {
            $.getJSON('/Referrals/GetReferralDoctors', function (data) {
                ko.mapping.fromJS(data, self.referralDoctors);
            }).done(function () {
                if (typeof self.selectedReferralDoctorId() == "undefined") {
                    self.selectedReferralDoctorId(referredDoctorId);
                }
            });
        };

        self.getClientIdCard = ko.computed(function () {
            if (self.selectedClientId() > 0) {
                $.getJSON('/Clients/GetClientIdCard?clientId=' + self.selectedClientId(), function (data) {
                    self.idCard(data.idCard);
                    if (isEmptyOrWhiteSpaces(self.ClientNameWithPatients())) {
                        self.ClientNameWithPatients(data.clientName);
                        $(".client-search").val(data.clientName);
                    }
                });
            }
            else
            {
                self.patients([]);
                self.selectedPatientId(undefined);
            }
        });

        self.getPatients = function () {
            $.getJSON('/Clients/GetClientPatients?clientId=' + self.selectedClientId(), function (data) {
                ko.mapping.fromJS(data, self.patients);
            }).done(function () {
                if (typeof self.selectedPatientId() == "undefined" && self.selectedClientId() == selectedClient) {
                    self.selectedPatientId(selectedPatient);
                }
            });

        };

        self.patientOptions = ko.computed(function () {
            if (self.selectedClientId() > 0) {
                self.patients([]);
                self.getPatients();
            }
        });

        self.init = function () {
         //   self.getClients();
            self.getReferralDoctors();
        };

        self.init();
    }

    ko.applyBindings(new viewModel());

    $('#btnOkAndPrint').click(function (event) {
        document.getElementById('IsForPrint').value = 'True';
        event.preventDefault();
        var form = $('form');
        form.attr('target', '_blank');
        form.submit();
    });
});