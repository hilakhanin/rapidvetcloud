﻿$(function () {

    function viewModel() {

        var self = this;
        self.from = ko.observable($('#from').val());
        self.to = ko.observable($('#to').val());
        self.months = ko.observable($('#months').val());
        self.method = ko.observable('0');


        self.searchLink = ko.computed(function () {
            return '/Referrals/Index?fromDate=' + self.from() 
                + '&toDate=' + self.to() 
                + '&months=' + self.months() 
                + '&methos=' + self.method() ;
        });

    }

    ko.applyBindings(new viewModel());
});