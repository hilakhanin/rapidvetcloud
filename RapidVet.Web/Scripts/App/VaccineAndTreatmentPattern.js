﻿$(function () {

    function priceListItemMap(item) {
        var self = this;
        self.name = ko.observable(item.Name);
        self.id = ko.observable(item.Id);
    }

    function vaccineAndTreatmentMap(item) {
        var self = this;
        self.name = ko.observable(item.PriceListItem.Name);
        self.id = ko.observable(item.PriceListItemId);
        self.order = ko.observable(item.Order);
    }

    function vaccineAndTreatmentFromMappedMap(item) {
        var self = this;
        self.name = ko.observable(item.name());
        self.id = ko.observable(item.id());
        self.order = ko.observable();
    }

    function vAndTPatternViewModel() {

        /// view model fields
        var self = this;
        self.allItems = ko.observableArray([]);
        self.selectedItems = ko.observableArray([]);
        self.animalKind = ko.observableArray([]);
        self.animalKindId = ko.observable();
        self.selectedItem = ko.observable();
        ///-------- fields end-----------

        /// getting data from server and page
        var animalKindId = $('input[type="hidden"][name="Id"]').val();
        if (!animalKindId) {
            animalKindId = 0;
        }
        //general pattern fields
        $.getJSON('/VaccinesAndTreatments/PatternForAnimal/' + animalKindId, function (data) {

            var allItems = $.map(data.AllItems, function (item) {
                return new priceListItemMap(item);
            });
            var selectedItems = $.map(data.SelectedItems, function (item) {
                return new vaccineAndTreatmentMap(item);
            });

            self.allItems(allItems);
            self.selectedItems(selectedItems);
            self.animalKind(data.AnimalKind);
            self.animalKindId(data.AnimalKindId);
        });

        self.addItem = function () {
            var matchItem = $.grep(self.allItems(), function (item) { return item.id() == self.selectedItem(); });
            var newItem = new vaccineAndTreatmentFromMappedMap(matchItem[0]);
            self.selectedItems.push(newItem);
        };

        self.removeItem = function (element) {
            self.selectedItems.remove(element);
        };

        self.save = function () {
            //var itemsJson = JSON.stringify(self.selectedItems());
            var itemsJson = ko.toJSON(self.selectedItems());

            $.post("../SaveVaccinesAndTreatments/" + animalKindId, { itemsJson: itemsJson }, function (data) {
                getCookieMessages();
            });
        };

        ///-------- getting data ends------

        ///operations

        ///-------operations end --------
    }

    ko.applyBindings(vAndTPatternViewModel());
});