﻿$(function () {

    function Procedure(data) {
        var self = this;
        self.id = ko.observable(data.Id);
        self.patientId = ko.observable(data.PatientId);
        self.letter = ko.observable(data.Letter);
        self.editorId = ko.computed(function () { return 'e_' + self.id() });
    }

    var viewModel = function () {

        var self = this;
        self.idToPrint = ko.observable($('#id').val());
        self.from = ko.observable($('#from').val());
        self.to = ko.observable($('#to').val());
        self.printUrl = ko.computed(function () {
            return '/MedicalProcedures/Print?id=' + self.idToPrint() + '&from=' + self.from() + '&to=' + self.to();
        });

        self.procedures = ko.observableArray([]);
        self.success = ko.observable(false);
        self.print = ko.observable(false);

        self.getData = function () {

            ShowLoder();

            $.getJSON('/MedicalProcedures/GetPrintingData?id=' + self.idToPrint() + '&from=' + self.from() + '&to=' + self.to(), function (data) {

                var mappedProcedures = $.map(data, function (item) {
                    return new Procedure(item);
                });

                self.procedures(mappedProcedures);

            }).done(function () {
                hideLoder();
            });
        };

        self.initTextEditor = function (elements, data) {

            //if (self.procedures()[self.procedures().length - 1] === data) {
            //    $('.summernote').summernote();
            //}
            CKEDITOR.replace(data.editorId());

        };



        self.save = function () {

            ko.utils.arrayForEach(self.procedures(), function (p) {
                var editor = p.editorId();
                var content = CKEDITOR.instances[editor].getData(); //$('#' + p.editorId()).siblings('.note-editor').children('.note-editable').html();
                p.letter(content);
            });

            ShowLoder();
            $.ajax({
                type: "POST",
                url: '/MedicalProcedures/SaveBeforePrint',
                data: {
                    'Letters': ko.toJSON(self.procedures())
                },
                success: function (data) {

                    self.success = ko.observable(data);
                    if (self.success() == true) {
                        
                        if (self.print() == true) {
                            window.open(self.printUrl(), "_blank");
                        }
                        
                        window.location.href = '/MedicalProcedures/Index';
                    }
                }
            }).done(function () {
                if (self.success() == false) {
                    hideLoder();
                    getCookieMessages();
                }
            });

        };


        self.saveAndPrint = function () {
            self.print(true);
            self.save();
        };

        self.getData();

    };

    ko.applyBindings(new viewModel());

});