﻿$(function () {

    function viewModel() {

        var self = this;
        self.from = ko.observable($('#from').val());

        self.to = ko.observable($('#to').val());

        self.searchLink = ko.computed(function () {
            return '/MedicalProcedures/Index?fromDate=' + self.from() + '&toDate=' + self.to();
        });

        self.printAllLink = ko.computed(function () {
            return '/MedicalProcedures/PrePrint?fromDate=' + self.from() + '&toDate=' + self.to();
        });

    }

    ko.applyBindings(new viewModel());
});