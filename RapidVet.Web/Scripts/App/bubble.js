﻿$(function () {
    $('#selectDeselectAll').change(function () {
        if($('#selectDeselectAll')[0].checked)
        {
            $('.li-group-cb').attr("checked", "checked");
        }
        else
        {
            $('.li-group-cb').removeAttr("checked");
        }

        $('.li-group-cb').change();
    });
});