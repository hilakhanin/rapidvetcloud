﻿$(function () {

    function visitViewModel() {

        /// view model fields
        var self = this;
        self.visit = ko.observableArray([]);
        self.examinations = ko.observableArray([]);
        self.diagnoses = ko.observableArray([]);
        self.treatments = ko.observableArray([]);
        self.medications = ko.observableArray([]);
        ///-------- fields end-----------

        /// getting data from server and page
        var visitId = $('input[type="hidden"][name="Id"]').val();
        if (!visitId) {
            visitId = 0;
        }
        //general visit fields
        $.getJSON('/Visits/Visit/' + visitId, function (data) {
            self.visit(data);
        });

        //all examinations, treatments, diagnoses and medications from db for this clinic
        //item prices by client tariffId ,if null then by clinic default tariff id
        var clientId = $('input[type="hidden"][name="ClientId"]').val();
        $.getJSON('/Visits/VisitSectionsData/' + clientId, function (data) {

            self.diagnoses(data.Diagnoses);
            self.examinations(data.Examinations);
            self.treatments(data.Treatments);
            self.medications(data.Medications);
        });

        ///-------- getting data ends------

        ///operations

        ///-------operations end --------
    }

    ko.applyBindings(visitViewModel());
});