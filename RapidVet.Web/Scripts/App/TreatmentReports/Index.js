﻿var vm;
function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

function clearClientSearch(element) {
    vm.selectedClientId(undefined);
    vm.ClientNameWithPatients("");   
    element.value = "";
    $(".no-results").hide();
};

$(function () {

    $(".client-search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Clients/Search",
                type: "POST",
                data: {
                    query: request.term
                },
                success: function (data) {
                    response(data);

                    if (data.length == 0) {
                        $(".no-results").show();
                        setTimeout(function () { $(".no-results").hide(); }, 5000);
                    }
                    else {
                        $(".no-results").hide();
                    }
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            event.preventDefault();
            vm.ClientNameWithPatients(ui.item.label);
            vm.selectedClientId(ui.item.value.toString());
            this.value = ui.item.label;

            //window.location = "/Clients/Patients/" + ui.item.value;
        },
        focus: function (event, ui) {
            event.preventDefault();
            this.value = ui.item.label;
        }
        ,
        change: function (event, ui) {
            event.preventDefault();
            if (isEmptyOrWhiteSpaces(this.value) ||
                typeof vm.selectedClientId() == "undefined" || vm.ClientNameWithPatients() != this.value) {
                clearClientSearch(this);
            }

        },
        open: function () {
            $(".ui-autocomplete").css("max-height", 120);
            $(".ui-autocomplete").css("max-width", 225);
            $(".ui-autocomplete").css("overflow-y", "auto");
            $(".ui-autocomplete").css("overflow-x", "hidden");

        }
    });
});


$(function () {

    // mapper for list item
    function selectItem(value, text) {
        var self = this;
        self.Value = value.toString();
        self.Text = text;
    }

    function selectLetterTemplateItem(id, name, content) {
        var self = this;
        self.Id = id;
        self.Name = name;
        self.Content = content;
    }

    function visitList(data) {
        var self = this;

        var mapped = $.map(data.Items, function (item) {
            return new visit(item);
        });
        self.Items = ko.observable(mapped);
        self.Sum = ko.observable(data.Sum);
        self.Name = ko.observable(data.Name);
    }

    function visit(data) {
        var self = this;
        self.nameVisit = ko.observable(data.nameVisit);

        //DateTime
        if (String(data.datePerformed).indexOf('/Date(') == 0) {
            var tmp = new Date(parseInt(data.datePerformed.replace(/\/Date\((.*?)\)\//gi, "$1")));
            self.datePerformed = ko.observable([tmp.getDate(), tmp.getMonth() + 1, tmp.getFullYear()].join('/'));
        } else {
            self.datePerformed = ko.observable(data.datePerformed);
        }
        if (String(data.birthDate).indexOf('/Date(') == 0) {
            var tmp = new Date(parseInt(data.birthDate.replace(/\/Date\((.*?)\)\//gi, "$1")));
            self.birthDate = ko.observable([tmp.getDate(), tmp.getMonth() + 1, tmp.getFullYear()].join('/'));
        } else {
            self.birthDate = ko.observable(data.birthDate);
        }

        self.clientName = ko.observable(data.clientName);
        self.patientName = ko.observable(data.patientName);
        self.serial = ko.observable(data.serial);
        self.address = ko.observable(data.address);
        self.homePhone = ko.observable(data.homePhone);
        self.cellPhone = ko.observable(data.cellPhone);
        self.animalKind = ko.observable(data.animalKind);
        self.animalRace = ko.observable(data.animalRace);
        self.animalGender = ko.observable(data.animalGender);
        self.animalColor = ko.observable(data.animalColor);
        self.animalAge = ko.observable(data.animalAge);
        self.sterilization = ko.observable(data.sterilization);
        self.electronicNum = ko.observable(data.electronicNum);
        self.licenseNum = ko.observable(data.licenseNum);
        self.chargeFee = ko.observable(data.chargeFee);
        self.performingDoc = ko.observable(data.performingDoc);
        self.numOfUnits = ko.observable(data.numOfUnits);
        self.sterilization = ko.observable(data.sterilization);
        // self.plateNum = ko.observable(data.plateNum);
        self.visitDesc = ko.observable(data.visitDesc);
        self.treatingDoctor = ko.observable(data.treatingDoctor);
        self.receipt = ko.observable(data.receipt);
        self.clientId = ko.observable(data.clientId);
        self.patientId = ko.observable(data.patientId);
        self.visitId = ko.observable(data.visitId)

        self.clientUrl = ko.computed(function () {
            return '/Clients/Patients/' + self.clientId();
        });

        self.visitUrl = ko.computed(function () {            
            return '/Visit/Index/' + self.patientId() +'?visitId='+self.visitId();
        });
    }

    function viewModel() {
        var self = this;
        vm = self;
        //Visits
        self.visits = ko.mapping.fromJS([]);

        //Filters
        self.filters = ko.mapping.fromJS([]);
        self.selectedFilterId = ko.observable();
        self.filterRename = ko.observable();
        self.filterSaveToId = ko.observable();

        //Client
        self.clients = ko.mapping.fromJS([]);
        self.selectedClientId = ko.observable("");
        self.ClientNameWithPatients = ko.observable("");

        //Animal Kind
        self.animalKinds = ko.mapping.fromJS([]);
        self.selectedAnimalKindId = ko.observable("");

        //Date Range
        self.filterFromDate = ko.observable("");
        self.filterToDate = ko.observable("");

        //Animal Age Range
        self.filterAnimalFromYear = ko.observable("").extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 1, message: "ערך מינימלי: 1" } })
        .extend({ max: { params: 99, message: "ערך מקסימלי: 99" } });

        self.filterAnimalFromMonth = ko.observable("").extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 1, message: "ערך מינימלי: 1" } })
        .extend({ max: { params: 12, message: "ערך מקסימלי: 12" } });

        self.filterAnimalToYear = ko.observable("").extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 1, message: "ערך מינימלי: 1" } })
        .extend({ max: { params: 99, message: "ערך מקסימלי: 99" } });

        self.filterAnimalToMonth = ko.observable("").extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 1, message: "ערך מינימלי: 1" } })
        .extend({ max: { params: 12, message: "ערך מקסימלי: 12" } });

        //Treated By Doctor
        self.doctors = ko.mapping.fromJS([]);
        self.selectedDoctorId = ko.observableArray([]);

        //Lists
        self.categories = ko.mapping.fromJS([]);
        self.selectedCategoryId = ko.observableArray([]);

        self.treatments = ko.mapping.fromJS([]);
        self.selectedTreatmentId = ko.observableArray([]);

        self.diagnoses = ko.mapping.fromJS([]);
        self.selectedDiagnosisId = ko.observable("");

        //self.examinations = ko.mapping.fromJS([]);
        //self.selectedExaminationId = ko.observable("");

        self.medications = ko.mapping.fromJS([]);
        self.selectedMedicationId = ko.observable("");
        //Sum Range
        self.fromSum = ko.observable("").extend({ number: { params: true, message: "יש להזין ערך תקין" } });
        self.toSum = ko.observable("").extend({ number: { params: true, message: "יש להזין ערך תקין" } });

        //Keymatches
        self.visitDescription = ko.observable("");
        self.mainComplaint = ko.observable("");

        //Personal Letter
        self.personalLetters = ko.mapping.fromJS([]);
        self.selectedLetterId = ko.observable("");
        self.selectedLetterContent = ko.observable("");
        self.personalTemplateError = ko.observable(false);

        //Subscribers
        self.getTreatmentsDataOpts = function (newValue) {
            if (typeof newValue !== 'undefined') {
                $.get('/MetaData/GetTreatmentsByCategories?list=' + newValue.toString(), function (data) {
                    var mapped = $.map(data, function (item) {
                        return new selectItem(item.Id, item.Name);
                    });
                    self.treatments(mapped);
                });
            }
        };

        self.selectedLetterId.subscribe(function () {
            if (self.selectedLetterId() == "") {
                self.selectedLetterContent("");
            }
            else {
                for (var i = 0; i < self.personalLetters().length; i++) {
                    if (self.personalLetters()[i].Id == self.selectedLetterId()) {
                        self.selectedLetterContent(self.personalLetters()[i].Content);
                    }
                }
            }
        });


        self.selectedCategoryId.subscribe(self.getTreatmentsDataOpts);

        //tableOptions
        self.printFontSize = ko.observable(14);
        self.nameVisitCb = ko.observable(true);
        self.datePerformedCb = ko.observable(true);
        self.clientNameCb = ko.observable(true);
        self.patientNameCb = ko.observable(true);
        self.serialCb = ko.observable(false);
        self.addressCb = ko.observable(false);
        self.homePhoneCb = ko.observable(false);
        self.cellPhoneCb = ko.observable(false);
        self.animalKindCb = ko.observable(false);
        self.animalRaceCb = ko.observable(false);
        self.animalGenderCb = ko.observable(false);
        self.animalColorCb = ko.observable(false);
        self.animalAgeCb = ko.observable(false);
        self.sterilizationCb = ko.observable(false);
        self.electronicNumCb = ko.observable(false);
        self.birthDateCb = ko.observable(false);
        self.licenseNumCb = ko.observable(false);
        self.chargeFeeCb = ko.observable(true);
        self.performingDocCb = ko.observable(true);
        self.numOfUnitsCb = ko.observable(false);
        //  self.plateNumCb = ko.observable(false);
        self.visitDescCb = ko.observable(false);
        self.treatingDoctorCb = ko.observable(false);
        self.receiptCb = ko.observable(false);

        self.columnNum = ko.computed(function () {
            return Number(self.nameVisitCb()) +
                Number(self.datePerformedCb()) +
                Number(self.clientNameCb()) +
                Number(self.patientNameCb()) +
                Number(self.serialCb()) +
                Number(self.addressCb()) +
                Number(self.homePhoneCb()) +
                Number(self.cellPhoneCb()) +
                Number(self.animalKindCb()) +
                Number(self.animalRaceCb()) +
                Number(self.animalGenderCb()) +
                Number(self.animalColorCb()) +
                Number(self.animalAgeCb()) +
                Number(self.sterilizationCb()) +
                Number(self.electronicNumCb()) +
                Number(self.birthDateCb()) +
                Number(self.licenseNumCb()) +
                Number(self.chargeFeeCb()) +
                Number(self.performingDocCb()) +
                Number(self.numOfUnitsCb()) +
                Number(self.visitDescCb()) +
                Number(self.treatingDoctorCb()) +
                Number(self.receiptCb());
        });

        //urls
        self.filterUrl = ko.computed(function () {
            var url =
                '?FromDate=' + self.filterFromDate() +
                    '&ToDate=' + self.filterToDate() +
                    '&ClientId=' + self.selectedClientId() +
                    '&AnimalKindId=' + self.selectedAnimalKindId() +
                    '&FromYear=' + self.filterAnimalFromYear() +
                    '&FromMonth=' + self.filterAnimalFromMonth() +
                    '&ToYear=' + self.filterAnimalToYear() +
                    '&ToMonth=' + self.filterAnimalToMonth() +
                    '&DoctorId=' + self.selectedDoctorId() +
                    '&CategoryId=' + self.selectedCategoryId() +
                    '&TreatmentId=' + self.selectedTreatmentId() +
                    '&DiagnosisId=' + self.selectedDiagnosisId() +
                    //'&ExaminationId=' + self.selectedExaminationId() +
                    '&FromSum=' + self.fromSum() +
                    '&ToSum=' + self.toSum() +
                    '&VisitDescription=' + self.visitDescription() +
                    '&MainComplaint=' + self.mainComplaint() +
                    '&printFontSize=' + self.printFontSize() +
                    '&nameVisitCb=' + self.nameVisitCb() +
                    '&datePerformedCb=' + self.datePerformedCb() +
                    '&clientNameCb=' + self.clientNameCb() +
                    '&patientNameCb=' + self.patientNameCb() +
                    '&serialCb=' + self.serialCb() +
                    '&addressCb=' + self.addressCb() +
                    '&homePhoneCb=' + self.homePhoneCb() +
                    '&cellPhoneCb=' + self.cellPhoneCb() +
                    '&animalKindCb=' + self.animalKindCb() +
                    '&animalRaceCb=' + self.animalRaceCb() +
                    '&animalGenderCb=' + self.animalGenderCb() +
                    '&animalColorCb=' + self.animalColorCb() +
                    '&toDateanimalAgeCb=' + self.animalAgeCb() +
                    '&sterilizationCb=' + self.sterilizationCb() +
                    '&electronicNumCb=' + self.electronicNumCb() +
                    '&birthDateCb=' + self.birthDateCb() +
                    '&licenseNumCb=' + self.licenseNumCb() +
                    '&chargeFeeCb=' + self.chargeFeeCb() +
                    '&performingDocCb=' + self.performingDocCb() +
                    '&numOfUnitsCb=' + self.numOfUnitsCb() +
                   // '&plateNumCb=' + self.plateNumCb() +
                    '&visitDescCb=' + self.visitDescCb() +
                    '&treatingDoctorCb=' + self.treatingDoctorCb() +
                    '&receiptCb=' + self.receiptCb() +
                    '&MedicationId=' + self.selectedMedicationId();
            return url;
        });

        self.printUrl = ko.computed(function () {
            return '/TreatmentReports/Print' + self.filterUrl();
        });

        self.stickersUrl = ko.computed(function () {
            return '/TreatmentReports/Stickers' + self.filterUrl();
        });

        self.lettersUrl = ko.computed(function () {
            return '/TreatmentReports/Letters' + self.filterUrl();
        });

        self.excelUrl = ko.computed(function () {
            return '/TreatmentReports/Excel' + self.filterUrl();
        });

        self.smsUrl = ko.computed(function () {
            return '/TreatmentReports/Sms' + self.filterUrl();
        });

        self.emailUrl = ko.computed(function () {
            return '/TreatmentReports/Email' + self.filterUrl();
        });

        //filter functions

        self.getFilters = function () {
            $.getJSON('/TreatmentReports/GetFiltersList', function (data) {
                self.filters([]);
                ko.mapping.fromJS(data, self.filters);
            });
        };

        self.getClients = function () {
            $.getJSON('/Clinics/GetClinicClients', function (data) {
                ko.mapping.fromJS(data, self.clients);
            });
        };

        self.getAnimalKinds = function () {
            $.getJSON('/MetaData/GetAnimalKinds', function (data) {
                ko.mapping.fromJS(data, self.animalKinds);
            });
        };

        self.getDoctors = function () {
            $.getJSON('/Clinics/GetClinicDoctors', function (data) {
                ko.mapping.fromJS(data, self.doctors);
            });
        };

        self.getCategories = function () {
            $.getJSON('/FinanceMetaData/GetPriceListCategories', function (data) {
                ko.mapping.fromJS(data, self.categories);
            });
        };

        self.getTreatments = function () {
            $.getJSON('/MetaData/GetTreatments/', function (data) {
                var mapped = $.map(data, function (item) {
                    return new selectItem(item.Id, item.Name);
                });
                self.treatments(mapped);
            });
        };

        //self.getExaminations = function () {
        //    $.getJSON('/MetaData/GetExaminations/', function (data) {
        //        var mapped = $.map(data, function (item) {
        //            return new selectItem(item.Id, item.Name);
        //        });
        //        self.examinations(mapped);
        //    });
        //};

        self.getDiagnoses = function () {
            $.getJSON('/MetaData/GetDiagnoses/', function (data) {
                var mapped = $.map(data, function (item) {
                    return new selectItem(item.Id, item.Name);
                });
                self.diagnoses(mapped);
            });
        };
        self.getMedications = function () {
            $.getJSON('/MetaData/GetMedications/', function (data) {
                var mapped = $.map(data, function (item) {
                    return new selectItem(item.Id, item.Name);
                });
                self.medications(mapped);
            });
        };

        self.getPersonalLetters = function () {
            $.getJSON('/LetterTemplates/GetClinicsPersonalLetters/', function (data) {
                if (data != null) {
                    var mapped = $.map(data, function (item) {
                        return new selectLetterTemplateItem(item.Id, item.TemplateName, item.Content);
                    });
                    self.personalLetters(mapped);
                }
            });
        };

        self.loadSelectedFilter = function () {

            if (typeof self.selectedFilterId() != "undefined" && self.selectedFilterId() > 0) {

                ShowLoder();

                $.getJSON('/TreatmentReports/GetFilterData/' + self.selectedFilterId(), function (data) {

                    self.selectedClientId(data.ClientId);
                    self.selectedAnimalKindId(data.AnimalKindId);
                    self.filterFromDate(data.FromDate);
                    self.filterToDate(data.ToDate);
                    self.filterAnimalFromYear(data.FromYear);
                    self.filterAnimalFromMonth(data.FromMonth);
                    self.filterAnimalToYear(data.ToYear);
                    self.filterAnimalToMonth(data.ToMonth);
                    if (data.DoctorId == null) {
                        self.selectedDoctorId([]);
                    } else {
                        self.selectedDoctorId(data.DoctorId.split(','));
                    }
                    if (data.CategoryId == null) {
                        self.selectedCategoryId([]);
                    } else {
                        self.selectedCategoryId(data.CategoryId.split(','));
                    }
                    if (data.TreatmentId == null) {
                        self.selectedTreatmentId([]);
                    } else {
                        self.selectedTreatmentId(data.TreatmentId.split(','));
                    }
                    self.selectedDiagnosisId(data.DiagnosisId);
                    self.selectedMedicationId(data.MedicationId);
                   // self.selectedExaminationId(data.ExaminationId);
                    self.fromSum(data.FromSum);
                    self.toSum(data.ToSum);
                    self.visitDescription(data.VisitDescription);
                    self.mainComplaint(data.MainComplaint);

                }).done(function () { hideLoder(); });
            }
        };

        self.saveFilter = function () {

            ShowLoder();
            self.hideFilterModal();
            $.post('/TreatmentReports/SaveFilter/', {

                Id: self.filterSaveToId(),
                Name: self.filterRename(),
                ClientId: self.selectedClientId(),
                AnimalKindId: self.selectedAnimalKindId(),
                FromDate: self.filterFromDate(),
                ToDate: self.filterToDate(),
                FromYear: self.filterAnimalFromYear(),
                FromMonth: self.filterAnimalFromMonth(),
                ToYear: self.filterAnimalToYear(),
                ToMonth: self.filterAnimalToMonth(),
                DoctorId: self.selectedDoctorId() != null ? self.selectedDoctorId().toString() : null,
                CategoryId: self.selectedCategoryId() != null ? self.selectedCategoryId().toString() : null,
                TreatmentId: self.selectedTreatmentId() != null ? self.selectedTreatmentId().toString() : null,
                DiagnosisId: self.selectedDiagnosisId(),
                MedicationId: self.selectedMedicationId(),
               // ExaminationId: self.selectedExaminationId() != null ? self.selectedExaminationId().toString() : null,
                FromSum: self.fromSum(),
                ToSum: self.toSum(),
                VisitDescription: self.visitDescription(),
                MainComplaint: self.mainComplaint()
            })
                .done(function (data) {
                    if (data == true || data == 'true') {
                        self.getFilters();
                    }
                    hideLoder();
                    getCookieMessages();
                });

        };

        self.showFilterModal = function () {
            self.filterRename(null);
            $('#filterModal').modal('show');

        };

        self.hideFilterModal = function () {
            $('#filterModal').modal('hide');
        };


        //general functions
        self.init = function () {
            ShowLoder();

            self.getFilters();
          //  self.getClients();
            self.getAnimalKinds();
            self.getDoctors();
            self.getCategories();
            self.getTreatments();
           // self.getExaminations();
            self.getDiagnoses();
            self.getMedications();
            self.getPersonalLetters();

            hideLoder();;
        };

        self.getReportData = function () {

            ShowLoder();

            $.getJSON('/TreatmentReports/GetReportData' + self.filterUrl(), function (data) {
                self.visits([]);
                var mapped = $.map(data, function (item) {
                    return new visitList(item);
                });
                self.visits(mapped);
            }).done(function () {
                hideLoder();
            });
        };

        //sending functions
        self.emailClients = function () {

            ShowLoder();

            $.post('/TreatmentReports/Email', {
                ClientId: self.selectedClientId(),
                AnimalKindId: self.selectedAnimalKindId(),
                FromDate: self.filterFromDate(),
                ToDate: self.filterToDate(),
                FromYear: self.filterAnimalFromYear(),
                FromMonth: self.filterAnimalFromMonth(),
                ToYear: self.filterAnimalToYear(),
                ToMonth: self.filterAnimalToMonth(),
                DoctorId: self.selectedDoctorId().length > 0 ? self.selectedDoctorId().toString() : null,
                CategoryId: self.selectedCategoryId().length > 0 ? self.selectedCategoryId().toString() : null,
                TreatmentId: self.selectedTreatmentId().length > 0 ? self.selectedTreatmentId().toString() : null,
                DiagnosisId: self.selectedDiagnosisId(),
                MedicationId: self.selectedMedicationId(),
              //  ExaminationId: self.selectedExaminationId().length > 0 ? self.selectedExaminationId().toString() : null,
                FromSum: self.fromSum(),
                ToSum: self.toSum(),
                VisitDescription: self.visitDescription(),
                MainComplaint: self.mainComplaint()
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });

        };

        self.smsClients = function () {

            ShowLoder();

            $.post('/TreatmentReports/Sms', {
                ClientId: self.selectedClientId(),
                AnimalKindId: self.selectedAnimalKindId(),
                FromDate: self.filterFromDate(),
                ToDate: self.filterToDate(),
                FromYear: self.filterAnimalFromYear(),
                FromMonth: self.filterAnimalFromMonth(),
                ToYear: self.filterAnimalToYear(),
                ToMonth: self.filterAnimalToMonth(),
                DoctorId: self.selectedDoctorId(),
                DoctorId: self.selectedDoctorId().length > 0 ? self.selectedDoctorId().toString() : null,
                CategoryId: self.selectedCategoryId().length > 0 ? self.selectedCategoryId().toString() : null,
                TreatmentId: self.selectedTreatmentId().length > 0 ? self.selectedTreatmentId().toString() : null,
                DiagnosisId: self.selectedDiagnosisId(),
                MedicationId: self.selectedMedicationId(),
               // ExaminationId: self.selectedExaminationId().length > 0 ? self.selectedExaminationId().toString() : null,
                FromSum: self.fromSum(),
                ToSum: self.toSum(),
                VisitDescription: self.visitDescription(),
                MainComplaint: self.mainComplaint()
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        };

        self.showPersonalLetterModal = function (element) {
            self.personalTemplateError(false);
            self.selectedLetterId("");
            $('#personalLetterModal').modal('show');
        };
        self.hidePersonalLetterModal = function () {
            self.personalTemplateError(false);
            self.selectedLetterId("");
            $('#personalLetterModal').modal('hide');
        };

        self.sendPersonalLetter = function () {
            if (self.selectedLetterId() > 0) {
                $('#personalLetterModal').modal('hide');
                window.open(self.lettersUrl() + "&LetterId=" + self.selectedLetterId() + "&print=false", '_blank');
                done = true;
            }
            else {
                self.personalTemplateError(true);
            }
        };

        //execution
        self.init();
    }

    ko.applyBindings(new viewModel());

});