﻿var isOnAdding = false;

function startEditing(id) {
    startUIEdit([{ id: "lbl_starttime_" + id, hide: true }, { id: "lbl_endtime_" + id, hide: true }, { id: "txt_starttime_" + id, hide: false }, { id: "txt_endtime_" + id, hide: false }, { id: "edit_icon_" + id, hide: true }, { id: "confirm_icon_" + id, hide: false }]);
}

function cancelEditing(id) {
    startUIEdit([{ id: "lbl_starttime_" + id, hide: false }, { id: "lbl_endtime_" + id, hide: false }, { id: "txt_starttime_" + id, hide: true }, { id: "txt_endtime_" + id, hide: true }, { id: "edit_icon_" + id, hide: false }, { id: "confirm_icon_" + id, hide: true }]);
}

function finisheEditing(id) {
    startUIEdit([{ id: "lbl_starttime_" + id, hide: false }, { id: "lbl_endtime_" + id, hide: false }, { id: "txt_starttime_" + id, hide: true }, { id: "txt_endtime_" + id, hide: true }, { id: "edit_icon_" + id, hide: false }, { id: "confirm_icon_" + id, hide: true }]);
    saveLogReportRow(id);
}

function saveLogReportRow(id) {
    var starttimelbl = document.getElementById("lbl_starttime_" + id);
    var endtimelbl = document.getElementById("lbl_endtime_" + id);
    var starttimetxt = document.getElementById("starttime" + id);
    var endtimetxt = document.getElementById("endtime" + id);
    var starttime = starttimetxt.value;
    var endtime = endtimetxt.value;
    //if ((new Date('01/01/1990 ' + endtime)) - (new Date('01/01/1990 ' + starttime)) < 0) {
    //    startUIEdit(id, false);
    //    return false;
    //}
    ShowLoder();
    var userid = document.getElementById("rowUserID");
    var startdate = document.getElementById("lbl_startdate_" + id);
    $.ajax({
        type: "POST",
        url: '/AttendanceLog/SaveLogReport',
        data: {
            'id': id,
            'userID': userid.innerText,
            'startTime': starttime,
            'endTime': endtime,
            'startDate': startdate.innerText
        },
        success: function (data) {
            if (data === "1") {
                starttimelbl.innerText = starttime;
                endtimelbl.innerText = endtime;
                //computeNewTimeDif(id, starttime, endtime);
                window.location.href = document.URL;
            } else {
                starttimetxt.value = starttimelbl.innerText;
                endtimetxt.value = endtimelbl.innerText;
            }
        }
    }).done(function () {
        hideLoder();
        getCookieMessages();
    });
}

//function computeNewTimeDif(id, starttime, endtime) {
//    var computedEndTime = document.getElementById("comp_endtime_" + id);
//    var msecPerMinute = 1000 * 60;
//    var msecPerHour = msecPerMinute * 60;
//    var msecPerDay = msecPerHour * 24;
//    var date = new Date('1/1/1990 ' + starttime);
//    var dateMsec = new Date('1/1/1990 ' + endtime);
//    var interval = dateMsec - date.getTime();
//    var days = Math.floor(interval / msecPerDay);
//    interval = interval - (days * msecPerDay);
//    var hours = Math.floor(interval / msecPerHour);
//    interval = interval - (hours * msecPerHour);
//    var minutes = Math.floor(interval / msecPerMinute);
//    computedEndTime.innerText = hours + ':' + ("0" + minutes).slice(-2) + ':00';
//}

function addLogReportCreationRow() {
    if (isOnAdding === true) {
        return;
    }

    isOnAdding = true;
    var table = document.getElementById('tblLogReports');
    var row = table.insertRow(1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    cell1.innerHTML = '<input id="newCreateDP" type="text" class="datepicker_recurring_start" style="width: auto;" autocomplete="off" value="' + getTodayFormat() + '" readonly/>';
    cell2.innerHTML = '<label>' + document.getElementById('users')[document.getElementById('users').selectedIndex].innerHTML + '</label>';
    cell3.innerHTML = '<input type="text" id="starttime-1" style="width: auto;" oninput="autoCompleteTimeSection(this);"  onblur="checkvalidTime(this, event)"/>';
    cell4.innerHTML = '<input type="text" id="endtime-1" style="width: auto;" oninput="autoCompleteTimeSection(this);" onblur="checkvalidTime(this, event)"  />';
    cell5.innerHTML = '<label></label>';
    cell6.innerHTML = '<label><a onclick="doneAdding();" data-toggle="tooltip" data-placement="top" title="אישור הוספה" data-original-title="רישור הוספה"><i class="icon-ok"></i></a><a onclick="cancelAdding();" data-toggle="tooltip" data-placement="top" title="ביטול הוספה" data-original-title="ביטול הוספה"><i class="icon-remove"></i></a></label>';
}

//adds : automaticaly
function autoCompleteTimeSection(val) {
    var v = val.value.trim();
    if (v.length === 2 && v.indexOf(':') === -1) {
        val.value = v + ":";
    }
}

//can be modified for better validation, for now 
function checkvalidTime(val, e) {
    var v = val.value.trim();
    var splitted = v.split(':');
    val.style.background = 'white';
    if (v.length === 0) {
        return false;
    }

    if (v.length !== 5 || splitted.length !== 2 || splitted[0] > 23 || splitted[1] > 59) {
        val.style.background = errorBackground;
        e.preventDefault();
        return false;
    }
}

function doneAdding() {
    var newAdddate = document.getElementById("newCreateDP");
    var newStartTime = document.getElementById("starttime-1");
    var newEndTime = document.getElementById("endtime-1");
    var qr = getQueryStrings();
    var userid = qr['userId'];
    $.ajax({
        type: "POST",
        url: '/AttendanceLog/SaveLogReport',
        data: {
            'id': -1,
            'userID': userid,
            'startTime': newStartTime.value,
            'endTime': newEndTime.value,
            'startDate': newAdddate.value
        },
        success: function (data) {
            if (data === "1") {
                window.location.href = document.URL;
            } else {
                cancelAdding();
            }
        }
    }).done(function () {
        isOnAdding = false;
        hideLoder();
        getCookieMessages();
    });
}

function cancelAdding() {
    var table = document.getElementById('tblLogReports');
    table.deleteRow(1);
    isOnAdding = false;
}

function deleteLogReportRow(id) {
    var qr = getQueryStrings();
    var userid = qr['userId'];
    $.ajax({
        type: "POST",
        url: '/AttendanceLog/DeleteLogReport',
        data: {
            'id': id,
            'userID': userid
        },
        success: function (data) {
            if (data === "1") {
                window.location.href = document.URL;
            }
        }
    }).done(function () {
        hideLoder();
        getCookieMessages();
    });
}
