//$("#ContactPhoneB").keydown(function () {
//    var strLen = $(this).val().length;
//    if (strLen < 10) {
//        return true;
//    }
//    else {
//        var strLen = $(this).val().length;
//        if (strLen < 10) {
//            return true;
//        }
//        return false;
//    }
//});

RapidVet.TimeDisplay = function () {
    var weekday = new Array(7);
    init = function () {
        weekday[0] = "ראשון";
        weekday[1] = "שני";
        weekday[2] = "שלישי";
        weekday[3] = "רביעי";
        weekday[4] = "חמישי";
        weekday[5] = "שישי";
        weekday[6] = "שבת";
        startTime();
    };
    startTime = function () {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();

        var month = checkTime(today.getUTCMonth() + 1);
        var day = checkTime(today.getUTCDate());
        var year = today.getUTCFullYear();
        // add a zero in front of numbers<10
        m = checkTime(m);


        $("#time").html(h + ":" + m);

        $("#weekday").html(weekday[today.getDay()]);

        $("#date").html(day + "." + month + "." + year);

        t = setTimeout(function () {
            startTime()
        }, 500);
    };

    checkTime = function (i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    };
    return {
        init: init
    };
}();