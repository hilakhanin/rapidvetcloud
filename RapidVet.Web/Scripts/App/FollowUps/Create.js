﻿var viewModel;
var vm;
function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

function clearClientSearch(element) {
    vm.clientId(undefined);
    vm.ClientNameWithPatients("");
    vm.patients([]);
    vm.patientId("");
    element.value = "";
    $(".no-results").hide();
};

$(function () {
    $(".no-results").hide();
    $(".client-search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Clients/Search",
                type: "POST",
                data: {
                    query: request.term
                },
                success: function (data) {
                    response(data);

                    if (data.length == 0) {
                        $(".no-results").show();
                        setTimeout(function () { $(".no-results").hide(); }, 5000);
                    }
                    else {
                        $(".no-results").hide();
                    }
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            event.preventDefault();
            vm.ClientNameWithPatients(ui.item.label);
            vm.clientId(ui.item.value.toString());
            this.value = ui.item.label;

            //window.location = "/Clients/Patients/" + ui.item.value;
        },
        focus: function (event, ui) {
            event.preventDefault();
            this.value = ui.item.label;
        }
        ,
        change: function (event, ui) {
            event.preventDefault();
            if (isEmptyOrWhiteSpaces(this.value) ||
                typeof vm.clientId() == "undefined" || vm.ClientNameWithPatients() != this.value) {
                clearClientSearch(this);
            }

        },
        open: function () {
            $(".ui-autocomplete").css("max-height", 120);
            $(".ui-autocomplete").css("max-width", 225);
            $(".ui-autocomplete").css("overflow-y", "auto");
            $(".ui-autocomplete").css("overflow-x", "hidden");

        }
    });
});

ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datepickerOptions || { dateFormat: 'dd/mm/yy' };
        $(element).datepicker(options);

        //handle the field changing
        ko.utils.registerEventHandler(element, "change", function () {
            var observable = valueAccessor();
            observable($(element).datepicker("getDate"));
            $(element).datepicker(valueAccessor());
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).datepicker("destroy");
        });

    },
    //update the control when the view model changes
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var value = ko.utils.unwrapObservable(valueAccessor()),
            current = $(element).datepicker("getDate");

        if (value - current !== 0) {
            $(element).datepicker("setDate", value);

        }
    }
};

$(function () {

    function viewModel() {

        var self = this;
        vm = self;
        self.clients = ko.mapping.fromJS([]);
        self.clientId = ko.observable().extend({ required: { params: true, message: "שדה חובה" } });
        self.ClientNameWithPatients = ko.observable("");

        self.patients = ko.mapping.fromJS([]);
        self.patientId = ko.observable().extend({ required: { params: true, message: "שדה חובה" } });

        self.doctors = ko.mapping.fromJS([]);
        self.doctorId = ko.observable().extend({ required: { params: true, message: "שדה חובה" } });
        self.date = ko.observable($("#DateTime").val()).extend({ required: { params: true, message: "שדה חובה" } });
        self.originalDate = ko.observable($("#DateTime").val());
        self.comments = ko.observable();

        //functions
        self.getClients = function () {
            $.getJSON('/Clinics/GetClinicClients', function (data) {
                ko.mapping.fromJS(data, self.clients);
            });
        };

        self.getPatients = function () {
            ShowLoder();
            $.getJSON('/Clients/GetClientPatients?clientId=' + self.clientId(), function (data) {
                ko.mapping.fromJS(data, self.patients);
            }).done(function () {
                hideLoder();
            });
        };

        self.patientOptions = ko.computed(function () {
            if (self.clientId() > 0) {
                self.patients([]);
                self.getPatients();
            }

        });

        self.getDoctors = function () {
            $.getJSON('/Clinics/GetClinicDoctors', function (data) {
                ko.mapping.fromJS(data, self.doctors);

                if ($('#defaultDr').val() > 0) {
                    self.doctorId($('#defaultDr').val());
                }
            });
        };

        self.init = function () {

            ShowLoder();
        //    self.getClients();
            self.getDoctors();
            hideLoder();

            
        };

        self.isValid = function () {
            var errors = ko.validation.group(self, { deep: false });
            errors.showAllMessages(true);
            return errors().length == 0;
        };

        self.submitForm = function () {
            if (self.isValid()) {
                ShowLoder();

                var date;

                if (self.date() != self.originalDate()) {
                    date = self.getDate(self.date());
                }
                else {
                    date = self.date();
                }

                $.ajax({
                    type: "POST",
                    url: '/FollowUps/Create',
                    data: {
                        'Id': 0,
                        'PatientId': self.patientId(),
                        'DateTime': date,
                        'Comment': self.comments(),
                        'UserId': self.doctorId()
                    },
                    success: function (data) {

                        hideLoder();
                        getCookieMessages();
                        setTimeout(function () { window.location.href = '/FollowUps/Index'; }, 1000);                        
                        
                    }
                });
            }
        };

        self.getDate = function (data) {
            var date = new Date(data);
            return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
        };

        //execution
        self.init();

    }

    viewModel = new viewModel();
    ko.applyBindings(viewModel);
});