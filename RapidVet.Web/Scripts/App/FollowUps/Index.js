﻿
ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datepickerOptions || { dateFormat: 'dd/mm/yy' };
        $(element).datepicker(options);

        //handle the field changing
        ko.utils.registerEventHandler(element, "change", function () {
            var observable = valueAccessor();
            observable($(element).datepicker("getDate"));
            $(element).datepicker(valueAccessor());
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).datepicker("destroy");
        });

    },
    //update the control when the view model changes
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var value = ko.utils.unwrapObservable(valueAccessor()),
            current = $(element).datepicker("getDate");

        if (value - current !== 0) {
            $(element).datepicker("setDate", value);

        }
    }
};

$(function () {

    function selectLetterTemplateItem(id, name, content) {
        var self = this;
        self.Id = id;
        self.Name = name;
        self.Content = content;
    }


    function FollowUp(data) {
        var self = this;
        self.FollowUpId = ko.observable(data.FollowUpId);
        self.ClientId = ko.observable(data.ClientId);
        self.ClientName = ko.observable(data.ClientName);
        self.Address = ko.observable(data.Address);
        self.CellPhone = ko.observable(data.CellPhone + " " + data.CellPhoneComment);
        self.HomePhone = ko.observable(data.HomePhone + " " + data.HomePhoneComment);
        self.Email = ko.observable(data.Email);
        self.PatientName = ko.observable(data.PatientName);
        self.PatientKind = ko.observable(data.PatientKind);
        self.PatientGender = ko.observable(data.PatientGender);
        self.Created = ko.observable(data.Created);
        self.Comment = ko.observable(data.Comment);
        self.NextAppointment = ko.observable(data.NextAppointment);
        self.Doctors = ko.observable(data.Doctors);
        self.DoctorId = ko.observable(data.DoctorId);
        self.Selected = ko.observable(false);
        self.DateTime = ko.observable(data.DateTime);
        
        self.singleLetter = ko.computed(function () {
            return '/FollowUps/Letter/' + self.FollowUpId();
        });

        self.clientUrl = ko.computed(function () {
            return '/Clients/Patients/' + self.ClientId();
        });
    }


    function viewModel() {

        var self = this;

        //general
        self.from = ko.observable($('#from').val());
        self.to = ko.observable($('#to').val());
        self.originalFrom = ko.observable($('#from').val());
        self.originalTo = ko.observable($('#to').val());
        self.IncludeInactivePatients = ko.observable(false);
        self.clients = ko.observableArray([]);

        //Personal Letter
        self.personalLetters = ko.mapping.fromJS([]);
        self.selectedLetterId = ko.observable("");
        self.selectedLetterContent = ko.observable("");
        self.personalTemplateError = ko.observable(false);
        self.followUpId = ko.observable("");
        self.selectedFollowUpElement = ko.observable();

        self.selectedLetterId.subscribe(function () {
            if (self.selectedLetterId() == "") {
                self.selectedLetterContent("");
            }
            else {
                for (var i = 0; i < self.personalLetters().length; i++) {
                    if (self.personalLetters()[i].Id == self.selectedLetterId()) {
                        self.selectedLetterContent(self.personalLetters()[i].Content);
                    }
                }
            }
        });

        self.getPersonalLetters = function () {
            $.getJSON('/LetterTemplates/GetClinicsPersonalLetters/', function (data) {
                if (data != null) {
                    var mapped = $.map(data, function (item) {
                        return new selectLetterTemplateItem(item.Id, item.TemplateName, item.Content);
                    });
                    self.personalLetters(mapped);
                }
            });
        };

        self.getData = function () {
            ShowLoder();
            var from;
            var to;

            if (self.from() != self.originalFrom()) {
                from = self.getDate(self.from());
            }
            else {
                from = self.from();
            }

            if (self.to() != self.originalTo()) {
                to = self.getDate(self.to());
            }
            else {
                to = self.to();
            }

            $.getJSON('/FollowUps/GetFollowUps?from=' + from
                + '&to=' + to
                + '&docID=' + $('#users').val()
                + '&includeInactivePatients=' + self.IncludeInactivePatients(), function (data) {
                    var mapped = $.map(data, function (item) {
                        return new FollowUp(item);
                    });
                    self.clients(mapped);
                    hideLoder();
                });
        };

        self.selectedClients = ko.computed(function () {

            var selected = ko.utils.arrayFilter(self.clients(), function (c) {
                return c.Selected() == true;
            });

            return ko.toJSON(selected);
        });

        self.anyClientsSelected = ko.computed(function () {
            var result = false;

            if (self.selectedClients().length > 2) {
                result = true;
            }
            return result;
        });

        self.recipients = ko.computed(function () {
            var recipients;
            if (self.anyClientsSelected()) {
                recipients = self.selectedClients();
            } else {
                recipients = ko.toJSON(self.clients());
            }
            return recipients;
        });


        //followUps
        self.doctors = ko.mapping.fromJS([]);
        self.getDoctors = function () {
            $.getJSON('/Clinics/GetClinicDoctors', function (data) {
                ko.mapping.fromJS(data, self.doctors);
            });
        };


        self.followUpToEdit = ko.observable();
        self.showEditModal = function (element) {
            self.followUpToEdit();
            self.followUpToEdit(element);
            $('#editModal').modal('show');
            self.initDatePicker('editDate');
        };
        self.saveFollowUp = function () {
            ShowLoder();
            $('#editModal').modal('hide');
            $.post('/FollowUps/Update?json=' + ko.toJSON(self.followUpToEdit()))
                .done(function (data) {
                    if (data) {
                        self.getData();
                    }
                    hideLoder();
                    getCookieMessages();
                });
        };

        self.followUpToDelete = ko.observable();
        self.showDeleteModal = function (element) {
            self.followUpToDelete();
            self.followUpToDelete(element);
            $('#deleteModal').modal('show');
        };

        self.deleteFollowUp = function () {
            ShowLoder();
            $('#deleteModal').modal('hide');
            $.post('/FollowUps/Delete/' + self.followUpToDelete().FollowUpId())
                .done(function (data) {
                    if (data) {
                        self.getData();
                    }
                    hideLoder();
                    getCookieMessages();
                });
        };


        //communication
        self.smsAll = function () {
            ShowLoder();
            $.post('/FollowUps/SendSMS', { clients: self.recipients() }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        };

        self.smsSingle = function (element) {
            ShowLoder();
            $.post('/FollowUps/SendSMS', { clients: ko.toJSON(element), single: true }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        };

        self.emailAll = function () {
            ShowLoder();
            $.post('/FollowUps/SendEmail', { clients: self.recipients() }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        };

        self.emailSingle = function (element) {
            ShowLoder();
            $.post('/FollowUps/SendEmail', { clients: ko.toJSON(element), single: true }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        };

        //renew all modal
        self.minDaysFromFollowUpWithoutAppointment = ko.observable(0);
        self.remindersAddMonths = ko.observable(0);

        self.showRenewAllModal = function () {
            self.minDaysFromFollowUpWithoutAppointment(0);
            self.remindersAddMonths(0);
            $('#renewAllModal').modal('show');
        };

        self.renewAll = function () {
            $('#renewAllModal').modal('hide');
            ShowLoder();
            $.post('/FollowUps/RenewAll',
                {
                    MinDaysFromFollowUpWithoutAppointment: self.minDaysFromFollowUpWithoutAppointment(),
                    RemindersAddMonths: self.remindersAddMonths()
                }).done(function (data) {
                    self.clients([]);
                    self.getData();
                    hideLoder();
                    getCookieMessages();
                });
        };

        //renew single
        self.renewSingleElement = ko.observable();

        self.showRenewSingle = function (element) {
            self.renewSingleElement();
            self.renewSingleElement(element);
            $('#renewSingleModal').modal('show');
            self.initDatePicker('renewSingleDate');
        };

        self.renewSingle = function () {
            if (typeof self.renewSingleElement().NextAppointment() != "undefined") {
                ShowLoder();
                $('#renewSingleModal').modal('hide');
                $.post('/FollowUps/RenewSingle/'
                    + self.renewSingleElement().FollowUpId()
                    + '?date=' + self.renewSingleElement().NextAppointment())
                    .done(function (data) {

                        if (data) {
                            self.getData();
                        }
                        hideLoder();
                        getCookieMessages();
                    });
            }
        };

        self.init = function () {
            ShowLoder();
            self.getData();
            self.getDoctors();
            self.getPersonalLetters();
            hideLoder();
        };

        self.initDatePicker = function (data) {
            $('#' + data).datepicker({
                changeMonth: true,
                changeYear: true
            });
        };

        self.getDate = function (data) {
            var date = new Date(data);
            return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
        };

        self.showPersonalLetterModal = function (element) {
            self.personalTemplateError(false);
            self.selectedLetterId("");
            self.followUpId(element.FollowUpId());
            self.selectedFollowUpElement(element);
            $('#personalLetterModal').modal('show');
        };
        self.hidePersonalLetterModal = function () {
            self.personalTemplateError(false);
            self.selectedLetterId("");
            self.followUpId("");
            self.selectedFollowUpElement(null);
            $('#personalLetterModal').modal('hide');
        };

        self.sendPersonalLetter = function () {
            if (self.selectedLetterId() > 0) {
                $('#personalLetterModal').modal('hide');
                window.open('/FollowUps/Letter/' + self.followUpId() + "?LetterId=" + self.selectedLetterId() + "&clients=" + ko.toJSON(self.selectedFollowUpElement()) + "&print=false", '_blank');
                done = true;
            }
            else {
                self.personalTemplateError(true);
            }
        };

        //execution
        self.init();
    }

    ko.applyBindings(new viewModel());

});