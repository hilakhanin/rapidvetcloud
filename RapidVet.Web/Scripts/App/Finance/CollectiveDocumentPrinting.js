﻿function GetShortDate() {
    var MyDate = new Date();
    var MyDateString;

    MyDate.setDate(MyDate.getDate());

    MyDateString = ('0' + MyDate.getDate()).slice(-2) + '/'
                 + ('0' + (MyDate.getMonth() + 1)).slice(-2) + '/'
                 + MyDate.getFullYear();

    return MyDateString;
}

function viewModel() {
    var self = this;
    self.DocTypeOptions = ko.mapping.fromJS([]);
    self.SelectedDocType = ko.observable();
    self.issuers = ko.observable([]);
    self.issuerId = ko.observable();

    /*-----------------------------------------------Print by serial number---------------------------------*/
    self.FromSerialNumberOptions = ko.mapping.fromJS([]);
    self.FromSerialNumber = ko.observable();

    self.ToSerialNumberOptions = ko.computed(function () {
        return ko.utils.arrayFilter(self.FromSerialNumberOptions(), function (item) {
            if (typeof self.FromSerialNumber() !== 'undefined') {
                return (parseInt(item.Text())) >= (parseInt(self.FromSerialNumber().Text()));
            }
            return true;
        });
    });
    self.ToSerialNumber = ko.observable();

    self.SelectedDocumentsBySerial = ko.computed(function () {        
        var from = self.FromSerialNumber();
        var to = self.ToSerialNumber();

        if(isNaN(parseInt(from)) || isNaN(parseInt(to)))
        {
            return "";
        }

        var result = "";

        for( i = from; i<= to; i++)
        {
            result = result + "," + i;

        }
        
        result = result.substring(1);

        return result;
    });

    self.PrintBySerial = function () {
        if (self.serialValidate()) {
            window.open('/FinanceDocumentCollectivePrinting/PrintBySerialRange?from=' + self.FromSerialNumber() + '&to=' + self.ToSerialNumber()
                + '&documentType=' + self.SelectedDocType() + '&issuerId=' + self.issuerId(), '_blank');
        }
    };
  
    /*---------------------------------------------Print by date --------------------------------------------------------*/

    self.FromDate = ko.observable();
    self.ToDate = ko.observable();

    self.PrintByDate = function() {
         if (self.dateValidate()) {
            var toDate = self.ToDate();

            if (typeof toDate === 'undefined' || toDate == null || toDate == "") {

                toDate = GetShortDate();
            }
            window.open('/FinanceDocumentCollectivePrinting/PrintByDate?from=' + self.FromDate() + '&to=' + toDate + '&documentType=' + self.SelectedDocType(), '_blank');

           
        }        
    };

    //self.SelectedDocType.subscribe(function () {
    //    if (typeof self.SelectedDocType() === 'undefined') {
    //        return;
    //    }
    //    var qs = {
    //        documentType: self.SelectedDocType()
    //    };
    //    $.get('/FinanceDocumentCollectivePrinting/GetFromSerialNumberOptions', $.param(qs), function (data) {
    //        // ko.mapping.fromJS(data, self.FromSerialNumberOptions);

    //        self.FromSerialNumber(data.Min);
    //        self.ToSerialNumber(data.Max);

    //    });
    //});


    $.get('/FinanceDocumentCollectivePrinting/GetDocumentTypeOptions', null, function (data) {
        ko.mapping.fromJS(data, self.DocTypeOptions);
        self.getIssuers();
    });

    self.getIssuers = function () {
        ShowLoder();

        $.get('/FinanceReports/GetAllIssuersDropDown', function (data) {
            var mappedIssuers = $.map(data, function (item) {
                return new SelectItem(item.Value, item.Text);
            });
            self.issuers(mappedIssuers);
        }).done(function () {
            hideLoder();
        });
    };

    function SelectItem(value, text) {
        var self = this;
        self.itemValue = value;
        self.itemText = text;
    }

    /*--validation--*/
    self.typeValidate = ko.computed(function() {
        return typeof self.SelectedDocType() !== 'undefined' && typeof self.issuerId() !== 'undefined' && self.issuerId() != null;
       
    });

    self.serialValidate = ko.computed(function () {
        return self.typeValidate() && (self.ToSerialNumber() - self.FromSerialNumber() >= 0);
    });

    self.dateValidate = ko.computed(function () {
        return self.typeValidate() &&
        typeof self.FromDate() !== 'undefined' && self.FromDate() != null && self.FromDate() != "" ;//&&
                //self.ToDate() !== 'undefined' && self.ToDate() != null && self.ToDate() != "";
        });

}

