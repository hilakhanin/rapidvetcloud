﻿

function viewModel() {

    var self = this;
    //filters

    self.issuers = ko.mapping.fromJS([]);
    self.issuerId = ko.observable();

    self.fromDate = ko.observable();
 
    self.toDate = ko.observable();

    self.showLink = ko.observable(false);

    self.showErrors = ko.observable(false);

    self.create = function () {
        if (self.toDate() != undefined && self.fromDate() != undefined && self.issuerId() != undefined) {
            self.showLink(true);
            self.showErrors(false);
        } else {
            self.showErrors(true);
            self.showLink(false);
        }
    };

    self.url = ko.computed(function () {
        self.showLink(false);
        return '/OpenFormat/GetData?issuerId=' + self.issuerId() + '&from=' + self.fromDate() + '&to=' + self.toDate();
    });
    
    self.kupaurl = ko.computed(function () {
        self.showLink(false);
        return '/HS/KUPAIN?issuerId=' + self.issuerId() + '&from=' + self.fromDate() + '&to=' + self.toDate();
    });
    
    self.invoiceurl = ko.computed(function () {
        self.showLink(false);
        return '/HS/MOVEINInvoice?issuerId=' + self.issuerId() + '&from=' + self.fromDate() + '&to=' + self.toDate();
    });
    
    self.recipturl = ko.computed(function () {
        self.showLink(false);
        return '/HS/MOVEINRecipt?issuerId=' + self.issuerId() + '&from=' + self.fromDate() + '&to=' + self.toDate();
    });
    
    self.allurl = ko.computed(function () {
        self.showLink(false);
        return '/HS/AllFiles?issuerId=' + self.issuerId() + '&from=' + self.fromDate() + '&to=' + self.toDate();
    });
    //functions
    //FinanceMetaData

    self.getIssuers = function () {
        $.getJSON('/FinanceMetaData/GetClinicIssuers', function (data) {
            ko.mapping.fromJS(data, self.issuers);
        });
    };

    self.filtersInit = function () {

        ShowLoder();

        self.getIssuers();

        hideLoder();

    };

    self.getFile = function() {
        var qs = {
            from: self.fromDate(),
            to: self.toDate(),
            issuerId: self.issuerId()
        };
        $.get('/OpenFormat/GetData', $.param(qs), function(res) {

        });
    };



    //execution

    self.filtersInit();
}




