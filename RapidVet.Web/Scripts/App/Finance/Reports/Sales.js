﻿$(function () {   

    function viewModel() {

        var self = this;

        self.sales = ko.mapping.fromJS([]);

        //default dates
        var year = moment().year();
        var month = moment().month() + 1;

        self.from = ko.observable(moment().month(moment().month()).set('date', 1).format("DD/MM/YYYY")).extend({ required: { params: true, message: "הכנס תאריך" } });
        self.to = ko.observable(moment().format('DD/MM/YYYY')).extend({ required: { params: true, message: "הכנס תאריך" } });

        self.categories = ko.mapping.fromJS([]);
        self.categoryId = ko.observable();

        self.items = ko.mapping.fromJS([]);
        self.ItemId = ko.observable();

        self.itemsOptions = ko.computed(function () {
            if (typeof self.categoryId() != "undefined") {
                self.getItems();
            }
        });

        self.issuers = ko.observable([]);
        self.issuerId = ko.observable().extend({ required: { params: true, message: "בחר מנפיק" } });

        self.groupBySeller = ko.observable(false);
        self.showAllItems = ko.observable(false);

        self.results = ko.observable(' ');

        self.search = function () {
            if (self.validate()) {
                ShowLoder();
                var url = '/FinanceReports/GetSalesReportData' +
                    '?categoryId=' + self.categoryId() +
                    '&priceListItemId=' + self.ItemId() +
                    '&issuerId=' + self.issuerId() +
                    '&from=' + self.from() +
                    '&to=' + self.to() +
                    '&groupBySeller=' + self.groupBySeller() +
                    '&showAllItems=' + self.showAllItems();

                self.results(' ');
                $.getJSON(url, function (data) {
                    self.results(data);
                }).done(function () {
                    hideLoder();
                });
            }
        };

        self.getCategories = function () {

            ShowLoder();

            $.getJSON('/FinanceMetaData/GetPriceListCategories', function (data) {
                ko.mapping.fromJS(data, self.categories);
            }).done(function () {
                hideLoder();
            });
        };
        
        self.getIssuers = function () {
            ShowLoder();

            $.get('/FinanceReports/GetAllIssuersDropDown', function (data) {
                var mappedIssuers = $.map(data, function (item) {
                    return new SelectItem(item.Value, item.Text);
                });
                self.issuers(mappedIssuers);
            }).done(function () {
                hideLoder();
            });
        };

       

        self.getItems = function () {

            if (typeof self.categoryId() != "undefined") {
                ShowLoder();
                self.items([]);
                $.getJSON('/FinanceMetaData/GetPriceListItemsForCategory/' + self.categoryId(), function (data) {
                    ko.mapping.fromJS(data, self.items);
                }).done(function () {
                    hideLoder();
                });
            }
        };

        self.validate = function () {
            var errors = ko.validation.group(self, { deep: false });
            errors.showAllMessages(true);
            return errors().length == 0;
        };

        // mapper for list item

        function SelectItem(value, text) {
            var self = this;
            self.itemValue = value;
            self.itemText = text;
        }

        //executions
        self.getCategories();
        self.getIssuers();


    }

    ko.validation.init({
            decorateElement: true,
            decorateInputElement: true

        });

    ko.applyBindings(new viewModel());
   
    

    
});