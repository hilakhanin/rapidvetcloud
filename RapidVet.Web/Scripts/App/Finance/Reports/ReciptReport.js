﻿$(function () {


    function viewModel() {

        var self = this;
        self.results = ko.mapping.fromJS([]);
        self.showSummary = ko.observable(false);
        self.fromDate = ko.observable();
        self.from = ko.computed(function () {
            return replaceDateSeperator(self.fromDate());
        });
        self.toDate = ko.observable();
        self.to = ko.computed(function () {
            return replaceDateSeperator(self.toDate());
        });

        self.issuers = ko.mapping.fromJS([]);
        self.selectedIssuer = ko.observable();



        //total variables
        self.totalWithoutVat = ko.observable();
        self.totalVat = ko.observable();
        self.totalIncludingVat = ko.observable();


        self.total = ko.computed(function () {
            var totalInvoiceRecipts = 0;
            var totalInvoices = 0;
            var totalRefunds = 0;
            var totalWithoutVat = 0;
            var totalVat = 0;
            var totalIncludingVat = 0;

            for (var i = 0; i < self.results().length; i++) {
                var beforeVat = parseFloat((self.results()[i].TotalAmount() / self.results()[i].VAT()).toFixed(2));
                totalWithoutVat += beforeVat;
                totalVat += parseFloat((self.results()[i].TotalAmount() - beforeVat).toFixed(2))
                totalIncludingVat += parseFloat(self.results()[i].TotalAmount());
            }
            totalWithoutVat = totalIncludingVat;
            self.totalWithoutVat(totalWithoutVat.toFixed(2));
            self.totalVat(totalVat.toFixed(2));
            self.totalIncludingVat(totalIncludingVat.toFixed(2));
        });

        //actions

        self.search = function () {

            //showModal();
            if (self.selectedIssuer() == null)
                return;

            ShowLoder();

            var url = '/FinanceReports/GetReciptReportData' +
                '?from=' + self.from() +
                '&to=' + self.to() +
                '&issuerId=' + self.selectedIssuer();

            $.getJSON(url, function (data) {
                ko.mapping.fromJS(data, self.results);
                self.showSummary(true);
                hideLoder();
            });
        };

        self.ExportToExcel = function () {
            window.location.href = '/FinanceReports/ReciptReportExportToExcel' + '?from=' + self.from() + '&to=' + self.to() + '&issuerId=' + self.selectedIssuer();
        };

        self.print = function () {
            var url = '/FinanceReports/PrintReciptReport' +
                '?from=' + self.from() +
                '&to=' + self.to() +
                '&issuerId=' + self.selectedIssuer();
            //location.href = url;
            window.open(url, '_blank');
        };

        self.init = function () {
            ShowLoder();
            $.get('/FinanceMetaData/GetClinicIssuers', null, function (data) {
                ko.mapping.fromJS(data, self.issuers);
                hideLoder();
            });


            self.fromDate(moment().month(moment().month() - 1).set('date', 1).format("DD/MM/YYYY"));
            self.toDate(moment().subtract(1, 'month').endOf('month').format("DD/MM/YYYY"));

        };



        self.init();
    }

    ko.applyBindings(new viewModel());

});