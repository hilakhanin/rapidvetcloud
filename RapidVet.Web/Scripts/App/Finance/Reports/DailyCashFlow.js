﻿var date = new Date();
function viewModel() {
    var self = this;
    self.fromDate = ko.observable();
    self.toDate = ko.observable();

    self.result = ko.mapping.fromJS([]);

    self.displayResult = ko.observable(false);

    self.years = ko.observableArray([1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030]);
    self.months = ko.observable([
        { Value: 1, Name: 'ינואר' },
        { Value: 2, Name: 'פברואר' },
        { Value: 3, Name: 'מרץ' },
        { Value: 4, Name: 'אפריל' },
        { Value: 5, Name: 'מאי' },
        { Value: 6, Name: 'יוני' },
        { Value: 7, Name: 'יולי' },
        { Value: 8, Name: 'אוגוסט' },
        { Value: 9, Name: 'ספטמבר' },
        { Value: 10, Name: 'אוקטובר' },
        { Value: 11, Name: 'נובמבר' },
        { Value: 12, Name: 'דצמבר' }
    ]);

    self.selectedYear = ko.observable(date.getFullYear());
    self.selectedMonth = ko.observable(date.getMonth() + 1);

    self.issuers = ko.mapping.fromJS([]);
    self.selectedIssuer = ko.observable();


    self.display = function () {
        //var qs = {
        //    from: self.fromDate(),
        //    to: self.toDate()
        //};

        //$.get('/FinanceReports/GetDailyCashFlow', $.param(qs), function (data) {
        //    ko.mapping.fromJS(data, self.result);
        //});
        ShowLoder();
        var qs = {
            month: self.selectedMonth(),
            year: self.selectedYear(),
            issuerId: self.selectedIssuer()
        };
        $.get('/FinanceReports/GetMonthlyCashFlow', $.param(qs), function (data) {
            ko.mapping.fromJS(data, self.result);
            self.displayResult(true);
            hideLoder();
        });
    };

    self.init = function () {
        ShowLoder();
        $.get('/FinanceMetaData/GetClinicIssuers', null, function (data) {
            ko.mapping.fromJS(data, self.issuers);
            hideLoder();
        });
    };

    self.init();

}