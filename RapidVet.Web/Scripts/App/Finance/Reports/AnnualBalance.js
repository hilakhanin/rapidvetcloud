﻿$(function () {

    function viewModel() {

        var self = this;
        self.months = ko.mapping.fromJS([]);

        //filters

        self.issuers = ko.mapping.fromJS([]);
        self.issuerId = ko.observable().extend({ required: { params: true, message: "בחר מנפיק" } });

        self.years = ko.observableArray([1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030]);
        self.selectedYear = ko.observable((new Date()).getFullYear()).extend({ required: { params: true, message: "בחר שנה" } });;


        self.filterUrl = ko.computed(function () {
            return '?issuerId=' + self.issuerId() + '&year=' + self.selectedYear();
        });

        self.printUrl = ko.computed(function () {
            return '/FinanceReports/PrintAnuualBalanceReport' + self.filterUrl();
        });

        self.fileUrl = ko.computed(function () {
            return '/FinanceReports/ExportAnnualBalance' + self.filterUrl();
        });

        //result
        self.dislpayResult = ko.observable(false);


        //functions

        self.getIssuers = function () {
            $.getJSON('/FinanceMetaData/GetClinicIssuers', function (data) {
                ko.mapping.fromJS(data, self.issuers);
            });
        };


        self.filtersInit = function () {

            ShowLoder();
            self.getIssuers();
            hideLoder();

        };

        self.search = function () {
            if (self.validate()) {
                ShowLoder();
                var url = '/FinanceReports/GetAnnualBalanceReport' + self.filterUrl();
                self.months([]);
                $.getJSON(url, function (data) {
                    ko.mapping.fromJS(data, self.months);
                }).done(function () {
                    self.dislpayResult(true);
                    hideLoder();
                });
            }
        };

        self.validate = function () {
            var errors = ko.validation.group(self, { deep: false });
            errors.showAllMessages(true);
            return errors().length == 0;
        };

        //execution

        self.filtersInit();
    }

    ko.applyBindings(new viewModel());

});