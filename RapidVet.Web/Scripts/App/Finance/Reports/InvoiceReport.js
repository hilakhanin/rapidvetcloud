﻿$(function () {


    function viewModel() {

        var self = this;
        self.results = ko.mapping.fromJS([]);
        self.showSummary = ko.observable(false);
        self.fromDate = ko.observable();
        self.from = ko.computed(function () {
            return replaceDateSeperator(self.fromDate());
        });
        self.toDate = ko.observable();
        self.to = ko.computed(function () {
            return replaceDateSeperator(self.toDate());
        });

        self.onlyConvertedInvoices = ko.observable(false);

        self.issuers = ko.mapping.fromJS([]);
        self.selectedIssuer = ko.observable();

        self.redirected = ko.observable($("#redirected").val());

        //total variables
        self.totalInvoiceRecipts = ko.observable();
        self.totalInvoices = ko.observable();
        self.totalRefunds = ko.observable();
        self.totalWithoutVat = ko.observable();
        self.totalVat = ko.observable();
        self.totalIncludingVat = ko.observable();
        self.invoiceString = $("#invoiceString").val();
        self.invoiceReciptString = $("#invoiceReciptString").val();
        self.refundString = $("#refundString").val();

        self.total = ko.computed(function () {
            var totalInvoiceRecipts = 0;
            var totalInvoices = 0;
            var totalRefunds = 0;
            var totalWithoutVat = 0;
            var totalVat = 0;
            var totalIncludingVat = 0;

            for (var i = 0; i < self.results().length; i++) {
                if (self.results()[i].FinanceDocumentType() == self.invoiceString)
                {
                    totalInvoices += self.results()[i].TotalAmount()
                }
                else if (self.results()[i].FinanceDocumentType() == self.invoiceReciptString) {
                    totalInvoiceRecipts += self.results()[i].TotalAmount()
                }
                else if (self.results()[i].FinanceDocumentType() == self.refundString) {
                    totalRefunds += self.results()[i].TotalAmount()
                }

                var beforeVat = parseFloat(self.results()[i].TotalAmount() / self.results()[i].VAT());
                totalWithoutVat += beforeVat;
                totalVat += parseFloat(self.results()[i].TotalAmount() - beforeVat);
                totalIncludingVat += parseFloat(self.results()[i].TotalAmount());
            }
         
            self.totalInvoiceRecipts(totalInvoiceRecipts.toFixed(2));
            self.totalInvoices(totalInvoices.toFixed(2));
            self.totalRefunds(totalRefunds.toFixed(2));
            self.totalWithoutVat(totalWithoutVat.toFixed(2));
            self.totalVat(totalVat.toFixed(2));
            self.totalIncludingVat(totalIncludingVat.toFixed(2));
        });

        //actions

        self.search = function () {

            if (self.selectedIssuer() == null)
                return;

            //showModal();
            ShowLoder();

            var url = '/FinanceReports/GetInvoiceReportData' +
                '?from=' + self.from() +
                '&to=' + self.to() +
                '&showOnlyConverted=' + self.onlyConvertedInvoices() +
                '&issuerId=' + self.selectedIssuer();

            $.getJSON(url, function (data) {
                ko.mapping.fromJS(data, self.results);
                self.showSummary(true);
                hideLoder();
            });
        };

        self.ExportToExcel = function () {
            window.location.href = '/FinanceReports/InvoiceReportExportToExcel' +
                '?from=' + self.from() +
                '&to=' + self.to() +
                '&showOnlyConverted=' + self.onlyConvertedInvoices() +
                '&issuerId=' + self.selectedIssuer();
        };

        self.print = function () {
            var url = '/FinanceReports/PrintInvoiceReport' +
                '?from=' + self.from() +
                '&to=' + self.to() +
                '&showOnlyConverted=' + self.onlyConvertedInvoices() +
                '&issuerId=' + self.selectedIssuer();
            //location.href = url;
            window.open(url, '_blank');
        };

        self.init = function () {
            ShowLoder();
            $.get('/FinanceMetaData/GetClinicIssuers', null, function (data) {
                ko.mapping.fromJS(data, self.issuers);
                hideLoder();
            });

            if(self.redirected() == "True")
            {
                self.onlyConvertedInvoices(true);
                self.fromDate(moment().format("DD/MM/YYYY"));
                self.toDate(moment().format("DD/MM/YYYY"));
            }
            else
            {
                self.fromDate(moment().month(moment().month() - 1).set('date',1).format("DD/MM/YYYY"));
                self.toDate(moment().subtract(1, 'month').endOf('month').format("DD/MM/YYYY"));
            }
        };



        self.init();
    }

    ko.applyBindings(new viewModel());

});