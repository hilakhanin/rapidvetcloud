﻿$(function () {


    function viewModel() {

        var self = this;
        self.cheques = ko.mapping.fromJS([]);

        //filters
        self.fromDate = ko.observable();
        self.toDate = ko.observable();

        self.paidCheques = ko.observable('true');
        self.documentType = ko.observable('0');

        self.issuers = ko.mapping.fromJS([]);
        self.selectedIssuer = ko.observable();

        self.filterUrl = ko.computed(function () {
            return '?from=' + self.fromDate() +
                '&to=' + self.toDate() +
                '&documentTypeId=' + self.documentType() +
                '&paidCheques=' + self.paidCheques() +
                '&issuerId=' + self.selectedIssuer();
        });

        self.noSearch = ko.computed(function() {
            var result = true;
            if (typeof self.fromDate() != "undefined" && typeof self.toDate() != "undefined") {
                result = false;
            }

            return result;
        });

        self.printUrl = ko.computed(function () {
            return '/FinanceReports/PrintChequesReport' + self.filterUrl();
        });


        //actions
        self.getData = function () {
            if (!self.noSearch()) {
                self.cheques([]);
                ShowLoder();
                var url = '/FinanceReports/GetChequesData' + self.filterUrl();

                $.getJSON(url, function(data) {
                    ko.mapping.fromJS(data, self.cheques);
                }).done(function() {
                    hideLoder();
                });
            }
        };

        self.init = function () {
            ShowLoder();
            $.get('/FinanceMetaData/GetClinicIssuers', null, function (data) {
                ko.mapping.fromJS(data, self.issuers);
                hideLoder();
            });
        };

        self.init();

    }

    ko.applyBindings(new viewModel());
});