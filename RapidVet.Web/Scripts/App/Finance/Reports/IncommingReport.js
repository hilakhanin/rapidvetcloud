﻿$(function () {
    function validateHhMm(inputField) {
        var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField);
        
        return isValid;
    }

    function viewModel() {

        var self = this;
        self.fromDate = ko.observable(moment().format('DD/MM/YYYY'));
        self.toDate = ko.observable(moment().format('DD/MM/YYYY'));
        self.fromTime = ko.observable("00:00");
        self.toTime = ko.observable("23:59");
        self.errorMsg = ko.observable('');
        self.issuers = ko.observable([]);
        self.selectedIssuerId = ko.observable();
        self.documents = ko.mapping.fromJS([]);

        self.totalCash = ko.computed(function () {
            var result = 0;
            ko.utils.arrayForEach(self.documents(), function (d) {
                result += parseFloat(d.CashPaymentSum());
            });
            return result;
        });

        self.totalTransfers = ko.computed(function () {
            var result = 0;
            ko.utils.arrayForEach(self.documents(), function (d) {
                result += parseFloat(d.BankTransferSum());
            });
            return result;
        });

        self.totalCredit = ko.computed(function () {
            var result = 0;
            ko.utils.arrayForEach(self.documents(), function (d) {
                result += parseFloat(d.TotalCreditPaymentSum());
            });
            return result;
        });

        self.totalChecqes = ko.computed(function () {
            var result = 0;
            ko.utils.arrayForEach(self.documents(), function (d) {
                result += parseFloat(d.TotalChequesPayment());
            });
            return result;
        });

        self.totalReport = ko.computed(function () {
            return self.totalChecqes() + self.totalCredit() + self.totalTransfers() + self.totalCash();
        });


        //functions
        self.search = function () {
           

            if (!validateHhMm(self.fromTime()))
            {
                self.fromTime("00:00");
            }

            if (!validateHhMm(self.toTime())) {
                self.toTime("23:59");
            }

            if (self.hoursValid(self.fromTime(), self.toTime())) {
                ShowLoder();

                $.getJSON('/DoctorLog/GetIncomingsReportData?fromTime=' + self.fromTime() + '&toTime=' + self.toTime() + '&fromDate=' + self.fromDate() + '&toDate=' + self.toDate() + '&issuerId=' + self.selectedIssuerId(), function (data) {
                    ko.mapping.fromJS(data, self.documents);
                }).done(function () {
                    hideLoder();
                });
            }                      
        };

        self.ExportToExcel = function () {


            if (!validateHhMm(self.fromTime())) {
                self.fromTime("00:00");
            }

            if (!validateHhMm(self.toTime())) {
                self.toTime("23:59");
            }

            if (self.hoursValid(self.fromTime(), self.toTime())) {                
                window.location.href = '/DoctorLog/ExportToExcel?fromTime=' + self.fromTime() + '&toTime=' + self.toTime() + '&fromDate=' + self.fromDate() + '&toDate=' + self.toDate() + '&issuerId=' + self.selectedIssuerId();
            }
        };

        self.getIssuers = function () {
            ShowLoder();

            $.get('/FinanceReports/GetIssuers', function (data) {
                var mappedIssuers = $.map(data, function (item) {
                    return new SelectItem(item.Value, item.Text);
                });
                self.issuers(mappedIssuers);
            }).done(function () {
              
                hideLoder();
            });
        };

        self.hoursValid = function (from, to) {
            hours = to.split(':')[0] - from.split(':')[0],
               minutes = to.split(':')[1] - from.split(':')[1];

            minutes = minutes.toString().length < 2 ? '0' + minutes : minutes;
            if (minutes < 0) {
                hours--;
                minutes = 60 + minutes;
            }
            hours = hours.toString().length < 2 ? '0' + hours : hours;
            var diff = hours + ':' + minutes;

            if (diff[0] == "-") {
                self.errorMsg('שעת ההתחלה הרצויה מאוחרת משעת הסיום הרצויה');
                return false;
            }
            else if (diff == "00:00") {
                self.errorMsg('שעת ההתחלה הרצויה שווה לשעת הסיום הרצויה');
                return false;
            }
            self.errorMsg('');
            return true;
        };

        self.getIssuers();

    }

    ko.applyBindings(new viewModel());

});

function SelectItem(value, text) {
    var self = this;
    self.Value = value;
    self.Text = text;
}