﻿$(function () {

    function viewModel() {

        var self = this;
        self.optionsCaption = ko.observable('בחר..');
        self.documents = ko.mapping.fromJS([]);

        //filters

        self.issuers = ko.mapping.fromJS([]);
        self.issuerId = ko.observable();

        self.documentTypes = ko.mapping.fromJS([]);
        self.documentTypeId = ko.observable();

        self.paymentTypes = ko.mapping.fromJS([]);
        self.paymentTypeId = ko.observable();

        self.fromDate = ko.observable();
        self.toDate = ko.observable();

        self.filterUrl = ko.computed(function () {
            var notSelectedDocs = "";
            var i = 0;
            $(self.documentTypes()).each(function () {
                if (!this.Slected()) {
                    notSelectedDocs = notSelectedDocs + '&notSelectedDocIds[' + i + ']=' + this.Value();
                    i++;
                }
            });
            var notSelectedpayments = "";
            i = 0;
            $(self.paymentTypes()).each(function () {
                if (this.Slected()) {
                    notSelectedpayments = notSelectedpayments + '&selectedPaymentType[' + i + ']=' + this.Value();
                    i++;
                }
            });
            return '?from=' + self.fromDate() +
                '&to=' + self.toDate() +
                '&issuerId=' + self.issuerId() +
                 notSelectedDocs +
                 notSelectedpayments;
        });

        self.printUrl = ko.computed(function () {

            return '/FinanceReports/PrintIncome' + self.filterUrl();

        });

        self.exportUrl = ko.computed(function() {
            return '/FinanceReports/ExportIncomeReport' + self.filterUrl();
        });

        self.totalDocmentsSum = ko.observable(0);
       

        //functions
        //FinanceMetaData

        self.getIssuers = function () {
            $.getJSON('/FinanceMetaData/GetClinicIssuers', function (data) {
                ko.mapping.fromJS(data, self.issuers);
                if (self.issuers().length == 1) {
                    self.optionsCaption(undefined);
                }
            });
        };

        self.getDocumentTypes = function () {
            $.getJSON('/FinanceMetaData/GetFinanceDocumentTypes', function (data) {
                $(data).each(function () {
                    this.Slected = true;
                });
                ko.mapping.fromJS(data, self.documentTypes);
            });
        };

        self.GetPaymentTypes = function () {
            $.getJSON('/FinanceMetaData/GetPaymentTypes', function (data) {
                $(data).each(function () {
                    this.Slected = true;
                });
                ko.mapping.fromJS(data, self.paymentTypes);
            });
        };


        self.filtersInit = function () {

            ShowLoder();

            self.getIssuers();
            self.GetPaymentTypes();
            self.getDocumentTypes();

            hideLoder();

        };

        self.search = function () {

            ShowLoder();
            var url = '/FinanceReports/GetIncomeData' + self.filterUrl();
            self.documents([]);
            $.getJSON(url, function (data) {
                ko.mapping.fromJS(data, self.documents);
                var sum = 0;
               
                $(data).each(function () {
                    sum += this.TotalToPay;                    
                });
                self.totalDocmentsSum(sum.toFixed(2));
                hideLoder();
            });

        };


        //execution

        self.filtersInit();
    }


    ko.applyBindings(new viewModel());

});