﻿$(function () {

    function viewModel() {
        var self = this;

        //filters
        self.fromDate = ko.observable();
        self.toDate = ko.observable();

        self.issuers = ko.mapping.fromJS([]);
        self.selectedIssuer = ko.observable();
        
        self.createdDate = ko.observable('true');

        self.noSearch = ko.computed(function() {
            var result = true;
            
            if (typeof self.fromDate() != "undefined" && typeof self.toDate() != "undefined") {
                result = false;
            }

            return result;
        });


        //results
        self.cash = ko.observable();
        self.cheques = ko.observable();
        self.creditCards = ko.observable();
        self.transfers = ko.observable();
        self.totalIncludeVat = ko.observable();
        //self.totalNoVat = ko.observable();


        //actions
        self.getData = function () {

            ShowLoder();

            var url = '/FinanceReports/GetPaymentMethodsSegmentation' +
                '?from=' + self.fromDate() +
                '&to=' + self.toDate() +
                '&sumByCreatedDate=' + self.createdDate() +
                '&issuerId=' + self.selectedIssuer();


            $.getJSON(url, function (data) {

                self.cash(data.Cash);
                self.cheques(data.Cheques);
                self.creditCards(data.CreditCards);
                self.transfers(data.Transfers);
                self.totalIncludeVat(data.TotalIncludeVat);
                //self.totalNoVat(data.TotalNoVat);

            }).done(function () {
                hideLoder();
            });
        };

        self.init = function () {
            ShowLoder();
            $.get('/FinanceMetaData/GetClinicIssuers', null, function(data) {
                ko.mapping.fromJS(data, self.issuers);
                hideLoder();
            });
        };

        self.init();
    }

    ko.applyBindings(new viewModel());

});