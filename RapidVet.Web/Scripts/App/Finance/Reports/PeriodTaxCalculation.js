﻿function viewModel() {
    var self = this;
    /*-----------------------------------------------------------DATES -------------------------------------------*/
    self.ToDate = ko.observable().extend({ required: { params: true, message: "הכנס תאריך" } });
    self.FromDate = ko.observable().extend({ required: { params: true, message: "הכנס תאריך" } });

    self.reportVisible = ko.observable(false);
    self.issuers = ko.observable([]);
    self.selectedIssuer = ko.observable();

    /*-----------------------------------------------------------TAX REPORT----------------------------------------*/
    self.TuroverNoVAT = ko.observable(0);
    self.AdvancedPaymentPercent = ko.observable(0);

    self.AdvancedPaymentPercentFactor = ko.computed(function () {
        return self.AdvancedPaymentPercent() / 100;
    });

    self.AdvanceByTurnoverPercent = ko.computed(function () {
        return self.TuroverNoVAT() * self.AdvancedPaymentPercentFactor();
    });

    self.orgnDeduction = ko.observable(0);
    self.ovrallDeductions = ko.observable(0);

    self.OriginDeduction = ko.computed({ //this is NIKUIM BAMAKOR LEKIZUZ
        read: function () {
            return self.orgnDeduction();
        },
        write: function (newValue) {
            var parsedValue = parseFloat(newValue);
            self.orgnDeduction(isNaN(parsedValue) ? newValue : parsedValue);
        }
    });

    self.OverallDeductions = ko.computed({
        read: function () {
            return self.ovrallDeductions();
        },
        write: function (newValue) {
            var parsedValue = parseFloat(newValue);
            self.ovrallDeductions(isNaN(parsedValue) ? newValue : parsedValue);
        }
    });

    self.DeductionAndTurnover = ko.computed(function () {
        return self.OverallDeductions() + self.TuroverNoVAT();
    });

    self.OverallToPay = ko.computed(function () {
        return self.AdvanceByTurnoverPercent() - self.OriginDeduction();
    });

    self.validate = function () {
        var errors = ko.validation.group(self, { deep: false });
        errors.showAllMessages(true);
        return errors().length == 0;
    };

    /*-----------------------------------------------------------VAT REPORT--------------------------------------------------*/

    self.NoVATDeals = ko.observable(0);
    self.WithVATDeals = ko.observable(0);
    self.VATonDeals = ko.observable(0);
    self.PermanentAssetsInputTax = ko.observable(0);
    self.OtherInputTax = ko.observable(0);

    self.OverallVAT = ko.computed(function () {
        return self.VATonDeals() - self.PermanentAssetsInputTax() - self.OtherInputTax();
    });

    /*-----------------------------------------------------LOAD DATA-----------------------------------------------*/

    self.LoadData = function () {
        if (self.validate()) {
            ShowLoder();
            var qs = {
                from: self.FromDate(),
                to: self.ToDate(),
                issuerId: self.selectedIssuer()
            };
            $.get('/FinanceReports/GetPeriodTaxCalculationData', $.param(qs), function (data) {
                //tax
                self.TuroverNoVAT(data.TuroverNoVAT);
                self.AdvancedPaymentPercent(data.AdvancedPaymentPercent);
                //vat
                self.NoVATDeals(data.NoVATDeals);
                self.WithVATDeals(data.WithVATDeals);
                self.VATonDeals(data.VATonDeals);
                self.PermanentAssetsInputTax(data.PermanentAssetsInputTax);
                self.OtherInputTax(data.OtherInputTax);

                self.reportVisible(true);
                hideLoder();
            });
        }
    };

    self.getIssuers = function () {
        ShowLoder();

        $.get('/FinanceReports/GetAllIssuersDropDown', function (data) {
            var mappedIssuers = $.map(data, function (item) {
                return new SelectItem(item.Value, item.Text);
            });
            self.issuers(mappedIssuers);
        }).done(function () {
            hideLoder();
        });
    };

    self.getIssuers()
}

function SelectItem(value, text) {
    var self = this;
    self.Value = value;
    self.Text = text;
}
