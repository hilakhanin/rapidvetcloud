﻿
function viewModel(itemsList) {
    var self = this;
    self.fromDate = ko.observable();
    self.toDate = ko.observable();

    self.items = ko.mapping.fromJS([]);

    self.selectedItems = ko.computed(function() {
        return ko.utils.arrayFilter(self.items(), function(item) {
            return item.Selected();
        });
    });

    self.groupIds = ko.computed(function() {
        var res = new Array();
        ko.utils.arrayForEach(self.selectedItems(), function(item) {
            res.push(item.Value());
        });
        return res;
    });

    self.graphActionSrc = ko.computed(function() {
        var qs = {
            fromDate: self.fromDate(),
            toDate: self.toDate(),
            groupIds: ko.toJSON(self.groupIds)
        };
            return '/FinanceReports/GetExpenseDistributionChart?' + $.param(qs);
  
    });

    self.selectedNumber = ko.computed(function() {
        return self.selectedItems().length;
    });

    self.display = function () {
        if (typeof self.fromDate() == "undefined" || typeof self.toDate() == "undefined") {
            $('#datesError').show();
        }
        else {
            $('#datesError').hide();
            $('#result').attr('src', self.graphActionSrc());
        }
    };

    ko.mapping.fromJS(itemsList, self.items);
}