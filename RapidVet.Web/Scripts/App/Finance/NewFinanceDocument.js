﻿function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

function clearClientSearch() {
    viewModel.ClientId(undefined);
    viewModel.ClientNameWithPatients("");
    $("#client-search").val("");
    $("#no-results").hide();
};

$(function () {

    $("#client-search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Clients/Search",
                type: "POST",
                data: {
                    query: request.term
                },
                success: function (data) {
                    response(data);

                    if (data.length == 0) {
                        $("#no-results").show();
                        setTimeout(function () { $("#no-results").hide(); }, 5000);
                    }
                    else {
                        $("#no-results").hide();
                    }
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            event.preventDefault();
            viewModel.ClientId(ui.item.value);
            viewModel.ClientNameWithPatients(ui.item.label);
            $("#client-search").val(ui.item.label);
            viewModel.CasualClient(false);
            //window.location = "/Clients/Patients/" + ui.item.value;
        },
        focus: function (event, ui) {
            event.preventDefault();
            $("#client-search").val(ui.item.label);
        }
        ,
        change: function (event, ui) {
            event.preventDefault();
            if (isEmptyOrWhiteSpaces($("#client-search").val()) ||
                typeof viewModel.ClientId() == "undefined" || viewModel.ClientNameWithPatients() != $("#client-search").val()) {
                clearClientSearch();
            }

        },
        open: function () {
            $(".ui-autocomplete").css("max-height", 120);
            $(".ui-autocomplete").css("max-width", 225);
            $(".ui-autocomplete").css("overflow-y", "auto");
            $(".ui-autocomplete").css("overflow-x", "hidden");

        }
    });
});

/*--------------------------------------------------------------VALIDATION STUFF--------------------------------------*/
var oldTarrifId;

var percentValidation = {
    validator: function (val) {
        var parsed = parseFloat(val);
        return parsed <= 100 && parsed >= 0;
    },
    message: "יש לרשום הנחה באחוזים"
};

//here positive = nonnegative 
var positiveValidation = {
    validator: function (val) {
        if (!invoice && receipt) {
            return true;
        }
        var parsed = parseFloat(val);
        return parsed >= 0;
    },
    message: "יש לרשום מספר גדול מ-0"
};

var differentThanZero = {
    validator: function (val) {
        if (!invoice && receipt) {
            return true;
        }
        var parsed = parseFloat(val);
        return parsed != 0;
    },
    message: "חובה להזין ערך"
};

function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

function itemNameLength(val) {
    var isValid = val.length < 100 && val.length > 0;

    if (isValid) {
        $("#itemNameError").hide();
    }
    else {
        $("#itemNameError").css('display', 'block');
        $("#itemNameError").modal('show');
    }

    return isValid;

};

function itemPriceCheck(price) {
    var isValid = !isNaN(parseFloat(price));

    if (isValid) {
        $("#itemPriceError").hide();
    }
    else {
        $("#itemPriceError").css('display', 'block');
        $("#itemPriceError").modal('show');
    }

    return isValid;

};


var lastFourDigits = {
    validator: function (val) {
        if (typeof (val) == "undefined") {
            return true;
        }
        if (!viewModel.CreditPayment()) {
            return true;
        }
        return val.length == 4;
    },
    message: "4 ספרות בלבד"
};

var totalValidation = {
    validator: function (val) {
        if (!invoice && receipt) {
            return true;
        }
        var parsed = parseFloat(val);
        return parsed != 0;
    },
    message: "לא ניתן להנפיק מסמך בסכום 0"
};

/*----onlyIf: BankTransfer validation-----*/
var bankTransferPercent = percentValidation;
bankTransferPercent['onlyIf'] = function () {
    return viewModel.BankTransfer();
};
var bankTransferPositive = positiveValidation;
bankTransferPositive['onlyIf'] = function () {
    return viewModel.BankTransfer();
};
/*----onlyIf: CreditPayment validation----*/
var creditPositive = positiveValidation;
creditPositive['onlyIf'] = function () {
    return viewModel.CreditPayment();
};
/*----onlyIf: CashPayment validation----*/
var cashValidation = differentThanZero;
cashValidation['onlyIf'] = function () {
    return viewModel.CashPayment();
};

/*-----validation rules (this doesn't seem to work for computed hence the repetition)-------*/
//ko.validation.rules['required'].message = "שדה הכרחי";
ko.validation.rules['percent'] = percentValidation;
ko.validation.rules['positive'] = positiveValidation;

/*---------------------------------------------------VIEW MODEL---------------------------------------------------------------*/

function openInvioce(data) {
    var self = this;
    self.Id = ko.observable(data.Id);
    self.SerialNumber = ko.observable(data.SerialNumber);
    self.Sum = ko.observable(data.Sum);
    self.Date = ko.observable(data.Date);
    self.SumToPay = ko.observable(data.SumToPay);
}

function billedItem(description, catalogNumber, quantity, unitprice, discount, visitId, allowUpdate, itemId, discountPercentage, addedBy, addedByUserId, examinationID, medicationID, preventiveMedicineID, treatmentID, updateItemInVisit) {
    var self = this;
    self.qntty = ko.observable(quantity);
    self.up = ko.observable(parseFloat(unitprice));
    self.dscnt = ko.observable(discount);
    self.dscntpercent = ko.observable(discountPercentage);
    self.itemExaminationID = ko.observable(examinationID);
    self.itemMedicationID = ko.observable(medicationID);
    self.itemPreventiveMedicineID = ko.observable(preventiveMedicineID);
    self.itemTreatmentID = ko.observable(treatmentID);
    self.PriceListItemId = ko.observable(itemId);

    self.VisitId = ko.observable(visitId);
    self.AllowUpdate = ko.observable(allowUpdate);
    self.Description = ko.observable(description).extend({
        validation: {
            validator: function (val) {
                if (self.PriceListItemId() != null || !self.AllowUpdate()) {
                    return true;
                }
                else {
                    if (val == null) {
                        return true;
                    }
                    return val.length < 100;
                }

            },
            message: "נא להזין שם פריט עד 100 תווים"
        }
    });


    

    //self.TotalToPay

    setTimeout("SetTotalBeforeVAT();", 200);

    self.CatalogNumber = ko.observable(catalogNumber);
    self.UpdateItemInVisit = ko.observable(updateItemInVisit);

    self.Quantity = ko.computed({
        read: function () {
            return self.qntty();
        },
        write: function (newValue) {
            var parsedValue = parseFloat(newValue);
            self.qntty(isNaN(parsedValue) ? newValue : parsedValue);
        }
    }).extend({ validation: differentThanZero })
    .extend({ number: { params: true, message: "יש להזין ערך תקין" } });

    self.PriceWithVAT = ko.computed({
        read: function () {
            return self.up();
        },
        write: function (newValue) {
            var parsedValue = parseFloat(newValue);
            self.up(isNaN(parsedValue) ? newValue : parsedValue);
        }
    });

    self.UnitPrice = ko.computed(function () {
        return self.PriceWithVAT() / vatFactor;
    });

    self.Discount = ko.computed({
        read: function () {
            return self.dscnt();
        },
        write: function (newValue) {
            var parsedValue = parseFloat(newValue);
            self.dscnt(isNaN(parsedValue) ? newValue : parsedValue);
        }
    }).extend({
        validation: {
            validator: function (val) {
                return val >= 0 && val <= Math.abs(self.PriceWithVAT());
            },
            message: "יש להכניס מספר חוקי"
        }
    });

    self.DiscountPercentage = ko.computed({
        read: function () {
            return self.dscntpercent();
        },
        write: function (value) {
            var parsedValue = parseFloat(value);
            if (!isNaN(parsedValue)) {
                self.dscntpercent(parsedValue);
            }
        }
    }).extend({
        validation: percentValidation
    });



    self.TotalBeforeVAT = ko.computed(function () {        
        return ((self.PriceWithVAT() * (1.00 - (self.DiscountPercentage() / 100)) - self.Discount()) / vatFactor) * self.Quantity();
    });

    self.TotalAfterVAT = ko.computed(function () {
        return self.TotalBeforeVAT() * vatFactor;//((self.UnitPrice() * (1.00 - (self.DiscountPercentage() / 100)) - self.Discount()) * vatFactor) * self.Quantity();
    });

    self.AddedBy = ko.observable(addedBy);
    self.AddedByUserId = ko.observable(addedByUserId);
   
}


function cheque() {
    var self = this;
    self.checkSum = ko.observable(0.00);

    self.ChequeDate = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return viewModel.ChequePayment()
            }
        }
    });

    self.BankCodeId = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return viewModel.ChequePayment()
            }
        }
    });

    self.Branch = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return viewModel.ChequePayment()
            }
        },
        number: { params: true, message: "יש להזין ערך תקין" },
        min: { params: 0, message: "ערך מינימלי: 0" }

    });


    self.Account = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return viewModel.ChequePayment()
            }
        },
        number: { params: true, message: "יש להזין ערך תקין" },
        min: { params: 0, message: "ערך מינימלי: 0" }

    });

    self.ChequeNumber = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return viewModel.ChequePayment()
            }
        },
        number: { params: true, message: "יש להזין ערך תקין" },
        min: { params: 0, message: "ערך מינימלי: 0" }

    });


    self.Sum = ko.computed({
        read: function () {
            return self.checkSum();
        },
        write: function (newValue) {
            var parsedValue = parseFloat(newValue);
            self.checkSum(isNaN(parsedValue) ? newValue : parsedValue);
        }
    }).extend({
        required: {
            onlyIf: function () {
                return viewModel.ChequePayment();
            }
        },
    }).extend({
        validation: differentThanZero,
        onlyIf: function () {
            return viewModel.ChequePayment();
        }
    });
}


/*------------------------------------------------------------------ BANK TRANSFER ------------------------------------------------------------*/
function bankTransfer() {
    var self = this;
    self.transferSum = ko.observable(0.00);
    self.witholdingPercent = ko.observable(0.00);

    self.TransferDate = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return viewModel.BankTransfer();
            }
        }
    });;

    self.TransferBankCodeId = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return viewModel.BankTransfer()
            }
        }
    });

    self.TransferAccount = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return viewModel.BankTransfer()
            }
        }
    });

    self.TransferBranch = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return viewModel.BankTransfer()
            }
        }
    });

    self.BankTransferNumber = ko.observable();

    self.BankTransferSum = ko.computed({
        read: function () {
            return self.transferSum();
        },
        write: function (newValue) {
            var parsedValue = parseFloat(newValue);
            self.transferSum(isNaN(parsedValue) ? newValue : parsedValue);
        }
    }).extend({
        validation: differentThanZero
    }).extend({
        required: {
            onlyIf: function () {
                return viewModel.BankTransfer();
            }
        }
    });

    self.WitholdingTax = ko.computed({
        read: function () {
            return self.witholdingPercent();
        },
        write: function (newValue) {
            var parsedValue = parseFloat(newValue);
            self.witholdingPercent(isNaN(parsedValue) ? newValue : parsedValue);
        }
    }).extend({
        validation: bankTransferPercent
    });

    self.Total = ko.computed(function () {
        return self.BankTransferSum();
    });
}

var debtDescription = "יתרה לתשלום";
/*-----------------------------------------------------------------------MAIN VIEW MODEL-------------------------------------------------------------*/
function viewModel(id, withInvoice, withReceipt, docTypeId, defaultTariff, defaultIssuerId, updateItemInPatientFile) {
    var self = this;
    self.Invoice = ko.observable(withInvoice);
    self.Receipt = ko.observable(withReceipt);
    self.DocumentTypeId = ko.observable(docTypeId);

    self.isReceipt = ko.computed(function () {
        return self.DocumentTypeId() == 1;
    });

    /*-------------------------------------------------------------OPTIONS FOR DDL MENUS ------------------------------------------------*/
    self.DoctorOptions = ko.mapping.fromJS([]);
    self.ClientOptions = ko.mapping.fromJS([]);
    self.IssuerOptions = ko.mapping.fromJS([]);
    self.BankCodeOptions = ko.mapping.fromJS([]);
    self.CreditCardCodeOptions = ko.mapping.fromJS([]);
    self.CreditPaymentTypeOptions = ko.mapping.fromJS([]);
    self.TerminalOptions = ko.mapping.fromJS([]);

    self.TariffOptions = ko.mapping.fromJS([]);
    self.SelectedTariff = ko.observable(defaultTariff);

    self.IssuerId = ko.observable();
    self.OldIssuerId = ko.observable();
    self.DoctorId = ko.observable();
    self.Comments = ko.observable();

    self.AddedBy = ko.observable();
    self.AddedByUserId = ko.observable();
    self.UpdateItemInVisit = ko.observable(updateItemInPatientFile);

    self.RoundingFactor = ko.observable(0.00);
    self.RoundingFactorForDisplay = ko.observable(0.00);


    self.IssuerId.subscribe(function () {
        if (self.IssuerId() != self.OldIssuerId()) {
            ShowLoder();

            var params = {
                //clientId: id,
                documentType: self.DocumentTypeId(),
                issuerId: self.IssuerId() ? self.IssuerId() : defaultIssuerId
            };

            $.get('/FinanceDocument/GetNewInvoiceReceipt', $.param(params), function (data) {
                //ko.mapping.fromJS(data.DoctorOptions, self.DoctorOptions);
                //ko.mapping.fromJS(data.ClientOptions, self.ClientOptions);
                //ko.mapping.fromJS(data.IssuerOptions, self.IssuerOptions);
                //ko.mapping.fromJS(data.BankCodeOptions, self.BankCodeOptions);
                ko.mapping.fromJS(data.CreditCardCodeOptions, self.CreditCardCodeOptions);
                //ko.mapping.fromJS(data.TariffOptions, self.TariffOptions);
                //self.SelectedTariff(defaultTariff);
                //self.UpdatePriceListItems();
                //if (data.CreditCardCodeOptions.length == 0) {
                //    self.disableCreditPayment(true);
                //    alert("לא הוגדרו סוגי כרטיסי אשראי למנפיק, לא ניתן לחייב באשראי");
                //} else {
                //    self.disableCreditPayment(false);
                //}
                self.SelectedTariff.subscribe(function (newValue) {
                    if (typeof (newValue) !== "undefined") {
                        if (newValue.toString() != oldTarrifId) {
                            self.UpdatePriceListItems();
                            oldTarrifId = newValue.toString();
                        }
                    }
                });
                // self.ClientId(id);
            });

            var qs = {
                issuerId: self.IssuerId() ? self.IssuerId() : defaultIssuerId
            };

            $.get('GetIssuerData', $.param(qs), function (data) {
                self.EasyCard(data.IsEasyCard);
                ko.mapping.fromJS(data.CreditTypes, self.CreditPaymentTypeOptions);
                if (data.IsEasyCard) {
                    ko.mapping.fromJS(data.Terminals, self.TerminalOptions);
                }
                if (data.CreditTypes.length == 0) {
                    self.disableCreditPayment(true);
                    alert("לא הוגדרו סוגי כרטיסי אשראי למנפיק, לא ניתן לחייב באשראי");
                } else {
                    self.disableCreditPayment(false);
                }
                hideLoder();
            });
            self.CreditPayment(false);
            self.ManualCreditPayment(false);
            self.OldIssuerId(self.IssuerId());
        }
    });

    self.UpdatePriceListItems = function () {

        //if (NoMham.length == 0)
        //    NoMham = "True";
        //self.NoMham(NoMham);
        var noMham = false;
        if ($('.NoMham') && $('.NoMham').length > 0 && $('.NoMham')[0].checked)
            noMham = true;
        var qs = {
            tariff: self.SelectedTariff(),
            noMham: noMham//self.NoMham()
        };
        $.get('GetPriceListItems', $.param(qs), function (data) {
            ko.mapping.fromJS(data.Items, self.priceListItems);
            ko.mapping.fromJS(data.Categories, self.priceListCategories);
        });
    };

    self.ClientNameWithPatients = ko.observable("");
    self.CasualClient = ko.observable(false);
    self.ClientId = ko.observable(undefined).extend({ required: { params: true, message: "יש לבחור לקוח" } });
    self.ClientBalance = ko.observable();
    //    .extend({
    //    required: true,
    //    message: "יש לבחור לקוח"
    //});
    self.ClientName = ko.observable();
    self.ClientAddress = ko.observable();
    self.ClientPhone = ko.observable();
    /*--------------------------------------------------------------BILLED ITEMS----------------------------------------------------------*/
    self.items = ko.observableArray();
    self.removeItem = function (item) {
        self.items.remove(item);
    };
    /*---------------------------------------------------------------OPEN INVOICES------------------------------------------------------*/
    self.OpenInvoices = ko.observableArray([]);
    self.RelatedDocuments = ko.observableArray([]);
    self.SelectedOpenInvoicesSum = ko.computed(function () {
        var sum = 0;
        for (var i = 0; i < self.RelatedDocuments().length; i++) {
            sum = sum + parseFloat(self.RelatedDocuments()[i].SumToPay());
        }
        return sum;
    });



    self.ReciptPriningOnly = ko.observable(self.DocumentTypeId() == 1 ? true : false);
    /*-------------------------------------------------------------payment flags----------------------------------------------------------*/
    self.CashPayment = ko.observable(false);
    self.ChequePayment = ko.observable(false);
    self.CreditPayment = ko.observable(false);
    self.BankTransfer = ko.observable(false);
    self.NoMham = ko.observable(false);

    /*-------------------------------------------------------------CASH PAYMENT-----------------------------------------------------------*/
    self.cashSum = ko.observable(undefined);
    self.CashPaymentSum = ko.computed({
        read: function () {
            return self.cashSum();
        },
        write: function (newValue) {
            var parsedValue = parseFloat(newValue);
            self.cashSum(isNaN(parsedValue) ? newValue : parsedValue);
        }
    })//.extend({ validation: differentThanZero, onlyIf: function () { return self.CashPayment(); } });
    .extend({
        required: {
            onlyIf: function () {
                return self.CashPayment();
            }
        },
    }).extend({
        validation: differentThanZero,
        onlyIf: function () {
            return self.CashPayment();
        }
    });


    self.CashPayment.subscribe(function () {
        if (self.CashPayment()) {
            self.CashPaymentSum(0);
        } else {
            self.cashSum(undefined);
        }
    });

    /*--------------------------------------------------------------CHEQUE PAYMENT----------------------------------------------------------*/

    self.Cheques = ko.observableArray();

    self.addCheque = function () {
        var numberOfCheques = self.Cheques().length;

        if (numberOfCheques == 0) {
            $.get('GetLastChequeDetails', { id: self.ClientId() }, function (data) {
                if (data != null) {
                    var myChequesArray = self.Cheques;

                    //var lastCheque = myChequesArray.pop();
                    var newCheque = new cheque();
                    newCheque.ChequeDate(moment().format("DD/MM/YYYY"));
                    newCheque.BankCodeId(parseInt(data.BankCodeId));
                    newCheque.Branch(data.BankBranch);
                    newCheque.Account(data.BankAccount);
                    myChequesArray.push(newCheque);
                }
                else {
                    self.Cheques.push(new cheque());
                }
            });
        }
        else {
            var myChequesArray = self.Cheques;
            var lastCheque = myChequesArray.pop();
            myChequesArray.push(lastCheque);

            if (self.lastChequeIsValid(lastCheque)) {
                $("#chequeDetailsEroorMsg").hide();
                self.pushCheque(false);
            }
            else {
                $("#chequeDetailsEroorMsg").show();
            }
        }
    };
    self.addMultipleCheques = function () {

        for (i = 0; i < self.multipleChequesNumber() ; i++)
            self.pushCheque(true);

        self.hideMultipleCheques();
    };

    self.pushCheque = function (copySum) {
        var myChequesArray = self.Cheques;

        var lastCheque = myChequesArray.pop();
        var newCheque = new cheque();

        var chequeDate = lastCheque.ChequeDate();
        var newDate = moment(chequeDate, "DD/MM/YYYY");
        newDate.add(1, 'months');

        newCheque.ChequeDate(newDate.format("DD/MM/YYYY"));
        newCheque.BankCodeId(lastCheque.BankCodeId());
        newCheque.Branch(lastCheque.Branch());
        newCheque.Account(lastCheque.Account());
        newCheque.ChequeNumber(parseInt(lastCheque.ChequeNumber()) + 1);
        newCheque.Sum(copySum ? self.futureChequeSum() : 0);
        myChequesArray.push(lastCheque);
        myChequesArray.push(newCheque);
    }
    self.lastChequeIsValid = function (lastCheque) {
        var isValid = false;

        if (self.Cheques().length > 0) {

            isValid = self.isUndefinedOrWhitespaces(lastCheque.ChequeDate()) || self.isUndefinedOrWhitespaces(lastCheque.BankCodeId()) ||
                      self.isUndefinedOrWhitespaces(lastCheque.Branch()) || self.isUndefinedOrWhitespaces(lastCheque.Account()) ||
                      self.isUndefinedOrWhitespaces(lastCheque.ChequeNumber())
            if (!isValid) {
                //validate all fields are ints
                isValid = isNaN(parseInt(lastCheque.Branch())) || isNaN(parseInt(lastCheque.Account())) || isNaN(parseInt(lastCheque.ChequeNumber()));
            }
        }

        return !isValid;
    }

    self.isUndefinedOrWhitespaces = function (str) {
        if (typeof str === 'undefined') {
            // nothing
            return true;
        }
        if (typeof str !== 'string') {
            str = str.toString();
        }
        if (str.match(/^\s*$/)) {
            // nothing but whitespace
            return true;
        }

        return false;
    };

    self.removeCheque = function (item) {
        self.Cheques.remove(item);
    };

    //alert(cheque[0].Branch);
    self.TotalChequesPayment = ko.computed(function () {
        var total = 0.00;
        ko.utils.arrayForEach(self.Cheques(), function (cheque) {
            total += cheque.Sum();
        });
        return total;
    });
    //clear cheques when checkbox unchecked
    self.ChequePayment.subscribe(function () {
        self.Cheques.removeAll();
    });

    self.multipleCheques = ko.observable(false);

    self.futureChequeSum = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return self.multipleCheques()
            }
        },
        number: { params: true, message: "יש להזין ערך תקין" },
        validation: differentThanZero

    });
    self.multipleChequesNumber = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return self.multipleCheques()
            }
        },
        number: { params: true, message: "יש להזין ערך תקין" },
        min: { params: 0, message: "ערך מינימלי: 1" }

    });

    self.itemName = ko.observable();
    self.itemPrice = ko.observable();

    self.showMultipleCheques = function () {
        if (self.Cheques().length > 0) {
            var myChequesArray = self.Cheques;
            var lastCheque = myChequesArray.pop();
            myChequesArray.push(lastCheque);

            if (self.lastChequeIsValid(lastCheque)) {
                self.futureChequeSum(lastCheque.Sum());
                self.multipleChequesNumber(1);
                self.multipleCheques(true);
                $("#chequeDetailsEroorMsg").hide();
                $("#multipleCheques").modal('show');
            }
            else {
                $("#chequeDetailsEroorMsg").show();
            }
        }
        else {
            $("#chequeDetailsEroorMsg").show();
        }

    };
    self.hideMultipleCheques = function () {
        self.multipleCheques(false);
        $("#multipleCheques").modal('hide');
    };

    self.showEmptyItemLineModal = function () {
        self.itemName("");
        self.itemPrice(1);
        $("#itemNameError").hide();
        $("#itemPriceError").hide();
        $("#EmptyItemModal").modal('show');

    };

    self.hideEmptyItemLineModal = function () {
        var nameValidation = itemNameLength(self.itemName());
        var priceValidation = itemPriceCheck(self.itemPrice());

        if (nameValidation && priceValidation) {
            self.NoVAT($('.NoMham') && $('.NoMham').length > 0 && $('.NoMham')[0].checked);
            if ($('.NoMham') && $('.NoMham').length > 0 && $('.NoMham')[0].checked) {
                vatFactor = 1.0;
            }
            var listInFile = (self.DocumentTypeId() == 3 || self.DocumentTypeId() == 4) ? false : true;
            self.items.push(new billedItem(self.itemName(), "", 1, parseFloat(self.itemPrice()).toFixed(2), 0, 0, listInFile, null, 0, self.AddedBy(), self.AddedByUserId(), 0, 0, 0, 0, self.UpdateItemInVisit()));
            $("#EmptyItemModal").modal('hide');
            $('.OnlyPlus1').bind('input propertychange', function () {
                if (!isNumberOnlyPlus1($(this).val()))
                    $(this).val("");
            });
        }
    };

    /*-------------------------------------------------------------CREDIT CARD PAYMENT---------------------------------------------------*/
    self.firstSum = ko.observable(undefined);
    self.numPayments = ko.observable(1);
    self.furtherSum = ko.observable(undefined);
    self.disableCreditPayment = ko.observable(false);
    self.disableIssuerChange = ko.observable(false);

    /*--------------------- EasyCard payment -------------------------------*/
    self.EasyCard = ko.observable(false);
    self.EasyCardSum = ko.observable(0).extend({ validation: creditPositive });
    self.CardNumberLastFour = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return self.CreditPayment();
            }
        },
        number: { params: true, message: "יש להזין ערך תקין" },
        validation: lastFourDigits
    });

    self.CreditDealNumber = ko.observable();
    self.CreditValidDateMonth = ko.observable("").extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return self.CreditPayment() && !self.EasyCard();
            }
        },
        number: { params: true, message: "יש להזין ערך תקין" },
        min: { params: 1, message: "ערך מינימלי: 1" },
        max: { params: 12, message: "ערך מקסימלי: 12" }

    });
    self.CreditValidDateYear = ko.observable("").extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return self.CreditPayment() && !self.EasyCard();
            }
        },
        number: { params: true, message: "יש להזין ערך תקין" },
        min: { params: moment().year(), message: "ערך מינימלי: " + moment().year() },
        max: { params: moment().year(moment().year() + 20).year(), message: "ערך מקסימלי: " + moment().year(moment().year() + 20).year() }
    });
    self.CreditValidDate = ko.computed(function () {
        if (typeof (self.CreditValidDateMonth()) == "undefined" || typeof (self.CreditValidDateYear()) == "undefined")
            return "";
        var month = self.CreditValidDateMonth().length == 1 ? "0" + self.CreditValidDateMonth() : self.CreditValidDateMonth();
        var year = self.CreditValidDateYear().substring(2, 4);
        return month + "/" + year;
    });
    self.CreditOkNumber = ko.observable();
    self.EasyCardPaidSum = ko.observable(0);
    self.ManualCreditPayment = ko.observable(false);
    self.EasyCardDealId = ko.observable(0);
    self.EasyCardTerminalId = ko.observable(0);

    self.EasyCardTotal = ko.computed(function () {
        if (self.ManualCreditPayment()) {
            var sum = self.FirstCreditPaymentSum() + self.FurtherCreditPaymentsSum() * (self.NumOfCreditPayments() - 1);
            return isNaN(sum) ? 0 : sum.toFixed(2);
        }
        else {
            return self.EasyCardPaidSum().toFixed(2);
        }
    });

    self.EasyCardDealType = ko.observable();
    self.EasyCardPostponed = ko.observable(false);

    self.updateEasyCard = function (okNumber, cardNumber, dealNumber, total, dealtype, numOfPayments, firstPayment, furtherPayments, cardNameID, cardDate, dealId) {
        if (numOfPayments > 1) {
            self.EasyCardPostponed(true);
        }
        self.CreditOkNumber(okNumber);
        self.CardNumberLastFour(cardNumber);
        self.CreditDealNumber(dealNumber);
        self.EasyCardPaidSum(parseFloat(total));
        self.EasyCardDealType(dealtype);//TODO :: add validation date

        for (var i = 0 ; i < self.CreditPaymentTypeOptions().length; i++) {
            if (dealtype == self.CreditPaymentTypeOptions()[i].Name()) {
                self.selectDealType(self.CreditPaymentTypeOptions()[i]);
            }
        }

        for (var i = 0 ; i < self.CreditCardCodeOptions().length; i++) {
            if (cardNameID.trim() == self.CreditCardCodeOptions()[i].Text()) {
                self.CreditCardCodeId(self.CreditCardCodeOptions()[i].Value());
            }
        }

        self.NumOfCreditPayments(parseFloat(numOfPayments));
        self.FirstCreditPaymentSum(parseFloat(firstPayment));
        self.FurtherCreditPaymentsSum(parseFloat(furtherPayments));
        self.disableCreditPayment(true);
        self.disableIssuerChange(true);
        var validDate = cardDate.split('/');

        self.CreditValidDateMonth(validDate[0]);
        self.CreditValidDateYear("20" + validDate[1]);

        self.EasyCardDealId(dealId);
        self.EasyCardTerminalId(self.terminalObject().TerminalId());
    };
    /*---------------------Regular payment-----------------------------------*/
    self.CreditSlipNumber = ko.observable();
    self.creditPaymentTypeObject = ko.observable();
    self.terminalObject = ko.observable();

    self.terminalObjectId = ko.computed(function () {
        if (typeof self.terminalObject() === 'undefined') {
            return 0;
        }
        return self.terminalObject().Id();
    });

    self.selectDefault = function (option, item) {
        if (item.IsDefault()) {
            self.terminalObject(item);
        }
    };

    self.selectDealType = function (type) {
        self.creditPaymentTypeObject(type);
    };

    self.CreditPaymentType = ko.computed(function () {
        if (typeof self.creditPaymentTypeObject() === 'undefined') {
            return 0;
        }
        return self.creditPaymentTypeObject().Id();
    });

    self.CreditPaymentTypeName = ko.computed(function () {
        if (typeof self.creditPaymentTypeObject() === 'undefined') {
            return 0;
        }
        return self.creditPaymentTypeObject().Name();
    });

    self.CreditPaymentPostponed = ko.computed(function () {
        if (typeof self.creditPaymentTypeObject() === 'undefined') {
            return self.EasyCardPostponed();
        }
        else {
            if (self.creditPaymentTypeObject().IgnorePayments()) {
                return false;
            }
            else {
                return self.creditPaymentTypeObject().PostponedPayment();
            }
        }
    });

    self.firstPaymentText = ko.computed(function () {
        if (typeof self.creditPaymentTypeObject() === 'undefined') {
            return 'סכום תשלום ראשון';
        }
        return self.creditPaymentTypeObject().PostponedPayment() ? 'סכום תשלום ראשון' : 'סכום לתשלום';
    });

    //self.onePayment = ko.computed(function () {
    //    return !self.CreditPaymentPostponed();
    //});

    self.FirstCreditPaymentSum = ko.computed({
        read: function () {
            return self.firstSum();
        },
        write: function (value) {
            self.firstSum(isNaN(value) ? 0 : parseFloat(value));
        }
    }).extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return self.CreditPayment();// && !self.EasyCard();
            }
        },
        //   number: { params: true, message: "יש להזין ערך תקין" },
        validation: {
            validator: function (val) {
                if (!self.CreditPayment()) {
                    return true;
                }
                else {
                    var parsed = parseFloat(val);
                    return parsed != 0;
                }
            },
            message: "יש להכניס סכום חוקי"
        }
    });

    self.NumOfCreditPayments = ko.computed({
        read: function () {
            //if (self.onePayment()) {
            //    return 1;
            //}
            return self.numPayments();
        },
        write: function (value) {
            self.numPayments(parseFloat(value));
        }
    }).extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return self.CreditPayment() && self.CreditPaymentPostponed();
            }
        },
        //  number: { params: true, message: "יש להזין ערך תקין" },
        validation: creditPositive
    });
    //.extend({ validation: creditPositive });

    self.FurtherCreditPaymentsSum = ko.computed({
        read: function () {
            //if (self.onePayment()) {
            //    return 0;
            //}
            return self.furtherSum();
        },
        write: function (value) {
            self.furtherSum(parseFloat(value));
        }
    }).extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return self.CreditPayment() && !self.EasyCard();
            }
        },
        //   number: { params: true, message: "יש להזין ערך תקין" },
        validation: {
            validator: function (val) {
                if (!self.CreditPayment() || !self.CreditPaymentPostponed()) {
                    return true;
                }
                else {
                    var parsed = parseFloat(val);
                    return parsed != 0;
                }
            },
            message: "יש להכניס סכום חוקי"
        }
    });

    self.CreditCardCodeId = ko.observable();

    /*---------------------------------total-----------------------------*/
    self.TotalCreditPaymentSum = ko.computed(function () {
        if (self.EasyCard()) {
            return self.EasyCardPaidSum();
        } else {
            var sum = self.FirstCreditPaymentSum() + self.FurtherCreditPaymentsSum() * (self.NumOfCreditPayments() - 1);
            return isNaN(sum) ? "" : sum;
        }
    });

    //clear fields when checkbox unchecked
    self.CreditPayment.subscribe(function () {
        if (self.CreditPayment()) {
            self.FirstCreditPaymentSum(0);
            self.FurtherCreditPaymentsSum(0);
            self.CardNumberLastFour("");
        } else {
            self.FirstCreditPaymentSum(undefined);
            self.FurtherCreditPaymentsSum(undefined);
            self.CardNumberLastFour(undefined);
        }

        self.CreditSlipNumber(null);
        self.NumOfCreditPayments(0);
        self.CreditCardCodeId(0);
        if (!self.EasyCard()) {
            self.CreditOkNumber(null);
        }
        self.CreditValidDateMonth('');
        self.CreditValidDateYear('');

    });

    /*-------------------------------------------------------------BANK TRANSFER PAYMENT------------------------------------------------------*/
    self.BankTransfers = ko.observableArray();

    self.addBankTransfer = function () {
        self.BankTransfers.push(new bankTransfer());
    };

    self.removeBankTransfer = function (item) {
        self.BankTransfers.remove(item);
    };

    self.BankTransferTotal = ko.computed(function () {
        var total = 0.00;
        ko.utils.arrayForEach(self.BankTransfers(), function (transfer) {
            total += transfer.Total();
        });
        return total;
    });
    //clear fields when checkbox unchecked
    self.BankTransfer.subscribe(function () {
        self.BankTransfers.removeAll();
    });

    var oldClientId;
    /*-----------------------------------------------------------Load billed items ------------------------------------------*/
    self.ClientId.subscribe(function (newId) {
        if (newId == 0 || typeof self.ClientId() == "undefined") {
            self.ClientName("");
            self.ClientAddress("");
            self.ClientPhone("");
            self.SelectedTariff(defaultTariff);
        }

        if (typeof newId === 'undefined' || newId < 0) {
            self.CasualClient(false);
            return;
        }


        if (newId.toString() == oldClientId) {
            return;
        }

        oldClientId = newId;

        ShowLoder();
        var qs = {
            clientId: newId
        };

        $.get('GetClientDetails', $.param(qs), function (data) {
            if (id > 0 && newId == id) {
                self.ClientNameWithPatients(data.Name);
                $("#client-search").val(data.Name);
            }
            self.ClientName(data.Name);
            self.ClientAddress(data.Address);
            self.ClientPhone(data.Phone);
            self.ClientBalance(data.Balance);
            self.SelectedTariff(data.DefaultTariffId);
            //ko.mapping.fromJS(data.OpenInvoices, self.OpenInvoices);
            if (data.OpenInvoices != null) {
                var mappedInvoice = $.map(data.OpenInvoices, function (item) { return new openInvioce(item, parent); });
                self.OpenInvoices(mappedInvoice);
            }
            //save all items with AllowUpdate==true(these are manually added items)
            var temp = new Array();
            ko.utils.arrayForEach(self.items(), function (item) {
                if (item.AllowUpdate()) {
                    temp.push(item);
                }
            });
            //get billed items
            if (quotation == 0) {
                $.get('/FinanceDocument/GetBilledItems/', $.param(qs), function (data) {
                    var mapped = $.map(data.items, function (item) {
                        return new billedItem(item.Description, item.CatalogNumber, item.Quantity, item.UnitPrice, item.Discount,
                            item.VisitId, false, item.PriceListItemId, item.DiscountPercentage,
                            item.AddedBy, item.AddedByUserId, item.ItemExaminationID, item.ItemMedicationID, item.ItemPreventiveMedicineID, item.ItemTreatmentID, true);
                    });
                    self.items(mapped);
                    //self.GlobalDiscount(data.discount);
                    //self.GlobalDiscountIsPercent(false);
                    ko.utils.arrayForEach(temp, function (item) {
                        self.items.push(item);
                    });
                    hideLoder();
                });
            } else {
                var params = {
                    id: quotation
                };
                $.get('/FinanceDocument/GetQuotationItems', $.param(params), function (data) {
                    var mapped = $.map(data.QuotationItems, function (item) {
                        return new billedItem(item.Description, item.CatalogNumber, item.Quantity, item.UnitPrice, item.Discount, item.VisitId, false, item.PriceListItemId, item.DiscountPercentage, data.AddedBy, data.AddedByUserId, 0, 0, 0, 0, true);
                    });
                    self.items(mapped);
                    ko.utils.arrayForEach(temp, function (item) {
                        self.items.push(item);
                    });
                    self.GlobalDiscount(data.GlobalDiscount);
                    self.GlobalDiscountIsPercent(false);
                    hideLoder();
                });
            }
        });

    });

    self.CasualClient.subscribe(function (val) {
        if (val) {
            //    clearClientSearch();
            self.ClientId(0);
            self.ClientNameWithPatients("");
            $("#client-search").val("");
            $("#no-results").hide();
        }

    });

    /*---------------------------------------------------------------------TOTAL PAYMENT--------------------------------------------------------*/
    self.TotalSumPaid = ko.computed(function () {
        var total = 0;
        if (self.CashPayment()) {
            total += self.CashPaymentSum();
        }
        if (self.CreditPayment()) {
            if (self.ManualCreditPayment()) {
                total += isNaN(self.EasyCardTotal()) ? 0 : parseFloat(self.EasyCardTotal());
            } else {
                total += self.TotalCreditPaymentSum();
            }
        }
        if (self.BankTransfer()) {
            total += self.BankTransferTotal();
        }
        if (self.ChequePayment()) {
            total += self.TotalChequesPayment();
        }

        var totalSum = parseFloat(total);
        return isNaN(totalSum) ? "" : totalSum.toFixed(2);
    });

    /*---------------------------------------------------------------PRICE LIST --------------------------------------------------------*/
    //self.priceListShown = ko.observable(false);
    self.showPriceList = function () {
        $('#priceListModal').modal('show');// self.priceListShown(true);//data-bind="style: { display: priceListShown() ? 'inline' : 'none'}"
    };
    self.hidePriceList = function () {
        $('#priceListModal').modal('hide');//self.priceListShown(false);
    };
    //onchange="SetCheckChangeNoMham();"
    self.priceListItems = ko.mapping.fromJS([]);

    var orgVat = vatFactor;
    $('.NoMham').change(function () {
        if (vatFactor != 1.0) {
            orgVat = vatFactor;
        }
        if ($('.NoMham') && $('.NoMham').length > 0 && $('.NoMham')[0].checked) {
            vatFactor = 1.0;
        }
        else {
            vatFactor = orgVat;
        }
        
        self.NoVAT($('.NoMham') && $('.NoMham').length > 0 && $('.NoMham')[0].checked);
        self.items.removeAll();
        self.UpdatePriceListItems();
    });
    self.addItemFromPriceList = function (item) {
        if ($('.NoMham') && $('.NoMham').length && $('.NoMham')[0].checked) {
            self.NoVAT(item.NoVAT());
            vatFactor = 1.0;
            var listInFile = (self.DocumentTypeId() == 3 || self.DocumentTypeId() == 4) ? false : true;
            self.items.push(new billedItem(item.Name(), item.Catalog(), 1, item.Price(), 0, 0, listInFile, item.Id(), 0, self.AddedBy(), self.AddedByUserId(), 0, 0, 0, 0, self.UpdateItemInVisit()));
        }
        else {
            self.NoVAT(item.NoVAT());
            var listInFile = (self.DocumentTypeId() == 3 || self.DocumentTypeId() == 4) ? false : true;
            self.items.push(new billedItem(item.Name(), item.Catalog(), 1, item.Price(), 0, 0, listInFile, item.Id(), 0, self.AddedBy(), self.AddedByUserId(), 0, 0, 0, 0, self.UpdateItemInVisit()));
        }

        setCookieMessage("successmsg", encodeURIComponent("הטיפול נוסף בהצלחה"), 1, 0, 0);
        getCookieMessages();
        //  self.hidePriceList();
    };

    //search
    self.priceListCategories = ko.mapping.fromJS([]);

    self.selectedCategoryId = ko.observable(null);
    self.searchQuery = ko.observable(null);

    self.clearSearch = function () {
        self.selectedCategoryId(null);
        self.searchQuery(null);
    };

    self.priceListOptions = ko.computed(function () {
        var options = self.priceListItems();
        if (self.selectedCategoryId() > 0) {
            options = ko.utils.arrayFilter(options, function (item) {
                if (item.CategoryId() == self.selectedCategoryId()) {
                    return item;
                }
                return false;
            });
        }
        if (self.searchQuery() != null) {
            if (self.searchQuery() != ' ' && self.searchQuery().length > 0) {
                options = ko.utils.arrayFilter(options, function (item) {
                    //name
                    if (item.Name().indexOf(self.searchQuery()) > -1) {
                        return item;
                    }
                    //barcode
                    if (typeof item.Barcode !== 'undefined' && item.Barcode() != null) {
                        if (item.Barcode().indexOf(self.searchQuery()) > -1) {
                            return item;
                        }
                    }
                    return false;
                });
            }
        }
        return options;

    });

    /*-------------------------------------------------------------- INVOICE TOTAL --------------------------------------------------------*/
    self.glblDiscount = ko.observable(0);
    self.roundTotal = ko.observable(false);
    self.VAT = ko.observable(vatFactor);
    self.NoVAT = ko.observable(false);
    self.TaxRateCopy = ko.observable(taxRate);
    self.TaxRate = ko.computed(function () {
        if (self.NoVAT()) {
            return 0;
        }
        else {
            return self.TaxRateCopy();
        }
    });
    self.VatFactor = ko.computed(function () {
        if (self.NoVAT()) {
            vatFactor = 1;
            return 1;
        } else {
            return vatFactor;
        }
    });

    self.TotalItemsForReceipt = ko.computed(function () {
        var total = 0.00;
        ko.utils.arrayForEach(self.items(), function (item) {
            total += item.TotalAfterVAT();
        });
        return total;
    });

    self.TotalBeforeVAT = ko.computed(function () {
        /*---------receipt only ----*/
        if (self.isReceipt()) {
            return self.TotalSumPaid() / self.VatFactor();
        }
        /*---------else -----------*/
        var total = 0.00;

        ko.utils.arrayForEach(self.items(), function (item) {
            try {
                if (VatFactor == 1.0) {
                    total += item.TotalAfterVAT();
                }
                else {
                    total += item.TotalBeforeVAT();
                }
            }
            catch (e) {
                total += item.TotalBeforeVAT();
            }
        });


        return total;//parseFloat(total.toFixed(2));
    });


    self.GlobalDiscountIsPercent = ko.observable(false);
    self.GlobalDiscount = ko.computed({
        read: function () {
            return self.glblDiscount();
        },
        write: function (newValue) {
            var parsedValue = parseFloat(newValue);
            self.glblDiscount(isNaN(parsedValue) ? newValue : parsedValue.toFixed(2));
        }
    }).extend({
        validation: {
            validator: function (val) {
                if (self.GlobalDiscountIsPercent()) {
                    return percentValidation.validator(val);
                }
                else {
                    if (self.DocumentTypeId() == 1) {
                        return true;
                    }
                    return !isNaN(val);//val >= 0 && val <= Math.abs(self.TotalBeforeVAT() * self.VatFactor());
                }
            },
            message: "יש להכניס סכום חוקי"
        }
    });


    if (window.location.href.indexOf("/Receipt") > -1) {
        self.TotalAfterDiscount = ko.computed(function () {

            //var total = self.TotalBeforeVAT();//0.00;
            var total = 0.00;
            //  total = total * self.VAT();
            ko.utils.arrayForEach(self.items(), function (item) {
                total += item.TotalAfterVAT();
            });

            var val;

            if (self.GlobalDiscountIsPercent()) {
                val = total * (1.00 - self.GlobalDiscount() / 100);
            } else {
                val = total - self.GlobalDiscount();
            }
            return val;

        });
    }
    else {

        self.TotalAfterDiscount = ko.computed(function () {

            //var total = self.TotalBeforeVAT();//0.00;
            var total = 0.00;
            //  total = total * self.VAT();
            ko.utils.arrayForEach(self.items(), function (item) {
                total += item.TotalBeforeVAT();
            });

            var val;

            if (self.GlobalDiscountIsPercent()) {
                val = total * (1.00 - self.GlobalDiscount() / 100);
            } else {
                val = total - self.GlobalDiscount();
            }
            return val;

        });
    }
    self.TotalToPay = ko.computed(function () {
        var val;

        val = self.TotalAfterDiscount() * self.VatFactor();

        /// ? ///
        if (self.Receipt() && !self.Invoice()) {
            if (self.RelatedDocuments().length > 0) {

                val = self.SelectedOpenInvoicesSum() + self.TotalItemsForReceipt();
            } else {
                val = self.TotalItemsForReceipt();
            }
        }
        //////////

        if (self.roundTotal()) {
            return Math.round(val + self.RoundingFactor() * self.VatFactor());
        }


        // return parseFloat(parseFloat(val.toFixed(2)) + parseFloat((self.RoundingFactor() * self.VatFactor()).toFixed(2))).toFixed(2);
        return val;
    }).extend({
        validation: totalValidation
    });



    /*----------------------------------------------------------------Validation-------------------------------------------------------*/
    self.validate = function () {
        var errors = ko.validation.group(viewModel, { deep: true });
        errors.showAllMessages(true);
        return errors().length == 0;
    };

    self.submitValidate = function () {
        if (self.DocumentTypeId() == 4 || self.DocumentTypeId() == 0 || self.DocumentTypeId() == 3) //refund or invoice or proforma
            return true;
        //  if (/*self.Invoice() &&*/ self.Receipt()) {
        return self.TotalToPay().toFixed(2) == self.TotalSumPaid() && self.TotalSumPaid() != 0;
        //  }
        //  return true;
    };

    /*---------------------------------------------------------------------Save--------------------------------------------------------*/
    self.save = function () {
        if ($('.NoMham') && $('.NoMham').length > 0 && $('.NoMham')[0].checked) {
            taxRate = 0;
            self.TaxRate = ko.observable(0);
            self.VAT = ko.observable(1);
            vatFactor = 1.0;
            self.NoVAT(true);
            SetTotalBeforeVATMain();
        }
        try{
            self.TotalBeforeVAT = self.TotalBeforeVAT();
        }
        catch(e)
        {
        }

        if (self.validate()) {
            if (self.submitValidate()) {
                for (var i = 0; i < self.items().length; i++) {
                    if (taxRate == 0) {
                        self.items()[i].TotalBeforeVAT = self.items()[i].TotalAfterVAT();
                        self.items()[i].UnitPrice = self.items()[i].TotalAfterVAT();
                    }
                    
                    if (self.items()[i].TotalAfterVAT() == 0 && isEmptyOrWhiteSpaces(self.items()[i].Description())) {
                        self.removeItem(self.items()[i]);
                        i--;
                    }
                }
                ShowLoder();

                if (self.ManualCreditPayment()) {
                    self.EasyCardDealType(self.CreditPaymentTypeName());
                    self.EasyCardPaidSum(self.EasyCardTotal());
                }
                var json = ko.toJSON(self);
                //$.post('SaveDocument', { docJson: json }, function (data) {
                //    window.location = data.redirect;
                //});
                $.ajax({
                    url: "/FinanceDocument/SaveDocument",
                    type: "POST",
                    data: { docJson: json },
                    success: function (data) {
                        window.open(data.redirect, '_blank');
                        parent.window.location.href = data.redirectToClient;
                    },
                    error: function (data) {
                    }
                });
            } else {
                if (self.TotalToPay() == 0) {
                    alert("לא ניתן להנפיק מסמך בסכום 0");
                }
                else if (self.ClientId() == "0") {
                    alert("על הסכום ששולם להיות זהה לסכום המחויב");
                }
                else if (self.TotalSumPaid() != 0) {
                    if (parseFloat(self.GlobalDiscount()) == 0) {
                        $('#partialPaymentModel').modal('show');
                    }
                    else {
                        alert("סכום התשלום אינו תואם לסכום הפריטים. לא ניתן לבצע תשלום חלקי בשילוב הנחה כללית.");
                    }
                }
                else {
                    alert("חובה להזין אמצעי תשלום");
                }
                hideLoder();
            }
        }
    };
    /* This function is for generating 2000 records for the tex auth */
    self.saveCounter = 0;
    self.saveMultipalTimes = function () {
        self.saveCounter++;
        if (self.validate() & self.saveCounter < 510) {
            if (self.submitValidate()) {
                ShowLoder();
                var json = ko.toJSON(self);
                $.post('SaveDocument', { docJson: json }, function (data) {
                    self.saveMultipalTimes();
                });
            }
            else if (self.counter >= 510) {
                window.location = data.redirect;
            }
            else {
                alert("על הסכום ששולם להיות זהה לסכום המחויב");
                hideLoder();

            }
        }
    };

    self.partialPaymentActive = ko.computed(function () {
        var isPartialPayment = false;

        for (var i = 0 ; i < self.items().length; i++) {
            if (debtDescription == self.items()[i].Description()) {
                isPartialPayment = true;
            }
        }

        if (isPartialPayment) {
            self.glblDiscount(0);
            self.GlobalDiscountIsPercent(false);
        }

        return isPartialPayment;
    });

    self.partialPayment = function () {

        self.items.push(new billedItem(debtDescription, "", 1, (self.TotalAfterDiscount() * self.VatFactor() - self.TotalSumPaid()) * (-1), 0, 0, false, 0, 0, "", 0, 0, 0, 0, 0, true));

        $('#partialPaymentModel').modal('hide');
    }




    self.roundingFactor = ko.computed(function () {
        if (self.roundTotal()) {
            if (self.GlobalDiscountIsPercent()) {
                self.GlobalDiscount(self.TotalBeforeVAT() - (self.TotalBeforeVAT() * (1.00 - self.GlobalDiscount() / 100)));
                self.GlobalDiscountIsPercent(false);
            }
            var total = parseFloat(((self.TotalBeforeVAT() - parseFloat(self.GlobalDiscount())) * self.VatFactor()).toFixed(2));
            var round = Math.round(total);
            var VatPayed = parseFloat((round - (round / self.VatFactor())).toFixed(2)); //vat payed
            var roundString = ((round - total) / self.VatFactor()).toString();
            self.RoundingFactor(round - VatPayed - total / self.VatFactor());
            self.RoundingFactorForDisplay(self.RoundingFactor().toFixed(2));//(parseFloat(roundString.substring(0, roundString.lastIndexOf('.') + 3)));
        }
        else {
            self.RoundingFactor(0.00);
            self.RoundingFactorForDisplay(0.00);
        }
    });

    /*---------------------------------------------------------------------INIT--------------------------------------------------------*/

    var params = {
        //clientId: id,
        documentType: self.DocumentTypeId(),
        issuerId: self.IssuerId() ? self.IssuerId() : defaultIssuerId
    };

    $.get('/FinanceDocument/GetNewInvoiceReceipt', $.param(params), function (data) {
               
        ko.mapping.fromJS(data.DoctorOptions, self.DoctorOptions);
        //  ko.mapping.fromJS(data.ClientOptions, self.ClientOptions);
        ko.mapping.fromJS(data.IssuerOptions, self.IssuerOptions);
        ko.mapping.fromJS(data.BankCodeOptions, self.BankCodeOptions);
        ko.mapping.fromJS(data.CreditCardCodeOptions, self.CreditCardCodeOptions);
        ko.mapping.fromJS(data.TariffOptions, self.TariffOptions);
        self.SelectedTariff(defaultTariff);
        self.UpdatePriceListItems();
        //if (data.CreditCardCodeOptions.length == 0) {
        //    self.disableCreditPayment(true);
        //    alert("לא הוגדרו כרטיסי אשראי למרפאה  , לא ניתן לחייב באשראי");
        //} else {
        //    self.disableCreditPayment(false);
        //}
        self.SelectedTariff.subscribe(function (newValue) {
            if (typeof (newValue) !== "undefined") {
                if (newValue.toString() != oldTarrifId) {
                    self.UpdatePriceListItems();
                    oldTarrifId = newValue.toString();
                }
            }
        });

        self.AddedBy(data.AddedBy);
        self.AddedByUserId(data.AddedByUserId);

        if (id > 0) {
            self.ClientId(id);
        }
        self.IssuerId(defaultIssuerId);

        setTimeout(function () {
            for (var i = 0; i < data.DoctorOptions.length; i++) {
                if (data.DoctorOptions[i].Selected) {
                    $('.ddlDoctorId').val(data.DoctorOptions[i].Value);
                    break;
                }
            }
        }, 500);
        
    });
}

function creditUpdateFinish(okNumber, cardNumber, dealNumber, total, dealtype, numOfPayments, firstPayment, furtherPayments, cardNameID, cardDate, dealId) {
    numOfPayments = parseFloat(numOfPayments);
    if (isNaN(numOfPayments) || firstPayment == "") {
        numOfPayments = 0;
        firstPayment = total;
    }
    numOfPayments = numOfPayments + 1;

    if (isNaN(furtherPayments) || furtherPayments == "") {
        furtherPayments = 0;
    }
    viewModel.updateEasyCard(okNumber, cardNumber, dealNumber, total, dealtype, numOfPayments, firstPayment, furtherPayments, cardNameID, cardDate, dealId);
}
function SetTotalBeforeVATMain() {
    if (window.location.href.indexOf("/Receipt") > -1) {
        $('.TotalBeforeVAT').text($('.TotalAfterVAT').text());
    }
}