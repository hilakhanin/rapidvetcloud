﻿var vm;
function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

function clearClientSearch(element) {
    vm.clientId(undefined);
    vm.ClientNameWithPatients("");   
    element.value = "";
    $(".no-results").hide();
};

$(function () {

    $(".client-search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Clients/Search",
                type: "POST",
                data: {
                    query: request.term
                },
                success: function (data) {
                    response(data);

                    if (data.length == 0) {
                        $(".no-results").show();
                        setTimeout(function () { $(".no-results").hide(); }, 5000);
                    }
                    else {
                        $(".no-results").hide();
                    }
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            event.preventDefault();
            vm.ClientNameWithPatients(ui.item.label);
            vm.clientId(ui.item.value.toString());
            this.value = ui.item.label;

            //window.location = "/Clients/Patients/" + ui.item.value;
        },
        focus: function (event, ui) {
            event.preventDefault();
            this.value = ui.item.label;
        }
        ,
        change: function (event, ui) {
            event.preventDefault();
            if (isEmptyOrWhiteSpaces(this.value) ||
                typeof vm.clientId() == "undefined" || vm.ClientNameWithPatients() != this.value) {
                clearClientSearch(this);
            }

        },
        open: function () {
            $(".ui-autocomplete").css("max-height", 120);
            $(".ui-autocomplete").css("max-width", 225);
            $(".ui-autocomplete").css("overflow-y", "auto");
            $(".ui-autocomplete").css("overflow-x", "hidden");

        }
    });
});

$(function () {

    function viewModel() {

        var self = this;
        vm = self;
        self.documents = ko.mapping.fromJS([]);

        //filters

        self.issuers = ko.mapping.fromJS([]);
        self.issuerId = ko.observable(undefined).extend({ required: { message: "שדה חובה" } });

        self.documentTypes = ko.mapping.fromJS([]);
        self.documentTypeId = ko.observable(undefined).extend({ required: { message: "שדה חובה" } });

        self.clients = ko.mapping.fromJS([]);
        self.clientId = ko.observable();
        self.ClientNameWithPatients = ko.observable("");

        self.totalToPay = ko.observable();
        self.chequeNumber = ko.observable('');
        self.documentSerial = ko.observable();
        self.queryError = ko.observable('');

        self.Errors = ko.validation.group(self);

        //validation
        self.isValid = function () {
            if (self.Errors().length == 0) {
                return true;
            }
            else {
                self.Errors.showAllMessages(true);
                return false;
            }
        };

        //functions
        //FinanceMetaData

        self.getIssuers = function () {
            $.getJSON('/FinanceMetaData/GetClinicIssuers', function (data) {
                ko.mapping.fromJS(data, self.issuers);
            });
        };

        self.getClients = function () {
            $.getJSON('/Clinics/GetClinicClients', function (data) {
                //add 0 value for walk-in customers
                var walkIn = { Text: 'לקוח מזדמן', Value: 0, Selected: false };
                data.unshift(walkIn);
                ko.mapping.fromJS(data, self.clients);
            });
        };

        self.getDocumentTypes = function () {
            $.getJSON('/FinanceMetaData/GetFinanceDocumentTypes', function (data) {
                ko.mapping.fromJS(data, self.documentTypes);
            });
        };


        self.filtersInit = function () {

            ShowLoder();

            self.getIssuers();
          //  self.getClients();
            self.getDocumentTypes();

            hideLoder();

        };

        self.search = function () {
            if (self.Errors().length == 0 && self.isQueryValid()) {
                ShowLoder();

                var url = '/FinanceDocumentDetection/Search' +
                    '?issuerId=' + self.issuerId() +
                    '&&documentTypeId=' + self.documentTypeId() +
                    '&&documentSerial=' + self.documentSerial() +
                    '&&clientId=' + self.clientId() +
                    '&&totalToPay=' + self.totalToPay() +
                    '&&chequeNumber=' + self.chequeNumber();

                $.getJSON(url, function (data) {
                    ko.mapping.fromJS(data, self.documents);
                }).done(function () {
                    hideLoder();
                });
            }
            else {
                self.Errors.showAllMessages(true);
            }
        };

        self.isQueryValid = function () {
            if(!isEmptyOrWhiteSpaces(self.documentSerial()) || !isEmptyOrWhiteSpaces(self.chequeNumber()) 
                || !isEmptyOrWhiteSpaces(self.totalToPay()) || typeof self.clientId() !== "undefined")
            {
                self.queryError('');
                return true;
            }
            else
            {
                self.queryError('נא להזין ערך לחיפוש באחד מהשדות');
                return false;
            }
        }

        //execution

        self.filtersInit();
    }

    function isEmptyOrWhiteSpaces(str) {
        return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null; //str.match(/^$|\s+/) !== null;
    }

    ko.applyBindings(new viewModel());

});