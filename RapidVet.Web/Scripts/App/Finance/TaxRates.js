﻿var viewModel;

ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datepickerOptions || { dateFormat: 'dd/mm/yy' };
        $(element).datepicker(options);

        //handle the field changing
        ko.utils.registerEventHandler(element, "change", function () {
            var observable = valueAccessor();
            observable($(element).datepicker("getDate"));
            $(element).datepicker(valueAccessor());
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).datepicker("destroy");
        });

    },
    //update the control when the view model changes
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var value = ko.utils.unwrapObservable(valueAccessor()),
            current = $(element).datepicker("getDate");

        if (value - current !== 0) {
            $(element).datepicker("setDate", value);

        }
    }
};

function Rate(data) {
    var self = this;
    self.id = ko.observable(data.Id);
    self.date = ko.observable(data.Date).extend({ required: { params: true, message: "שדה חובה" } });
    self.oldDate = ko.observable(data.Date);
    self.dateChange = ko.computed(function () {
        return self.date() != self.oldDate();
    });

    self.rate = ko.observable(data.Rate).extend({ required: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 0, message: "ערך מינימום: 0" } })
        .extend({ max: { params: 99, message: "ערך מקסימלי: 99" } });;
    self.oldRate = ko.observable(data.Rate);
    self.rateChenge = ko.computed(function () {
        return self.rate() != self.oldRate();
    });

    self.pendingChanges = ko.computed(function () {
        var result = false;
        if (self.dateChange() == true || self.rateChenge() == true) {
            result = true;
        }

        return result;
    });

    self.validate =function () {
        var errors = ko.validation.group(self, { deep: false });
        errors.showAllMessages(true);
        return errors().length == 0;
    };
}

function viewModel() {

    var self = this;
    self.rates = ko.observableArray([]);
    self.date = ko.observable().extend({ required: { params: true, message: "שדה חובה" } });
    self.rate = ko.observable()
        .extend({ required: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 0, message: "ערך מינימום: 0" } })
        .extend({ max: { params: 99, message: "ערך מקסימלי: 99" } });



    //validation
    self.isValid = function () {
        var errors = ko.validation.group(viewModel, { deep: false });
        errors.showAllMessages(true);
        return errors().length == 0;
    };

    //functions
    self.getData = function () {
        ShowLoder();

        $.getJSON('/Clinics/GetTaxRatesData', function (data) {
            self.refreshRateList(data);
        }).done(function () {
            hideLoder();
        });
    };

    self.submitForm = function() {
        if (self.isValid()) {
            $('#createRate').submit();
        }
    };
    
    self.createRate = function () {
        if (self.isValid()) {
            ShowLoder();

            $.post('/Clinics/CreateTaxRate', { date: self.date(), rate: self.rate() }, function (data) {
                self.date('');
                self.rate('');
                self.refreshRateList(data);
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        } else {
            //$('#formAlert').show();
        }

    };

    self.updateRate = function (element) {
        if (element.validate()) {
            ShowLoder();

            if (element.date() != element.oldDate()) {
                date = self.getDate(element.date());
            }
            else {
                date = element.date();
            }
            
            $.post('/Clinics/UpdateTaxRate', { id: element.id(), date: date, rate: element.rate() }, function (data) {
                self.refreshRateList(data);
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        }
    };


    self.delete = function (element) {
        ShowLoder();
        $.post('/Clinics/DeleteTaxRate/' + element.id(), function (data) {
            if (data) {
                self.rates.remove(element);
            }
        }).done(function () {
            hideLoder();
            getCookieMessages();
        });
    };

    self.getDate = function (data) {
        var date = new Date(data);
        return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    };



    self.refreshRateList = function (data) {
        self.rates([]);
        var mapped = $.map(data, function (item) {
            return new Rate(item);
        });
        self.rates(mapped);
    };


    //execution
    self.getData();
}

$(function () {

    ko.validation.init({
        decorateElement: true,
        decorateInputElement: true,
        errorElementClass: 'input-validation-error',

    });
    viewModel = new viewModel();
    ko.applyBindings(viewModel);

});