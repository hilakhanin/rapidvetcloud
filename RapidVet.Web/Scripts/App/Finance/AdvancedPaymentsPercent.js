﻿var viewModel;

ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datepickerOptions || { dateFormat: 'dd/mm/yy' };
        $(element).datepicker(options);

        //handle the field changing
        ko.utils.registerEventHandler(element, "change", function () {
            var observable = valueAccessor();
            observable($(element).datepicker("getDate"));
            $(element).datepicker(valueAccessor());
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).datepicker("destroy");
        });

    },
    //update the control when the view model changes
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var value = ko.utils.unwrapObservable(valueAccessor()),
            current = $(element).datepicker("getDate");

        if (value - current !== 0) {
            $(element).datepicker("setDate", value);

        }
    }
};

function Percent(data) {
    var self = this;
    self.id = ko.observable(data.Id);
    self.date = ko.observable(data.Date).extend({ required: { params: true, message: "שדה חובה" } });
    self.oldDate = ko.observable(data.Date);
    self.dateChange = ko.computed(function () {
        return self.date() != self.oldDate();
    });

    self.percent = ko.observable(data.Percent).extend({ required: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 0, message: "ערך מינימלי: 0" } })
        .extend({ max: { params: 100, message: "ערך מקסימלי: 100" } });;
    self.oldPercent = ko.observable(data.Percent);
    self.percentChenge = ko.computed(function () {
        return self.percent() != self.oldPercent();
    });
    self.issuerId = ko.observable(data.IssuerId);
    self.pendingChanges = ko.computed(function () {
        var result = false;
        if (self.dateChange() == true || self.percentChenge() == true) {
            result = true;
        }

        return result;
    });

    self.validate =function () {
        var errors = ko.validation.group(self, { deep: false });
        errors.showAllMessages(true);
        return errors().length == 0;
    };
}

function viewModel() {

    var self = this;
    self.percents = ko.observableArray([]);
    self.date = ko.observable().extend({ required: { params: true, message: "שדה חובה" } });
    self.percent = ko.observable()
        .extend({ required: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 0, message: "ערך מינימלי: 0" } })
        .extend({ max: { params: 100, message: "ערך מקסימלי: 100" } });

    self.issuerId = ko.observable($('#issuerId').val());

    //validation
    self.isValid = function () {
        var errors = ko.validation.group(viewModel, { deep: false });
        errors.showAllMessages(true);
        return errors().length == 0;
    };

    //functions
    self.getData = function () {
        ShowLoder();

        $.getJSON('/Clinics/GetAdvancedPaymentPercentsData/' + self.issuerId(), function (data) {
            self.refreshPercentList(data);
        }).done(function () {
            hideLoder();
        });
    };

    self.submitForm = function() {
        if (self.isValid()) {
            $('#createPercent').submit();
        }
    };
    
    self.createPercent = function () {
        if (self.isValid()) {
            ShowLoder();

            $.post('/Clinics/CreateAdvancedPaymentPercent', { IssuerId: self.issuerId(), Date: self.date(), Percent: self.percent() }, function (data) {
                self.date('');
                self.percent('');
                self.refreshPercentList(data);
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        } else {
            //$('#formAlert').show();
        }

    };

    self.updatePercent = function (element) {
        if (element.validate()) {
            ShowLoder();
            var date;
            
            if (element.date() != element.oldDate()) {
                date = self.getDate(element.date());
            }
            else {
                date = element.date();
            }
            
            $.post('/Clinics/UpdateAdvancedPaymentPercent', { id: element.id(), date: date, percent: element.percent(), IssuerId: element.issuerId() }, function (data) {
                self.refreshPercentList(data);
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        }
    };


    self.delete = function (element) {
        ShowLoder();
        $.post('/Clinics/DeleteAdvancedPaymentPercent/' + element.id(), function (data) {
            if (data) {
                self.percents.remove(element);
            }
        }).done(function () {
            hideLoder();
            getCookieMessages();
        });
    };

    self.getDate = function (data) {
        var date = new Date(data);
        return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    };



    self.refreshPercentList = function (data) {
        self.percents([]);
        var mapped = $.map(data, function (item) {
            return new Percent(item);
        });
        self.percents(mapped);
    };


    //execution
    self.getData();
}

$(function () {

    ko.validation.init({
        decopercentElement: true,
        decopercentInputElement: true,
        errorElementClass: 'input-validation-error',

    });
    viewModel = new viewModel();
    ko.applyBindings(viewModel);

});