﻿$(function () {

    function CardCode(data) {
        var self = this;
        self.id = ko.observable(data.Id);
        self.name = ko.observable(data.Name);
        self.externalAccountId = ko.observable(data.ExternalAccountId);
    }

    function viewModel() {

        var self = this;

        self.cardCodes = ko.observableArray();
        self.issuerId = ko.observable($('#issuerId').val());

        //actions
        self.add = function () {
            self.cardCodes.push(new CardCode({ 'Id': 0, 'Name': null }));
        };


        self.save = function (element) {
            ShowLoder();

            $.ajax({
                type: "POST",
                url: '/CreditCardCodes/Update',
                data: {
                    'Id': element.id(),
                    'Name': element.name(),
                    'ExternalAccountId': element.externalAccountId(),
                    'IssuerId': self.issuerId()
                },
                success: function (data) {
                    if (data.success && element.id() == 0) {
                        element.id(data.ItemId);
                    }
                }
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        };

        self.remove = function (element) {
            if (element.id() > 0) {

                $.ajax({
                    type: "POST",
                    url: '/CreditCardCodes/Delete',
                    data: {
                        'Id': element.id(),
                        'Name': element.name()
                    }
                }).done(function () {
                    hideLoder();
                    getCookieMessages();

                });
            }

            self.cardCodes.remove(element);
        };

        //self.getData = function () {

        //    ShowLoder();

        //    $.getJSON('/CreditCardCodes/GetAllCodes', function (data) {

        //        var mapped = $.map(data, function (item) {
        //            return new CardCode(item);
        //        });

        //        self.cardCodes(mapped);

        //    }).done(function () { hideLoder(); });
        //};

        self.getData = function () {

            ShowLoder();

            $.getJSON('/CreditCardCodes/GetIssuerData/' + self.issuerId(), function (data) {

                var mapped = $.map(data, function (item) {
                    return new CardCode(item);
                });
                self.cardCodes(mapped);
            }).done(function () {
                hideLoder();
            });
        };

        //execution
        self.getData();
    }


    ko.applyBindings(new viewModel());


});