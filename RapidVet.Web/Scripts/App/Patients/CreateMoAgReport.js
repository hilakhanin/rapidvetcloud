﻿function viewModel(patientId, reportTypeOptions, complaintOptions,offenceOptions,dischargeOptions) {
    var self = this;
    self.PatientId = ko.observable(patientId);
    self.reportTypeOptions = ko.observableArray(reportTypeOptions);
    self.reportType = ko.observable();
    self.complaintOptions = ko.observableArray(complaintOptions);
    self.offenceOptions = ko.observableArray(offenceOptions);
    self.dischargeOptions = ko.observableArray(dischargeOptions);

    self.postUrl = ko.computed(function () {
      
        switch (self.reportType()) {
            case "1":
                {
                    return "/MinistryOfAgriculture/CreateChipImplantReport";
                }
            case "2":
                {
                    return "/MinistryOfAgriculture/CreateRabiesVaccineReport";
                }
            case "3":
                {
                    return "/MinistryOfAgriculture/CreateAttackReport";
                }
            case "4":
                {
                    return "/MinistryOfAgriculture/CreateQuarantineAdmissionReport";
                }
            case "5":
                {
                    return "/MinistryOfAgriculture/CreateDogLicenseReport";
                }
            case "6":
                {
                    return "/MinistryOfAgriculture/CreateQuarantineDischargeReport";
                }
            case "7":
                {
                    return "/MinistryOfAgriculture/CreateRevokingLicenseReport";
                }
            case "8":
                {
                    return "/MinistryOfAgriculture/CreateDangerousDogReport";
                }
            case "9":
                {
                    return "/MinistryOfAgriculture/CreateComplaintReport";
                }
            case "10":
                {
                    return "/MinistryOfAgriculture/CreateDogFindingReport";
                }
        }
    });
}