﻿
$(function () {
    $('.upload-document-image').click(function () {
        $(this).next('input[type="file"]').trigger('click');
        return false;
    });
});

function handleFiles(fileInput) {
    var files = fileInput.files;
    if (!checkFilesSize(files)) {
        fileInput.value = null;
        return false;
    }

    for (var i = 0; i < files.length; i++) {
        $(".objFileName").remove();
        var file = files[i];
        var imageType = /image.*/;

        if (!file.type.match(imageType)) {
            continue;
        }

        var img = document.createElement("img");
        img.classList.add("objFileName");
        img.file = file;
        $(fileInput).after(img);

        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                var scr = e.target.result;
                $(".ProfileImage").attr("src", scr)
            };
        })(img);
        $(".objFileName").hide();
        reader.readAsDataURL(file);
    }
}
var selfAll;
function raceAndKindDDL() {
    var self = this;
    selfAll = self;

    self.kindOptions = ko.mapping.fromJS([]);
    
    self.animalKindId = ko.observable(0);
  
    self.animalRaceId = ko.observable(0);

    self.raceOptions = ko.mapping.fromJS([]);

    var initialSterilization = $('#InitialSterilizationStatus').val() == "True";
    self.Sterilization = ko.observable(initialSterilization);

    self.updateRace = function (newValue) {
        if (typeof newValue !== 'undefined') {
            var qs = {
                kindId: newValue
            };
            $.get('/Patients/GetAnimalRaceOptions', $.param(qs), function(data) {
                ko.mapping.fromJS(data, self.raceOptions);
                self.animalRaceId($('#InitialAnimalRace').val());
               
            });
        }
    };

    $.get('/Patients/GetAnimalKindOptions', null,function(data) {
        ko.mapping.fromJS(data, self.kindOptions);
        var kind = $('#InitialAnimalKindId').val();
        self.animalKindId(kind);
        self.updateRace(kind);
        self.animalKindId.subscribe(self.updateRace);
        setTimeout(function () {
             SetInint();
        }, 500);
    });
 

}