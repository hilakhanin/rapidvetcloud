﻿var vm;
function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

function clearClientSearch(element) {
    vm.selectedClientId(undefined);
    vm.ClientNameWithPatients("");
    element.value = "";
    $(".no-results").hide();
};

$(function () {
    getCookieComments();
    $(".client-search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Clients/Search",
                type: "POST",
                data: {
                    query: request.term
                },
                success: function (data) {
                    response(data);

                    if (data.length == 0) {
                        $(".no-results").show();
                        setTimeout(function () { $(".no-results").hide(); }, 5000);
                    }
                    else {
                        $(".no-results").hide();
                    }
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            event.preventDefault();
            vm.ClientNameWithPatients(ui.item.label);
            vm.selectedClientId(ui.item.value.toString());
            this.value = ui.item.label;

            //window.location = "/Clients/Patients/" + ui.item.value;
        },
        focus: function (event, ui) {
            event.preventDefault();
            this.value = ui.item.label;
        }
        ,
        change: function (event, ui) {
            event.preventDefault();
            if (isEmptyOrWhiteSpaces(this.value) ||
                typeof vm.selectedClientId() == "undefined" || vm.ClientNameWithPatients() != this.value) {
                clearClientSearch(this);
            }

        },
        open: function () {
            $(".ui-autocomplete").css("max-height", 120);
            $(".ui-autocomplete").css("max-width", 225);
            $(".ui-autocomplete").css("overflow-y", "auto");
            $(".ui-autocomplete").css("overflow-x", "hidden");

        }
    });
});

function ViewModel() {
    var self = this;
    vm = self;
    self.comments = ko.mapping.fromJS([]);
    self.patientId = ko.observable($('#patientId').val());
    self.selectedClientId = ko.observable();

    self.getData = function () {
        ShowLoder();

        $.get("../Get/" + self.patientId(), function (data) {
         //   ko.mapping.fromJS(data, self.comments);
            var mapped = $.map(data, function (item) {
                return new addExistingComment(item.Id, item.CreatedDate, item.CreatedBy, item.CommentBody, item.ValidThrough, item.Popup);
            });
            self.comments(mapped);

            $('.date:visible').datepicker();
        }).done(function () {
            hideLoder();
        });
    };
   
    self.save = function (element) {
       self.UpdateErrors = ko.validation.group(element);
        if (!isEmptyOrWhiteSpaces(element.CommentBody())) {

            ShowLoder();
            $.post("../Update", { id: element.Id, commentBody: element.CommentBody, validThrough: element.ValidThrough, popup: element.Popup }, function (data) {
                if (data == true) {
                    hideLoder();
                }
                getCookieMessages();
            });
        }
        else
        {
            self.UpdateErrors.showAllMessages(true);
        }
    };
    self.create = function (element) {
        self.CreateErrors = ko.validation.group(element);
        if (!isEmptyOrWhiteSpaces(element.CommentBody())) {

            ShowLoder();
            $.post("../Create", {
                PatientId: self.patientId(),
                commentBody: element.CommentBody,
                validThrough: element.ValidThrough,
                popup: element.Popup
            }, function (data) {
                if (data.Success == true) {
                    element.Id(data.Id);
                    element.CreatedBy(data.CreatedBy);
                    hideLoder();
                    getCookieMessages();
                }
            });
        }
        else
        {
            self.CreateErrors.showAllMessages(true);
        }
    };
    self.remove = function (element) {
        ShowLoder();
        if (element.Id() == 0) {
            self.comments.remove(element);
            hideLoder();
        } else {
            $.post("../Delete", { id: element.Id() }, function (data) {
                if (data == true) {
                    self.comments.remove(element);
                    hideLoder();
                    getCookieMessages();
                }
            });
        }

    };
    self.add = function () {
        var newComment = {
            Id: ko.observable(0),
            CreatedDate: ko.observable(GetShortDate()),
            CreatedBy: ko.observable(),
            CommentBody: ko.observable().extend({ required: { params: true, message: "שדה חובה" } }),
            ValidThrough: ko.observable(),
            Popup: ko.observable(true)            
        };

        self.comments.push(newComment);
        $('.date:visible').datepicker();
    };

    function addExistingComment(id, created, createdBy, body, validThrough, popup) {
        var newComment = {
            Id: ko.observable(id),
            CreatedDate: ko.observable(created),
            CreatedBy: ko.observable(createdBy),
            CommentBody: ko.observable(body).extend({ required: { params: true, message: "שדה חובה" } }),
            ValidThrough: ko.observable(validThrough),
            Popup: ko.observable(popup)
        };

        return newComment;
    };

    self.getData();
}

ko.applyBindings(ViewModel());

function GetShortDate() {
    var MyDate = new Date();
    var MyDateString;

    MyDate.setDate(MyDate.getDate());

    MyDateString = ('0' + MyDate.getDate()).slice(-2) + '/'
                 + ('0' + (MyDate.getMonth() + 1)).slice(-2) + '/'
                 + MyDate.getFullYear();

    return MyDateString;
}

function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;//str.match(/^$|\s+/) !== null;
}