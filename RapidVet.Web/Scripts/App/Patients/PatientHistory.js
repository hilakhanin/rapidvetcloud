﻿var visitToDeactivateId;
var patientToDeactivateId;

var deactivateVisitFunc = function () {
    $.getJSON('/Visit/DeactivateVisit?id=' + patientToDeactivateId + '&visitId=' + visitToDeactivateId, function (data) {
        if (data == true) {
            location.reload();
        }
    });
};

$(function () {
    function viewModel() {

     //vars
        var self = this;
        self.historyDates = ko.mapping.fromJS([]);
        self.patientId = ko.observable($('#patientId').val());


        //functions
        self.getData = function () {

            ShowLoder();

            $.getJSON('/History/GetHistory/' + patientId(), function (data) {
                ko.mapping.fromJS(data, self.historyDates);
            }).done(function() {
                hideLoder();
            });
        };

        self.deactivateVisit = function (visitId) {
            $('#visitDeleteModel').modal();
            visitToDeactivateId = visitId;
            patientToDeactivateId = patientId();
        };
       

        //execution
        self.getData();

    }
    ko.applyBindings(viewModel());
});