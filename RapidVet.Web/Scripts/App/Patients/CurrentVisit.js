﻿ko.validation.rules['required'].message = "שדה הכרחי";
ko.validation.rules['number'].message = "מספר אינו תקין";
ko.validation.rules['max'].message = "מספר גדול מדי";
ko.validation.rules['min'].message = "מספר שלילי";
var currentUserName;
var currentUserId;
function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

function setTextArea() {
    //$(document).on('mousemove', 'textarea', function (e) {
    //    var a = $(this).offset().top + $(this).outerHeight() - 16,	//	top border of bottom-right-corner-box area
    //        b = $(this).offset().left + $(this).outerWidth() - 16;	//	left border of bottom-right-corner-box area
    //    $(this).css({
    //        cursor: e.pageY > a && e.pageX > b ? 'nw-resize' : ''
    //    });
    //})
    $("textarea").each(function () {
     //   console.log("2222");
          //  the following simple make the textbox "Auto-Expand" as it is typed in
  //  $('textarea').on('mousemove', function (e) {
              //  the following will help the text expand as typing takes place
              while ($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
                  $(this).height($(this).height() + 1);
              };
    });
  
}

$(function () {

    $('.simpleColor').simpleColor({
        displayColorCode: true,
        onSelect: function (hex) {
            $('#color').val('#' + hex);
        }
    });


    function ChildItem(data) {

        var self = this;
        self.Id = ko.observable(0);
        self.PriceListItemId = ko.observable(data.Id());
        self.DiagnosisId = ko.observable(null);
        self.CategoryName = ko.observable();
        self.Name = ko.observable(data.Name());
        self.Quantity = ko.observable(1);
        self.UnitPrice = ko.observable(data.Price());
        self.Discount = ko.observable(0);
        self.DiscountPercent = ko.observable(0).extend({ number: { params: true, message: "יש להזין ערך תקין" } })
                                              .extend({ min: { params: 0, message: "ערך מינימלי: 0" } })
                                              .extend({ max: { params: 100, message: "ערך מקסימלי: 100" } });
        self.Price = ko.observable(0);
        self.AddedBy = ko.observable(currentUserName);
        self.AddedByUserId = ko.observable(currentUserId);
        self.CalculatedPrice = ko.computed(function () {
            var result = isNaN(self.UnitPrice()) ? 0 : self.UnitPrice();

            if (self.Quantity() > 0 && !isNaN(self.UnitPrice())) {
                result = (self.UnitPrice() * self.Quantity()) - self.UnitPrice() * self.Quantity() * (self.DiscountPercent() / 100) - self.Discount();
                self.Price(result);
            }

            return result;
        });

    }

    function ChildItemFromServer(data) {

        var self = this;
        self.Id = ko.observable(data.Id);
        self.PriceListItemId = ko.observable(data.PriceListItemId);
        self.DiagnosisId = ko.observable(null);
        self.CategoryName = ko.observable(data.CategoryName);
        self.Name = ko.observable(data.Name);
        self.Quantity = ko.observable(data.Quantity);
        self.UnitPrice = ko.observable(data.UnitPrice);
        self.Discount = ko.observable(data.Discount);
        self.DiscountPercent = ko.observable(data.DiscountPercent).extend({ number: { params: true, message: "יש להזין ערך תקין" } })
                                               .extend({ min: { params: 0, message: "ערך מינימלי: 0" } })
                                               .extend({ max: { params: 100, message: "ערך מקסימלי: 100" } });
        self.Price = ko.observable();
        self.AddedBy = ko.observable(data.AddedBy == null ? '' : data.AddedBy);
        self.CalculatedPrice = ko.computed(function () {
            var result = isNaN(self.UnitPrice()) ? 0 : self.UnitPrice();

            if (self.Quantity() > 0 && !isNaN(self.UnitPrice())) {
                result = (self.UnitPrice() * self.Quantity()) - self.UnitPrice() * self.Quantity() * (self.DiscountPercent() / 100) - self.Discount();
                self.Price(result);
            }

            return result;
        });

    }

    function BlankChildItem() {

        var self = this;
        self.Id = ko.observable(0);
        self.PriceListItemId = ko.observable(0);
        self.DiagnosisId = ko.observable(0);
        self.CategoryName = ko.observable(null);
        self.Name = ko.observable(null);
        self.Quantity = ko.observable(1);
        self.UnitPrice = ko.observable(0);
        self.Discount = ko.observable(0);
        self.DiscountPercent = ko.observable(0).extend({ number: { params: true, message: "יש להזין ערך תקין" } })
                                              .extend({ min: { params: 0, message: "ערך מינימלי: 0" } })
                                              .extend({ max: { params: 100, message: "ערך מקסימלי: 100" } });
        self.Price = ko.observable();
        self.AddedBy = ko.observable(currentUserName);
        self.AddedByUserId = ko.observable(currentUserId);
        self.CalculatedPrice = ko.computed(function () {
            var result = isNaN(self.UnitPrice()) ? 0 : self.UnitPrice();

            if (self.Quantity() > 0 && !isNaN(self.UnitPrice())) {
                result = (self.UnitPrice() * self.Quantity()) - self.UnitPrice() * self.Quantity() * (self.DiscountPercent() / 100) - self.Discount();
                self.Price(result);
            }

            return result;
        });
    }


    function Medication(medicationId, name, dosage, timesPerDay, numberOfDays, insturctions, unitPrice, administrationTypeId, administrationName, overallAmount, discount, discountPercent, addedBy, addedByUserId) {

        var self = this;
        self.Id = ko.observable(0);
        self.MedicationId = ko.observable(medicationId);
        self.Name = ko.observable(name);
        self.Dosage = ko.observable(dosage);
        self.TimesPerDay = ko.observable(timesPerDay);
        self.NumberOfDays = ko.observable(numberOfDays);
        self.Insturctions = ko.observable(insturctions);
        self.UnitPrice = ko.observable(unitPrice);
        self.MedicationAdministrationTypeId = ko.observable(administrationTypeId);
        self.MedicationAdministrationTypeName = ko.observable(administrationName);
        self.AddedBy = ko.observable(addedBy);
        self.AddedByUserId = ko.observable(addedByUserId);
        self.OverallAmount = ko.observable(overallAmount).extend({ number: true });
        self.Discount = ko.observable(discount).extend({ number: true });
        self.DiscountPercent = ko.observable(discountPercent).extend({ number: { params: true, message: "יש להזין ערך תקין" } })
                                              .extend({ min: { params: 0, message: "ערך מינימלי: 0" } })
                                              .extend({ max: { params: 100, message: "ערך מקסימלי: 100" } });
        self.Price = ko.observable();
        self.Total = ko.computed(function () {
            var result = self.UnitPrice();

            if (self.OverallAmount() > 0) {
                result = (self.UnitPrice() * self.OverallAmount()) - self.UnitPrice() * self.OverallAmount() * (self.DiscountPercent() / 100) - self.Discount();
                self.Price(result);
            }

            return result;
        });
    }


    function dangerousDrug(medicationId, medicationName, injected, destroyed) {
        var self = this;
        self.Id = ko.observable(0);
        self.MedicationId = ko.observable(medicationId);
        self.MedicationName = ko.observable(medicationName);
        self.AmountInjected = ko.observable(injected);
        self.AmountDestroyed = ko.observable(destroyed);
    }




    function viewModel() {

        // -------------properties start------------

        var self = this;

        self.patientId = ko.observable($('#patientId').val());
        self.visitId = ko.observable($('#visitId').val());

        //general visit fields
        self.mainComplaint = ko.observable();
        self.bpm = ko.observable().extend({ number: true }).extend({ max: maxInt }).extend({ min: 0 });
        self.bcs = ko.observable().extend({ number: true }).extend({ max: maxInt }).extend({ min: 0 });
        self.hr = ko.observable().extend({ number: true }).extend({ max: maxInt }).extend({ min: 0 });
        self.temp = ko.observable().extend({ number: true }).extend({ max: maxInt }).extend({ min: 0 });
        self.weight = ko.observable().extend({ number: true }).extend({ max: maxInt }).extend({ min: 0 });
        self.time = ko.observable();
        self.date = ko.observable();
        self.mucosa = ko.observable();
        self.visitNote = ko.observable();
        self.color = ko.observable();
        self.closeVisit = ko.observable(false);
        self.print = ko.observable(false);
        self.printPrescription = ko.observable(false);
        self.printPrescriptionValidityComment = ko.observable(false);
        self.active = ko.observable();
        self.ActiveUserName = ko.observable();

        //search related 
        self.selectedCategoryId = ko.observable(null);
        self.searchQuery = ko.observable(null);

        self.clearSearch = function () {
            self.selectedCategoryId(null);
            self.searchQuery(null);
        };

        //doctors
        self.doctors = ko.mapping.fromJS([]);
        self.selectedDoctorId = ko.observable();

        //examinations

        self.examinations = ko.mapping.fromJS([]);
        self.visitExaminations = ko.mapping.fromJS([]);
        self.examinationsOptions = ko.computed(function () {

            var options = self.examinations();

            if (self.selectedCategoryId() > 0) {
                options = ko.utils.arrayFilter(options, function (item) {
                    if (item.CategoryId() == self.selectedCategoryId()) {
                        return item;
                    }
                    return false;
                });
            }
            if (self.searchQuery() != null) {
                if (self.searchQuery() != " " && self.searchQuery().length > 0) {
                    options = ko.utils.arrayFilter(options, function (item) {
                        if (item.Name().indexOf(self.searchQuery()) > -1) {
                            return item;
                        }
                        return false;
                    });
                }
            }
            return options;
        });

        self.showExaminationsModal = function () {
            $('#examinationsModal').modal('show');
        };

        self.hideExaminationsModal = function () {
            $('#examinationsModal').modal('hide');
            self.clearSearch();
        };



        self.addExamination = function (element) {

            var item = new ChildItem(element);
            item.CategoryName(self.getCategoryName(element.CategoryId()));
            self.visitExaminations.push(item);
            self.hideExaminationsModal();
        };

        self.addBlankExamination = function () {
            self.visitExaminations.push(new BlankChildItem());
        };

        self.removeExamination = function (element) {
            self.visitExaminations.remove(element);
        };

        self.getExaminationsData = function () {

            $.getJSON('/Visit/GetExaminationsData/' + self.patientId(), function (data) {
                ko.mapping.fromJS(data, self.examinations);
                setTextArea();
            });

        };

        //diagnoses

        self.diagnoses = ko.mapping.fromJS([]);
        self.visitDiagnoses = ko.mapping.fromJS([]);
        self.diagnosesOptions = ko.computed(function () {

            var options = self.diagnoses();
            if (self.searchQuery() != null) {
                if (self.searchQuery() != ' ' && self.searchQuery().length > 0) {
                    options = ko.utils.arrayFilter(options, function (item) {
                        if (item.Name().indexOf(self.searchQuery()) > -1) {
                            return item;
                        }
                        return false;
                    });
                }
            }

            return options;

        });

        self.showDiagnosesModal = function () {

            $('#diagnosesModal').modal('show');
        };

        self.hideDiagnosesModal = function () {
            $('#diagnosesModal').modal('hide');
            self.clearSearch();
        };

        self.addDiagnosis = function (element) {

            var item = new BlankChildItem();
            item.DiagnosisId(element.Id());
            item.Name(element.Name());

            self.visitDiagnoses.push(item);
            self.hideDiagnosesModal();
        };

        self.addBlankDiagnosis = function () {
            self.visitDiagnoses.push(new BlankChildItem());
        };

        self.removeDiagnosis = function (element) {
            self.visitDiagnoses.remove(element);
        };

        self.getDiagnosesData = function () {

            $.getJSON('/Visit/GetDiagnosisData/' + self.patientId(), function (data) {
                ko.mapping.fromJS(data, self.diagnoses);
                setTextArea();
            });

        };

        //treatments

        self.treatments = ko.mapping.fromJS([]);
        self.visitTreatments = ko.observableArray([]);
        self.treatmentsOptions = ko.computed(function () {

            var options = self.treatments();
            if (self.selectedCategoryId() > 0) {
                options = ko.utils.arrayFilter(options, function (item) {
                    if (item.CategoryId() == self.selectedCategoryId()) {
                        return item;
                    }
                    return false;
                });
            }
            if (self.searchQuery() != null) {
                if (self.searchQuery() != ' ' && self.searchQuery().length > 0) {
                    options = ko.utils.arrayFilter(options, function (item) {
                        if (item.Name().indexOf(self.searchQuery()) > -1) {
                            return item;
                        }
                        return false;
                    });
                }
            }
            return options;

        });

        self.showTreatmentsModal = function () {

            $('#treatmentsModal').modal('show');
        };

        self.hideTreatmentsModal = function () {
            $('#treatmentsModal').modal('hide');
            self.clearSearch();
        };

        self.addTreatment = function (element) {
            var item = new ChildItem(element);
            item.CategoryName(self.getCategoryName(element.CategoryId()));
            self.visitTreatments.push(item);

            setCookieMessage("successmsg", encodeURIComponent("הטיפול נוסף בהצלחה"), 1, 0, 0);
            getCookieMessages();
            //    self.hideTreatmentsModal();
        };

        self.addBlankTreatment = function () {
            self.visitTreatments.push(new BlankChildItem());
        };

        self.removeTreatment = function (element) {
            self.visitTreatments.remove(element);
        };

        self.getTreatmentsData = function () {

            $.getJSON('/Visit/GetTreatmentsData/' + self.patientId(), function (data) {
                ko.mapping.fromJS(data, self.treatments);
                setTextArea();
            });

        };

        //medications

        self.medications = ko.mapping.fromJS([]);
        self.medicationAdministrationTypes = ko.mapping.fromJS([]);
        self.visitMedications = ko.mapping.fromJS([]);

        self.selectedMedicationId = ko.observable();
        self.selectedMedicationName = ko.observable();
        self.selectedMedicationDosage = ko.observable();
        self.selectedMedicationTimesPerDay = ko.observable();
        self.selectedMedicationNumberOfDays = ko.observable();
        self.selectedMedicationInsturctions = ko.observable();
        self.selectedMedicationUnitPrice = ko.observable();
        self.selectedMedicationDangerousDrug = ko.observable();
        self.selectedMedicationDoctorInfo = ko.observable();
        self.selectedMedicationMedicationAdministrationTypeId = ko.observable();
        self.selectedMedicationAdministrationName = ko.computed(function () {
            var adminType = ko.utils.arrayFirst(self.medicationAdministrationTypes(), function (a) {
                return self.selectedMedicationMedicationAdministrationTypeId() == a.Value();
            });

            if (adminType) {
                return adminType.Text();
            }
        });

        self.selectedMedication = ko.computed(function () {
            var selected = ko.utils.arrayFirst(self.medications(), function (m) {
                return self.selectedMedicationId() == m.Id();
            });

            if (selected) {
                self.selectedMedicationName(selected.Name());
                self.selectedMedicationDosage(selected.Dosage());
                self.selectedMedicationTimesPerDay(selected.TimesPerDay());
                self.selectedMedicationNumberOfDays(selected.NumberOfDays());
                self.selectedMedicationInsturctions(selected.Insturctions());
                self.selectedMedicationUnitPrice(selected.UnitPrice());
                self.selectedMedicationDangerousDrug(selected.DangerousDrug());
                self.selectedMedicationDoctorInfo(selected.DoctorInfo());
                self.selectedMedicationMedicationAdministrationTypeId(selected.MedicationAdministrationTypeId());
            }
        });


        self.showMedicationsModal = function () {

            $('#medicationsModal').modal('show');
        };

        self.hideMedicationsModal = function () {
            $('#medicationsModal').modal('hide');
            self.clearSearch();
        };

        self.addSelectedMedication = function (givenID) {

            var item = new Medication(
                self.selectedMedicationId(),
                self.selectedMedicationName(),
                self.selectedMedicationDosage(),
                self.selectedMedicationTimesPerDay(),
                self.selectedMedicationNumberOfDays(),
                self.selectedMedicationInsturctions(),
                self.selectedMedicationUnitPrice(),
                self.selectedMedicationMedicationAdministrationTypeId(),
                self.selectedMedicationAdministrationName(),
                1, 0, 0, currentUserName, currentUserId
            );

            if (givenID > -1)
                item.MedicationId = givenID;

            self.visitMedications.push(item);
            self.hideMedicationsModal();
        };

        self.removeMedication = function (element) {
            self.visitMedications.remove(element);
        };

        self.createNewMedication = function () {
            ShowLoder();
            $.ajax({
                type: "POST",
                url: '/Medication/CreateFromVisit',
                data: {
                    'Name': self.selectedMedicationName(),
                    'MedicationAdministrationTypeId': self.selectedMedicationMedicationAdministrationTypeId(),
                    'Dosage': self.selectedMedicationDosage(),
                    'TimesPerDay': self.selectedMedicationTimesPerDay(),
                    'NumberOfDays': self.selectedMedicationNumberOfDays(),
                    'Insturctions': self.selectedMedicationInsturctions(),
                    'UnitPrice': self.selectedMedicationUnitPrice(),
                    'DangerousDrug': self.selectedMedicationDangerousDrug(),
                    'DoctorInfo': self.selectedMedicationDoctorInfo()
                },
                success: function (data) {
                    self.hideMedicationsModal();
                    if (data.Success) {
                        self.addSelectedMedication(data.ItemId);
                        self.getMedicationsData();
                    }
                    getCookieMessages();

                    hideLoder();
                },
                failuer: function () {
                    self.hideMedicationsModal();
                    getCookieMessages();
                    hideLoder();
                }
            });

            self.hideMedicationsModal();
        };

        self.getMedicationsData = function () {
            self.medications([]);
            $.getJSON('/Visit/GetMedicationData/' + self.patientId(), function (data) {
                ko.mapping.fromJS(data, self.medications);
            });

            self.getMedicationAdministrationTypesData = function () {

                $.getJSON('/Visit/GetMedicationAdministrationTypes', function (data) {
                    ko.mapping.fromJS(data, self.medicationAdministrationTypes);
                });

            };

        };

        //dangerous drugs
        self.dangerousDrugs = ko.mapping.fromJS([]);

        self.dangerousMedicationsList = ko.computed(function () {
            var result = $.grep(self.medications(), function (m) {
                return m.DangerousDrug() == true;
            });
            return result;
        });
        self.dangerousDrugId = ko.observable();
        self.dangerousDrugName = ko.computed(function () {
            var drug = ko.utils.arrayFirst(self.dangerousMedicationsList(), function (d) {
                return d.Id() == self.dangerousDrugId();
            });
            if (drug) {
                return drug.Name();
            }
        });
        self.dangerousDrugInjected = ko.observable(0);
        self.dangerousDrugDesytoyed = ko.observable(0);

        self.showDangerousDrugsModel = function () {

            $('#dangerousDrugsModal').modal('show');
        };

        self.hideDangerousDrugsModal = function () {
            $('#dangerousDrugsModal').modal('hide');
            self.clearDangerousDrugForm();
        };

        self.addDangerousDrug = function () {

            var item = new dangerousDrug(self.dangerousDrugId(), self.dangerousDrugName(), self.dangerousDrugInjected(), self.dangerousDrugDesytoyed());
            self.dangerousDrugs.push(item);
            self.clearDangerousDrugForm();

        };
        self.removeDangerousDrug = function (element) {
            self.dangerousDrugs.remove(element);
        };

        self.clearDangerousDrugForm = function () {
            self.dangerousDrugId(null);
            self.dangerousDrugInjected(0);
            self.dangerousDrugDesytoyed(0);
        };


        //priceListCategories
        self.categories = ko.mapping.fromJS([]);

        //visit prices
        self.dummy = ko.observable();
        self.fixedVisitPrice = ko.observable();
        self.computedVisitPrice = ko.computed(function () {
            var result = 0;
            for (var i = 0; i < self.visitExaminations().length; i++) {
                result += parseFloat(self.visitExaminations()[i].CalculatedPrice());
                // result += parseFloat((self.visitExaminations()[i].UnitPrice() * self.visitExaminations()[i].Quantity()) - self.visitExaminations()[i].UnitPrice() * self.visitExaminations()[i].Quantity() * (self.visitExaminations()[i].DiscountPercent() / 100) - self.visitExaminations()[i].Discount());
            }
            for (var i = 0; i < self.visitTreatments().length; i++) {
                result += parseFloat(self.visitTreatments()[i].CalculatedPrice());
                //  result += parseFloat((self.visitTreatments()[i].UnitPrice() * self.visitTreatments()[i].Quantity()) - self.visitTreatments()[i].UnitPrice() * self.visitTreatments()[i].Quantity() * (self.visitTreatments()[i].DiscountPercent() / 100) - self.visitTreatments()[i].Discount());
            }
            for (var i = 0; i < self.visitMedications().length; i++) {
                result += parseFloat(self.visitMedications()[i].Total());
            }
            return parseFloat(result);
        });

        self.displayedVisitPrice = ko.computed({
            read: function () {
                self.dummy();
                var result = self.computedVisitPrice();
                if (typeof self.fixedVisitPrice() != "undefined") {
                    result = self.fixedVisitPrice();
                }
                return result;
            },
            write: function (value) {
                self.fixedVisitPrice(parseFloat(value));
            }
        }).extend({ number: true });

        self.clearVisitFixedPrice = function () {
            self.fixedVisitPrice = ko.observable();
            self.dummy.notifySubscribers();
        };


        //general
        self.updateColor = function () {
            self.color($('#color').val());
        };

        //data related operations

        self.getGeneralData = function () {
            ShowLoder();
            $.getJSON('/Visit/GetData/' + self.patientId() + '?visitId=' + self.visitId(), function (data) {

                self.visitId(data.Id);
                self.mainComplaint(data.MainComplaint);
                self.bpm(data.BPM);
                self.bcs(data.BCS);
                self.hr(data.HR);
                if (data.Temp > 0)
                    self.temp(data.Temp);

                if (data.Weight > 0)
                    self.weight(data.Weight);

                self.visitNote(data.VisitNote);
                self.time(data.Time);
                self.date(data.Date);
                self.mucosa(data.Mucosa);
                self.color(data.Color);
                self.closeVisit(data.Close);
                self.active(data.Active);
                self.ActiveUserName(data.ActiveUserName)
                currentUserName = data.ActiveUserName;
                currentUserId = data.ActiveUserId;
                $('.simpleColor').setColor(self.color());
                ko.mapping.fromJS(data.Doctors, self.doctors);
                self.selectedDoctorId(data.SelectedDoctorId);
                var visitExaminations = $.map(data.VisitExaminations, function (item) { return new ChildItemFromServer(item); });
                self.visitExaminations(visitExaminations);
                //  ko.mapping.fromJS(data.VisitExaminations, self.visitExaminations);
                ko.mapping.fromJS(data.VisitDiagnoses, self.visitDiagnoses);
                var visitTreatments = $.map(data.VisitTreatments, function (item) { return new ChildItemFromServer(item); });
                self.visitTreatments(visitTreatments);
                var visitMedications = $.map(data.VisitMedications, function (item) { return new Medication(item.MedicationId, item.Name, item.Dosage, item.TimesPerDay, item.NumberOfDays, item.Insturctions, item.UnitPrice, item.MedicationAdministrationTypeId, item.MedicationAdministrationTypeName, item.OverallAmount, item.Discount, item.DiscountPercent, item.AddedBy, item.AddedByUserId); });
                self.visitMedications(visitMedications);

                // ko.mapping.fromJS(data.VisitMedications, self.visitMedications);
                ko.mapping.fromJS(data.DangerousDrugs, self.dangerousDrugs);
                ko.mapping.fromJS(data.Categories, self.categories);

                if (typeof data.TotalPrice != "undefined" && data.TotalPrice != self.computedVisitPrice()) {
                    self.fixedVisitPrice(data.TotalPrice);
                }

                if (self.active() == false) {
                    $('#visitDiv :input').attr('disabled', true);
                    $('#visitDiv a').attr('disabled', true);
                    $('#visitDiv a').unbind();
                    $('#visitDiv').css('background-color', 'gainsboro');
                    $('.simpleColor').attr('disabled', true);
                    $('.simpleColorDisplay').attr('disabled', true);
                    $('.simpleColorDisplay').unbind();
                    $("#currentVisit").html($("#currentVisit").html().substring(0, $("#currentVisit").html().indexOf(">") + 1) + "*** ביקור מבוטל ***" + "</a>");
                    $("#back").text("סגור");
                }
                setTextArea();
            }).done(function () {
                hideLoder();
            });
        };

        self.getCategoryName = function (id) {
            var category = $.grep(self.categories(), function (c) { return c.Id() == id; });
            return category[0].Name();
        };

        self.removeEmptyLines = function () {

            for (var i = 0; i < self.visitExaminations().length; i++) {
                if (self.visitExaminations()[i].Price() == 0 && isEmptyOrWhiteSpaces(self.visitExaminations()[i].Name())) {
                    self.removeExamination(self.visitExaminations()[i]);
                    i--;
                }
            }
            for (var i = 0; i < self.visitTreatments().length; i++) {
                if (self.visitTreatments()[i].Price() == 0 && isEmptyOrWhiteSpaces(self.visitTreatments()[i].Name())) {
                    self.removeTreatment(self.visitTreatments()[i]);
                    i--;
                }
            }
            for (var i = 0; i < self.visitDiagnoses().length; i++) {
                if (isEmptyOrWhiteSpaces(self.visitDiagnoses()[i].Name())) {
                    self.removeDiagnosis(self.visitDiagnoses()[i]);
                    i--;
                }
            }

        };

        self.saveVisit = function () {
            self.save();
        };

        self.saveAndPrint = function () {
            self.print(true);
            self.save();
        };

        self.saveAndPrintSubscription = function () {
            self.printPrescription(true);
            self.save();
        };


        self.save = function () {
            if (self.validate()) {
                $('#validateSummary').hide();

                self.removeEmptyLines();

                self.updateColor();

                ShowLoder();

                var visitPrice = self.computedVisitPrice();
                if (typeof self.fixedVisitPrice() != "undefined") {
                    visitPrice = self.fixedVisitPrice();
                }

                $.ajax({
                    type: "POST",
                    url: '/Visit/SaveVisit',
                    data: {
                        'Id': self.visitId(),
                        'PatientId': self.patientId(),
                        'VisitNote': self.visitNote(),
                        'MainComplaint': self.mainComplaint(),
                        'Weight': self.weight(),
                        'Temp': self.temp(),
                        'BPM': self.bpm(),
                        'BCS': self.bcs(),
                        'HR': self.hr(),
                        'Mucosa': self.mucosa(),
                        'Color': self.color(),
                        'Time': self.time(),
                        'Date': self.date(),
                        'SelectedDoctorId': self.selectedDoctorId(),
                        'TotalPrice': visitPrice,
                        'Examinations': ko.toJSON(self.visitExaminations()),
                        'Diagnoses': ko.toJSON(self.visitDiagnoses()),
                        'Treatments': ko.toJSON(self.visitTreatments()),
                        'Medications': ko.toJSON(self.visitMedications()),
                        'DangerousDrugs': ko.toJSON(self.dangerousDrugs()),
                        'Close': self.closeVisit(),
                        'Print': self.print(),
                        'printPrescription': self.printPrescription()
                    },
                    success: function (data) {
                        self.hideMedicationsModal();
                        if (data.success) {

                            if (self.print() || (self.printPrescription())) {

                                var url = data.printUrl;
                                if (self.printPrescription() && self.printPrescriptionValidityComment()) {
                                    url += '?printValidityComment=' + self.printPrescriptionValidityComment();
                                }
                                window.open(url, "_blank");
                            }

                            window.location.href = data.url;
                        }

                        hideLoder();
                    },
                    failuer: function () {
                        self.hideMedicationsModal();
                        getCookieMessages();
                        hideLoder();
                    }
                });


                self.hideMedicationsModal();
            } else {
                $('#validateSummary').show();
            }
        };

        //--------operations end-----------


        //execution

        self.getGeneralData();
        self.getTreatmentsData();
        self.getExaminationsData();
        //self.getExaminationsData();
        self.getMedicationsData();
        self.getDiagnosesData();
        self.getMedicationAdministrationTypesData();


        self.validate = function () {
            var errors = ko.validation.group(this, { deep: true });
            errors.showAllMessages();
            return errors().length == 0;
        };
    }

    ko.applyBindings(new viewModel());
});

