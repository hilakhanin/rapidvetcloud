﻿$(function () {

    Date.prototype.formatMMDDYYYY = function () {
        var day = ("0" + this.getDate()).slice(-2);
        var month = ("0" + (this.getMonth() + 1)).slice(-2);
        var year = this.getFullYear();
        return day + "/" + month + "/" + year;
    };
    
    function Expense(data) {
        var self = this;
        self.id = ko.observable(data.Id);
        self.expenseGroupId = ko.observable(data.ExpenseGroupId);
        self.expenseGroup = ko.observable(data.ExpenseGroup);
        self.supplierId = ko.observable(data.SupplierId);
        self.supplier = ko.observable(data.Supplier);
        self.enteredDate = ko.observable(data.EnteredDate);
        self.invoiceDate = ko.observable(data.InvoiceDate);
        self.description = ko.observable(data.Description);
        self.invoiceNumber = ko.observable(data.InvoiceNumber);
        self.totalAmount = ko.observable(data.TotalAmount);
        self.vat = ko.observable(data.Vat);
        self.paymentDate = ko.observable(data.PaymentDate);
        self.recognitionPercent = ko.observable(data.RecognitionPercent);
        self.editUrl = ko.computed(function() {
            return '/Expenses/Edit/' + self.id();
        });
        self.deleteUrl = ko.computed(function() {
            return 'javascript: GetModal(\'/Expenses/ConfirmDelete/' + self.id() + '\', \'deleteModal\')';
        });
    }
    

    function viewModel() {

        var self = this;
        self.expenses = ko.observableArray([]);
        self.issuers = ko.observable([]);
        self.selectedIssuerId = ko.observable();
        self.dateTypes = ko.observable([]);
        self.selectedDateType = ko.observable();
        self.from = ko.observable();
        self.to = ko.observable();

        self.displayResults = ko.observable(false);

        self.getData = function () {
            if (typeof self.from() != "undefined" && typeof self.to() != "undefined") {
                ShowLoder();

                var from = replaceDateSeperator(self.from());
                var to = replaceDateSeperator(self.to());

                $.getJSON('/Expenses/GetClinicExpenses?fromDate=' + from + '&toDate=' + to + '&issuerId=' + self.selectedIssuerId()+ '&dateType=' + self.selectedDateType()
                    , function (data) {
                    var counter = 0;
                    var mapped = $.map(data, function (item) {
                        return new Expense(item);
                    });

                    self.expenses(mapped);
                    self.displayResults(true);

                }).done(function () {
                    hideLoder();
                });
            }
            else {
                ShowLoder();
                $.getJSON('/Expenses/GetClinicExpenses?fromDate=&toDate=&issuerId=' + self.selectedIssuerId() + '&dateType=' + self.selectedDateType()
                    , function (data) {
                    var counter = 0;
                    var mapped = $.map(data, function (item) {
                        return new Expense(item);
                    });

                    self.expenses(mapped);
                    self.displayResults(true);

                }).done(function () {
                    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
                    var firstDay = new Date(y, m, 1);
                    var lastDay = new Date(y, m + 1, 0);
                    self.from(firstDay.formatMMDDYYYY());
                    self.to(lastDay.formatMMDDYYYY());
                    hideLoder();
                });
            }
        };

        self.getIssuers = function () {
            ShowLoder();

            $.get('/FinanceReports/GetIssuers', function (data) {
                var mappedIssuers = $.map(data, function (item) {
                    return new SelectItem(item.Value, item.Text);
                });
                self.issuers(mappedIssuers);
            }).done(function () {
                self.getDateTypes();
                hideLoder();
            });
        };

        self.getDateTypes = function () {
            ShowLoder();

            $.get('/Expenses/GetDateTypes', function (data) {
                var mappedDateTypes = $.map(data, function (item) {
                    return new SelectItem(item.Value, item.Text);
                });
                self.dateTypes(mappedDateTypes);
            }).done(function () {
                self.getData();
                hideLoder();
            });
        };

        self.print = function () {
            var from = replaceDateSeperator(self.from());
            var to = replaceDateSeperator(self.to());

            window.open('/Expenses/PrintClinicExpenses?fromDate=' + from + '&toDate=' + to + '&issuerId=' + self.selectedIssuerId() + '&dateType=' + self.selectedDateType(), '_blank');

            return false;
        }

        self.exportCsv = function () {
            var from = replaceDateSeperator(self.from());
            var to = replaceDateSeperator(self.to());

            window.open('/Expenses/ExportToCSV?fromDate=' + from + '&toDate=' + to + '&issuerId=' + self.selectedIssuerId() + '&dateType=' + self.selectedDateType(), '_blank');

            return false;
        }

        self.getIssuers();
        
    }


    ko.applyBindings(new viewModel());


});

function SelectItem(value, text) {
    var self = this;
    self.Value = value;
    self.Text = text;
}