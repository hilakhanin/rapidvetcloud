﻿$(function () {

    Date.prototype.formatMMDDYYYY = function () {
        var day = ("0" + this.getDate()).slice(-2);
        var month = ("0" + (this.getMonth() + 1)).slice(-2);
        var year = this.getFullYear();
        return day + "/" + month + "/" + year;
    };

    function viewModel() {
        var self = this;
        self.taxRate = ko.observable($("#taxRate").val());
        self.id = ko.observable($("#expenseId").val());
        self.enteredDate = ko.observable(moment().format("DD/MM/YYYY"));

        //containers
        self.issuers = ko.observable([]);
        self.expenseGroups = ko.mapping.fromJS([]);
        self.expenseGroupOptions = ko.observable([]);
        self.supplier = ko.mapping.fromJS([]);
        self.paymentType = ko.mapping.fromJS([]);

        //selected values
        self.expenseGroupId = ko.observable();
        self.selectedIssuerId = ko.observable();
        self.supplierId = ko.observable();
        self.paymentTypeId = ko.observable();

        self.invoiceDate = ko.observable(moment().format("DD/MM/YYYY"));
        self.invoiceNumber = ko.observable();
        self.reciptNumber = ko.observable();

        self.description = ko.observable();
        self.totalAmount = ko.observable(0);
        self.recognitionPercent = ko.observable();
        self.recognitionTotalAmountIncludingVat = ko.computed(function () {
            if (self.recognitionPercent() == 0)
                return parseFloat(self.totalAmount());
            else {
                return self.recognitionPercent() / 100 * self.totalAmount();
            }
        });
        self.vat = ko.computed(function () {
            if (self.recognitionPercent() == 0) {
                return 0;
            }
            else {
                return self.recognitionTotalAmountIncludingVat() - self.recognitionTotalAmountIncludingVat() / self.taxRate();
            }
        });
        self.paymentDate = ko.observable();
        self.comments = ko.observable();

        //subscribers
        self.expenseGroupId.subscribe(function (newValue) {

            for (var i = 0; i < self.expenseGroups().length; i++) {
                if (newValue == self.expenseGroups()[i].Id()) {
                    if (self.expenseGroups()[i].IsTextDeductible()) {
                        self.recognitionPercent(self.expenseGroups()[i].RecognitionPercent());
                    }
                    else {
                        self.recognitionPercent(0);
                    }
                    break;
                }
            }

        });

        self.paymentTypeId.subscribe(function (newValue) {
            if(typeof newValue == "undefined")
            {
                self.paymentDate("");
            }
        });


        self.getIssuers = function () {
            ShowLoder();

            $.get('/FinanceReports/GetIssuers', function (data) {
                var mappedIssuers = $.map(data, function (item) {
                    return new SelectItem(item.Value, item.Text);
                });
                self.issuers(mappedIssuers);
                self.getClinicExpenseGroups();
            }).done(function () {

                hideLoder();
            });
        };

        self.getClinicExpenseGroups = function () {
            ShowLoder();

            $.get('/Expenses/GetClinicExpenseGroups', function (data) {
                ko.mapping.fromJS(data, self.expenseGroups);
                var mappedGroups = $.map(data, function (item) {
                    return new SelectItem(item.Id, item.Name);
                });
                self.expenseGroupOptions(mappedGroups);
                self.getClinicSuppliers();
            }).done(function () {
                hideLoder();
            });
        };

        self.getClinicSuppliers = function () {
            ShowLoder();

            $.get('/Expenses/GetClinicSuppliers', function (data) {
                ko.mapping.fromJS(data, self.supplier);
                self.getPaymentTypes();
            }).done(function () {
                hideLoder();
            });
        };

        self.getPaymentTypes = function () {
            ShowLoder();

            $.get('/Expenses/GetPaymentTypes', function (data) {
                ko.mapping.fromJS(data, self.paymentType);
                self.checkExpense();
            }).done(function () {
                hideLoder();
            });
        };

        self.checkExpense = function () {
            if (self.id() > 0) {
                ShowLoder();
                $.get('/Expenses/GetExpense?id=' + self.id(), function (data) {
                    self.enteredDate(data.EnteredDate);
                    self.expenseGroupId(data.ExpenseGroupId);
                    self.selectedIssuerId(data.IssuerId);
                    self.supplierId(data.SupplierId);
                    self.paymentTypeId(data.PaymentTypeId == "null" ? "undefined" : data.PaymentTypeId);

                    self.invoiceDate(data.InvoiceDate);
                    self.invoiceNumber(data.InvoiceNumber);
                    self.reciptNumber(data.ReciptNumber);

                    self.description(data.Description);
                    self.totalAmount(data.TotalAmount);
                    self.paymentDate(data.PaymentDate);
                    self.comments(data.Comments);
                });

                hideLoder();
            }
        };

        self.copyInvoiceNumber = function () {
            self.reciptNumber(self.invoiceNumber());
        };

        self.stringToDate = function (_date, _format, _delimiter) {
            var formatLowerCase = _format.toLowerCase();
            var formatItems = formatLowerCase.split(_delimiter);
            var dateItems = _date.split(_delimiter);
            var monthIndex = formatItems.indexOf("mm");
            var dayIndex = formatItems.indexOf("dd");
            var yearIndex = formatItems.indexOf("yyyy");
            var month = parseInt(dateItems[monthIndex]);
            month -= 1;
            var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
            return formatedDate.toISOString();
        };


        self.save = function () {

            ShowLoder();

            var url = self.id() == 0 ? '/Expenses/Create' : '/Expenses/Edit'

            $.post(url, {
                Id: self.id(),
                ExpenseGroupId: self.expenseGroupId(),
                SupplierId: self.supplierId(),
                IssuerId: self.selectedIssuerId(),
                InvoiceDate: moment().format("DD/MM/YYYY") === self.invoiceDate() ? self.invoiceDate() : self.stringToDate(self.invoiceDate(),"dd/MM/yyyy","/"),
                EnteredDate: moment().format("DD/MM/YYYY") === self.enteredDate() ? self.enteredDate() : self.stringToDate(self.enteredDate(),"dd/MM/yyyy","/"),
                Description: self.description(),
                InvoiceNumber: self.invoiceNumber(),
                ReciptNumber: self.reciptNumber(),
                TotalAmount: self.totalAmount(),
                RecognitionPercent: self.recognitionPercent(),
                RecognitionTotalAmountIncludingVat: self.recognitionTotalAmountIncludingVat(),
                Vat: self.vat(),
                PaymentDate: (typeof self.paymentDate() == "undefined" || self.paymentDate() == "") ? "" : moment().format("DD/MM/YYYY") === self.paymentDate() ? self.paymentDate() :  self.stringToDate(self.paymentDate(), "dd/MM/yyyy", "/"),
                PaymentTypeId: self.paymentTypeId() > -1 ? self.paymentTypeId() : -1,
                Comments: self.comments()

            }, function (data) {
                setTimeout(function () { window.location.href = '/Expenses/Index'; }, 1000);
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        };

        self.getIssuers();
      
        
       
        
    }


    ko.applyBindings(new viewModel());


});

function SelectItem(value, text) {
    var self = this;
    self.Value = value;
    self.Text = text;
}