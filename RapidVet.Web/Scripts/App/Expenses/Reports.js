﻿var viewModel;

$(function () {


    function viewModel() {
        var self = this;
        self.fromDate = ko.observable();
        self.from = ko.computed(function () {
            return replaceDateSeperator(self.fromDate());
        });
        self.toDate = ko.observable();
        self.to = ko.computed(function () {
            return replaceDateSeperator(self.toDate());
        });

        self.suppliers = ko.mapping.fromJS([]);
        self.selectedSupplierId = ko.observable();
        self.issuers = ko.observable([]);
        self.selectedIssuerId = ko.observable();

        self.validate = ko.computed(function () {
            return typeof self.fromDate() !== 'undefined' && self.fromDate() != null && self.fromDate() != "" &&
                self.toDate() !== 'undefined' && self.toDate() != null && self.toDate() != "";
        });
        
        self.notWithSupplier = ko.computed(function () {
            var css = 'btn btn-primary disabled';
            if (typeof self.selectedSupplierId() == "undefined" && self.validate()) {
                css = 'btn btn-primary';
            }
            return css;
        });


    

        self.withSupplier = ko.computed(function () {
            var css = 'btn btn-primary';
            if (!self.validate() || typeof self.selectedSupplierId() == "undefined") {
                css = 'btn btn-primary disabled';
            }
            return css;
        });


        //report url's
        self.reportBySupplier = ko.computed(function () {
            var result = '#';
            if (typeof self.selectedSupplierId() != "undefined") {
                result = '/Expenses/ReportBySupplier/' + self.selectedSupplierId() + '?from=' + self.from() + '&to=' + self.to() + '&issuerId=' + self.selectedIssuerId();
            }
            return result;
        });

        self.reportBySupplierTarget = ko.computed(function () {
            result = '_self';
            if (typeof self.selectedSupplierId() != "undefined") {
                result = '_blank';
            }
            return result;
        });


        self.reportByGroups = ko.computed(function () {
            return '/Expenses/ReportByGroups' + '?from=' + self.from() + '&to=' + self.to() + '&issuerId=' + self.selectedIssuerId();
        });

        self.monthlyReport = ko.computed(function () {
            return '/Expenses/MonthlyReport' + '?from=' + self.from() + '&to=' + self.to() + '&issuerId=' + self.selectedIssuerId();
        });

        self.quarterlyRepot = ko.computed(function () {
            return '/Expenses/QuarterlyReport' + '?from=' + self.from() + '&to=' + self.to() + '&issuerId=' + self.selectedIssuerId();

        });

        self.getSuppliers = function () {

            ShowLoder();

            $.getJSON('/Suppliers/GetClinicSuppliers', function (data) {
                ko.mapping.fromJS(data, self.suppliers);
            }).done(function () {
                hideLoder();
            });
        };

        self.getIssuers = function () {
            ShowLoder();

            $.get('/FinanceReports/GetAllIssuersDropDown', function (data) {
                var mappedIssuers = $.map(data, function (item) {
                    return new SelectItem(item.Value, item.Text);
                });
                self.issuers(mappedIssuers);
            }).done(function () {
                hideLoder();
            });
        };

        self.getSuppliers();
        self.getIssuers();

    }

    function SelectItem(value, text) {
        var self = this;
        self.Value = value;
        self.Text = text;
    }

    viewModel = new viewModel();
    ko.applyBindings(viewModel);

});