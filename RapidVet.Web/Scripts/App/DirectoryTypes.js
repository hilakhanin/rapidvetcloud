﻿$(function () {

    function DirectoryType(data) {
        var self = this;
        self.id = ko.observable(data.Id);
        self.name = ko.observable(data.Name);
        self.clinicId = ko.observable(data.ClinicId);
    }


    function viewModel() {

        var self = this;
        self.clinicId = ko.observable($('#clinicId').val());
        self.types = ko.observableArray([]);

        //actions
        self.add = function () {
            self.types.push(new DirectoryType({ 'Id': 0, 'Name': null, 'ClinicId': self.clinicId() }));
        };

        self.save = function (element) {
            ShowLoder();

            $.ajax({
                type: "POST",
                url: '/TelephoneDirectory/SaveDirectoryType',
                data: {
                    'Id': element.id(),
                    'ClinicId': self.clinicId(),
                    'Name': element.name()                 
                },
                success: function (data) {
                    if (data.success && element.id() == 0) {
                        element.id(data.itemId);
                    }
                }
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        };

        self.remove = function (element) {

            if (element.id() > 0) {

                ShowLoder();

                $.ajax({
                    type: "POST",
                    url: '/TelephoneDirectory/DeleteDirectoryType',
                    data: {
                        'Id': element.id(),
                        'ClinicId': self.clinicId(),
                        'Name': element.name()
                        
                    },
                    success: function (data) {
                        if (data.success && element.id() == 0) {
                            element.id(data.ItemId);
                        }
                    }
                }).done(function () {
                    hideLoder();
                    getCookieMessages();
                });
            }

            self.types.remove(element);
        };

        self.getData = function () {

            ShowLoder();

            $.getJSON('/TelephoneDirectory/GetDirectoryTypes', function (data) {

                var mapped = $.map(data, function (item) {
                    return new DirectoryType(item);
                });
                self.types(mapped);
            }).done(function () {
                hideLoder();
            });
        };

        //execution
        self.getData();

    }

    ko.applyBindings(new viewModel());


});