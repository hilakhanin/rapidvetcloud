//fixes input leangth to match design. is used in various screens throughout the app

RapidVet.FixInputWidth = function () {
    init = function () {
        $('.form-inline   .control-group label').each(function () {
            if (!$(this).hasClass('ignore-uniform')) {
                setLable($(this));
            }
        });

        $('.template label').each(function () {
            if (!$(this).hasClass('ignore-uniform')) {
                setLable($(this));
            }
        });
    }

    setLable = function (l) {
        var lWidth = l.width();
        var s = l.find('span');
        var sWidth = s.width();
        var i = l.find('input');
        i.width(lWidth - sWidth - 22);
    }
    return {
        init: init
    };
}();