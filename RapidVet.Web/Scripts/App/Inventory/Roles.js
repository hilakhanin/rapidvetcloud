﻿$(function () {

    function viewModel() {

        var self = this;
        self.inventoryRoles = ko.mapping.fromJS([]);
        self.userId = ko.observable();

        self.getUserInventoryRoles = function () {
            ShowLoder();
            self.inventoryRoles([]);
            $.getJSON('/Users/GetInventoryRoles/' + self.userId(), function (data) {
                ko.mapping.fromJS(data, self.inventoryRoles);
            }).done(function () { hideLoder(); });
        };

        self.showInventoryModal = function (data, event) {
            self.userId($(event.srcElement).children('input').val());
            self.getUserInventoryRoles();
            $('#inventoryModal').modal('show');
        };

        self.saveInventoryRoles = function () {
            ShowLoder();
            $('#inventoryModal').modal('hide');

            $.post('/Users/SaveInventoryRoles',
                { Id: self.userId(), Roles: ko.toJSON(self.inventoryRoles()) }).
                done(function () {
                    hideLoder();
                    getCookieMessages();
                });

        };

    }

    ko.applyBindings(new viewModel());

});