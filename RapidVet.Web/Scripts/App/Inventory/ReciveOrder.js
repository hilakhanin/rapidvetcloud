﻿var vm;

$(function () {

    $('#recieveForm').keypress(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });

    function PriceListItem(data) {
        var self = this;
        self.id = ko.observable(data.Id);
        self.category = ko.observable(data.CategoryName);
        self.name = ko.observable(data.Name);
        self.price = ko.observable(data.Price);
        self.quantity = ko.observable(1);
        self.show = ko.observable(true);
        self.requireBarcode = ko.observable(data.RequireBarcode);
    }

    function OrderedItem(data) {

        var self = this;
        self.orderItemId = ko.observable(data.orderItemId);
        self.id = ko.observable(data.id);
        self.category = ko.observable(data.category);
        self.name = ko.observable(data.name);
        self.unitPrice = ko.observable(data.price);
        self.actualUnitPrice = ko.observable(data.price);
        self.ordered = ko.observable(data.quantity);
        self.recieved = ko.observable(data.quantity);
        self.requireBarcode = ko.observable(data.requireBarcode);
        self.barcode = ko.observable();

    }

    function OrderedItemFromKo(data) {

        var self = this;
        self.orderItemId = ko.observable(0);
        self.id = ko.observable(data.id());
        self.category = ko.observable(data.category());
        self.name = ko.observable(data.name());
        self.unitPrice = ko.observable(data.price());
        self.actualUnitPrice = ko.observable(data.price());
        self.ordered = ko.observable(0);
        self.recieved = ko.observable(data.quantity());
        self.requireBarcode = ko.observable(data.requireBarcode());
        self.barcode = ko.observable();
    }


    function viewModel() {

        var self = this;
        self.orderId = ko.observable($('#Id').val());
        self.supplierId = ko.observable($('#SupplierId').val());
        self.priceListItems = ko.observableArray([]);
        self.orderedItems = ko.observableArray([]);
        self.orderedJson = ko.computed(function () {
            return ko.toJSON(self.orderedItems());
        });
        self.showAllItems = ko.observable(true);
        self.barcodes = ko.computed(function () {
            var result = false;

            var items = ko.utils.arrayFilter(self.orderedItems(), function (i) {
                return i.requireBarcode() == true;
            });

            if (items != null) {
                result = true;
            }

            return result;
        });

        //functions

        self.getAllItems = function () {

            $.getJSON('/Inventory/GetPriceListItemsBySupplier?supplierId=' + self.supplierId(), function (data) {
                var mapped = $.map(data, function (item) {
                    return new PriceListItem(item);
                });
                self.priceListItems(mapped);
            });
        };

        self.getOrderesItems = function () {

            $.getJSON('/Inventory/GetOrderItems/' + self.orderId(), function (data) {
                var mapped = $.map(data, function (item) {
                    return new OrderedItem(item);
                });
                self.orderedItems(mapped);
            });

        };

        self.showItemsModel = function () {
            if (self.priceListItems().length > 0) {
                $('#itemsModal').modal('show');
            }
        };

        self.havePrice = function (element) {
            var result = false;

            if (element.Price() > 0 || (element.Price() < 1) && self.showAllItems() == true) {
                result = true;
            }
            return result;
        };

        self.add = function (element) {
            self.orderedItems.push(new OrderedItemFromKo(element));
            $('#itemsModal').modal('hide');
        };

        self.remove = function (element) {
            self.orderedItems.remove(element);
        };
        


        self.init = function () {
            ShowLoder();
            self.getAllItems();
            self.getOrderesItems();
            hideLoder();
        };

        //execution
        self.init();
    }

    vm = new viewModel();
    ko.applyBindings(vm);

});