﻿var vm;

$(function () {
           

    function OrderedItem(data) {

        var self = this;
        self.orderItemId = ko.observable(data.orderItemId);
        self.id = ko.observable(data.id);
        self.category = ko.observable(data.category);
        self.catalog = ko.observable(data.catalog);
        self.name = ko.observable(data.name);
        self.unitPrice = ko.observable(data.price + $('#Currency').val());
        self.actualPrice = ko.observable(data.actualUnitPrice + $('#Currency').val());
        self.quantity = ko.observable(data.quantity);
        self.arrivedQuantity = ko.observable(data.recieved);
        self.total = ko.computed(function () {
            if (vm.recieved()) {
                return parseFloat(self.arrivedQuantity()) * parseFloat(data.actualUnitPrice) + $('#Currency').val();
            }
            else {
                return self.quantity() * data.price + $('#Currency').val();
            }
        });

    }

    

    function viewModel() {

        var self = this;
        self.orderId = ko.observable($('#OrderId').val());
        self.currency = ko.observable($('#Currency').val());
        self.priceListItems = ko.observableArray([]);
        self.orderedItems = ko.observableArray([]);
        self.orderedJson = ko.computed(function () {
            return ko.toJSON(self.orderedItems());
        });
        self.showAllItems = ko.observable(true);
        self.recieved = ko.observable(false);


        self.totalOrder = ko.computed(function () {
            var totalSum = 0;

            for (var i = 0; i < self.orderedItems().length; i++) {
                totalSum += parseFloat(self.orderedItems()[i].total());
            }

            return totalSum + self.currency();
        });

        //functions

        self.getOrderesItems = function () {

            $.getJSON('/Inventory/GetOrderItems/' + self.orderId(), function (data) {
                var mapped = $.map(data, function (item) {
                    return new OrderedItem(item);
                });
              
                self.orderedItems(mapped);
            });

        };

        self.getOrder = function () {
            
            $.getJSON('/Inventory/IsOrderRecieved/' + self.orderId(), function (isRecieved) {
               
                self.recieved(isRecieved);
              
            });

        };

        self.init = function () {
            ShowLoder();
            self.getOrder();
            self.getOrderesItems();
            hideLoder();
        };

        //execution
        self.init();
    }

    vm = new viewModel();
    ko.applyBindings(vm);

});