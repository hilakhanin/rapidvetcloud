﻿$(function () {

    function viewModel() {

        var self = this;
        self.visitId = ko.observable($('#visitId').val());

        self.visitItems = ko.mapping.fromJS([]);

        self.priceListItems = ko.mapping.fromJS([]);


        self.getVisitItems = function () {
            $.getJSON('/Inventory/GetVisitInventoryPriceListItems/' + self.visitId(), function (data) {
                ko.mapping.fromJS(data, self.visitItems);
            });
        };

        self.getInventoryPriceListItems = function () {
            $.getJSON('/Inventory/GetAllInventoryRelatedPriceListItems', function (data) {
                ko.mapping.fromJS(data, self.priceListItems);
            });
        };

        self.init = function () {
            ShowLoder();

            self.getVisitItems();
            self.getInventoryPriceListItems();

            hideLoder();
        };

        self.showModal = function () {
            $('#itemsModal').modal('show');
        };

        self.add = function (element) {
            self.visitItems.push(element);
            $('#itemsModal').modal('hide');
        };

        self.remove = function (element) {
            self.visitItems.remove(element);
        };

        self.closeWindow = function () {
            window.open('', '_self', '');
            window.close();
        };

        self.save = function () {
            //(int visitId, string items)
            ShowLoder();

            $.post('/Inventory/UpdateAfterVisit',
                { VisitId: self.visitId(), Items: ko.toJSON(self.visitItems()) })
                .done(function(data) {
                    if (data) {
                        self.closeWindow();
                    } else {
                        hideLoder();
                        getCookieMessages();
                    }
                });
        };

        //executions

        self.init();
    }

    ko.applyBindings(new viewModel());
});

function closeWindow() {

}

