﻿$(function () {


    function viewModel() {

        var self = this;
        self.from = ko.observable($('#from').val());

        self.to = ko.observable($('#to').val());

        self.priceListItems = ko.mapping.fromJS([]);
        self.priceListItemId = ko.observable();

        self.results = ko.mapping.fromJS([]);

        self.increses = ko.computed(function () {
            var result = 0;
            var items = ko.utils.arrayFilter(self.results(), function (r) {
                return (r.ActionId() == 0 || r.ActionId() == 2) && r.Quantity() > 0;
            });
            if (items != null) {
                ko.utils.arrayForEach(items, function (i) {
                    result += parseInt(i.Quantity());
                });
            }

            return result;
        });

        self.decreases = ko.computed(function () {
            var result = 0;
            var items = ko.utils.arrayFilter(self.results(), function (r) {
                return (r.ActionId() == 1 || r.ActionId() == 2) && r.Quantity() < 0;
            });
            if (items != null) {
                ko.utils.arrayForEach(items, function (i) {
                    result += parseInt(i.Quantity() * -1);
                });
            }
            return result;
        });

        self.currentInventoryStatus = ko.observable(0);

        self.getCurrentInventoryStatus = function () {

            if (self.showCurrentStatus()) {
                $.getJSON('/Inventory/GetCurrentInventoryQuantity/' + self.priceListItemId(), function(data) {
                    self.currentInventoryStatus(data);
                });
            }
        };

        self.showCurrentStatus = ko.computed(function () {
            return typeof self.priceListItemId() != "undefined" && self.priceListItemId() > 0;
        });

        self.showSummery = ko.computed(function () {
            var result = false;
            if (typeof self.results() != "undefined" && self.results().length > 0) {
                result = true;
            }
            return result;
        });

        //functions
        self.getItems = function () {

            ShowLoder();
            $.getJSON('/Inventory/GetInventoryPriceListItems', function (data) {
                ko.mapping.fromJS(data, self.priceListItems);
            }).done(function () { hideLoder(); });
        };

        self.search = function () {

            ShowLoder();
            $.post('/Inventory/GetStatusReportData',
                {
                    PriceListItemId: self.priceListItemId(),
                    From: self.from(),
                    To: self.to()
                }, function (data) {
                    ko.mapping.fromJS(data, self.results);
                }).done(function () {
                    hideLoder();
                    getCookieMessages();
                });

            self.getCurrentInventoryStatus();

        };

        //execution
        self.getItems();

    }

    ko.applyBindings(new viewModel());


});