﻿$(function () {


    function viewModel() {

        var self = this;
        self.categories = ko.mapping.fromJS([]);

        //functions

        self.getCategories = function () {

            ShowLoder();

            $.getJSON('/Inventory/GetCategories', function (data) {
                ko.mapping.fromJS(data, self.categories);
            }).done(function () {
                hideLoder();
            });
        };

        self.save = function () {
            ShowLoder();
            $.post('/Inventory/Settings', { data: ko.toJSON(self.categories()) }, function (data) {
            }).done(function () {
                hideLoder();
                getCookieMessages();
            });
        };
        
        //execution

        self.getCategories();

    }

    ko.applyBindings(new viewModel());

});