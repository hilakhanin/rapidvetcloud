﻿$(function () {

    function viewModel() {

        var self = this;
        self.id = ko.observable($('#priceListItemId').val());
        self.suppliers = ko.mapping.fromJS([]);


        //functions

        self.getSuppliers = function () {
            ShowLoder();

            $.getJSON('/Inventory/GetSuppliers/' + self.id(), function (data) {
                ko.mapping.fromJS(data, self.suppliers);
            }).done(function () {
                hideLoder();
            });
        };


        self.save = function () {

            var manufacturer = $('#Manufacturer').val();
            var catalog = $('#Catalog').val();
            var barcode = $('#Barcode').val();
            var itemsInUnit = $('#ItemsInUnit').val();
            var defaultConsumption = $('#DefultConsuptionAmount').val();
            var requireBarcode = $('#RequireBarcode').prop('checked');
            var inventoryActive = $('#InventoryActive').prop('checked');
     
            
            ShowLoder();
            $.post('/Inventory/SaveItem', {
                Id: self.id(),
                Manufacturer: manufacturer,
                Catalog: catalog,
                Barcode: barcode,
                ItemsInUnit: itemsInUnit,
                DefultConsuptionAmount: defaultConsumption,
                RequireBarcode: requireBarcode,
                InventoryActive: inventoryActive,
                Suppliers: ko.toJSON(self.suppliers())
            }).done(function (data) {
                if (data) {
                    window.location.href = '/Inventory/Index';
                } else {
                    hideLoder();
                    getCookieMessages();
                }
            });
        };

        //execution
        self.getSuppliers();

    }

    ko.applyBindings(new viewModel());
});