﻿var vm;

$(function () {


    function PriceListItem(data) {
        var self = this;
        self.id = ko.observable(data.Id);
        self.category = ko.observable(data.CategoryName);
        self.name = ko.observable(data.Name);
        self.price = ko.observable(data.Price );
        self.quantity = ko.observable(1);
        self.total = ko.computed(function () { return parseFloat(self.price()) * self.quantity()  });


        self.show = ko.computed(function() {
            var result = false;
            
            if (self.price() > 0 || (self.price() < 1 && vm.showAllItems() == true)) {
                result = true;
            }

            return result;
        });
    }

    function viewModel() {

        var self = this;
        self.suppliers = ko.mapping.fromJS([]);
        self.supplierId = ko.observable();

        self.priceListItems = ko.observableArray([]);
        self.selectedItems = ko.observableArray([]);

        self.comment = ko.observable();
        self.showAllItems = ko.observable(false);
        self.employeeName = ko.observable();

        self.totalOrder = ko.computed(function () {
            var totalSum = 0;

            for (var i = 0; i < self.selectedItems().length; i++) {
                totalSum += parseFloat(self.selectedItems()[i].total());
            }

            return totalSum + $('#Currency').val();
        });
        //function

        self.getSuppliers = function () {
            $.getJSON('/Inventory/GetClinicSuppliers', function (data) {
                ko.mapping.fromJS(data, self.suppliers);
            });
        };

        self.getItems = function () {
            ShowLoder();
            $.getJSON('/Inventory/GetPriceListItemsBySupplier?supplierId=' + self.supplierId(), function (data) {
                var mapped = $.map(data, function (item) {
                    return new PriceListItem(item);
                });
                self.priceListItems(mapped);
            }).done(function () { hideLoder(); });
        };

        self.selectionChanged = function () {
            self.selectedItems([]);
        };

        self.priceListOptions = ko.computed(function () {
            if (typeof self.supplierId() != "undefined") {
                self.priceListItems([]);
                self.getItems();
            }
        });

        self.showItemsModel = function () {
            if (self.priceListItems().length > 0) {
                $('#itemsModal').modal('show');
            }
        };

        self.havePrice = function (element) {
            var result = false;

            if (element.Price() > 0 || (element.Price() < 1) && self.showAllItems() == true) {
                result = true;
            }
            return result;
        };

        self.add = function (element) {
            self.selectedItems.push(element);
            $('#itemsModal').modal('hide');
        };

        self.remove = function (element) {
            self.selectedItems.remove(element);
        };

        self.save = function () {
            $.post('/Inventory/CreateOrder',
                {
                    SupplierId: self.supplierId(),
                    Comment: self.comment(),
                    Items : ko.toJSON(self.selectedItems())
                }).done(function (data) {
                if (data) {
                    window.location.href = "/Inventory/Orders";
                } else {
                    hideLoder();
                    getCookieMessages();
                }
            });
        };

        self.init = function () {

            ShowLoder();
            self.getSuppliers();
            hideLoder();
        };

        //execution
        self.init();

    }

    vm = new viewModel();
    ko.applyBindings(vm);


});