﻿$(function () {


    function viewModel() {

        var self = this;
        self.priceListItems = ko.mapping.fromJS([]);
        self.priceListItemId = ko.observable().extend({ required: { params: true, message: "שדה חובה" } });

        self.getItems = function () {

            ShowLoder();
            $.getJSON('/Inventory/GetInventoryPriceListItems', function (data) {
                ko.mapping.fromJS(data, self.priceListItems);
            }).done(function () { hideLoder(); });
        };
        self.Errors = ko.validation.group(self);

        //validation
        self.isValid = function () {
            if (self.Errors().length == 0) {
                return true;
            }
            else {
                self.Errors.showAllMessages(true);
                return false;
            }
        };
        //execution
        self.getItems();
    }

    ko.applyBindings(new viewModel());
});