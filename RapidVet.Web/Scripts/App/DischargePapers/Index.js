﻿$(function () {

    function viewModel() {

        var self = this;
        self.from = ko.observable($('#from').val());
        self.revisedFrom = ko.computed(function () {
            return replaceDateSeperator(self.from());
        });

        self.to = ko.observable($('#to').val());
        self.revisedTo = ko.computed(function () {
            return replaceDateSeperator(self.to());
        });

        self.searchLink = ko.computed(function () {
            return '/DischargePapers/Index?fromDate=' + self.revisedFrom() + '&toDate=' + self.revisedTo();
        });

        self.printAllLink = ko.computed(function () {
            return '/DischargePapers/PrePrint?fromDate=' + self.revisedFrom() + '&toDate=' + self.revisedTo();
        });

    }

    ko.applyBindings(new viewModel());
});