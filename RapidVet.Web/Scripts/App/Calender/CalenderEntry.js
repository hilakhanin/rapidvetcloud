
var pendingWaitingItemId = -1;
var currentPatientVal = null;
// helper function for format

var vm;
var dataRow;

//$('.li-group-cb').change(function() {    
//    $.ajax({
//        url:'/Calender/ShowCalendarChanged',
//        data: { Val: $(this)[0].checked, Id: $('#' + $(this)[0].id.replace('Show', 'ID'))[0].value },
//        });
//});

function showHideCalendarsToView() {//Edit profile calendar visibility setting
    $('#showUsersSettings')[0].className = $('#showUsersSettings')[0].className == 'hide' ? 'bubble' : 'hide';
    if ($('#showUsersSettings')[0].className == 'hide')// submit after hide in order to refresh
        $("form").submit();
}


function isEmptyOrWhiteSpaces(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

function clearClientSearch(element) {
    vm.clientId(undefined);
    vm.ClientNameWithPatients("");
    element.value = "";
    $(".no-results").hide();
};

$(function () {

    $("#from").datepicker({
        defaultDate: "+0",
        dateFormat: 'dd/mm/yy',
        onClose: function (selectedDate) {
            $("#to").datepicker("option", "minDate", selectedDate);
            //setAttributes();
        },
        isRTL: true
    });
    $("#to").datepicker({
        defaultDate: "+0",
        dateFormat: 'dd/mm/yy',
        onClose: function (selectedDate) {
            $("#from").datepicker("option", "maxDate", selectedDate);
            //setAttributes();
        },
        isRTL: true
    });
    
    $(".client-search").autocomplete({        
        source: function (request, response) {
            
            $.ajax({
                url: "/Clients/Search",
                type: "POST",
                data: {
                    query: request.term
                },
                success: function (data) {
                    response(data);

                    if (data.length == 0) {
                        $(".no-results").show();
                        setTimeout(function () { $(".no-results").hide(); }, 5000);
                    }
                    else {                       
                        $(".no-results").hide();
                    }
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            event.preventDefault();
            self.ClientNameWithPatients(ui.item.label);
            self.clientId(ui.item.value.toString());
            this.value = ui.item.label;

            //window.location = "/Clients/Patients/" + ui.item.value;
        },
        focus: function (event, ui) {
            event.preventDefault();
            this.value = ui.item.label;
        }
        ,
        change: function (event, ui) {
            event.preventDefault();
            if (isEmptyOrWhiteSpaces(this.value) ||
                typeof vm.clientId() == "undefined" || vm.ClientNameWithPatients() != this.value) {
                clearClientSearch(this);
            }

        },
        open: function () {
            $(".ui-autocomplete").css("max-height", 120);
            $(".ui-autocomplete").css("max-width", 225);
            $(".ui-autocomplete").css("overflow-y", "auto");
            $(".ui-autocomplete").css("overflow-x", "hidden");

        }
    });
});
// helper function for format
Date.prototype.format = function (format) //author: meizz
{
    var o = {
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(),    //day
        "h+": this.getHours(),   //hour
        "m+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter
        "S": this.getMilliseconds() //millisecond
    };

    if (/(y+)/.test(format))
        format = format.replace(RegExp.$1,
            (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1,
                RegExp.$1.length == 1 ? o[k] :
                    ("00" + o[k]).substr(("" + o[k]).length));
    return format;
};

// mapper for list item
function SelectItem(value, text) {
    var self = this;
    self.itemValue = value;
    self.itemText = text;
}

function viewModel() {
    var self = this;
    vm = self;
    // calenderEntry props
    self.entryId = ko.observable();
    self.date = ko.observable().extend({ required: { params: true, message: "שדה חובה" } });
    self.time = ko.observable().extend({ required: { params: true, message: "שדה חובה" } });
    self.duration = ko.observable().extend({
        required: { params: true, message: "שדה חובה" },
        number: { params: true, message: "יש להזין ערך תקין" },
        min: { params: 5, message: "ערך מינימלי: 5" }
    });
    self.roomId = ko.observable();
    self.generalEntry = ko.observable();
    self.doctorId = ko.observable();
    self.byUserId = ko.observable();
    self.clientId = ko.observable(undefined).extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return !self.generalEntry()
            }
        }
    });
    self.patientId = ko.observable();
    self.comments = ko.observable();
    self.color = ko.observable();
    self.title = ko.observable().extend({
        required: {
            message: "שדה הכרחי",
            onlyIf: function () {
                return self.generalEntry()
            }
        }
    });
    self.isNewClient = ko.observable();
    self.isNoShow = ko.observable();


    // metadata props
    self.defaultDuration = ko.observable();
    self.rooms = ko.observable([]);
    self.users = ko.observable([]);
    self.doctors = ko.observable([]);
    self.clients = ko.observable([]);
    self.ClientNameWithPatients = ko.observable("");
    self.patients = ko.mapping.fromJS([]);
    self.currentUserId = ko.observable();

    //blockedDays
    self.blockedDays = ko.mapping.fromJS([]);

    // search calenderEntries props
    self.searchResults = ko.mapping.fromJS([]);
    self.searchTimeType = ko.observable();
    self.searchCancelled = ko.observable();

    // waitinglist props
    self.waitingListItems = ko.mapping.fromJS([]);

    // waitinglistItem edit/create
    self.waitingListItem = ko.observable();
    self.deleteWaitingListItemId = ko.observable();

    // jewishDate
    self.JewishDate = ko.observable();

    //errors
    self.errorMsg = ko.observable('');

    //waiting list item pop up
    self.WaitingListItemId = ko.observable(0);
    self.FromTime = ko.observable("07:00");
    self.ToTime = ko.observable("20:00");
    self.from = ko.observable($('#from').val());
    self.revisedFrom = ko.computed(function () {
        return replaceDateSeperator(self.from());
    });

    self.to = ko.observable($('#to').val());
    self.revisedTo = ko.computed(function () {
        return replaceDateSeperator(self.to());
    });
    self.AppointmentDurationInMinutes = ko.observable();
    self.WaitingListItemDoctorId = ko.observable("");
    self.WaitingListItemUrgencyTypeId = ko.observable("");
    self.WaitingListItemComments = ko.observable("");
    self.IsSunday = ko.observable(false);
    self.IsMonday = ko.observable(false);
    self.IsTuesday = ko.observable(false);
    self.IsWednesday = ko.observable(false);
    self.IsThursday = ko.observable(false);
    self.IsFriday = ko.observable(false);
    self.IsSaturday = ko.observable(false);

    //---------
    //functions
    //---------

    // get meta data
    self.getMetaData = function (newClientId, newClientName) {
        $.post('/Calender/CalenderEntryMetaData', function (data) {
            var mappedRooms = $.map(data.Rooms, function (item) {
                return new SelectItem(item.Id, item.Name);
            });
            self.rooms(mappedRooms);

            var mappedUsers = $.map(data.Users, function (item) {
                return new SelectItem(item.Id, item.Name);
            });
            self.users(mappedUsers);

            var mappedDoctors = $.map(data.Doctors, function (item) {
                return new SelectItem(item.Id, item.Name);
            });
            self.doctors(mappedDoctors);

            //var mappedClients = $.map(data.Clients, function (item) {
            //    return new SelectItem(item.Id, item.NameWithPatients);
            //});
            //self.clients(mappedClients);

            self.clientId.subscribe(function () {
                self.getPatients();
            });
            
            if (newClientId != null) {
                self.setNewClient(data.ClientId, data.ClientName);
            }
            //
            
            //self.clientIdForSet = data.ClientId;
            //vm.clientId(self.clientIdForSet);
            setTimeout(function () {
                $('.txtClientName')[0].placeholder = "חיפוש";
                try {

                    if ($('.txtClientName').val().length == 0) {
                        if (data.ClientName == "") {

                            $('.txtClientName').val(decodeURIComponent(data.ClientName).split('+').join(' ').split('[object HTMLInputElement]').join('').toString().trim());

                            self.setNewClient(self.clientId, self.ClientNameWithPatients.split('[object HTMLInputElement]').join('').toString().trim());
                        }
                        else {
                            dataRow = data.ClientId + ";" + decodeURIComponent(data.ClientName).split('+').join(' ');
                            $('.txtClientName').val(decodeURIComponent(data.ClientName).split('+').join(' ').split('[object HTMLInputElement]').join('').toString().trim());

                            self.setNewClient(data.ClientId, decodeURIComponent(data.ClientName).split('+').join(' ').toString().trim());
                        }
                    }
                }
                catch (e) {
                }

            }, 3000);
            
            setTimeout(function () {
                $('.txtClientName')[0].placeholder = "חיפוש";
            }, 3500);
            setTimeout(function () {
                $('.txtClientName')[0].placeholder = "חיפוש";
            }, 4500);
            setTimeout(function () {
                $('.txtClientName')[0].placeholder = "חיפוש";                
            }, 5500);
            setTimeout(function () {
                $('.txtClientName')[0].placeholder = "חיפוש";               
            }, 6500);
            self.currentUserId(data.CurrentUser);
            self.defaultDuration(data.DefaultDuration);
            self.AppointmentDurationInMinutes(data.DefaultDuration);
            self.generalEntry(data.isGeneralEntry);

        }, 'json');
        
    };

    // get patients for selected clientId
    self.getPatients = function () {
        self.patients([]);
        if (typeof (self.clientId()) != "undefined" && self.clientId() != null) {
            getPatientsByID(self.clientId());
        }
    };

    self.getPatientsByID = function (clientID) {
        self.patients([]);
        $.get('/Clients/GetClientPatientsAndColor?clientId=' + clientID, function (data) {
            ko.mapping.fromJS(data.Patients, self.patients);
            if (self.patients().length == 1) {
                self.patientId(data.Patients[0].Value);
            }
            else if (currentPatientVal != null) {
                self.patientId(currentPatientVal)
                currentPatientVal = null;
            }

            self.color(data.Color);
            $('.simpleColor').setColor(data.Color);
        });
    };


    self.invoiceLinkVisible = ko.computed(function () {
        var result = false;
        if (self.clientId() != 'undefined' && self.clientId() > 0) {
            result = true;
        }
        return result;
    });

    self.invoiceLink = ko.computed(function () {
        return '/FinanceDocument/InvoiceReceipt?clientId=' + self.clientId();
    });

    // get blockedDays for selected doctorId
    self.getBlockedDays = function () {
        self.blockedDays([]);
        if ($('#SelectedDoctorId').val() != "undefined") {
            $.get('/Calender/GetDocsBlockedDays?doctorId=' + $('#SelectedDoctorId').val(), function (data) {
                for (var i = 0; i < data.length; i++) {
                    if (String(data[i].start).indexOf('/Date(') == 0) {
                        data[i].start = new Date(parseInt(data[i].start.replace(/\/Date\((.*?)\)\//gi, "$1")));
                    }
                    if (String(data[i].end).indexOf('/Date(') == 0) {
                        data[i].end = new Date(parseInt(data[i].end.replace(/\/Date\((.*?)\)\//gi, "$1")));
                    }
                }
                ko.mapping.fromJS(data, self.blockedDays);
            });
        }
    };

    // reset details for new calenderEntry
    self.createCalenderEntry = function (date, docId, roomId, isWeekly) {
        $('.simpleColor').setColor('#33cccc');

        self.entryId(0);
        self.date(date.format('DD/MM/YYYY'));
        self.time(date.format('HH:mm'));
        self.duration(self.defaultDuration());
        if (roomId != null) {
            self.roomId(roomId);
        } else {
            self.roomId(null);
        }
        if (docId != null) {
            self.doctorId(docId);
        } else {
            self.doctorId(null);
        }
        self.byUserId(self.currentUserId());
        self.clientId(null);
        self.patientId(null);
        self.comments(null);
        self.color('#33cccc');
        self.title(null);
        self.isNoShow(false);
        self.isNewClient(false);
        self.generalEntry(false);

        if ($('#defaultDr').val() > 0 && !isWeekly) {
            self.doctorId($('#defaultDr').val());
        }

        self.ClientNameWithPatients("");
        $(".client-search").val("");

    };

    // get from server calenderEntry and load them to view model
    self.getCalenderEntry = function (id) {
        $.post('/Calender/GetCalenderEntry', {
            calenderEntryId: id
        }, function (data) {            
            $('.simpleColor').setColor(data.color);
            self.entryId(data.entryId);
            self.date(data.date);
            self.time(data.time);
            self.duration(data.duration);
            self.roomId(data.roomId);
            self.doctorId(data.doctorId);
            self.byUserId(data.byUserId);
            self.clientId(data.clientId);
            self.patientId(data.patientId);
            currentPatientVal = data.patientId;
            self.comments(data.comments);
            self.color(data.color);
            self.title(data.title);
            self.isNoShow(data.isNoShow);
            self.isNewClient(data.isNewClient);
            self.generalEntry(data.isGeneralEntry);
            if (!self.generalEntry()) {
                self.ClientNameWithPatients(data.clientName);
                $(".client-search").val(data.clientName);
            }

        }, 'json');
    };

    // save or update the current calender entry in view model
    self.saveCalenderEntry = function () {
        if (self.Errors().length == 0) {
            var calenderEntry = {
                entryId: self.entryId(),
                date: self.date(),
                time: self.time(),
                duration: self.duration(),
                roomId: self.roomId(),
                doctorId: self.doctorId(),
                byUserId: self.byUserId(),
                clientId: self.clientId(),
                patientId: self.patientId(),
                comments: self.comments(),
                color: self.color(),
                title: self.title(),
                isNewClient: self.isNewClient(),
                isNoShow: self.isNoShow(),
                isGeneralEntry: self.generalEntry()
            };

            $.post('/Calender/SaveCalenderEntry/', {
                calenderEntryString: JSON.stringify(calenderEntry)
            },
                function (data) {
                    if (data) {
                        $('#myModal').modal('hide');
                        $('.calender').fullCalendar('refetchEvents');
                        if (pendingWaitingItemId > 0) {
                            $.ajax({
                                url: '/Calender/RemoveWaitingListItem/',
                                type: 'POST',
                                data: { waitingListItemId: pendingWaitingItemId }
                            });
                            pendingWaitingItemId = -1;
                        }
                    }
                    getCookieMessages();
                }, 'json');
        }
        else {
            self.Errors.showAllMessages(true);
        }
    };

    // change color
    self.changeSelfColor = function (hex) {        
        self.color('#' + hex);
    };

    // duplicate calenderEntry
    self.changeIdToZero = function () {
        self.entryId(0);
        self.date(null);
        self.time(null);
        $('.modal-body').scrollTop(0);
    };

    // update movement of calenderEntry in calender to server
    self.updateChangedCalenderEntry = function (event) {
        $.post('/Calender/UpdateCalenderEntryMovement/', {
            calenderEntryId: event.id, dateStart: event.start.toString(), dateEnd: event.end.toString()
        });
    };

    // check if clicked date or moved calenderentry is on blocked day
    self.dateIsBlocked = function (doctorId, dateStart, dateEnd, eventId) {
        var isBlocked = false;
        $.ajax({
            url: '/Calender/IsDateRangeBlockedForDoctor/',
            type: 'POST',
            async: false,
            data:
            {
                doctorId: doctorId,
                dateStart: dateStart.toString(),
                dateEnd: dateEnd.toString(),
                cEntryId: eventId
            },
            success: function (result) {
                isBlocked = result;
            }
        });
        getCookieMessages();
        return isBlocked;
    };

    // check if clicked date or moved calenderentry is on blocked day
    self.getBlockedDayId = function (doctorId, date) {
        var blockDayId = 0;
        $.ajax({
            url: '/Calender/GetBlockedIdForDoctor/',
            type: 'POST',
            async: false,
            data:
            {
                doctorId: doctorId,
                date: date,
            },
            success: function (result) {
                blockDayId = result;
            }
        });
        return blockDayId;
    };

    self.locateEventClientIDEntries = function (clientID, clientName) {
        clearSearchData();
        self.ClientNameWithPatients(clientName); //not working by this single line
        $('#inputClientNameSearch').val(clientName);
        self.clientId(clientID);
        $('#searchModal').modal('show');
        searchCalenderEntries();
    };

    // search for calenderEntries
    self.searchCalenderEntries = function () {
        if (typeof self.clientId() != "undefined" && self.clientId() != null) {
            $("#chooseClientError").hide();
            self.searchResults([]);
            $.ajax({
                url: '/Calender/SearchCalenderyEntries/',
                type: 'POST',
                data:
                {
                    doctorId: self.doctorId(),
                    clientId: self.clientId(),
                    searchTimeType: self.searchTimeType(),
                    searchCancelled: self.searchCancelled(),
                    fromDate: self.from(),
                    toDate: self.to()
                },
                success: function (data) {
                    if (data.length == 0) {
                        alert("לא נמצאו תוצאות");
                    }
                    else {
                        ko.mapping.fromJS(data, self.searchResults);
                    }
                }
            });
        }
        else {
            $("#chooseClientError").show();
        }
    };

    self.printCalenderEntries = function () {
        if (typeof self.clientId() != "undefined" && self.clientId() != null) {
            $("#chooseClientError").hide();

            window.open('/calender/PrintCalenderEntries?doctorId=' + self.doctorId() + '&clientId=' + self.clientId() + '&searchTimeType=' + self.searchTimeType() + '&fromDate=' + self.from() + '&toDate=' + self.to(), '_blank');
        }
        else {
            $("#chooseClientError").show();
        }
    };

    self.exportCalenderEntries = function () {
        if (typeof self.clientId() != "undefined" && self.clientId() != null) {
            $("#chooseClientError").hide();

            window.open('/calender/DownloadCsv?doctorId=' + self.doctorId() + '&clientId=' + self.clientId() + '&searchTimeType=' + self.searchTimeType() + '&fromDate=' + self.from() + '&toDate=' + self.to(), '_blank');
        }
        else {
            $("#chooseClientError").show();
        }
    };

    // clear search data input
    self.clearSearchData = function () {
        self.searchResults([]);
        self.clientId(null);
        self.doctorId(null);
        self.searchTimeType(0);
        self.searchCancelled(0);
        vm.ClientNameWithPatients("");
        $(".client-search").val("");
    };

    // delete calenderEntry
    self.deleteCalenderEntry = function (calenderEntryId) {
        $.ajax({
            url: '/Calender/RemoveCalenderEntry/',
            type: 'POST',
            data:
            {
                calenderEntryId: calenderEntryId
            },
            success: function (data) {
                $('#myModal').modal('hide');
                $('.calender').fullCalendar('refetchEvents');
                searchCalenderEntries();
                if (data == 0) {
                    return;
                }
                else {
                    ko.mapping.fromJS(data, self.waitingListItems);
                    showwaitingListModal(true);
                }
            }
        });
    };

    self.showwaitingListModal = function (isFromSuggestions) {
        var className = isFromSuggestions ? 'hide' : '';
        var classNameRewind = isFromSuggestions ? '' : 'hide';
        var standartButtom = 'btn btn-primary waitingBtn ';
        document.getElementById('waitingListHeaderButtons').className = className;
        setClassNameForElementsName('waitingListDayColVal', className);
        setClassNameForElementsName('waitingListDayColHeader', className);
        setClassNameForElementsName('waitingListHomePhoneColVal', className);
        setClassNameForElementsName('waitingListHomePhoneColHeader', className);
        setClassNameForElementsName('waitingListWorkPhoneColVal', className);
        setClassNameForElementsName('waitingListWorkPhoneColHeader', className);
        setClassNameForElementsName('waitingListMobilePhoneColVal', className);
        setClassNameForElementsName('waitingListMobilePhoneColHeader', className);
        setClassNameForElementsName('waitingListExitWaitingRoom', standartButtom + className);
        setClassNameForElementsName('waitingListEdit', standartButtom + className);
        setClassNameForElementsName('waitingListDelete', standartButtom + className);
        setClassNameForElementsName('waitingListAddToMeeting', standartButtom + classNameRewind);
        setClassNameForElementsName('waitingListWaitingFromColVal', classNameRewind);
        setClassNameForElementsName('waitingListWaitingFromColHeader', classNameRewind);
        //setClassNameForElementsName('waitingListPatienNameHeader', classNameRewind);
        //setClassNameForElementsName('waitingListPatienNameVal', classNameRewind);
        $('#waitingListTitle').text(isFromSuggestions ? 'הצעות לממתינים שנמצאו מתאימות לזמן שנמחק' : 'ממתינים');
        $('#waitingListModal').modal('show').css({ 'width': '70%', 'margin-right': function () { return -($(this).width() / 2); } });
        self.doctorId(null);
        if (!isFromSuggestions) {
            return;
        }
        self.generalEntry(false);
    };

    self.setClassNameForElementsName = function (name, cn) {
        var elements = document.getElementsByName(name);
        for (var i = 0; i < elements.length; i++) {
            elements[i].className = cn;
        }
    };

    // get all waitinglist items for clinic
    self.getAllWaitingListItems = function () {
        self.waitingListItems([]);
        self.doctorId(null);
        $.ajax({
            url: '/Calender/GetWaitingListItems/',
            type: 'POST',
            success: function (data) {
                ko.mapping.fromJS(data, self.waitingListItems);
            }
        });
    };

    // edit waiting list item
    self.editWaitingListItem = function (item) {
        //  self.waitingListItem(item);
        self.clientId(item.ClientId());

        $.getJSON('/Clients/GetClientIdCard?clientId=' + self.clientId(), function (data) {
            self.ClientNameWithPatients(data.clientName);
            $(".client-search").val(data.clientName);
            self.WaitingListItemId(item.Id());
            self.patientId(item.PatientId());
            self.FromTime(item.FromTime());
            self.ToTime(item.ToTime());
            self.AppointmentDurationInMinutes(item.AppointmentDurationInMinutes());
            self.WaitingListItemDoctorId(item.DoctorId());
            self.WaitingListItemUrgencyTypeId(item.UrgencyTypeId());
            self.WaitingListItemComments(item.Comments());
            self.IsSunday(item.IsSunday());
            self.IsMonday(item.IsMonday());
            self.IsTuesday(item.IsTuesday());
            self.IsWednesday(item.IsWednesday());
            self.IsThursday(item.IsThursday());
            self.IsFriday(item.IsFriday());
            self.IsSaturday(item.IsSaturday());
            $('#waitingListModal').modal('hide');
            $('#waitingListItemModal').modal('show');
        });
    };

    // create new waiting list item
    self.createNewWaitingListItem = function () {
        self.WaitingListItemId(0);
        self.clientId(undefined);
        self.patientId(undefined);
        self.ClientNameWithPatients("");
        $(".client-search").val("");
        self.FromTime("07:00");
        self.ToTime("22:00");
        self.AppointmentDurationInMinutes(self.defaultDuration());
        self.WaitingListItemDoctorId(undefined);
        self.WaitingListItemUrgencyTypeId("0");
        self.WaitingListItemComments("");
        self.IsSunday(false);
        self.IsMonday(false);
        self.IsTuesday(false);
        self.IsWednesday(false);
        self.IsThursday(false);
        self.IsFriday(false);
        self.IsSaturday(false);

        $('#waitingListModal').modal('hide');
        $('#waitingListItemModal').modal('show');
        //self.waitingListItem({
        //    Id: function () { return 0; },
        //    ClientId: "",
        //    PatientId: "",
        //    FromTime: "07:00",
        //    ToTime: "20:00",
        //    AppointmentDurationInMinutes: self.defaultDuration(),
        //    DoctorId: "",
        //    UrgencyTypeId: "0",
        //    Comments: "",
        //    IsSunday: false,
        //    IsMonday: false,
        //    IsTuesday: false,
        //    IsWednesday: false,
        //    IsThursday: false,
        //    IsFriday: false,
        //    IsSaturday: false,
        //});
        //$('.time').timepicker();
        //$('.time').timepicker('option', {
        //    'timeFormat': 'H:i',
        //    'step': 30,
        //    'minTime': '9:00am',
        //    'disableTimeRanges': [],
        //});
    };


    // save or update waiting list item
    self.saveWaitingListItem = function () {
        $("#appointmentLength").val(parseInt($("#appointmentLength").val(), 10));
        if (self.clientValidation() && self.daysValidation() && self.timeValidation() && self.appointmentLength()) {

            var waitingListItem;
            waitingListItem = {
                Id: self.WaitingListItemId(),
                ClientId: self.clientId(),
                PatientId: self.patientId(),
                FromTime: self.FromTime(),
                ToTime: self.ToTime(),
                AppointmentDurationInMinutes: self.WaitingListItemId() == 0 ? $("#appointmentLength").val() : self.AppointmentDurationInMinutes(),
                DoctorId: self.WaitingListItemDoctorId(),
                UrgencyTypeId: self.WaitingListItemUrgencyTypeId(),
                Comments: self.WaitingListItemComments(),
                IsSunday: self.IsSunday(),
                IsMonday: self.IsMonday(),
                IsTuesday: self.IsTuesday(),
                IsWednesday: self.IsWednesday(),
                IsThursday: self.IsThursday(),
                IsFriday: self.IsFriday(),
                IsSaturday: self.IsSaturday()
            };
            //if (item.Id() == 0) {
            //    waitingListItem = {
            //        Id: item.Id(),
            //        ClientId: item.ClientId,
            //        PatientId: item.PatientId,
            //        FromTime: item.FromTime,
            //        ToTime: item.ToTime,
            //        AppointmentDurationInMinutes: $("#appointmentLength").val(),
            //        DoctorId: item.DoctorId,
            //        UrgencyTypeId: item.UrgencyTypeId,
            //        Comments: item.Comments,
            //        IsSunday: item.IsSunday,
            //        IsMonday: item.IsMonday,
            //        IsTuesday: item.IsTuesday,
            //        IsWednesday: item.IsWednesday,
            //        IsThursday: item.IsThursday,
            //        IsFriday: item.IsFriday,
            //        IsSaturday: item.IsSaturday,
            //    };
            //} else {
            //    waitingListItem = {
            //        Id: item.Id(),
            //        ClientId: item.ClientId(),
            //        PatientId: item.PatientId(),
            //        FromTime: item.FromTime(),
            //        ToTime: item.ToTime(),
            //        AppointmentDurationInMinutes: item.AppointmentDurationInMinutes(),
            //        DoctorId: item.DoctorId(),
            //        UrgencyTypeId: item.UrgencyTypeId(),
            //        Comments: item.Comments(),
            //        IsSunday: item.IsSunday(),
            //        IsMonday: item.IsMonday(),
            //        IsTuesday: item.IsTuesday(),
            //        IsWednesday: item.IsWednesday(),
            //        IsThursday: item.IsThursday(),
            //        IsFriday: item.IsFriday(),
            //        IsSaturday: item.IsSaturday(),
            //    };
            //}

            $.post('/Calender/SaveWaitingListItem/', {
                waitingListItemString: JSON.stringify(waitingListItem)
            },
                function (data) {
                    if (data) {
                        $('#waitingListItemModal').modal('hide');
                        getAllWaitingListItems();
                        $('#waitingListModal').modal('show');
                    }
                    getCookieMessages();
                }, 'json');
        }

    };

    // open delete waitinglistItem box
    self.openDeleteWaitingListItemBox = function (id) {
        self.deleteWaitingListItemId(id);
        $('#waitingListModal').modal('hide');
        $('#deleteWaitingListItemModal').modal('show');
    };

    // delete waitinglistItem
    self.deleteWaitingListItem = function (waitingListItemId) {
        $.ajax({
            url: '/Calender/RemoveWaitingListItem/',
            type: 'POST',
            data:
            {
                waitingListItemId: waitingListItemId
            },
            success: function () {
                getAllWaitingListItems();
            }
        });
    };

    // delete waitinglistItem and open new calender entry with it's properties
    self.replaceToNewCalenderEntry = function (item) {
        self.clientId(item.ClientId());

        $.getJSON('/Clients/GetClientIdCard?clientId=' + self.clientId(), function (data) {
            self.ClientNameWithPatients(data.clientName);
            $(".client-search").val(data.clientName);
            $('.simpleColor').setColor('#33cccc');
            self.patientId(item.PatientId());
            self.entryId(0);
            self.date('');
            self.time(item.FromTime());
            self.duration(item.AppointmentDurationInMinutes());
            self.roomId(null);
            self.doctorId(item.DoctorId());
            self.byUserId(self.currentUserId());
            self.comments(item.Comments());
            self.color('#33cccc');
            self.title(null);
            $('#waitingListModal').modal('hide');
            $('#myModal').modal('show');

            $.ajax({
                url: '/Calender/RemoveWaitingListItem/',
                type: 'POST',
                data: { waitingListItemId: item.Id() }
            });
        });

    };

    self.AddNewCalenderEntry = function (item) {
        $('.simpleColor').setColor('#33cccc');
        self.entryId(0);
        self.date(self.date());
        self.time(item.FromTime());
        self.duration(item.AppointmentDurationInMinutes());
        self.roomId(null);
        self.doctorId(item.DoctorId());
        self.byUserId(self.currentUserId());
        self.clientId(item.ClientId());
        self.comments(item.Comments());
        self.color('#33cccc');
        self.title(null);
        $('#waitingListModal').modal('hide');
        $('#myModal').modal('show');
        pendingWaitingItemId = item.Id();
        self.ClientNameWithPatients(item.ClientName());
        $(".client-search").val(item.ClientName());
        currentPatientVal = item.PatientId();
    };

    // delete calendary entry and make new waiting list item
    self.replaceToNewWaitingListItem = function (calenderEntryId) {
        $.ajax({
            url: '/Calender/CreateNewWaitingListItemFromCalendarEntry/',
            type: 'POST',
            data:
            {
                calenderEntryId: calenderEntryId
            },
            success: function (data) {
                $('.calender').fullCalendar('refetchEvents');
                $('#myModal').modal('hide');

                self.WaitingListItemId(0);
                self.clientId(data.ClientId);
                self.FromTime(data.FromTime);
                self.ToTime(data.ToTime);
                self.AppointmentDurationInMinutes(data.AppointmentDurationInMinutes);
                self.WaitingListItemDoctorId(data.DoctorId);
                self.WaitingListItemUrgencyTypeId("0");
                self.WaitingListItemComments(data.Comments);
                self.IsSunday(data.IsSunday);
                self.IsMonday(data.IsMonday);
                self.IsTuesday(data.IsTuesday);
                self.IsWednesday(data.IsWednesday);
                self.IsThursday(data.IsThursday);
                self.IsFriday(data.IsFriday);
                self.IsSaturday(data.IsSaturday);
                //self.waitingListItem({
                //    Id: function () { return 0; },
                //    ClientId: data.ClientId,
                //    FromTime: data.FromTime,
                //    ToTime: data.ToTime,
                //    AppointmentDurationInMinutes: data.AppointmentDurationInMinutes,
                //    DoctorId: data.DoctorId,
                //    UrgencyTypeId: "0",
                //    Comments: data.Comments,
                //    IsSunday: data.IsSunday,
                //    IsMonday: data.IsMonday,
                //    IsTuesday: data.IsTuesday,
                //    IsWednesday: data.IsWednesday,
                //    IsThursday: data.IsThursday,
                //    IsFriday: data.IsFriday,
                //    IsSaturday: data.IsSaturday
                //});

                $('#waitingListItemModal').modal('show');
                getCookieMessages();
            }
        });
    };

    self.getJewishDate = function (date) {
        self.JewishDate(null);
        $.ajax({
            url: '/Calender/GetJewishDate/',
            type: 'POST',
            data: { date: date },
            success: function (data) {
                self.JewishDate(data);
            }
        });
    };

    self.setAsShown = function (eventId) {
        $.ajax({
            url: '/Calender/SetAsShown/',
            type: 'POST',
            data:
            {
                calenderEntryId: eventId,
            },
            success: function (data) {
                if (data) {
                    $('.calender').fullCalendar('refetchEvents');
                }
                getCookieMessages();
            }
        });
    };

    self.setAsNoShow = function (eventId) {
        $.ajax({
            url: '/Calender/SetAsNoShow/',
            type: 'POST',
            data:
            {
                calenderEntryId: eventId,
            },
            success: function (data) {
                if (data) {
                    $('.calender').fullCalendar('refetchEvents');
                }
                getCookieMessages();
            }
        });
    };

    self.setNewClient = function (newClientId, newClientName) {
        self.isNewClient(true);
        self.clientId(newClientId);
        self.ClientNameWithPatients(newClientName);
        $(".client-search").val(newClientName);

        if (self.isNewClient()) {
            $('.simpleColor').setColor('#FF9900');
            self.color('#FF9900');
        }

    };

    self.Errors = ko.validation.group(self);

    // on start get meta data
    self.getMetaData();

    self.clientValidation = function () {
        // if ($("select[name='dd_client'] option:selected").index() > 0) {
        if (self.clientId() > 0) {
            self.errorMsg('');
            return true;
        }
        else {
            self.errorMsg('אנא בחר לקוח');
            return false;
        }
    };

    self.daysValidation = function () {
        var sunday = $('#sunday').is(':checked');
        var monday = $('#monday').is(':checked');
        var tuesday = $('#tuesday').is(':checked');
        var wednesday = $('#wednesday').is(':checked');
        var thursday = $('#thursday').is(':checked');
        var friday = $('#friday').is(':checked');
        var saturday = $('#saturday').is(':checked');

        if (sunday || monday || tuesday || wednesday || thursday || friday || saturday) {
            self.errorMsg('');
            return true;
        }
        else {
            self.errorMsg('יש לבחור לפחות יום אחד');
            return false;
        }
    };



    self.timeValidation = function () {
        var from = $("#dd_from").val();
        var to = $("#dd_to").val();

        if (from == "" && to == "") {
            self.errorMsg('אנא בחר שעת התחלה רצויה ושעת סיום רצויה');
            return false;
        }
        else if (from != "" && to == "") {
            self.errorMsg('אנא בחר שעת סיום רצויה');
            return false;
        }
        else if (from == "" && to != "") {
            self.errorMsg('אנא בחר שעת התחלה רצויה');
            return false;
        }
        else {
            //      var timeOfCall = $('#timeOfCall').val(), from
            // timeOfResponse = $('#timeOfResponse').val(), to
            hours = to.split(':')[0] - from.split(':')[0],
            minutes = to.split(':')[1] - from.split(':')[1];

            minutes = minutes.toString().length < 2 ? '0' + minutes : minutes;
            if (minutes < 0) {
                hours--;
                minutes = 60 + minutes;
            }
            hours = hours.toString().length < 2 ? '0' + hours : hours;
            var diff = hours + ':' + minutes;

            if (diff[0] == "-") {
                self.errorMsg('שעת ההתחלה הרצויה מאוחרת משעת הסיום הרצויה');
                return false;
            }
            else if (diff == "00:00") {
                self.errorMsg('שעת ההתחלה הרצויה שווה לשעת הסיום הרצויה');
                return false;
            }
            self.errorMsg('');
            return true;
        }
    }

    self.appointmentLength = function () {
        if (parseInt($("#appointmentLength").val(), 10)) {
            if (parseInt($("#appointmentLength").val(), 10) < 5) {
                self.errorMsg('אורך מינימלי 5 דקות');
                return false;
            }
            self.errorMsg('');
            return true;
        }
        else {
            self.errorMsg('אורך בדקות לא תקין');
            return false;
        }
    };


}

