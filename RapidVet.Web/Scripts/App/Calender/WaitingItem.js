﻿function isEmptyOrWhiteSpaces2(str) {
    return typeof str == "undefined" || str === null || str.match(/^ *$/) !== null;
}

function clearClientSearch2(element) {
    //vm.clientId(undefined);
    //vm.ClientNameWithPatients("");
    //element.value = "";
    //$(".no-results").hide();
};

$(function () {

    $(function () {

        $(".client-search").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/Clients/Search",
                    type: "POST",
                    data: {
                        query: request.term
                    },
                    success: function (data) {
                        response(data);

                        if (data.length == 0) {
                            $(".no-results").show();
                            setTimeout(function () { $(".no-results").hide(); }, 5000);
                        }
                        else {
                            $(".no-results").hide();
                        }
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                event.preventDefault();
                self.ClientNameWithPatients(ui.item.label);
                self.clientId(ui.item.value.toString());
                this.value = ui.item.label;

                //window.location = "/Clients/Patients/" + ui.item.value;
            },
            focus: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.label;
            }
            ,
            change: function (event, ui) {
                event.preventDefault();
                if (isEmptyOrWhiteSpaces(this.value) ||
                    typeof vm.clientId() == "undefined" || vm.ClientNameWithPatients() != this.value) {
                    clearClientSearch(this);
                }

            },
            open: function () {
                $(".ui-autocomplete").css("max-height", 120);
                $(".ui-autocomplete").css("max-width", 225);
                $(".ui-autocomplete").css("overflow-y", "auto");
                $(".ui-autocomplete").css("overflow-x", "hidden");

            }
        });
    });
});