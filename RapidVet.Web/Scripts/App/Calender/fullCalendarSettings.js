﻿
//-------------------full calendar---------------------------------------------------
function oneScrollBarForAll() {
}

var lastDayClick = 0;

function addBlockedDay() {
    $.ajax({
        url: '/Calender/AddBlockedDay/',
        data: { DoctorId: $('#SelectedDoctorId').val(), Comment: $('#blockedDayComment').val(), DayDate: $('#BlockedDayDate').val() }
    }).done(function () {
        $('.calender').fullCalendar('refetchEvents');
    });
}

function removeBlockedDay(id, isHideButton) {
    $.ajax({
        url: '/Calender/RemoveBlockedDay/',
        data: { blockedDayId: id }
    }).done(function () {
        $('.calender').fullCalendar('refetchEvents');
        if (isHideButton) {
            $("#IsUnblock").hide();
            $("#IsUnblock").attr('onclick', 'removeBlockedDay(0);');
        }
        getCookieMessages();
    });
}

$(function () {
    ko.validation.init({
        decopercentElement: true,
        decopercentInputElement: true,
        errorElementClass: 'input-validation-error',

    });

    ko.validation.rules['daysValidation'] = {
        validator: function (val, otherValue) {
            /*important to use otherValue(), because ko.computed returns a function,
            and the value is inside that function*/
            return false;
            //return otherValue().item.IsSunday || otherValue().item.IsMonday || otherValue().item.IsTuesday || otherValue().item.IsWednesday ||
            //    otherValue().item.IsThursday || otherValue().item.IsFriday || otherValue().item.IsSaturday;
        },
        message: "יש לבחור לפחות יום אחד"
    };
    ko.validation.registerExtenders();

    ko.applyBindings(viewModel());

    $('.simpleColor').simpleColor({
        defaultColor: '#0066cc',
        displayColorCode: true,
        onSelect: function (hex) {
            $('#color').val('#' + hex);
            changeSelfColor(hex);
        }
    });

    $('#BlockedDayDate').datepicker({ defaultDate: '+0', isRTL: true, width: '200px' });

    //Holiday Events
    var holidaySource = {
        url: '/Calender/GetHolidays/',  //'@Url.Action("GetHolidays", "Calender")',
        type: 'POST',
        error: function () {
            console.log('there was an error while fetching events!');
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                if (String(data[i].start).indexOf('/Date(') == 0) {
                    data[i].start = new Date(parseInt(data[i].start.replace(/\/Date\((.*?)\)\//gi, "$1")));
                }
                if (String(data[i].end).indexOf('/Date(') == 0) {
                    data[i].end = new Date(parseInt(data[i].end.replace(/\/Date\((.*?)\)\//gi, "$1")));
                }
            }
        }
    };

    // Doc's Schedule Events
    var scheduleSource = {
        url: '/Calender/GetDocsWeeklySchedule/',//'@Url.Action("GetDocsWeeklySchedule", "Calender")',
        type: 'POST',
        data: { docId: $('#SelectedDoctorId').val(), date: new Date() },
        error: function () {
            console.log('there was an error while fetching events!');
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                if (String(data[i].start).indexOf('/Date(') == 0) {
                    data[i].start = new Date(parseInt(data[i].start.replace(/\/Date\((.*?)\)\//gi, "$1")));
                }
                if (String(data[i].end).indexOf('/Date(') == 0) {
                    data[i].end = new Date(parseInt(data[i].end.replace(/\/Date\((.*?)\)\//gi, "$1")));
                }
            }
        }
    };

    // docs calender entries events
    var calednerEntries = {
        url: '/Calender/GetDocsCalenderEntries/',//'@Url.Action("GetDocsCalenderEntries", "Calender")',
        type: 'POST',
        data: { doctorId: $('#SelectedDoctorId').val() },
        error: function () {
            console.log('there was an error while fetching events!');
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                if (String(data[i].start).indexOf('/Date(') == 0) {
                    data[i].start = new Date(parseInt(data[i].start.replace(/\/Date\((.*?)\)\//gi, "$1")));//;  moment(data[i].start).toISOString()
                }
                if (String(data[i].end).indexOf('/Date(') == 0) {
                    data[i].end = new Date(parseInt(data[i].end.replace(/\/Date\((.*?)\)\//gi, "$1")));// ;
                }
            }
        }
    };

    // docs blocked days
    var blockedDays = {
        url: '/Calender/GetDocsBlockedDays/',//'@Url.Action("GetDocsBlockedDays", "Calender")',
        type: 'POST',
        data: { doctorId: $('#SelectedDoctorId').val() },
        error: function () {
            console.log('there was an error while fetching events!');
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                if (String(data[i].start).indexOf('/Date(') == 0) {
                    data[i].start = new Date(parseInt(data[i].start.replace(/\/Date\((.*?)\)\//gi, "$1")));
                }
                if (String(data[i].end).indexOf('/Date(') == 0) {
                    data[i].end = new Date(parseInt(data[i].end.replace(/\/Date\((.*?)\)\//gi, "$1")));
                }
            }
        }
    };

    // Add unblock item
    var unblockIdBefore = getBlockedDayId($('#SelectedDoctorId').val(), new Date());
    if (unblockIdBefore > 0) {
        $("#IsUnblock").show();
        $("#IsUnblock").attr('onclick', 'removeBlockedDay(' + unblockIdBefore + ', true);');
    } else {
        $("#IsUnblock").hide();
        $("#IsUnblock").attr('onclick', 'removeBlockedDay(0, true);');
    }

    //Datepicker initialize
    $('#datepicker').datepicker({
        onSelect: function (dateText, inst) {

            var dateInFormat = new Date(inst.currentYear, inst.currentMonth, inst.currentDay);

            //Remove Current's Week Regular Sources
            $('#calendar').fullCalendar('removeEventSource', scheduleSource);

            //New week's regular Sources
            var newScheduleSource = {
                url: '/Calender/GetDocsWeeklySchedule/',//'@Url.Action("GetDocsWeeklySchedule", "Calender")',
                type: 'POST',
                data: { docId: $('#SelectedDoctorId').val(), date: new Date(inst.currentYear, inst.currentMonth, inst.currentDay) },
                error: function () {
                    console.log('there was an error while fetching events!');
                },
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        if (String(data[i].start).indexOf('/Date(') == 0) {
                            data[i].start = new Date(parseInt(data[i].start.replace(/\/Date\((.*?)\)\//gi, "$1")));
                        }
                        if (String(data[i].end).indexOf('/Date(') == 0) {
                            data[i].end = new Date(parseInt(data[i].end.replace(/\/Date\((.*?)\)\//gi, "$1")));
                        }
                    }
                }
            };

            $('.calender').fullCalendar('gotoDate', new Date(inst.currentYear, inst.currentMonth, inst.currentDay));

            // Add unblock item
            var unblockId = getBlockedDayId($('#SelectedDoctorId').val(), dateInFormat);
            if (unblockId > 0) {
                $("#IsUnblock").show();
                $("#IsUnblock").attr('onclick', 'removeBlockedDay(' + unblockId + ', true);');
            } else {
                $("#IsUnblock").hide();
                $("#IsUnblock").attr('onclick', 'removeBlockedDay(0, true);');
            }

            $('#calendar').fullCalendar('addEventSource', newScheduleSource);
            scheduleSource = newScheduleSource;
            var d = $("#datepicker").datepicker('getDate');
            setJewishDate(d);

        }
    });

    //FullCaledner initialize
    var fullCalenderOption = {
        dayClick: function (date, allDay, jsEvent, view) {
            var d = new Date();
            if (d.getTime() - lastDayClick < 1000) {
                var isBlocked = dateIsBlocked($('#SelectedDoctorId').val(), date, date);
                if (isBlocked) {
                    return;
                }
                createCalenderEntry(date, $('#SelectedDoctorId').val());

                $('#myModal').modal('show');
                lastDayClick = 0;
            } else {
                lastDayClick = d.getTime();
            }

        },
        eventClick: function (calEvent, jsEvent, view) {

            if (calEvent.description == "calenderentry") {
                getCalenderEntry(calEvent.id);
                $('#myModal').modal('show');//.dialog({ modal: true });//.modal('show');
            }
        },
        eventRender: function (event, element, view) {
            if (event.rendering == 'background') {
                element.append("<div class='bgText'>" + event.title + "</div>");
            }
            var items = [
                {
                    label: 'העבר לרשימת ממתינים',
                    action: function () {
                        if (event.id != 0) {
                            replaceToNewWaitingListItem(event.id);
                        }
                    }
                }
            ];
            if (event.isNoShow == null) {
                items.push({ label: 'סמן שהופיע', action: function () { setAsShown(event.id); } });
                items.push({ label: 'סמן לא הופיע', action: function () { setAsNoShow(event.id); } });
            } else {
                if (event.isNoShow) {
                    items.push({ label: 'סמן שהופיע', action: function () { setAsShown(event.id); } });
                } else {
                    items.push({ label: 'סמן לא הופיע', action: function () { setAsNoShow(event.id); } });
                }
            }

            if (event.clientId != null) {
                items.push({
                    label: 'עבור לכרטיס לקוח',
                    action: function () {
                        if (event.id != 0) {
                            window.location.href = '/Clients/Patients/' + event.clientId;
                        }
                    }
                },
                {
                    label: 'רשימת תורים עתידיים',
                    action: function () { locateEventClientIDEntries(event.clientId, event.title); }
                },
                    {
                        label: 'חשבונית',
                        action: function () {
                            if (event.id != 0) {
                                window.location.href = '/FinanceDocument/InvoiceReceipt?clientId=' + event.clientId;
                            }
                        }
                    });
            }

            if (event.id) {
                items.push({ label: 'מחיקה', action: function () { deleteCalenderEntry(event.id); } });
            }

            if (event.description != 'holiday' && event.description != 'recess') {
                $(element).contextPopup({
                    title: event.title,
                    items: items
                });
            }

            element.qtip({
                content: event.comment,
                show: {
                    delay: 100,
                    solo: true
                },
                hide: {
                    delay: 50,
                    when: 'mouseout'
                },
                //position: {
                //    my: 'top center',  // Position my top left...
                //    at: 'bottom center' // at the bottom right of...

                //},
                position: {
                    target: 'mouse',
                    adjust: {
                        x: 10,
                        y: 4,
                        mouse: true,
                        screen: true,
                        scroll: false,
                        resize: false
                    },
                    corner: {
                        target: 'bottomLeft',
                        tooltip: 'bottomLeft'
                    }
                },
                style: {
                    tip: 'topLeft',
                    padding: 10,
                    background: event.bgColor,
                    color: event.fgColor,
                    border: {
                        width: 2,
                        radius: 7,
                        color: event.bdrColor
                    },
                    width: element.width,
                    classes: 'qtip-rounded qtip-shadow'
                },
                api: {
                    onRender: function () {
                        var self = this;
                        qtipTimeout = setTimeout(function () {
                            self.show();
                        }, 450);
                    },
                    beforeShow: function () {
                        return (!suspendTooltips);
                    }
                }
            });

            element.bind('dblclick', function () {
                console.log("dblclick");
                if (event.description == "calenderentry") {
                    getCalenderEntry(event.id);
                    $('#myModal').modal('show');
                    $('#myModal').modal('show');//.dialog({ modal: true });//.modal('show');
                }

            });
        },
        eventAfterRender: function (event, $el, view) {
            $el.removeClass('fc-short');
        },
        eventResize: function (event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
            var isBlocked = dateIsBlocked($('#SelectedDoctorId').val(), event.start, event.end, event.id);
            if (isBlocked) {
                $('.calender').fullCalendar('refetchEvents');
                return;
            }
            updateChangedCalenderEntry(event);
        },
        eventDrop: function (event, delta, revertFunc) {//(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
            var isBlocked = dateIsBlocked($('#SelectedDoctorId').val(), event.start, event.end, event.id);
            if (isBlocked) {
                $('.calender').fullCalendar('refetchEvents');
                return;
            }
            updateChangedCalenderEntry(event);
        },
        defaultView: 'agendaWeek',
        scrollTime: "08:00:00",
        header: false,
        slotLabelFormat: 'H:mm',
        timeFormat: 'H:mm', //timeFormat: { agenda: 'H:mm{ - H:mm}' },
        isRTL: true,
        monthNames: ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי',
            'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'],
        monthNamesShort: ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי',
            'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'],
        dayNames: ['ראשון', 'שני', 'שלישי', 'רביעי', 'חמישי', 'שישי', 'שבת'],
        dayNamesShort: ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ש'],
        columnFormat: {
            week: 'ddd d/M'
        },
        contentHeight: 600,
        allDaySlot: false,
        defaultTimedEventDuration: '02:00:00',
        eventSources: [blockedDays, scheduleSource, holidaySource, calednerEntries],
        displayEventTime: true,
        displayEventEnd: true,
        selectOverlap: true
    };

    $('#calendar').fullCalendar(fullCalenderOption);
    $('#SelectedDoctorId').on('change', function () {
        if ($('#SelectedDoctorId') && $('#SelectedDoctorId').val().length > 0)
            window.location.href = '/calender/weekly/' + $('#SelectedDoctorId').val();
        else
            window.location.href = '/calender/weekly/0';
    });


});

function getWindowWidth() {
    return $(window).width();
}
function getView() {
    return (ww <= 768) ? 'month' : 'month';
}

function setJewishDate(date) {
    var weekdays = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
    $.ajax({
        url: '/Calender/GetWeekJewishDate/',
        type: 'POST',
        data: { date: date },
        success: function (data) {
            for (var i = 0; i < 7; i++) {
                var conNum = weekdays[i];//6 - i;
                $('.fc-day-header.fc-' + conNum).html(data[i]);
                //$('.fc-widget-header.fc-col' + conNum).html(data[i]);
            }

        }
    });
}

setJewishDate(new Date());

ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datepickerOptions || {};
        $(element).datepicker(options);

        //handle the field changing
        ko.utils.registerEventHandler(element, "change", function () {
            var observable = valueAccessor();
            observable($(element).val()); //.datepicker("getDate"));
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).datepicker("destroy");
        });
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());

        //handle date data coming via json from Microsoft
        if (String(value).indexOf('/Date(') == 0) {
            value = new Date(parseInt(value.replace(/\/Date\((.*?)\)\//gi, "$1")));
        }

        var current = $(element).val(); //.datepicker("getDate");

        if (value - current !== 0) {
            $(element).val(value); //.datepicker("setDate", value);
        }
    }


};
$(document).ready(function () {
    $(window).resize(function () {
        $('#calendar').fullCalendar('option', 'height', get_calendar_height());
    });
});
function get_calendar_height() {
    return $(window).height() - 30;
}