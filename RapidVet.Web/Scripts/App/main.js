var errorBackground = '#FFA5A5';
var RapidVet = new Object();

var qs = (function (a) {
    if (a == "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i) {
        var p = a[i].split('=');
        if (p.length != 2) continue;
        b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
})(window.location.search.substr(1).split('&'));

$(function () {
    $('#main-search').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            //do something   
        }
    });

    //$("#main-search").typeahead({
    //    source: function (typeahead, query) {
    //        return $.post(
    //            '/Clients/Search',
    //            { query: query },
    //            function (data) {
    //                return typeahead.process(data);
    //            });
    //    },
    //    highlighter: function (item) {
    //        fixSearchTypeaheadPos();
    //        return item;
    //    },
    //    property: "name",
    //    onselect: function (obj) { console.log(obj) }
    //});

    $(function () {

        $("#main-search").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/Clients/Search",
                    type: "POST",
                    data: {
                        query: request.term
                    },
                    success: function (data) {
                        if (data.length > 0) {
                            response(data);
                        }
                        else
                        {
                            response([{ label: $("#no_results").val(), val: -1 }]);
                        }
                        //response($.map(data, function (item) {
                        //    return {
                        //        label: item.name,
                        //        value: item.id
                        //    };
                        //}));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.val == -1) {
                    return false;
                } else
                {
                    window.location = "/Clients/Patients/" + ui.item.value;
                }
            },
            focus: function (event, ui) {
                event.preventDefault();
                if (ui.item.val != -1) {
                    $("#main-search").val(ui.item.label);
                }
            }
            ,
           
            open: function () {
                $(".ui-autocomplete").css("max-height", 150);
                $(".ui-autocomplete").css("max-width", 227);               
                $(".ui-autocomplete").css("overflow-y", "auto");
                $(".ui-autocomplete").css("overflow-x", "hidden");

            }
        });
    });



    RapidVet.TimeDisplay.init();
    RapidVet.FixInputWidth.init();



});

//http://fusiongrokker.com/post/heavily-customizing-a-bootstrap-typeahead
var fixSearchTypeaheadPos = function () {
    setTimeout(function () {
        $('.navbar.sub-nav .navbar-search .dropdown-menu').css('left', '0').css('top', '-4px');
        console.log('Testing console');
    }, 4);

}

function switchPatients(e) {
    if (typeof e != "undefined" && typeof e.value != "undefined" && e.value != '-1') {
        window.location.href = e.value;
    }

}

function getTodayFormat() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return dd + '/' + mm + '/' + yyyy;
}

function getQueryStrings() {
    var assoc = {};
    var decode = function (s) { return decodeURIComponent(s.replace(/\+/g, " ")); };
    var queryString = location.search.substring(1);
    var keyValues = queryString.split('&');

    for (var i in keyValues) {
        var key = keyValues[i].split('=');
        if (key.length > 1) {
            assoc[decode(key[0])] = decode(key[1]);
        }
    }

    return assoc;
}

//in order to enable new row datepicker functionality
$('body').on('focus', ".datepicker_recurring_start", function () {
    $(this).datepicker();
});

//takes array of ids, in order to hide specefic id concat it with ^1
function startUIEdit(idsArray) {
    var elem;
    for (var i = 0; i < idsArray.length; i++) {
        elem = document.getElementById(idsArray[i].id);
        elem.className = idsArray[i].hide ? "hide" : "";
    }
}

function stringContainsPhraseStartsWith(str, strToCheck) {
    if (str == null) {
        return false;
    }

    strToCheck = strToCheck == null ? "" : strToCheck.trim();
    if (strToCheck.trim() == "") {
        return true;
    }
    
    var splitted = str.split(' ');
    for (var i = 0; i < splitted.length; i++) {
        if (splitted[i].startsWith(strToCheck)) {
            return true;
        }
    }
    return false;
}

function getCookieComments() {
    var comments = getCookie("comments");
    if (comments != null) {
        jaaulde.utils.cookies.del('comments');
        var commentsSplit = comments.split("|");
        for (var i = 0; i < commentsSplit.length; i++) {
            var str = commentsSplit[i];
            if (str.indexOf('^') > -1) {
                str = str.replace('^', '');
                str = "<label style='color:red;font-size: 18px;font-weight: bold;'>" + str + "</label>";
            }

            $("#commentsContent").append("<li>" + str + "</li>");
        }
        $("#commentModal").modal();
    }
}