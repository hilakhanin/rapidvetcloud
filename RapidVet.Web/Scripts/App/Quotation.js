﻿var viewModel;



function Treatment(data) {
    var self = this;
    self.id = ko.observable(data.Id);
    self.quotationId = ko.observable(data.QuotationId);
    self.name = ko.observable(data.Name);

    self.price = ko.observable(data.Price);

    self.amount = ko.observable(data.Amount).extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 0, message: "ערך מינימלי: 1" } })
        .extend({ max: { params: 100, message: "ערך מקסימלי: 999" } });;
    self.type = ko.observable(data.ItemType);
    self.priceListItemId = ko.observable(data.PriceListItemId);
    self.comments = ko.observable(data.Comments);
    self.percentDiscount = ko.observable(data.PercentDiscount).extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 0, message: "ערך מינימלי: 0" } })
        .extend({ max: { params: 100, message: "ערך מקסימלי: 100" } });;
    self.discount = ko.observable(data.Discount).extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 0, message: "ערך מינימלי: 0" } });
    self.totalAmount = ko.computed(function () {
        var price = self.price() * self.amount();

        if (self.percentDiscount() > 0) {
            price = self.price() * self.amount() * ((100 - self.percentDiscount()) / 100);
            self.discount((self.price() * self.amount() - price).toFixed(2));
        }

        return price.toFixed(2);
    });

    self.totalAmountNoDiscounts = ko.computed(function () {
        return (self.price() * self.amount()).toFixed(2);
    });

    self.treatmentPackageItemId = ko.observable(data.TreatmentPackageItemId);
    self.treatmentPackageId = ko.observable(data.TreatmentPackageId);
    self.catalog = ko.observable(data.Catalog);
    self.enableEdit = ko.observable(false);
    self.isItemPercentDiscount = ko.observable("true");
    self.wasTreatmentMovedToExecution = ko.observable(data.WasTreatmentMovedToExecution);

    self.cssClass = ko.computed(function () {
        if (data.WasTreatmentMovedToExecution == true) {
            return 'executed';
        } else if (data.IsNextFuture == false) {
            return 'not_executed';
        }
        return 'not_executed';
    });
    self.alreadyExecuted = ko.computed(function () {
        if (data.WasTreatmentMovedToExecution == true) {
            return 'בוצע';
        } else if (data.IsNextFuture == false) {
            return '';
        }
        return '';
    });

    self.visitId = ko.observable(data.VisitId);
}


function PriceListItem(data) {
    var self = this;
    self.name = ko.observable(data.Name);
    self.price = ko.observable(data.Price);
    self.id = ko.observable(data.Id);
    self.category = ko.observable(data.CategoryId);
    self.catalog = ko.observable(data.Catalog);
    self.itemType = ko.observable(data.ItemType);


}

function viewModel() {

    var self = this;

    self.isPercentDiscount = ko.observable("true");

    self.totalPercentDiscount = ko.observable().extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 0, message: "ערך מינימלי: 0" } })
        .extend({ max: { params: 100, message: "ערך מקסימלי: 100" } });
    self.totalSumDiscount = ko.observable().extend({ number: { params: true, message: "יש להזין ערך תקין" } })
        .extend({ min: { params: 0, message: "ערך מינימלי: 0" } });


    self.clientId = ko.observable($('#clientId').val());
    self.quotationId = ko.observable($('#quotationId').val());
    self.name = ko.observable().extend({ required: { params: true, message: "שדה חובה" } });
    self.selectedPatient = ko.observable().extend({ required: { params: true, message: "שדה חובה" } });;
    self.patients = ko.mapping.fromJS([]);
    self.comments = ko.observable();
    self.treatments = ko.observableArray([]);
    self.items = ko.observableArray([]);
    self.packages = ko.mapping.fromJS([]);
    self.selectedPackage = ko.observable();
    self.categories = ko.mapping.fromJS([]);
    self.packageSavingDestination = ko.observable();
    self.newPackageName = ko.observable();

    self.totalPrice = ko.computed(function () {
        var total = 0;

        for (var i = 0; i < self.treatments().length; i++) {
            total += parseFloat(self.treatments()[i].totalAmount());
        }

        return total;
    });

    self.isTreamtmentChosenForExecution = ko.computed(function () {
        for (var i = 0; i < self.treatments().length; i++) {
            if (self.treatments()[i].wasTreatmentMovedToExecution() == true) {
                return true;
            }
        }

        return false;
    });

    self.executeText = ko.computed(function () {
        var icon = "<i class='icon-ok-circle'></i>";
        for (var i = 0; i < self.treatments().length; i++) {
            if (self.treatments()[i].wasTreatmentMovedToExecution() == true) {
                return icon + "העברת טיפולים מסומנים לביצוע";
            }
        }

        return icon + "העבר הצעת מחיר לביצוע";
    });

    self.executeAllTreatments = ko.computed(function () {
        for (var i = 0; i < self.treatments().length; i++) {
            if (self.treatments()[i].wasTreatmentMovedToExecution() == true) {
                return false;
            }
        }

        return true;
    });

    self.markAllTreatmentsForExecution = function () {
        //mark all treatments to be executed
        for (var i = 0; i < self.treatments().length; i++) {
            self.treatments()[i].wasTreatmentMovedToExecution(true);
        }
    }


    self.print = ko.observable(false);
    self.executeTreatments = ko.observable(false);
    self.showPricesInPrint = ko.observable(true);
    self.showCommentsPrint = ko.observable(true);
    self.execute = ko.observable(false);
    self.searchQuery = ko.observable(' ');

    self.selectedCategoryId = ko.observable(0);

    self.priceListOptions = ko.computed(function () {
        var result = self.items();

        if (self.selectedCategoryId() > 0 || self.searchQuery != ' ' && self.searchQuery().length > 0) {

            if (self.selectedCategoryId() > 0) {
                result = ko.utils.arrayFilter(result, function (item) {
                    if (item.category() == self.selectedCategoryId()) {
                        return item;
                    }
                    return false;
                });
            }
            if (self.searchQuery != ' ' && self.searchQuery().length > 0) {
                result = ko.utils.arrayFilter(result, function (item) {
                    if (stringContainsPhraseStartsWith(item.name(), self.searchQuery())) {
                        return item;
                    }
                    return false;
                });
            }
        }

        return result;
    });   

    self.startEditing = function (element) {
        $(".percentCb").show();
        element.enableEdit(true);             
    };

    self.finisheEditing = function (element) {
        $(".percentCb").hide();
        element.enableEdit(false);

        if (element.isItemPercentDiscount() =="true") {
            element.discount(0);
        }
        else {
            if (element.discount() > 0 && element.discount() < element.price() * element.amount()) {
                var discountPercentage = element.discount() / (element.price() * element.amount()) * 100;
                element.percentDiscount(discountPercentage.toFixed(2));
            }
            else {
                element.discount(0);
                element.percentDiscount(0);
            }

        }
    };



    //loading data
    self.getData = function () {

        var url = '/Quotations/GetQuotationData/' + self.clientId() + '?quotationId=' + self.quotationId();
        ShowLoder();
        $.getJSON(url, function (data) {

            ko.mapping.fromJS(data.Patients, self.patients);
            ko.mapping.fromJS(data.Packages, self.packages);
            ko.mapping.fromJS(data.Categories, self.categories);

            var mappedItems = $.map(data.Items, function (pl) {
                return new PriceListItem(pl);
            });
            self.items(mappedItems);

            var mappedTreatments = $.map(data.Treatments, function (trt) {
                return new Treatment(trt);
            });
            self.treatments(mappedTreatments);

            self.name(data.Name);
            self.selectedPatient(data.PatientId);

            self.comments(data.Comments);
            if (CKEDITOR.instances.editor1 == null) {
                CKEDITOR.replace("editor1");
            }
            CKEDITOR.instances.editor1.setData(self.comments());
        }).done(function () {
            hideLoder();
        });
    };

    self.refreshPackeges = function (newSelected) {
        $.getJSON('/Quotations/GetTreatmentPackagesData/', function (data) {
            self.packages([]);
            ko.mapping.fromJS(data, self.packages);
            self.selectedPackage(newSelected);
        });
    };


    self.refreshTreatments = function () {
        self.treatments([]);
        $.getJSON('/Quotations/GetQuotationTreatments/' + self.quotationId(), function (data) {
            var mapped = $.map(data.Treatments, function (item) {
                return new Treatment(item);
            });
            self.treatments(mapped);
        });
    };


    // collection operations

    self.clearSearch = function () {
        self.searchQuery(' ');
        self.selectedCategoryId(0);
    };

    self.addTreatment = function () {
        self.clearSearch();
        $('#priceListModal').modal('show');
    };

    self.addToTreatments = function (element, data) {

        var temp = {
            'Name': element.name(),
            'Price': element.price(),
            'Amount': 1,
            'ItemType': element.itemType(),
            'PriceListItemId': element.id(),
            'Comment': ' ',
            'PercentDiscount': 0,
            'Discount': 0,
            'Catalog': element.catalog()
        };
        
        var treatment = new Treatment(temp);
        self.treatments.push(treatment);

        setCookieMessage("successmsg", encodeURIComponent("הטיפול נוסף בהצלחה"), 1, 0, 0);
        getCookieMessages();
      //  $('#priceListModal').modal('hide');
    };

    self.removeMarkedTreatments = function () {
        for (var i = 0; i < self.treatments().length; i++) {
            if (self.treatments()[i].wasTreatmentMovedToExecution() == true) {
                self.treatments.remove(self.treatments()[i]);
                i = i - 1;
            }
        }
    };

    self.removeTreatment = function (elements, data) {
        self.treatments.remove(elements);
    };

    self.loadPackage = function () {
        if (typeof self.selectedPackage() != "undefined") {
            $.getJSON('/Quotations/GetTreatmentPackageData/' + self.selectedPackage(), function (data) {
                $.map(data, function (item) {
                    var t = new Treatment(item);
                    self.treatments.push(t);
                });
            });
        }
    };

    self.showPackageModal = function () {
        $('#savePackageModal').modal('show');
    };

    self.showGeneralDiscountModal = function () {
        $('#generalDiscountModal').modal('show');
    };

    self.clearAllDiscounts = function () {
        for (var i = 0; i < self.treatments().length; i++) {
            if (self.treatments()[i].cssClass() !== "executed") {
                self.treatments()[i].discount(0);
                self.treatments()[i].percentDiscount(0);
            }
        };
    }
    //saving data
    self.savePackage = function () {
        $('#savePackageModal').modal('hide');
        ShowLoder();

        if (typeof self.packageSavingDestination() == "undefined") {
            self.packageSavingDestination(0);
        }

        $.ajax({
            type: "POST",
            url: '/Quotations/SaveTreatmentPackage',
            data: {
                'PackageId': self.packageSavingDestination(),
                'NewPackageName': self.newPackageName(),
                'Treatments': ko.toJSON(self.treatments())
            },
            success: function (data) {
                $('#savePackageModal').modal('hide');
                self.newPackageName(' ');

                self.selectedPackage(data.ItemId);

                if (data.Success) {
                    if (typeof self.packageSavingDestination() == "undefined" || self.packageSavingDestination() == 0) {
                        self.refreshPackeges(data.ItemId);
                        self.selectedPackage(data.ItemId);
                        //self.loadPackage();
                        self.packageSavingDestination(data.ItemId);
                    }

                    else if (self.packageSavingDestination() > 0) {
                        self.selectedPackage(self.packageSavingDestination());
                        //self.loadPackage();
                    }

                }
            }
        }).done(function () {
            hideLoder();
            getCookieMessages();
        });
    };

    self.saveQuotation = function () {
        if (self.isValid()) {
            ShowLoder();
            if (CKEDITOR.instances.editor1 == null) {
                CKEDITOR.replace("editor1");
            }
            self.comments(CKEDITOR.instances.editor1.getData());

            $.post(
                '/Quotations/SaveQuotation',
                {
                    'PatientId': self.selectedPatient(),
                    'Id': self.quotationId(),
                    'Name': self.name(),
                    'Treatments': ko.toJSON(self.treatments()),
                    'Comments': self.comments(),
                    //'TotalPrice': self.totalPrice(),
                    //'Discount': self.totalDiscount(),
                    //'PercentDiscount': self.totalPercentDiscount(),
                    'Print': self.print(),
                    'PrintPrices': self.showPricesInPrint(),
                    'PrintComments': self.showCommentsPrint(),
                    'executeTreatments': self.executeTreatments()
                },
                function (data) {
                    if (data.success) {
                        self.quotationId = ko.observable(data.quotationId);
                        if (self.print()) {
                            window.open(data.url, "_blank");
                            window.location.href = '/Quotations/Quotation/' + self.clientId() + '?quotationId=' + self.quotationId();
                        }
                        else if (self.executeTreatments()) {
                            window.location.href = data.url;
                        }

                    } else {
                        hideLoder();
                        getCookieMessages();
                    }
                }).done(function () {
                    hideLoder();
                });
        }
    };

    self.saveAndPrint = function () {
        self.print(true);
        self.executeTreatments(false);
        self.saveQuotation();

    };

    self.save = function () {
        self.print(false);
        self.executeTreatments(false);
        self.saveQuotation();
    };

    self.saveAndExecutePackage = function () {
        if (self.executeAllTreatments()) {
            $('#moveQuotationToExecutionModel').modal('show');
        } else {
            self.print(false);
            self.executeTreatments(true);
            self.saveQuotation();
        }
    };

    //validation
    self.isValid = function () {
        var errors = ko.validation.group(viewModel, { deep: true });
        errors.showAllMessages(true);
        return errors().length == 0;
    };

    self.executePackage = function () {
        if (self.isValid()) {
            ShowLoder();
            if (CKEDITOR.instances.editor1 == null) {
                CKEDITOR.replace("editor1");
            }
            CKEDITOR.instances.editor1.setData(self.comments());
            self.comments(CKEDITOR.instances.editor1.getData());
            $.ajax({
                type: "POST",
                url: '/Quotations/ExecuteTreatments',
                data: {
                    'Id': self.selectedPatient(),
                    'Treatments': ko.toJSON(self.treatments()),
                    'TotalPrice': self.totalPrice(),
                    'Comments': self.comments()
                },
                success: function (data) {
                    if (data.success) {
                        window.location.href = data.url;
                    }
                }
                , failure: function () {
                    hideLoder();
                    getCookieMessages();
                }
            });
        }
    };

    self.applyDiscount = function (element, data) {

        if (element.isPercentDiscount() === "false") {
            if (element.totalSumDiscount() > 0 && element.totalSumDiscount() < self.totalPrice()) {
                self.clearAllDiscounts();
                var discountPercentage = element.totalSumDiscount() / self.totalPrice() * 100;
                self.totalPercentDiscount(discountPercentage.toFixed(3));
            }
        }
        if (element.totalPercentDiscount() >= 0 && element.totalPercentDiscount() <= 100) {
            for (var i = 0; i < self.treatments().length; i++) {
                if (self.treatments()[i].cssClass() !== "executed")
                    self.treatments()[i].percentDiscount(element.totalPercentDiscount());
            };
        }
        else {
            self.totalPercentDiscount(0);
        }

        $('#generalDiscountModal').modal('hide');
    };

    self.moveToExecution = function () {
        self.print(false);
        self.executeTreatments(true);
        if (self.executeAllTreatments()) {
            self.markAllTreatmentsForExecution();
        }
        $('#moveQuotationToExecutionModel').modal('hide');
        self.saveQuotation();
    }

    self.getData();
}

$(function () {

    viewModel = new viewModel();
    ko.applyBindings(viewModel);

});