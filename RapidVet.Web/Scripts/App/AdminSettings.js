﻿$(function () {
    CKEDITOR.on('dialogDefinition', function (ev) {
        // Take the dialog name and its definition from the event data.
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        // Check if the definition is from the dialog we're
        // interested in (the 'link' dialog).
        if (dialogName == 'link') {
            // Remove the 'Advanced' tabs from the 'Link' dialog.        
            dialogDefinition.removeContents('advanced');

           
            // Get a reference to the 'Link Info' tab.
            //var infoTab = dialogDefinition.getContents('info');

            // Remove unnecessary widgets from the 'Link Info' tab.         
            //infoTab.remove('linkType');
            //infoTab.remove('browse');

            /* Getting the contents of the Target tab */

            var informationTab = dialogDefinition.getContents('target');

            /* Getting the contents of the dropdown field "Target" so we can set it */

            var targetField = informationTab.get('linkTargetType');

            /* Now that we have the field, we just set the default to _blank
        
            A good modification would be to check the value of the URL field
        
            and if the field does not start with "mailto:" or a relative path,
        
            then set the value to "_blank" */

            targetField['default'] = '_blank';

           // dialogDefinition.removeContents('target');

        }
    });

    ///* Here we are latching on an event ... in this case, the dialog open event */

    //CKEDITOR.on('dialogDefinition', function (ev) {

    //    try {

    //        /* this just gets the name of the dialog */

    //        var dialogName = ev.data.name;

    //        /* this just gets the contents of the opened dialog */

    //        var dialogDefinition = ev.data.definition;



    //        /* Make sure that the dialog opened is the link plugin ... otherwise do nothing */

    //        if (dialogName == 'link') {

    //            /* Getting the contents of the Target tab */

    //            var informationTab = dialogDefinition.getContents('target');

    //            /* Getting the contents of the dropdown field "Target" so we can set it */

    //            var targetField = informationTab.get('linkTargetType');

    //            /* Now that we have the field, we just set the default to _blank
            
    //            A good modification would be to check the value of the URL field
            
    //            and if the field does not start with "mailto:" or a relative path,
            
    //            then set the value to "_blank" */

    //            targetField['default'] = '_blank';

    //        }

    //    } catch (exception) {

    //        alert('Error ' + ev.message);

    //    }

    //});

    CKEDITOR.on('instanceReady', function (ck) { ck.editor.removeMenuItem('Link'); });

    CKEDITOR.replace('editor1', {
        toolbar: [
		{ name: 'document', items: ['Source'] },
		{ name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
		{ name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll'] },
		{ name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },

		'/',
		{ name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Indent', 'Outdent', '-', 'JustifyRight', 'JustifyCenter', 'JustifyLeft', 'JustifyBlock', '-', 'BidiRtl', 'BidiLtr'] },
		{ name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'] },

		{ name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar'] },
		{ name: 'colors', items: ['TextColor', 'BGColor'] },
        { name: 'links', items: ['Link', 'Unlink'] },
        ]
    });


    function viewModel() {
        var self = this;
        self.messageId = ko.observable($('#MessageId').val())
        self.content = ko.observable();
        
        self.save = function () {
            self.content(CKEDITOR.instances.editor1.getData());

            ShowLoder();

            $.ajax({
                type: "POST",
                url: '/Admin/Edit',
                data: {
                    'Id': self.messageId(),                   
                    'GeneralText': self.content(),                    
                },
                success: function (data) {
                    if (data.Success) {
                        hideLoder();
                        getCookieMessages();
                        window.location.reload();
                    }
                },
                failure: function () {
                    hideLoder();
                    getCookieMessages();
                }
            });
        };

        self.saveForm = function () {
            self.save();
        };
               
    }


    ko.applyBindings(new viewModel());

});
