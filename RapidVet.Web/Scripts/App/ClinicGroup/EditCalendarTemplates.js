﻿var viewModel;

$(function () {
    //Calendar templates
    CKEDITOR.replace('CalenderEntryEmailTemplateEditor', { height: '250px' });  
});


function viewModel() {
    var self = this;
  
    self.TemplateKeywords = ko.mapping.fromJS([]);

    self.fillTemplates = function () {
        //Calendar templates
        $("#CalenderEntryEmailTemplate").val(CKEDITOR.instances.CalenderEntryEmailTemplateEditor.getData());
        ShowLoder();
        return true;
    };

    self.getTemplateKeywords = function () {
        $.getJSON('/ClinicGroups/GetTemplateKeywordsForGroup/?groupName=Calendar', function (data) {
            if (data != null) {
                ko.mapping.fromJS(data, self.TemplateKeywords);
            }
            else
            {
                self.TemplateKeywords(null);
            }
        });
    }

    self.showKeyWordsHelpModal = function () {       
        $('#KeyWordsHelpModal').modal('show');
    };

    self.hideKeyWordsHelpModal = function () {
        $('#KeyWordsHelpModal').modal('hide');
    };

    self.getTemplateKeywords();
}

$(function () {    
    viewModel = new viewModel();
    ko.applyBindings(viewModel);
});

