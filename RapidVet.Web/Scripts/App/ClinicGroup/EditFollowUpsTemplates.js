﻿var viewModel;

$(function () {
    //FollowUp templates
    CKEDITOR.replace('FollowUpsEmailTemplateEditor', { height: '250px' });
    //   CKEDITOR.replace('FollowUpsSmsTemplateEditor', { height: '150px' });
});


function viewModel() {
    var self = this;
  
    self.TemplateKeywords = ko.mapping.fromJS([]);

    self.fillTemplates = function () {
        //FollowUp templates
        $("#FollowUpsEmailTemplate").val(CKEDITOR.instances.FollowUpsEmailTemplateEditor.getData());
        //$("#FollowUpsSmsTemplate").val(CKEDITOR.instances.FollowUpsSmsTemplateEditor.getData());
        ShowLoder();
        return true;
    };      

    self.getTemplateKeywords = function () {
        $.getJSON('/ClinicGroups/GetTemplateKeywordsForGroup/?groupName=FollowUps', function (data) {
            if (data != null) {
                ko.mapping.fromJS(data, self.TemplateKeywords);
            }
            else {
                self.TemplateKeywords(null);
            }
        });
    }

    self.showKeyWordsHelpModal = function () {
        $('#KeyWordsHelpModal').modal('show');
    };

    self.hideKeyWordsHelpModal = function () {
        $('#KeyWordsHelpModal').modal('hide');
    };

    self.getTemplateKeywords();
}

$(function () {    
    viewModel = new viewModel();
    ko.applyBindings(viewModel);
});

