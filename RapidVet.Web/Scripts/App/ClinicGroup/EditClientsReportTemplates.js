﻿var viewModel;

$(function () {
    //ClientsReport templates
    CKEDITOR.replace('ClientsStikerTemplateEditor', { height: '150px' });
    CKEDITOR.replace('ClientsEmailTemplateEditor', { height: '250px' });
    //  CKEDITOR.replace('ClientsSmsTemplateEditor', { height: '150px' });

});


function viewModel() {
    var self = this;
  
    self.TemplateKeywords = ko.mapping.fromJS([]);

    self.fillTemplates = function () {
        //ClientsReport templates
        $("#ClientsStikerTemplate").val(CKEDITOR.instances.ClientsStikerTemplateEditor.getData());
        $("#ClientsEmailTemplate").val(CKEDITOR.instances.ClientsEmailTemplateEditor.getData());
        //$("#ClientsSmsTemplate").val(CKEDITOR.instances.ClientsSmsTemplateEditor.getData());
        ShowLoder();
        return true;
    };      

    self.getTemplateKeywords = function () {
        $.getJSON('/ClinicGroups/GetTemplateKeywordsForGroup/?groupName=ClientsReport', function (data) {
            if (data != null) {
                ko.mapping.fromJS(data, self.TemplateKeywords);
            }
            else {
                self.TemplateKeywords(null);
            }
        });
    }

    self.showKeyWordsHelpModal = function () {
        $('#KeyWordsHelpModal').modal('show');
    };

    self.hideKeyWordsHelpModal = function () {
        $('#KeyWordsHelpModal').modal('hide');
    };

    self.getTemplateKeywords();
}

$(function () {    
    viewModel = new viewModel();
    ko.applyBindings(viewModel);
});

