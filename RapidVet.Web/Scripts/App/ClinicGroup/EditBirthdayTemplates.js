﻿var viewModel;

$(function () {
    //Birthday templates
    //CKEDITOR.replace('BirthdayLetterTemplateEditor', { height: '250px' });
    CKEDITOR.replace('BirthdayStickerTemplateEditor', { height: '150px' });
    CKEDITOR.replace('BirthdayEmailTemplateEditor', { height: '250px' });
    // CKEDITOR.replace('BirthdaySmsTemplateEditor', { height: '150px' });

});


function viewModel() {
    var self = this;
  
    self.TemplateKeywords = ko.mapping.fromJS([]);

    self.fillTemplates = function () {
        //Birthday templates
        //$("#BirthdayLetterTemplate").val(CKEDITOR.instances.BirthdayLetterTemplateEditor.getData());
        $("#BirthdayStickerTemplate").val(CKEDITOR.instances.BirthdayStickerTemplateEditor.getData());
        $("#BirthdayEmailTemplate").val(CKEDITOR.instances.BirthdayEmailTemplateEditor.getData());
        //$("#BirthdaySmsTemplate").val(CKEDITOR.instances.BirthdaySmsTemplateEditor.getData());
        ShowLoder();
        return true;
    };      

    self.getTemplateKeywords = function () {
        $.getJSON('/ClinicGroups/GetTemplateKeywordsForGroup/?groupName=Birthday', function (data) {
            if (data != null) {
                ko.mapping.fromJS(data, self.TemplateKeywords);
            }
            else {
                self.TemplateKeywords(null);
            }
        });
    }

    self.showKeyWordsHelpModal = function () {
        $('#KeyWordsHelpModal').modal('show');
    };

    self.hideKeyWordsHelpModal = function () {
        $('#KeyWordsHelpModal').modal('hide');
    };

    self.getTemplateKeywords();
}

$(function () {    
    viewModel = new viewModel();
    ko.applyBindings(viewModel);
});

