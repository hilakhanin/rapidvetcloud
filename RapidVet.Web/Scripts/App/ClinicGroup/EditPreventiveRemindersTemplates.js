﻿var viewModel;

$(function () {
    //PreventiveMedicineReminders templates
    // CKEDITOR.replace('PreventiveMedicineSmsTemplateEditor', { height: '150px' });
    CKEDITOR.replace('PreventiveMedicineLetterTemplateEditor', { height: '250px' });
    CKEDITOR.replace('PreventiveMedicineLetterMultiAnimalsTemplateEditor', { height: '250px' });
    CKEDITOR.replace('PreventiveMedicinePostCardTemplateEditor', { height: '250px' });
    CKEDITOR.replace('PreventiveMedicinePostCardMultiAnimalsTemplateEditor', { height: '250px' });
    CKEDITOR.replace('PreventiveMedicineStikerTemplateEditor', { height: '150px' });
    CKEDITOR.replace('PreventiveMedicineEmailTemplateEditor', { height: '250px' });
    CKEDITOR.replace('PreventiveMedicineEmailMultiAnimalsTemplateEditor', { height: '250px' });
});


function viewModel() {
    var self = this;
  
    self.TemplateKeywords = ko.mapping.fromJS([]);
    
    self.fillTemplates = function () {
        //PreventiveMedicineReminders templates
        //$("#PreventiveMedicineSmsTemplate").val(CKEDITOR.instances.PreventiveMedicineSmsTemplateEditor.getData());
        $("#PreventiveMedicineLetterTemplate").val(CKEDITOR.instances.PreventiveMedicineLetterTemplateEditor.getData());
        $("#PreventiveMedicineLetterMultiAnimalsTemplate").val(CKEDITOR.instances.PreventiveMedicineLetterMultiAnimalsTemplateEditor.getData());
        $("#PreventiveMedicinePostCardTemplate").val(CKEDITOR.instances.PreventiveMedicinePostCardTemplateEditor.getData());
        $("#PreventiveMedicinePostCardMultiAnimalsTemplate").val(CKEDITOR.instances.PreventiveMedicinePostCardMultiAnimalsTemplateEditor.getData());
        $("#PreventiveMedicineStikerTemplate").val(CKEDITOR.instances.PreventiveMedicineStikerTemplateEditor.getData());
        $("#PreventiveMedicineEmailTemplate").val(CKEDITOR.instances.PreventiveMedicineEmailTemplateEditor.getData());
        $("#PreventiveMedicineEmailMultiAnimalsTemplate").val(CKEDITOR.instances.PreventiveMedicineEmailMultiAnimalsTemplateEditor.getData());
        ShowLoder();
        return true;
    };      

    self.getTemplateKeywords = function () {
        $.getJSON('/ClinicGroups/GetTemplateKeywordsForGroup/?groupName=PreventiveMedicineReminders', function (data) {
            if (data != null) {
                ko.mapping.fromJS(data, self.TemplateKeywords);
            }
            else {
                self.TemplateKeywords(null);
            }
        });
    }

    self.showKeyWordsHelpModal = function () {
        $('#KeyWordsHelpModal').modal('show');
    };

    self.hideKeyWordsHelpModal = function () {
        $('#KeyWordsHelpModal').modal('hide');
    };

    self.getTemplateKeywords();
}

$(function () {    
    viewModel = new viewModel();
    ko.applyBindings(viewModel);
});

