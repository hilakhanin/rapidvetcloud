﻿var viewModel;

$(function () {
    //TreatmentReport templates
    CKEDITOR.replace('TreatmentStikerTemplateEditor', { height: '150px' });
    CKEDITOR.replace('TreatmentEmailTemplateEditor', { height: '250px' });
    //  CKEDITOR.replace('TreatmentSmsTemplateEditor', { height: '150px' });
});


function viewModel() {
    var self = this;
  
    self.TemplateKeywords = ko.mapping.fromJS([]);

    self.fillTemplates = function () {
        //TreatmentReport templates
        $("#TreatmentStikerTemplate").val(CKEDITOR.instances.TreatmentStikerTemplateEditor.getData());
        $("#TreatmentEmailTemplate").val(CKEDITOR.instances.TreatmentEmailTemplateEditor.getData());
        //$("#TreatmentSmsTemplate").val(CKEDITOR.instances.TreatmentSmsTemplateEditor.getData());
        ShowLoder();
        return true;
    };      


    self.getTemplateKeywords = function () {
        $.getJSON('/ClinicGroups/GetTemplateKeywordsForGroup/?groupName=VisitsReport', function (data) {
            if (data != null) {
                ko.mapping.fromJS(data, self.TemplateKeywords);
            }
            else {
                self.TemplateKeywords(null);
            }
        });
    }

    self.showKeyWordsHelpModal = function () {
        $('#KeyWordsHelpModal').modal('show');
    };

    self.hideKeyWordsHelpModal = function () {
        $('#KeyWordsHelpModal').modal('hide');
    };

    self.getTemplateKeywords();
}

$(function () {    
    viewModel = new viewModel();
    ko.applyBindings(viewModel);
});

