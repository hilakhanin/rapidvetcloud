﻿$(function () {

    function Terminal(data) {
        var self = this;
        self.id = ko.observable(data.Id);
        self.name = ko.observable(data.Name).extend({ required: { params: true, message: "שדה חובה" } });;
        self.terminalId = ko.observable(data.TerminalId).extend({ required: { params: true, message: "שדה חובה" } });;
        self.terminalPassword = ko.observable(data.Password).extend({ required: { params: true, message: "שדה חובה" } });;
        self.isDefaultTerminal = ko.observable(data.IsDefault == true ? 'on' : 'off');
    }

    function viewModel() {

        var self = this;

        self.terminals = ko.observableArray();
        self.issuerId = ko.observable($('#issuerId').val());

        //actions
        self.add = function () {
            self.terminals.push(new Terminal({ 'Id': 0, 'TerminalId': null, 'Password': null, 'IsDefault': false, 'Name': null, }));
        };

       

        self.isValid = function () {
            var errors = ko.validation.group(self, { deep: true, observable: true });
            errors.showAllMessages(true);
            return errors().length == 0;
        };



        self.save = function (element) {
            self.Errors = ko.validation.group(element);
            if (self.Errors().length == 0) {
                ShowLoder();

                $.ajax({
                    type: "POST",
                    url: '/Issuers/UpdateEasyCardSettings',
                    data: {
                        'Id': element.id(),
                        'IssuerId': self.issuerId(),
                        'TerminalId': element.terminalId(),
                        'Password': element.terminalPassword(),
                        'IsDefault': element.isDefaultTerminal() == 'on',
                        'Name':element.name()
                    },
                    success: function (data) {
                        if (data.success && element.id() == 0) {
                            element.id(data.ItemId);
                        }
                    }
                }).done(function () {
                    hideLoder();
                    getCookieMessages();
                });
            } else {
                self.Errors.showAllMessages(true);
            }
        };

        self.remove = function (element) {
            if (element.id() > 0) {

                $.ajax({
                    type: "POST",
                    url: '/Issuers/DeleteEasyCardTerminal',
                    data: {
                        'Id': element.id(),
                        'IssuerId': self.issuerId(),
                        'TerminalId': element.terminalId(),
                        'Password': element.terminalPassword(),
                        'IsDefault': element.isDefaultTerminal(),
                        'Name': element.name()
                    }
                }).done(function () {
                    hideLoder();
                    getCookieMessages();

                });
            }

            self.terminals.remove(element);
        };

        //self.getData = function () {

        //    ShowLoder();

        //    $.getJSON('/CreditCardCodes/GetAllCodes', function (data) {

        //        var mapped = $.map(data, function (item) {
        //            return new CardCode(item);
        //        });

        //        self.cardCodes(mapped);

        //    }).done(function () { hideLoder(); });
        //};

        self.getData = function () {

            ShowLoder();

            $.getJSON('/Issuers/GetEasyCardData/' + self.issuerId(), function (data) {

                var mapped = $.map(data, function (item) {
                    return new Terminal(item);
                });
                self.terminals(mapped);
            }).done(function () {
                hideLoder();
            });
        };

        //execution
        self.getData();
    }


    ko.applyBindings(new viewModel());


});