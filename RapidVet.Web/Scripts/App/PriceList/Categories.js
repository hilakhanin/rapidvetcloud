﻿$(function () {

    function Category(data) {
        var self = this;
        self.id = ko.observable(data.Id);
        self.name = ko.observable(data.Name);
        self.itemsUrl = ko.computed(function () {
            return '/PriceListItems/CategoryItemsView/' + self.id();
        });

    }


    function viewModel() {

        var self = this;
        self.categories = ko.observableArray([]);
        self.modalTitle = ko.observable();
        self.categoryName = ko.observable();
        self.categoryId = ko.observable();
        self.formAction = ko.observable();
        self.categoryToDelete = ko.observable();


        //functions
        self.getCategories = function () {
            ShowLoder();

            $.getJSON('/PriceListItems/CategoriesJson', function (data) {
                var mapped = $.map(data, function (item) {
                    return new Category(item);
                });
                self.categories(mapped);
            }).done(function () {
                hideLoder();
            });
        };

        self.showCreateCategory = function () {
            self.clearModal();
            self.formAction('/PriceListCategories/Create');
            self.modalTitle('יצירת קטגוריה');
            self.showModal();
        };

        self.showEditCategory = function (element) {
            self.clearModal();
            self.categoryName(element.name());
            self.categoryId(element.id());
            self.formAction('/PriceListCategories/Edit/' + self.categoryId());
            self.modalTitle('עריכת קטגוריה');
            self.showModal();
        };

        self.submit = function () {
            $('#form').submit();
        };

        self.showDeleteCategory = function (element) {
            self.categoryToDelete(element);
            $('#deletemodal').modal('show');
        };

        self.deleteCategory = function () {
            $('#deleteForm').submit();
        };

        self.showModal = function () {
            $('#modal').modal('show');
        };

        self.clearModal = function () {
            self.modalTitle(null);
            self.categoryName(null);
            self.categoryId(null);
            self.formAction(null);
        };


        //execution
        self.getCategories();
    }

    ko.applyBindings(new viewModel());

});