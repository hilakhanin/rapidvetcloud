﻿$(function () {


    function viewModel() {

        var self = this;
        self.clinics = ko.mapping.fromJS([]);
               
        self.showAll = ko.observable('true');
        self.showExceeding = ko.observable();
        
        self.filterUrl = ko.computed(function () {
            return '?showAll=' + self.showAll();
        });
        
        //actions
        self.getData = function () {
            self.clinics([]);
                ShowLoder();
                var url = '/Admin/GetStorageUsage' + self.filterUrl();

                $.getJSON(url, function(data) {
                    ko.mapping.fromJS(data, self.clinics);
                }).done(function() {
                    hideLoder();
                });
        };

        self.init = function () {
            self.getData();
        };

        self.init();

    }

    ko.applyBindings(new viewModel());
});