﻿
var today = new Date();

$(function () {


    function viewModel() {

        var self = this;
        self.clinics = ko.mapping.fromJS([]);
               
        self.showAll = ko.observable('true');
        self.showExceeding = ko.observable();
        

        var year = today.getFullYear();

        self.years = ko.observableArray([year - 2, year - 1 , year, year + 1]);
        self.months = ko.observable([
            { Value: 1, Name: 'ינואר' },
            { Value: 2, Name: 'פברואר' },
            { Value: 3, Name: 'מרץ' },
            { Value: 4, Name: 'אפריל' },
            { Value: 5, Name: 'מאי' },
            { Value: 6, Name: 'יוני' },
            { Value: 7, Name: 'יולי' },
            { Value: 8, Name: 'אוגוסט' },
            { Value: 9, Name: 'ספטמבר' },
            { Value: 10, Name: 'אוקטובר' },
            { Value: 11, Name: 'נובמבר' },
            { Value: 12, Name: 'דצמבר' }
        ]);

        self.selectedYear = ko.observable(today.getFullYear());
        self.selectedMonth = ko.observable(today.getMonth() + 1);

        self.filterUrl = ko.computed(function () {
            return '?showAll=' + self.showAll() + '&month=' + self.selectedMonth() + '&year=' + self.selectedYear();
        });
        
        //actions
        self.getData = function () {
            self.clinics([]);
                ShowLoder();
                var url = '/Admin/GetEmailsUsage' + self.filterUrl();

                $.getJSON(url, function(data) {
                    ko.mapping.fromJS(data, self.clinics);
                }).done(function() {
                    hideLoder();
                });
        };

        self.init = function () {
            self.getData();
        };

        self.init();

    }

    ko.applyBindings(new viewModel());
});