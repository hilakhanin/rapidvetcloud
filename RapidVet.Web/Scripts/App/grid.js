﻿
//this file handles  price list grid

//currently not in use
function ToggleItemsGrid(categoryId) {

    var categoryElement = $('#' + 'category_' + categoryId);
    var itemsGrid = $('#' + 'categoryGrid_' + categoryId);
    var statusIndicator = $('#' + 'categoryStatus_' + categoryId);

    if ($(itemsGrid).hasClass('grid-active')) {
        $(itemsGrid).removeClass('grid-active').addClass('inactive');
        $(categoryElement).children('div').removeClass('grid-active').addClass('inactive');
    } else {
        $(itemsGrid).removeClass('inactive').addClass('grid-active');
        $(categoryElement).children('div').removeClass('inactive').addClass('grid-active');
    }

    $(statusIndicator).children('i').each(function () {

        if ($(this).hasClass('grid-active')) {
            $(this).addClass('inactive').removeClass('grid-active');
        } else {
            $(this).addClass('grid-active').removeClass('inactive');
        }
    });
}

//handles the barcode read event when entering new item or editing an existing item (priceListItem)
function BarcodeRead() {
    $('#Barcode').keydown(function (e) {
        if (e.keyCode == 13) { // barcode scanned!
            $('#PriceListItemTypeId').focus();
            return false; // block form from being submitted yet
        }
        return true;
    });
}

//gets called after ajax posts from grid-related views
function TariffSuccess(data) {
    Success(data);
    if (data.success) {
        var categoryId = $('#categoriesList').val();
        if (categoryId > 0) {
            var target = '/PriceListItems/CategoryItemsView/' + categoryId;
            $('#itemsGrid').load(target);
        }
        if (data.modalId) {
            $('#' + data.modalId).modal('hide');
        }
    }
}

//gets called after ajax posts related to priceListCategory
function categoryColumnSuccess(data) {
    Success(data);
    if (data.success) {
        $('#categoryModal').hide();
    }
}


//changes the items in the display to match the selected category from the dropDown menu
function CategoryChange() {
    var categoryId = $('#categoriesList').val();
    if (categoryId >= 0) {
        var menubarTarget = '/PriceListCategories/MenuBar?selected=' + categoryId;
        var menuBar = $('#categoryMenuBar');
        menuBar.load(menubarTarget, function (data) {
            menuBar.html(data);
        });
        if (categoryId > 0) {
            var itemsTarget = '/PriceListItems/CategoryItemsView/' + categoryId;
            var itemsTable = $('#itemsTable');
            itemsTable.load(itemsTarget, function (data) {
                itemsTable.html(data);

            });
        }
    } else {
        GetModal('/PriceListCategories/Create', 'categoryModal');
    }
}

