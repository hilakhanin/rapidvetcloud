﻿$(function () {

    function vaccineAndTreatmentItemMap(item) {
        var self = this;
        self.name = ko.observable(item.PriceListItem.Name);
        self.id = ko.observable(item.Id);
    }

    function nextVaccineAndTreatmentMap(item) {
        var self = this;
        self.name = ko.observable(item.VaccineOrTreatment.PriceListItem.Name);
        self.id = ko.observable(item.VaccineOrTreatmentId);
        self.days = ko.observable(item.DaysToNext);
    }

    function nextVaccineAndTreatmentFromMappedMap(item) {
        var self = this;
        self.name = ko.observable(item.name());
        self.id = ko.observable(item.id());
        self.days = ko.observable();
    }

    function nextVAndTPatternViewModel() {

        /// view model fields
        var self = this;
        self.allItems = ko.observableArray([]);
        self.selectedItems = ko.observableArray([]);
        self.animalKind = ko.observableArray([]);
        self.animalKindId = ko.observable();
        self.vaccineOrTreatmentId = ko.observable();
        self.vaccineOrTreatment = ko.observableArray([]);
        self.vaccineOrTreatmentName = ko.observable();
        self.selectedItem = ko.observable();
        ///-------- fields end-----------

        /// getting data from server and page
        var vaccineOrTreatmentId = $('input[type="hidden"][name="Id"]').val();
        if (!vaccineOrTreatmentId) {
            vaccineOrTreatmentId = 0;
        }
        //general pattern fields
        $.getJSON('/VaccinesAndTreatments/NextVaccinesAndTreatmentsForItem/' + vaccineOrTreatmentId, function (data) {

            var allItems = $.map(data.AllItems, function (item) {
                return new vaccineAndTreatmentItemMap(item);
            });
            var selectedItems = $.map(data.SelectedItems, function (item) {
                return new nextVaccineAndTreatmentMap(item);
            });

            self.allItems(allItems);
            self.selectedItems(selectedItems);
            self.animalKind(data.AnimalKind);
            self.animalKindId(data.AnimalKindId);
            self.vaccineOrTreatment(data.VaccineOrTreatment);
            self.vaccineOrTreatmentName(data.VaccineOrTreatment.PriceListItem.Name);
            self.vaccineOrTreatmentId(data.VaccineOrTreatmentId);
        });

        self.addItem = function () {
            var matchItem = $.grep(self.allItems(), function (item) { return item.id() == self.selectedItem(); });
            var newItem = new nextVaccineAndTreatmentFromMappedMap(matchItem[0]);
            self.selectedItems.push(newItem);
        };

        self.removeItem = function (element) {
            self.selectedItems.remove(element);
        };

        self.save = function () {
            //var itemsJson = JSON.stringify(self.selectedItems());
            var itemsJson = ko.toJSON(self.selectedItems());

            $.post("../SaveNextVaccinesAndTreatments/" + vaccineOrTreatmentId, { itemsJson: itemsJson }, function (data) {
                getCookieMessages();
            });
        };

        ///-------- getting data ends------

        ///operations

        ///-------operations end --------
    }

    ko.applyBindings(nextVAndTPatternViewModel());
});