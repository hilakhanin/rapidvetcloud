/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
     config.language = 'he';
     config.defaultLanguage = 'he';
     config.extraAllowedContent = 'table(*)';
     config.allowedContent = true;
    // config.contentsCss = '/Content/css/RapidCssBundle.min.css';
   // config.uiColor = '#FF0C6E';

    //enter fix:
     config.enterMode = CKEDITOR.ENTER_BR;
     config.fillEmptyBlocks;
     config.forceEnterMode;
     config.ignoreEmptyParagraph;
    //
     config.toolbar = [
		{ name: 'document', items: ['Source'] },
		{ name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
		{ name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll'] },
		{ name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },

		'/',
		{ name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Indent', 'Outdent', '-', 'JustifyRight', 'JustifyCenter', 'JustifyLeft', 'JustifyBlock', '-', 'BidiRtl', 'BidiLtr'] },
		{ name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'] },

		{ name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar'] },
		{ name: 'colors', items: ['TextColor', 'BGColor'] },
     ];
};

