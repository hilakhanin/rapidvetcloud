﻿using DotNetOpenAuth.OAuth2;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Http;
using Google.GData.Client;
using RapidVet.Model.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidVet.Web.Infra.Google
{
    public class GoogleAuthenticator
    {
        IAuthorizationState _authenticator;
        /// <summary>
        /// google email
        /// </summary>
        public string UserID { get; set; }
        public GoogleAuthenticator(IAuthorizationState authenticator)
        {
            _authenticator = authenticator;
        }

        public bool IsValid
        {
            get
            {
                return _authenticator != null &&
                    DateTime.Compare(DateTime.Now.ToUniversalTime(), _authenticator.AccessTokenExpirationUtc.Value) < 0;
            }
        }

        public string RefreshToken
        {
            get { return _authenticator.RefreshToken; }
        }

        #region Getters
        public TokenResponse GetTokenReponse
        {
            get
            {
                return new TokenResponse()
                {
                    AccessToken = _authenticator.AccessToken,
                    ExpiresInSeconds = (long)(_authenticator.AccessTokenExpirationUtc.Value - _authenticator.AccessTokenIssueDateUtc.Value).TotalSeconds,
                    Issued = _authenticator.AccessTokenIssueDateUtc.Value,
                    RefreshToken = _authenticator.RefreshToken,
                    Scope = "https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.readonly",
                    TokenType = "bearer"
                };
            }
        }

        public OAuth2Parameters GetOAuth2Parameters
        {
            get
            {
                return new OAuth2Parameters()
                {
                    ClientId = GoogleAuthorizationHelper._clientId,
                    ClientSecret = GoogleAuthorizationHelper._clientSecret,
                    RedirectUri = "urn:ietf:wg:oauth:2.0:oob",
                    AccessToken = _authenticator.AccessToken,
                    RefreshToken = _authenticator.RefreshToken,
                    Scope = "https://www.googleapis.com/auth/contacts https://www.googleapis.com/auth/contacts.readonly",
                };
            }
        }

        public GoogleToken GetGoogleTokenRepository
        {
            get
            {
                return new GoogleToken()
                {
                    AccessTokenExpirationUtc = _authenticator.AccessTokenExpirationUtc.Value,
                    AccessTokenIssueDateUtc = _authenticator.AccessTokenIssueDateUtc.Value,
                    AccessToken = _authenticator.AccessToken,
                    RefreshToken = _authenticator.RefreshToken,
                    EMail = UserID
                };
            }
        } 
        #endregion      
    }
}