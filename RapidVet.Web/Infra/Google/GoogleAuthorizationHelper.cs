﻿using DotNetOpenAuth.OAuth2;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace RapidVet.Web.Infra.Google
{
    public static class GoogleAuthorizationHelper
    {
        public static string _clientId = ConfigurationManager.AppSettings["GoogleSyncClientId"];
        internal static string _clientSecret = ConfigurationManager.AppSettings["GoogleSyncClientSecret"];
        internal static string _redirectUri = ConfigurationManager.AppSettings["GoogleSyncRedirectUri"];
        internal static string _appName = ConfigurationManager.AppSettings["GoogleSyncAppName"];
        internal static string[] Scopes = new[] { "https://www.googleapis.com/auth/calendar", "https://www.googleapis.com/auth/calendar.readonly", "https://www.googleapis.com/auth/contacts", "https://www.googleapis.com/auth/contacts.readonly" };

        public static string GetScopes()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var i in Scopes)
                sb.AppendFormat("{0} ", i);

            return sb.ToString().Trim();
        }

        public static string GetAuthorizationUrl(string emailAddress)
        {
            var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description, _clientId, _clientSecret);
            IAuthorizationState authorizationState = new AuthorizationState(Scopes);
            authorizationState.Callback = new Uri(_redirectUri);

            UriBuilder builder = new UriBuilder(provider.RequestUserAuthorization(authorizationState));
            NameValueCollection queryParameters = HttpUtility.ParseQueryString(builder.Query);

            queryParameters.Set("access_type", "offline");
            queryParameters.Set("approval_prompt", "force");
            queryParameters.Set("user_id", emailAddress);

            builder.Query = queryParameters.ToString();
            return builder.Uri.ToString();
        }

        public static GoogleAuthenticator GetAuthenticator(string authorizationCode)
        {
            var client = new NativeApplicationClient(GoogleAuthenticationServer.Description, _clientId, _clientSecret);
            IAuthorizationState state = new AuthorizationState() { Callback = new Uri(_redirectUri) };
            state = client.ProcessUserAuthorization(authorizationCode, state);

            return new GoogleAuthenticator(state);
        }

        public static GoogleAuthenticator RefreshAuthenticator(string refreshToken)
        {
            var state = new AuthorizationState(Scopes)
            {
                RefreshToken = refreshToken
            };

            var client = new NativeApplicationClient(GoogleAuthenticationServer.Description, _clientId, _clientSecret);
            bool result = client.RefreshToken(state);

            return new GoogleAuthenticator(state);
        }
    }
}