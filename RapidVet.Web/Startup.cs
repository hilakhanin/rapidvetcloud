﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin;
using Owin;
using Hangfire;
using Hangfire.SqlServer;

[assembly: OwinStartup(typeof(RapidVet.Web.Startup))]
namespace RapidVet.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            try
            {
                app.UseHangfire(config =>
                    {
                        config.UseSqlServerStorage(System.Configuration.ConfigurationManager.ConnectionStrings["RapidVetDataContext"].ConnectionString, new SqlServerStorageOptions() { InvisibilityTimeout = TimeSpan.FromMinutes(120) });
                        config.UseServer();
                    });
            }
            catch (Exception ex)
            {
                BaseLogger.LogWriter.WriteExPath(ex, System.Configuration.ConfigurationManager.AppSettings["LOG_PATH"]);
            }
        }
    }
}