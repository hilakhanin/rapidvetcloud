﻿using RapidVet.Helpers;
using RapidVet.Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages;

namespace RapidVet.Web.Views
{
    public abstract class ViewBase<T> : System.Web.Mvc.WebViewPage<T>
    {
        private User _currentUser;

        public User CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    _currentUser = ViewBag.CurrentUser as User;
                }
                return _currentUser;
            }
        }

        public string Alert
        {
            get
            {
                return ViewBag.Alert as string;
            }
        }

        public AlertType? AlertType
        {
            get
            {
                return ViewBag.AlertType as AlertType?;
            }
        }

    }
}