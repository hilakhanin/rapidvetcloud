﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RapidVet.Repository;
using RapidVet.Repository.Clinics;

namespace RapidVet.Tests
{
    public class TestBase
    {
        public TestBase()
        {
            //// This Part delete the data base befor every test class run.
            //Database.SetInitializer(new TestDeleteInitializer());
            //using (var dataContext = new RapidVetDataContext())
            //{
            //    dataContext.Database.Initialize(false);
            //}
        }

        private IClinicGroupRepository _clinicGroupRepository;

        protected IClinicGroupRepository ClinicGroupRepository
        {
            get
            {
                if (_clinicGroupRepository == null)
                {
                    _clinicGroupRepository = new ClinicGroupRepository();
                }

                return _clinicGroupRepository;
            }
        }

        [ClassCleanup]
        public  void BaseCleanup()
        {
            if (_clinicGroupRepository != null)
            {
                _clinicGroupRepository.Dispose();
            }
        }
    }
}
