﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RapidVet.Repository.Finances;
using RapidVet.WebModels.Finance;

namespace RapidVet.Tests.Repository_Tests.PriceList_Tests
{
    [TestClass]
    public class PriceShift
    {
        [TestMethod]
        public void Increase_Precentage()
        {
            var tariffId = 1;
            var method = PriceShiftMethod.Precentage;
            var action = PriceShiftAction.Increase;
            var amount = (decimal)12.5;
            var repository = new PriceListRepository();
            var result = repository.ShiftPrices(tariffId, method, action, amount);
            Assert.IsTrue(result.Success);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.Message));
        }


        [TestMethod]
        public void Decrease_Precentage()
        {
            var tariffId = 1;
            var method = PriceShiftMethod.Precentage;
            var action = PriceShiftAction.Decrease;
            var amount = (decimal)12.5;
            var repository = new PriceListRepository();
            var result = repository.ShiftPrices(tariffId, method, action, amount);
            Assert.IsTrue(result.Success);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.Message));
        }


        [TestMethod]
        public void Increase_Nis()
        {
            var tariffId = 1;
            var method = PriceShiftMethod.Nis;
            var action = PriceShiftAction.Decrease;
            var amount = (decimal)12.5;
            var repository = new PriceListRepository();
            var result = repository.ShiftPrices(tariffId, method, action, amount);
            Assert.IsTrue(result.Success);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.Message));
        }


        [TestMethod]
        public void Decrease_Nis()
        {
            var tariffId = 1;
            var method = PriceShiftMethod.Nis;
            var action = PriceShiftAction.Decrease;
            var amount = (decimal)99999999.32;
            var repository = new PriceListRepository();
            var result = repository.ShiftPrices(tariffId, method, action, amount);
            Assert.IsTrue(result.Success);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.Message));
        }
    }
}
