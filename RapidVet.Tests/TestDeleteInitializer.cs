﻿//using System;
//using System.Data.Entity;
//using System.Web.Security;
//using RapidVetMembership;

//namespace RapidVet.Tests
//{
//    public class TestDeleteInitializer : DropDatabaseInitializer<Repository.RapidVetDataContext>
//    {
//        public TestDeleteInitializer()
//        {
             
//        }
//        protected override void Seed(Repository.RapidVetDataContext context)
//        {
//            MembershipCreateStatus Status;
//            var membership = new CodeFirstMembershipProvider();
//            membership.CreateUser("doctor", "123456", "demo@demo.com", null, null, true, null, out Status);
//            var roles = new RapidVetMembership.CodeFirstRoleProvider();
//            roles.CreateRole("Doctor");
//            roles.AddUsersToRoles(new string[] { "doctor" }, new string[] { "Doctor" });
//            base.Seed(context);
//        }
//    }

//    public class DropDatabaseInitializer<T> : IDatabaseInitializer<T> where T : DbContext, new()
//    {
//        protected virtual void Seed(T context)
//        {
           
//        }

//        public void InitializeDatabase(T context)
//        {
//            if (context.Database.Exists())
//            {
//                context.Database.ExecuteSqlCommand("ALTER DATABASE \"" + context.Database.Connection.Database + "\" SET SINGLE_USER WITH ROLLBACK IMMEDIATE");
//                context.Database.ExecuteSqlCommand("USE master DROP DATABASE \"" + context.Database.Connection.Database +"\"");
//            }

//            context.Database.ClinicGroupCreate();

//            Seed(context);
//        }
//    }
//}
