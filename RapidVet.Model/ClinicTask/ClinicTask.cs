﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Model.Clinics;
using RapidVet.Model.Users;

namespace RapidVet.Model
{
    public class ClinicTask
    {
        public int Id { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? TargetDate { get; set; }

        [NotMapped]
        public ClinicTaskStatus Status
        {
            get { return (ClinicTaskStatus)StatusId; }
            set { StatusId = (int)value; }
        }

        public int StatusId { get; set; }

        [NotMapped]
        public ClinicTasksUrgency Urgency
        {
            get { return (ClinicTasksUrgency)UrgencyId; }
            set { UrgencyId = (int)value; }
        }

        public int UrgencyId { get; set; }

        public int? DesignatedUserId { get; set; }
        [ForeignKey("DesignatedUserId")]
        public User DesignatedUser { get; set; }

        public int ClinicId { get; set; }
        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public DateTime? Updated { get; set; }
    }
}

