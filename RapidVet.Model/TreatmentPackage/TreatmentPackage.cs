﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.Visits;

namespace RapidVet.Model.TreatmentPackages
{
    public class TreatmentPackage
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        private ICollection<PackageItem> _treatments;

        public ICollection<PackageItem> Treatments
        {
            get { return _treatments ?? (_treatments = new Collection<PackageItem>()); }
            set { _treatments = value; }
        }

        public string Name { get; set; }
    }
}
