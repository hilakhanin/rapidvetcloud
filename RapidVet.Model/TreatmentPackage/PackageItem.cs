﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Finance;

namespace RapidVet.Model.TreatmentPackages
{
    public class PackageItem
    {
        [Key]
        public int Id { get; set; }

        public int TreatmentPackageId { get; set; }

        [ForeignKey("TreatmentPackageId")]
        public TreatmentPackage TreatmentPackage { get; set; }

        public int PriceListItemId { get; set; }

        [ForeignKey("PriceListItemId")]
        public PriceListItem PriceListItem { get; set; }

        public decimal Price { get; set; }

        public decimal Amount { get; set; }

        public string Comments { get; set; }

        public decimal Discount { get; set; }

        public decimal PercentDiscount { get; set; }
    }
}
