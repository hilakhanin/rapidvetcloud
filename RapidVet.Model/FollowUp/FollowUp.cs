﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Patients;
using RapidVet.Model.Users;

namespace RapidVet.Model.PatientFollowUp
{
    public class FollowUp
    {
        [Key]
        public int Id { get; set; }

        public DateTime Created { get; set; }

        public int PatientId { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

        public DateTime DateTime { get; set; }

        public string Comment { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public DateTime? Updated { get; set; }
    }
}
