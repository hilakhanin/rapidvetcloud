﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace RapidVet.Model
{
    public static class EncryptionHelper
    {
        public static byte[] Hash(string value, byte[] salt)
        {
            return Hash(Encoding.UTF8.GetBytes(value), salt);
        }

        public static byte[] Hash(byte[] value, byte[] salt)
        {
            var saltedValue = value.Concat(salt).ToArray();
            return new SHA256Managed().ComputeHash(saltedValue);
        }

        public static byte[] GenerateSalt(int byteLength = 24)
        {
            var buff = new byte[byteLength];
            using (var prng = new RNGCryptoServiceProvider())
            {
                prng.GetBytes(buff);
            }
            return buff;
        }
    }
}
