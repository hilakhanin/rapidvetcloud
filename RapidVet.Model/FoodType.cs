﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Patients;

namespace RapidVet.Model
{
    /// <summary>
    /// a class that represents an aniomal food type
    /// </summary>
    public class FoodType
    {
        [Key]
        public int Id { get; set; }
        public int AnimalKindId { get; set; }
        //[ForeignKey("AnimalKindId")]
        //public virtual AnimalKind AnimalKind { get; set; }
        public string Value { get; set; }
    }
}
