﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clinics;
using RapidVet.Model.Expenses;
using RapidVet.Model.Users;

namespace RapidVet.Model.CliinicInventory
{
    public class InventoryOrder
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        public Clinic Clinic { get; set; }

        public int SupplierId { get; set; }

        [ForeignKey("SupplierId")]
        public Supplier Supplier { get; set; }

        private ICollection<InventoryPriceListItemOrder> _items;
        public ICollection<InventoryPriceListItemOrder> Items
        {
            get { return _items ?? (_items = new Collection<InventoryPriceListItemOrder>()); }
            set { _items = value; }
        }

        public string SupplierInvoice { get; set; }

        public string SupplierShipmentId { get; set; }

        public DateTime Created { get; set; }

        public bool Recived { get; set; }

        public DateTime? RecivedDate { get; set; }

        public string Comments { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public string UserName { get; set; }

        public int? RecivingUserId { get; set; }

        public User RecivingUser { get; set; }

        public string RecivingUserName { get; set; }

        public decimal Amount { get { return _items.Sum(i => i.OrderedQuantity*i.UnitPrice); } }
    }
}
