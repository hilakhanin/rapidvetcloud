﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Expenses;
using RapidVet.Model.Finance;

namespace RapidVet.Model.CliinicInventory
{
    public class PriceListItemSupplier
    {
      [Key]
      public int Id { get; set; }

      public int PriceListItemId { get; set; }

      [ForeignKey("PriceListItemId")]
      public PriceListItem PriceListItem { get; set; }

      public int SupplierId { get; set; }

      [ForeignKey("SupplierId")]
      public Supplier Supplier { get; set; }

      public decimal CostPrice { get; set; }

      public bool DefaultSupplier { get; set; }


    }
}
