﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;

namespace RapidVet.Model.CliinicInventory
{
    public class Inventory
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public int? OrderId { get; set; }

        [ForeignKey("OrderId")]
        public InventoryOrder Order { get; set; }

        public int? VisitId { get; set; }

        [ForeignKey("VisitId")]
        public Visit Visit { get; set; }

        public int PriceListItemId { get; set; }

        [ForeignKey("PriceListItemId")]
        public PriceListItem PriceListItem { get; set; }

        public string Barcode { get; set; }

        public int InventoryOperationTypeId { get; set; }

        public InventoryOperationType OperationType
        {
            get
            {
                return (InventoryOperationType)InventoryOperationTypeId;
            }
            set { InventoryOperationTypeId = (int)value; }
        }

        public decimal Quantity { get; set; }

        public decimal UnitPrice { get; set; }

        public DateTime Created { get; set; }

        public string UserName { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

    }
}
