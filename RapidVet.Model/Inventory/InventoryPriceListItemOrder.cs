﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Finance;

namespace RapidVet.Model.CliinicInventory
{
    public class InventoryPriceListItemOrder
    {
        [Key]
        public int Id { get; set; }

        public int InventoryOrderId { get; set; }

        [ForeignKey("InventoryOrderId")]
        public InventoryOrder Order { get; set; }

        public int PriceListItemId { get; set; }

        [ForeignKey("PriceListItemId")]
        public PriceListItem PriceListItem { get; set; }

        public decimal OrderedQuantity { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal RecievedQuantity { get; set; }

        public decimal ActualUnitPrice { get; set; }
    }
}
