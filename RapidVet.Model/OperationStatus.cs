﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace RapidVet.Model
{
    /// <summary>
    /// a class that represents the failure / success of actions. 
    /// is in use throughout the project.
    /// if an exception is thrown, will create an object from the exception
    /// </summary>
    /// 
    [DebuggerDisplay("Status: {Status}")]
    public class OperationStatus
    {
        /// <summary>
        /// treu if the poeration succedded, false if failed
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// returns the number of rows affected in DB by the command
        /// </summary>
        public int RecordsAffected { get; set; }

        /// <summary>
        /// error / success message inserted in code
        /// </summary>
        public string Message { get; set; }

        public Object OperationID { get; set; }

        /// <summary>
        /// the exception message as thrown
        /// </summary>
        public string ExceptionMessage { get; set; }

        /// <summary>
        /// the exception stack trace as thrown
        /// </summary>
        public string ExceptionStackTrace { get; set; }

        /// <summary>
        /// the  inner exception message as thrown
        /// </summary>
        public string ExceptionInnerMessage { get; set; }

        /// <summary>
        /// the inner exception stack trace as thrown
        /// </summary>
        public string ExceptionInnerStackTrace { get; set; }

        /// <summary>
        /// the id of a new add item
        /// </summary>
        public int? ItemId { get; set; }

        /// <summary>
        /// creates an OperationStatus object from a thrown exception
        /// </summary>
        /// <param name="message">user message</param>
        /// <param name="ex">thrown exception</param>
        /// <returns></returns>
        public static OperationStatus CreateFromException(string message, Exception ex)
        {
            OperationStatus opStatus = new OperationStatus
            {
                Success = false,
                Message = message,
                OperationID = null
            };

            if (ex != null)
            {
                opStatus.ExceptionMessage = ex.Message;
                opStatus.ExceptionStackTrace = ex.StackTrace;
                opStatus.ExceptionInnerMessage = (ex.InnerException == null) ? null : ex.InnerException.Message;
                opStatus.ExceptionInnerStackTrace = (ex.InnerException == null) ? null : ex.InnerException.StackTrace;
            }
            return opStatus;
        }
    }
}
