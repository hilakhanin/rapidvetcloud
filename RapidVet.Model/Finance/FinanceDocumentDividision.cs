﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Users;

namespace RapidVet.Model.Finance
{
    public class FinanceDocumentDividision
    {
        [Key]
        public int Id { get; set; }

        public int DoctorId { get; set; }

        [ForeignKey("DoctorId")]
        public User Doctor { get; set; }

        public string DoctorName { get; set; }

        public decimal Amount { get; set; }

        public int DocumentId { get; set; }

        [ForeignKey("DocumentId")]
        public FinanceDocument Document { get; set; }
    }
}
