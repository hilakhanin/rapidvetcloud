﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Visits;

namespace RapidVet.Model.Finance
{
    public class FinanceDocumentItem
    {
        public int Id { get; set; }

        public int FinanceDocumentId { get; set; }
        [ForeignKey("FinanceDocumentId")]
        public FinanceDocument FinanceDocument { get; set; }

        public int? VisitId { get; set; }
        [ForeignKey("VisitId")]
        public Visit Visit { get; set; }

        public String Description { get; set; }
        public String CatalogNumber { get; set; }

        public decimal? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }

        public decimal? Discount { get; set; }
        public decimal? DiscountPercentage { get; set; }

        public decimal TotalBeforeVAT { get; set; }

        public int? PriceListItemId { get; set; }

        [NotMapped]
        //row disscount
        public decimal TotalDiscount
        {
            get
            {
                decimal one = 1;
                return UnitPrice.Value * Quantity.Value - ((UnitPrice.Value * (one - (DiscountPercentage.Value / (decimal)100)) - Discount.Value / FinanceDocument.VAT) * Quantity.Value);
            }
        }

        public string AddedBy { get; set; }
        public int AddedByUserId { get; set; }
    }
}
