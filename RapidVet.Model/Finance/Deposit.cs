﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Enums.Finances;

namespace RapidVet.Model.Finance
{
    public class Deposit
    {
        [Key]
        public int Id { get; set; }

        public DateTime Date { get; set; }
       
        public decimal DepositAmountIncludingTax { get; set; }

        private ICollection<FinanceDocumentPayment> _payments;
        public ICollection<FinanceDocumentPayment> Payments
        {
            get { return _payments ?? (_payments = new Collection<FinanceDocumentPayment>()); }
            set { _payments = value; }
        }
    }
}
