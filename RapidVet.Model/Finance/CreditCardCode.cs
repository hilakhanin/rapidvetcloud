﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.Finance
{
    public class CreditCardCode
    {
        public int Id { get; set; }

        public String Name { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public bool Active { get; set; }

        public DateTime? Updated { get; set; }

        public int IssuerId { get; set; }

        public string ExternalAccountId { get; set; }
        
    }
}
