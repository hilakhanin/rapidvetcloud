﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Enums.Finances;
using RapidVet.Model.Finance;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.Finance
{

    public class FinanceDocumentPayment
    {
        public int Id { get; set; }
    
        /// <summary>
        /// This item is linked to Invoice or invoice/recipt
        /// </summary>
        public int? InvoiceId { get; set; }
        [ForeignKey("InvoiceId")]
        public FinanceDocument Invoice { get; set; }

        /// <summary>
        /// This item is linked torecipt
        /// </summary>
        public int? ReciptId { get; set; }
        [ForeignKey("ReciptId")]
        public FinanceDocument Recipt { get; set; }

        /// <summary>
        /// This item is linked to refound
        /// </summary>
        public int? RefoundId { get; set; }
        [ForeignKey("RefoundId")]
        public FinanceDocument Refound { get; set; }

        [ForeignKey("IssuerId")]
        public Issuer Issuer { get; set; }

        public int  IssuerId { get; set; }
        public int ClinicId { get; set; }

        public int PaymentTypeId { get; set; }

        [NotMapped]
        public PaymentType PaymentType
        {
            get { return (PaymentType)PaymentTypeId; }
            set { PaymentTypeId = (int)value; }
        }

        public decimal Sum { get; set; }
        //Credt Card
        public bool? IsEasyCard { get; set; }
        public string EasyCardDealId { get; set; }
        public string EasyCardTerminalId { get; set; }
        public String CreditDealNumber { get; set; }
        public String CreditOkNumber { get; set; }
        public String CardNumberLastFour { get; set; }
        public String CreditValidDate { get; set; }

        public int? IssuerCreditTypeId { get; set; }
        [ForeignKey("IssuerCreditTypeId")]
        public IssuerCreditType IssuerCreditType { get; set; }

        public int? CreditCardCodeId { get; set; }
        [ForeignKey("CreditCardCodeId")]
        public CreditCardCode CreditCardCode { get; set; }

        //Cheque and transfer
        public DateTime? DueDate { get; set; }
        public int? BankCodeId { get; set; }
        [ForeignKey("BankCodeId")]
        public BankCode BankCode { get; set; }
        public String BankBranch { get; set; }
        public String BankAccount { get; set; }
        //Cheque
        public String ChequeNumber { get; set; }
        //Bank Transfer
        public String BankTransferNumber { get; set; }
        public decimal? WitholdingTax { get; set; } //this is NIKUI BAMAKOR

        //NoMham
        public bool? IsNoMham { get; set; }

        [ForeignKey("DepositId")]
        public Deposit Deposit { get; set; }

        public int? DepositId { get; set; }

        public int DepositNumber { get; set; }

        public DateTime? Created { get; set; }
    }
}
