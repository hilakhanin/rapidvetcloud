﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Finance
{
    public class BankCode
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int Code { get; set; }
    }
}
