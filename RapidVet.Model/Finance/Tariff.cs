﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.Finance
{
    public class Tariff : ChangeTracker
    {
        [Key]
        public int Id { get; set; }

        //[Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public string Name { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        private ICollection<PriceListItemTariff> _items;

        public ICollection<PriceListItemTariff> TariffsItems
        {
            get { return _items ?? (_items = new Collection<PriceListItemTariff>()); }
            set { _items = value; }
        }

        private ICollection<Client> _clients;

        public ICollection<Client> Clients
        {
            get { return _clients ?? (_clients = new Collection<Client>()); }
            set { _clients = value; }
        }

        public bool Active { get; set; }

        public int? OldId { get; set; }

    }

}
