﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Enums.Finances;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Finance;
using RapidVet.Model.Users;

namespace RapidVet.Model.Finance
{
    public class FinanceDocument
    {
        public int Id { get; set; }

        public int SerialNumber { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public int? ClientId { get; set; }

        [ForeignKey("ClientId")]
        public Client Client { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public int IssuerId { get; set; }

        [ForeignKey("IssuerId")]
        public Issuer Issuer { get; set; }


        public int FinanceDocumentTypeId { get; set; }

        [NotMapped]
        public FinanceDocumentType FinanceDocumentType
        {
            get { return (FinanceDocumentType)FinanceDocumentTypeId; }
            set { FinanceDocumentTypeId = (int)value; }
        }

        public int FinanceDocumentStatusId { get; set; }

        [NotMapped]
        public FinanceDocumentStatus FinanceDocumentStatus
        {
            get { return (FinanceDocumentStatus)FinanceDocumentStatusId; }
            set { FinanceDocumentStatusId = (int)value; }
        }

        /*-------------------------------------Client Info ------------------------------------------------*/
        public String ClientName { get; set; }
        public String ClientAddress { get; set; }
        public String ClientPhone { get; set; }


        /*Payments*/
        [NotMapped]
        public List<FinanceDocumentPayment> Payments { get; set; }


        /*--------------------------------------Credit Card Payment ---------------------------------------*/
        public decimal TotalCreditPaymentSum { get; set; }
        public decimal TotalChequesPayment { get; set; }
        public decimal CashPaymentSum { get; set; }
        public decimal BankTransferSum { get; set; }

        /*------------------------------------------------------------------------------------------------*/
        public List<FinanceDocumentItem> Items { get; set; }
        /*--------------------------------------Total Billing-----------------------------------------------*/
        public decimal VAT { get; set; }

        [NotMapped]
        public decimal VatPrcent
        {
            get { return (100 * VAT - 100); }
        }

        public Decimal Discount { get; set; }
        public Decimal RoundingFactor { get; set; }

        public decimal SumToPayForInvoice { get; set; }

        [NotMapped]
        public Decimal TotalSum
        {
            get
            {
                if (FinanceDocumentType == FinanceDocumentType.Invoice || FinanceDocumentType == FinanceDocumentType.Refound || FinanceDocumentType == FinanceDocumentType.Proforma)
                {
                    return SumToPayForInvoice;
                }
                return TotalCreditPaymentSum + TotalChequesPayment + BankTransferSum + CashPaymentSum;
            }
        }

        [NotMapped]
        public Decimal VatPayed { get { return TotalSum - TotalBeforeVATAfterDisscount; } }

        [NotMapped]
        public Decimal TotalBeforeVAT
        {
            get { return Items.Sum(t => t.TotalBeforeVAT); }
        }

        [NotMapped]
        public Decimal TotalBeforeVATAfterDisscount
        {
            get { return (TotalSum) / VAT; }
        }


        public String Comments { get; set; }

        public DateTime Created { get; set; }
        public int CreatorUserId { get; set; }

        private ICollection<FinanceDocumentDividision> _financeDocumentDividisions;
        public ICollection<FinanceDocumentDividision> FinanceDocumentDividisions
        {
            get { return _financeDocumentDividisions ?? (_financeDocumentDividisions = new Collection<FinanceDocumentDividision>()); }
            set { _financeDocumentDividisions = value; }
        }

        public DateTime? OrginalPrintedDate { get; set; }

        public bool ConvertedFromRecipt { get; set; }

        public int ConvertedFromReciptSerialNumber { get; set; }

        //if the invoice is partly closed
        public decimal AmountPayed { get; set; }

        private ICollection<FinanceDocument> _relatedDocuments;
        public ICollection<FinanceDocument> RelatedDocuments
        {
            get { return _relatedDocuments ?? (_relatedDocuments = new Collection<FinanceDocument>()); }
            set { _relatedDocuments = value; }
        }

        public string DocumentCloseNote { get; set; }

        public bool? ReciptPriningOnly { get; set; }

        public string SignatureKey { get; set; }
    }
}
