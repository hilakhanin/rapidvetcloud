﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Finance
{
   public class PriceListItemTariff
    {
       [Key]
       public int Id { get; set; }

       public int ItemId { get; set; }

       [ForeignKey("ItemId")]
       public PriceListItem Item { get; set; }
           
       public int TariffId { get; set; }

       [NotMapped]
       public string TariffName { get; set; }

       [ForeignKey("TariffId")]
       public Tariff Tariff { get; set; }

       public decimal Price { get; set; }
    }
}
