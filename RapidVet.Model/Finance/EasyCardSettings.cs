﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clinics;
using System.ComponentModel.DataAnnotations;

namespace RapidVet.Model.Finance
{
    public class EasyCardSettings
    {
        public int Id { get; set; }

        [Required]
        public int IssuerId { get; set; }

        [ForeignKey("IssuerId")]
        public Issuer Issuer { get; set; }

        [Required]
        public String Name { get; set; }

        [Required]
        public String TerminalId { get; set; }

        [Required]
        public String Password { get; set; }

        public bool IsDefault { get; set; }

        public bool Active { get; set; }

        public DateTime? Updated { get; set; }

    }
}
