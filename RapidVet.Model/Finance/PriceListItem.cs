﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Clinics;
using RapidVet.Model.CliinicInventory;
using RapidVet.Model.Patients;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Users;

namespace RapidVet.Model.Finance
{
    /// <summary>
    /// a class that represents Price List Item
    /// </summary>

    public class PriceListItem : ChangeTracker
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        //[Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public int ItemTypeId { get; set; }

        [ForeignKey("ItemTypeId")]
        public PriceListItemType PriceListItemType { get; set; }


        /// <summary>
        /// PriceListItem Name
        /// </summary>
        //[Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        public string Name { get; set; }

        /// <summary>
        /// representing the category for  this priceListItem object for db & code-first purposes
        /// <summary>
        public int CategoryId { get; set; }


        /// <summary>
        /// the category this PriceListItem object is attached to
        /// </summary>
        [ForeignKey("CategoryId")]
        public PriceListCategory Category { get; set; }

        /// <summary>
        /// Catalog number (makat)
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.PriceList), Name = "Catalog")]
        public string Catalog { get; set; }

        /// <summary>
        /// Catalog number (makat)
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.PriceList), Name = "Barcode")]
        public string Barcode { get; set; }

        //public int TariffId { get; set; }

        //[ForeignKey("TariffId")]
        //public Tariff Tariff { get; set; }

        public bool FixedPrice { get; set; }
        public bool CopyFixedPrice { get; set; }

        private ICollection<PriceListItemTariff> _itemsTariffs;

        public ICollection<PriceListItemTariff> ItemsTariffs
        {
            get { return _itemsTariffs ?? (_itemsTariffs = new Collection<PriceListItemTariff>()); }
            set { _itemsTariffs = value; }
        }

        private ICollection<Patient> _patients;

        public ICollection<Patient> Patients
        {
            get { return _patients ?? (_patients = new Collection<Patient>()); }
            set { _patients = value; }
        }

        private ICollection<VaccineOrTreatment> _vaccineOrTreatment;

        public ICollection<VaccineOrTreatment> VaccineOrTreatment
        {
            get { return _vaccineOrTreatment ?? (_vaccineOrTreatment = new Collection<VaccineOrTreatment>()); }
            set { _vaccineOrTreatment = value; }
        }


        public bool Available { get; set; }

        private ICollection<PreventiveMedicineItem> _preventiveMedicineItems;
        public ICollection<PreventiveMedicineItem> PreventiveMedicineItems
        {
            get { return _preventiveMedicineItems ?? (_preventiveMedicineItems = new Collection<PreventiveMedicineItem>()); }
            set { _preventiveMedicineItems = value; }
        }

        private ICollection<PreventiveMedicineFilterPriceListItem> _reminderFilters;
        public ICollection<PreventiveMedicineFilterPriceListItem> ReminderFilters
        {
            get { return _reminderFilters ?? (_reminderFilters = new Collection<PreventiveMedicineFilterPriceListItem>()); }
            set { _reminderFilters = value; }
        }

        public string Manufacturer { get; set; }

        public int ItemsInUnit { get; set; }

        public int DefultConsuptionAmount { get; set; }

        public bool InventoryActive { get; set; }

        private ICollection<PriceListItemSupplier> _suppliers;
        public ICollection<PriceListItemSupplier> Suppliers
        {
            get { return _suppliers ?? (_suppliers = new Collection<PriceListItemSupplier>()); }
            set { _suppliers = value; }
        }

        public DateTime? Updated { get; set; }

        public bool RequireBarcode { get; set; }

        public bool IsRabiesVaccine { get; set; }

        private ICollection<Inventory> _inventories;
        public ICollection<Inventory> Inventories
        {
            get { return _inventories ?? (_inventories = new Collection<Inventory>()); }
            set { _inventories = value; }
        }

        public int? OldId { get; set; }

        public string OldCode { get; set; }

        public string OldInnoculationId { get; set; }

        //public string AddedBy { get; set; }

        //public int AddedByUserId { get; set; }
    }
}
