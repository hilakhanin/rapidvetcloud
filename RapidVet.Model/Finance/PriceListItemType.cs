﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Finance
{
   
    public class PriceListItemType
    {
         [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Code { get; set; }

        private ICollection<PriceListItem> _items { get; set; }

        public ICollection<PriceListItem> Items {
            get { return _items ?? (_items = new Collection<PriceListItem>()); }
            set { _items = value; }
        }
    }
}
