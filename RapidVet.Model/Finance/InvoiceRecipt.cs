﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace RapidVet.Model.Finance
{
    /// <summary>
    /// a class that represents an InvioceRecipt - חשבונית מס קבלה
    /// </summary>
    public class InvoiceRecipt : FinanceBase
    {
        [Key]
        public int Id { get; set; }
    }
}
