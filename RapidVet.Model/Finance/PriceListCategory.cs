﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.Finance
{
    /// <summary>
    /// a class that represents price list category
    /// </summary>
    public class PriceListCategory
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// price list Category name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// representing the Clinic for this priceListCategory object for db & code-first purposes
        /// <summary>
        public int ClinicId { get; set; }

        /// <summary>
        /// the Clinic this PriceListCategory object is attached to
        /// </summary>
        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        /// <summary>
        /// contains all PriceListItem objects that are attached to this category
        /// </summary>
        private ICollection<PriceListItem> _priceListItems;
        public ICollection<PriceListItem> PriceListItems
        {
            get { return _priceListItems ?? (_priceListItems = new Collection<PriceListItem>()); }
            set { _priceListItems = value; }
        }

        public bool Available { get; set; }

        public bool IsInventory { get; set; }

        public int? OldId { get; set; }

        public string ShortName { get; set; }

        public string OriginalName { get; set; }

    }
}
