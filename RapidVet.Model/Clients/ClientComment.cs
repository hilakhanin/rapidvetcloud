﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace RapidVet.Model.Clients
{
    /// <summary>
    /// class that represents a comment for a client object. inherits the 'CommentBase' class
    /// </summary>
    public class ClientComment : Comment
    {
        /// <summary>
        /// representing the client id for the comment. for db & code-first purposes
        /// </summary>
        public int ClientId { get; set; }
        [ForeignKey("ClientId")]

        ///the client that this comment is attached to 
        public virtual Client Client { get; set; }

        /// <summary>
        /// is this comment hidden or visible
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.Comments), Name = "ClientHidden")]
        public bool Hidden { get; set; }
    }
}
