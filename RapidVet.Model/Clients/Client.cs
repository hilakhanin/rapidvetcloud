﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Finance;
using RapidVet.Model.FullCalender;
using RapidVet.Model.InternalSettlements;
using RapidVet.Model.Patients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Quotations;
using RapidVet.Model.Users;
using RapidVet.Model.Clients.Details;
using RapidVet.Model.Visits;

namespace RapidVet.Model.Clients
{
    /// <summary>
    /// class that represents a client ( pet owner , human)
    /// </summary>
    public class Client : ChangeTracker
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }


        public virtual int LocalId { get; set; }

        /// <summary>
        ///  client first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///  client last name
        /// </summary>
        public string LastName { get; set; }

        public string Name
        {
            get { return FirstName + " " + LastName; }
        }

        public string IdCard { get; set; }
        /// <summary>
        ///  client Title
        /// </summary>
        public int? TitleId { get; set; }

        [ForeignKey("TitleId")]
        public Title Title { get; set; }

        /// <summary>
        ///  client date of birth
        /// </summary>
        public DateTime? BirthDate { get; set; }


        /// <summary>
        ///  client work place
        /// </summary>
        public string WorkePlace { get; set; }

        /// <summary>
        ///  client occupation
        /// </summary>
        public string Practice { get; set; }

        /// <summary>
        ///  who reffered the client to this Clinic / doctor
        /// </summary>
        public string ReferredBy { get; set; }

        /// <summary>
        /// another doctor that treates / treated in the past / is consoulting for this client
        /// </summary>
        public string ExternalVet { get; set; }

        /// <summary>
        /// representing the user id for the attending doctor. for code-first purposes
        /// </summary>
        public int? VetId { get; set; }

        /// <summary>
        /// the attending doctor for this client
        /// </summary>
        [ForeignKey("VetId")]
        public virtual User Vet { get; set; }


        private ICollection<Address> _addresses;
        public ICollection<Address> Addresses
        {
            get { return _addresses ?? (_addresses = new Collection<Address>()); }
            set { _addresses = value; }
        }


        private ICollection<Phone> _phones;
        public ICollection<Phone> Phones
        {
            get { return _phones ?? (_phones = new Collection<Phone>()); }
            set { _phones = value; }
        }

        private ICollection<Email> _emails;
        public ICollection<Email> Emails
        {
            get { return _emails ?? (_emails = new Collection<Email>()); }
            set { _emails = value; }
        }


        private ICollection<Patient> _patients;
        public ICollection<Patient> Patients
        {
            get { return _patients ?? (_patients = new Collection<Patient>()); }
            set { _patients = value; }
        }

        /// <summary>
        /// dose this client approves his details being transferred to the authorities
        /// </summary>
        public bool ApproveEmailTranfer { get; set; }

        /// <summary>
        /// dose this client approves his details being posted online (email, phone number) for lost animal purposes
        /// </summary>
        public bool ApproveDisplayPhone { get; set; }


        /// <summary>
        ///  representing the Clinic id for thhis client's Clinic. for code-first purposes
        /// </summary>
        public int ClinicId { get; set; }

        /// <summary>
        /// Clinic that this client belongs to
        /// </summary>
        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        /// <summary>
        /// is the client active in the Clinic
        /// </summary>
        //ClientStatus.public bool Active { get; set; }

        public int? StatusId { get; set; }

        [ForeignKey("StatusId")]
        public ClientStatus ClientStatus { get; set; }

        public string VisibleComment { get; set; }

        public string HiddenComment { get; set; }

        public int? TariffId { get; set; }

        [ForeignKey("TariffId")]
        public Tariff Tariff { get; set; }

        private ICollection<Quotation> _quotations;
        public ICollection<Quotation> Quotations
        {
            get { return _quotations ?? (_quotations = new Collection<Quotation>()); }
            set { _quotations = value; }
        }

        private ICollection<CalenderEntry> _calenderEntries;
        public ICollection<CalenderEntry> CalenderEntries
        {
            get { return _calenderEntries ?? (_calenderEntries = new Collection<CalenderEntry>()); }
            set { _calenderEntries = value; }
        }

        private ICollection<WaitingListItem> _waitingListItems;
        public ICollection<WaitingListItem> WaitingListItems
        {
            get { return _waitingListItems ?? (_waitingListItems = new Collection<WaitingListItem>()); }
            set { _waitingListItems = value; }
        }

        //public int? RegionalCounilId { get; set; }

        //[ForeignKey("RegionalCounilId")]
        //public RegionalCouncil RegionalCouncil { get; set; }

        private ICollection<FinanceDocument> _financeDocuments;
        public ICollection<FinanceDocument> FinanceDocuments
        {
            get { return _financeDocuments ?? (_financeDocuments = new Collection<FinanceDocument>()); }
            set { _financeDocuments = value; }
        }

        public decimal? Balance { get; set; }

/// <summary>
/// מספר לקוח בחשבשבת . אם לא קיים ביצוא ראשוני ייצר מספר לפי סדר רץ בהתאם למספר ראשוני שהוגדר למרפאה
///ביבוא לקוח קיים מציב PATID
///  </summary>
        public int? HsId { get; set; }

        private ICollection<InternalSettlement> _internalSettlements;
        public ICollection<InternalSettlement> InternalSettlements
        {
            get { return _internalSettlements ?? (_internalSettlements = new Collection<InternalSettlement>()); }
            set { _internalSettlements = value; }
        }

        public string PatientString { get; set; }

        public string NameWithPatients { get { return Name + " - " + PatientString; } }

        public string PatientNames { get; set; }

        private ICollection<Visit> _visits;
        public ICollection<Visit> Visits
        {
            get { return _visits ?? (_visits = new Collection<Visit>()); }
            set { _visits = value; }
        }

    }
}
