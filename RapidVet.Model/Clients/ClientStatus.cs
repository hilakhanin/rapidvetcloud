﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.Clients
{
    public class ClientStatus
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }

        private ICollection<Client> _clients;
        public ICollection<Client> Clients
        {
            get { return _clients ?? (_clients = new Collection<Client>()); }
            set { _clients = value; }
        }

        public string MeetingColor { get; set; }

        public int ClinicGroupId { get; set; }

        [ForeignKey("ClinicGroupId")]
        public ClinicGroup ClinicGroup { get; set; }
    }
}
