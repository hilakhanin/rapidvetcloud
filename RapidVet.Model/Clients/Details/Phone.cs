﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace RapidVet.Model.Clients.Details
{
    /// <summary>
    /// a class that represernts a phone number
    /// </summary>
    public class Phone// : DetailsBase
    {
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        public int ClientId { get; set; }

        /// <summary>
        /// the client this detail object is attached to
        /// </summary>
        [ForeignKey("ClientId")]
        public Client Client { get; set; }

        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Details), Name = "PhoneNumber")]
        public string PhoneNumber { get; set; }

        public int PhoneTypeId { get; set; }

        public PhoneType PhoneType { get; set; }

        public string Comment { get; set; }
    }
}
