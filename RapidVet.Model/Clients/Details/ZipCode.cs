﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidVet.Model.Clients.Details
{
    public class ZipCode
    {
        public bool Status { get; set; }
        public string Value { get; set; }
    }
}