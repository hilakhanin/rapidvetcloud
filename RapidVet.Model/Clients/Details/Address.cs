﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace RapidVet.Model.Clients.Details
{
    /// <summary>
    /// class that represents a postal address
    /// </summary>
    public class Address //: DetailsBase
    {
        /// </summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        public int ClientId { get; set; }

        /// <summary>
        /// the client this detail object is attached to
        /// </summary>
        [ForeignKey("ClientId")]
        public Client Client { get; set; }

        public int CityId { get; set; }

        [ForeignKey("CityId")]
        public City City { get; set; }
        //  public string City { get; set; }

        public string Street { get; set; }

        public string ZipCode { get; set; }

        public string GetStreetAndCity()
        {
            string address = "";

            if (!String.IsNullOrWhiteSpace(Street))
                address = Street;

            if (City != null)
                address += (" " + City.Name);


            return address.Trim();
        }
    }
}
