﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Clients.Details
{
    public class City
    {
        [Key]
        public int Id { get; set; }

        public int CityId { get; set; }

        public string Name { get; set; }

        [UIHint("RegionalCouncils")]
        public int RegionalCouncilId { get; set; }

        [ForeignKey("RegionalCouncilId")]
        public RegionalCouncil RegionalCouncil { get; set; }

        private ICollection<Address> _addresses;

        public ICollection<Address> Addresses
        {
            get { return _addresses ?? (_addresses = new Collection<Address>()); }
            set { _addresses = value; }
        }

        public bool Active { get; set; }
    }
}
