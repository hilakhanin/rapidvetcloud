﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace RapidVet.Model.Clients.Details
{
    /// <summary>
    /// a class that represents an Email address
    /// </summary>
    public class Email //: DetailsBase
    {
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        public int ClientId { get; set; }

        /// <summary>
        /// the client this detail object is attached to
        /// </summary>
        [ForeignKey("ClientId")]
        public Client Client { get; set; }


        public string Name { get; set; }
    }
}
