﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Clients.Details
{
    public class PhoneType
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        private ICollection<Phone> _phones;

        public ICollection<Phone> Phones {
            get { return _phones ?? (_phones = new Collection<Phone>()); }
            set { _phones = value; }
        }
    }
}
