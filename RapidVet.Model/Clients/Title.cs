﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using RapidVet.Model.Users;

namespace RapidVet.Model.Clients
{
    public class Title
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        private ICollection<Client> _clients;

        public ICollection<Client> Clients {
            get { return _clients ?? (_clients = new Collection<Client>()); }
            set { _clients = value; }
        }

        private ICollection<User> _users;

        public ICollection<User> Users {
            get { return _users ?? (_users = new Collection<User>()); }
            set { _users = value; }
        }
    }
}
