using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.PatientLabTests
{
    public class PatientLabTestResult
    {
        [Key]
        public int Id { get; set; }

        public int PatientLabTestId { get; set; }

        [ForeignKey("PatientLabTestId")]
        public PatientLabTest PatientLabTest { get; set; }

        public int LabTestId { get; set; }

        [ForeignKey("LabTestId")]
        public LabTest LabTest { get; set; }

        public string Result { get; set; }

    }
}