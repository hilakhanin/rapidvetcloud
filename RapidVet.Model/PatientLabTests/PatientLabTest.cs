﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;

namespace RapidVet.Model.PatientLabTests
{
    /// <summary>
    /// a class that represents a lab test
    /// </summary>
    public class PatientLabTest
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        public int PatientId { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

        public int LabTestTemplateId { get; set; }

        [ForeignKey("LabTestTemplateId")]
        public LabTestTemplate LabTestTemplate { get; set; }

        private ICollection<PatientLabTestResult> _results;
        public ICollection<PatientLabTestResult> Results
        {
            get { return _results ?? (_results = new Collection<PatientLabTestResult>()); }
            set { _results = value; }
        }

        public DateTime Created { get; set; }

    }
}
