﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Finance;
using RapidVet.Model.Clients.Details;

namespace RapidVet.Model.PreventiveMedicine
{
    public class PreventiveMedicineFilterCity
    {
        [Key]
        public int Id { get; set; }

        public int CityId { get; set; }

        [ForeignKey("CityId")]
        public City City { get; set; }

        public int FilterId { get; set; }

        [ForeignKey("FilterId")]
        public UserPreventiveMedicineReminderFilter Filter { get; set; }
    }
}
