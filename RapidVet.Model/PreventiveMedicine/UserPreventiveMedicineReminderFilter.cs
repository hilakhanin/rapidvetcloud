﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.Model.Users;

namespace RapidVet.Model.PreventiveMedicine
{
    public class UserPreventiveMedicineReminderFilter
    {
        public int Id { get; set; }

        public int ClinicId { get; set; }

        public Clinic Clinic { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public string Name { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public DateTime? FromReminderDate { get; set; }

        public DateTime? ToReminderDate { get; set; }

        public int? AnimalKindId { get; set; }

        [ForeignKey("AnimalKindId")]
        public AnimalKind AnimalKind { get; set; }

        public int? RegionalCouncilId { get; set; }

        [ForeignKey("RegionalCouncilId")]
        public RegionalCouncil RegionalCouncil { get; set; }

        private ICollection<PreventiveMedicineFilterPriceListItem> _preventiveMedicineItems;
        public ICollection<PreventiveMedicineFilterPriceListItem> PreventiveMedicineItems
        {
            get { return _preventiveMedicineItems ?? (_preventiveMedicineItems = new Collection<PreventiveMedicineFilterPriceListItem>()); }
            set { _preventiveMedicineItems = value; }
        }

        private ICollection<PreventiveMedicineFilterCity> _preventiveMedicineFilterCity;
        public ICollection<PreventiveMedicineFilterCity> PreventiveMedicineFilterCity
        {
            get { return _preventiveMedicineFilterCity ?? (_preventiveMedicineFilterCity = new Collection<PreventiveMedicineFilterCity>()); }
            set { _preventiveMedicineFilterCity = value; }
        }

        public bool ShowOnlyFutureReminders { get; set; }

        public bool ShowNotActiveClients { get; set; }

        public bool ShowNotActivePatients { get; set; }

        public bool RemoveClientsWithFutureCalendarEntries { get; set; }
        
        public int? DoctorId { get; set; }
    }
}
