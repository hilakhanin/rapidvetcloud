﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using RapidVet.Model.Finance;
using RapidVet.Model.Patients;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;
using RapidVet.Model.Quotations;

namespace RapidVet.Model.PreventiveMedicine
{
    public enum TollPaymentMethod
    {
        Cash = 1, Cheque, Postal
    }

    public class PreventiveMedicineItem
    {
        [Key]
        public int Id { get; set; }

        public int DocumentId { get; set; }

        public int PriceListItemTypeId { get; set; }

        [ForeignKey("PriceListItemTypeId")]
        public PriceListItemType PriceListItemType { get; set; }

        public int PriceListItemId { get; set; }

        [ForeignKey("PriceListItemId")]
        public PriceListItem PriceListItem { get; set; }

        public int PatientId { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

        public int? VisitId { get; set; }

        [ForeignKey("VisitId")]
        public Visit Visit { get; set; }

        public DateTime? Preformed { get; set; }

        public int? PreformingUserId { get; set; }

        [ForeignKey("PreformingUserId")]
        public User PreformingUser { get; set; }

        public string Comments { get; set; }

        public bool ExternalyPreformed { get; set; }

        public decimal Discount { get; set; }

        public decimal DiscountPercent { get; set; }

        public decimal Quantity { get; set; }

        public decimal Price { get; set; }

        public int? ParentPreventiveItemId { get; set; }

        [ForeignKey("ParentPreventiveItemId")]
        public PreventiveMedicineItem Parent { get; set; }

        public DateTime? Scheduled { get; set; }

        public decimal DiscountPerUnit
        {
            get
            {
                if (Quantity != 0)
                {
                    return Discount / Quantity;
                }
                return 0;
            }
        }

        public DateTime? LastReminderDate { get; set; }

        public int ReminderCount { get; set; }

        public string ReminderComments { get; set; }

        public int? OldId { get; set; }

        public string ExternalDrName { get; set; }

        public string ExternalDrLicenseNumber { get; set; }

        public string BatchNumber { get; set; }

        //toll data
        public bool ClientPaidOrReturnedTollVaucher { get; set; }
        public decimal TollPrice { get; set; }
        public DateTime? TollPaymentDate { get; set; }

        [NotMapped]
        public TollPaymentMethod TollPaymentMethod{
            get { return (TollPaymentMethod)TollPaymentMethodId; }
            set { TollPaymentMethodId = (int)value; }
        }
        public int TollPaymentMethodId { get; set; }

        //quotation treatment data
        public int? QuotationTreatmentId { get; set; }

        [ForeignKey("QuotationTreatmentId")]
        public QuotationTreatment QuotationTreatment { get; set; }

        public bool Invoiced { get; set; }

        /// <summary>
        /// related to preventive delete action, to allow delete item and treatment on ame window
        /// </summary>
        [NotMapped]
        public bool DeleteFromTreatment { get; set; }
    }
}
