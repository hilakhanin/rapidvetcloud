﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Finance;

namespace RapidVet.Model.PreventiveMedicine
{
    public class PreventiveMedicineFilterPriceListItem
    {
        [Key]
        public int Id { get; set; }

        public int PriceListItemId { get; set; }

        [ForeignKey("PriceListItemId")]
        public PriceListItem PriceListItem { get; set; }

        public int FilterId { get; set; }

        [ForeignKey("FilterId")]
        public UserPreventiveMedicineReminderFilter Filter { get; set; }
    }
}
