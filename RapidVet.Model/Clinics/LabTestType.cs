﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.PatientLabTests;
using RapidVet.Model.Patients;

namespace RapidVet.Model.Clinics
{
    public class LabTestType
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public string Name { get; set; }

        private ICollection<LabTestTemplate> _animalKinds;
        public ICollection<LabTestTemplate> AnimalKinds
        {
            get { return _animalKinds ?? (_animalKinds = new Collection<LabTestTemplate>()); }
            set { _animalKinds = value; }
        }
    }
}
