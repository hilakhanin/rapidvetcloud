﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Model.Clients;
using RapidVet.Model.Users;
using RapidVet.Model.Patients;

namespace RapidVet.Model.Clinics
{
    public class WaitingListItem
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public int ClientId { get; set; }

        [ForeignKey("ClientId")]
        public Client Client { get; set; }

        public bool IsSunday { get; set; }
        public bool IsMonday { get; set; }
        public bool IsTuesday { get; set; }
        public bool IsWednesday { get; set; }
        public bool IsThursday { get; set; }
        public bool IsFriday { get; set; }
        public bool IsSaturday { get; set; }
        [Required]
        public string FromTime { get; set; }
        [Required]
        public string ToTime { get; set; }
        [Required]
        public int AppointmentDurationInMinutes { get; set; }

        public UrgencyEnum UrgencyType { get; set; }

        public int UrgencyTypeId { get; set; }

        public int? DoctorId { get; set; }

        [ForeignKey("DoctorId")]
        public User Doctor { get; set; }

        public string Comments { get; set; }
        public DateTime DateStamp { get; set; }

        public int? PatientID { get; set; }

        [ForeignKey("PatientID")]
        public Patient Animal { get; set; }
    }
}
