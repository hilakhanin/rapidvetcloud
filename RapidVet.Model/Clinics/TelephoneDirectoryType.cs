﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Clinics
{
    public class TelephoneDirectoryType
    {
        public int Id { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }
        public int ClinicId { get; set; }

        public string Name { get; set; }
       
        public DateTime? Updated { get; set; }

        public bool Active { get; set; }
    }
}
