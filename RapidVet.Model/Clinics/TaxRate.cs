﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RapidVet.Model.Clinics
{
    public class TaxRate
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public DateTime TaxRateChangedDate { get; set; }

        public decimal Rate { get; set; }
    }
}