﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Expenses;
using RapidVet.Model.Finance;
using RapidVet.Model.Users;

namespace RapidVet.Model.Clinics
{
    /// <summary>
    /// a class that represents a user who is enabled to issue financial documents in ClinicGroup
    /// </summary>
    public class Issuer : ChangeTracker
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string CompanyId { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public virtual Clinic Clinic { get; set; }

        public int InvoiceInitialNum { get; set; }
        public int InvoiceReceiptInitialNum { get; set; }
        public int RefoundInitialNum { get; set; }
        public int ReceiptInitialNum { get; set; }
        public int ProformaInitialNum { get; set; }

        //public String EasyCardPassword { get; set; }
        //public String EasyCardClientId { get; set; }

        public bool Active { get; set; }

        private ICollection<FinanceDocument> _financeDocuments { get; set; }
        public ICollection<FinanceDocument> FinanceDocuments
        {
            get { return _financeDocuments ?? (_financeDocuments = new Collection<FinanceDocument>()); }
            set { _financeDocuments = value; }
        }


        private ICollection<Expense> _expenses { get; set; }
        public ICollection<Expense> Expenses
        {
            get { return _expenses ?? (_expenses = new Collection<Expense>()); }
            set { _expenses = value; }
        }

        private ICollection<IssuerCreditType> _creditTypes { get; set; }
        public ICollection<IssuerCreditType> CreditTypes
        {
            get { return _creditTypes ?? (_creditTypes = new Collection<IssuerCreditType>()); }
            set { _creditTypes = value; }
        }

        public string TaxDedationFileNumber { get; set; }

        //חשבון מע"מ עסקאות בחשבשבת
        public string HSVatAccount { get; set; }

        //חשבון הכנסות מזומן
        public string HSCashAccount { get; set; }

        //חשבון הכנסות צקים
        public string HSChequeAccount { get; set; }

        //חשבון הכנסות 
        public string HSIncomeAccount { get; set; }

        //חשבון הכנסות  פטורות
        public string HSFreeIncomeAccount { get; set; }

        //חשבון לקוח מזדמן
        public string HSRandoClientAccount { get; set; }

        //חשבון לקוח כללי
        public string HSGeneralClientAccount { get; set; }
        //סוג תנועה חשבונית
        public string HSInvoiceMovementType { get; set; }
        //סוג תנועה חשבונית זיכוי
        public string HSRefundMovementType { get; set; }
        //חשבון הכנסות העברה בנקאית
        public string HSTranferAccount { get; set; }
        //קוד מיון לקוחות
        public string HSClientSortKey { get; set; }

        public string BankDetails { get; set; }

        private ICollection<User> _users;
        public ICollection<User> Users
        {
            get { return _users ?? (_users = new Collection<User>()); }
            set { _users = value; }
        }
    }
}
