﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.PatientLabTests;

namespace RapidVet.Model.Clinics
{
    public class LabTest
    {
        [Key]
        public int Id { get; set; }

        public int LabTestTemplateId { get; set; }

        [ForeignKey("LabTestTemplateId")]
        public LabTestTemplate LabTestTemplate { get; set; }

        public string Name { get; set; }

        public decimal MinValue { get; set; }

        public decimal MaxValue { get; set; }

        private ICollection<PatientLabTestResult> _patientResults;
        public ICollection<PatientLabTestResult> PatientResults
        {
            get { return _patientResults ?? (_patientResults = new Collection<PatientLabTestResult>()); }
            set { _patientResults = value; }
        }

        public int? OldId { get; set; }

        public int? OldTestId { get; set; }
    }
}
