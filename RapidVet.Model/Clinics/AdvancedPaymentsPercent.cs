﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RapidVet.Model.Clinics
{
    public class AdvancedPaymentsPercent
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public DateTime PercentChangedDate { get; set; }

        public decimal Percent { get; set; }

        public int IssuerId { get; set; }

    }
}