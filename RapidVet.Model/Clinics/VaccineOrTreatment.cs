﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Finance;
using RapidVet.Model.Patients;

namespace RapidVet.Model.Clinics
{
    public class VaccineOrTreatment
    {
        [Key]
        public int Id { get; set; }
        
        public int Order { get; set; }

        public int AnimalKindId { get; set; }

        [ForeignKey("AnimalKindId")]
        public AnimalKind AnimalKind { get; set; }

        public int PriceListItemId { get; set; }

        [ForeignKey("PriceListItemId")]
        public PriceListItem PriceListItem { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        private ICollection<NextVaccineOrTreatment> _nextVoT;

        public ICollection<NextVaccineOrTreatment> NextVaccinesAndTreatments
        {
            get { return _nextVoT ?? (_nextVoT = new Collection<NextVaccineOrTreatment>()); }
            set { _nextVoT = value; }
        }

        public bool Active { get; set; }

        public int? OldId { get; set; }
    }
}
