﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Clients;
using RapidVet.Model.Expenses;
using RapidVet.Model.Finance;
using RapidVet.Model.Patients;
using RapidVet.Model.Visits;

//using RapidVet.WebModels.ClinicGroupModels;


namespace RapidVet.Model.Clinics
{
    /// <summary>
    /// a class that represents a group of clinics
    /// </summary>
    public class ClinicGroup : ChangeTracker
    {

        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// clinicGroup name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// clinicGroup comments
        /// </summary>
        public string Comments { get; set; }

        public int MaxConcurrentUsers { get; set; }

        public int MaxGoogleSyncAllowed { get; set; }

        public bool Active { get; set; }

        /// <summary>
        /// clinics attached to this ClinicGroup objects
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "Clinics")]
        public ICollection<Clinic> Clinics { get; set; }

        /// <summary>
        /// users attached to this ClinicGroup object
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "Users")]
        public ICollection<RapidVet.Model.Users.User> Users { get; set; }

        public ICollection<Patients.AnimalKind> AnimalKinds { get; set; }

        public ICollection<Patients.AnimalColor> AnimalColors { get; set; }

        public bool HasLogo { get; set; }
        public int LogoSizeInBytes { get; set; }

        public ClinicGroup()
        {
            Clinics = new List<Clinic>();
            Users = new List<RapidVet.Model.Users.User>();
            AnimalKinds = new List<AnimalKind>();
            AnimalColors = new Collection<AnimalColor>();
        }


        //public ClinicGroup(ClinicGroupCreate model)
        //    : this()
        //{
        //    this.Name = model.Name;
        //    this.Comments = model.Comments;
        //    this.CreatedDate = DateTime.Now;
        //}

        // public int? ConcurrentUsers { get; set; }

        public void Deactivate()
        {
            this.Active = false;

            foreach (var item in this.Clinics)
            {
                item.Active = false;
            }
        }

        private ICollection<ClientStatus> _clientStatuses;

        public ICollection<ClientStatus> ClientStatuses
        {
            get { return _clientStatuses ?? (_clientStatuses = new Collection<ClientStatus>()); }
            set { _clientStatuses = value; }
        }

        private ICollection<Medication> _medications;
        public ICollection<Medication> Medications
        {
            get { return _medications ?? (_medications = new Collection<Medication>()); }
            set { _medications = value; }
        }

        public string UnicellUserName { get; set; }

        public string UnicellPassword { get; set; }

        public string CalenderEntrySmsTemplate { get; set; }

        public string CalenderEntryEmailTemplate { get; set; }

        public string BirthdaySmsTemplate { get; set; }

        public string PreventiveMedicineSmsTemplate { get; set; }
        public string PreventiveMedicineSmsMultiAnimalsTemplate { get; set; }

        public string PreventiveMedicineLetterTemplate { get; set; }

        public string PreventiveMedicineLetterMultiAnimalsTemplate { get; set; }

        public string PreventiveMedicinePostCardTemplate { get; set; }
        public string PreventiveMedicinePostCardMultiAnimalsTemplate { get; set; }

        public string PreventiveMedicineStikerTemplate { get; set; }

        public string PreventiveMedicineEmailSubject { get; set; }

        public string PreventiveMedicineEmailTemplate { get; set; }
       
        public string PreventiveMedicineEmailMultiAnimalsSubject { get; set; }
        public string PreventiveMedicineEmailMultiAnimalsTemplate { get; set; }

        public string SignatureSmsTemplate { get; set; }

        public string EmailAddress { get; set; }
        public bool RabiesReportCC { get; set; }

        public string EmailBusinessName { get; set; }

        public decimal PostCardWidth { get; set; }

        public decimal PostCardHight { get; set; }

        public int StikerColumns { get; set; }

        public int StikerRows { get; set; }

        public decimal StikerRowHight { get; set; }

        //  public int StikerMargins { get; set; }

        public decimal StikerTopMargin { get; set; }

        public decimal StickerSideMargin { get; set; }

        public string CalenderEntryEmailSubject { get; set; }

       // public string BirthdayLetterTemplate { get; set; }

        public string BirthdayStickerTemplate { get; set; }

        public string BirthdayEmailTemplate { get; set; }

        public string BirthdayEmailSubject { get; set; }

        public string TreatmentSmsTemplate { get; set; }

        public string TreatmentStikerTemplate { get; set; }

        public string TreatmentEmailSubject { get; set; }

        public string TreatmentEmailTemplate { get; set; }

        public string ClientsSmsTemplate { get; set; }

        public string ClientsStikerTemplate { get; set; }

        public string ClientsEmailSubject { get; set; }

        public string ClientsEmailTemplate { get; set; }

        // public string PersonalLetterTemplate { get; set; }

        public string MigrationFolder { get; set; }

        public int TimeToDisconnectInactiveUsersInMinutes { get; set; }

        public bool HSModule { get; set; }

        public bool StockModule { get; set; }

        public string FollowUpsEmailSubject { get; set; }

        public string FollowUpsEmailTemplate { get; set; }

        public string FollowUpsSmsTemplate { get; set; }

        public bool TrialPeriod { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public bool UnitePreventiveReminders { get; set; }
    }
}
