﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Clinics
{
    public class NextVaccineOrTreatment
    {
        [Key]
        public int Id { get; set; }

        public int VaccineOrTreatmentId { get; set; }

        [ForeignKey("VaccineOrTreatmentId")]
        public VaccineOrTreatment VaccineOrTreatment { get; set; }

        public int ParentVaccineOrTreatmentId { get; set; }

        [ForeignKey("ParentVaccineOrTreatmentId")]
        public VaccineOrTreatment ParentVaccineOrTreatment { get; set; }

        public int DaysToNext { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Updated { get; set; }

        public int? OldVoTId { get; set; }
    }
}
