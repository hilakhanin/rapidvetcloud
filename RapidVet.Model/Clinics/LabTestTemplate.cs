﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using RapidVet.Model.PatientLabTests;
using RapidVet.Model.Patients;

namespace RapidVet.Model.Clinics
{
    public class LabTestTemplate
    {
        [Key]
        public int Id { get; set; }

        public int LabTestTypeId { get; set; }

        public string Name { get; set; }

        [ForeignKey("LabTestTypeId")]
        public LabTestType LabTestType { get; set; }

        public int AnimalKindId { get; set; }

        [ForeignKey("AnimalKindId")]
        public AnimalKind AnimalKind { get; set; }

        private ICollection<LabTest> _labTestResults;
        public ICollection<LabTest> LabTestResults
        {
            get { return _labTestResults ?? (_labTestResults = new Collection<LabTest>()); }
            set { _labTestResults = value; }
        }

        private ICollection<PatientLabTest> _patientLabTests;
        public ICollection<PatientLabTest> PatientLabTests
        {
            get { return _patientLabTests ?? (_patientLabTests = new Collection<PatientLabTest>()); }
            set { _patientLabTests = value; }
        }
    }
}
