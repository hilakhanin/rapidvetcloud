﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.ClinicGroups
{
    public class TemplateKeyWords
    {
        public int Id { get; set; }
        [Key]
        public string TemplateKeyWord { get; set; }
        [Required]
        public string KeyWord { get; set; }
        public string Comment { get; set; }
        [Key]
        public string Culture { get; set; }
    }
}
