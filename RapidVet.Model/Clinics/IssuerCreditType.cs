﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Clinics
{
    public class IssuerCreditType
    {
        public int Id { get; set; }

        [ForeignKey("IssuerId")]
        public Issuer Issuer { get; set; }
        public int IssuerId { get; set; }

        public string Name { get; set; }
        public bool PostponedPayment { get; set; }
        public bool IgnorePayments { get; set; } 
       
        //HASHAVSHEVET account
        public string ExternalAccountId { get; set; }

        public DateTime? Updated { get; set; }

        public bool Active { get; set; }
    }
}
