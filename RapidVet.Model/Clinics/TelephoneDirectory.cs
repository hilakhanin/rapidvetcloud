﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Clinics
{
    public class TelephoneDirectory
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string PrimaryPhone { get; set; }

        public string SecondaryPhone { get; set; }

        public string CellPhone { get; set; }

        public string PrimaryPhoneComment { get; set; }

        public string SecondaryPhoneComment { get; set; }

        public string CellPhoneComment { get; set; }

        public string Fax { get; set; }

        public string ContactPerson { get; set; }

        public string Comments { get; set; }

        public string Email { get; set; }

        public int ClinicId { get; set; }

        public int? TelephoneDirectoryTypeId { get; set; }

        [ForeignKey("TelephoneDirectoryTypeId")]
        public TelephoneDirectoryType TelephoneDirectoryType { get; set; }
        
        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }
    }
}
