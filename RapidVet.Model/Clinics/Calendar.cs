﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Users;

namespace RapidVet.Model.Clinics
{
    /// <summary>
    /// a class that reperesent a calendar
    /// </summary>
    public class Calendar
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// representing the Clinic for  this calendar object for db & code-first purposes
        /// <summary>
        public int ClinicId { get; set; }

        /// <summary>
        /// the Clinic this calendar object is attached to
        /// </summary>
        [ForeignKey("ClinicId")]
        public virtual Clinic Clinic { get; set; }

        /// <summary>
        ///collection of User objects attached to this calendar
        /// </summary>
        //[Display(ResourceType = typeof(RapidVet.Resources.Calendar), Name = "Users")]
        //public ICollection<User> Users { get; set; }

        /// <summary>
        /// collection of Event objects attached to this calendar
        /// </summary>
        public virtual ICollection<Event> Events { get; set; }

        /// <summary>
        /// collection of WorkingHour objects attached to this calendar
        /// </summary>
        public virtual ICollection<WorkingHours> WorkingHours { get; set; }

        /// <summary>
        /// calendar name
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Calendar), Name = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// calendar description
        /// </summary>
         [Display(ResourceType = typeof(RapidVet.Resources.Calendar), Name = "Description")]
        public string Description { get; set; }


        public Calendar()
        {
            //this.Users = new List<User>();
            this.Events = new List<Event>();
            this.WorkingHours = new List<WorkingHours>();
        }
    }
}
