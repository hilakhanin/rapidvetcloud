﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Users;

namespace RapidVet.Model.Clinics
{
    public enum MessageType
    {
        Email, Sms
    }

    public class MessageDistributionCount
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        public int EmailsCount { get; set; }

        public int SmsCount { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }
    }
}
