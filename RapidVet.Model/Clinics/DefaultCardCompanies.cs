﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Clinics
{
    public class DefaultCardCompanies
    {
        [Key]
        public int Id { get; set; }
        [Required] 
        public string Name { get; set; }
        [Required] 
        public string Localization { get; set; }

        public bool Active { get; set; }
    }
}
