﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace RapidVet.Model.Clinics
{
    /// <summary>
    /// a class that represents set period of time in a calendar.
    /// is used for working hours and for breaks
    /// </summary>
    public class WorkingHours
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// representing the calander for this workingHour object. for db & code-first purposes
        /// <summary>
        public int CalendarId { get; set; }

        /// <summary>
        /// the calendar this workingHour object is attached to
        /// </summary>
        [ForeignKey("CalendarId")]
        public Calendar Calendar { get; set; }

        /// <summary>
        /// int representation of weekdays enum
        /// </summary>
        public int? DayId { get; set; }

        /// <summary>
        /// enum representing week days
        /// </summary>
        public DayOfWeek Day
        {
            get
            {
                return (DayOfWeek)this.DayId;
            }
            set
            {
                this.DayId = (int)value;
            }
        }

        /// <summary>
        /// working hour start
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Calendar), Name = "WorkingHourFromTime")]
        public DateTime FromTime { get; set; }

        /// <summary>
        /// working hour end
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Calendar), Name = "WorkingHourToTime")]
        public DateTime ToTime { get; set; }

    }
}
