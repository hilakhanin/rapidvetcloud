﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Enums;

namespace RapidVet.Model.Clinics
{
    public class LetterTemplate
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public int LetterTemplateTypeId { get; set; }

        [NotMapped]
        public LetterTemplateType LetterTemplateType
        {
            get { return (LetterTemplateType)LetterTemplateTypeId; }
            set { LetterTemplateTypeId = (int)value; }
        }

        public string TemplateName { get; set; }

        public string Content { get; set; }
    }
}
