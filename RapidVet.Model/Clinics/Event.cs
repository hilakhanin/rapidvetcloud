﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Clients;
using RapidVet.Model.Patients;
using RapidVet.Model.Users;

namespace RapidVet.Model.Clinics
{
    /// <summary>
    /// a class that represents an event in a calendar
    /// </summary>
    public class Event
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// representing the calender for this comment for db & code-first purposes
        /// <summary>
        public int CalendarId { get; set; }

        /// <summary>
        /// the calendar this comment is attached to
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.Event), Name = "Calendar")]
        [ForeignKey("CalendarId")]
        public Calendar Calendar { get; set; }

        /// <summary>
        /// event starting date time
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Event), Name = "To")]
        public DateTime From { get; set; }
        
        /// <summary>
        /// event finish date time
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Event), Name = "From")]
        public DateTime To { get; set; }

        /// <summary>
        /// event title
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Event), Name = "Title")]
        public string Title { get; set; }

        /// <summary>
        /// event body
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.Event), Name = "Body")]
        public string Body { get; set; }

        /// <summary>
        /// representing the client for this comment. for db & code-first purposes
        /// <summary>
        public int ClientId { get; set; }

        /// <summary>
        /// he client attached to this event
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.Event), Name = "Client")]
        [ForeignKey("ClientId")]
        public virtual Client Client { get; set; }

        /// <summary>
        /// representing the patient for the this comment. for db & code-first purposes
        /// <summary>
        public int PatientId { get; set; }

        /// <summary>
        /// the patient attached to this event
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.Event), Name = "Patient")]
        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

    }
}
