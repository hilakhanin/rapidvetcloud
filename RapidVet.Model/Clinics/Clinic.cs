﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Enums;
using RapidVet.Model.Archives;
using RapidVet.Model.Clients;
using RapidVet.Model.Expenses;
using RapidVet.Model.Finance;
using RapidVet.Model.FullCalender;
using RapidVet.Model.CliinicInventory;
using RapidVet.Model.Quotations;
using RapidVet.Model.TreatmentPackages;
using RapidVet.Model.Users;
using RapidVet.Model.Visits;

namespace RapidVet.Model.Clinics
{
    /// <summary>
    /// a class that represents a Clinic
    /// </summary>
    public class Clinic : ChangeTracker
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Clinic name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// representing the Clinic group id for the Clinic for db & code-first purposes
        /// </summary>
        public int ClinicGroupID { get; set; }


        //the Clinic group that this Clinic is attached to
        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "ClinicGroup")]
        [ForeignKey("ClinicGroupID")]
        virtual public ClinicGroup ClinicGroup { get; set; }

        /// <summary>
        /// has this Clinic uploaded a logo to the system
        /// </summary>
        public bool HasLogo { get; set; }

        /// <summary>
        /// preffered documrnt header for this Clinic
        /// </summary>
        public string DocumentHeader { get; set; }

        /// <summary>
        /// representing the Clinic manager user id for the Clinic for db & code-first purposes
        /// </summary>
        public int? MainVetId { get; set; }

        /// <summary>
        /// Clinic manager user
        /// </summary>
        [Display(ResourceType = typeof(RapidVet.Resources.Clinic), Name = "MainVet")]
        [ForeignKey("MainVetId")]
        public RapidVet.Model.Users.User MainVet { get; set; }


        private ICollection<Issuer> _issuers;

        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "Issuers")]
        public ICollection<Issuer> Issuers
        {
            get { return _issuers ?? (_issuers = new Collection<Issuer>()); }
            set { _issuers = value; }
        }


        private ICollection<User> _users;
        [Display(ResourceType = typeof(RapidVet.Resources.ClinicGroup), Name = "Users")]
        public ICollection<User> Users
        {
            get { return _users ?? (_users = new Collection<User>()); }
            set { _users = value; }
        }

        [Display(ResourceType = typeof(RapidVet.Resources.Global), Name = "Active")]
        public bool Active { get; set; }

        private ICollection<PriceListCategory> _priceListCategories;
        public ICollection<PriceListCategory> PriceListCategories
        {
            get { return _priceListCategories ?? (_priceListCategories = new Collection<PriceListCategory>()); }
            set { _priceListCategories = value; }
        }

        public int DefualtTariffId { get; set; }

        private ICollection<Tariff> _tariffs;
        public ICollection<Tariff> Tariffs
        {
            get { return _tariffs ?? (_tariffs = new Collection<Tariff>()); }
            set { _tariffs = value; }
        }

        private ICollection<Client> _clients;
        public ICollection<Client> Clients
        {
            get { return _clients ?? (_clients = new Collection<Client>()); }
            set { _clients = value; }
        }

        private ICollection<TreatmentPackage> _treatmentPackages;
        public ICollection<TreatmentPackage> TreatmentPackages
        {
            get { return _treatmentPackages ?? (_treatmentPackages = new Collection<TreatmentPackage>()); }
            set { _treatmentPackages = value; }
        }

        private ICollection<VaccineOrTreatment> _vaccineOrTreatment;

        public ICollection<VaccineOrTreatment> VaccineOrTreatment
        {
            get { return _vaccineOrTreatment ?? (_vaccineOrTreatment = new Collection<VaccineOrTreatment>()); }
            set { _vaccineOrTreatment = value; }
        }

        private ICollection<LabTestType> _labTests;

        public ICollection<LabTestType> LabTests
        {
            get { return _labTests ?? (_labTests = new Collection<LabTestType>()); }
            set { _labTests = value; }
        }

        public string QuotationPreface { get; set; }

        private ICollection<ArchiveDocument> _archives;
        public ICollection<ArchiveDocument> Archives
        {
            get { return _archives ?? (_archives = new Collection<ArchiveDocument>()); }
            set { _archives = value; }
        }

        private ICollection<LetterTemplate> _letterTemplates;
        public ICollection<LetterTemplate> LetterTemplates
        {
            get { return _letterTemplates ?? (_letterTemplates = new Collection<LetterTemplate>()); }
            set { _letterTemplates = value; }
        }

        public bool ShowComplaint { get; set; }

        public bool ShowExaminations { get; set; }

        public bool ShowDiagnosis { get; set; }

        public bool ShowTreatments { get; set; }

        public bool ShowMedicins { get; set; }

        public bool ShowTemperature { get; set; }

        public bool ShowWeight { get; set; }

        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }


        private ICollection<Holiday> _holidays;
        public ICollection<Holiday> Holidays
        {
            get { return _holidays ?? (_holidays = new Collection<Holiday>()); }
            set { _holidays = value; }
        }

        private ICollection<Room> _rooms;
        public ICollection<Room> Rooms
        {
            get { return _rooms ?? (_rooms = new Collection<Room>()); }
            set { _rooms = value; }
        }

        public int? EntryDuration { get; set; }
        public string _calendarSpread;
        public string CalendarSpread { get { if (string.IsNullOrWhiteSpace(_calendarSpread)) { return "00:15:00"; } else { return _calendarSpread; } } set { _calendarSpread = value; } }

        private ICollection<CalenderEntry> _calenderEntries;
        public ICollection<CalenderEntry> CalenderEntries
        {
            get { return _calenderEntries ?? (_calenderEntries = new Collection<CalenderEntry>()); }
            set { _calenderEntries = value; }
        }

        private ICollection<BlockedDay> _blockedDays;
        public ICollection<BlockedDay> BlockedDays
        {
            get { return _blockedDays ?? (_blockedDays = new Collection<BlockedDay>()); }
            set { _blockedDays = value; }
        }

        private ICollection<WaitingListItem> _waitingListItems;
        public ICollection<WaitingListItem> WaitingListItems
        {
            get { return _waitingListItems ?? (_waitingListItems = new Collection<WaitingListItem>()); }
            set { _waitingListItems = value; }
        }

        public int CityId { get; set; }

        public int AnimalKindId { get; set; }

        public string RegionalVet { get; set; }

        public int RegionalCouncilId { get; set; }

        // Finance
        public decimal DollarExchangeRate { get; set; }

        public bool ApplyDividingIncomeModel { get; set; }

        public bool PerformDividingIncomeWithoutConciderationOfDistributedPayment { get; set; }

        // ArrangeIncomeByEnum
        public int ArrangeIncomeById { get; set; }

        public bool IssueTaxInvoiceOnly { get; set; }

        // DefaultIssuerEnum
        public int DefaultIssuerId { get; set; }

        public int DefaultNewClientStatusId { get; set; }

        //  public decimal AdvancedPaymentPercent { get; set; }

        private ICollection<TaxRate> _taxRates;
        public ICollection<TaxRate> TaxRates
        {
            get { return _taxRates ?? (_taxRates = new Collection<TaxRate>()); }
            set { _taxRates = value; }
        }

        //[NotMapped]
        //public decimal TaxRate
        //{
        //    get
        //    {
        //        var dt = DateTime.Now;
        //        //var result = 0;
        //        var taxRate =
        //            TaxRates.Where(t=>t.TaxRateChangedDate <= dt)
        //                     .OrderByDescending(r => r.TaxRateChangedDate)
        //                     .FirstOrDefault();

        //        return taxRate != null ? taxRate.Rate : 0;
        //    }

        //}

        [NotMapped]
        public decimal AdvancedPaymentPercent
        {
            get
            {
                var dt = DateTime.Now;
                //var result = 0;
                var advancedPaymentsPercent =
                    AdvancedPaymentsPercents.Where(t => t.PercentChangedDate <= dt)
                             .OrderByDescending(r => r.PercentChangedDate)
                             .FirstOrDefault();

                return advancedPaymentsPercent != null ? advancedPaymentsPercent.Percent : 0;
            }
        }

        private ICollection<AdvancedPaymentsPercent> _advancedPaymentsPercents;
        public ICollection<AdvancedPaymentsPercent> AdvancedPaymentsPercents
        {
            get { return _advancedPaymentsPercents ?? (_advancedPaymentsPercents = new Collection<AdvancedPaymentsPercent>()); }
            set { _advancedPaymentsPercents = value; }
        }

        private ICollection<ExpenseGroup> _expenseGroups;
        public ICollection<ExpenseGroup> ExpenseGroups
        {
            get { return _expenseGroups ?? (_expenseGroups = new Collection<ExpenseGroup>()); }
            set { _expenseGroups = value; }
        }

        private ICollection<Supplier> _suppliers;
        public ICollection<Supplier> Suppliers
        {
            get { return _suppliers ?? (_suppliers = new Collection<Supplier>()); }
            set { _suppliers = value; }
        }

        public ICollection<CreditCardCode> _creditCardCodes;
        public ICollection<CreditCardCode> CreditCardCodes
        {
            get { return _creditCardCodes ?? (_creditCardCodes = new Collection<CreditCardCode>()); }
            set { _creditCardCodes = value; }
        }

        public ICollection<ClinicTask> _clinicTasks;
        public ICollection<ClinicTask> ClinicTasks
        {
            get { return _clinicTasks ?? (_clinicTasks = new Collection<ClinicTask>()); }
            set { _clinicTasks = value; }
        }

        public bool InvoiceReciptOnly { get; set; }

        /// <summary>
        /// מספק התחלתי למספרי לקוח בחשבשבת
        /// </summary>
        public int? HSInitNumber { get; set; }

        private ICollection<InventoryOrder> _orders;
        public ICollection<InventoryOrder> Orders
        {
            get { return _orders ?? (_orders = new Collection<InventoryOrder>()); }
            set { _orders = value; }
        }

        private ICollection<VisitFilter> _visitFilters;
        public ICollection<VisitFilter> VisitFilters
        {
            get { return _visitFilters ?? (_visitFilters = new Collection<VisitFilter>()); }
            set { _visitFilters = value; }
        }

        //------follow up section------
        public int FollowUpTimeRangeId { get; set; }

        [NotMapped]
        public FollowUpTimeRange FollowUpTimeRange
        {
            get { return (FollowUpTimeRange)FollowUpTimeRangeId; }
            set { FollowUpTimeRangeId = (int)value; }
        }

        public int FollowUpPast { get; set; }

        public int FollowUpFuture { get; set; }

        public int DefaultFollowUpTypeId { get; set; }

        [NotMapped]
        public FollowUpTimeRange DefaultFollowUpType
        {
            get { return (FollowUpTimeRange)DefaultFollowUpTypeId; }
            set { DefaultFollowUpTypeId = (int)value; }
        }

        public int DefaultFollowUpTime { get; set; }

        public bool ShowFollowUpsInPatientHistory { get; set; }

        public bool AutomaticallySetFollowUpsAfterVisit { get; set; }

        public bool ShowPopUpWhenClientInDebt { get; set; }
     
        public bool CancelAutomaticPrinting { get; set; }

        //-----followUp end--------
        public DateTime? Updated { get; set; }

        //CoSign
        public string CoSignUserName { get; set; }

        public string CoSignPassword { get; set; }

        private ICollection<Diagnosis> _diagnoses { get; set; }
        public ICollection<Diagnosis> Diagnoses
        {
            get { return _diagnoses ?? (_diagnoses = new Collection<Diagnosis>()); }
            set { _diagnoses = value; }
        }

        public bool MoveZeroSumTreatmentsToInvoice { get; set; }

        //Licensing
        private int _archiveAllocation;
        public int ArchiveAllocation { get { if (_archiveAllocation == 0) { return 20; } else { return _archiveAllocation; } } set { _archiveAllocation = value; } }

        [System.ComponentModel.DefaultValue("false")]
        public bool ShowOldVisits { get; set; }
        public bool ShowVisitNote { get; set; }

        public bool DontMoveItemsFromVisitToInvoice { get; set; }       
        public bool DontAddToVisitNewItemsInInvoice { get; set; }

        public DateTime? MoveItemsFromVisitToInvoiceFromDate { get; set; }

        public bool HideInActiveAnimals { get; set; }
    }
}
