﻿using RapidVet.Model.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Tools
{
    public enum BackgroundJobType
    {
        Email, SMS
    }

    public class ClinicBackgroundJob
    {
        [Key]
        public int Id { get; set; }
        public string JobGuid { get; set; }
        public int ClinicId { get; set; }       
        public string JobId { get; set; }
        public string JobModule { get; set; }
        public BackgroundJobType BackgroundJobType { get; set; }
        public int Processed { get; set; }
        public int Total { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        [Required]
        public int InitiatingUserId { get; set; }
        [ForeignKey("InitiatingUserId")]
        public User InitiatingUser { get; set; }
        public bool WasCancelled { get; set; }
        public int? CancellingUserId { get; set; }
        [ForeignKey("CancellingUserId")]
        public User CancellingUser { get; set; }
    }
}
