﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Tools
{
    public class CreateDoctorRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int ClinicGroupId { get; set; }
        public int ClinicId { get; set; }
        public string InterfaceName { get; set; }
        public string ApiKey { get; set; }
        public bool IsActive { get; set; }
    }
}
