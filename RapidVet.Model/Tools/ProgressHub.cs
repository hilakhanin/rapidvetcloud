﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNet.SignalR;

namespace RapidVet.Model.Tools
{
    public class ProgressHub: Hub
    {
        
            public static void NotifyStart(string taskId)
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<ProgressHub>();
                hubContext.Clients.All.initProgressBar(taskId);
            }
            public static void NotifyProgress(string taskId, int percentage)
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<ProgressHub>();
                hubContext.Clients.All.updateProgressBar(taskId, percentage);
            }
            public static void NotifyEnd(string taskId)
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<ProgressHub>();
                hubContext.Clients.All.clearProgressBar(taskId);
            }
    }
}
