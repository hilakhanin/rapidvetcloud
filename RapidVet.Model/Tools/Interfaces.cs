﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Tools
{
    public class Interface
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public Guid ApiKey { get; set; }

    }
}
