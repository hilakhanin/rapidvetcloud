﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Tools
{
    public class GeneralSettings
    {
        public int Id { get; set; }                
        public string GeneralText { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        //clinic data export
        public bool IsExportingClinic { get; set; }
        public int ClinicId { get; set; }
        public DateTime? ExportStartTime { get; set; }
        //public string ExportNotifyEmail { get; set; }
    }
}
