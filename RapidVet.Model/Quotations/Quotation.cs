﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.Model.Users;

namespace RapidVet.Model.Quotations
{
    public class Quotation
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public int PatientId { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

        private ICollection<QuotationTreatment> _treatments;

        public ICollection<QuotationTreatment> Treatments
        {
            get { return _treatments ?? (_treatments = new Collection<QuotationTreatment>()); }
            set { _treatments = value; }
        }

        public string Name { get; set; }

        public string Comments { get; set; }

        //public decimal Discount { get; set; }

        //public decimal PercentDiscount { get; set; }

        // public decimal TotalPrice { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Updated { get; set; }

        public int? OldId { get; set; }

        public string GetTreatmentsDescription()
        {
            if (Treatments == null || Treatments.Count == 0)
                return String.Empty;

            StringBuilder sb = new StringBuilder();
            var q = (from t in Treatments
                     let row = String.Format("{0} - {1}", t.Name, t.Price == 0 ? "0" : t.Price.ToString("#.##"))
                     select sb.AppendFormat("{0}{1}", Environment.NewLine, row)).ToList();

            return q == null ? String.Empty : q[0].ToString();
        }
    }
}
