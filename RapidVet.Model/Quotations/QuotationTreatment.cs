﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clients;
using RapidVet.Model.Finance;
using RapidVet.Model.Visits;

namespace RapidVet.Model.Quotations
{
    public class QuotationTreatment
    {
        [Key]
        public int Id { get; set; }

        public int QuotationId { get; set; }

        [ForeignKey("QuotationId")]
        public Quotation Quotation { get; set; }

        public int? PriceListItemId { get; set; }

        [ForeignKey("PriceListItemId")]
        public PriceListItem PriceListItem { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public decimal Amount { get; set; }

        public string Comments { get; set; }

        public int TreatmentPackageItemId { get; set; }

        public  decimal? Discount { get; set; }

        public decimal? PercentDiscount { get; set; }

        public bool WasTreatmentMovedToExecution { get; set; }

        public int? VisitId { get; set; }

        [ForeignKey("VisitId")]
        public Visit visit { get; set; }

    }
}
