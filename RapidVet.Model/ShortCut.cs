﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model
{
   public class ShortCut
    {
       [Key]
       public int Id { get; set; }

       public string Name { get; set; }

       public string Link { get; set; }
    }
}
