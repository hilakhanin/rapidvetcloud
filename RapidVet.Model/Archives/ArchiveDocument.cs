﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;

namespace RapidVet.Model.Archives
{
    public class ArchiveDocument
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public int PatientId { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

        public int FileTypeId { get; set; }

        [NotMapped]
        public ArchivesFileType FileType
        {
            get { return (ArchivesFileType)FileTypeId; }
            set { FileTypeId = (int)value; }
        }

        public string Title { get; set; }

        public string KeyWord1 { get; set; }

        public string KeyWord2 { get; set; }

        public string KeyWord3 { get; set; }

        public string KeyWord4 { get; set; }

        public string Comments { get; set; }

        public string FileExtension { get; set; }

        public string ContentType { get; set; }

        public bool ShowInPatientHistory { get; set; }

        public bool HasAudio { get; set; }

        public DateTime Created { get; set; }

        public string FileMigrationLocation { get; set; }

        public int UploadedByUserId { get; set; }

        public int FileSizeInBytes { get; set; }
    }
}
