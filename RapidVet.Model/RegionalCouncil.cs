﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using RapidVet.Model.Clients;
using RapidVet.Model.Clients.Details;

namespace RapidVet.Model
{
    public class RegionalCouncil
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Email { get; set; }

        private ICollection<Client> _clients;
        public ICollection<Client> Clients
        {
            get { return _clients ?? (_clients = new Collection<Client>()); }
            set { _clients = value; }
        }

        private ICollection<City> _cities;
        public ICollection<City> Cities
        {
            get { return _cities ?? (_cities = new Collection<City>()); }
            set { _cities = value; }
        }

    }
}
