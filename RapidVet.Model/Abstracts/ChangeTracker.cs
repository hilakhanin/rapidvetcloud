﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Users;

namespace RapidVet.Model
{
    /// <summary>
    /// tracks changes in data enteties
    /// </summary>
    public abstract class ChangeTracker
    {
        public DateTime? CreatedDate { get; set; }

        public int? CreatedById { get; set; }
        //[ForeignKey("CreatedById")]
        //public virtual User CreatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedById { get; set; }
        //[ForeignKey("UpdatedById")]
        //public virtual User UpdatedBy { get; set; }
    }
}
