﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace RapidVet.Model
{
    /// <summary>
    /// base class for all comment classes
    /// </summary>
    public abstract class Comment : ChangeTracker
    {
        /// <summary>
        /// primary key in table
        /// </summary> 
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// body of comment
        /// </summary>
        public string CommentBody { get; set; }

        /// <summary>
        /// comment expireation date
        /// </summary>
        public DateTime? ValidThrough { get; set; }

        /// <summary>
        /// should this comment behave as a pop-up
        /// </summary>
        public bool Popup { get; set; }
    }
}
