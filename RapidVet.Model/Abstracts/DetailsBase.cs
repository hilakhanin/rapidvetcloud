﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Clients;

namespace RapidVet.Model.Clients.Details
{
    public enum DetailType { work, home, other, }

    /// <summary>
    /// base class for all details classes (phone, email, address)
    /// </summary>
    public abstract class DetailsBase
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// representing the client for this detail object. for db & code-first purposes
        /// <summary>
        public int ClientId { get; set; }

        /// <summary>
        /// the client this detail object is attached to
        /// </summary>
        [ForeignKey("ClientId")]
        public Client Client { get; set; }

        /// <summary>
        /// int representation of DetailType enum
        /// </summary>
        public int TypeId { get; set; }


        /// <summary>
        /// enum representing item type
        /// </summary>
        [NotMapped]
        public DetailType Type
        {
            get
            {
                return (DetailType)this.TypeId;
            }

            set
            {
                this.TypeId = (int)value;
            }
        }

    }
}
