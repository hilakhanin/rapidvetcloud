﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Clinics;
using RapidVet.Model.Clients;

namespace RapidVet.Model.Finance
{
    /// <summary>
    /// base class for all finance related classes
    /// </summary>
    public abstract class FinanceBase
    {
        /// <summary>
        /// representing the Clinic for this item for db & code-first purposes
        /// <summary>
        public int ClinicId { get; set; }


        /// <summary>
        /// the Clinic this item is attached to
        /// </summary>
        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }


        /// <summary>
        /// representing the client for this item. for db & code-first purposes
        /// <summary>
        public int ClientId { get; set; }


        /// <summary>
        /// the Client this item is attached to
        /// </summary>
        [ForeignKey("ClientId")]
        public Client Client { get; set; }

    }
}
