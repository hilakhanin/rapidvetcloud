﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clients;

namespace RapidVet.Model.InternalSettlements
{
    public class InternalSettlement
    {
        public int Id { get; set; }

        public String Description { get; set; }

        public decimal Sum { get; set; }

        public DateTime Date { get; set; }

        public int ClientId { get; set; }

        [ForeignKey("ClientId")]
        public Client Client { get; set; }
    }
}
