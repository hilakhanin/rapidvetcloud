﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Users
{
    public class UsersDoctorsView
    {
        [Key]
        public int Id { get; set; }
        public int UserID { get; set; }
        public int DoctorID { get; set; }
        public int ClinicID { get; set; }
        public bool ShowOnMainWindow { get; set; }
        public bool IsPrivate { get; set; }
    }
}
