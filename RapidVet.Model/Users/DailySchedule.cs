﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Users
{
    public class DailySchedule
    {
        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }
        
        [ForeignKey("UserId")]
        public User User { get; set; }
        
        public DateTime Start { get; set; }
        
        public DateTime End { get; set; }

        public int DayId { get; set; }

        public int ClinicId { get; set; }
    }
}
