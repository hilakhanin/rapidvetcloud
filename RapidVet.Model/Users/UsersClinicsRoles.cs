﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Users
{
  public  class UsersClinicsRoles
    {
      [Key]
      public int Id { get; set; }

      public int UserId { get; set; }

      [ForeignKey("UserId")]
      public User User { get; set; }

      public int ClinicId { get; set; }

      [ForeignKey("ClinicId")]
      public Clinics.Clinic Clinic { get; set; }

      public Guid RoleId { get; set; }

      [ForeignKey("RoleId")]
      public Role Role { get; set; }
    }
}
