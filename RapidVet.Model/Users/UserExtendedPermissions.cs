﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Users
{
    public enum ExtendedPermissions
    {
        ViewFinancialReports, ViewFollowUps
    }

    public class UserExtendedPermissions
    {
        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public int ObjectId { get; set; }
        public ExtendedPermissions ExtendedPermission { get; set; }
    }
}
