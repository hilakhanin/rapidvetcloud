﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Users
{
    public class ConcurrentUserEntity
    {
        [Key]
        public Guid UserName { get; set; }

        public DateTime LoginTime { get; set; }

        public int UserId { get; set; }

        public int ActiveClinicId { get; set; }

        public int ActiveClinicGroupId { get; set; }
    }
}
