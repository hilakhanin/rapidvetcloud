﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.Users
{
    public class UsersExtendedPermissionsIndexModel
    {
        public List<User> Users { get; set; }
        public List<Issuer> Issuers { get; set; }
        public List<UserExtendedPermissions> Permissions { get; set; }
    }
}
