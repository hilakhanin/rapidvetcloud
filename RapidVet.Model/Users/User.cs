﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;
using RapidVet.Enums;
using RapidVet.Exeptions;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.FullCalender;
using RapidVet.Model.CliinicInventory;
using RapidVet.Model.PatientFollowUp;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.RapidConsts;
using System.Web;

namespace RapidVet.Model.Users
{
    public class User
    {
        public User()
        {
            //Calendars = new List<Calendar>();
        }

        [Key]
        public virtual int Id { get; set; }

        [Required]
        public virtual String Username { get; set; }

        public virtual String Email { get; set; }

        public virtual String FirstName { get; set; }

        public virtual String LastName { get; set; }

        public virtual String MobilePhone { get; set; }

        [DataType(DataType.MultilineText)]
        public virtual String Comment { get; set; }

        public virtual DateTime? LastActivityDate { get; set; }

        public virtual DateTime? LastLoginDate { get; set; }

        public virtual DateTime? CreateDate { get; set; }

        public virtual DateTime? LastPasswordChangedDate { get; set; }

        public string LicenceNumber { get; set; }

        #region Password



        [Required, DataType(DataType.Password)]
        public virtual byte[] HashPassword { get; set; }

        public virtual byte[] Salt { get; set; }

        [NotMapped]
        [ScriptIgnore]
        public String Password
        {
            set
            {
                if (value != null)
                {
                    if (value.Length > 0)
                    {
                        Salt = EncryptionHelper.GenerateSalt();
                        HashPassword = EncryptionHelper.Hash(value, Salt);
                    }
                    else
                    {
                        throw new ShortPasswordExeption();
                    }
                }
            }
            get { return string.Empty; }
        }

        public bool IsPasswordValid(string password)
        {
            var passwordHash = EncryptionHelper.Hash(password, Salt);
            return HashPassword.SequenceEqual(passwordHash);
        }    
        #endregion


        public virtual ICollection<Role> Roles { get; set; }

        public int? ClinicGroupId { get; set; }

        [ForeignKey("ClinicGroupId")]
        public Clinics.ClinicGroup ClinicGroup { get; set; }

        public int PasswordFailuresSinceLastSuccess { get; set; }

        //public ICollection<RapidVet.Model.Clinics.Calendar> Calendars { get; set; }

        public bool IsUserInRole(string roles)
        {
            if (string.IsNullOrEmpty(roles))
            {
                return true;
            }
            var rolesArray = roles.Split(',');
            bool result = true;
            foreach (var s in rolesArray)
            {
                if (this.Roles == null || this.Roles.All(r => r.RoleName.ToLower() != s.ToLower()))
                {
                    result = false;
                }
            }
            return result;
        }

        public virtual DateTime? LastPasswordFailureDate { get; set; }

        public virtual DateTime? LastLockoutDate { get; set; }

        public virtual bool IsLockedOut { get; set; }

        public virtual bool IsApproved { get; set; }

        public String Name
        {
            get
            {
                var fn = FirstName;
                var ln = LastName;
                var tName = this.Title == null ? String.Empty : this.Title.Name;
                return ReturnFriendlyName(tName, fn, ln);
            }
        }

        public static string ReturnFriendlyName(string titleName, string fn, string ln)
        {
            var result = String.IsNullOrEmpty(titleName) ? String.Format("{0} {1}", fn, ln) : String.Format("{0} {1} {2}", titleName, fn, ln);
            return String.IsNullOrWhiteSpace(result) ? "--------" : result;
        }

        // public String Title { get; set; }

        public void UpdateClinic(Clinic clinic)
        {
            if (ActiveClinicId == clinic.Id)
            {
                ActiveClinicGroup.Clinics.Remove(ActiveClinic);
                ActiveClinicGroup.Clinics.Add(clinic);

            }
        }

        public void UpdateClinicGroup(ClinicGroup clinicGroup)
        {
            if (ActiveClinicGroupId == clinicGroup.Id)
            {
                ActiveClinicGroup = clinicGroup;
            }
        }


        [NotMapped]
        [ScriptIgnore]
        public Clinic ActiveClinic
        {
            get
            {
                var clinic = ActiveClinicGroup.Clinics.FirstOrDefault(c => c.Id == ActiveClinicId);

                //if(IsAdministrator && clinic ==null)
                //{
                //    clinic = ActiveClinicGroup.Clinics.FirstOrDefault();
                //    HttpContext.Current.Session["ActiveClinicId"] = clinic.Id;
                //}
                return clinic;
            }
        }

        private ClinicGroup _activeClinicGroup;
        [NotMapped]
        [ScriptIgnore]
        public ClinicGroup ActiveClinicGroup
        {
            get
            {
                if (IsAdministrator && _activeClinicGroup != null)
                {
                    return _activeClinicGroup;
                }
                else return _activeClinicGroup ?? ClinicGroup;
            }
            set
            {
                if (IsAdministrator)
                {
                    _activeClinicGroup = value;
                    _activeClinicId = null;
                }
                else
                {
                    if (ActiveClinicGroup != null && ActiveClinicGroup.Id == value.Id)
                    {
                        _activeClinicGroup = value;
                    }
                }

            }
        }

        private int? _activeClinicId;

        [NotMapped]
        [ScriptIgnore]
        public int ActiveClinicId
        {
            get
            {
                //if(IsAdministrator)
                //{
                //    if(HttpContext.Current.Session["ActiveClinicId"] == null)
                //    {
                //        HttpContext.Current.Session["ActiveClinicId"] = 1;
                //    }

                //    _activeClinicId = (int)HttpContext.Current.Session["ActiveClinicId"];

                //    return (int)HttpContext.Current.Session["ActiveClinicId"];
                //}
                if (_activeClinicId == null)
                {
                    _activeClinicId = DefaultClinicId;
                    if (DefaultClinicId == null && ActiveClinicGroup != null)
                    {
                        if (this.UsersClinicsRoleses.Any())
                        {
                            _activeClinicId = UsersClinicsRoleses.First().ClinicId;
                        }
                        if (IsAdministrator)
                        {
                            _activeClinicId = ActiveClinicGroup.Clinics.First(c=>c.Active).Id;
                        }
                    }
                }

                if (_activeClinicId != null) return _activeClinicId.Value;

                if(IsAdministrator)
                {
                    _activeClinicId = 1;
                    return _activeClinicId.Value;
                }
                throw new Exception(RapidVet.Resources.Exceptions.ClinicNotSet);
            }
            set
            {
                if ((IsClinicGroupManager && this.ActiveClinicGroup.Clinics.Any(c => c.Id == value)) || this.UsersClinicsRoleses.Any(c => c.ClinicId == value))
                {
                    _activeClinicId = value;
                }
                else
                {
                    throw new SecurityException(RapidVet.Resources.Exceptions.TheClinicIDIsInvalid);
                }
            }
        }

        private int? _activeClinicGroupId;

        [NotMapped]
        [ScriptIgnore]
        public int ActiveClinicGroupId
        {
            get
            {
                if (IsAdministrator)
                {
                    //if (HttpContext.Current.Session["ActiveClinicGroupId"] == null)
                    //{
                    //    HttpContext.Current.Session["ActiveClinicGroupId"] = 1;
                    //}

                    //_activeClinicGroupId = (int)HttpContext.Current.Session["ActiveClinicGroupId"];

                    //return (int)HttpContext.Current.Session["ActiveClinicGroupId"];
                    if (_activeClinicGroupId == null)
                    {
                        _activeClinicGroupId = 1;
                    }
                    return _activeClinicGroupId.Value;
                }
                else
                {
                    var clinicGroupId = this.ClinicGroupId;
                    if (clinicGroupId != null) return clinicGroupId.Value;
                }
                throw new Exception(RapidVet.Resources.Exceptions.ClinicGroupNotSet);
            }
            set
            {
                if (IsAdministrator)
                {
                    _activeClinicGroupId = value;
                }
            }
        }

        public ICollection<RapidVet.Model.Users.UsersClinicsRoles> UsersClinicsRoleses { get; set; }
        [ScriptIgnore]
        public bool IsClinicManager
        {
            get
            {
                if (IsClinicGroupManager)
                {
                    return true;
                }

                var usersClinicsRolese =
                    this.UsersClinicsRoleses.SingleOrDefault(
                        c =>
                        c.ClinicId == this.ActiveClinicId &&
                        c.Role.RoleName.ToLower() == RolesConsts.CLINICMANAGER.ToLower());
                if (usersClinicsRolese != null &&
                    (
                    usersClinicsRolese.Role.RoleName.ToLower() == RolesConsts.CLINICMANAGER.ToLower()
                    ))
                {
                    return true;
                }

                return false;
            }
        }
        [ScriptIgnore]
        public bool IsDoctor
        {
            get
            {
                if (IsClinicGroupManager)
                {
                    return true;
                }
                var usersClinicsRolese =
                    this.UsersClinicsRoleses.SingleOrDefault(
                        c =>
                        c.ClinicId == this.ActiveClinicId && c.Role.RoleName.ToLower() == RolesConsts.DOCTOR.ToLower());
                if (usersClinicsRolese != null &&
                    (
                    usersClinicsRolese.Role.RoleName.ToLower() == RolesConsts.CLINICMANAGER.ToLower()
                    || usersClinicsRolese.Role.RoleName.ToLower() == RolesConsts.DOCTOR.ToLower()
                    ))
                {
                    return true;
                }

                return false;
            }
        }
        [ScriptIgnore]
        public bool IsSecratery
        {
            get
            {
                if (IsClinicGroupManager)
                {
                    return true;
                }
                var usersClinicsRolese =
                    this.UsersClinicsRoleses.SingleOrDefault(
                        c =>
                        c.ClinicId == this.ActiveClinicId &&
                        c.Role.RoleName.ToLower() == RolesConsts.SECRATERY.ToLower());
                if (usersClinicsRolese != null &&
                    (
                    usersClinicsRolese.Role.RoleName.ToLower() == RolesConsts.CLINICMANAGER.ToLower()
                    || usersClinicsRolese.Role.RoleName.ToLower() == RolesConsts.DOCTOR.ToLower()
                    || usersClinicsRolese.Role.RoleName.ToLower() == RolesConsts.SECRATERY.ToLower()
                    ))
                {
                    return true;
                }

                return false;
            }
        }
        [ScriptIgnore]
        public bool IsClinicGroupManager
        {
            get
            {
                return IsUserInRole(RolesConsts.CLINICGROUPMANAGER) || IsUserInRole(RolesConsts.ADMINISTRATOR);
            }
        }
        [ScriptIgnore]
        public bool IsAdministrator
        {
            get
            {
                return IsUserInRole(RolesConsts.ADMINISTRATOR);
            }
        }

        [ScriptIgnore]
        public bool IsViewReports
        {
            get
            {
                if (IsClinicGroupManager)
                {
                    return true;
                }
                var usersClinicsRolese =
                    this.UsersClinicsRoleses.LastOrDefault(
                        c =>
                        c.ClinicId == this.ActiveClinicId &&
                        c.Role.RoleName.ToLower() == RolesConsts.VIEW_REPORTS.ToLower() ||
                        c.Role.RoleName.ToLower() == RolesConsts.EXCEL_EXPORT.ToLower());

                return usersClinicsRolese != null;
            }
        }

        [ScriptIgnore]
        public bool IsViewFinancialReports
        {
            get
            {
                if (IsClinicGroupManager)
                {
                    return true;
                }
                var usersClinicsRolese =
                    this.UsersClinicsRoleses.SingleOrDefault(
                        c =>
                        c.ClinicId == this.ActiveClinicId && c.UserId == this.Id && 
                        c.Role.RoleName.ToLower() == RolesConsts.VIEW_FINANCIAL_REPORTS.ToLower());// ||
                 //       c.Role.RoleName.ToLower() == RolesConsts.EXCEL_EXPORT.ToLower());

                return usersClinicsRolese != null;
            }
        }

        [ScriptIgnore]
        public bool IsExcelExport
        {
            get
            {
                if (IsClinicGroupManager)
                {
                    return true;
                }
                var usersClinicsRolese =
                    this.UsersClinicsRoleses.SingleOrDefault(
                        c =>
                        c.ClinicId == this.ActiveClinicId &&
                        c.Role.RoleName.ToLower() == RolesConsts.EXCEL_EXPORT.ToLower());

                return usersClinicsRolese != null;
            }
        }
        [ScriptIgnore]
        public bool IsNoDiary
        {
            get
            {
                return IsUserInRole(RolesConsts.NO_DIARY);
                //var NO_DIARY =
                //   this.UsersClinicsRoleses.SingleOrDefault(
                //       c =>
                //       c.ClinicId == this.ActiveClinicId && c.UserId == this.Id &&
                //       c.Role.RoleName.ToLower() == RolesConsts.NO_DIARY.ToLower());

                //return (NO_DIARY != null && NO_DIARY.ToString().ToLower() == "false");
            }
        }
        
        public int? DefaultClinicId { get; set; }

        public int? TitleId { get; set; }

        [ForeignKey("TitleId")]
        public Title Title { get; set; }

        public int? ShortCutAId { get; set; }

        public int? ShortCutBId { get; set; }

        public int? ShortCutCId { get; set; }

        [ForeignKey("ShortCutAId")]

        public ShortCut ShortCutA { get; set; }

        [ForeignKey("ShortCutBId")]
        public ShortCut ShortCutB { get; set; }

        [ForeignKey("ShortCutCId")]
        public ShortCut ShortCutC { get; set; }

        public int HomePageDisplay { get; set; }

        private ICollection<LastClient> _lastClients;
        public ICollection<LastClient> LastClients
        {
            get { return _lastClients ?? (_lastClients = new Collection<LastClient>()); }
            set { _lastClients = value; }
        }

        private ICollection<DailySchedule> _dailySchedules;
        public ICollection<DailySchedule> DailySchedules
        {
            get { return _dailySchedules ?? (_dailySchedules = new Collection<DailySchedule>()); }
            set { _dailySchedules = value; }
        }

        private ICollection<Recess> _recesses;
        public ICollection<Recess> Recesses
        {
            get { return _recesses ?? (_recesses = new Collection<Recess>()); }
            set { _recesses = value; }
        }

        private ICollection<CalenderEntry> _calenderEntries;
        public ICollection<CalenderEntry> CalenderEntries
        {
            get { return _calenderEntries ?? (_calenderEntries = new Collection<CalenderEntry>()); }
            set { _calenderEntries = value; }
        }

        private ICollection<BlockedDay> _blockedDays;
        public ICollection<BlockedDay> BlockedDays
        {
            get { return _blockedDays ?? (_blockedDays = new Collection<BlockedDay>()); }
            set { _blockedDays = value; }
        }


        private ICollection<WaitingListItem> _waitingListItems;
        public ICollection<WaitingListItem> WaitingListItems
        {
            get { return _waitingListItems ?? (_waitingListItems = new Collection<WaitingListItem>()); }
            set { _waitingListItems = value; }
        }

        private ICollection<UserPreventiveMedicineReminderFilter> _preventiveMedicineReminderFilters;
        public ICollection<UserPreventiveMedicineReminderFilter> PreventiveMedicineReminderFilters
        {
            get { return _preventiveMedicineReminderFilters ?? (_preventiveMedicineReminderFilters = new Collection<UserPreventiveMedicineReminderFilter>()); }
            set { _preventiveMedicineReminderFilters = value; }
        }

        private ICollection<ClinicTask> _clinicTasks;
        public ICollection<ClinicTask> ClinicTasks
        {
            get { return _clinicTasks ?? (_clinicTasks = new Collection<ClinicTask>()); }
            set { _clinicTasks = value; }
        }

        // public bool? ThermalPrinter { get; set; }

        private ICollection<InventoryOrder> _orders;
        public ICollection<InventoryOrder> Orders
        {
            get { return _orders ?? (_orders = new Collection<InventoryOrder>()); }
            set { _orders = value; }
        }

   //     public int? DefaultIssuerId { get; set; } //DO NOT USE
        public int? DefaultIssuerEmployerId { get; set; }

        [ForeignKey("DefaultIssuerEmployerId")]
        private Issuer Issuer { get; set; }

        private ICollection<FollowUp> _followUps;
        public ICollection<FollowUp> FollowUps
        {
            get { return _followUps ?? (_followUps = new Collection<FollowUp>()); }
            set { _followUps = value; }
        }

        public string OldDoctorId { get; set; }

        public bool Active { get; set; }

        public int? DefaultDrId { get; set; }

        [ScriptIgnore]
        [ForeignKey("DefaultDrId")]
        public User DefaultDr { get; set; }


        [NotMapped]
        public bool ShowOnMainWin { get; set; }

        [NotMapped]
        public int UsersDoctorsViewId { get; set; }

        [NotMapped]
        public string CurrentGoogleSyncEmail { get; set; }

        public Guid PasswordResetGuid { set; get; }
        public DateTime? LastPasswordResetRequest { get; set; }

        public bool LastAdvancedSearchByContaining { get; set; }
    }
}
