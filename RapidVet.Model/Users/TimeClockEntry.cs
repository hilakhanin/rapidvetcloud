﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace RapidVet.Model.Users
{
    /// <summary>
    /// a class that represents a user's work hours log
    /// </summary>
   public class TimeClockEntry
    {
        /// <summary>
        /// primary key in table
        /// </summary>
       [Key] 
       public int Id { get; set; }
    }
}
