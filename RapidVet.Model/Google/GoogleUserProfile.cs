﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Google
{
    public class GoogleUserProfile
    {
        public string email { get; set; }
        public string kid { get; set; }
    }
}
