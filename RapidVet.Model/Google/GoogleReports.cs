﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Google
{
    public class GoogleReports
    {
        [Key]
        public int Id { get; set; }

        public int GoogleTokenID { get; set; }

        public int DoctorID { get; set; }

        public string SyncGuid { get; set; }

        public string GoogleCalendar { get; set; }

        public DateTime SyncStarted { get; set; }

        public DateTime SyncEnded { get; set; }

        public int ContactsUpdatedInGoogle { get; set; }

        public int ContactsCreatedInGoogle { get; set; }

        public int EventsUpdatedInGoogle { get; set; }

        public int EventsUpdatedInRapid { get; set; }

        public int EventsDeletedInGoogle { get; set; }

        public int EventsDeletedInRapid { get; set; }

        public int EventsCreatedInGoogle { get; set; }

        public int EventsCreatedInRapid { get; set; }

        public string ErrorMsg { get; set; }
    }
}
