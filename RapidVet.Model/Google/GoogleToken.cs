﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Google
{
    public class GoogleToken : ChangeTracker
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        public string EMail { get; set; }

        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        public DateTime AccessTokenIssueDateUtc { get; set; }

        public DateTime AccessTokenExpirationUtc { get; set; }
    }
}
