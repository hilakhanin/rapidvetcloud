﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Google
{
    public class ClientGoogleIDs : ChangeTracker
    {
        [Key]
        public int Id { get; set; }

        public int ClientID { get; set; }

        public string GoogleID { get; set; }

        public string GoogleEmail { get; set; }
    }
}
