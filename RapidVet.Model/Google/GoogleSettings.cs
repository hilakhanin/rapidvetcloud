﻿using RapidVet.Model.Clinics;
using RapidVet.WebModels.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Google
{
    public class GoogleSettings : ChangeTracker
    {
        [Key]
        public int Id { get; set; }

        public int GoogleTokenId { get; set; }

        public int ClinicGroupID { get; set; }

        public int ClinicID { get; set; }

        public int DoctorID { get; set; }

        [Required(ErrorMessage="Must provide calendar name in google")]
        public string GoogleCalName { get; set; }

        public bool AllowSync { get; set; }

        public bool AllowSyncCalendar { get; set; }

        public int SyncCalBackDays { get; set; }

        public int SyncCalForwardDays { get; set; }

        public bool SyncCalFromGoogle { get; set; }

        public bool SyncCalToGoogle { get; set; }

        public int SyncPeriod { get; set; }

        public bool AllowSyncContacts { get; set; }

        public List<UsersSettingsShowUserModel> UsersList { get; set; }

        public DateTime? NextSync { get; set; }

        public string GoogleCalendarID { get; set; }
    }
}
