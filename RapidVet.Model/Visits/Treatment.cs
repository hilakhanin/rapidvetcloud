﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.Visits
{
    /// <summary>
    /// a class that represents treatment in visit
    /// </summary>
    public class Treatment
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        private ICollection<Visit> _visits;
        public virtual ICollection<Visit> Visits
        {
            get { return _visits ?? (_visits = new Collection<Visit>()); }
            set { _visits = value; }
        }

        public bool Active { get; set; }
    }
}
