﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace RapidVet.Model.Visits
{
    /// <summary>
    /// a class that represents an examination in visit
    /// </summary>
    public class Examination
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }
    
    }
}
