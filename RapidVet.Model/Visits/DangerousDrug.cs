﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Visits
{
    public class DangerousDrug
    {
        [Key]
        public int Id { get; set; }

        public int MedicationId { get; set; }

        [ForeignKey("MedicationId")]
        public Medication Medication { get; set; }

        public int VisitId { get; set; }

        [ForeignKey("VisitId")]
        public Visit Visit { get; set; }

        public decimal AmountInjected { get; set; }

        public decimal AmountDestroyed { get; set; }
    }
}
