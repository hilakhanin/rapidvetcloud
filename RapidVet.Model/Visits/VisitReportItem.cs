﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RapidVet.Model.Clients.Details;

namespace RapidVet.Model.Visits
{
    public class VisitReportItem
    {
        public string nameVisit { get; set; }
        public DateTime datePerformed { get; set; }
        public string clientFirstName { get; set; }
        public string clientLastName { get; set; }
        public string clientName { get; set; }
        public string patientName { get; set; }
        public string serial { get; set; }
        public string address { get; set; }
        public string homePhone { get; set; }
        public string cellPhone { get; set; }
        public string animalKind { get; set; }
        public string animalRace { get; set; }
        public string animalGender { get; set; }
        public string animalColor { get; set; }
        public string animalAge { get; set; }
        public string sterilization { get; set; }
        public string electronicNum { get; set; }
        public DateTime? birthDate { get; set; }
        public string licenseNum { get; set; }
        public decimal? chargeFee { get; set; }
        public string performingDoc { get; set; }
        public decimal? numOfUnits { get; set; }
        public string plateNum { get; set; }
        public string visitDesc { get; set; }
        public string treatingDoctor { get; set; }
        public int? receipt { get; set; }
        public int clientId { get; set; }
        public int patientId { get; set; }
        public int visitId { get; set; }

        public string clientReferer { get; set; }
        public decimal? clientBalance { get; set; }
        public string clientAge { get; set; }
        public string clientBirthdate { get; set; }
        public string clientTitle { get; set; }
        public string clientCity { get; set; }
        public string clientZip { get; set; }
        public string clientStreetAndNumber { get; set; }
        public int clientRecordNumber { get; set; }
    }

    public class VisitReportGrouped
    {
        public List<VisitReportItem> Items { get; set; }

        public decimal Sum { get; set; }

        public string Name { get; set; }
    }
}
