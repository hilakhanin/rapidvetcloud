﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.Visits
{
    public class VisitFilter
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        //Client
        public int? ClientId { get; set; }
        
        //Animal Kind
        public int? AnimalKindId { get; set; }

        //DateRange
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        //Animal Age Range
        public int? FromYear { get; set; }
        public int? FromMonth { get; set; }
        public int? ToYear { get; set; }
        public int? ToMonth { get; set; }

        //Visit collections Ids
        public string CategoryId { get; set; }
        public string TreatmentId { get; set; }
        public int? DiagnosisId { get; set; }
        public int? ExaminationId { get; set; }

        //Doctor
        public string DoctorId { get; set; }

        //Sum Range of Visit
        public decimal? FromSum { get; set; }
        public decimal? ToSum { get; set; }

        //Visit Descriptions and Complaint Keymatch
        public string VisitDescription { get; set; } //refers to same as main complaint
        public string MainComplaint { get; set; }
    }
}
