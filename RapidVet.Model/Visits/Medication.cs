﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Enums;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.Visits
{
    /// <summary>
    /// a class that represents medication treatment in visit
    /// </summary>
    public class Medication
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The Clinic group Id
        /// </summary>
        public int ClinicGroupId { get; set; }

        /// <summary>
        /// Clinic Group Object
        /// </summary>
        [ForeignKey("ClinicGroupId")]
        public ClinicGroup ClinicGroup { get; set; }

        /// <summary>
        /// Medication Name (שם)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Medication Administration Type Id (תצורה)
        /// </summary>
        public int MedicationAdministrationTypeId { get; set; }

        /// <summary>
        /// Medication Administration Type
        /// </summary>
        [NotMapped]
        public MedicationAdministrationType MedicationAdministrationType
        {
            get
            {
                return (MedicationAdministrationType)MedicationAdministrationTypeId;
            }

            set
            {
                this.MedicationAdministrationTypeId = (int)value;
            }
        }

        /// <summary>
        /// The Dosage of the medication (מנה)
        /// </summary>
        public string Dosage { get; set; }

        /// <summary>
        /// Times of usage every day (מספר פעמים ביום)
        /// </summary>
        public int? TimesPerDay { get; set; }

        /// <summary>
        /// Numbers of dayes (זמן לקיחה בימים)
        /// </summary>
        public int? NumberOfDays { get; set; }

        /// <summary>
        /// usage instructions (הוראות שימוש)
        /// </summary>
        public string Insturctions { get; set; }

        /// <summary>
        /// Price per Unit (מחיר יחידה)
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Is this a dangerous drug (סם מסוכן)
        /// </summary>
        public bool DangerousDrug { get; set; }

        /// <summary>
        /// Doctor Information (מידע לרופא)
        /// </summary>
        public string DoctorInfo { get; set; }


        /// <summary>
        /// Is this Record active (false for deleted recoeds)
        /// </summary>
        public bool Active { get; set; }

        private ICollection<VisitMedication> _visitMedications;
        public ICollection<VisitMedication> VisitMedications
        {
            get { return _visitMedications ?? (_visitMedications = new Collection<VisitMedication>()); }
            set { _visitMedications = value; }
        }

        public int? OldId { get; set; }

        public DateTime? Updated { get; set; }



    }
}
