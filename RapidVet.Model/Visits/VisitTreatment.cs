﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Finance;
using RapidVet.Model.Quotations;

namespace RapidVet.Model.Visits
{
    public class VisitTreatment
    {
        public int Id { get; set; }

        public int VisitId { get; set; }

        [ForeignKey("VisitId")]
        public Visit Visit { get; set; }

        public int? PriceListItemId { get; set; }

        [ForeignKey("PriceListItemId")]
        public PriceListItem PriceListItem { get; set; }

        public string Name { get; set; }

        public string CategoryName { get; set; }

        public decimal Quantity { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal Discount { get; set; }

        public decimal DiscountPercent { get; set; }

        public decimal Price { get; set; }

        public decimal DiscountPerUnit
        {
            get
            {
                if (Quantity != 0)
                {
                    return Discount/Quantity;
                }
                return 0;
            }
        }



        public bool Invoiced { get; set; }

        public int? QuotationTreatmentId { get; set; }

        [ForeignKey("QuotationTreatmentId")]
        public QuotationTreatment QuotationTreatment { get; set; }

        public string AddedBy { get; set; }

        public int AddedByUserId { get; set; }
    }
}
