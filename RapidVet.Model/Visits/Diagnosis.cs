﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;

namespace RapidVet.Model.Visits
{
    /// <summary>
    /// a class that represents a diagnosis given in visit
    /// </summary>
    public class Diagnosis
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        private ICollection<VisitDiagnosis> _visits;
        public virtual ICollection<VisitDiagnosis> Visits
        {
            get { return _visits ?? (_visits = new Collection<VisitDiagnosis>()); }
            set { _visits = value; }
        }

        private ICollection<MedicalProcedure> _medicalProcedures;
        public virtual ICollection<MedicalProcedure> MedicalProcedures
        {
            get { return _medicalProcedures ?? (_medicalProcedures = new Collection<MedicalProcedure>()); }
            set { _medicalProcedures = value; }
        }

        public bool Active { get; set; }

        public int? OldId { get; set; }
    }
}
