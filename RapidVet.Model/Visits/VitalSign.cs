﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Patients;

namespace RapidVet.Model.Visits
{
    /// <summary>
    /// measurment type
    /// </summary>
    public enum VitalSignType
    {
        Temp = 1, Pulse = 2, Weight = 3, BCS = 4, Respiration = 5
    }

    /// <summary>
    /// a class that represents a single measurement of one of the vital signs in the above enum
    /// </summary>
    public class VitalSign
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// representing the visit for  this VitalSign object for db & code-first purposes
        /// </summary>
        //public int VisitId { get; set; }

        ///// <summary>
        ///// Visit object attached to this VitalSign object
        ///// </summary>
        //[ForeignKey("VisitId")]
        //public virtual Visit Visit { get; set; }

        /// <summary>
        /// representing the patient for  this VitalSign object for db & code-first purposes
        /// </summary>
        public int PatientId { get; set; }

        /// <summary>
        /// patient object attached to this VitalSign object
        /// </summary>
        [ForeignKey("PatientId")]
        public virtual Patient Patient { get; set; }


        /// <summary>
        /// representaion of VitalSignType enum in table
        /// </summary>
        public int VitalSignTypeId { get; set; }


        /// <summary>
        ///t reffers to "VitalSignTypeId" for data storing ans extracting purposes
        /// </summary>
        [NotMapped]
        public VitalSignType VitalSignType
        {
            get
            {
                return (VitalSignType)VitalSignTypeId;
            }
            set
            {
                VitalSignTypeId = (int)value;
            }

        }

        /// <summary>
        /// measurment time
        /// </summary>
        public DateTime Time { get; set; }

       /// <summary>
        /// measurment result
       /// </summary>
        public decimal Value { get; set; }

        public int? VisitId { get; set; }

        /// <summary>
        /// patient object attached to this VitalSign object
        /// </summary>
        [ForeignKey("VisitId")]
        public virtual Visit Visit { get; set; }
    }
}
