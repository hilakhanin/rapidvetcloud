﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Quotations;

namespace RapidVet.Model.Visits
{
    public class VisitMedication
    {
        [Key]
        public int Id { get; set; }

        public int VisitId { get; set; }

        [ForeignKey("VisitId")]
        public Visit Visit { get; set; }

        public int? MedicationId { get; set; }

        [ForeignKey("MedicationId")]
        public Medication Medication { get; set; }

        public string Name { get; set; }

        public int DrugAdministrationId { get; set; }

        public string Dosage { get; set; }

        public int? TimesPerDay { get; set; }

        public int? NumberOfDays { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal Total { get; set; }

        public string Instructions { get; set; }

        public decimal OverallAmount { get; set; }

        public decimal Discount { get; set; }

        public decimal DiscountPercent { get; set; }

        public decimal DiscountPerUnit {get
        {
            if (OverallAmount != 0)
            {
            //    var discount = Total - OverallAmount*UnitPrice;
                return Discount/OverallAmount;
            }
            return 0;
        }
        }

        public bool Invoiced { get; set; }

        public int? QuotationTreatmentId { get; set; }

        [ForeignKey("QuotationTreatmentId")]
        public QuotationTreatment QuotationTreatment { get; set; }

        public string AddedBy { get; set; }

        public int AddedByUserId { get; set; }
    }
}
