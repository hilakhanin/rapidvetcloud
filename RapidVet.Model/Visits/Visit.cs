﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Clients;
using RapidVet.Model.Finance;
using RapidVet.Model.PatientLabTests;
using RapidVet.Model.Patients;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Users;

namespace RapidVet.Model.Visits
{
    /// <summary>
    /// a class that represents a doctor - patient session in a Clinic
    /// </summary>
    public class Visit : ChangeTracker
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// representing vital signs primary key in table for delete and edit
        /// </summary>
        [NotMapped]
        public int ManualId { get; set; }

        /// <summary>
        /// representing the Patient for  this visit object for db & code-first purposes
        /// <summary>
        public Nullable<int> PatientId { get; set; }

        /// <summary>
        /// the patient this visit object is attached to
        /// </summary>
        [ForeignKey("PatientId")]
        public virtual Patient Patient { get; set; }

        /// <summary>
        /// date of the visit
        /// </summary>
        public DateTime VisitDate { get; set; }

        /// <summary>
        /// main complaint
        /// </summary>
        public string MainComplaint { get; set; }

        public int DoctorId { get; set; }

        /// <summary>
        /// the doctor this visit treated by
        /// </summary>
        [ForeignKey("DoctorId")]
        public User Doctor { get; set; }

        public decimal Weight { get; set; }

        public decimal Temp { get; set; }

        public int BPM { get; set; }

        public int BCS { get; set; }

        public int Pulse { get; set; }

        public string Mucosa { get; set; }

        public string Color { get; set; }

        public decimal? Price { get; set; }

        /// <summary>
        /// collection of examination objects attached to this visit object
        /// </summary>
        private ICollection<VisitExamination> _examinations;
        public ICollection<VisitExamination> Examinations
        {
            get { return _examinations ?? (_examinations = new Collection<VisitExamination>()); }
            set { _examinations = value; }
        }

        /// <summary>
        /// collection of diagnosis objects attached to this visit object
        /// </summary>
        private ICollection<VisitDiagnosis> _diagnoses;

        public ICollection<VisitDiagnosis> Diagnoses
        {
            get { return _diagnoses ?? (_diagnoses = new Collection<VisitDiagnosis>()); }
            set { _diagnoses = value; }
        }

        /// <summary>
        /// collection of treatment objects attached to this visit object
        /// </summary>
        private ICollection<VisitTreatment> _treatments;
        public ICollection<VisitTreatment> Treatments
        {
            get { return _treatments ?? (_treatments = new Collection<VisitTreatment>()); }
            set { _treatments = value; }
        }

        /// <summary>
        /// collection of medication objects attached to this visit object
        /// </summary>
        private ICollection<VisitMedication> _medications;
        public ICollection<VisitMedication> Medications
        {
            get { return _medications ?? (_medications = new Collection<VisitMedication>()); }
            set { _medications = value; }
        }

        private ICollection<PreventiveMedicineItem> _preventiveMedicineItems;
        public ICollection<PreventiveMedicineItem> PreventiveMedicineItems
        {
            get { return _preventiveMedicineItems ?? (_preventiveMedicineItems = new Collection<PreventiveMedicineItem>()); }
            set { _preventiveMedicineItems = value; }
        }

        public bool Close { get; set; }

        private ICollection<DangerousDrug> _dangerousDrugs;
        public ICollection<DangerousDrug> DangerousDrugs
        {
            get { return _dangerousDrugs ?? (_dangerousDrugs = new Collection<DangerousDrug>()); }
            set { _dangerousDrugs = value; }
        }

        //billing
        public int? ReciptId { get; set; }
        [ForeignKey("ReciptId")]
        public FinanceDocument Recipt { get; set; }

        //billing collection in case of more than one
        private ICollection<FinanceDocument> _recipts;

        [NotMapped]
        public ICollection<FinanceDocument> Recipts
        {
            get { return _recipts ?? (_recipts = new Collection<FinanceDocument>()); }
            set { _recipts = value; }
        }

        //DangerousDrug

        /// <summary>
        /// collection of lab objects attached to this visit object
        /// </summary>
        //[Display(ResourceType = typeof(RapidVet.Resources.Visit), Name = "Labs")]
        //public ICollection<PatientLabTest> Labs { get; set; }

        ///// <summary>
        ///// collection of vitalSign objects attached to this visit object
        ///// </summary>
        //[Display(ResourceType = typeof(RapidVet.Resources.Visit), Name = "VitalSigns")]
        //public ICollection<VitalSign> VitalSigns { get; set; }

        public long OldId { get; set; }

        public int ClientIdAtTimeOfVisit { get; set; }

        [ForeignKey("ClientIdAtTimeOfVisit")]
        public Client ClientAtTimeOfVisit { get; set; }

        public bool Active { get; set; }

        public int? OldRapidvetTreatmentId { get; set; }

        public bool? DebtVisit { get; set; }

        /// <summary>
        /// collection of vitalsign objects attached to this visit object
        /// </summary>
        private ICollection<VitalSign> _vitalSigns;
        public ICollection<VitalSign> VitalSigns
        {
            get { return _vitalSigns ?? (_vitalSigns = new Collection<VitalSign>()); }
            set { _vitalSigns = value; }
        }

        public string VisitNote { get; set; }
    }
}
