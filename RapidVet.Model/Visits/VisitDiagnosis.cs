﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Visits
{
    public class VisitDiagnosis
    {
        public int Id { get; set; }

        public int VisitId { get; set; }

        [ForeignKey("VisitId")]
        public Visit Visit { get; set; }

        public int? DiagnosisId { get; set; }

        [ForeignKey("DiagnosisId")]
        public Diagnosis Diagnosis { get; set; }

        public string Name { get; set; }

        public string CategoryName { get; set; }

        public string AddedBy { get; set; }

        public int AddedByUserId { get; set; }
    }
}
