﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Users;

namespace RapidVet.Model.FullCalender
{
    public class Recess
    {
        [Key]
        public int Id { get; set; }

        public int DayId { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public int ClinicId { get; set; }
    }
}
