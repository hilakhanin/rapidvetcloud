﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Resources;

namespace RapidVet.Model.FullCalender
{
    public class Holiday
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public bool HasHolidayEve { get; set; }

        public DateTime StartDay { get; set; }

        public DateTime EndDay { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        [NotMapped]
        public string StartDayString { get; set; }

        [NotMapped]
        public string EndDayString { get; set; }
    }
}
