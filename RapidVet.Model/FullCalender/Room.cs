﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Resources;

namespace RapidVet.Model.FullCalender
{
    public class Room
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public bool Active { get; set; }

        private ICollection<CalenderEntry> _calenderEntries;
        public ICollection<CalenderEntry> CalenderEntries
        {
            get { return _calenderEntries ?? (_calenderEntries = new Collection<CalenderEntry>()); }
            set { _calenderEntries = value; }
        }

    }
}
