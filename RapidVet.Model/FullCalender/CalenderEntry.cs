﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clients;
using RapidVet.Model.Clinics;
using RapidVet.Model.Patients;
using RapidVet.Model.Users;

namespace RapidVet.Model.FullCalender
{
    public class CalenderEntry
    {
        [Key]
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        // In Minutes
        public int Duration { get; set; }

        [ForeignKey("RoomId")]
        public Room Room { get; set; }

        public int? RoomId { get; set; }

        public int CreatedByUserId { get; set; }

        public int DoctorId { get; set; }

        [ForeignKey("DoctorId")]
        public User Doctor { get; set; }

        public int? ClientId { get; set; }

        [ForeignKey("ClientId")]
        public Client Client { get; set; }

        public int? PatientId { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

        public string Comments { get; set; }

        public string Color { get; set; }

        public string Title { get; set; }

        public bool? IsNoShow { get; set; }

        public bool IsNewClient { get; set; }

        public bool IsCancelled { get; set; }

        public string GoogleId { get; set; }

        public int? OldId { get; set; }

        public DateTime? Updated { get; set; }
        
        public bool IsGeneralEntry { get; set; }
    }
}
