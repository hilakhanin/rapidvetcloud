﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using RapidVet.Enums.DataMigration;

namespace RapidVet.Model
{
    public class DataMigrationLog
    {
        [Key]
        public int Id { get; set; }

        public int? ClinicGroupId { get; set; }

        public int? ClinicId { get; set; }

        public int LastSuccessfullStageId { get; set; }

        public MigrationOrder LastSuccessfullStage
        {
            get { return (MigrationOrder)LastSuccessfullStageId; }
            set { LastSuccessfullStageId = (int)value; }
        }

        public string LastSuccessfullStageName { get; set; }

        public DateTime DateTime { get; set; }

        public bool MigrationFinished { get; set; }

        public string Exception { get; set; }
    }
}
