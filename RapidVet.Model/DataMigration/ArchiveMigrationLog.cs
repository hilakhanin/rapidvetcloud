﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.DataMigration
{
    public class ArchiveMigrationLog
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        public int? ArchiveDocumentId { get; set; }

        public string LocalFilePath { get; set; }

        public DateTime DateTime { get; set; }
    }
}
