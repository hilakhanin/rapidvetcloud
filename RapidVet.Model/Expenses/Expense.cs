﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clinics;
using RapidVet.Enums.Finances;

namespace RapidVet.Model.Expenses
{
    public class Expense
    {
        [Key]
        public int Id { get; set; }

        public int ExpenseGroupId { get; set; }
        [ForeignKey("ExpenseGroupId")]
        public ExpenseGroup ExpenseGroup { get; set; }
        public int SupplierId { get; set; }
        [ForeignKey("SupplierId")]
        public Supplier Supplier { get; set; }
        public int IssuerId { get; set; }
        [ForeignKey("IssuerId")]
        public Issuer Issuer { get; set; }

        public DateTime InvoiceDate { get; set; }
        public DateTime EnteredDate { get; set; }
        public string Description { get; set; }

        public string InvoiceNumber { get; set; }
        public string ReciptNumber { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal RecognitionPercent { get; set; }
        public decimal RecognitionTotalAmountIncludingVat { get; set; }
        public decimal Vat { get; set; }

        public DateTime? PaymentDate { get; set; }
        public int? PaymentTypeId { get; set; }
      
        public string Comments { get; set; }

        public DateTime? Updated { get; set; }


    }
}
