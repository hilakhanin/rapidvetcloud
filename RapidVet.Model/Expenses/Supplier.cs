﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clinics;
using RapidVet.Model.CliinicInventory;

namespace RapidVet.Model.Expenses
{
    public class Supplier
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public string Name { get; set; }

        private ICollection<Expense> _expenses;
        public ICollection<Expense> Expenses
        {
            get { return _expenses ?? (_expenses = new Collection<Expense>()); }
            set { _expenses = value; }
        }

        public string ExternalId { get; set; }

        public DateTime? Updated { get; set; }

        public string CompanyId { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string ContactNameA { get; set; }

        public string ContactPhoneA { get; set; }

        public string ContactCommentA { get; set; }

        public string ContactNameB { get; set; }

        public string ContactPhoneB { get; set; }

        public string ContactCommentB { get; set; }

        private ICollection<PriceListItemSupplier> _priceListItems;
        public ICollection<PriceListItemSupplier> PriceListItems
        {
            get { return _priceListItems ?? (_priceListItems = new Collection<PriceListItemSupplier>()); }
            set { _priceListItems = value; }
        }

        public int? OldId { get; set; }
    }
}
