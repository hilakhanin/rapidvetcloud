﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.Expenses
{
    public class ExpenseGroup
    {
        [Key]
        public int Id { get; set; }

        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public string Name { get; set; }

        public bool IsTextDeductible { get; set; }

        public decimal RecognitionPercent { get; set; }

        public string ExternalId { get; set; }

        private ICollection<Expense> _expenses;
        public ICollection<Expense> Expenses
        {
            get { return _expenses ?? (_expenses = new Collection<Expense>()); }
            set { _expenses = value; }
        }

        public DateTime? Updated { get; set; }
    }
}
