﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using RapidVet.Model.Users;

namespace RapidVet.Model.Patients
{
    /// <summary>
    /// a class that represents a patient comment, inherits from 'CommentBase'
    /// </summary>
    public class PatientComment : Comment
    {
        /// <summary>
        /// representing the patient id for the comment. for db & code-first purposes
        /// </summary>
        public int PatientId { get; set; }

        ///the patient that this comment is attached to 
        [ForeignKey("PatientId")]
        [ScriptIgnore]
        public Patient Patient { get; set; }

        [ForeignKey("CreatedById")]
        public virtual User CreatedBy { get; set; }

        public bool Active { get; set; }
    }
}
