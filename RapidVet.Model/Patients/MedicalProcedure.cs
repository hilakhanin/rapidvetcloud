﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Enums;
using RapidVet.Model.Visits;

namespace RapidVet.Model.Patients
{
    public class MedicalProcedure
    {
        [Key]
        public int Id { get; set; }

        public int PatientId { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

        public int DisagnosisId { get; set; }

        [ForeignKey("DisagnosisId")]
        public Diagnosis Diagnosis { get; set; }

        public DateTime Start { get; set; }

        public DateTime? Finish { get; set; }

        public string ProcedureName { get; set; }

        public int ProcedureTypeId { get; set; }

        [NotMapped]
        public MedicalProcedureType ProcedureType
        {
            get { return (MedicalProcedureType)ProcedureTypeId; }
            set { ProcedureTypeId = (int)value; }
        }

        public string ProcedureLocation { get; set; }

        public string SurgeonName { get; set; }

        public string AnesthesiologistName { get; set; }

        public string NurseName { get; set; }

        public string AdditionalPersonnel { get; set; }

        public string Summery { get; set; }

        public string Letter { get; set; }

        public DateTime? Updated { get; set; }

    }
}
