﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.Patients
{
    /// <summary>
    /// a class that represents  animal race
    /// </summary>
    public class AnimalRace
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        [Display(ResourceType = typeof(RapidVet.Resources.Global) , Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// representing the animal kind for  this AnimalRace object for db & code-first purposes
        /// <summary>
        public int AnimalKindId { get; set; }

        /// <summary>
        /// representing the AnimalKind for  this AnimalRace object for db & code-first purposes
        /// <summary>
        [ForeignKey("AnimalKindId")]
        public AnimalKind AnimalKind { get; set; }

        /// <summary>
        /// animal race value
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(RapidVet.Resources.GlobalValidationErrors), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(RapidVet.Resources.Controllers.AnimalRaceController), Name = "Value")]
        public string Value { get; set; }


        public bool Active { get; set; }
    }
}
