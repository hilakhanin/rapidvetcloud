﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Patients
{
    public class DischargePaper
    {
        [Key]
        public int Id { get; set; }

        public int PatientId { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string Summery { get; set; }

        public string Background { get; set; }

        public string Recommendations { get; set; }

        public DateTime? CheckUp { get; set; }

        public DateTime DateTime { get; set; }

        public string Letter { get; set; }

        public DateTime? Updated { get; set; }

    }
}
