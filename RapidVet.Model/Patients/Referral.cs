﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Users;

namespace RapidVet.Model.Patients
{
    public class Referral
    {
        [Key]
        public int Id { get; set; }

        public int PatientId { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

        public DateTime DateTime { get; set; }

        public string Reason { get; set; }

        public int RefferingUserId { get; set; }

        [ForeignKey("RefferingUserId")]
        public User RefferingUser { get; set; }

        public int? RefferedToUserId { get; set; }

        [ForeignKey("RefferedToUserId")]
        public User RefferedToUser { get; set; }

        public string RefferedToExternal { get; set; }

        public string RefferingClinicAddress { get; set; }

        public string RefferingClinicPhone { get; set; }

        public DateTime? Updated { get; set; }

        public int? OldId { get; set; }
    }
}
