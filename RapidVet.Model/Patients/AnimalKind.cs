﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Clinics;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Users;

namespace RapidVet.Model.Patients
{
    /// <summary>
    /// a class that represents animal kind ( cat, dog..)
    /// </summary>
    public class AnimalKind
    {
        public AnimalKind()
        {
            //   AnimalRaces = new LinkedList<AnimalRace>();
        }
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        public string Value { get; set; }

        /// <summary>
        /// representing the Clinic group id for the animal kind for db & code-first purposes
        /// </summary>
        public int ClinicGroupId { get; set; }


        //the Clinic group that this animalKind is attached to
        [ForeignKey("ClinicGroupId")]
        virtual public ClinicGroup ClinicGroup { get; set; }

        private ICollection<AnimalRace> _races;

        public ICollection<AnimalRace> AnimalRaces {
            get { return _races ?? (_races = new Collection<AnimalRace>()); }
            set { _races = value; }
        }

        public int? AnimalIconId { get; set; }

        [ForeignKey("AnimalIconId")]
        public AnimalIcon AnimalIcon { get; set; }

        public bool Active { get; set; }

        private ICollection<VaccineOrTreatment> _vaccinesAndTreatments;

        public ICollection<VaccineOrTreatment> VaccineOrTreatments
        {
            get { return _vaccinesAndTreatments ?? (_vaccinesAndTreatments = new Collection<VaccineOrTreatment>()); }
            set { _vaccinesAndTreatments = value; }
        }

        private ICollection<LabTestTemplate> _labTestForAnimals;
        public ICollection<LabTestTemplate> LabTestForAnimals
        {
            get { return _labTestForAnimals ?? (_labTestForAnimals = new Collection<LabTestTemplate>()); }
            set { _labTestForAnimals = value; }
        }

        private ICollection<UserPreventiveMedicineReminderFilter> _preventiveMedicineReminderFilters;
        public ICollection<UserPreventiveMedicineReminderFilter> PreventiveMedicineReminderFilters
        {
            get { return _preventiveMedicineReminderFilters ?? (_preventiveMedicineReminderFilters = new Collection<UserPreventiveMedicineReminderFilter>()); }
            set { _preventiveMedicineReminderFilters = value; }
        }

        public int? OldId { get; set; }
    }
}
