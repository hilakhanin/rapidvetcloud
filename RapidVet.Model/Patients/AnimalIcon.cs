﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Patients
{
    public class AnimalIcon
    {
        [Key]
        public int Id { get; set; }

        public string FileName { get; set; }

        private ICollection<AnimalKind> _animalKinds;
        public ICollection<AnimalKind> AnimalKinds
        {
            get { return _animalKinds ?? (_animalKinds = new Collection<AnimalKind>()); }
            set { _animalKinds = value; }
        }
    }
}
