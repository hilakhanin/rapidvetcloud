﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Clinics;

namespace RapidVet.Model.Patients
{
    /// <summary>
    /// a class that represents animal color
    /// </summary>
    public class AnimalColor
    {
        /// <summary>
        /// primary key in table
        /// </summary>
        [Key]
        public int Id { get; set; }

        public int ClinicGroupId { get; set; }

        [ForeignKey("ClinicGroupId")]
        public ClinicGroup ClinicGroup { get; set; }

        /// <summary>
        /// color value
        /// </summary>
        public string Value { get; set; }

        private ICollection<Patient> _patients;

        public ICollection<Patient> Patients
        {
            get { return _patients ?? (_patients = new Collection<Patient>()); }
            set { _patients = value; }
        }

        public bool Active { get; set; }

    }
}
