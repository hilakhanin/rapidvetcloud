﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Enums;

namespace RapidVet.Model.Patients
{
    public class MinistryOfAgricultureReport
    {
        [Key]
        public int Id { get; set; }

        public int PatientId { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

        public int ReportTypeId { get; set; }

        [NotMapped]
        public MinistryOfAgricultureReportType ReportType
        {
            get { return (MinistryOfAgricultureReportType)ReportTypeId; }
            set { ReportTypeId = (int)value; }
        }

        public DateTime DateTime { get; set; }

        public string Comments { get; set; }

        public string Report { get; set; }

        public int? VisitId { get; set; }
    }
}
