﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Enums;

namespace RapidVet.Model.Patients
{
    public class Letter
    {
        [Key]
        public int Id { get; set; }

        public int PatientId { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

        public int LetterTemplateTypeId { get; set; }
        public LetterTemplateType LetterTemplateType
        {
            get { return (LetterTemplateType)LetterTemplateTypeId; }
            set { LetterTemplateTypeId = (int)value; }
        }

        public DateTime CreatedDate { get; set; }

        public string Content { get; set; }

        public bool ShowInPatientHistory { get; set; }
    }
}
