﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RapidVet.Model.Archives;
using RapidVet.Model.Clients;
using RapidVet.Model.Finance;
using RapidVet.Model.FullCalender;
using RapidVet.Model.PatientFollowUp;
using RapidVet.Model.PatientLabTests;
using RapidVet.Model.PreventiveMedicine;
using RapidVet.Model.Quotations;
using RapidVet.Model.Visits;

namespace RapidVet.Model.Patients
{
    public enum GenderEnum
    {
        Male = 1, Female = 2
    }
    /// <summary>
    /// a class that represents a patient (pet, animal)
    /// </summary>
    public class Patient : ChangeTracker
    {
        /// <summary>
        /// represents gender options
        /// </summary>
        [Key]
        public int Id { get; set; }

        public virtual int LocalId { get; set; }

        public int ClientId { get; set; }

        public Client Client { get; set; }

        /// <summary>
        /// patient name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// representing the animal race for  this patient object for db & code-first purposes
        /// <summary>
        public int? AnimalRaceId { get; set; }

        /// <summary>
        /// the race  this patient object is attached to
        /// </summary>
        [ForeignKey("AnimalRaceId")]
        public virtual AnimalRace AnimalRace { get; set; }

        /// <summary>
        /// representing the animal color for  this patient object for db & code-first purposes
        /// <summary>
        public int? AnimalColorId { get; set; }

        /// <summary>
        /// the color this patient object is attached to
        /// </summary>
        [ForeignKey("AnimalColorId")]
        public AnimalColor AnimalColor { get; set; }

        /// <summary>
        /// patient birth date
        /// </summary>
        public DateTime? BirthDate { get; set; }

        public decimal? Age
        {
            
            get {
                if (BirthDate == null)
                {
                    return null;
                }
                var span = DateTime.Now - BirthDate.Value;
                return (span.Days / (decimal?)365);
            }
        }

        public int GenderId { get; set; }


        /// <summary>
        /// is the patient pure bread
        /// </summary>
        public bool PureBred { get; set; }

        /// <summary>
        /// has the patient been through fertilization
        /// </summary>
        public bool Fertilization { get; set; }

        /// <summary>
        /// has the patient been through sterilization
        /// </summary>
        public bool Sterilization { get; set; }

        /// <summary>
        /// sterilization date
        /// </summary>
        public DateTime? SterilizationDate { get; set; }

        /// <summary>
        /// patient electronic number
        /// </summary>
        public string ElectronicNumber { get; set; }

        /// <summary>
        /// patient license number
        /// </summary>
        public string LicenseNumber { get; set; }

        /// <summary>
        /// owner / farm, for farm animals mostly
        /// </summary>
        public string Owner_Farm { get; set; }

        /// <summary>
        /// number in the Israeli Breeding Book - ספר הגידול הישראלי - for purebreads only
        /// </summary>
        public string SAGIRNumber { get; set; }

        /// <summary>
        /// is the patient dangerous to humans
        /// </summary>
        public bool HumanDangerous { get; set; }

        /// <summary>
        /// is the patient dangerous to other animals
        /// </summary>
        public bool AnimalDangerous { get; set; }

        /// <summary>
        /// patient blood type
        /// </summary>
        public string BloodType { get; set; }

        /// <summary>
        /// patient allergies
        /// </summary>
        public string Allergies { get; set; }

        /// <summary>
        /// food amoun consumed by patient
        /// </summary>
        public string FoodAmount { get; set; }

        private ICollection<PatientComment> _comments;

        public ICollection<PatientComment> Comments
        {
            get { return _comments ?? (_comments = new Collection<PatientComment>()); }
            set { _comments = value; }
        }
        public bool Active { get; set; }


        public int? FoodTypeId { get; set; }

        [ForeignKey("FoodTypeId")]
        public PriceListItem FoodType { get; set; }

        private ICollection<Visit> _visits;
        public ICollection<Visit> Visits
        {
            get { return _visits ?? (_visits = new Collection<Visit>()); }
            set { _visits = value; }
        }

        private ICollection<Quotation> _quotations;
        public ICollection<Quotation> Quotations
        {
            get { return _quotations ?? (_quotations = new Collection<Quotation>()); }
            set { _quotations = value; }
        }

        private ICollection<ArchiveDocument> _archives;
        public ICollection<ArchiveDocument> Archives
        {
            get { return _archives ?? (_archives = new Collection<ArchiveDocument>()); }
            set { _archives = value; }
        }

        private ICollection<PatientLabTest> _labTests;
        public ICollection<PatientLabTest> LabTests
        {
            get { return _labTests ?? (_labTests = new Collection<PatientLabTest>()); }
            set { _labTests = value; }
        }

        private ICollection<PreventiveMedicineItem> _preventiveMedicine;
        public ICollection<PreventiveMedicineItem> PreventiveMedicine
        {
            get { return _preventiveMedicine ?? (_preventiveMedicine = new Collection<PreventiveMedicineItem>()); }
            set { _preventiveMedicine = value; }
        }

        private ICollection<MedicalProcedure> _medicalProcedures;
        public ICollection<MedicalProcedure> MedicalProcedures
        {
            get { return _medicalProcedures ?? (_medicalProcedures = new Collection<MedicalProcedure>()); }
            set { _medicalProcedures = value; }
        }

        private ICollection<DischargePaper> _dischargePapers;
        public ICollection<DischargePaper> DischargePapers
        {
            get { return _dischargePapers ?? (_dischargePapers = new Collection<DischargePaper>()); }
            set { _dischargePapers = value; }
        }

        private ICollection<MinistryOfAgricultureReport> _ministryOfAgricultureReports;
        public ICollection<MinistryOfAgricultureReport> MinistryOfAgricultureReports
        {
            get { return _ministryOfAgricultureReports ?? (_ministryOfAgricultureReports = new Collection<MinistryOfAgricultureReport>()); }
            set { _ministryOfAgricultureReports = value; }
        }

        private ICollection<Letter> _letters;
        public ICollection<Letter> Letters
        {
            get { return _letters ?? (_letters = new Collection<Letter>()); }
            set { _letters = value; }
        }

        private ICollection<CalenderEntry> _calenderEntries;
        public ICollection<CalenderEntry> CalenderEntries
        {
            get { return _calenderEntries ?? (_calenderEntries = new Collection<CalenderEntry>()); }
            set { _calenderEntries = value; }
        }

        private ICollection<FollowUp> _followUps;
        public ICollection<FollowUp> FollowUps
        {
            get { return _followUps ?? (_followUps = new Collection<FollowUp>()); }
            set { _followUps = value; }
        }

        public int? OldId { get; set; }

        //האם כלב נחיה
        public bool SeeingEyeDog { get; set; }

        //האם יש פטור מאגרת כלבת
        public bool Exemption { get; set; }

        //סיבת הפטור
        public string ExemptionCause { get; set; }

        public int ProfilePicSizeInBytes { get; set; }

        public string oldAnimalType { get; set; }
        public string oldAnimalRace { get; set; }
    }
}
