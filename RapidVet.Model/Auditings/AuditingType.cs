﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RapidVet.Model.Auditings
{
    public class AuditingType
    {
        [Key]
        public int RecordID { get; set; }

        public string TypeName { get; set; }
    }
}
