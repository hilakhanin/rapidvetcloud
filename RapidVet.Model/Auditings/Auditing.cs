﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using RapidVet.Model.Users;

namespace RapidVet.Model.Auditings
{
    public class Auditing
    {
        [Key]
        public int RecordID { get; set; }

        public int ClinicId { get; set; }

        public int UserId { get; set; }

        public string LogType { get; set; }

        /// <summary>
        /// תוכן חדש
        /// </summary>
        public string LogDescription { get; set; }

        /// <summary>
        /// תוכן ישן
        /// </summary>
        public string OldContent { get; set; }

        public DateTime DateStamp { get; set; }

        [NotMapped]
        public string UserName { get; set; }

        [NotMapped]
        public string ClinicGroupName { get; set; }

        [NotMapped]
        public string ClinicName { get; set; }

        public int LogTypeID { get; set; }
    }
}
